package com.practice.dbaccess;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class DataAcceess<T> {

	public List<T> select(String query, Class<T> cl) throws SQLException {
		ResultSetHandler<List<T>> rsh = new BeanListHandler<T>(cl);
		QueryRunner run = new QueryRunner();
		try {
			return run.query(DbConnectionManager.getConnection(), query, rsh);
		} catch (SQLException e) {
			System.out.println("Error occured while Executing Query: " + query);
			throw e;
		}
	}
}
