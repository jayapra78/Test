package com.practice.dbaccess;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DbConnectionManager {

	private static DbConnectionManager dbConnection;
	private static String DB_CONFIG_FILE = "DbConfig.properties";
	private Connection con;
	private String url;
	private String driverClassName;
	private String dataBase;
	private String userName;
	private String passWord;

	public static Connection getConnection() {
		return DbConnectionManager.getInstance().con;
	}
	
	private DbConnectionManager(){
		this.loadDbConnectionDetails();
		this.createDbConnection();
	}
	
	public static DbConnectionManager getInstance(){
		if(dbConnection == null){
			dbConnection = new DbConnectionManager(); 
		}
		return dbConnection;
	}
	
	private void loadDbConnectionDetails(){
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			input = new FileInputStream("E:/Projects/J2EETestWorkSpace/CommonParts/config/DbConfig.properties");
			//Load property file
			prop.load(input);
			//prop.load(getClass().getClassLoader().getResourceAsStream(DB_CONFIG_FILE));
			
			//get values from property file
			url = prop.getProperty("url");
			driverClassName = prop.getProperty("driver");
			dataBase = prop.getProperty("database");
			userName = prop.getProperty("username");
			passWord = prop.getProperty("password");
			
		} catch (IOException e) {
			System.out.println("Error occured while loading " + DB_CONFIG_FILE );
		}
	}
	
	private void createDbConnection(){
		try {
			Class.forName(driverClassName);
			con = DriverManager.getConnection(url+dataBase + "?useSSL=false", userName, passWord);
		} catch (Exception e) {
			System.out.println("Error while creating Database Connection URL :"
					+ url + dataBase + ", USERNAME :" + userName
					+ ", PASSWORD :" + passWord 
					+ "\n\n" + e.getMessage());
		}
		System.out.println("Databese connection created DB : " + url + dataBase);
	}
	
}
