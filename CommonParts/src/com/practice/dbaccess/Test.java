package com.practice.dbaccess;

import java.util.List;
import java.sql.SQLException;


import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class Test {

	public static void main(String[] args) throws SQLException {
		DataAcceess<Employee> t = new DataAcceess<>();
		
		
		List<Employee> emplist = t.select("SELECT emp_id,emp_name FROM employeee", Employee.class);
		
		for(Employee emp: emplist){
			System.out.println(emp.getEmp_id() + "--" + emp.getEmp_name());
		}
		
		
	}
	
	
}
