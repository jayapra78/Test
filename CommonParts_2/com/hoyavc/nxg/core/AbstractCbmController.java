/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00001;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00002;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00003;

/**
 * Abstract class for business CBM classes, it defines invoking method to invoke
 * CBM & DAO.
 * 
 * This class implements the methods to invoke other
 * <code>{@link com.hoyavc.nxg.core.CbmController CbmController}</code> and
 * <code>{@link com.hoyavc.nxg.core.DataAccessObjectInvoker DataAccessObjectInvoker}</code>
 * implementation classes
 * 
 * A CBM class can invoke other CBM and DAO classes.
 * 
 * <p>
 * "*WithNewTransaction" methods are used for nested transaction
 * </p>
 * 
 */
public abstract class AbstractCbmController implements CbmController {

	/** Cbm Controller Invoker Id spring bean id. */
	private static final String CBM_CONTROLLER_INVOKER_ID = "cbmControllerInvokerImpl";

	/** Data Access Object Invoker Id spring bean id. */
	private static final String DATA_ACCESS_OBJECT_INVOKER_ID = "dataAccessObjectInvokerImpl";

	/** Nest Transaction Controller spring bean id. */
	private static final String NEST_TRANSACTION_CONTROLLER = "nestTransactionController";

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());

	/**
	 * To invoke the implementation class of <code>CbmControllerInvoker</code>.
	 * The <code>CbmControllerInvoker</code> will be invoked with the current
	 * or existing transaction.
	 * 
	 * @param cbmControllerId
	 *            is the cbm controller bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject implementation class, passed to business classes
	 *            for business operations
	 * @return ValueObject The implementation of business CBM class
	 *         {@link CbmControllerInvoker#invoke(String, TransactionContext, ValueObject) invoke}
	 *         returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbmControllerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	protected final ValueObject invokeCbmController(
			final String cbmControllerId, final TransactionContext trxContext,
			final ValueObject vo) throws ApplicationException {

		// parameter validating
		validateParams(trxContext, vo);

		if (cbmControllerId == null || cbmControllerId.trim().length() <= 0) {
			handleBeanIdException(CbmControllerInvoker.class, trxContext);
		}

		// get the spring bean using id
		CbmControllerInvoker cbmControllerInvoker = ConfigurationManager
				.<CbmControllerInvoker> getBean(CBM_CONTROLLER_INVOKER_ID);

		// invoking CbmControllerInvoker
		return cbmControllerInvoker.invoke(cbmControllerId, trxContext, vo);
	}

	/**
	 * To invoke the implementation class of
	 * <code>DataAccessObjectInvoker</code>. The
	 * <code>DataAccessObjectInvoker</code> will be invoked With the current
	 * or existing transaction.
	 * 
	 * @param daoId
	 *            is the dao bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @return ValueObject The implementation of business CBM class {@link
	 *         DataAccessObjectInvoker#invoke(String, TransactionContext,
	 *         ValueObject) invoke } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If daoId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	protected final ValueObject invokeDataAccessObject(final String daoId,
			final TransactionContext trxContext, final ValueObject vo)
			throws ApplicationException {

		// parameter validating
		validateParams(trxContext, vo);

		if (daoId == null || daoId.trim().length() <= 0) {
			handleBeanIdException(DataAccessObjectInvoker.class, trxContext);
		}

		// get the spring bean using id
		DataAccessObjectInvoker dataAccessObjectInvoker = ConfigurationManager
				.<DataAccessObjectInvoker> getBean(DATA_ACCESS_OBJECT_INVOKER_ID);

		// invoking DataAccessObjectInvoker
		return dataAccessObjectInvoker.invoke(daoId, trxContext, vo);
	}

	/**
	 * To invoke the implementation class of CbmControllerInvoker with a new
	 * transaction. A nested transaction will be created to invoke the
	 * CbmControllerInvoker.
	 * 
	 * @param cbmControllerId
	 *            is the cbm controller bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @return ValueObject The implementation of business CBM class {@link
	 *         CbmControllerInvoker#invoke(String, TransactionContext,
	 *         ValueObject) invoke } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbmControllerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 * 
	 */

	protected final ValueObject invokeCbmControllerWithNewTransaction(
			final String cbmControllerId, final TransactionContext trxContext,
			final ValueObject vo) throws ApplicationException {

		// invoking NestTransactionStrategy
		return this.invokeCbmControllerWithNewTransaction(cbmControllerId, trxContext, vo, false);
	}

	/**
	 * To invoke the implementation class of CbmControllerInvoker with a new
	 * transaction. A nested transaction will be created to invoke the
	 * CbmControllerInvoker.
	 * 
	 * @param cbmControllerId
	 *            is the cbm controller bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @param isNewTransaction
	 * 			  Boolean, True: to create new Processing GMT DateTime.
	 *                     False: Use the same processing GMT DateTime.
	 * 
	 * @return ValueObject The implementation of business CBM class {@link
	 *         CbmControllerInvoker#invoke(String, TransactionContext,
	 *         ValueObject) invoke } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbmControllerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 * 
	 */

	protected final ValueObject invokeCbmControllerWithNewTransaction(
			final String cbmControllerId, final TransactionContext trxContext,
			final ValueObject vo, final boolean isNewTransaction) throws ApplicationException {

		// parameter validating
		validateParams(trxContext, vo);

		if (cbmControllerId == null || cbmControllerId.trim().length() <= 0) {
			handleBeanIdException(CbmControllerInvoker.class, trxContext);
		}

		// get the spring bean using id
		NestTransactionController nestTransactionController = ConfigurationManager
				.<NestTransactionController> getBean(NEST_TRANSACTION_CONTROLLER);

		NestTransactionStrategy nestTransactionStrategy = nestTransactionController
				.getNestTransactionStrategy();

		// invoking NestTransactionStrategy
		return nestTransactionStrategy.invokeWithNewTransaction(
				cbmControllerId, CBM_CONTROLLER_INVOKER_ID, trxContext, vo, isNewTransaction);
	}
	
	/**
	 * To invoke the implementation class of DataAccessObjectInvoker with a new
	 * transaction. A nested transaction will be created to invoke the
	 * DataAccessObjectInvoker.
	 * 
	 * @param daoId
	 *            is the cbm controller bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @return ValueObject The implementation of business CBM class {@link
	 *         DataAccessObjectInvoker#invoke(String, TransactionContext,
	 *         ValueObject) invoke } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If daoId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 * 
	 * </ul>
	 */

	protected final ValueObject invokeDataAccessObjectWithNewTransaction(
			final String daoId, final TransactionContext trxContext,
			final ValueObject vo) throws ApplicationException {

		return this.invokeDataAccessObjectWithNewTransaction(daoId, trxContext, vo, false);
	}

	/**
	 * To invoke the implementation class of DataAccessObjectInvoker with a new
	 * transaction. A nested transaction will be created to invoke the
	 * DataAccessObjectInvoker.
	 * 
	 * @param daoId
	 *            is the cbm controller bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @param isNewTransaction
	 * 			  Boolean, True: To create new Processing GMT DateTime.
	 *                     False: Use the same processing GMT DateTime.
	 *                     
	 * @return ValueObject The implementation of business CBM class {@link
	 *         DataAccessObjectInvoker#invoke(String, TransactionContext,
	 *         ValueObject) invoke } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If daoId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 * 
	 * </ul>
	 */

	protected final ValueObject invokeDataAccessObjectWithNewTransaction(
			final String daoId, final TransactionContext trxContext,
			final ValueObject vo, final boolean isNewTransaction) throws ApplicationException {

		// parameter validating
		validateParams(trxContext, vo);

		if (daoId == null || daoId.trim().length() <= 0) {
			handleBeanIdException(DataAccessObjectInvoker.class, trxContext);
		}

		// get the spring bean using id
		NestTransactionController nestTransactionController = ConfigurationManager
				.<NestTransactionController> getBean(NEST_TRANSACTION_CONTROLLER);

		NestTransactionStrategy nestTransactionStrategy = nestTransactionController
				.getNestTransactionStrategy();

		// invoking NestTransactionStrategy
		return nestTransactionStrategy.invokeWithNewTransaction(daoId,
				DATA_ACCESS_OBJECT_INVOKER_ID, trxContext, vo, isNewTransaction);
	}

	/**
	 * To get the NXGLogger instance for logging messages.
	 * 
	 * @return NXGLogger instance.
	 */
	protected final NXGLogger getLogger() {
		return logger;
	}

	/**
	 * Method to handle the Bean ID exception, In-case of bean id is null or
	 * empty this method will be invoked.
	 * 
	 * @param beanType
	 *            Bean class type
	 * @param trxContext
	 *            TransactionContext object
	 * @throws SystemException
	 */
	private void handleBeanIdException(final Class beanType,
			final TransactionContext trxContext) {

		Message message = fjfe00003.createMessage(new String[] { beanType
				.getName() });
		logger.error(trxContext, message, new NullPointerException());
		throw new SystemException(message, new NullPointerException());
	}

	/**
	 * Method to validate the TransactionContext and ValueObject.
	 * 
	 * @param trxContext
	 *            TransactionContext object
	 * @param vo
	 *            An implementation class for ValueObject
	 * 
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	private void validateParams(final TransactionContext trxContext,
			final ValueObject vo) {

		if (trxContext == null) {
			Message message = fjfe00001.createMessage();
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (vo == null) {
			Message message = fjfe00002.createMessage();
			logger.error(trxContext, message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
	}
}
