/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.*;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Abstract class for business CBS classes, it defines invoking method to invoke
 * CBS & CBM.
 * 
 * <p>
 * This class implements the methods to invoke other
 * <code>{@link com.hoyavc.nxg.core.CbsHandler CbsHandler}</code> or
 * <code>{@link com.hoyavc.nxg.core.CbmController CbmController}</code>
 * implementation classes
 * </p>
 * 
 * A CBS class can invoke other CBSHandlerInvoker and CBM classes.
 * 
 * <p>
 * "*WithNewTransaction" methods are used for NestedTransaction
 * </p>
 * 
 */
public abstract class AbstractCbsHandler implements CbsHandler {

	/** Cbm Controller Invoker Id spring bean id. */
	private static final String CBM_CONTROLLER_INVOKER_ID = "cbmControllerInvokerImpl";

	/** Cbs Handler Invoker Id spring bean id. */
	private static final String CBS_HANDLER_INVOKER_ID = "cbsHandlerInvokerImpl";

	/** Nest Transaction Controller spring bean id. */
	private static final String NEST_TRANSACTION_CONTROLLER = "nestTransactionController";

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());
	
	private final NXGLogger logger_local =  DefaultLogger.getInstance(AbstractCbsHandler.class);
	
	/** JBOSS_JVM_USED_SIZE_TAG tag name. */
	private static final String JBOSS_JVM_USED_SIZE_TAG = "JBOSS_JVM_USED_SIZE";

	/** DefaultEnvironmentVariables */
	private DefaultEnvironmentVariables defaultEnvironmentVariables;

	/**
	 * @return the defaultEnvironmentVariables
	 */
	public DefaultEnvironmentVariables getDefaultEnvironmentVariables() {
		return defaultEnvironmentVariables;
	}

	/**
	 * @param defaultEnvironmentVariables the defaultEnvironmentVariables to set
	 */
	public void setDefaultEnvironmentVariables() {
		DefaultEnvironmentVariables defaultEnvironmentVariables = new DefaultEnvironmentVariables();
		defaultEnvironmentVariables.setCONF_FOLDER("conf");
		defaultEnvironmentVariables.setCONF_FILE("jboss-jvm-memory-conf.xml");
		defaultEnvironmentVariables.setROOT_TAG("JBOSS_JVM_MEMORY");
		defaultEnvironmentVariables.setLIST_TAG("LIST");
		defaultEnvironmentVariables.setVALUE_TAG("VALUE");
		defaultEnvironmentVariables.init();
		
		this.defaultEnvironmentVariables = defaultEnvironmentVariables;
	}
	

	/**
	 * To invoke the implementation class of <code>CbsHandlerInvoker</code>.
	 * The <code>CbsHandlerInvoker</code> will be invoked with the current or
	 * existing transaction.
	 * 
	 * @param cbsHandlerId
	 *            is the cbs handler bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @return ValueObject The implementation of business CBS class {@link
	 *         CbsHandlerInvoker#invoke(String, TransactionContext, ValueObject)
	 *         invoke } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbsHandlerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	protected final ValueObject invokeCbsHandler(final String cbsHandlerId,
			final TransactionContext trxContext, final ValueObject vo)
			throws ApplicationException {

		try {
			// parameter validating
			validateParams(trxContext, vo);
	
			// get the spring bean using id
			if (cbsHandlerId == null || cbsHandlerId.trim().length() <= 0) {
				handleBeanIdException(CbsHandlerInvoker.class, trxContext);
			}
	
			CbsHandlerInvoker cbsHandlerInvoker = ConfigurationManager
					.<CbsHandlerInvoker> getBean(CBS_HANDLER_INVOKER_ID);
	
			// invoking CbsHandlerInvoker
			return cbsHandlerInvoker.invoke(cbsHandlerId, trxContext, vo);
		} catch (SystemException e){
			if(logger_local.isDebugEnabled()){
				Message message = fjfe00008.createMessage();
				logger_local.debug(message, e);
			}
			throw e;
		}
	}

	/**
	 * To invoke the implementation class of <code>CbmControllerInvoker</code>.
	 * The <code>CbmControllerInvoker</code> will be invoked with the current
	 * or existing transaction .
	 * 
	 * @param cbmControllerId
	 *            is the cbm controller bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @return ValueObject The implementation of business CBM class {@link
	 *         CbmControllerInvoker#invoke(String, TransactionContext,
	 *         ValueObject) invoke } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbmControllerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	protected final ValueObject invokeCbmController(
			final String cbmControllerId, final TransactionContext trxContext,
			final ValueObject vo) throws ApplicationException {
		try{
			// parameter validating
			validateParams(trxContext, vo);
	
			if (cbmControllerId == null || cbmControllerId.trim().length() <= 0) {
				handleBeanIdException(CbmControllerInvoker.class, trxContext);
			}
	
			// get the spring bean using id
			CbmControllerInvoker cbmControllerInvoker = ConfigurationManager
					.<CbmControllerInvoker> getBean(CBM_CONTROLLER_INVOKER_ID);
	
			long jbossFreeMemorySizeBeforeInvokeCbmInMB = 0;
//			if (logger.isDebugEnabled()) {
				jbossFreeMemorySizeBeforeInvokeCbmInMB = Runtime.getRuntime().freeMemory();
//			}
			
			// invoking CbmControllerInvoker
			ValueObject returnVo = (ValueObject) cbmControllerInvoker.invoke(cbmControllerId, trxContext, vo);
			
			this.checkJbossFreeMemory(jbossFreeMemorySizeBeforeInvokeCbmInMB, cbmControllerId);
			
			return returnVo;
			
		} catch (SystemException e){
			if(logger_local.isDebugEnabled()){
				Message message = fjfe00008.createMessage();
				logger_local.debug(message, e);
			}
			throw e;
		}
	}

	/**
	 * To invoke the implementation class of CbsHandlerInvoker with a new
	 * transaction. A nested transaction will be created to invoke the
	 * CbsHandlerInvoker.
	 * 
	 * @param cbsHandlerId
	 *            is the cbs handler invoker bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @return ValueObject The implementation of business CBS class {@link
	 *         CbsHandlerInvoker#invoke(String, TransactionContext, ValueObject)
	 *         invoke } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbsHandlerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	protected final ValueObject invokeCbsHandlerWithNewTransaction(
			final String cbsHandlerId, final TransactionContext trxContext,
			final ValueObject vo) throws ApplicationException {


		// invoking NestTransactionStrategy
		return this.invokeCbsHandlerWithNewTransaction(cbsHandlerId, trxContext, vo, false);
	}

	/**
	 * To invoke the implementation class of CbsHandlerInvoker with a new
	 * transaction. A nested transaction will be created to invoke the
	 * CbsHandlerInvoker.
	 * 
	 * @param cbsHandlerId
	 *            is the cbs handler invoker bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @param isNewTransaction
	 * 			  Boolean, True: to create new Processing GMT DateTime.
	 *                     False: Use the same processing GMT DateTime.
	 *                     
	 * @return ValueObject The implementation of business CBS class {@link
	 *         CbsHandlerInvoker#invoke(String, TransactionContext, ValueObject)
	 *         invoke } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbsHandlerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	protected final ValueObject invokeCbsHandlerWithNewTransaction(
			final String cbsHandlerId, final TransactionContext trxContext,
			final ValueObject vo, final boolean isNewTransaction) throws ApplicationException {

		try{ 
			// parameter validating
			validateParams(trxContext, vo);
	
			if (cbsHandlerId == null || cbsHandlerId.trim().length() <= 0) {
				handleBeanIdException(CbsHandlerInvoker.class, trxContext);
			}
	
			// get the spring bean using id
			NestTransactionController nestTransactionController = ConfigurationManager
					.<NestTransactionController> getBean(NEST_TRANSACTION_CONTROLLER);
	
			NestTransactionStrategy nestTransactionStrategy = nestTransactionController
					.getNestTransactionStrategy();
	
			// invoking NestTransactionStrategy
			return nestTransactionStrategy.invokeWithNewTransaction(cbsHandlerId,
					CBS_HANDLER_INVOKER_ID, trxContext, vo, isNewTransaction);
		} catch (SystemException e){
			if(logger_local.isDebugEnabled()){
				Message message = fjfe00008.createMessage();
				logger_local.debug(message, e);
			}
			throw e;
		}
		
	}
	/**
	 * To invoke the implementation class of CbmControllerInvoker with a new
	 * transaction. A nested transaction will be created to invoke the
	 * CbmControllerInvoker.
	 * 
	 * @param cbmControllerId
	 *            is the cbm controller bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @return ValueObject The implementation of business CBM class {@link
	 *         CbmControllerInvoker#invoke(String, TransactionContext,
	 *         ValueObject) invoke } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbmControllerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	protected final ValueObject invokeCbmControllerWithNewTransaction(
			final String cbmControllerId, final TransactionContext trxContext,
			final ValueObject vo) throws ApplicationException {

		// invoking NestTransactionStrategy
		return this.invokeCbmControllerWithNewTransaction(cbmControllerId, trxContext, vo, false);
	}

	/**
	 * To invoke the implementation class of CbmControllerInvoker with a new
	 * transaction. A nested transaction will be created to invoke the
	 * CbmControllerInvoker.
	 * 
	 * @param cbmControllerId
	 *            is the cbm controller bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @param isNewTransaction
	 * 			  Boolean, True: to create new Processing GMT DateTime.
	 *                     False: Use the same processing GMT DateTime.
	 *            
	 * @return ValueObject The implementation of business CBM class {@link
	 *         CbmControllerInvoker#invoke(String, TransactionContext,
	 *         ValueObject) invoke } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbmControllerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	protected final ValueObject invokeCbmControllerWithNewTransaction(final String cbmControllerId,
			final TransactionContext trxContext, final ValueObject vo, final boolean isNewTransaction)
			throws ApplicationException {

		try {

			// parameter validating
			validateParams(trxContext, vo);

			if (cbmControllerId == null || cbmControllerId.trim().length() <= 0) {
				handleBeanIdException(CbmControllerInvoker.class, trxContext);
			}

			// get the spring bean using id
			NestTransactionController nestTransactionController = ConfigurationManager
					.<NestTransactionController> getBean(NEST_TRANSACTION_CONTROLLER);

			NestTransactionStrategy nestTransactionStrategy = nestTransactionController.getNestTransactionStrategy();
			
			long jbossFreeMemorySizeBeforeInvokeCbmInMB = 0;
//			if (logger.isDebugEnabled()) {
				jbossFreeMemorySizeBeforeInvokeCbmInMB = Runtime.getRuntime().freeMemory();
//			}
			// invoking NestTransactionStrategy
			ValueObject outVo = nestTransactionStrategy.invokeWithNewTransaction(cbmControllerId,
					CBM_CONTROLLER_INVOKER_ID, trxContext, vo, isNewTransaction);
			
			this.checkJbossFreeMemory(jbossFreeMemorySizeBeforeInvokeCbmInMB, cbmControllerId);

			return outVo;
		} catch (SystemException e) {
			if (logger_local.isDebugEnabled()) {
				Message message = fjfe00008.createMessage();
				logger_local.debug(message, e);
			}
			throw e;
		}
	}
	
	/**
	 * Method to check JBOSS free memory decreasing size.
	 * 
	 * @param jbossFreeMemorySizeBeforeInvokeCbmInMB long
	 * @param cbmControllerId 
	 */
	private void checkJbossFreeMemory(long jbossFreeMemorySizeBeforeInvokeCbmInMB, String cbmControllerId) {
		//if (logger.isDebugEnabled()) {
			long jbossFreeMemorySizeAfterInvokeCbmInMB = Runtime.getRuntime().freeMemory();
			
			if(null == defaultEnvironmentVariables) {
				// set Values from Environmental Variables
				this.setDefaultEnvironmentVariables();
			}
			
			int jbossMemorySizeInEnvVariables = (null != defaultEnvironmentVariables.getStringValue(JBOSS_JVM_USED_SIZE_TAG) ? 
					Integer.valueOf(defaultEnvironmentVariables.getStringValue(JBOSS_JVM_USED_SIZE_TAG)) : 100);
			
			jbossMemorySizeInEnvVariables = jbossMemorySizeInEnvVariables * 1048576;
			
			if((jbossFreeMemorySizeBeforeInvokeCbmInMB - jbossFreeMemorySizeAfterInvokeCbmInMB) > jbossMemorySizeInEnvVariables) { 
				
				BigDecimal decimalObject = new BigDecimal(
						String.valueOf(jbossFreeMemorySizeBeforeInvokeCbmInMB - jbossFreeMemorySizeAfterInvokeCbmInMB));
				
				StringBuilder param = new StringBuilder("");
				param.append("Memory occupied by CBM(ID) : " + cbmControllerId + " is ");
				param.append(this.convertJbossFreeMemoryIntoMB(decimalObject.doubleValue()));
				logger.info(CoreMessageCodeEnum.fjfi80002.createMessage(new String[] { param.toString()}));
			}
		//}
	}

	/**
	 * Method to convert bytes to MB.
	 * 
	 * @param jbossFreeMemorySizeInBytes
	 * @return String
	 */
	private String convertJbossFreeMemoryIntoMB(double jbossFreeMemorySizeInBytes) {
		
		DecimalFormat twoDecimalForm = new DecimalFormat("#.####");
		final double MB = 1024L * 1024L;
		
		double tempBytes = jbossFreeMemorySizeInBytes/MB;
		String bytesToSuitableUnit = twoDecimalForm.format(tempBytes);
		
		return bytesToSuitableUnit + " MB.";
	}

	/**
	 * To get the NXGLogger instance for logging messages.
	 * 
	 * @return NXGLogger instance.
	 */
	protected final NXGLogger getLogger() {
		return logger;
	}

	/**
	 * Method to handle the Bean ID exception, In-case of bean id is null or
	 * empty this method will be invoked.
	 * 
	 * @param beanType
	 *            Bean class type
	 * @param trxContext
	 *            TransactionContext object
	 * @throws SystemException
	 */
	private void handleBeanIdException(final Class beanType,
			final TransactionContext trxContext) {

		Message message = fjfe00003.createMessage(new String[] { beanType
				.getName() });
		logger.error(trxContext, message, new NullPointerException());
		throw new SystemException(message, new NullPointerException());
	}

	/**
	 * Method to validate the TransactionContext and ValueObject.
	 * 
	 * @param trxContext
	 *            TransactionContext object
	 * @param vo
	 *            An implementation class for ValueObject
	 * 
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	private void validateParams(final TransactionContext trxContext,
			final ValueObject vo) {

		if (trxContext == null) {
			Message message = fjfe00001.createMessage();
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (vo == null) {
			Message message = fjfe00002.createMessage();
			logger.error(trxContext, message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
	}

}
