/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;

/**
 * Abstract class for SQL DAO classes. It maintains the reference of
 * SqlDataAccessStrategy.
 * 
 */
public abstract class AbstractSqlDataAccessObject implements DataAccessObject {

	/** Data access strategy for SQL DAO. */
	private SqlDataAccessStrategy sqlDataAccessStrategy;

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());

	/**
	 * To get the NXGLogger instance for logging messages.
	 * 
	 * @return NXGLogger instance.
	 */
	protected final NXGLogger getLogger() {
		return logger;
	}

	/**
	 * To get the SqlDataAccessStrategy instance for SQL DAO.
	 * 
	 * @return SqlDataAccessStrategy
	 */
	public SqlDataAccessStrategy getSqlDataAccessStrategy() {
		return sqlDataAccessStrategy;
	}

	/**
	 * To set the SqlDataAccessStrategy instance for SQL DAO.
	 * 
	 * @param sqlDataAccessStrategy
	 *            SQL DAO strategy
	 * @throws SystemException
	 *             If sqlDataAccessStrategy is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setSqlDataAccessStrategy(
			final SqlDataAccessStrategy sqlDataAccessStrategy) {

		// validating parameter
		if (sqlDataAccessStrategy == null) {
			String[] params = new String[] { SqlDataAccessStrategy.class
					.getName() };
			Message message = fjff40001.createMessage(params);

			// Using InitializationErrorLogger to log error message
			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.sqlDataAccessStrategy = sqlDataAccessStrategy;
	}
}
