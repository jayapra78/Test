/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                                
 *  2007/05/17 <Change NO.0001> created by Alex (SRM) 
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;

/**
 * Abstract class for SQL DAO classes. It maintains the reference of
 * SqlDataAccessStrategy.
 * 
 */
public abstract class AbstractStoredProcedureDataAccessObject implements
		DataAccessObject {

	/** Data access strategy for SQL DAO. */
	private StoredProcedureDataAccessStrategy procedureDataAccessStrategy;

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());

	/**
	 * To get the NXGLogger instance for logging messages.
	 * 
	 * @return NXGLogger instance.
	 */
	protected final NXGLogger getLogger() {
		return logger;
	}

	/**
	 * To get the SqlDataAccessStrategy instance for SQL DAO.
	 * 
	 * @return SqlDataAccessStrategy
	 */
	public StoredProcedureDataAccessStrategy getProcedureDataAccessStrategy() {
		return procedureDataAccessStrategy;
	}

	/**
	 * To set the SqlDataAccessStrategy instance for SQL DAO.
	 * 
	 * @param procedureDataAccessStrategy
	 *            Procedure Data Access Strategy
	 * @throws SystemException
	 *             If sqlDataAccessStrategy is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setProcedureDataAccessStrategy(
			final StoredProcedureDataAccessStrategy procedureDataAccessStrategy) {

		// validating parameter
		if (procedureDataAccessStrategy == null) {
			String[] params = new String[] { SqlDataAccessStrategy.class
					.getName() };
			Message message = fjff40001.createMessage(params);

			// Using InitializationErrorLogger to log error message
			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.procedureDataAccessStrategy = procedureDataAccessStrategy;
	}
}
