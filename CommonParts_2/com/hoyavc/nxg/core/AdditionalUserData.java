/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import java.io.Serializable;

/**
 * Marking Interface for AdditionalUserData classes.
 * 
 * @see UserData
 */
public interface AdditionalUserData extends Serializable {

}
