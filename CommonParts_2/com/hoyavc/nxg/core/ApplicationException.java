/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */

package com.hoyavc.nxg.core;

/**
 * Wrapper class for business logic exceptions in NXG framework.
 * 
 * <p>
 * The database transaction will be rollback after the ApplicationException.
 * but {@link ApplicationExceptionForCommit} used for the transactions that to
 * be committed after the exception.
 * </p>
 * 
 * @see SystemException
 * @see ApplicationExceptionForCommit
 * 
 */
public class ApplicationException extends Exception {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/** To handle the NXG's MessageCode and Message. */
	private final Message messageObject;

	/**
	 * To get the Message object.
	 * 
	 * @return messageObject Message
	 */
	public Message getMessageObject() {
		return messageObject;
	}

	/**
	 * Create an instance of ApplicationException with NXG
	 * com.hoyavc.nxg.core.Message as parameter.
	 * 
	 * <p>
	 * If messageObject is null then java.lang.Throwable#getMessage() will be
	 * null otherwise "messagecode + message" as message
	 * </p>
	 * 
	 * @param messageObject
	 *            Message with messageCode and message String
	 */
	public ApplicationException(final Message messageObject) {
		super(messageObject == null ? null : messageObject.toString());
		this.messageObject = messageObject;
	}

	/**
	 * Create an instance of ApplicationException with
	 * com.hoyavc.nxg.core.Message and Throwable as parameter.
	 * 
	 * <p>
	 * If messageObject is null then java.lang.Throwable#getMessage() will be
	 * null otherwise "messagecode + message" as message
	 * </p>
	 * 
	 * @param messageObject
	 *            Message with messageCode and message String
	 * @param throwable
	 *            The occurred exception
	 */
	public ApplicationException(final Message messageObject,
			final Throwable throwable) {

		super(messageObject == null ? null : messageObject.toString(),
				throwable);
		this.messageObject = messageObject;
	}
}
