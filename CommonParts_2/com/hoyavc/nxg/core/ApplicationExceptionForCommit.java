/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */

package com.hoyavc.nxg.core;

/**
 * Wrapper class for business logic exceptions in NXG framework with transaction
 * committed.
 * 
 * <p>
 * All database transaction occurred before the ApplicationExceptionForCommit
 * will be committed. but {@link ApplicationException} used to rollback the
 * transactions after the exception
 * </p>
 * 
 * @see SystemException
 * @see ApplicationException
 * 
 */
public class ApplicationExceptionForCommit extends ApplicationException {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create an instance of ApplicationExceptionForCommit with
	 * com.hoyavc.nxg.core.Message as parameter.
	 * 
	 * <p>
	 * If messageObject is null then java.lang.Throwable#getMessage() will be
	 * null otherwise "messagecode + message" as message
	 * </p>
	 * 
	 * @param messageObject
	 *            Message with messageCode and message String
	 */
	public ApplicationExceptionForCommit(final Message messageObject) {
		super(messageObject);
	}

	/**
	 * Create an instance of ApplicationExceptionForCommit with
	 * com.hoyavc.nxg.core.Message and Throwable as parameter.
	 * 
	 * <p>
	 * If messageObject is null then java.lang.Throwable#getMessage() will be
	 * null otherwise "messagecode + message" as message
	 * </p>
	 * 
	 * @param messageObject
	 *            Message with messageCode and message String
	 * @param throwable
	 *            The occurred exception
	 */
	public ApplicationExceptionForCommit(final Message messageObject,
			final Throwable throwable) {
		super(messageObject, throwable);

	}

}
