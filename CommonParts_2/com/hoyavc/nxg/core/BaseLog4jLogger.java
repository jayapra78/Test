/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40002;

/**
 * LogParts class. This class requires Log4J 1.2.8.
 */
final class BaseLog4jLogger {

	/** Logger. */
	private org.apache.log4j.Logger logger;

	/**
	 * Constructor.
	 * 
	 * @param logger
	 *            Logger instance
	 */
	private BaseLog4jLogger(final org.apache.log4j.Logger logger) {
		this.logger = logger;
	}

	/**
	 * Get instance of LogParts.
	 * 
	 * @param className
	 *            Output log base class name.
	 * @return Instance of this class.
	 * @throws SystemException
	 *             If className is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40002 fjff40002})
	 */
	static BaseLog4jLogger getInstance(final String className) {

		// validating parameter
		if (className == null || className.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(), className };
			Message message = fjff40002.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(BaseLog4jLogger.class);
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		return new BaseLog4jLogger(org.apache.log4j.Logger.getLogger(className));
	}

	/**
	 * Get instance of LogParts.
	 * 
	 * @param classObject
	 *            Output log base Class class.
	 * @return Instance of this class.
	 * @throws SystemException
	 *             If classObject is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40002 fjff40002})
	 */
	static BaseLog4jLogger getInstance(final Class classObject) {

		// validating parameter
		if (classObject == null) {
			String[] params = new String[] { Class.class.getName(), null };
			Message message = fjff40002.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(BaseLog4jLogger.class);
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		return getInstance(classObject.getName());
	}

	/**
	 * Method to Log debug message.
	 * 
	 * @param message
	 *            String
	 */
	void debug(final String message) {
		logger.debug(message);
	}

	/**
	 * Method to Log debug message.
	 * 
	 * @param message
	 *            String
	 * @param throwable
	 *            Throwable
	 */
	void debug(final String message, final Throwable throwable) {
		logger.debug(message, throwable);
	}

	/**
	 * Method to Log info message.
	 * 
	 * @param message
	 *            String
	 */
	void info(final String message) {
		logger.info(message);
	}

	/**
	 * Method to Log info message.
	 * 
	 * @param message
	 *            String
	 * @param throwable
	 *            Throwable
	 */
	void info(final String message, final Throwable throwable) {
		logger.info(message, throwable);
	}

	/**
	 * Method to Log warn message.
	 * 
	 * @param message
	 *            String
	 */
	void warn(final String message) {
		logger.warn(message);
	}

	/**
	 * Method to Log warn message.
	 * 
	 * @param message
	 *            String
	 * @param throwable
	 *            Throwable
	 */
	void warn(final String message, final Throwable throwable) {
		logger.warn(message, throwable);
	}

	/**
	 * Method to Log error message.
	 * 
	 * @param message
	 *            String
	 */
	void error(final String message) {
		logger.error(message);
	}

	/**
	 * Method to Log error message.
	 * 
	 * @param message
	 *            String
	 * @param throwable
	 *            Throwable
	 */
	void error(final String message, final Throwable throwable) {
		logger.error(message, throwable);
	}

	/**
	 * Method to Log fatal message.
	 * 
	 * @param message
	 *            String
	 */
	void fatal(final String message) {
		logger.fatal(message);
	}

	/**
	 * Method to Log fatal message.
	 * 
	 * @param message
	 *            String
	 * @param throwable
	 *            Throwable
	 */
	void fatal(final String message, final Throwable throwable) {
		logger.fatal(message, throwable);

	}

	/**
	 * Method to find debug is enabled or not.
	 * 
	 * @return boolean
	 */
	boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}

	/**
	 * Method to find Info is enabled or not.
	 * 
	 * @return boolean
	 */
	boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}

}
