/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * An implementation class of <code>{@link ValueObject}</code>, accepts
 * <code>boolean</code> value only.
 * 
 * @see ValueObject
 * @see ValueObjectList
 * 
 */
@XmlRootElement(name = "com.hoyavc.nxg.core.BooleanValueObject")
public class BooleanValueObject implements ValueObject {

	/** serial value. */
	private static final long serialVersionUID = 1L;

	/** <code>boolean</code> value variable, default is <code>false</code>. */
	private boolean booleanValue = false;;

	/**
	 * getter method for <code>boolean</code> value.
	 * 
	 * @return boolean
	 */
	public boolean isBooleanValue() {
		return booleanValue;
	}

	/**
	 * setter method for <code>boolean</code> value.
	 * 
	 * @param booleanValue
	 *            Boolean Value
	 */
	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}

}
