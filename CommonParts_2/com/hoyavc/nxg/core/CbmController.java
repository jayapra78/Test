/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for CbmController classes.
 * 
 * @see com.hoyavc.nxg.core.AbstractCbmController
 */
public interface CbmController {

	/**
	 * For business logic implementations. The CbmController implementation
	 * classes should implement this method for business logic.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param vo
	 *            ValueObject
	 * @return ValueObject as result
	 * @throws ApplicationException
	 *             business logic exception
	 */
	ValueObject execute(final TransactionContext trxContext,
			final ValueObject vo) throws ApplicationException;

}
