/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for CBM controller invoker classes.
 * 
 * @see com.hoyavc.nxg.core.CbmControllerInvokerImpl
 */
public interface CbmControllerInvoker {

	/**
	 * CbmController bean invoker interface. The CbmControllerInvoker
	 * implementation class implements the invoke method to invoke the given CBM
	 * controller bean id.
	 * 
	 * @param cbmControllerId
	 *            CBM controller ID
	 * @param trxContext
	 *            TransactionContext
	 * @param vo
	 *            ValueObject
	 * @return ValueObject as result
	 * @throws ApplicationException
	 *             business logic exception
	 */
	ValueObject invoke(final String cbmControllerId,
			final TransactionContext trxContext, final ValueObject vo)
			throws ApplicationException;

}
