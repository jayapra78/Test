/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00001;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00002;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00003;

/**
 * An implementation of <code>CbmControllerInvoker</code>, it defines invoke
 * method for invoking CBM controller.
 * 
 */
class CbmControllerInvokerImpl implements CbmControllerInvoker {

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());

	/**
	 * To invoke the business CBM controller's execute method. Gets the spring
	 * bean for the given id <code>cbmControllerId</code> and invoke the
	 * execute method.
	 * 
	 * @param cbmControllerId
	 *            is the CBM controller bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @return ValueObject The implementation of business CBM class {@link
	 *         CbmController#execute(TransactionContext, ValueObject) } returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbmControllerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If TransactionContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If ValueObject is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	public ValueObject invoke(final String cbmControllerId,
			final TransactionContext trxContext, final ValueObject vo)
			throws ApplicationException {

		// validating parameters
		validateParams(cbmControllerId, trxContext, vo);

		// get the spring bean using id
		CbmController cbmController = ConfigurationManager
				.<CbmController> getBean(cbmControllerId);

		// invoking execute method of business cbm controller
		return cbmController.execute(trxContext, vo);
	}

	/**
	 * Method to validate parameters.
	 * 
	 * @param cbmControllerId
	 *            bean id
	 * @param trxContext
	 *            TransactionContext
	 * @param vo
	 *            ValueObject
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbmControllerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If TransactionContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If ValueObject is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	private void validateParams(final String cbmControllerId,
			final TransactionContext trxContext, final ValueObject vo) {

		if (trxContext == null) {
			Message message = fjfe00001.createMessage();
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (cbmControllerId == null || cbmControllerId.trim().length() <= 0) {
			String[] params = new String[] { CbmController.class.getName(),
					cbmControllerId };
			Message message = fjfe00003.createMessage(params);

			logger.error(trxContext, message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (vo == null) {
			Message message = fjfe00002.createMessage();
			logger.error(trxContext, message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

	}
}
