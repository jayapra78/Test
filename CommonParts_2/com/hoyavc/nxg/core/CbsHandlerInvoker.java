/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for CBS handler invoker classes.
 * 
 * @see com.hoyavc.nxg.core.CbsHandlerInvokerImpl
 */
public interface CbsHandlerInvoker {

	/**
	 * CbsHandlerInvoker bean invoker interface. The CbsHandlerInvoker
	 * implementation class implements the invoke method to invoke the given CBS
	 * handler bean id.
	 * 
	 * @param cbsHandlerId
	 *            CBS Handler ID
	 * @param trxContext
	 *            TransactionContext
	 * @param vo
	 *            ValueObject
	 * @return ValueObject as result
	 * @throws ApplicationException
	 *             business logic exception
	 */
	ValueObject invoke(final String cbsHandlerId,
			final TransactionContext trxContext, final ValueObject vo)
			throws ApplicationException;

}
