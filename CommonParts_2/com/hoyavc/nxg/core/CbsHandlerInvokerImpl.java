/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00001;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00002;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00003;

/**
 * An implementation of <code>CbmControllerInvoker</code>, it defines invoke
 * method for invoking CBM controller.
 */
class CbsHandlerInvokerImpl implements CbsHandlerInvoker {

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());

	/**
	 * To invoke the business CBS handler's execute method. Gets the spring bean
	 * for the given id <code>cbsHandlerId</code> and invoke the execute
	 * method.
	 * 
	 * @param cbsHandlerId
	 *            is the CBS handler bean id
	 * @param trxContext
	 *            TransactionContext object with UserData
	 * @param vo
	 *            ValueObject
	 * @return ValueObject The implementation of business CBS class
	 *         {@link CbsHandler#execute(TransactionContext, ValueObject)}
	 *         returns
	 * 
	 * @throws ApplicationException
	 *             business logic exception
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbsHandlerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If TransactionContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If ValueObject is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	public ValueObject invoke(final String cbsHandlerId,
			final TransactionContext trxContext, final ValueObject vo)
			throws ApplicationException {

		// validating parameters
		validateParams(cbsHandlerId, trxContext, vo);

		// get the spring bean using id
		CbsHandler cbsHandler = ConfigurationManager
				.<CbsHandler> getBean(cbsHandlerId);

		// invoking execute method of business cbs handler
		return cbsHandler.execute(trxContext, vo);
	}

	/**
	 * Method to validate parameters.
	 * 
	 * @param cbsHandlerId
	 *            bean id
	 * @param trxContext
	 *            TransactionContext
	 * @param vo
	 *            ValueObject
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If cbsHandlerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00003 fjfe00003})
	 *             <li>If TransactionContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If ValueObject is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	private void validateParams(final String cbsHandlerId,
			final TransactionContext trxContext, final ValueObject vo) {

		if (trxContext == null) {
			Message message = fjfe00001.createMessage();
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (cbsHandlerId == null || cbsHandlerId.trim().length() <= 0) {
			String[] params = new String[] { CbsHandler.class.getName(),
					cbsHandlerId };
			Message message = fjfe00003.createMessage(params);

			logger.error(trxContext, message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (vo == null) {
			Message message = fjfe00002.createMessage();
			logger.error(trxContext, message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
	}
}
