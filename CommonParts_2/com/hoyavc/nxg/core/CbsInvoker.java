/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for CbsInvoker classes.
 * 
 */
public interface CbsInvoker {

	/**
	 * Method to invoke CBS Handler.
	 * 
	 * @param cbsHandlerId
	 *            CBS handler bean Id
	 * @param trxContext
	 *            TransactionContext
	 * @param vo
	 *            ValueObject
	 * @return ValueObject
	 * @throws ApplicationException Application Exception
	 */
	ValueObject invokeCbs(final String cbsHandlerId,
			final TransactionContext trxContext, final ValueObject vo)
			throws ApplicationException;

}
