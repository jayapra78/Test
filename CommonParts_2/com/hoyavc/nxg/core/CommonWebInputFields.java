/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 * Change Tracking                                         
 * 2007/06/06 <Change NO.0001> created by Alex (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for Common WebService input fields.
 * 
 * <p>
 * In Web Service, this interface's implementation class uses to pass user's
 * session id from NXG web service client like VB.NET to NXG server. The
 * implementation class of CommonWebInputFields should implement
 * ValueObject also. WebService Input VOs must implement this interface. If the
 * Input VO's instance field is ValueObject, those ValueObjects no need to
 * implement this interface.
 * </p>
 * 
 */
public interface CommonWebInputFields {

	/**
	 * Get User's Session id.
	 * 
	 * @return String Session ID
	 */
	String getSessionId();

	/**
	 * Set User's Session id.
	 * 
	 * @param sessionId
	 *            Session Id
	 */
	void setSessionId(String sessionId);

}
