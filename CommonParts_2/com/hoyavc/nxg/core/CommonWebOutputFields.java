/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 * Change Tracking                                                 
 * 2007/06/06 <Change NO.0001> created by Alex (SRM)  
 * 2010/07/14 <Change NO.0002> File renamed by Baskaran C(SRM)
 */
package com.hoyavc.nxg.core;

/**
 * Interface for Common Web Client output fields.
 * 
 * <p>
 * In Web Service, this interface's implementation class uses to pass warning
 * message from the NXG server to NXG web service invoker like VB.NET. The
 * implementation class of CommonWebOutputFields should implement
 * ValueObject also. WebService Output VOs must implement this interface. If the
 * Output VO's instance field is ValueObject, those ValueObjects no need to
 * implement this interface.
 * </p>
 * 
 * @see com.hoyavc.nxg.core.web.AbstractRestCbsInvokeService
 */
public interface CommonWebOutputFields {

	/**
	 * get WarningMessages array.
	 * 
	 * @return WarningMessageForWeb array
	 */
	WarningMessageForWeb[] getWarningMessages();

	/**
	 * set WarningMessages array.
	 * 
	 * @param warningMessages
	 *            WarningMessageForWeb array
	 */
	void setWarningMessages(WarningMessageForWeb[] warningMessages);
}
