/*
 * Copyright 2006 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT)       
 *  2007/08/23 <Change NO.0002> Added Java Doc by Alex (SRM) 
 *  2008/12/04 <Change NO.0003> Removed synchronization of the method "getBean" for performance by N.Fukuda (VCIT) 
 *  2010/10/06 <EJB Invoker feature> updated by Dhanasekaran (NT)
 *  2010/11/08 <EJB Invoker feature> subsystem is derived from VO instead of getCanonicalBeanName
 *  								(reverted the addition of getCanonicalBeanName)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40201;
import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40202;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00101;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00102;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00103;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00104;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * This class acts as a bean container.
 * <p>
 * Using ClassPathXmlApplicationContext, it loads all the beans from spring configuration file.
 * This class is a central interface for accessing the beans used in NXG application.
 * </p>
 *
 */
public final class ConfigurationManager {

	/** ApplicationContext instance. */
	private static ApplicationContext appContext;
	
	/** NXGLogger instance. */
	private static final NXGLogger logger = DefaultLogger.getInstance(ConfigurationManager.class);	

	/** Private Constructor. */
	private ConfigurationManager() {
	}
	
	/**
	 * Creates ClassPathXmlApplicationContext and loads all bean defintions from configuration files.
	 * Files should be in classpath.
	 * 
	 * @param path
	 *            String array contains configuration file paths.
	 * @throws SystemException
	 *             If any one of the following cases occurs
	 *             <ul>
	 *             <li>Configuration file path is null or empty. 
	 *             	({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40201 fjff40201})
	 *             </li>
	 *             <li>Exception is thrown while initializing ConfigurationManager for Web. 
	 *             ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40202 fjff40202})
	 *             </li>
	 *             </ul>           
	 */
	public static void init(final String[] path) {

		if (path == null || path.length == 0) {
			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(ConfigurationManager.class);
			initializationErrorLogger.fatal(fjff40201.createMessage(),
					new NullPointerException());
			throw new SystemException(fjff40201.createMessage(),
					new NullPointerException());
		}
		
		for (String filePath : path) {
			if (filePath == null || filePath.length() <= 0) {
				NXGLogger initializationErrorLogger = InitializationErrorLogger
						.getInstance(ConfigurationManager.class);
				initializationErrorLogger.fatal(fjff40201.createMessage(),
						new NullPointerException());
				throw new SystemException(fjff40201.createMessage(),
						new NullPointerException());
			}
		}
		
		synchronized (ConfigurationManager.class) {
			
			try {
				
				appContext = new ClassPathXmlApplicationContext(path);
			} catch (BeansException beanExp) {
				NXGLogger initializationErrorLogger = InitializationErrorLogger
						.getInstance(ConfigurationManager.class);
				String[] params = new String[] { beanExp.getMessage() };
				initializationErrorLogger.fatal(fjff40202.createMessage(params),
						beanExp);
				throw new SystemException(fjff40202.createMessage(params), beanExp);
			}
		}
	}
	
	/**
	 * Reload the spring configuration from inputted configuration files.
	 * This is used for overriding certain bean configurations dynamically without halting the application.
	 * It overrides existing beans if already beans exists.
	 * Configuration files can be placed anywhere in the file system.
	 * 
	 * @param filePath String array contains configuration file paths.
	 * 
	 * @throws SystemException
	 *             If any one of the following cases occurs
	 *             <ul>
	 *             <li>Configuration file path is null or empty. 
	 *             	({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00101 fjfe00101})
	 *             </li>
	 *             <li>Exception is thrown while reloading ConfigurationManager. 
	 *             ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00102 fjfe00102})
	 *             </li>
	 *             </ul>           
	 */
	public static void reloadFromFile(final String[] filePath){
		
		if (filePath == null || filePath.length <= 0) {
			throw createSystemException(fjfe00101, null,
					new NullPointerException());
		}
		
		for (String path : filePath) {
			if (path == null || path.length() <= 0) {
				throw createSystemException(fjfe00101, null,
						new NullPointerException());
			}
		}
		
		synchronized (ConfigurationManager.class) {
			ApplicationContext newAppContext = null;
			
			try {
				newAppContext = new FileSystemXmlApplicationContext(filePath,
						appContext);
			} catch (BeansException beansExp) {
				String[] params = new String[] { beansExp.getMessage() };
				logger.error(fjfe00102.createMessage(params), beansExp);
			}
			
			if (newAppContext != null) {
				appContext = newAppContext;
			}
		}		
	}

	/**
	 * Loads all bean defintions from configuration files and adding it into ApplicationContext.
	 * It is useful for adding new beans while doing Unit Testing.
	 * 
	 * @param path
	 *            String array contains configuration file paths.
	 */
	static void addContext(final String[] path) {
		// temporary
		synchronized (ConfigurationManager.class) {
			appContext = new ClassPathXmlApplicationContext(path, appContext);

		}

	}

	/**
	 * Retrieves bean using bean id.
	 * If ApplicationContext is null, 
	 * it will initialize the ApplicationContext using hard coded configuration file paths.
	 * ClassCastException never occurs inside this method, 
	 * since it helps type checking at Compiletime only not in Runtime.
	 * 
	 * Do not call this method while AppContext is being instantiated.
	 * 
	 * @param beanId Bean Id
	 * @return Bean instance.
	 * 
	 * @throws SystemException
	 *             If any one of the following cases occurs
	 *             <ul>
	 *             <li>Bean Id is null or empty. 
	 *             	({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00103 fjfe00103})
	 *             </li>
	 *             <li>No Bean is available for Bean Id. 
	 *             ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00104 fjfe00104})
	 *             </li>
	 *             </ul>           
	 */
	public static <E> E getBean(final String beanId) {
		
//		synchronized (ConfigurationManager.class) { // comment out for performance by N.Fukuda 2008.12.04
			if (appContext == null) {
				final String[] path = new String[] { "/nxg-conf.xml",
						"/core-conf.xml" };

				init(path);
			}

			if(beanId == null || beanId.trim().length() <= 0) {
				String[] params = new String[] { beanId };
				throw createSystemException(fjfe00103, params, new NullPointerException());
			}
			
			Object object = null;
			try {
				object = appContext.getBean(beanId);
			} catch (BeansException beansExp) {
				String[] params = new String[] { beansExp.getMessage() };
				throw createSystemException(fjfe00104, params, beansExp);
			}

			E returnObject = (E) object;
			
			return returnObject;

		}

//	}

	/**
	 * Replace the old bean instance with new one.
	 * It is useful for replacing old beans with new beans while doing Unit Testing.
	 * 
	 * @param beanName Name of the Bean
	 * @param newBean Bean instance.
	 */
	static void registerNewSingletonBean(String beanName, Object newBean) {

		synchronized (ConfigurationManager.class) {
			// temporary
			ClassPathXmlApplicationContext classPathXmlApplicationContext = (ClassPathXmlApplicationContext) appContext;

			DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) classPathXmlApplicationContext
					.getBeanFactory();

			defaultListableBeanFactory.destroySingleton(beanName);

			defaultListableBeanFactory.registerSingleton(beanName, newBean);

		}

	}

	/**
	 * Creates SystemException and returns it.
	 * 
	 * @param messageCode
	 *            Message code.
	 * @param params
	 *            Parameters for the message.
	 * @param exception
	 *            Exception.
	 * 
	 * @return SystemException
	 */
	private static SystemException createSystemException(
			final CoreMessageCodeEnum messageCode, final String[] params,
			final Exception exception) {
		Message message = messageCode.createMessage(params);
		logger.error(message, exception);
		return new SystemException(message, exception);
	}
}