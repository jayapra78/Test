/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * enum class to handle MessageCodes which used when the NXG framework
 * initialization time.
 * 
 */
public enum CoreFatalMessageCodeEnum implements MessageCode {

	/** Initialization Error: Bean cannot be set to null [{0}]. */
	fjff40001,

	/** Initialization error: Null Argument(s) [parameter(s):{0}] [value(s) {1}] . */
	fjff40002,

	/** Error reading resource file [{0}]. */
	fjff40003,

	/** JNDI Name exception {0}. */
	fjff40004,

	/** Class Cast Exception: {0}. */
	fjff40005,

	/** Unexpected Exception: {0}. */
	fjff40006,

	/** No resource bundle is available for file name [{0}]. */
	fjff40007,

	/** web.xml context-param [{0}] value should not be null or empty [{1}]. */
	fjff40008,

	
	/** Number of arguments is invalid [{0}] for Batch. */
	fjff40101,

	/** Core configuration path is invalid [{0}] for Batch. */
	fjff40102,

	/** Business batch configuration path is invalid [{0}]. */
	fjff40103,

	/** Batch Main ID is null or empty [{0}]. */
	fjff40104,

	/** Batch InVo ID is null or empty [{0}]. */
	fjff40105,

	/** Exception is thrown while initializing ConfigurationManager for Batch[{0}]. */
	fjff40106,
	
	/** Business batch configuration file path is invalid [{0}]. */
	fjff40107,
	
	/** Spring configuration files classpath is null or empty. */
	fjff40201,

	/** Exception is thrown while initializing ConfigurationManager [{0}]. */
	fjff40202,

	/** Environment conf file could not parse. */
	fjff40301,

	/** Full file name for configurating envronment variables is not found. [{0}]. */
	fjff40302,

	/** SAXParseException has occurred in configurating envronment variables. [{0}]. */
	fjff40303,

	/** NXG EJB Client environment variables initialization error. [{0}] is null. */
	fjff40304,

	/** Tag name is irregal. It must be specified from [{0}]. */
	fjff40305,

	/** Message : {0} may contain invalid characters. */ 
	fjfw50001,


	/** {0} : [{1}]. */
	fjfi90001;

	/**
	 * Array of String is converted in to single String which is in comma
	 * separated format.
	 * 
	 * @param params
	 *            Array of String (varargs param)
	 * 
	 * @return Single String which contains inputed Strings in comma separated
	 *         format.
	 * 
	 */
	public String appendParamter(final String[] params) {

		if (params == null || params.length == 0) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		for (String s : params) {
			sb.append(s);
			sb.append(',');
		}
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length() - 1);
		}
		return sb.toString();
	}

	/**
	 * Method to create Message string with given params.
	 * 
	 * @param params
	 *            Message parameter
	 * @return Message object
	 */
	public Message createMessage(final String[] params) {
		return InitializationErrorMessageFactory.getMessage(this, params);
	}

	/**
	 * Method to create Message string.
	 * 
	 * @return Message object
	 */
	public Message createMessage() {
		return InitializationErrorMessageFactory.getMessage(this);
	}

}
