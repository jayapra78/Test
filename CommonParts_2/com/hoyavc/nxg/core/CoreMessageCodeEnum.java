/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM) 
 *  2007/05/21 <Change NO.0003> Message codes for Stored Procedure is added by Alex (SRM)
 *  2010/07/20 <Change NO.0004> Message code fjfe00613 is added by Kalai.P (SRM)   
 *  2010/09/17 <Change NO.0005> Message code fjfe30003 is added by Kalai.P (SRM) 
 *  2011/02/22 <Change NO.0006> Message code fjfe00614 is added by Kalai.P (SRM)     
 */
package com.hoyavc.nxg.core;

/**
 * enum class to handle MessageCodes which used when the NXG framework.
 * 
 */
public enum CoreMessageCodeEnum implements MessageCode {

	/** Transaction context is null. */ 
	fjfe00001,

	/** Value Object is null. */ 
	fjfe00002,

	/** Bean id is null or empty for {0}. */ 
	fjfe00003,

	/** Null Argument(s) [parameter(s):{0}] [value(s) {1}] . */ 
	fjfe00004,

	/** SQL Exception {0}. */ 
	fjfe00005,

	/** User data is null. */ 
	fjfe00006,

	/** Returned Value Object is null. */ 
	fjfe00007,
	
	/** System Exception occurred at server. Error Log {0}. */ 
	fjfe00008,
	
	/** Spring configuration filepath is null or empty for reloading configuration. */
	fjfe00101,
	
	/** Exception is thrown while reloading the ConfigurationManager But Configuration has not been changed [{0}]. */
	fjfe00102,
	
	/** Bean Id is null or empty [{0}]. */
	fjfe00103,

	/** No Bean is available for Bean Id [{0}]. */
	fjfe00104,

	
	/** Processing GMT Time is null or empty [{0}]. */ 
	fjfe00501,

	/** Processing GMT Time has a Format error [{0}]. */ 
	fjfe00502,

	/** Locale is null in Date Time Util. */ 
	fjfe00503,
	

	/** Column not found [column : {0}]. */ 
	fjfe00601,

	/**
	 * Column type mismatching [column : {0}, given type: {1}, available type:
	 * {2}].
	 */ 
	fjfe00602,

	/** Error in TimeStamp {0}. */ 
	fjfe00603,

	/** Exception in reading Blob object {0}, cause{1}. */ 
	fjfe00604,

	/** Exception in creating Blob object {0}. */ 
	fjfe00605,

	/** SQL Query result map is null. */ 
	fjfe00606,

	/** Exception in reading Clob object {0}, cause{1}. */ 
	fjfe00607,

	/**
	 * SQL Exception thrown while retrieving Blob content for column : [{0}],
	 * cause : {1}.
	 */ 
	fjfe00608,

	/**
	 * SQL Exception thrown while retrieving Clob content for column : [{0}],
	 * cause : {1}.
	 */ 
	fjfe00609,

	/**
	 * Clone Not Supported Exception thrown while cloning SQL Parameter List
	 * instance : {0}.
	 */ 
	fjfe00610,

	/** Start row should not be less than 1 for High Volume Data retrieval : {0}. */ 
	fjfe00611,

	/**
	 * Number of records to be fetched should not be less than 1 for High Volume
	 * Data retrieval : [{0}].
	 */ 
	fjfe00612,
	
	/** {0} data type is not supported by {1}. */ 
	fjfe00613,
	
	/** {0} is not supported in {1}. */ 
	fjfe00614,	

	/** SQL Exception is thrown while executing Stored Procedure : [{0}]. */ 
	fjfe00651,

	/**
	 * Exception is thrown while adding cursor parameter into stored procedure
	 * parameter list : {0}.
	 */ 
	fjfe00652,

	/**
	 * No matching result for Parameter in Stored Procedure Result [Parameter
	 * name : {0}].
	 */ 
	fjfe00653,

	/** Stored Procedure result map is null. */ 
	fjfe00654,



	/** Security Exception when invoking {0}. */ 
	fjfe00701,

	/** No Method found {0} . */ 
	fjfe00702,

	/** Arguments not match for the method {0}, Given arguments [{1}]. */ 
	fjfe00703,

	/** Illegal Access Exception. */ 
	fjfe00704,
	

	/**
	 * HibernateException is thrown from HibernateDAO. [DAO class] : {0},
	 * [cause] : {1}, [message] : {2}.
	 */ 
	fjfe00801,

	/** Unexpected Exception : {0}. */ 
	fjfe10001,

	/** inVo is not of the type wrapperVo. */ 
	fjfe10002,

	/** Invoker type does not match: {0}. */ 
	fjfe10003,

	/** Return Object is not an instance of Value Object: {0}. */ 
	fjfe10004,
	

	/** JMS Exception thrown while sending message : {0}. */ 
	fjfe10101,

	/** JMS Exception thrown while closing the queue connection : {0}. */ 
	fjfe10102,

	/** Serializable object used in JMS Messaging is null. */ 
	fjfe10103,
	

	/** {0} is caught in session bean {1}. */ 
	fjfe15001,

	/**
	 * [{0}] is caught in message driven bean [{1}]. message code is [{2}]
	 * message is [{3}].
	 */ 
	fjfe15002,


	/** Transaction Context is null and cannot be retrieved. */ 
	fjfe20001,

	/** Illegal Argument for back path. Must start with "/": {0}. */ 
	fjfe20002,

	/** MessageCode is null in the Message object. */ 
	fjfe20004,

	/** Object in the Session is not Value Object type :{0}. */ 
	fjfe20005,

	/** User's session id is invalid [{0}]. */ 
	fjfe20006,

	/**
	 * Request parameter value is null or empty [ param name : {0}] in Entry
	 * Servlet.
	 */ 
	fjfe20007,

	/** URL value is null or empty [{0}] for key name [{1}] in Entry Servlet. */ 
	fjfe20008,

	/** ApplicationException is caught in TransactionContextFactoryForWeb [{0}]. */ 
	fjfe20009,

	/** User is unauthentic user [session id : {0}]. */ 
	fjfe20010,

	/** Given file is not found [{0}] for download. */ 
	fjfe20011,

	/** Error downloading file. */ 
	fjfe20012,

	/** Error closing Servlet output stream for download. */ 
	fjfe20013,

	/** Error closing File input stream for download. */ 
	fjfe20014,

	/** Invalid character encoding [{0}] for download. */ 
	fjfe20015,

	/** Exception thrown from Default Filter [{0}]. */ 
	fjfe20016,

	/** Error occurred when file upload [{0}]. */ 
	fjfe20017,

	/** Character encoding error in reading Form field [{0}] in File Upload filter. */ 
	fjfe20018,
	
	/** Error searching for JSF Message resource bundle: {0}. */ 
	fjfe25001,

	/** Error reading JSF Message [{0}]. */ 
	fjfe25002,

	/**
	 * Date Time format is not applicable for the locale [{0}] in Utility
	 * Backing bean.
	 */ 
	fjfe25003,

	/** Locale is null in Utility Backing bean. */ 
	fjfe25004,

	/** JSF tag: Invalid value [{0}] for the attribute [{1}]. */ 
	fjfe25101,

	/** JSF tag: No such component : [{0}]. */ 
	fjfe25102,

	/** JSF tag: [{0}] component's value is not of type [{1}]. */ 
	fjfe25103,

	/** JSF tag: [{0}] attribute should not be null or empty, given value [{1}]. */ 
	fjfe25104,

	/** JSF tag: Object is not of type {0}. */ 
	fjfe25105,

	/**
	 * JSF tag: Invalid value [{0}] for the attribute [{1}], should be true or
	 * false.
	 */ 
	fjfe25106,

	/** JSF tag: [{0}] attribute's value is not of type [{1}]. */ 
	fjfe25107,

	/** JSF tag: Date Format is not applicable for component attribute. */ 
	fjfe25108,

	/** JSF tag: Attribute should not be null or empty, given value [{0}]. */ 
	fjfe25109,

	/** JSF tag: [{0}] should not be null or empty, given value of [{1}]. */ 
	fjfe25201,

	/** No Backing Bean is available for key [{0}]. */ 
	fjfe25202,

	/** JSF tag attribute value should be EL [{0}]. */ 
	fjfe25203,

	/** JSF tag: Object is not of type {0}. */ 
	fjfe25204,

	/** JSF tag: [{0}] attribute's value is not of type [{1}]. */ 
	fjfe25205,

	/**
	 * JSF tag: Invalid value [{0}] for the attribute [{1}], should be true or
	 * false.
	 */ 
	fjfe25206,

	/** JSF tag: Locale is null. */ 
	fjfe25207,

	/** HighVolumeNavigateParams is null. */ 
	fjfe25301,

	/** JSF tag, Invalid value [{0}] for the attribute [{1}], should be integer. */ 
	fjfe25302,

	/**
	 * JSF tag, Invalid value [{0}] for the attribute [{1}], should be positive
	 * integer.
	 */ 
	fjfe25303,

	/**
	 * Error in creating "Exception xml" : {0}, But SystemException can't be
	 * thrown.
	 */ 
	fjfe29001,

	/** Unexpected Exception: {0}. */ 
	fjfe29004,

	/** Batch login user ID is null or empty [{0}]. */ 
	fjfe30001,

	/** Batch login password is null or empty [{0}]. */ 
	fjfe30002,
	
	/** Batch login corporation code is null or empty [{0}]. */ 
	fjfe30003,

	/** Batch login IP address is null or empty [{0}]. */ 
	fjfe30004,

	/** SystemException is thrown while executing batch login process. */ 
	fjfe30005,

	/** ApplicationException is thrown while executing batch login process. */ 
	fjfe30006,

	/** Unexpected exception is thrown while executing batch login process. */ 
	fjfe30007,

	/** SystemException is thrown while executing batch main process. */ 
	fjfe30008,

	/** ApplicationException is thrown while executing batch main process. */ 
	fjfe30009,

	/** Unexpected exception is thrown while executing batch main process. */ 
	fjfe30010,

	/**
	 * Application Exception is caught in Transaction Context Factory For Batch
	 * [{0}].
	 */ 
	fjfe30011,

	/** SystemException is thrown while executing batch logout process. */ 
	fjfe30012,

	/** CbsHandler ID for batch to get AdditionalUserData is null or empty [{0}]. */
	fjfe30013,

	/** Could not get additional user data [{0}]. */
	fjfe30014,

	/** {0} has started{1}. */
	fjfd90001, 
	
	/** {0} has finished{1}. */
	fjfd90002,
	
	/** [{0}]Elapsed time is {1} ms. */
	fjfd90003,

	/** SQL : [{0}]. */
	fjfd90004,
	
	/** SQL Result count : [{0}]. */
	fjfd90005,
	
	/** Cannot read JSF Message [{0}] . */ 
	fjfw50002,
	
	fjfi80001,
	
	fjfi80002;

	/**
	 * Array of String is converted in to single String which is in comma
	 * separated format.
	 * 
	 * @param params
	 *            Array of String (args param)
	 * 
	 * @return Single String which contains inputted Strings in comma separated
	 *         format.
	 * 
	 */
	public String appendParamter(final String... params) {

		if (params == null || params.length == 0) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		for (String s : params) {
			sb.append(s);
			sb.append(',');
		}
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length() - 1);
		}
		return sb.toString();
	}

	/**
	 * Method to create Message string with given params.
	 * 
	 * @param params
	 *            message parameter
	 * @return Message object
	 */
	public Message createMessage(final String[] params) {
		return SimpleMessageFactory.getMessage(this, params);
	}

	/**
	 * Method to create Message string.
	 * 
	 * @return Message object
	 */
	public Message createMessage() {
		return SimpleMessageFactory.getMessage(this);
	}

}
