/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;
import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40006;
import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjfi90001;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00005;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

/**
 * An implementation of <code>DBConnectionManager</code> to get database
 * connection thru data source.
 * <p>
 * It instantiate and initialization at the startup as bean
 */
class DBConnectionManagerImpl implements DBConnectionManager, InitialProcess {

	/** The reference variable for DataSource. */
	private DataSource dataSource;

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());

	/**
	 * Init method to write Debug log.
	 */
	@Override
	public void init() {
		
		try {
			
			Message message = fjfi90001.createMessage(new String[] {"DB Connection ", dataSource.getConnection().getMetaData().getURL()});
			NXGLogger initializationErrorLogger = InitializationErrorLogger.getInstance(this.getClass());
			initializationErrorLogger.info(message);
			
		} catch (Exception e) {
			NXGLogger initializationErrorLogger = InitializationErrorLogger.getInstance(this.getClass());
			String[] params = new String[] { "DB Connection info not found : "+e.getMessage() };
			Message message = fjff40006.createMessage(params);
			initializationErrorLogger.fatal(message, e);
		}
	}
	
	/**
	 * To get the database connection thru DataSource.
	 * 
	 * @return Connection database connection
	 * @throws SystemException
	 *             If any SQLException occurs ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 */
	public Connection getConnection() throws SystemException {
		try {
			return dataSource.getConnection();
		} catch (SQLException e) {

			Message message = fjfe00005.createMessage(new String[] { e
					.getMessage() });

			logger.error(message, e);
			throw new SystemException(message, e);
		}
	}

	/**
	 * getter method for dataSource.
	 * 
	 * @return dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * setter method for dataSource. At the startup it initialize.
	 * 
	 * @param dataSource
	 *            Data Source
	 * @throws SystemException
	 *             If dataSource is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setDataSource(final DataSource dataSource) {

		if (dataSource == null) {
			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			String[] params = new String[] { DataSource.class.getName(), null };
			Message message = fjff40001.createMessage(params);

			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.dataSource = dataSource;
	}

	
}
