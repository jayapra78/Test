/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for data access object classes.
 * 
 * <p>
 * Two different Data access classes
 * <code>{@link AbstractHibernateDataAccessObject}</code> and
 * <code>{@link AbstractSqlDataAccessObject}</code> which uses Hibernate and
 * SQL DAO respectively
 * </p>
 * 
 */
public interface DataAccessObject {

	/**
	 * 
	 * For DAO business logic implementations. The DataAccessObject
	 * implementation classes should implement this method for business logic.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param vo
	 *            ValueObject
	 * @return ValueObject as result
	 * @throws ApplicationException
	 *             business logic exception
	 */
	ValueObject execute(final TransactionContext trxContext,
			final ValueObject vo) throws ApplicationException;

}
