/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40006;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00503;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Current dateTime as NXG formatted "yyyyMMddHHmmssSSS" ex: "20070315035647980" .
 * 
 */
public final class DateTimeUtil {

	
	/** GMT TimeZone. */
	private static final TimeZone GMT_TIME_ZONE = TimeZone.getTimeZone("GMT");

	/** DateFormat for "yyyyMMddHHmmssSSS". */
	private static final DateFormat processingGMTTimeFormat = new SimpleDateFormat(
			"yyyyMMddHHmmssSSS", new Locale(""));

	static {
		processingGMTTimeFormat.setTimeZone(GMT_TIME_ZONE);
	}

	/** Private Constructor. */
	private DateTimeUtil() {
	}
	
	/**
	 * Current GMT time in NXG format "yyyyMMddHHmmssSSS" ex:
	 * "20070315035647980".
	 * 
	 * @return String ProcessingGMTTime
	 */
	public static String getProcessingGMTTime() {

		Calendar cal = Calendar.getInstance();
		// Since DateFormat#format is not thread safe, we are getting seperate instance by using clone()
		return ((DateFormat) processingGMTTimeFormat.clone()).format(cal.getTime());
	}

	/**
	 * Method to get the DateTime pattern for the given locale.
	 * 
	 * @param locale Locale
	 * @return String representation of the DateTime pattern.
	 * @throws IllegalStateException
	 *             If the format is not matched.
	 * @throws SystemException
	 *             <ul>
	 *             <li>If locale is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00503 fjfe00503}).
	 *             <li>If the pattern is not matched ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40006 fjff40006}).
	 *             </ul>
	 */
	public static String getYMDPattern(final Locale locale) {

		if (locale == null) {
			NXGLogger logger = DefaultLogger.getInstance(DateTimeUtil.class);
			Message message = fjfe00503.createMessage();
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT,
				locale);

		final FieldPosition yearFieldPosition = new FieldPosition(
				DateFormat.YEAR_FIELD);
		final FieldPosition monthFieldPosition = new FieldPosition(
				DateFormat.MONTH_FIELD);
		final FieldPosition dayFieldPosition = new FieldPosition(
				DateFormat.DATE_FIELD);
		Date workDate = new Date();

		dateFormat.format(workDate, new StringBuffer(), yearFieldPosition);
		dateFormat.format(workDate, new StringBuffer(), monthFieldPosition);
		dateFormat.format(workDate, new StringBuffer(), dayFieldPosition);

		final int yearIndex = yearFieldPosition.getBeginIndex();
		final int monthIndex = monthFieldPosition.getBeginIndex();
		final int dayIndex = dayFieldPosition.getBeginIndex();

		if (yearIndex < monthIndex && yearIndex < dayIndex) {
			if (monthIndex < dayIndex) {
				return "yyyyMMddHHmmssSSS";
			}
			return "yyyyddMMHHmmssSSS";

		} else if (monthIndex < yearIndex && monthIndex < dayIndex) {
			if (yearIndex < dayIndex) {
				return "MMyyyyddHHmmssSSS";
			}
			return "MMddyyyyHHmmssSSS";

		} else if (dayIndex < yearIndex && dayIndex < monthIndex) {
			if (yearIndex < monthIndex) {
				return "ddyyyyMMHHmmssSSS";
			}
			return "ddMMyyyyHHmmssSSS";
		} else {
			NXGLogger logger = DefaultLogger.getInstance(DateTimeUtil.class);
			Message message = fjff40006
					.createMessage(new String[] { "Date Pattern" });
			logger.error(message);
			throw new SystemException(message);
		}

	}

}
