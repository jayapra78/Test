/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfd90004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfd90005;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00005;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

/**
 * An implementation of <code>SqlDataAccessStrategy</code> to get database
 * connection thru data source.
 * <p>
 * It instantiate and initialization at the startup as bean
 * 
 * <pre><code>
 *     code example:
 *      
 *     SqlDataAccessStrategy st = super.getSqlDataAccessStrategy();
 *     SqlPrameterList list = st.createSqlParameterList();
 *     
 *     StringBuilder str = new StringBuilder();
 *     str.append(&quot;select BASESTOCK_ITEMDETAIL_NUMBER, STOCK_PLACE_CODE &quot;);
 *     str.append(&quot;from  RSTK_M_BASESTOCK_ITEMDETAIL where DELETE_FLAG = '0' &quot;);
 *     str.append(&quot;and BASESTOCK_ITEMDETAIL_NUMBER = ? &quot;);
 *     str.append(&quot;and EFFECTIVE_END_DATETIME = ?&quot;);
 *     
 *     list.setString(itemdetailVo.getBasestockItemdetailNumber());
 *     list.setString(itemdetailVo.getEffectiveEndDatetime());
 *     
 *     List&lt;SqlReturnMap&gt; resultList = st.select(str.toString(), list);
 *     
 * </code></pre>
 * 
 */
abstract class DbUtilDataAccessStrategy implements SqlDataAccessStrategy {

	/** QueryRunner for SQL DAO. */
	private QueryRunner queryRunner = new QueryRunner();

	/** ResultSetHandler for SQL DAO. */
	private ResultSetHandler resultSetHandler;

	/** SqlParameterListFactory for SQL DAO. */
	private SqlParameterListFactory sqlParameterListFactory;

	/** database connection manager for DAO. */
	private DBConnectionManager dbConnectionManager;

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());

	/**
	 * Method to select the records from the database tables by specifying the
	 * query string and SqlParameterList.
	 * 
	 * @param queryLanguage
	 *            SQL query String
	 * @param paramList
	 *            SqlParameterList
	 * @return List<SqlReturnMap> Return value list containing SqlReturnMap
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If queryLanguage is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If SQLException occurs ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             </ul>
	 */
	@SuppressWarnings("unchecked")
	public List<SqlReturnMap> select(final String queryLanguage,
			final SqlParameterList paramList) {

		// validating parameter
		if (queryLanguage == null || queryLanguage.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(),
					queryLanguage };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		Connection con = null;

		try {
			// creating db connection
			con = dbConnectionManager.getConnection();
			Object[] param = paramList == null ? null : paramList.toArray();

			if (logger.isDebugEnabled()) {
				StackTraceElement[] stack = new Throwable().fillInStackTrace().getStackTrace();
				String className = stack[1].getClassName();
				if (className!= null && className.startsWith("com.hoyavc.nxg.core")) {
					className = stack[2].getClassName();
				}
				Message message = fjfd90004.createMessage(new String[] {className + " : "+fillValues(queryLanguage, param)});
				logger.debug(message);
			}
			// executing query
			List<SqlReturnMap> returnMapList = (List<SqlReturnMap>) queryRunner.query(con, queryLanguage,
					resultSetHandler, param);
			
			if (logger.isDebugEnabled()) {
				Message message = fjfd90005.createMessage(new String[] { String.valueOf(returnMapList.size())});
				logger.debug(message);
			}
			
			return returnMapList;
		} catch (SQLException se) {

			Message message = fjfe00005.createMessage(new String[] { se
					.getMessage() });

			logger.error(message, se);
			throw new SystemException(message, se);

		} finally {
			closeConnection(con);
			con = null;

		}
	}

    /**
     * Fill values to SQL
     * @param sql SQL query
     * @param values ParamValues
     * @return SQL
     */
	private static String fillValues(String sql, Object[] values) {

		if (sql == null)
			return null;
		StringBuilder sbl = new StringBuilder(sql);
		if (values != null) {
			int fromIndex = 0;
			for (Object object : values) {
				int index = sbl.indexOf("?", fromIndex);
				if (index != -1) {
					fromIndex = index;
					String replaceStr = null;
					if (object == null) {
						replaceStr = "'null'";
					} else {
						replaceStr = "'"
								+ String.valueOf(object).replace("$", "\\$")
								+ "'";
					}
					sbl.replace(index, index + 1, replaceStr);
				}
			}
			sql = sbl.toString();
		}
		
		return sql;
	}
	
	/**
	 * Method to update the records in the database table by specifying the query
	 * string and SqlParameterList.
	 * 
	 * @param queryLanguage
	 *            SQL query String
	 * @param paramList
	 *            SqlParameterList
	 * @return int Number of records updated successfully
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If queryLanguage is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If SQLException occurs ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             </ul>
	 * 
	 */
	public int update(final String queryLanguage,
			final SqlParameterList paramList) {

		Connection con = null;

		// validating parameter
		if (queryLanguage == null || queryLanguage.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(),
					queryLanguage };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		try {
			// creating db connection
			con = dbConnectionManager.getConnection();
			Object[] param = paramList == null ? null : paramList.toArray();

			if (logger.isDebugEnabled()) {
				StackTraceElement[] stack = new Throwable().fillInStackTrace().getStackTrace();
				Message message = fjfd90004.createMessage(new String[] {stack[1].getClassName() + " : "+fillValues(queryLanguage, param)});
				logger.debug(message);
			}

			// executing query
			int count = queryRunner.update(con, queryLanguage, param);

			if (logger.isDebugEnabled()) {
				Message message = fjfd90005.createMessage(new String[] { String.valueOf(count)});
				logger.debug(message);
			}
			return count;

		} catch (SQLException se) {
			Message message = fjfe00005.createMessage(new String[] { se
					.getMessage() });

			logger.error(message, se);
			throw new SystemException(message, se);

		} finally {
			closeConnection(con);
			con = null;
		}
	}

	/**
	 * Method to create SqlParameterList from sqlParameterListFactory object.
	 * 
	 * @return SqlParameterList
	 */
	public SqlParameterList createSqlParameterList() {
		return sqlParameterListFactory.createSqlParameterList();
	}

	/**
	 * Method to close the database connection.
	 * 
	 * @param con
	 *            Database Connection
	 * @throws SystemException
	 *             if any SQLException ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 */
	private void closeConnection(final Connection con) {

		if (con != null) {
			try {
				con.close();
			} catch (SQLException se) {
				Message message = fjfe00005.createMessage(new String[] { se
						.getMessage() });

				logger.error(message, se);
				throw new SystemException(message, se);
			}
		}
	}

	/**
	 * To get the NXGLogger instance for logging messages.
	 * 
	 * @return NXGLogger instance.
	 */
	protected final NXGLogger getLogger() {
		return logger;
	}

	/**
	 * getter method for DBConnectionManager.
	 * 
	 * @return DBConnectionManager
	 */
	public DBConnectionManager getDbConnectionManager() {
		return dbConnectionManager;
	}

	/**
	 * setter method for DBConnectionManager.
	 * 
	 * @param dbConnectionManager
	 *            DBConnectionManager
	 * @throws SystemException
	 *             if dbConnectionManager is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setDbConnectionManager(
			final DBConnectionManager dbConnectionManager) {

		if (dbConnectionManager == null) {
			String[] params = new String[] { DBConnectionManager.class
					.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		this.dbConnectionManager = dbConnectionManager;
	}

	/**
	 * getter method for ResultSetHandler.
	 * 
	 * @return resultSetHandler ResultSetHandler
	 */
	public ResultSetHandler getResultSetHandler() {
		return resultSetHandler;
	}

	/**
	 * setter method for ResultSetHandler.
	 * 
	 * @param resultSetHandler
	 *            Result Set Handler
	 * @throws SystemException
	 *             if resultSetHandler is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */

	public void setResultSetHandler(final ResultSetHandler resultSetHandler) {

		if (resultSetHandler == null) {
			String[] params = new String[] { ResultSetHandler.class.getName() };
			Message message = fjff40001.createMessage(params);
			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.resultSetHandler = resultSetHandler;
	}

	/**
	 * getter method for sqlParameterListFactory.
	 * 
	 * @return sqlParameterListFactory SqlParameterListFactory
	 */
	public SqlParameterListFactory getSqlParameterListFactory() {
		return sqlParameterListFactory;
	}

	/**
	 * setter method for SqlParameterListFactory.
	 * 
	 * @param sqlParameterListFactory
	 *            SQL parameter list factory
	 * @throws SystemException
	 *             if sqlParameterListFactory is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setSqlParameterListFactory(
			final SqlParameterListFactory sqlParameterListFactory) {

		if (sqlParameterListFactory == null) {
			String[] params = new String[] { SqlParameterListFactory.class
					.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.sqlParameterListFactory = sqlParameterListFactory;
	}
}
