/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/05/29 <Change NO.0001> created by Alex (SRM) 
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00611;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00612;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * A subclass of <code>DbUtilDataAccessStrategy</code> for Oracle.
 * 
 * Method for accessing high volume data from DB is implemented in this class.
 * 
 * Using single SQL for fetching records and total record count.
 * 
 */
class DbUtilDataAccessStrategyForOracle extends DbUtilDataAccessStrategy {

	/** High volume query contains Select Clause. */
	private static final String highVolumeQueryPrefixSelectClause = "SELECT original_2.*, "
			+ " ( SELECT COUNT(rownum) FROM ( ";

	/** High volume query contains From Clause. */
	private static final String highVolumeQueryPrefixFromClause = " ) ) RECORDCOUNT FROM ( "
			+ "  SELECT original_1.*, ROWNUM RNO FROM ( ";

	/** High volume query contains suffix. */
	private static final String highVolumeQuerySuffix = "   ) original_1 WHERE ROWNUM < ? "
			+ " ) original_2 WHERE RNO >= ? ";

	/** High volume query Select Clause for Top-N records. */
	private static final String highVolumeTopNQueryPrefixSelectClause = " SELECT original.*, ( SELECT COUNT(rownum) FROM ( ";

	/** High volume query From Clause for Top-N records. */
	private static final String highVolumeTopNQueryPrefixFromClause = " ) ) RECORDCOUNT FROM ( ";

	/** High volume query suffix for Top-N records. */
	private static final String highVolumeTopNQuerySuffix = " ) original WHERE ROWNUM < ? ";

	/**
	 * Method to fetch high volume data from database.
	 * 
	 * @param queryLanguage
	 *            SQL query
	 * @param paramList
	 *            List contains parameters for SQL query.
	 * @param startRow
	 *            Start row to fetch
	 * @param noOfRows
	 *            Number of records to be retrieved from database.
	 * 
	 * @return HighVolumeDataResult contains list of SqlReturnMap and total
	 *         record count.
	 * 
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If queryLanguage is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If Start row is less than 1 ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00611 fjfe00611})
	 *             <li>If Number of records to be fetched is less than 1 ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00612 fjfe00612})
	 *             <li>If CloneNotSupportedException is thrown ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00610 fjfe00610})
	 *             <li>If SQLException occurs ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             </ul>
	 */
	public HighVolumeDataResult selectHighVolumeData(
			final String queryLanguage, final SqlParameterList paramList,
			final int startRow, final int noOfRows) {
		// Validating parameters
		validateParams(queryLanguage, startRow, noOfRows);

		String highVolumeQuery = prepareHighVolumeQuery(queryLanguage, startRow);

		SqlParameterList highVolumeParamList = addHighVolumeParamsToList(
				paramList, startRow, noOfRows);

		List<SqlReturnMap> list = super.select(highVolumeQuery,
				highVolumeParamList);

		return getHighVolumeDataResult(list, startRow, noOfRows);

	}

	/**
	 * Construct high volume data query from actual query.
	 * 
	 * @param queryLanguage
	 *            SQL query
	 * @param startRow
	 *            Starting row number
	 * @return Query for High volume data.
	 */
	private String prepareHighVolumeQuery(final String queryLanguage,
			final int startRow) {

		StringBuilder highVolumeQuery = new StringBuilder();
		// Query for Top-N record retrieval.
		if (startRow == 1) {

			highVolumeQuery.append(highVolumeTopNQueryPrefixSelectClause)
					.append(queryLanguage).append(
							highVolumeTopNQueryPrefixFromClause).append(
							queryLanguage).append(highVolumeTopNQuerySuffix);
		} else {

			highVolumeQuery.append(highVolumeQueryPrefixSelectClause).append(
					queryLanguage).append(highVolumeQueryPrefixFromClause)
					.append(queryLanguage).append(highVolumeQuerySuffix);
		}

		return highVolumeQuery.toString();
	}

	/**
	 * Add start row to fetch and maximum row to fetch into SqlParameterList. If
	 * start row to fetch is first row then maximum row to fetch only added into
	 * SqlParameterList. If input SqlParameterList is null it creates new
	 * SqlParameterList Otherwise it gets the cloned SqlParameterList from
	 * inputed SqlParameterList.
	 * 
	 * @param paramList
	 *            List contains parameters for SQL query.
	 * @param startRow
	 *            Start row to fetch
	 * @param noOfRows
	 *            Number of records to be retrieved from database.
	 * 
	 * @return Cloned List contains parameters for select query and high volume
	 *         parameters.
	 * 
	 * @throws SystemException
	 *             If CloneNotSupportedException is thrown ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00610 fjfe00610})
	 */
	private SqlParameterList addHighVolumeParamsToList(
			final SqlParameterList paramList, final int startRow,
			final int noOfRows) {
		SqlParameterList highVolumeParamList;

		if (paramList == null) {
			highVolumeParamList = new SqlParameterListForOracle();
		} else {
			highVolumeParamList = paramList.clone();
			Object[] paramArray = highVolumeParamList.toArray();
			
			if (paramArray != null) {
				// Targetted query is used twice in HighVolume Query,
				// So we need to add the parameters of Targetted query once again.
				((SqlParameterListForOracle) highVolumeParamList).setList(Arrays.asList(paramArray));
			}
		}

		long maxRow = startRow + noOfRows;

		highVolumeParamList.setNumber(maxRow);

		if (startRow != 1) {
			highVolumeParamList.setNumber(startRow);
		}

		return highVolumeParamList;
	}

	/**
	 * Removes the RNO and RECORDCOUNT values from SqlReturnMap.
	 * 
	 * Creates HighVolumeDataResult instance and sets list of SqlReturnMap and
	 * total record count.
	 * 
	 * @param list
	 *            List contains SqlReturnMap
	 * @param startRow
	 *            Start row to fetch
	 * @param noOfRows
	 *            Number of records to be retrieved from database.
	 * 
	 * @return HighVolumeDataResult contains list of SqlReturnMap and total
	 *         record count.
	 */
	private HighVolumeDataResult getHighVolumeDataResult(
			final List<SqlReturnMap> list, final int startRow,
			final int noOfRows) {

		HighVolumeDataResult resultVo = new HighVolumeDataResult();
		resultVo.setStartRow(startRow);
		resultVo.setNoOfRows(noOfRows);
		resultVo.setSqlReturnMapList(new ArrayList<SqlReturnMap>(0)); // empty
		resultVo.setTotalRecordCount(0);

		if (list == null || list.size() == 0) {
			return resultVo;
		}

		BigDecimal recordCount = list.get(0).getNumber("RECORDCOUNT");

		if (recordCount == null) {
			return resultVo;
		}

		resultVo.setTotalRecordCount(recordCount.intValue());

		// Removes RNO and RECORDCOUNT values from SqlReturnMap
		for (SqlReturnMap map : list) {
			Set keySet = map.keySet();
			keySet.remove("RNO");
			keySet.remove("RECORDCOUNT");
		}

		resultVo.setSqlReturnMapList(list);

		return resultVo;
	}

	/**
	 * Validate the parameters.
	 * 
	 * @param queryLanguage
	 *            SQL query
	 * @param startRow
	 *            Start row to fetch
	 * @param noOfRows
	 *            Number of records to be retrieved.
	 * 
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If queryLanguage is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If Start row is less than 1 ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00611 fjfe00611})
	 *             <li>If Number of records to be fetched is less than 1 ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00612 fjfe00612})
	 *             </ul>
	 */
	private void validateParams(final String queryLanguage, final int startRow,
			final int noOfRows) {

		if (queryLanguage == null || queryLanguage.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(),
					queryLanguage };
			Message message = fjfe00004.createMessage(params);

			super.getLogger().error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (startRow < 1) {
			String[] params = new String[] { String.valueOf(startRow) };
			Message message = fjfe00611.createMessage(params);

			super.getLogger().error(message, new IllegalArgumentException());
			throw new SystemException(message, new IllegalArgumentException());
		}

		if (noOfRows < 1) {
			String[] params = new String[] { String.valueOf(noOfRows) };
			Message message = fjfe00612.createMessage(params);

			super.getLogger().error(message, new IllegalArgumentException());
			throw new SystemException(message, new IllegalArgumentException());
		}
	}

}
