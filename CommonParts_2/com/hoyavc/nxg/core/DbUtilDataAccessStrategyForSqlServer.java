/*
 * Copyright(C)2007-2011 Hoya corporation All rights reserved.
 *
 *  Change Tracking                                         
 *  2011/01/31 <Change NO.0001> created by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * A subclass of <code>DbUtilDataAccessStrategy</code> for SqlServer.
 * 
 * Method for accessing high volume data from DB is implemented in this class.
 * 
 * Using single SQL for fetching records and total record count.
 * 
 */
class DbUtilDataAccessStrategyForSqlServer extends DbUtilDataAccessStrategy {

//	/** High volume query contains Select Clause. */
//	private static final String highVolumeQueryPrefixTop = "SELECT TOP ";
//	
//	/** High volume query contains Select Clause. */
//	private static final String highVolumeQueryPrefixSelectClause = " original.*," + "  (SELECT COUNT(*) FROM( ";
//
//	/** High volume query contains From Clause. */
//	private static final String highVolumeQueryPrefixFromClause = " ) as countquery ) as recordcount " + "  FROM( ";
//
//	/** High volume query From Clause for Top-N records. */
//	private static final String highVolumeQuerySuffix = " ) as original";

	/** High volume query suffix for Top-N records. */
	//private static final String highVolumeQuerySuffixFromStart = " ) as original";

	/**
	 * Method to fetch high volume data from database.
	 * 
	 * @param queryLanguage
	 *            SQL query
	 * @param paramList
	 *            List contains parameters for SQL query.
	 * @param startRow
	 *            Start row to fetch
	 * @param noOfRows
	 *            Number of records to be retrieved from database.
	 * 
	 * @return HighVolumeDataResult contains list of SqlReturnMap and total
	 *         record count.
	 * 
	 * @throws SystemException (HighVolume data is not supported in SqlServer.)
	 */
	public HighVolumeDataResult selectHighVolumeData(final String queryLanguage, final SqlParameterList paramList,
			final int startRow, final int noOfRows) {
		// Validating parameters
//		validateParams(queryLanguage, startRow, noOfRows);
//
//		String highVolumeQuery = prepareHighVolumeQuery(queryLanguage, startRow, noOfRows);
//
//		SqlParameterList highVolumeParamList = addHighVolumeParamsToList(paramList, startRow, noOfRows);
//
//		List<SqlReturnMap> list = super.select(highVolumeQuery, highVolumeParamList);
		
//		return getHighVolumeDataResult(list, startRow, noOfRows);
//		
//		List<SqlReturnMap> list = super.select(queryLanguage, paramList);

		String[] params = new String[] { "HighVolume", "SqlServer" };
		Message message = fjfe00614.createMessage(params);
		throw new SystemException(message);

	}

	/**
	 * Construct high volume data query from actual query.
	 * 
	 * @param queryLanguage
	 *            SQL query
	 * @param startRow
	 *            Starting row number
	 * @return Query for High volume data.
	 */
//	private String prepareHighVolumeQuery(final String queryLanguage, final int startRow, final int noOfRows) {
//
//		StringBuilder highVolumeQuery = new StringBuilder();
//
//		highVolumeQuery.append(highVolumeQueryPrefixTop);
//		if (startRow == 1) {
//			highVolumeQuery.append(noOfRows);
//		}
//		highVolumeQuery.append(highVolumeQueryPrefixSelectClause);
//		highVolumeQuery.append(queryLanguage);
//		highVolumeQuery.append(highVolumeQueryPrefixFromClause);
//		highVolumeQuery.append(queryLanguage);
//		highVolumeQuery.append(highVolumeQuerySuffix);
//
//		// Query for Top-N record retrieval.
////		if (startRow == 1) {
////			highVolumeQuery.append(highVolumeQuerySuffix);
////		} 
////		else {
////			highVolumeQuery.append(highVolumeQuerySuffixFromStart);
////		}
//
//		return highVolumeQuery.toString();
//	}

	/**
	 * Add start row to fetch and maximum row to fetch into SqlParameterList. If
	 * start row to fetch is first row then maximum row to fetch only added into
	 * SqlParameterList. If input SqlParameterList is null it creates new
	 * SqlParameterList Otherwise it gets the cloned SqlParameterList from
	 * inputed SqlParameterList.
	 * 
	 * @param paramList
	 *            List contains parameters for SQL query.
	 * @param startRow
	 *            Start row to fetch
	 * @param noOfRows
	 *            Number of records to be retrieved from database.
	 * 
	 * @return Cloned List contains parameters for select query and high volume
	 *         parameters.
	 * 
	 * @throws SystemException
	 *             If CloneNotSupportedException is thrown (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00610 fjfe00610})
	 */
//	private SqlParameterList addHighVolumeParamsToList(final SqlParameterList paramList, final int startRow,
//			final int noOfRows) {
//		SqlParameterList highVolumeParamList;
//
//		if (paramList == null) {
//			highVolumeParamList = new SqlParameterListForSqlServer();
//		} else {
//			highVolumeParamList = paramList.clone();
//			Object[] paramArray = highVolumeParamList.toArray();
//
//			if (paramArray != null) {
//				// Targeted query is used twice in HighVolume Query,
//				// So we need to add the parameters of Targeted query once
//				// again.
//				((SqlParameterListForSqlServer) highVolumeParamList).setList(Arrays.asList(paramArray));
//			}
//		}
//
////		if (startRow != 1) {
////			highVolumeParamList.setNumber(startRow - 1);
////		}
//		//highVolumeParamList.setNumber(noOfRows);
//
//		return highVolumeParamList;
//	}

	/**
	 * Removes the RNO and RECORDCOUNT values from SqlReturnMap.
	 * 
	 * Creates HighVolumeDataResult instance and sets list of SqlReturnMap and
	 * total record count.
	 * 
	 * @param list
	 *            List contains SqlReturnMap
	 * @param startRow
	 *            Start row to fetch
	 * @param noOfRows
	 *            Number of records to be retrieved from database.
	 * 
	 * @return HighVolumeDataResult contains list of SqlReturnMap and total
	 *         record count.
	 */
//	private HighVolumeDataResult getHighVolumeDataResult(final List<SqlReturnMap> list, final int startRow,
//			final int noOfRows) {
//
//		HighVolumeDataResult resultVo = new HighVolumeDataResult();
//		resultVo.setStartRow(startRow);
//		resultVo.setNoOfRows(noOfRows);
//		resultVo.setSqlReturnMapList(new ArrayList<SqlReturnMap>(0)); // empty
//		resultVo.setTotalRecordCount(0);
//
//		if (list == null || list.size() == 0) {
//			return resultVo;
//		}
//
//		BigDecimal recordCount = list.get(0).getNumber("RECORDCOUNT");
//
//		if (recordCount == null) {
//			return resultVo;
//		}
//
//		resultVo.setTotalRecordCount(recordCount.intValue());
//
//		// Removes RECORDCOUNT values from SqlReturnMap
//		for (SqlReturnMap map : list) {
//			Set keySet = map.keySet();
//			keySet.remove("RECORDCOUNT");
//		}
//
//		resultVo.setSqlReturnMapList(list);
//
//		return resultVo;
//	}

	/**
	 * Validate the parameters.
	 * 
	 * @param queryLanguage
	 *            SQL query
	 * @param startRow
	 *            Start row to fetch
	 * @param noOfRows
	 *            Number of records to be retrieved.
	 * 
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If queryLanguage is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If Start row is less than 1 (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00611 fjfe00611})
	 *             <li>If Number of records to be fetched is less than 1 (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00612 fjfe00612})
	 *             </ul>
	 */
//	private void validateParams(final String queryLanguage, final int startRow, final int noOfRows) {
//
//		if (queryLanguage == null || queryLanguage.trim().length() <= 0) {
//			String[] params = new String[] { String.class.getName(), queryLanguage };
//			Message message = fjfe00004.createMessage(params);
//
//			super.getLogger().error(message, new NullPointerException());
//			throw new SystemException(message, new NullPointerException());
//		}
//
//		if (startRow < 1) {
//			String[] params = new String[] { String.valueOf(startRow) };
//			Message message = fjfe00611.createMessage(params);
//
//			super.getLogger().error(message, new IllegalArgumentException());
//			throw new SystemException(message, new IllegalArgumentException());
//		}
//
//		if (noOfRows < 1) {
//			String[] params = new String[] { String.valueOf(noOfRows) };
//			Message message = fjfe00612.createMessage(params);
//
//			super.getLogger().error(message, new IllegalArgumentException());
//			throw new SystemException(message, new IllegalArgumentException());
//		}
//	}

}
