/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.*;

/**
 * Class to invoke the CBS handler thru sessionBeanController.
 * 
 * @see SessionBeanController
 * 
 */
class DefaultCbsInvoker implements CbsInvoker {

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());
	private final NXGLogger logger_local =  DefaultLogger.getInstance(DefaultCbsInvoker.class);

	/** Session bean controller. */
	private SessionBeanController sessionBeanController;

	/**
	 * getter method for SessionBeanController.
	 * 
	 * @return sessionBeanController
	 */
	public SessionBeanController getSessionBeanController() {
		return sessionBeanController;
	}

	/**
	 * setter method for SessionBeanController.
	 * 
	 * @param sessionBeanController
	 *            Session Bean Controller
	 * @throws SystemException
	 *             if sessionBeanController is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setSessionBeanController(
			final SessionBeanController sessionBeanController) {

		if (sessionBeanController == null) {
			String[] params = new String[] { SessionBeanController.class
					.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.sessionBeanController = sessionBeanController;
	}

	/**
	 * Method to invoke CBS handler thru sessionBeanController.
	 * 
	 * @param cbsHandlerId
	 *            CBS Handler Id
	 * @param trxContext
	 *            TransactionContext
	 * @param vo
	 *            ValueObject
	 * @return ValueObject
	 * @throws ApplicationException
	 *             Application Exception
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If cbsHandlerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             <li>If ReturnValueObject is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00007 fjfe00007})
	 *             </ul>
	 */
	public ValueObject invokeCbs(final String cbsHandlerId, final TransactionContext trxContext, final ValueObject vo)
			throws ApplicationException {
		try {

			// validating parameter
			validateParams(cbsHandlerId, trxContext, vo);

			ReturnValueObject rvo = (ReturnValueObject) sessionBeanController.invoke(cbsHandlerId, trxContext, vo);

			if (rvo == null) {
				Message message = fjfe00007.createMessage();
				logger.error(trxContext, message, new NullPointerException());
				throw new SystemException(message, new NullPointerException());
			}

			if (rvo.getWarningList() != null) {
				trxContext.addWarningList(rvo.getWarningList());
			}

			return rvo.getVo();
		} catch (SystemException e) {
			if (logger_local.isDebugEnabled()) {
				Message message = fjfe00008.createMessage();
				logger_local.debug(message, e);
			}
			throw e;
		}

	}

	/**
	 * Method to validate parameters.
	 * 
	 * @param cbsHandlerId
	 *            CBS Handler bean ID
	 * @param trxContext
	 *            TransactionContext
	 * @param vo
	 *            ValueObject
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 *             <li>If cbsHandlerId is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If vo is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00002 fjfe00002})
	 *             </ul>
	 */
	private void validateParams(final String cbsHandlerId,
			final TransactionContext trxContext, final ValueObject vo) {

		if (trxContext == null) {
			Message message = fjfe00001.createMessage();
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (cbsHandlerId == null || cbsHandlerId.trim().length() <= 0) {
			Message message = fjfe00004.createMessage(new String[] {
					String.class.getName(), cbsHandlerId });
			logger.error(trxContext, message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (vo == null) {
			Message message = fjfe00002.createMessage();
			logger.error(trxContext, message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
	}

}
