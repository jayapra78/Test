/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2009/07/31 <Change NO.0001> created by T.Saito (VCIT) 
 *  2010/10/15 <Change NO.0002> Changed to get DIV_MARK automatically by T.Saito (VCIT)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40301;
//import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40302;
import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40303;
import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40304;
import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40305;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.hoyavc.nxg.core.EnvironmentVariables;
import com.hoyavc.nxg.core.InitializationErrorLogger;
import com.hoyavc.nxg.core.Message;
import com.hoyavc.nxg.core.NXGLogger;
import com.hoyavc.nxg.core.SystemException;

/**
 * DefaultEnvironmentVariables is class to get configuration xml file
 * with path declared as defaultClientEnvironmentVariables variables.
 */
public class DefaultEnvironmentVariables implements EnvironmentVariables {

	/** NXG_HOME path. */
	private String NXG_HOME;

	/** Conf file folder under NXG_HOME. */
	private String CONF_FOLDER;

	/** Conf file under CONF_FOLDER. */
	private String CONF_FILE;

	/** Path dividing mark. */
	private final String DIV_MARK = System.getProperty("file.separator");

	/** Root tag name. */
	private String ROOT_TAG;

	/** List tag name. */
	private String LIST_TAG;

	/** Value tag name. */
	private String VALUE_TAG;

	/** Confifuration values map. */
	private Map<String, String> confStringMap = new HashMap<String, String>();
	private Map<String, List<String>> confListMap = new HashMap<String, List<String>>();

	/** NXGLogger for logging messages. */
	private final NXGLogger initializationErrorLogger = InitializationErrorLogger
			.getInstance(this.getClass());

	/**
	 * Method to get environment variable of String.
	 * 
	 * @param key
	 *            environment variable name
	 * @return environment variable
	 */
	public String getStringValue(final String key) {
		return confStringMap.get(key);
	}
	
	/**
	 * Method to get environment variable of List.
	 * 
	 * @param key
	 *            environment variable name
	 * @return environment variable
	 */
	public List<String> getListValue(final String key) {
		return confListMap.get(key);
	}

	/**
	 * getter method for CONF_FOLDER.
	 * 
	 * @return the CONF_FOLDER
	 */
	public String getCONF_FOLDER() {
		return CONF_FOLDER;
	}

	/**
	 * setter method for CONF_FOLDER.
	 * 
	 * @param CONF_FOLDER the CONF_FOLDER to set
	 */
	public void setCONF_FOLDER(final String CONF_FOLDER) {
		this.CONF_FOLDER = CONF_FOLDER;
	}

	/**
	 * getter method for CONF_FILE.
	 * 
	 * @return the CONF_FILE
	 */
	public String getCONF_FILE() {
		return CONF_FILE;
	}

	/**
	 * setter method for CONF_FILE.
	 * 
	 * @param CONF_FILE the CONF_FILE to set
	 */
	public void setCONF_FILE(final String CONF_FILE) {
		this.CONF_FILE = CONF_FILE;
	}

	/**
	 * getter method for ROOT_TAG.
	 * 
	 * @return the ROOT_TAG
	 */
	public String getROOT_TAG() {
		return ROOT_TAG;
	}

	/**
	 * setter method for ROOT_TAG.
	 * 
	 * @param ROOT_TAG the ROOT_TAG to set
	 */
	public void setROOT_TAG(final String ROOT_TAG) {
		this.ROOT_TAG = ROOT_TAG;
	}

	/**
	 * getter method for LIST_TAG.
	 * 
	 * @return the LIST_TAG
	 */
	public String getLIST_TAG() {
		return LIST_TAG;
	}

	/**
	 * setter method for LIST_TAG.
	 * 
	 * @param LIST_TAG the LIST_TAG to set
	 */
	public void setLIST_TAG(final String LIST_TAG) {
		this.LIST_TAG = LIST_TAG;
	}

	/**
	 * getter method for STRING_TAG.
	 * 
	 * @return the STRING_TAG
	 */
	public String getVALUE_TAG() {
		return VALUE_TAG;
	}

	/**
	 * setter method for STRING_TAG.
	 * 
	 * @param STRING_TAG the STRING_TAG to set
	 */
	public void setVALUE_TAG(final String STRING_TAG) {
		this.VALUE_TAG = STRING_TAG;
	}

	/**
	 * Method of Initialization.
	 * 
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If Innstanciating DocumentBuilder causes error ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40301 fjff40301})
	 *             <li>If Full file name for configurating envronment variables is not found ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40302 fjff40302})
	 *             <li>If SAXParseException has occurred in configurating envronment variables ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40303 fjff40303})
	 *             <li>If NXG EJB client environment variables initialization error  ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40304 fjff40304})
	 *             <li>If one of variables is null or empty ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40305 fjff40305})
	 *             </ul>
	 */
	public final void init() {

		// Get conf full file name.
		String fullFileName = getFullFileName();
		if (fullFileName == null ) {
			return;
		}

		// Get conf full file name.
		NodeList genericNodeList;
		genericNodeList = getRootNode(fullFileName);
		if (genericNodeList == null ) {
			return;
		}

		// Set map with configuration files.
		setConfMap(genericNodeList, confStringMap, confListMap);

	}

	/**
	 * Get full file path and name of config xml.
	 * 
	 * @return FullFileName
	 *             full file path and name.
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If one of variables is null or empty ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40305 fjff40305})
	 *             </ul>
	 */
	private String getFullFileName() {

		// Null check for configuration file path.
		// (if one of them is null, throw SystemException.)
		NXG_HOME = System.getenv("NXG_HOME");
		if (NXG_HOME == null) {
			//InitializationError("NXG_ENV");
			return null;
		}
		if (CONF_FOLDER == null) {
			InitializationError("CONF_FOLDER");
		}
		if (CONF_FILE == null) {
			InitializationError("CONF_FILE");
		}
		if (DIV_MARK == null) {
			InitializationError("DIV_MARK");
		}

		// Set full file name.
		StringBuffer fullFileName = new StringBuffer();

		fullFileName.append(NXG_HOME);
		// DIV_MARK is not included in NXG_HOME.
		if (!(NXG_HOME.substring(NXG_HOME.length()).equals(DIV_MARK))) {
			fullFileName.append(DIV_MARK);
		}
		fullFileName.append(CONF_FOLDER);
		fullFileName.append(DIV_MARK);
		fullFileName.append(CONF_FILE);

		// Return full file name.
		return fullFileName.toString();
		
	}
	
	/**
	 * Set configuration values into map.
	 * 
	 * @return NodeList
	 *             Nodelist gotton from environment configuration xml file.
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If Innstanciating DocumentBuilder causes error ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40301 fjff40301})
	 *             <li>If Full file name for configurating envronment variables is not found ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40302 fjff40302})
	 *             <li>If SAXParseException has occurred in configurating envronment variables ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40303 fjff40303})
	 *             </ul>
	 */
	private NodeList getRootNode(final String fullFileName) {

		// Create new DocumentBuilderFactory.
		DocumentBuilderFactory dbfactory = DocumentBuilderFactory
				.newInstance();
		dbfactory.setIgnoringElementContentWhitespace(true);
		dbfactory.setIgnoringComments(true);
		dbfactory.setValidating(false);

		// Create new DocumentBuilder.
		DocumentBuilder builder;
		try {
			builder = dbfactory.newDocumentBuilder();

		} catch (ParserConfigurationException e) {
			Message message = fjff40301.createMessage();
			initializationErrorLogger.fatal(message, e);
			throw new SystemException(message, e);

		}

		// Get document by full file name.
		Document doc;
		try {
			doc = builder.parse(new File(fullFileName));

		} catch (IOException e) {
			//String[] params = new String[] {fullFileName};
			//Message message = fjff40302.createMessage(params);
			//initializationErrorLogger.fatal(message, e);
			//throw new SystemException(message, e);
			return null;

		} catch (SAXException e) {
			String[] params = new String[] {fullFileName};
			Message message = fjff40303.createMessage(params);
			initializationErrorLogger.fatal(message, e);
			throw new SystemException(null);

		}

		// Get root tag.
		return doc.getDocumentElement().getChildNodes();

	}

	/**
	 * Get full file path and name of config xml.
	 * And set it to confStringMap and confListMap.
	 * 
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If NXG EJB client environment variables initialization error  ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40304 fjff40304})
	 *             </ul>
	 */
	private void setConfMap(final NodeList childNodeList,
			Map<String, String> stringMap, Map<String, List<String>> listMap) {

		// Get child NodeList.
		for(int i = 0; i < childNodeList.getLength(); i++){
			Node child = childNodeList.item(i);

			// Ingnore spaces and return code.
			if(isInvalidNode(child)){
			    continue;
			}

			String childKeyName = child.getNodeName();

			NodeList grandChildNodeList = child.getChildNodes();
			for (int j = 0; j < grandChildNodeList.getLength(); j++) {
				Node grandChild = grandChildNodeList.item(j);

				// Ingnore spaces and return code.
				if(isInvalidNode(grandChild)){
				    continue;
				}

				// Get node name which is set as map's key name.
				String grandChildKeyName = grandChild.getNodeName();

				// In case of List, get child node and set as List.
				if (grandChildKeyName.equals(LIST_TAG)) {
					NodeList valueNodeList = grandChild.getChildNodes();
					List<String> valueList = new ArrayList<String>();

					// Get child nodes and add to List.
					for (int k = 0; k < valueNodeList.getLength(); k++) {
						Node value = valueNodeList.item(k);

						// Ingnore spaces and return code.
						if (isInvalidNode(value)) {
							continue;
						}

						// Get actual node values of List
						String valueNodeValue = value.getFirstChild().getNodeValue();
						valueList.add(valueNodeValue);
					}
					listMap.put(childKeyName, valueList);

				// In case of String, set as String.
				} else if (grandChildKeyName.equals(VALUE_TAG)) {
					// Get actual node values of String
					String grandChildKeyValue = grandChild.getFirstChild().getNodeValue();
					stringMap.put(childKeyName, grandChildKeyValue);

				// The other cases, throw SystemException.
				} else {
					String[] params = new String[] { grandChildKeyName };
					Message message = fjff40304.createMessage(params);
					initializationErrorLogger.fatal(message, new NullPointerException());
					throw new SystemException(message, new NullPointerException());

				}
			}
		}
	}

	/**
	 * Check whether Node is invalid or not.
	 * Getting XML files includes spaces and return code, so it must be removed.
	 * 
	 * @return boolean
	 *             Whether node is invalid or not.
	 */
	private boolean isInvalidNode(Node node) {
		
		if(node.getNodeType()==Node.TEXT_NODE && node.getNodeValue().trim().length()==0){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Throws SystemException in intialization.
	 * 
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>If one of variables is null or empty ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40305 fjff40305})
	 *             </ul>
	 */
	private void InitializationError(final String nullValue) {

		String[] params = new String[] { nullValue };
		Message message = fjff40305.createMessage(params);
		initializationErrorLogger.fatal(message, new NullPointerException());
		throw new SystemException(message, new NullPointerException());
		
	}
}
