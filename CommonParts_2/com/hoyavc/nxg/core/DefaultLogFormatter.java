/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * In NXG framework, DefaultLogFormatter is used to format the log message.
 * 
 * <pre>
 *           Default Log Format:
 *             [MessageCode DateTime CorporationCode UserId IpAddress Message]
 *                 
 *           example :
 *             ffjf40002 20070227-135235949 corp. user 192.23.34.89 TransactionContext is null
 * </pre>
 * 
 * Default values for MessageCode, DateTime, CorporationCode, UserId, IpAddress
 * and Message are assigned at the bean initialization time.
 * 
 */
public class DefaultLogFormatter implements LogFormatter {

	/** For adding space in the message. */
	private String SPACE;

	/** For blank message code. */
	private String BLANK_MESSAGECODE;

	/** blank processing GMT time. */
	private String BLANK_PROCESSING_GMT_TIME;

	/** blank corporation code. */
	private String BLANK_CORPORATION_CODE;
	
	/** blank client version. */
	private String BLANK_CLIENT_VERSION="";

	/** blank user id. */
	private String BLANK_USER_ID;

	/** blank IP address. */
	private String BLANK_IP_ADDRESS;

	/** blank message. */
	private String BLANK_MESSAGE;

	/**
	 * To format the logging informations into String.
	 * 
	 * This method will format the given TransactionContext and messageObject
	 * into string as below.
	 * 
	 * <pre>
	 *    Default Log Format: 
	 *        [MessageCode DateTime CorporationCode UserId IpAddress Message]
	 *    
	 *    example : 
	 *        ffjf40002 20070227-135235949 corp. user 192.23.34.89 TransactionContext is null
	 * </pre>
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @return Log Format
	 */
	public String formatLog(final TransactionContext trxContext,
			final Message messageObject) {

		StringBuilder log = new StringBuilder();

		// temporary 1.messageCode
		log.append(editMessageCode(messageObject));
		log.append(SPACE);

		// 2.processingGMTTime
		if (trxContext != null && trxContext.getProcessingGMTTimeParent() != null) {
			log.append("[");
			log.append(editProcessingGMTTimeParent(trxContext));
			log.append("]");
			log.append(SPACE);
		}
		log.append(editProcessingGMTTime(trxContext));
		log.append(SPACE);

		final UserData userData;
		if (trxContext != null) {
			userData = trxContext.getUserData();
		} else {
			userData = null;
		}

		// 3.corporationCode
		log.append(editCorporationCode(userData));
		log.append(SPACE);
		
		// 4. Client version
		log.append(editClientVersion(userData));
		log.append(SPACE);

		// 5.userId
		log.append(editUserId(userData));
		log.append(SPACE);

		// 6.IpAddress
		log.append(editIpAddress(userData));
		log.append(SPACE);

		// 7.message
		log.append(editMessage(messageObject));

		return log.toString();

	}
	
	/**
	 * Editing ClientVersion. If userData or getClientVersion() is
	 * null then BLANK_CLIENT_VERSION will be assigned.
	 * 
	 * @param userData
	 *            UserData
	 * @return String clientVersion
	 */
	private String editClientVersion(final UserData userData) {
		if (userData == null) {
			return BLANK_CLIENT_VERSION;
		}
		String clientVersion = userData.getClientVersion();

		if (clientVersion == null) {
			return BLANK_CLIENT_VERSION;
		} else {
			return clientVersion;
		}

	}

	/**
	 * Editing message code. If messageCode is null then BLANK_MESSAGECODE will
	 * be assigned.
	 * 
	 * @param messageObject
	 *            Message
	 * @return String message code
	 */
	private String editMessageCode(final Message messageObject) {
		if (messageObject == null) {
			return BLANK_MESSAGECODE;
		}

		String messageCode = messageObject.getMessageCode();
		if (messageCode == null) {
			return BLANK_MESSAGECODE;
		} else {
			return messageCode;
		}

	}

	/**
	 * Editing ProcessingGMTTime. If trxContext or
	 * trxContext.getProcessingGMTTime() is null then BLANK_PROCESSING_GMT_TIME
	 * will be assigned.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @return String processingGMTTime
	 */
	private String editProcessingGMTTime(final TransactionContext trxContext) {
		if (trxContext == null) {
			return BLANK_PROCESSING_GMT_TIME;
		}

		String processingGMTTime = trxContext.getProcessingGMTTime();
		if (processingGMTTime == null || processingGMTTime.length() != 17) {
			return BLANK_PROCESSING_GMT_TIME;
		}

		return processingGMTTime.substring(0, 8) + "-"
				+ processingGMTTime.substring(8);
	}
	
	/**
	 * Editing ProcessingGMTTimeParent. If trxContext or
	 * trxContext.getProcessingGMTTimeParent() is null then ""
	 * will be assigned.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @return String processingGMTTimeParent
	 */
	private String editProcessingGMTTimeParent(final TransactionContext trxContext) {
		String processingGMTTime = trxContext.getProcessingGMTTimeParent();
		if (processingGMTTime == null || processingGMTTime.length() != 17) {
			return "";
		}

		return processingGMTTime.substring(0, 8) + "-"
				+ processingGMTTime.substring(8);
	}
	

	/**
	 * Editing corporationCode. If userData or userData.getCorporationCode() is
	 * null then BLANK_CORPORATION_CODE will be assigned.
	 * 
	 * @param userData
	 *            UserData
	 * @return String corporationCode
	 */
	private String editCorporationCode(final UserData userData) {
		if (userData == null) {
			return BLANK_CORPORATION_CODE;
		}
		String corporationCode = userData.getCorporationCode();

		if (corporationCode == null) {
			return BLANK_CORPORATION_CODE;
		} else {
			return corporationCode;
		}

	}

	/**
	 * Editing userId. If userData or userData.getUserId() is null then
	 * BLANK_USER_ID will be assigned.
	 * 
	 * @param userData
	 *            UserData
	 * @return String userId
	 */
	private String editUserId(final UserData userData) {
		if (userData == null) {
			return BLANK_USER_ID;
		}
		String userId = userData.getUserId();

		if (userId == null) {
			return BLANK_USER_ID;
		} else {
			return userId;
		}

	}

	/**
	 * Editing ipAddress. If userData or userData.getIpAddress() is null then
	 * BLANK_IP_ADDRESS will be assigned.
	 * 
	 * @param userData
	 *            UserData
	 * @return String ipAddress
	 */
	private String editIpAddress(final UserData userData) {
		if (userData == null) {
			return BLANK_IP_ADDRESS;
		}
		String ipAddress = userData.getIpAddress();

		if (ipAddress == null) {
			return BLANK_IP_ADDRESS;
		} else {
			return ipAddress;
		}

	}

	/**
	 * Editing message. If messageOject or messageOject.getMessage() is null
	 * then BLANK_MESSAGE will be assigned.
	 * 
	 * @param messageOject
	 *            Message
	 * @return String message
	 */
	private String editMessage(final Message messageOject) {
		if (messageOject == null) {
			// temporary
			return BLANK_MESSAGE;
		}
		String message = messageOject.getMessage();

		if (message == null) {
			return BLANK_MESSAGE;
		} else {
			return message;
		}

	}

	/**
	 * getter method for BLANK_CORPORATION_CODE.
	 * 
	 * @return the BLANK_CORPORATION_CODE
	 */
	public String getBLANK_CORPORATION_CODE() {
		return BLANK_CORPORATION_CODE;
	}

	/**
	 * setter method for BLANK_CORPORATION_CODE.
	 * 
	 * @param blank_corporation_code
	 *            the BLANK_CORPORATION_CODE to set
	 */
	public void setBLANK_CORPORATION_CODE(String blank_corporation_code) {
		BLANK_CORPORATION_CODE = blank_corporation_code;
	}

	/**
	 * getter method for BLANK_IP_ADDRESS.
	 * 
	 * @return the BLANK_IP_ADDRESS
	 */
	public String getBLANK_IP_ADDRESS() {
		return BLANK_IP_ADDRESS;
	}

	/**
	 * setter method for BLANK_IP_ADDRESS.
	 * 
	 * @param blank_ip_address
	 *            the BLANK_IP_ADDRESS to set
	 */
	public void setBLANK_IP_ADDRESS(String blank_ip_address) {
		BLANK_IP_ADDRESS = blank_ip_address;
	}

	/**
	 * getter method for BLANK_MESSAGE.
	 * 
	 * @return the BLANK_MESSAGE
	 */
	public String getBLANK_MESSAGE() {
		return BLANK_MESSAGE;
	}

	/**
	 * setter method for BLANK_MESSAGE.
	 * 
	 * @param blank_message
	 *            the BLANK_MESSAGE to set
	 */
	public void setBLANK_MESSAGE(String blank_message) {
		BLANK_MESSAGE = blank_message;
	}

	/**
	 * getter method for BLANK_MESSAGECODE.
	 * 
	 * @return the BLANK_MESSAGECODE
	 */
	public String getBLANK_MESSAGECODE() {
		return BLANK_MESSAGECODE;
	}

	/**
	 * setter method for BLANK_MESSAGECODE.
	 * 
	 * @param blank_messagecode
	 *            the BLANK_MESSAGECODE to set
	 */
	public void setBLANK_MESSAGECODE(String blank_messagecode) {
		BLANK_MESSAGECODE = blank_messagecode;
	}

	/**
	 * getter method for BLANK_PROCESSING_GMT_TIME.
	 * 
	 * @return the BLANK_PROCESSING_GMT_TIME
	 */
	public String getBLANK_PROCESSING_GMT_TIME() {
		return BLANK_PROCESSING_GMT_TIME;
	}

	/**
	 * setter method for BLANK_PROCESSING_GMT_TIME.
	 * 
	 * @param blank_processing_gmt_time
	 *            the BLANK_PROCESSING_GMT_TIME to set
	 */
	public void setBLANK_PROCESSING_GMT_TIME(String blank_processing_gmt_time) {
		BLANK_PROCESSING_GMT_TIME = blank_processing_gmt_time;
	}

	/**
	 * getter method for BLANK_USER_ID.
	 * 
	 * @return the BLANK_USER_ID
	 */
	public String getBLANK_USER_ID() {
		return BLANK_USER_ID;
	}

	/**
	 * setter method for BLANK_USER_ID.
	 * 
	 * @param blank_user_id
	 *            the BLANK_USER_ID to set
	 */
	public void setBLANK_USER_ID(String blank_user_id) {
		BLANK_USER_ID = blank_user_id;
	}

	/**
	 * getter method for SPACE.
	 * 
	 * @return the SPACE
	 */
	public String getSPACE() {
		return SPACE;
	}

	/**
	 * setter method for SPACE.
	 * 
	 * @param space
	 *            the SPACE to set
	 */
	public void setSPACE(String space) {
		SPACE = space;
	}

}
