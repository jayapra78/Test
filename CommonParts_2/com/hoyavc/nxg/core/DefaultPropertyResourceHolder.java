/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/06/29 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/06/29 <Change NO.0002> Added javadoc by Alex (SRM)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;
import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40007;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Class used to handle property file.
 * 
 */
public class DefaultPropertyResourceHolder implements InitialProcess,
		ResourceHolder {

	/** Property filename. */
	private String propertyFileName;

	/** Holds property file information. */
	private Map<String, String> propertyMap = new HashMap<String, String>();

	/**
	 * Getting value for key. If no match found for key, returns null.
	 * 
	 * @param key
	 *            Key name
	 * @return Property value
	 */
	public String getValue(final String key) {

		if (propertyMap.containsKey(key)) {
			return propertyMap.get(key);
		} else {
			return null;
		}

	}

	/**
	 * Init method is invoked by SpringFramework at initialization of beans.
	 * Load the resource bundle and copy all the values into Map.
	 * 
	 * @throws SystemException
	 *             If any one of the following cases occurs
	 *             <ul>
	 *             <li>No resource bundle is available ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40007 fjff40007})
	 *             </ul>
	 */
	public void init() {
		try {

			ResourceBundle resourceBundle = ResourceBundle
					.getBundle(propertyFileName);

			for (String key : Collections.list(resourceBundle.getKeys())) {
				propertyMap.put(key, resourceBundle.getString(key));

			}
		} catch (MissingResourceException misResExp) {
			Message message = fjff40007
					.createMessage(new String[] { propertyFileName });
			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger.error(message, misResExp);
			throw new SystemException(message, misResExp);
		}

	}

	/**
	 * Getter method for propertyFileName.
	 * 
	 * @return Property filename
	 */
	public String getPropertyFileName() {
		return propertyFileName;
	}

	/**
	 * Setter method for propertyFileName.
	 * 
	 * @param propertyFileName
	 *            Property filename.
	 * 
	 * @throws SystemException
	 *             if propertyFileName is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setPropertyFileName(String propertyFileName) {
		if (propertyFileName == null) {
			String[] params = new String[] { this.getClass().getName() };
			Message message = fjff40001.createMessage(params);
			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.propertyFileName = propertyFileName;
	}

}
