/*
* Copyright 2007 by NXG Project, All rights reserved.
*
*  Change Tracking                                         
*  2009/04/01 <Change NO.0001> created by T.Saito (VCIT) 
*/
package com.hoyavc.nxg.core;

import java.util.List;

public interface EnvironmentVariables {

	/**
	 * Method to get environment variable of String.
	 * 
	 * @param key
	 *            environment variable name
	 * @return environment variable
	 */
	String getStringValue(final String key);
	
	/**
	 * Method to get environment variable of List.
	 * 
	 * @param key
	 *            environment variable name
	 * @return environment variable
	 */
	List<String> getListValue(final String key);

}
