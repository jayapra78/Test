/*
 * Copyright 2006 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 */
package com.hoyavc.nxg.core;

import java.util.List;

/**
 * High volume data information.
 * 
 */
public class HighVolumeDataResult {

	/** Start row to fetch. */
	private int startRow;

	/** No. of rows to fetch. */
	private int noOfRows;

	/** List contains SqlReturnMap. */
	private List<SqlReturnMap> sqlReturnMapList;

	/** Total record count. */
	private int totalRecordCount;

	/**
	 * Getter method for sqlReturnMapList.
	 * 
	 * @return List contains SqlReturnMap.
	 */
	public List<SqlReturnMap> getSqlReturnMapList() {
		return sqlReturnMapList;
	}

	/**
	 * Setter method for sqlReturnMapList.
	 * 
	 * @param sqlReturnMapList
	 *            List contains SqlReturnMap.
	 */
	public void setSqlReturnMapList(List<SqlReturnMap> sqlReturnMapList) {
		this.sqlReturnMapList = sqlReturnMapList;
	}

	/**
	 * Getter method for totalRecordCount.
	 * 
	 * @return Total record count.
	 */
	public int getTotalRecordCount() {
		return totalRecordCount;
	}

	/**
	 * Setter method for totalRecordCount.
	 * 
	 * @param totalRecordCount
	 *            Total record count.
	 */
	public void setTotalRecordCount(int totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	/**
	 * Getter method for noOfRows.
	 * 
	 * @return the noOfRows
	 */
	public int getNoOfRows() {
		return noOfRows;
	}

	/**
	 * Setter method for noOfRows.
	 * 
	 * @param noOfRows
	 *            the noOfRows to set
	 */
	public void setNoOfRows(int noOfRows) {
		this.noOfRows = noOfRows;
	}

	/**
	 * Getter method for startRow.
	 * 
	 * @return the startRow
	 */
	public int getStartRow() {
		return startRow;
	}

	/**
	 * Setter method for startRow.
	 * 
	 * @param startRow
	 *            the startRow to set
	 */
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

}
