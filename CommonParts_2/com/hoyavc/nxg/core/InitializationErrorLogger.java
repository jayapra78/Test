/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import org.apache.log4j.Logger;

/**
 * In NXG framework, InitializationErrorLogger is used to log the spring bean
 * initialization error message. This class requires Log4J 1.2.8.
 * 
 * <pre>
 *      If TransactionContext is null
 *      Default Log Format:
 *        [MessageCode Message]
 *                            
 *      example :
 *        ffjf40002 Initialization Error: Can't set null to the bean bean type
 *      
 *      
 *      If TransactionContext is not null
 *      Default Log Format:
 *        [MessageCode DateTime CorporationCode UserId IpAddress Message]
 *                            
 *      example :
 *        ffjf40002 20070227-135235949 corp. user 192.23.34.89 TransactionContext is null
 *        
 * </pre>
 * 
 * <p>
 * Code example: <code>
 * 	private final NXGLogger logger = InitializationErrorLogger.getInstance(this.getClass());
 * </code>
 * </p>
 * 
 * @see InitializationErrorMessageFactory
 * @see DefaultLogger
 * @see DefaultLogFormatter
 * @see SimpleMessageFactory
 * @see MessageCode
 */
public final class InitializationErrorLogger implements NXGLogger {

	/** Logger used to log message. */
	private final Logger logger;

	/** anonymous logger in-case of null value. */
	private static final NXGLogger anonymousLogger = getInstance("anonymous");

	/**
	 * Constructor.
	 * 
	 * @param logger
	 *            Logger
	 */
	private InitializationErrorLogger(final Logger logger) {
		this.logger = logger;
		// Please set.
	}

	/**
	 * Get instance of LogParts.
	 * 
	 * @param className
	 *            Output log base class name.
	 * @return Instance of this class.
	 */
	public static NXGLogger getInstance(final String className) {

		if (className == null || className.trim().length() <= 0) {
			return anonymousLogger;
		}

		return new InitializationErrorLogger(Logger.getLogger(className));
	}

	/**
	 * Get instance of LogParts.
	 * 
	 * @param classObject
	 *            Output log base Class class.
	 * @return Instance of this class.
	 */
	public static NXGLogger getInstance(final Class classObject) {

		if (classObject == null) {
			return anonymousLogger;
		}

		return InitializationErrorLogger.getInstance(classObject.getName());
	}

	/**
	 * Method to Log debug message.
	 * 
	 * @param messageObject
	 *            Message
	 */
	public void debug(final Message messageObject) {

		logger.debug(getLogContent(null, messageObject));
	}

	/**
	 * Method to Log debug message.
	 * 
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	public void debug(final Message messageObject, final Throwable throwable) {
		if (throwable == null) {
			debug(messageObject);
		} else {
			logger.debug(getLogContent(null, messageObject), throwable);
		}
	}

	/**
	 * Method to Log debug message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 */
	public void debug(final TransactionContext trxContext,
			final Message messageObject) {
		logger.debug(getLogContent(trxContext, messageObject));
	}

	/**
	 * Method to Log debug message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	public void debug(final TransactionContext trxContext,
			final Message messageObject, final Throwable throwable) {
		if (throwable == null) {
			debug(trxContext, messageObject);
		} else {
			logger.debug(getLogContent(trxContext, messageObject), throwable);
		}
	}

	/**
	 * Method to Log info message.
	 * 
	 * @param messageObject
	 *            Message
	 */
	public void info(final Message messageObject) {
		logger.info(getLogContent(null, messageObject));
	}

	/**
	 * Method to Log info message.
	 * 
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	public void info(final Message messageObject, final Throwable throwable) {
		if (throwable == null) {
			info(messageObject);
		} else {
			logger.info(getLogContent(null, messageObject), throwable);
		}
	}

	/**
	 * Method to Log info message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 */
	public void info(final TransactionContext trxContext,
			final Message messageObject) {
		logger.info(getLogContent(trxContext, messageObject));
	}

	/**
	 * Method to Log info message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	public void info(final TransactionContext trxContext,
			final Message messageObject, final Throwable throwable) {
		if (throwable == null) {
			info(trxContext, messageObject);
		} else {
			logger.info(getLogContent(trxContext, messageObject), throwable);
		}
	}

	/**
	 * Method to Log warn message.
	 * 
	 * @param messageObject
	 *            Message
	 */
	public void warn(final Message messageObject) {
		logger.warn(getLogContent(null, messageObject));
	}

	/**
	 * Method to Log warn message.
	 * 
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	public void warn(final Message messageObject, final Throwable throwable) {
		if (throwable == null) {
			warn(messageObject);
		} else {
			logger.warn(getLogContent(null, messageObject), throwable);
		}
	}

	/**
	 * Method to Log warn message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 */
	public void warn(final TransactionContext trxContext,
			final Message messageObject) {
		logger.warn(getLogContent(trxContext, messageObject));
	}

	/**
	 * Method to Log warn message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	public void warn(final TransactionContext trxContext,
			final Message messageObject, final Throwable throwable) {
		if (throwable == null) {
			warn(trxContext, messageObject);
		} else {
			logger.warn(getLogContent(trxContext, messageObject), throwable);
		}
	}

	/**
	 * Method to Log error message.
	 * 
	 * @param messageObject
	 *            Message
	 */
	public void error(final Message messageObject) {
		logger.error(getLogContent(null, messageObject));
	}

	/**
	 * Method to Log error message.
	 * 
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	public void error(final Message messageObject, final Throwable throwable) {
		if (throwable == null) {
			error(messageObject);
		} else {
			logger.error(getLogContent(null, messageObject), throwable);
		}
	}

	/**
	 * Method to Log error message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 */
	public void error(final TransactionContext trxContext,
			final Message messageObject) {
		logger.error(getLogContent(trxContext, messageObject));
	}

	/**
	 * Method to Log error message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	public void error(final TransactionContext trxContext,
			final Message messageObject, final Throwable throwable) {
		if (throwable == null) {
			error(trxContext, messageObject);
		} else {
			logger.error(getLogContent(trxContext, messageObject), throwable);
		}
	}

	/**
	 * Method to Log fatal message.
	 * 
	 * @param messageObject
	 *            Message
	 */
	public void fatal(final Message messageObject) {
		logger.fatal(getLogContent(null, messageObject));
	}

	/**
	 * Method to Log fatal message.
	 * 
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	public void fatal(final Message messageObject, final Throwable throwable) {
		if (throwable == null) {
			fatal(messageObject);
		} else {
			logger.fatal(getLogContent(null, messageObject), throwable);
		}
	}

	/**
	 * Method to Log fatal message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 */
	public void fatal(final TransactionContext trxContext,
			final Message messageObject) {
		logger.fatal(getLogContent(trxContext, messageObject));
	}

	/**
	 * Method to Log fatal message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	public void fatal(final TransactionContext trxContext,
			final Message messageObject, final Throwable throwable) {
		if (throwable == null) {
			fatal(trxContext, messageObject);
		} else {
			logger.fatal(getLogContent(trxContext, messageObject), throwable);
		}
	}

	/**
	 * Method to find debug is enabled or not.
	 * 
	 * @return boolean
	 */
	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}

	/**
	 * Method to find Info is enabled or not.
	 * 
	 * @return boolean
	 */
	public boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}

	/**
	 * Method to format the log with ThansactionContext, MessageCode and Message
	 * into a formatted string. If trxContext is null then it gets
	 * TransactionContext from <code>{@link ThreadLocalContainer}</code>. It
	 * uses <code>{@link DefaultLogFormatter}</code> to format the message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @return String - log message
	 */
	private String getLogContent(TransactionContext trxContext,
			final Message messageObject) {

		if (trxContext == null) {
			trxContext = ThreadLocalContainer.getTransactionContext();
		}

		if (trxContext == null) {
			if (messageObject == null) {
				return "";
			}
			StringBuilder log = new StringBuilder();
			log.append(messageObject.getMessageCode());
			log.append(" ");
			log.append(messageObject.getMessage());

			return log.toString();
		} else {
			DefaultLogFormatter logFormatter = new DefaultLogFormatter();
			logFormatter.setSPACE(" ");
			logFormatter.setBLANK_MESSAGECODE("        ");
			logFormatter.setBLANK_CORPORATION_CODE("    ");
			logFormatter.setBLANK_USER_ID("                ");
			logFormatter.setBLANK_IP_ADDRESS("***.***.****");
			logFormatter.setBLANK_MESSAGE("");

			return logFormatter.formatLog(trxContext, messageObject);

		}
	}
}
