/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)
 *  2010/11/12 <Change NO.0004> MESSAGE_FILE_PATH changed from message to message_fw by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * InitializationErrorMessageFactory to manage initialization error message, It
 * reads message from properties file and format it.
 * 
 * @see InitializationErrorLogger
 * @see DefaultLogger
 * @see DefaultLogFormatter
 * @see SimpleMessageFactory
 * @see MessageCode
 */
public final class InitializationErrorMessageFactory {

	/** message properties file. */
	private static String MESSAGE_FILE_PATH = "com.hoyavc.nxg.message.message_fw";

	/** empty message in-case of MessageCode is null. */
	private static Message emptyMessage = new Message(null, "", null);

	/** Constructor. */
	private InitializationErrorMessageFactory() {
	}

	/**
	 * Method to get the message for the MessageCode and messageParam.
	 * <p>
	 * In-case message string contains "'" it will be replace with "''" as
	 * escape char.
	 * 
	 * <pre>
	 *    	example:
	 *          TransactionContext's UserData is [0]
	 *        will be replace as for processing
	 *          TransactionContext''s UserData is [0]
	 *        output will be
	 *          TransactionContext's UserData is invalid
	 * </pre>
	 * 
	 * </p>
	 * 
	 * @param messageCode
	 *            Message Code
	 * @param messageParam
	 *            Parameters for the message
	 * @return Message contains MessageCode and message, In-case of messageCode
	 *         is null it will returns emptyMessage
	 */
	public static Message getMessage(final MessageCode messageCode,
			final String[] messageParam) {

		// validating parameter
		if (messageCode == null) {
			return emptyMessage;
		}
		if (messageParam == null || messageParam.length <= 0) {
			return getMessage(messageCode);
		}

		// getting message string
		String rowMessage = getMessage(messageCode.toString());
		if (rowMessage == null || rowMessage.trim().length() <= 0) {
			return new Message(messageCode, "", messageParam);
		}

		// formatting message string with message parameters
		String message = "";
		try {
			if (rowMessage.indexOf("'") >= 0) {
				String tempRowMessage = rowMessage.replaceAll("'", "''");
				message = MessageFormat.format(tempRowMessage,
						(Object[]) messageParam);
			} else {
				message = MessageFormat.format(rowMessage,
						(Object[]) messageParam);
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return emptyMessage;
		}

		return new Message(messageCode, message, messageParam);
	}

	/**
	 * Method to get the message for the MessageCode.
	 * 
	 * @param messageCode
	 *            Message Code
	 * @return Message contains MessageCode and message, In-case of messageCode
	 *         is null it will returns emptyMessage
	 */
	public static Message getMessage(final MessageCode messageCode) {

		if (messageCode == null) {
			return emptyMessage;
		}
		String message = getMessage(messageCode.toString());

		return new Message(messageCode, message, null);

	}

	/**
	 * Getting message for the messageCode from the
	 * {@link InitializationErrorMessageFactory#MESSAGE_FILE_PATH messagePropertiesFile}.
	 * 
	 * @param stringMessageCode
	 *            Message Code as String
	 * @return String message string or In-case of exception returns ""
	 */
	private static String getMessage(final String stringMessageCode) {

		if (stringMessageCode == null) {
			return "";
		}
		// temporary

		try {
			ResourceBundle bundle = ResourceBundle.getBundle(MESSAGE_FILE_PATH,
					new Locale(""));
			return bundle.getString(stringMessageCode);

		} catch (RuntimeException e) {
			e.printStackTrace();
			return "";
		}
	}

}
