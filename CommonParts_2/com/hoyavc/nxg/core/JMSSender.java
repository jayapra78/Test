/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/05/03 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/05/07 <Change NO.0002> Add Param Validation and JavaDoc by Alex (SRM)  
 */
package com.hoyavc.nxg.core;

import java.io.Serializable;

/**
 * Interface for Sending JMS messages(Object Message) to JMS Server.
 * 
 */
public interface JMSSender {

	/**
	 * Sending Object message to JMS Destination.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param object
	 *            Serializable Object
	 * 
	 */
	void send(final TransactionContext trxContext, final Serializable object);

}
