/*
 * Copyright(C)2007-2013 Hoya corporation All rights reserved.
 *
 */
package com.hoyavc.nxg.core;


/**
 * Interface to load server information. 
 * 
 * @author Kalai.P
 *
 */
public interface LoadServerInfo {
	
	/**
	 * To load current Server corporation code.
	 * @param currentServerVersion
	 * @throws ApplicationException
	 */
	public void setCurrentServerCorporationCode(String corporationCode ) throws ApplicationException;

	/**
	 * To load current Server version.
	 * @param currentServerVersion
	 * @throws ApplicationException
	 */
	public void setCurrentServerVersion(int currentServerVersion ) throws ApplicationException;
}
