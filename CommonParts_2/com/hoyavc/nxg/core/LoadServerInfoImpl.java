/*
 * Copyright(C)2007-2013 Hoya corporation All rights reserved.
 *
 */
package com.hoyavc.nxg.core;


/**
 * Implementation class to load server information. 
 * 
 * @author Kalai.P
 *
 */
class LoadServerInfoImpl implements LoadServerInfo{

	/**
	 * To load current Server corporation code.
	 * @param currentServerVersion
	 * @throws ApplicationException
	 */
	public void setCurrentServerCorporationCode(String corporationCode) throws ApplicationException {
		ServerInfo.setCurrentServerCorporationCode(corporationCode);
	}
	
	/**
	 * To load current EAR version.
	 * @param currentEARVersion
	 * @throws ApplicationException
	 */
	public void setCurrentServerVersion(int currentServerVersion ) throws ApplicationException{
		ServerInfo.setCurrentServerVersion(currentServerVersion);
	}
}
