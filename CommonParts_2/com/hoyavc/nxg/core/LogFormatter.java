/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for format logging information implementation classes.
 * 
 * @see DefaultLogFormatter
 */
public interface LogFormatter {

	/**
	 * To format the logging informations into String.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param message
	 *            Message
	 * @return String Formatter Log
	 */
	String formatLog(final TransactionContext trxContext, final Message message);

}
