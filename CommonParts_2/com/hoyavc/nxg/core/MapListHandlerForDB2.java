/*
 * Copyright 2007-2013 by NXG Project, All rights reserved.
 *
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00005;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.dbutils.handlers.MapListHandler;

/**
 * Class to create SqlReturnMap list with ResultSet.
 * {@link ResultSetProxyFactoryForDB2} is used for handling LOB objects.
 * 
 * @see SqlReturnMap
 * @see SqlReturnMapForDB2
 */
class MapListHandlerForDB2 extends MapListHandler {

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());

	/** ResultSet Proxy Factory for DB2. */
	private ResultSetProxyFactoryForDB2 resultSetProxyFactoryForDB2;

	/**
	 * Method to create ResultSetMap with {@link ResultSet}.
	 * 
	 * <p>
	 * This method reads the ResultSet and creates the Map with the values.
	 * {@link ResultSetProxyFactoryForDB2} is used for handling LOB objects.
	 * </p>
	 * 
	 * @param rs
	 *            ResultSet
	 * @return Object
	 * @throws SQLException
	 *             SQL Exception
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If rs is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If SQLException occurs (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             <li>if IOException is thrown while reading Blob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>If SQLException is thrown while reading Blob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             <li>If ClassNotFoundException is thrown (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>if IOException is thrown while reading Clob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>If SQLException is thrown while reading Clob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             </ul>
	 * 
	 */
	protected final Object handleRow(final ResultSet rs) throws SQLException {

		// validating parameter.
		if (rs == null) {
			String[] params = new String[] { ResultSet.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		Map<String, Object> result = new HashMap<String, Object>();
		ResultSetProxyForDB2 rsProxy = resultSetProxyFactoryForDB2.create(rs);
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();

			for (int i = 1; i <= cols; i++) {

				String columnName = rsmd.getColumnLabel(i);
				// upper case
				result.put(columnName.toUpperCase(), rsProxy.getObject(columnName));
			}

		} catch (SQLException sqlExp) {
			String[] params = new String[] { sqlExp.getMessage() };
			Message message = fjfe00005.createMessage(params);

			logger.error(message, sqlExp);
			throw new SystemException(message, sqlExp);
		}

		return new SqlReturnMapForDB2(result);
	}

	/**
	 * Getter method for resultSetProxyFactoryForDB2.
	 * 
	 * @return the ResultSetProxyFactoryForDB2
	 */

	public ResultSetProxyFactoryForDB2 getResultSetProxyFactoryForDB2() {
		return resultSetProxyFactoryForDB2;
	}

	/**
	 * Setter method for resultSetProxyFactoryForDB2.
	 * 
	 * @param resultSetProxyFactoryForDB2
	 *            the ResultSetProxyFactoryForDB2 to set
	 * @throws SystemException
	 *             If resultSetProxyFactoryForDB2 is null (
	 *             {@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setResultSetProxyFactoryForDB2(final ResultSetProxyFactoryForDB2 resultSetProxyFactoryForDB2) {

		if (resultSetProxyFactoryForDB2 == null) {
			String[] params = new String[] { ResultSetProxyFactoryForDB2.class.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger.getInstance(this.getClass());
			initializationErrorLogger.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.resultSetProxyFactoryForDB2 = resultSetProxyFactoryForDB2;
	}

}
