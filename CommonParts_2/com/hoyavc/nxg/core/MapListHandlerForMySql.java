/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2010/06/07 <Change NO.0001> created by (SRM)  
 *  2011/04/18 <Change NO.0002> Modified to upgrade mysql connector by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00005;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.dbutils.handlers.MapListHandler;

/**
 * Class to create SqlReturnMap list with ResultSet.
 * {@link ResultSetProxyFactoryForMySql} is used for handling LOB objects.
 * 
 * @see SqlReturnMap
 * @see SqlReturnMapForMySql
 */
class MapListHandlerForMySql extends MapListHandler {

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());

	/** ResultSet Proxy Factory for MySQL. */
	private ResultSetProxyFactoryForMySql resultSetProxyFactoryForMySql;

	/**
	 * Method to create ResultSetMap with {@link ResultSet}.
	 * 
	 * <p>
	 * This method reads the ResultSet and creates the Map with the values.
	 * {@link ResultSetProxyFactoryForMySql} is used for handling LOB objects.
	 * </p>
	 * 
	 * @param rs
	 *            ResultSet
	 * @return Object
	 * @throws SQLException
	 *             SQL Exception
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If rs is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If SQLException occurs (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             <li>if IOException is thrown while reading Blob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>If SQLException is thrown while reading Blob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             <li>If ClassNotFoundException is thrown (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>if IOException is thrown while reading Clob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>If SQLException is thrown while reading Clob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             </ul>
	 * 
	 */
	protected final Object handleRow(final ResultSet rs) throws SQLException {

		// validating parameter.
		if (rs == null) {
			String[] params = new String[] { ResultSet.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		Map<String, Object> result = new HashMap<String, Object>();
		ResultSetProxyForMySql rsProxy = resultSetProxyFactoryForMySql.create(rs);
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();

			for (int i = 1; i <= cols; i++) {

				String columnName = rsmd.getColumnLabel(i);
				// upper case
				result.put(columnName.toUpperCase(), rsProxy.getObject(columnName));
			}

		} catch (SQLException sqlExp) {
			String[] params = new String[] { sqlExp.getMessage() };
			Message message = fjfe00005.createMessage(params);

			logger.error(message, sqlExp);
			throw new SystemException(message, sqlExp);
		}

		return new SqlReturnMapForMySql(result);
	}

	/**
	 * Getter method for resultSetProxyFactoryForMySql.
	 * 
	 * @return the ResultSetProxyFactoryForMySql
	 */

	public ResultSetProxyFactoryForMySql getResultSetProxyFactoryForMySql() {
		return resultSetProxyFactoryForMySql;
	}

	/**
	 * Setter method for resultSetProxyFactoryForMySql.
	 * 
	 * @param resultSetProxyFactoryForMySql
	 *            the ResultSetProxyFactoryForMySql to set
	 * @throws SystemException
	 *             If resultSetProxyFactoryForMySql is null (
	 *             {@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setResultSetProxyFactoryForMySql(final ResultSetProxyFactoryForMySql resultSetProxyFactoryForMySql) {

		if (resultSetProxyFactoryForMySql == null) {
			String[] params = new String[] { ResultSetProxyFactoryForMySql.class.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger.getInstance(this.getClass());
			initializationErrorLogger.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.resultSetProxyFactoryForMySql = resultSetProxyFactoryForMySql;
	}

}
