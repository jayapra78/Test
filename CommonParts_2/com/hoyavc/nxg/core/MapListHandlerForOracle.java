/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM) 
 *  2007/05/21 <Change NO.0004> ResultSet Proxy is used for handling LOB objects Added by Alex (SRM)  
 *  2008/11/07 <Change NO.0005> resultSetProxyFactoryForOracle has been added for perfomance improvement by N.Fukuda (VCIT) 
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00005;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.dbutils.handlers.MapListHandler;


/**
 * Class to create SqlReturnMap list with ResultSet.
 * {@link ResultSetProxyFactoryForOracle} is used for handling LOB objects.
 * 
 * @see SqlReturnMap
 * @see SqlReturnMapForOracle
 */
class MapListHandlerForOracle extends MapListHandler {

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());

	/** ResultSet Proxy Factory for oracle. */
	private ResultSetProxyFactoryForOracle resultSetProxyFactoryForOracle;

	
	
	/**
	 * Method to create ResultSetMap with {@link ResultSet}.
	 * 
	 * <p>
	 * This method reads the ResultSet and creates the Map with the values.
	 * {@link ResultSetProxyFactoryForOracle} is used for handling LOB objects.
	 * </p>
	 * 
	 * @param rs
	 *            ResultSet
	 * @return Object
	 * @throws SQLException SQL Exception
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If rs is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If SQLException occurs ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             <li>if IOException is thrown while reading Blob content ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>If SQLException is thrown while reading Blob content ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             <li>If ClassNotFoundException is thrown ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>if IOException is thrown while reading Clob content ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>If SQLException is thrown while reading Clob content ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             </ul>
	 * 
	 */
	protected final Object handleRow(final ResultSet rs) throws SQLException {

		// validating parameter.
		if (rs == null) {
			String[] params = new String[] { ResultSet.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		Map<String, Object> result = new HashMap<String, Object>();
		ResultSetProxyForOracle rsProxy = resultSetProxyFactoryForOracle
				.create(rs);
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();

			for (int i = 1; i <= cols; i++) {

				String columnName = rsmd.getColumnName(i);
				// upper case
				result.put(columnName.toUpperCase(), rsProxy
						.getObject(columnName));
			}

		} catch (SQLException sqlExp) {
			String[] params = new String[] { sqlExp.getMessage() };
			Message message = fjfe00005.createMessage(params);

			logger.error(message, sqlExp);
			throw new SystemException(message, sqlExp);
		}

		return new SqlReturnMapForOracle(result);
	}

	/**
	 * Getter method for resultSetProxyFactoryForOracle.
	 * 
	 * @return the ResultSetProxyFactoryForOracle
	 */

	public ResultSetProxyFactoryForOracle getResultSetProxyFactoryForOracle() {
		return resultSetProxyFactoryForOracle;
	}

	/**
	 * Setter method for resultSetProxyFactoryForOracle.
	 * 
	 * @param resultSetProxyFactoryForOracle
	 *            the ResultSetProxyFactoryForOracle to set
	 * @throws SystemException
	 *             If resultSetProxyFactoryForOracle is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setResultSetProxyFactoryForOracle(
			final ResultSetProxyFactoryForOracle resultSetProxyFactoryForOracle) {
		
		if (resultSetProxyFactoryForOracle == null) {
			String[] params = new String[] { ResultSetProxyFactoryForOracle.class
					.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.resultSetProxyFactoryForOracle = resultSetProxyFactoryForOracle;
	}


}
