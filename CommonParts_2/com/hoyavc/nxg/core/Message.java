/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import java.io.Serializable;

/**
 * Message class which encapsulating MessageCode and its Message. NXG framework
 * uses this class for logging message.
 * 
 */
public class Message implements Serializable {

	/** Serial ID. */
	private static final long serialVersionUID = 1L;

	/** message code. */
	private final String messageCode;

	/** message string. */
	private final String message;

	/**
	 * message param (kept for localization).
	 * <p>
	 * This message param is used to maintain the parameters for different
	 * locales.
	 * </p>
	 */
	private final String[] messageParam;

	/**
	 * Message Constructor to initialize with MessageCode, message and message
	 * param.
	 * 
	 * @param messageCodeEnum
	 *            MessageCode
	 * @param message
	 *            String
	 * @param messageParam
	 *            String[]
	 */
	public Message(final MessageCode messageCodeEnum, final String message,
			final String[] messageParam) {

		this.messageCode = messageCodeEnum == null ? null : messageCodeEnum
				.toString();
		this.message = message;

		this.messageParam = messageParam == null ? null : messageParam.clone();

	}

	/**
	 * Getter method for message string.
	 * 
	 * @return message String
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Getter method for message code.
	 * 
	 * @return message code
	 */
	public String getMessageCode() {
		return messageCode;

	}

	/**
	 * Getter method for message param.
	 * 
	 * @return message param
	 */
	public String[] getMessageParam() {

		return messageParam == null ? null : messageParam.clone();

	}

	/**
	 * Override method to return messageCode and message.
	 * 
	 * @return String
	 * @see Object#toString()
	 */
	@Override
	public String toString() {
		return messageCode + " " + message;
	}

}
