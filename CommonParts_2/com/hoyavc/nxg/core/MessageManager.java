/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for the MessageManager classes which implements methods for get
 * message string.
 * 
 */
public interface MessageManager {

	/**
	 * Method to get the message for the given MessageCode and localeKey.
	 * 
	 * @param messageCode
	 *            Message Code
	 * @param localeKey
	 *            Locale Key (en, jp, ...)
	 * @return String message
	 */
	String getMessage(final MessageCode messageCode, final String localeKey);

	/**
	 * Method to get the message for the given MessageCode, messageParam and
	 * localeKey.
	 * 
	 * @param messageCode
	 *            Message Code
	 * @param messageParam
	 *            Parameters for the message
	 * @param localeKey
	 *            Locale Key (en, jp, ...)
	 * @return String message
	 */
	String getMessage(final MessageCode messageCode, final String localeKey,
			final String[] messageParam);

}
