/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * NXGLogger is the interface for NXG Framework logging system.
 * 
 */
public interface NXGLogger {

	/**
	 * Method to Log debug message.
	 * 
	 * @param messageObject
	 *            Message
	 */
	void debug(final Message messageObject);

	/**
	 * Method to Log debug message.
	 * 
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	void debug(final Message messageObject, final Throwable throwable);

	/**
	 * Method to Log debug message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 */
	void debug(final TransactionContext trxContext, final Message messageObject);

	/**
	 * Method to Log debug message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	void debug(final TransactionContext trxContext,
			final Message messageObject, final Throwable throwable);

	/**
	 * Method to Log info message.
	 * 
	 * @param messageObject
	 *            Message
	 */
	void info(final Message messageObject);

	/**
	 * Method to Log info message.
	 * 
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	void info(final Message messageObject, final Throwable throwable);

	/**
	 * Method to Log info message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 */
	void info(final TransactionContext trxContext, final Message messageObject);

	/**
	 * Method to Log info message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	void info(final TransactionContext trxContext, final Message messageObject,
			final Throwable throwable);

	/**
	 * Method to Log warn message.
	 * 
	 * @param messageObject
	 *            Message
	 */
	void warn(final Message messageObject);

	/**
	 * Method to Log warn message.
	 * 
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	void warn(final Message messageObject, final Throwable throwable);

	/**
	 * Method to Log warn message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 */
	void warn(final TransactionContext trxContext, final Message messageObject);

	/**
	 * Method to Log warn message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	void warn(final TransactionContext trxContext, final Message messageObject,
			final Throwable throwable);

	/**
	 * Method to Log error message.
	 * 
	 * @param messageObject
	 *            Message
	 */
	void error(final Message messageObject);

	/**
	 * Method to Log error message.
	 * 
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	void error(final Message messageObject, final Throwable throwable);

	/**
	 * Method to Log error message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 */
	void error(final TransactionContext trxContext, final Message messageObject);

	/**
	 * Method to Log error message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	void error(final TransactionContext trxContext,
			final Message messageObject, final Throwable throwable);

	/**
	 * Method to Log fatal message.
	 * 
	 * @param messageObject
	 *            Message
	 */
	void fatal(final Message messageObject);

	/**
	 * Method to Log fatal message.
	 * 
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	void fatal(final Message messageObject, final Throwable throwable);

	/**
	 * Method to Log fatal message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 */
	void fatal(final TransactionContext trxContext, final Message messageObject);

	/**
	 * Method to Log fatal message.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @param messageObject
	 *            Message
	 * @param throwable
	 *            Throwable
	 */
	void fatal(final TransactionContext trxContext,
			final Message messageObject, final Throwable throwable);

	/**
	 * Method to find debug is enabled or not.
	 * 
	 * @return boolean
	 */
	boolean isDebugEnabled();

	/**
	 * Method to find Info is enabled or not.
	 * 
	 * @return boolean
	 */
	boolean isInfoEnabled();

}
