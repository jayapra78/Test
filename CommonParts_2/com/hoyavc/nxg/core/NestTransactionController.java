/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;

/**
 * Class to handle the NestTransactionStrategy.
 * 
 */
public class NestTransactionController {

	/** NestTransactionStrategy. */
	private NestTransactionStrategy nestTransactionStrategy;

	/**
	 * getter method for nestTransactionStrategy.
	 * 
	 * @return nestTransactionStrategy NestTransactionStrategy
	 */
	public NestTransactionStrategy getNestTransactionStrategy() {
		return nestTransactionStrategy;
	}

	/**
	 * setter method for nestTransactionStrategy.
	 * 
	 * @param nestTransactionStrategy
	 *            Nest Transaction Strategy
	 * @throws SystemException
	 *             If nestTransactionStrategy is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setNestTransactionStrategy(
			final NestTransactionStrategy nestTransactionStrategy) {

		if (nestTransactionStrategy == null) {
			String[] params = new String[] { NestTransactionStrategy.class
					.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.nestTransactionStrategy = nestTransactionStrategy;
	}

}
