/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for nested transaction strategy classes. Implementation class
 * defines the invokeWithNewTransaction method for new transaction invoker.
 * 
 */
public interface NestTransactionStrategy {

	/**
	 * Method to invoke CBS bean thru new transaction.
	 * 
	 * @param id
	 *            ID
	 * @param invokerId
	 *            Invoke ID
	 * @param trxContext
	 *            TransactionContext
	 * @param vo
	 *            ValueObject
	 * @return ValueObject
	 * @throws ApplicationException Application Exception
	 */
	ValueObject invokeWithNewTransaction(final String id,
			final String invokerId, final TransactionContext trxContext,
			final ValueObject vo) throws ApplicationException;
	
	/**
	 * Method to invoke CBS bean thru new transaction.
	 * 
	 * @param id
	 *            ID
	 * @param invokerId
	 *            Invoke ID
	 * @param trxContext
	 *            TransactionContext
	 * @param vo
	 *            ValueObject
	 * @param isNewTransaction
	 * 			  Boolean, True: to create new Processing GMT DateTime.
	 *                     False: Use the same processing GMT DateTime.
	 *  
	 * @return ValueObject
	 * @throws ApplicationException Application Exception
	 */
	ValueObject invokeWithNewTransaction(final String id,
			final String invokerId, final TransactionContext trxContext,
			final ValueObject vo, final boolean isNewTransaction) throws ApplicationException;

}
