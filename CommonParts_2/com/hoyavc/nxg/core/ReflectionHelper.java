/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00701;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00702;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00703;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00704;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * NXG framework's reflection wrapper class or reflection helper class. This
 * class wrappers all the exceptions into <code>{@link SystemException} </code>.
 * 
 */
public final class ReflectionHelper {

	/** NXGLogger for logging messages. */
	private static final NXGLogger logger = DefaultLogger
			.getInstance(ReflectionHelper.class);

	/** Constructor. */
	private ReflectionHelper() {
	};

	/**
	 * Method to get the specified declared method from the targetClass.
	 * 
	 * @param targetClass
	 *            Class object
	 * @param methodName
	 *            invoking method name
	 * @param paramTypes
	 *            methods parameter types as {@link Class}
	 * @return Method object for the respective request
	 * @throws SystemException
	 *             if any of the case
	 *             <ul>
	 *             <li>If targetClass is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If methodName is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If SecurityException throws ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00701 fjfe00701})
	 *             <li>If NoSuchMethodException throws ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00702 fjfe00702})
	 *             </ul>
	 * 
	 */
	public static Method getDeclaredMethod(final Class targetClass,
			final String methodName, final Class[] paramTypes) {

		// validating parameter
		if (targetClass == null) {
			String[] params = new String[] { Class.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		if (methodName == null || methodName.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(), methodName };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		try {
			return targetClass.getDeclaredMethod(methodName, paramTypes);

		} catch (SecurityException e) {
			Message message = fjfe00701
					.createMessage(new String[] { methodName });

			logger.error(message, e);
			throw new SystemException(message, e);

		} catch (NoSuchMethodException e) {
			Message message = fjfe00702
					.createMessage(new String[] { methodName });

			logger.error(message, e);
			throw new SystemException(message, e);
		}
	}

	/**
	 * Method to get the specified public method from the targetClass.
	 * 
	 * @param targetClass
	 *            Class object
	 * @param methodName
	 *            invoking method name
	 * @param paramTypes
	 *            methods parameter types as {@link Class}
	 * @return Method object for the respective request
	 * @throws SystemException
	 *             if any of the case
	 *             <ul>
	 *             <li>If targetClass is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If methodName is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If SecurityException throws ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00701 fjfe00701})
	 *             <li>If NoSuchMethodException throws ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00702 fjfe00702})
	 *             </ul>
	 */
	public static Method getMethod(final Class targetClass,
			final String methodName, final Class[] paramTypes) {

		// validating parameter
		if (targetClass == null) {
			String[] params = new String[] { Class.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		if (methodName == null || methodName.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(), methodName };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		try {
			return targetClass.getMethod(methodName, paramTypes);

		} catch (SecurityException e) {
			Message message = fjfe00701
					.createMessage(new String[] { methodName });
			logger.error(message, e);
			throw new SystemException(message, e);

		} catch (NoSuchMethodException e) {
			Message message = fjfe00702
					.createMessage(new String[] { methodName });
			logger.error(message, e);
			throw new SystemException(message, e);
		}
	}

	/**
	 * To invoke a method of the given instance using Method object. It returns
	 * the invoking result as Object. For the restricted access method it sets
	 * the access right and invoke.
	 * 
	 * @param method
	 *            Method object
	 * @param instance
	 *            of the Class
	 * @param params
	 *            Method parameters as object array
	 * @return Object result as object
	 * @throws Throwable
	 *             The cause of {@link InvocationTargetException}
	 * @throws SystemException
	 *             if any of the case
	 *             <ul>
	 *             <li>If method is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If instance is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If SecurityException throws ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00701 fjfe00701})
	 *             <li>If IllegalArgumentException throws ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00703 fjfe00703})
	 *             <li>If IllegalAccessException throws ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00704 fjfe00704})
	 *             </ul>
	 */
	public static Object invokeMethod(final Method method,
			final Object instance, final Object[] params) throws Throwable {

		// validating parameter
		if (method == null) {
			String[] param = new String[] { Method.class.getName(), null };
			Message message = fjfe00004.createMessage(param);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (instance == null && !Modifier.isStatic(method.getModifiers())) {
			String[] param = new String[] { Object.class.getName(), null };
			Message message = fjfe00004.createMessage(param);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		// setting access right for the private, protected and default access
		// rights methods
		try {
			method.setAccessible(true);

		} catch (SecurityException e) {
			Message message = fjfe00701.createMessage(new String[] { method
					.getName() });
			logger.error(message, e);
			throw new SystemException(message, e);
		}

		// invoking the method
		try {
			return method.invoke(instance, params);

		} catch (IllegalArgumentException e) {

			int paramLength = params == null ? 0 : params.length;

			String[] objType = new String[paramLength];
			for (int i = 0; i < paramLength; i++) {
				objType[i] = params[i].getClass().getName();
			}

			String[] param = new String[] { method.toString(),
					fjfe00703.appendParamter(objType) };

			Message message = fjfe00703.createMessage(param);
			logger.error(message, e);
			throw new SystemException(message, e);

		} catch (IllegalAccessException e) {
			Message message = fjfe00704.createMessage();
			logger.error(message, e);
			throw new SystemException(message, e);

		} catch (InvocationTargetException e) {
			// do not wrap with SystemException.
			throw e.getCause();

		}
	}
}
