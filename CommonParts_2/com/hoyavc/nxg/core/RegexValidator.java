/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/05/17 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/05/17 <Change NO.0002> JavaDoc by Baskaran (SRM)
 */
package com.hoyavc.nxg.core;

/**
 * Interface for the class that handle regular expression and validation.
 */
public interface RegexValidator {

	/**
	 * Method to validate the regular expression.
	 * 
	 * @param str
	 *            String to check.
	 * @return boolean True if matched, else false.
	 */
	boolean validate(final String str);

}
