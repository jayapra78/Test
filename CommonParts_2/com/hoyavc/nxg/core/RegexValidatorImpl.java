/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/05/17 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/05/17 <Change NO.0002> JavaDoc by Baskaran (SRM)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;

/**
 * Class to handle regular expression and validating.
 */
class RegexValidatorImpl implements RegexValidator {

	/**
	 * String to handle regular expression.
	 */
	private String regex;

	/**
	 * Getter method for regular expression.
	 * 
	 * @return String regular expression.
	 */
	public String getRegex() {
		return regex;
	}

	/**
	 * Setter method for regular expression.
	 * 
	 * @param regex
	 *            Regular expression
	 * @throws SystemException
	 *             if regex is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setRegex(final String regex) {

		if (regex == null) {
			String[] params = new String[] { RegexValidatorImpl.class.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		this.regex = regex.trim();
	}

	/**
	 * Method to validate the regular expression.
	 * <ul>
	 * <li>If str is null, it will return false.
	 * <li>If regex is empty string, it will return false.
	 * </ul>
	 * 
	 * @param str
	 *            String to check.
	 * @return boolean True if matched, else false.
	 */
	public boolean validate(final String str) {
		if (str == null || regex == null) {
			return false;
		}
		return str.matches(regex);
	}

}
