/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/06/29 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/06/29 <Change NO.0002> Added javadoc by Alex (SRM)
 */
package com.hoyavc.nxg.core;

/**
 * Interface for accessing resource files.
 * 
 */
public interface ResourceHolder {

	/**
	 * Getting value for key.
	 * 
	 * @param key
	 *            Key name
	 * @return Property value
	 */
	String getValue(final String key);

}
