/*
 * Copyright 2007-2013 by NXG Project, All rights reserved.
 *
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;

import java.sql.ResultSet;

import com.hoyavc.nxg.core.ResultSetProxyForDB2.ResultSetProxyForDB2Option;

/**
 * Factory class to create ResultSetProxyForDB2 with ResultSet and
 * ResultSetProxyForDB2Option. {@link ResultSetProxyFactoryForDB2} is used
 * for handling LOB objects.
 */
class ResultSetProxyFactoryForDB2 {

	/** ResultSet Proxy for option. */
	private ResultSetProxyForDB2Option resultSetProxyForDB2Option;

	/**
	 * To create the ResultSetProxyForDB2 with ResultSet and
	 * ResultSetProxyForDB2Option {@link ResultSetProxyFactoryForDB2} is
	 * used for handling LOB objects.
	 * 
	 * @param sourceResultSet
	 *            ResultSet
	 * 
	 * @return ResultSetProxyForDB2
	 */
	ResultSetProxyForDB2 create(final ResultSet sourceResultSet) {

		return new ResultSetProxyForDB2(sourceResultSet, resultSetProxyForDB2Option);

	}

	/**
	 * Getter method for resultSetProxyForDB2Option.
	 * 
	 * @return the ResultSetProxyForDB2Option
	 */

	public ResultSetProxyForDB2Option getResultSetProxyForDB2Option() {
		return resultSetProxyForDB2Option;
	}

	/**
	 * Setter method for resultSetProxyForDB2Option.
	 * 
	 * @param resultSetProxyForDB2Option
	 *            the ResultSetProxyForDB2Option to set
	 * 
	 * @throws SystemException
	 *             If resultSetProxyFactoryForDB2 is null (
	 *             {@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 * 
	 */
	public void setResultSetProxyForDB2Option(final ResultSetProxyForDB2Option resultSetProxyForDB2Option) {

		if (resultSetProxyForDB2Option == null) {
			String[] params = new String[] { ResultSetProxyForDB2Option.class.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger.getInstance(this.getClass());
			initializationErrorLogger.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.resultSetProxyForDB2Option = resultSetProxyForDB2Option;
	}

}
