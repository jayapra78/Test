/*
 * Copyright 2008 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 * 2010/06/07 <Change NO.0001> created by (SRM)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;

import java.sql.ResultSet;

import com.hoyavc.nxg.core.ResultSetProxyForMySql.ResultSetProxyForMySqlOption;

/**
 * Factory class to create ResultSetProxyForMySql with ResultSet and
 * ResultSetProxyForMySqlOption. {@link ResultSetProxyFactoryForMySql} is used
 * for handling LOB objects.
 */
class ResultSetProxyFactoryForMySql {

	/** ResultSet Proxy for option. */
	private ResultSetProxyForMySqlOption resultSetProxyForMySqlOption;

	/**
	 * To create the ResultSetProxyForMySql with ResultSet and
	 * ResultSetProxyForMySqlOption {@link ResultSetProxyFactoryForMySql} is
	 * used for handling LOB objects.
	 * 
	 * @param sourceResultSet
	 *            ResultSet
	 * 
	 * @return ResultSetProxyForMySql
	 */
	ResultSetProxyForMySql create(final ResultSet sourceResultSet) {

		return new ResultSetProxyForMySql(sourceResultSet, resultSetProxyForMySqlOption);

	}

	/**
	 * Getter method for resultSetProxyForMySqlOption.
	 * 
	 * @return the ResultSetProxyForMySqlOption
	 */

	public ResultSetProxyForMySqlOption getResultSetProxyForMySqlOption() {
		return resultSetProxyForMySqlOption;
	}

	/**
	 * Setter method for resultSetProxyForMySqlOption.
	 * 
	 * @param resultSetProxyForMySqlOption
	 *            the ResultSetProxyForMySqlOption to set
	 * 
	 * @throws SystemException
	 *             If resultSetProxyFactoryForMySql is null (
	 *             {@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 * 
	 */
	public void setResultSetProxyForMySqlOption(final ResultSetProxyForMySqlOption resultSetProxyForMySqlOption) {

		if (resultSetProxyForMySqlOption == null) {
			String[] params = new String[] { ResultSetProxyForMySqlOption.class.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger.getInstance(this.getClass());
			initializationErrorLogger.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.resultSetProxyForMySqlOption = resultSetProxyForMySqlOption;
	}

}
