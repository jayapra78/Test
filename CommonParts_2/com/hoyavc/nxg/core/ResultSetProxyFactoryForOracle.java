/*
 * Copyright 2008 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2008/11/05 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2008/11/06 <Change NO.0002> Java Doc Added by Thangaraj S (SRM)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;

import java.sql.ResultSet;

import com.hoyavc.nxg.core.ResultSetProxyForOracle.ResultSetProxyForOracleOption;

/**
 * Factory class to create ResultSetProxyForOracle with ResultSet and
 * ResultSetProxyForOracleOption. {@link ResultSetProxyFactoryForOracle} is used
 * for handling LOB objects.
 */
class ResultSetProxyFactoryForOracle {
	

	/** ResultSet Proxy for option. */
	private ResultSetProxyForOracleOption resultSetProxyForOracleOption;


	
	/**
	 * To create the ResultSetProxyForOracle with ResultSet and
	 * ResultSetProxyForOracleOption {@link ResultSetProxyFactoryForOracle} is
	 * used for handling LOB objects.
	 * 
	 * @param sourceResultSet
	 *            ResultSet
	 * 
	 * @return ResultSetProxyForOracle
	 */
	ResultSetProxyForOracle create(final ResultSet sourceResultSet) {
		
		return new ResultSetProxyForOracle(sourceResultSet,
				resultSetProxyForOracleOption);
				
		
	}
	
	/**
	 * Getter method for resultSetProxyForOracleOption.
	 * 
	 * @return the ResultSetProxyForOracleOption
	 */

	public ResultSetProxyForOracleOption getResultSetProxyForOracleOption() {
		return resultSetProxyForOracleOption;
	}

	/**
	 * Setter method for resultSetProxyForOracleOption.
	 * 
	 * @param resultSetProxyForOracleOption
	 *            the ResultSetProxyForOracleOption to set
	 *            
	 * @throws SystemException
	 *             If resultSetProxyFactoryForOracle is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 *            
	 */
	public void setResultSetProxyForOracleOption(
			final ResultSetProxyForOracleOption resultSetProxyForOracleOption) {
		
		if (resultSetProxyForOracleOption == null) {
			String[] params = new String[] { ResultSetProxyForOracleOption.class
					.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		
		this.resultSetProxyForOracleOption = resultSetProxyForOracleOption;
	}


	
	

}
