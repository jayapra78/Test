/*
 * Copyright(C)2007-2011 Hoya corporation All rights reserved.
 *
 *  Change Tracking                                         
 *  2011/02/01 <Change NO.0001> created by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;

import java.sql.ResultSet;

import com.hoyavc.nxg.core.ResultSetProxyForSqlServer.ResultSetProxyForSqlServerOption;

/**
 * Factory class to create ResultSetProxyForSqlServer with ResultSet and
 * ResultSetProxyForSqlServerOption. {@link ResultSetProxyFactoryForSqlServer}
 * is used for handling LOB objects.
 */
class ResultSetProxyFactoryForSqlServer {

	/** ResultSet Proxy for option. */
	private ResultSetProxyForSqlServerOption resultSetProxyForSqlServerOption;

	/**
	 * To create the ResultSetProxyForSqlServer with ResultSet and
	 * ResultSetProxyForSqlServerOption
	 * {@link ResultSetProxyFactoryForSqlServer} is used for handling LOB
	 * objects.
	 * 
	 * @param sourceResultSet
	 *            ResultSet
	 * 
	 * @return ResultSetProxyForSqlServer
	 */
	ResultSetProxyForSqlServer create(final ResultSet sourceResultSet) {

		return new ResultSetProxyForSqlServer(sourceResultSet, resultSetProxyForSqlServerOption);

	}

	/**
	 * Getter method for resultSetProxyForSqlServerOption.
	 * 
	 * @return the ResultSetProxyForSqlServerOption
	 */

	public ResultSetProxyForSqlServerOption getResultSetProxyForSqlServerOption() {
		return resultSetProxyForSqlServerOption;
	}

	/**
	 * Setter method for resultSetProxyForSqlServerOption.
	 * 
	 * @param resultSetProxyForSqlServerOption
	 *            the ResultSetProxyForSqlServerOption to set
	 * 
	 * @throws SystemException
	 *             If resultSetProxyFactoryForSqlServer is null (
	 *             {@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001
	 *             fjff40001})
	 * 
	 */
	public void setResultSetProxyForSqlServerOption(
			final ResultSetProxyForSqlServerOption resultSetProxyForSqlServerOption) {

		if (resultSetProxyForSqlServerOption == null) {
			String[] params = new String[] { ResultSetProxyForSqlServerOption.class.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger.getInstance(this.getClass());
			initializationErrorLogger.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.resultSetProxyForSqlServerOption = resultSetProxyForSqlServerOption;
	}

}
