/*
 * Copyright 2006 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                             
 *  2010/06/07 <Change NO.0001> created by (SRM) 
 *  2010/08/10 <Change NO.0002> handleLob method updated to handle mysql BLOB by Kalai.P(SRM)
 *  2010/09/28 <Change NO.0003> handleLob method updated by Kalai.P (SRM) 
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00604;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00608;

import java.io.*;
import java.sql.*;

/**
 * Proxy class for MySQL ResultSet. This Class is used to retrieve the Blob and
 * LongText content before closing the connection.
 * 
 */
class ResultSetProxyForMySql {

	/** NXGLogger for logging messages. */
	private static final NXGLogger logger = DefaultLogger.getInstance(ResultSetProxyForMySql.class);

	/** ResultSet object. */
	private final ResultSet resultSet;

	/** ResultSet Proxy for option. */
	private final ResultSetProxyForMySqlOption resultSetProxyForMySqlOption;

	/**
	 * Inner class used for holding buffer size configuration.
	 * 
	 */
	static class ResultSetProxyForMySqlOption {

		/** Buffer size for reading Blob. */
		private int inputBlobBufferSize;

		/** Buffer size for reading LongText. */
		private int inputLongTextBufferSize;

		/**
		 * Getter method for inputBlobBufferSize.
		 * 
		 * @return the inputBlobBufferSize
		 */
		public int getInputBlobBufferSize() {
			return inputBlobBufferSize;
		}

		/**
		 * Setter method for inputBlobBufferSize.
		 * 
		 * @param inputBlobBufferSize
		 *            the inputBlobBufferSize to set
		 */
		public void setInputBlobBufferSize(final int inputBlobBufferSize) {
			this.inputBlobBufferSize = inputBlobBufferSize;
		}

		/**
		 * Getter method for inputLongTextBufferSize.
		 * 
		 * @return the inputLongTextBufferSize
		 */
		public int getInputLongTextBufferSize() {
			return inputLongTextBufferSize;
		}

		/**
		 * Setter method for inputLongTextBufferSize.
		 * 
		 * @param inputLongTextBufferSize
		 *            the inputLongTextBufferSize to set
		 */
		public void setInputLongTextBufferSize(final int inputLongTextBufferSize) {
			this.inputLongTextBufferSize = inputLongTextBufferSize;
		}

	};

	/**
	 * Constructor. Set Targeted ResultSet object into instance field. Retrieve
	 * the ResultSetProxyForMySqlOption instance (which holds buffer size
	 * information) from ConfigurationManager and set it into instance field.
	 * 
	 * @param resultSet
	 *            Target ResultSet object (This can be null if the method
	 *            "getObject" is not used.)
	 * @param resultSetProxyForMySqlOption
	 *            for Blob and LongText buffer option.
	 * 
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If resultSetProxyForMySqlOption is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 * 
	 * 
	 */
	ResultSetProxyForMySql(final ResultSet resultSet, final ResultSetProxyForMySqlOption resultSetProxyForMySqlOption) {

		if (resultSetProxyForMySqlOption == null) {
			String[] params = new String[] { ResultSetProxyForMySqlOption.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());

		}

		this.resultSet = resultSet;
		this.resultSetProxyForMySqlOption = resultSetProxyForMySqlOption;

	}

	/**
	 * Retrieves the value of the column from the ResultSet.
	 * 
	 * @param columnName
	 *            Column's name
	 * @return Value of the column from the ResultSet
	 * @throws SQLException
	 *             If {@link ResultSet#getObject(String)} throws SQLException.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If resultSet is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>if IOException is thrown while reading Blob content using
	 *             {@link ObjectInputStream} (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>If SQLException is thrown while reading Blob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             <li>If ClassNotFoundException is thrown (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>if IOException is thrown while reading LongText content
	 *             using {@link BufferedReader} (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>If SQLException is thrown while reading LongText content
	 *             ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             </ul>
	 */
	Object getObject(final String columnName) throws SQLException {

		// validating resultSet.
		if (resultSet == null) {
			String[] params = new String[] { ResultSet.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		Object object = resultSet.getObject(columnName);

		return handleLob(object, columnName);

	}

	/**
	 * If inputed object is LOB type then handle the LOB content and returns
	 * it. Otherwise return the inputed object as it is.
	 * 
	 * @param object
	 *            Target.
	 * @param columnName
	 *            Column's name
	 * @return If LOB type then return the LOB content. Otherwise return the
	 *         inputed object
	 */
	Object handleLob(final Object object, final String columnName) {
		if (object instanceof Blob) {
			return getBlob(object, columnName);
		} else if (object instanceof byte[]) {
			return getDeserializedObject(object, columnName);
		}

		return object;

	}

	/**
	 * Method to get Deserialized object.
	 * 
	 * @param obj
	 *            Object
	 * @param coulmnName
	 *            column name
	 * @return Blob column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>if IOException is thrown while reading Blob content using
	 *             {@link ObjectInputStream} (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})             
	 *             <li>If ClassNotFoundException is thrown (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             </ul>
	 */
	private Object getDeserializedObject(final Object obj, final String coulmnName) {

		if (obj == null) {
			return null;
		}

		ByteArrayInputStream bytesIn = null;
		ObjectInputStream objIn = null;

		byte[] byteArray = (byte[]) obj;
		if (byteArray.length == 0) {
			return null;
		}
		Object retobj = byteArray;
		if ((byteArray != null) && (byteArray.length >= 2)) {
			if ((byteArray[0] == -84) && (byteArray[1] == -19)) {
				// Serialized object?
				try {
					bytesIn = new ByteArrayInputStream(byteArray);
					objIn = new ObjectInputStream(bytesIn);
					retobj = objIn.readObject();

				} catch (ClassNotFoundException e) {
					Message message = fjfe00604.createMessage(new String[] { coulmnName, e.getMessage() });
					logger.error(message, e);
					throw new SystemException(message, e);
				} catch (IOException ex) {
					retobj = byteArray; // not serialized?
//					Message message = fjfe00604.createMessage(new String[] { coulmnName, e.getMessage() });
//					logger.error(message, e);
//					throw new SystemException(message, e);
				} finally {
					try {
						if (bytesIn != null) {
							bytesIn.close();
						}
						if (objIn != null) {
							objIn.close();
						}
					} catch (IOException e) {
						Message message = fjfe00604.createMessage(new String[] { coulmnName, e.getMessage() });
						logger.error(message, e);
						throw new SystemException(message, e);
					}

				}
			}
		}
		return retobj;
	}
	
	/**
	 * Method to get Blob object.
	 * 
	 * @param obj
	 *            Object
	 * @param coulmnName
	 *            column name
	 * @return Blob column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>if IOException is thrown while reading Blob content using
	 *             {@link ObjectInputStream} (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             <li>If SQLException is thrown while reading Blob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00608 fjfe00608})
	 *             <li>If ClassNotFoundException is thrown (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604 fjfe00604})
	 *             </ul>
	 */
	private Object getBlob(final Object obj, final String coulmnName) {

		if (obj == null) {
			return null;
		}

		ObjectInputStream ois = null;

		try {

			Blob blobObj = (Blob) obj;
			if (blobObj.length() == 0) {
				return null;
			}

			ois = new ObjectInputStream(new BufferedInputStream(blobObj.getBinaryStream(), resultSetProxyForMySqlOption
					.getInputBlobBufferSize()));
			return ois.readObject();

		} catch (IOException e) {
			Message message = fjfe00604.createMessage(new String[] { coulmnName, e.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);

		} catch (SQLException e) {

			Message message = fjfe00608.createMessage(new String[] { coulmnName, e.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);

		} catch (ClassNotFoundException e) {

			Message message = fjfe00604.createMessage(new String[] { coulmnName, e.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);
		} finally {
			try {
				if (ois != null) {
					ois.close();
				}
			} catch (IOException e) {
				Message message = fjfe00604.createMessage(new String[] { coulmnName, e.getMessage() });
				logger.error(message, e);
				throw new SystemException(message, e);
			}
		}

	}
}
