/*
 * Copyright(C)2007-2011 Hoya corporation All rights reserved.
 *
 *  Change Tracking                                         
 *  2011/02/01 <Change NO.0001> created by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.*;

import java.io.*;
import java.sql.*;

/**
 * Proxy class for SqlServer ResultSet. This Class is used to retrieve the Blob
 * and Clob content before closing the connection.
 * 
 */
class ResultSetProxyForSqlServer {

	/** NXGLogger for logging messages. */
	private static final NXGLogger logger = DefaultLogger.getInstance(ResultSetProxyForSqlServer.class);

	/** ResultSet object. */
	private final ResultSet resultSet;

	/** ResultSet Proxy for option. */
	private final ResultSetProxyForSqlServerOption resultSetProxyForSqlServerOption;

	/**
	 * Inner class used for holding buffer size configuration.
	 * 
	 */
	static class ResultSetProxyForSqlServerOption {

		/** Buffer size for reading Blob. */
		private int inputBlobBufferSize;

		/** Buffer size for reading Clob. */
		private int inputClobBufferSize;

		/**
		 * Getter method for inputBlobBufferSize.
		 * 
		 * @return the inputBlobBufferSize
		 */
		public int getInputBlobBufferSize() {
			return inputBlobBufferSize;
		}

		/**
		 * Setter method for inputBlobBufferSize.
		 * 
		 * @param inputBlobBufferSize
		 *            the inputBlobBufferSize to set
		 */
		public void setInputBlobBufferSize(final int inputBlobBufferSize) {
			this.inputBlobBufferSize = inputBlobBufferSize;
		}

		/**
		 * Getter method for inputClobBufferSize.
		 * 
		 * @return the inputClobBufferSize
		 */
		public int getInputClobBufferSize() {
			return inputClobBufferSize;
		}

		/**
		 * Setter method for inputClobBufferSize.
		 * 
		 * @param inputClobBufferSize
		 *            the inputClobBufferSize to set
		 */
		public void setInputClobBufferSize(final int inputClobBufferSize) {
			this.inputClobBufferSize = inputClobBufferSize;
		}

	};

	/**
	 * Constructor. Set Targeted ResultSet object into instance field. Retrieve
	 * the ResultSetProxyForSqlServerOption instance (which holds buffer size
	 * information) from ConfigurationManager and set it into instance field.
	 * 
	 * @param resultSet
	 *            Target ResultSet object (This can be null if the method
	 *            "getObject" is not used.)
	 * @param resultSetProxyForSqlServerOption
	 *            for Blob and Clob buffer option.
	 * 
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If resultSetProxyForSqlServerOption is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004
	 *             fjfe00004})
	 *             </ul>
	 * 
	 * 
	 */
	ResultSetProxyForSqlServer(final ResultSet resultSet,
			final ResultSetProxyForSqlServerOption resultSetProxyForSqlServerOption) {

		if (resultSetProxyForSqlServerOption == null) {
			String[] params = new String[] { ResultSetProxyForSqlServerOption.class.getName(), null };
			Message message = fjfe00004.createMessage(params);
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.resultSet = resultSet;
		this.resultSetProxyForSqlServerOption = resultSetProxyForSqlServerOption;

	}

	/**
	 * Retrieves the value of the column from the ResultSet.
	 * 
	 * @param columnName
	 *            Column's name
	 * @return Value of the column from the ResultSet
	 * @throws SQLException
	 *             If {@link ResultSet#getObject(String)} throws SQLException.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If resultSet is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004
	 *             fjfe00004})
	 *             <li>if IOException is thrown while reading Blob content using
	 *             {@link ObjectInputStream} (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604
	 *             fjfe00604})
	 *             <li>If SQLException is thrown while reading Blob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005
	 *             fjfe00005})
	 *             <li>If ClassNotFoundException is thrown (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604
	 *             fjfe00604})
	 *             <li>if IOException is thrown while reading Clob content using
	 *             {@link BufferedReader} (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604
	 *             fjfe00604})
	 *             <li>If SQLException is thrown while reading Clob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005
	 *             fjfe00005})
	 *             </ul>
	 */
	Object getObject(final String columnName) throws SQLException {

		// validating resultSet.
		if (resultSet == null) {
			String[] params = new String[] { ResultSet.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		Object object = resultSet.getObject(columnName);

		return handleLob(object, columnName);

	}

	/**
	 * If inputed object is LOB type then handle the LOB content and returns it.
	 * Otherwise return the inputed object as it is.
	 * 
	 * @param object
	 *            Target.
	 * @param columnName
	 *            Column's name
	 * @return If LOB type then return the LOB content. Otherwise return the
	 *         inputed object
	 */
	Object handleLob(final Object object, final String columnName) {
		if (object instanceof Blob) {
			return getBlob(object, columnName);
		} else if (object instanceof Clob) {
			return getClob(object, columnName);
		}

		return object;

	}

	/**
	 * Method to get Blob object.
	 * 
	 * @param obj
	 *            Object
	 * @param coulmnName
	 *            column name
	 * @return Blob column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>if IOException is thrown while reading Blob content using
	 *             {@link ObjectInputStream} (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604
	 *             fjfe00604})
	 *             <li>If SQLException is thrown while reading Blob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00608
	 *             fjfe00608})
	 *             <li>If ClassNotFoundException is thrown (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604
	 *             fjfe00604})
	 *             </ul>
	 */
	private Object getBlob(final Object obj, final String coulmnName) {

		if (obj == null) {
			return null;
		}

		ObjectInputStream ois = null;

		try {

			Blob blobObj = (Blob) obj;
			if (blobObj.length() == 0) {
				return null;
			}

			ois = new ObjectInputStream(new BufferedInputStream(blobObj.getBinaryStream(),
					resultSetProxyForSqlServerOption.getInputBlobBufferSize()));
			return ois.readObject();

		} catch (IOException e) {
			Message message = fjfe00604.createMessage(new String[] { coulmnName, e.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);

		} catch (SQLException e) {

			Message message = fjfe00608.createMessage(new String[] { coulmnName, e.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);

		} catch (ClassNotFoundException e) {

			Message message = fjfe00604.createMessage(new String[] { coulmnName, e.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);
		} finally {
			try {
				if (ois != null) {
					ois.close();
				}
			} catch (IOException e) {
				Message message = fjfe00604.createMessage(new String[] { coulmnName, e.getMessage() });
				logger.error(message, e);
				throw new SystemException(message, e);
			}
		}

	}

	/**
	 * Method to get Clob object Read the clob content and returns it as String.
	 * 
	 * @param obj
	 *            Object
	 * @param coulmnName
	 *            column name
	 * @return Clob column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>if IOException is thrown while reading Clob content using
	 *             {@link BufferedReader} (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00604
	 *             fjfe00604})
	 *             <li>If SQLException is thrown while reading Clob content (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00609
	 *             fjfe00609})
	 *             </ul>
	 */
	private String getClob(final Object obj, final String coulmnName) {

		if (obj == null) {
			return null;
		}

		BufferedReader br = null;

		try {
			br = new BufferedReader(((Clob) obj).getCharacterStream());

			StringWriter sw = new StringWriter();

			char[] charArrTemp = new char[resultSetProxyForSqlServerOption.getInputClobBufferSize()];

			int readCount = 0;
			while ((readCount = br.read(charArrTemp)) != -1) {
				sw.write(charArrTemp, 0, readCount);
			}

			return sw.toString();

		} catch (IOException e) {
			Message message = fjfe00607.createMessage(new String[] { coulmnName, e.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);

		} catch (SQLException e) {
			Message message = fjfe00609.createMessage(new String[] { coulmnName, e.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);

		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException e) {
				Message message = fjfe00607.createMessage(new String[] { coulmnName, e.getMessage() });
				logger.error(message, e);
				throw new SystemException(message, e);
			}
		}

	}
}
