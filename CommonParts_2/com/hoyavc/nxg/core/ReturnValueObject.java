/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;

import java.util.List;

/**
 * 
 * ReturnValueObject class that returned as a result for the execution of the
 * business logic. It stores ValueObject and WarningList.
 * <p>
 * <code> com.hoyavc.nxg.core.RequiresNewSessionBean</code> use this class to
 * wraps the resultant ValueObject & WarningList and pass back to the invoking
 * class.
 * </p>
 * 
 * 
 */
public class ReturnValueObject implements ValueObject {

	/** serial Id. */
	private static final long serialVersionUID = 1L;

	/** Warning list, business logic use to set the warn. */
	private List<Message> warningList;

	/** ValueObject. */
	private ValueObject vo;

	/**
	 * getter method for vo ValueObject.
	 * 
	 * @return vo
	 */
	public ValueObject getVo() {
		return vo;
	}

	/**
	 * setter method for vo ValueObject.
	 * 
	 * @param vo ValueObject
	 */
	void setVo(final ValueObject vo) {
		this.vo = vo;
	}

	/**
	 * getter method for the warning list.
	 * 
	 * @return warningList
	 */
	public List<Message> getWarningList() {
		return warningList;
	}

	/**
	 * setter method for warning list.
	 * 
	 * @param warningList WarningList
	 * @throws SystemException
	 *             if warningList is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 */
	void setWarningList(final List<Message> warningList) {

		// validating parameter
		if (warningList == null) {
			String[] params = new String[] { List.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			NXGLogger logger = DefaultLogger.getInstance(this.getClass());
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.warningList = warningList;
	}
}
