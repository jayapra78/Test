/*
 * Copyright 2013 by NXG Project, All rights reserved.
 *
 *  Change Tracking
 *  2013/10/03 <Change NO.0001> Created by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import java.io.Serializable;

/**
 * Class to have the Server information.
 */
public class ServerInfo implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** Current EAR version */
	private static String currentServerCorporationCode;

	/** Current EAR version */
	private static int currentServerVersion;
	
	/** NXG_S Version */
	public static final int NXG_S_VERSION = 10;
	
	/** VL_CORE STEP1.0 Version */
	public static final int VL_CORE_10_VERSION = 20;
	
	/** CORE_CORE FULL Version */
	public static final int CORE_CORE_VERSION = 30;
	
	public static String getCurrentServerCorporationCode() {
		return currentServerCorporationCode;
	}

	protected static void setCurrentServerCorporationCode(String currentServerCorporationCode) {
		ServerInfo.currentServerCorporationCode = currentServerCorporationCode;
	}

	public static int getCurrentServerVersion() {
		return currentServerVersion;
	}

	protected static void setCurrentServerVersion(int currentServerVersion) {
		ServerInfo.currentServerVersion = currentServerVersion;
	}
}
