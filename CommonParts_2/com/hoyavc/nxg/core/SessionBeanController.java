/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for the session bean controller classes, which uses session bean
 * invoker to invoke the session bean.
 */
public interface SessionBeanController {

	/**
	 * The implementation class uses SessionBeanInvoker to invoke the session
	 * bean.
	 * 
	 * @param cbsHandlerId
	 *            invoking CBS handler Id
	 * @param trxContext
	 *            TransactionContext
	 * @param valueObject ValueObject
	 * @return ValueObject
	 * @throws ApplicationException
	 *             business logic exceptions
	 */
	ValueObject invoke(String cbsHandlerId, TransactionContext trxContext,
			ValueObject valueObject) throws ApplicationException;

}
