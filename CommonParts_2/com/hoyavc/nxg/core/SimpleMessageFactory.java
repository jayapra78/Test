/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;

/**
 * SimpleMessageFactory to manage log message, It gets message using
 * {@link com.hoyavc.nxg.core.message.DefaultMessageManager DefaultMessageManager}.
 * 
 * @see InitializationErrorLogger
 * @see DefaultLogger
 * @see DefaultLogFormatter
 * @see InitializationErrorMessageFactory
 * @see MessageCode
 */
public final class SimpleMessageFactory {

	/** NXGLogger for logging messages. */
	private static final NXGLogger logger = DefaultLogger
			.getInstance(SimpleMessageFactory.class);

	/**
	 * DefaultLocaleKey.
	 */
	private static final String DEFAULT_LOCALE_KEY = "";

	/** Constructor. */
	private SimpleMessageFactory() {
	}

	/**
	 * Method to get the message for the MessageCode and localeKey.
	 * 
	 * @param messageCode
	 *            Message Code
	 * 
	 * @return Message contains MessageCode, message and MessageParam. In-case
	 *         of relevant message not found, it will return message as "".
	 * 
	 * @throws SystemException
	 *             If messageCode is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 * 
	 */
	public static Message getMessage(final MessageCode messageCode) {

		// validating parameter
		validateParams(messageCode);

		// getting the message using implementation class of MessageManager in
		// spring bean
		String message = getMessageManager().getMessage(messageCode,
				DEFAULT_LOCALE_KEY);

		// new instance of Message with messageCode and message
		return new Message(messageCode, message, null);
	}

	/**
	 * Method to get the message for the MessageCode and messageParam.
	 * 
	 * @param messageCode
	 *            Message Code
	 * 
	 * @param messageParam
	 *            Parameters for the message
	 * @return Message contains MessageCode and message. In-case of relevant
	 *         message not found, it will return message as "".
	 * @throws SystemException
	 *             If messageCode is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 * 
	 */
	public static Message getMessage(final MessageCode messageCode,
			final String[] messageParam) {

		// validating parameter
		if (messageParam == null || messageParam.length == 0) {
			return SimpleMessageFactory.getMessage(messageCode);
		}
		validateParams(messageCode);

		// getting the message using implementation class of MessageManager in
		// spring bean
		String message = getMessageManager().getMessage(messageCode,
				DEFAULT_LOCALE_KEY, messageParam);

		// new instance of Message with messageCode and message
		return new Message(messageCode, message, messageParam);

	}

	/**
	 * Method to get the DefaultMessageManager from spring bean.
	 * 
	 * @return MessageManager Instance of DefaultMessageManager
	 */
	private static MessageManager getMessageManager() {
		return ConfigurationManager
				.<MessageManager> getBean("defaultMessageManager");
	}

	/**
	 * Method to validating MessageCode.
	 * 
	 * @param messageCode
	 *            Message Code
	 * @throws SystemException
	 *             If messageCode is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 */
	private static void validateParams(final MessageCode messageCode) {

		if (messageCode == null) {
			String[] params = new String[] { MessageCode.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

	}
}
