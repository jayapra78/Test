/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import java.util.List;

/**
 * Interface for sql data access strategy.
 * 
 */
public interface SqlDataAccessStrategy {

	/**
	 * Method to select the records from the database tables by specifying the
	 * query string and SqlParameterList.
	 * 
	 * @param queryLanguage
	 *            Sql query String
	 * @param paramList
	 *            SqlParameterList
	 * @return List<SqlReturnMap> Return value list containing SqlReturnMap
	 */
	List<SqlReturnMap> select(final String queryLanguage,
			final SqlParameterList paramList);

	/**
	 * Method to update the records in the database table by specifying the query
	 * string and SqlParameterList.
	 * 
	 * @param queryLanguage
	 *            Sql query String
	 * @param paramList
	 *            SqlParameterList
	 * @return int Number of records updated successfully
	 */
	int update(final String queryLanguage,
			final SqlParameterList paramList);

	/**
	 * Method to create SqlParameterList from sqlParameterListFactory object.
	 * 
	 * @return SqlParameterList
	 */
	SqlParameterList createSqlParameterList();

	/**
	 * Method to select high volume data from database.
	 * 
	 * @param queryLanguage
	 *            Sql query
	 * @param paramList
	 *            List contains parameters for sql query.
	 * @param startRow
	 *            Start row to fetch
	 * @param noOfRows
	 *            Number of records to be retrieved from database.
	 * 
	 * @return List contains SqlReturnMap instances.
	 */
	HighVolumeDataResult selectHighVolumeData(
			final String queryLanguage, final SqlParameterList paramList,
			final int startRow, final int noOfRows);

}
