/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 *  2007/05/21 <Change NO.0003> setClob(String) is Added by Alex (SRM)
 *  2010/07/16 <Change NO.0004> setLongText(String) is Added by Kalai.P (SRM) 
 *  2011/01/18 <Change NO.0005> setString(final String str, boolean isSetNull) is Added by Kalai.P (SRM) 
 */
package com.hoyavc.nxg.core;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * Interface for sql parameter list classes.
 * 
 */
public interface SqlParameterList {

	/**
	 * Method to set String. If value of str is "" then this method will set
	 * value as null instead of "".
	 * 
	 * @param str
	 *            String
	 */
	void setString(final String str);
	
	/**
	 * Method to set String. If isSetNull is true and value of str is "" then
	 * this method will set value as null instead of "".
	 * 
	 * @param str
	 *            String
	 * @param isSetNull
	 * 			  boolean
	 */
	void setString(final String str, boolean isSetNull);

	/**
	 * Method to set Number.
	 * 
	 * @param num
	 *            Number
	 */
	void setNumber(final Number num);

	/**
	 * Method to set Date.
	 * 
	 * @param date
	 *            Date
	 */
	void setDate(final Date date);

	/**
	 * Method to set Date.
	 * 
	 * @param date
	 *            Date
	 */
	void setTimeStamp(final Date date);

	/**
	 * Method to set a Serializable Object.
	 * 
	 * @param object
	 *            Object
	 */
	void setBlob(final Serializable object);

	/**
	 * Method to set Clob content.
	 * 
	 * @param clobContent
	 *            Clob content
	 */
	void setClob(final String clobContent);
	
	/**
	 * Method to set LongText content.
	 * 
	 * @param longTextContent
	 *            LongText content
	 */
	void setLongText(final String longTextContent);

	/**
	 * Method to get object array of parameter list.
	 * 
	 * @return Object[]
	 */
	Object[] toArray();

	/**
	 * Returns a shallow copy of this SqlParameterList instance. The list should
	 * be protected but elements no need to be protected.
	 * 
	 * @return A clone of the SqlParameterList instance.
	 */
	SqlParameterList clone();

}
