/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for sql parameter list factory.
 */
public interface SqlParameterListFactory {

	/**
	 * Method to create SqlParameterList from sqlParameterListFactory object.
	 * 
	 * @return SqlParameterList
	 */
	SqlParameterList createSqlParameterList();
}
