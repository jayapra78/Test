/*
 * Copyright 2007-2013 by NXG Project, All rights reserved.
 *
 */
package com.hoyavc.nxg.core;

/**
 * A factory class for DB2 to create SqlParameterList.
 * 
 */
class SqlParameterListFactoryForDB2 implements SqlParameterListFactory {

	/**
	 * Method to create SqlParameterList from sqlParameterListFactory object.
	 * 
	 * @return SqlParameterList
	 */
	public SqlParameterList createSqlParameterList() {
		return new SqlParameterListForDB2();
	}

}
