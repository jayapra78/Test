/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2010/06/07 <Change NO.0001> created by (SRM) 
 */
package com.hoyavc.nxg.core;

/**
 * A factory class for mysql to create SqlParameterList.
 * 
 */
class SqlParameterListFactoryForMySql implements SqlParameterListFactory {

	/**
	 * Method to create SqlParameterList from sqlParameterListFactory object.
	 * 
	 * @return SqlParameterList
	 */
	public SqlParameterList createSqlParameterList() {
		return new SqlParameterListForMySql();
	}

}
