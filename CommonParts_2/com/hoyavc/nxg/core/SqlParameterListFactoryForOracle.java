/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * A factory class for oracle to create SqlParameterList.
 * 
 */
class SqlParameterListFactoryForOracle implements SqlParameterListFactory {

	/**
	 * Method to create SqlParameterList from sqlParameterListFactory object.
	 * 
	 * @return SqlParameterList
	 */
	public SqlParameterList createSqlParameterList() {
		return new SqlParameterListForOracle();
	}

}
