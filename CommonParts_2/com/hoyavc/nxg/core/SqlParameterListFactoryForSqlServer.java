/*
 * Copyright(C)2007-2011 Hoya corporation All rights reserved.
 *
 *  Change Tracking                                         
 *  2011/01/31 <Change NO.0001> created by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

/**
 * A factory class for SqlServer to create SqlParameterList.
 * 
 */
class SqlParameterListFactoryForSqlServer implements SqlParameterListFactory {

	/**
	 * Method to create SqlParameterList from sqlParameterListFactory object.
	 * 
	 * @return SqlParameterList
	 */
	public SqlParameterList createSqlParameterList() {
		return new SqlParameterListForSqlServer();
	}

}
