/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 *  2007/05/21 <Change NO.0004> setClob(String) is Added by Alex (SRM)  
 *  2008/11/07 <Change NO.0005> changed logger as static. by N.Fukuda (VCIT) 
 *  2010/07/16 <Change NO.0006> setLongText method added by Kalai.P [SRM] 
 *  2011/01/18 <Change NO.0007> setString(final String str, boolean isSetNull) is Added by Kalai.P (SRM) 
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00605;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00610;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00613;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class to manage parameter list for oracle.
 * 
 */
class SqlParameterListForOracle implements SqlParameterList, Cloneable {

	/** NXGLogger for logging messages. */
	private static final NXGLogger logger = DefaultLogger.getInstance(SqlParameterListForOracle.class);
	
	/** Object ArrayList. */
	private List<Object> list = new ArrayList<Object>();


	/**
	 * Method to set String.
	 * 
	 * @param str
	 *            String
	 */
	public void setString(final String str) {
		add(str);
	}
	

	/**
	 * Method to set String.
	 * Added for MySql.
	 * @param str
	 *            String
	 */
	public void setString(final String str, boolean isSetNull) {
		add(str);
	}

	/**
	 * Method to set Number.
	 * 
	 * @param num
	 *            Number
	 */
	public void setNumber(final Number num) {
		add(num);
	}

	/**
	 * Method to set Date.
	 * 
	 * @param date
	 *            Date
	 */
	public void setDate(final Date date) {
		if (date == null) {
			list.add(null);
			return;
		}
		add(new java.sql.Date(date.getTime()));

	}

	/**
	 * Method to set time stamp.
	 * 
	 * @param date
	 *            Date
	 */
	public void setTimeStamp(final Date date) {
		if (date == null) {
			list.add(null);
			return;
		}
		add(new java.sql.Timestamp(date.getTime()));
	}

	/**
	 * Method to set an Blob Object.
	 * 
	 * @param object
	 *            Serializable
	 * @throws SystemException
	 *             If IOException throws ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00605 fjfe00605})
	 */
	public void setBlob(final Serializable object) {
		if (object == null) {
			list.add(null);
			return;
		}

		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(bao);
			oos.writeObject(object);
			oos.flush();

		} catch (IOException e) {

			Message message = fjfe00605.createMessage(new String[] { e
					.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);
		} finally {

			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					Message message = fjfe00605.createMessage(new String[] { e
							.getMessage() });
					logger.error(message, e);
					throw new SystemException(message, e);
				}

			}
		}

		add(bao.toByteArray());
	}

	/**
	 * Method to set the Clob Content.
	 * 
	 * @param clobContent
	 *            Clob Content
	 * 
	 */
	public void setClob(final String clobContent) {
		add(clobContent);
	}
	
	/**
	 * Not Supported in Oracle.
	 * 
	 * @param longTextContent
	 *            column name
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>Not Supported ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00613 fjfe00613})
	 *             </ul>
	 */
	public void setLongText(final String longTextContent) {
		String[] param = {"LongText","Oracle"};
		Message message = fjfe00613.createMessage(param);
		logger.error(message);
		throw new SystemException(message);
	}

	/**
	 * Method to add the object to the list.
	 * 
	 * @param obj
	 *            Object
	 */
	private void add(final Object obj) {
		list.add(obj);
	}

	/**
	 * Method to set parameter list.
	 * In case of null, just return.
	 * 
	 * @param paramList Parameter list
	 */
	void setList(List<Object> paramList) {
		if(paramList == null) {
			return;
		}
		
		list.addAll(paramList);
	}
	
	/**
	 * Method to convert the list into Object array.
	 * 
	 * @return Object[] In-case of list is empty then null otherwise Object[]
	 *         will return
	 * 
	 */
	public Object[] toArray() {
		if (list.isEmpty()) {
			return null;
		}
		return list.toArray(new Object[0]);
	}

	/**
	 * Returns a shallow copy of this SqlParameterListForOracle instance. (The
	 * elements of the instance field list are not copied Since we never
	 * modifies the elements).
	 * 
	 * @see DbUtilDataAccessStrategyForOracle#addHighVolumeParamsToList
	 * 
	 * @return a clone of this SqlParameterListForOracle instance.
	 * 
	 * @throws SystemException
	 *             If CloneNotSupportedException is thrown ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00610 fjfe00610})
	 */
	@SuppressWarnings("unchecked")
	public SqlParameterList clone() {
		try {
			SqlParameterListForOracle cloneList = (SqlParameterListForOracle) super
					.clone();
			cloneList.list = (ArrayList<Object>) ((ArrayList) this.list)
					.clone();

			return cloneList;
		} catch (CloneNotSupportedException cloneEx) {
			Message message = fjfe00610.createMessage(new String[] { cloneEx
					.getMessage() });
			logger.error(message, cloneEx);
			throw new SystemException(message, cloneEx);
		}
	}

}
