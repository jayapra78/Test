/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 *  2007/05/21 <Change NO.0003> getClob(String) is Added by Alex (SRM)  
 *  2010/07/16 <Change NO.0004> getLongText(String) is Added by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

/**
 * Interface for sql return map classes.
 */
public interface SqlReturnMap {

	/**
	 * Method to get String.
	 * 
	 * @param str
	 *            column name
	 * @return String column value
	 */
	String getString(final String str);

	/**
	 * Method to get Number.
	 * 
	 * @param str
	 *            column name
	 * @return Number column value
	 */
	BigDecimal getNumber(final String str);

	/**
	 * Method to get Date.
	 * 
	 * @param str
	 *            column name
	 * @return Date column value
	 */
	Date getDate(final String str);

	/**
	 * Method to get TimeStamp.
	 * 
	 * @param str
	 *            column name
	 * @return TimeStamp column value
	 */
	Timestamp getTimeStamp(final String str);

	/**
	 * Method to get Blob object.
	 * 
	 * @param str
	 *            column name
	 * @return Blob column value
	 */
	Object getBlob(final String str);

	/**
	 * Method to get Clob content.
	 * 
	 * @param str
	 *            column name
	 * @return Clob column value
	 */
	String getClob(final String str);
	
	/**
	 * Method to get LongText
	 * 
	 * @param str
	 *            column name
	 * @return LongText column value
	 */
	String getLongText(final String str);

	/**
	 * Method to get keys (Column names) as set.
	 * 
	 * @return Set
	 */
	Set keySet();
}
