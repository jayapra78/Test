/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 *  2007/05/21 <Change NO.0004> getClob(String) is Added And ResultSet Proxy is used for handling LOB objects is
 *  							Added by Alex (SRM) 
 *  2007/08/13 <Change NO.0005> getTimeStamp(String) also returns value in case object is instanceof java.sql.Timestamp
 *  							Added by T.Mitsuhashi (VCIT) 
 *  2008/11/07 <Change NO.0006> changed logger as static. by N.Fukuda (VCIT) 
 *  2010/07/16 <Change NO.0007> getLongText method added by Kalai.P [SRM]
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00601;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00602;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00603;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00606;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00613;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Sql result map class for oracle, it stores the result set as a Map<String,
 * Object> format.
 */
class SqlReturnMapForOracle implements SqlReturnMap {

	/** Oracle TimeStamp class. */
	private static String TIME_STAMP_ORACLE_CLASS = "oracle.sql.TIMESTAMP";

	/** Oracle TimeStamp Method. */
	private static String TIME_STAMP_ORACLE_METHOD = "timestampValue";

	/** NXGLogger for logging messages. */
	private static final NXGLogger logger = DefaultLogger.getInstance(SqlReturnMapForOracle.class);

	
	/** Result map variable. */
	private final Map map;

	/**
	 * Constructor with result map as argument.
	 * 
	 * @param map
	 *            result map
	 * 
	 * @throws SystemException
	 *             If map is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00606 fjfe00606})
	 */
	SqlReturnMapForOracle(final Map map) {
		if (map == null) {
			String[] params = new String[] { Map.class.getName() };
			Message message = fjfe00606.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.map = map;
	}

	/**
	 * Method to get String.
	 * 
	 * @param str
	 *            column name
	 * @return String column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If object not found for the given str ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601 fjfe00601})
	 *             </ul>
	 */
	public String getString(final String str) {
		Object obj = get(str);

		// temporary
		if (obj == null) {
			return null;
		}

		if (obj instanceof String) {
			return (String) obj;
		} else {
			return obj.toString();
		}

	}

	/**
	 * Method to get Number.
	 * 
	 * @param str
	 *            column name
	 * @return Number column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If object not found for the given str ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601 fjfe00601})
	 *             <li>If the object is not a BigNumber type ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601 fjfe00601})
	 *             </ul>
	 */
	public BigDecimal getNumber(final String str) {
		Object obj = get(str);

		// temporary
		if (obj == null) {
			return null;
		}

		if (obj instanceof BigDecimal) {
			return (BigDecimal) obj;
		} else {
			Message message = fjfe00601.createMessage(new String[] { str });
			logger.error(message, new ClassCastException());
			throw new SystemException(message, new ClassCastException());
		}
	}

	/**
	 * Method to get Date.
	 * 
	 * @param str
	 *            column name
	 * @return Date column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If object not found for the given str ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601 fjfe00601})
	 *             <li>If the object is not an instance of Date type ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00602 fjfe00602})
	 *             </ul>
	 */
	public Date getDate(final String str) {
		Object obj = get(str);

		// temporary
		if (obj == null) {
			return null;
		}

		if (obj instanceof Date) {
			return (Date) obj;
		} else {
			Message message = fjfe00602.createMessage(new String[] { str,
					Date.class.getName(), obj.getClass().getName() });
			logger.error(message, new ClassCastException());
			throw new SystemException(message, new ClassCastException());
		}
	}

	/**
	 * Method to get TimeStamp.
	 * 
	 * @param str
	 *            column name
	 * @return TimeStamp column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If object not found for the given str ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601 fjfe00601})
	 *             <li>If SystemException throws by {@link ReflectionHelper}
	 *             <li>If ClassNotFoundException throws when casting Timestamp
	 *             object ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00603 fjfe00603})
	 *             <li>If any Throwable exception throws ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00603 fjfe00603})
	 *             </ul>
	 */
	public Timestamp getTimeStamp(final String str) {
		Object obj = get(str);

		// temporary
		if (obj == null) {
			return null;
		}

		try {
			if (Class.forName(TIME_STAMP_ORACLE_CLASS).isAssignableFrom(
					obj.getClass())) {
				Method method = ReflectionHelper.getMethod(obj.getClass(),
						TIME_STAMP_ORACLE_METHOD, new Class[0]);
				return (Timestamp) ReflectionHelper.invokeMethod(method, obj,
						null);
			} else if (obj instanceof Timestamp) {
				return (Timestamp) obj;
			}
		} catch (SystemException s) {
			throw s;

		} catch (ClassNotFoundException e) {
			Message message = fjfe00603.createMessage(new String[] { e
					.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);

		} catch (Throwable e) {
			Message message = fjfe00603.createMessage(new String[] { e
					.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);

		}

		Message message = fjfe00602.createMessage(new String[] { str,
				Timestamp.class.getName(), obj.getClass().getName() });
		logger.error(message);
		throw new SystemException(message, null);
	}

	/**
	 * Method to get Blob object {@link ResultSetProxyForOracle#getBlob} is
	 * used or reading Blob content from ResultSet.
	 * 
	 * @param str
	 *            column name
	 * @return Blob column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If object not found for the given str ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601 fjfe00601})
	 *             </ul>
	 */
	public Object getBlob(final String str) {
		return get(str);

	}

	/**
	 * Method to get Clob object. {@link ResultSetProxyForOracle#getClob} is
	 * used or reading Clob content from ResultSet.
	 * 
	 * @param str
	 *            column name
	 * @return Clob column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If object not found for the given str ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601 fjfe00601})
	 *             </ul>
	 */
	public String getClob(final String str) {
		return getString(str);

	}
	
	/**
	 * Not Supported in Oracle.
	 * 
	 * @param str
	 *            column name
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>Not Supported ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00613 fjfe00613})
	 *             </ul>
	 */
	public String getLongText(final String str) {
		String[] param = {"LongText","Oracle"};
		Message message = fjfe00613.createMessage(param);
		logger.error(message);
		throw new SystemException(message);
	}

	/**
	 * Method to get keys (Column names) as set.
	 * 
	 * @return Set
	 * 
	 */
	public Set keySet() {
		return map.keySet();
	}

	/**
	 * Method to get object from the map for given string.
	 * 
	 * @param str
	 *            String
	 * @return Object, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If object not found for the given str ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601 fjfe00601})
	 *             </ul>
	 */
	private Object get(final String str) {

		if (str == null || str.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(), str };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (!map.containsKey(str.toUpperCase())) {
			String[] params = new String[] { str };
			Message message = fjfe00601.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		return map.get(str.toUpperCase());
	}
}
