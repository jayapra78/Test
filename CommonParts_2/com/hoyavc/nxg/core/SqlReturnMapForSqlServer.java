/*
 * Copyright(C)2007-2011 Hoya corporation All rights reserved.
 *
 *  Change Tracking                                         
 *  2011/01/31 <Change NO.0001> created by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00601;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00602;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00606;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00613;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Sql result map class for SqlServer, it stores the result set as a Map<String,
 * Object> format.
 */
class SqlReturnMapForSqlServer implements SqlReturnMap {

	/** NXGLogger for logging messages. */
	private static final NXGLogger logger = DefaultLogger.getInstance(SqlReturnMapForSqlServer.class);

	/** Result map variable. */
	private final Map map;

	/**
	 * Constructor with result map as argument.
	 * 
	 * @param map
	 *            result map
	 * 
	 * @throws SystemException
	 *             If map is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00606
	 *             fjfe00606})
	 */
	SqlReturnMapForSqlServer(final Map map) {
		if (map == null) {
			String[] params = new String[] { Map.class.getName() };
			Message message = fjfe00606.createMessage(params);
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.map = map;
	}

	/**
	 * Method to get String.
	 * 
	 * @param str
	 *            column name
	 * @return String column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004
	 *             fjfe00004})
	 *             <li>If object not found for the given str (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601
	 *             fjfe00601})
	 *             </ul>
	 */
	public String getString(final String str) {
		Object obj = get(str);

		// temporary
		if (obj == null) {
			return null;
		}

		if (obj instanceof String) {
			return (String) obj;
		} else {
			return obj.toString();
		}

	}

	/**
	 * Method to get Number.
	 * 
	 * @param str
	 *            column name
	 * @return Number column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004
	 *             fjfe00004})
	 *             <li>If object not found for the given str (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601
	 *             fjfe00601})
	 *             <li>If the object is not a BigNumber type (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601
	 *             fjfe00601})
	 *             </ul>
	 */
	public BigDecimal getNumber(final String str) {
		Object obj = get(str);

		// temporary
		if (obj == null) {
			return null;
		}
		if (obj instanceof Integer) {
			return new BigDecimal((Integer) obj);
		} else if (obj instanceof Long) {
			return new BigDecimal((Long) obj);
		} else if (obj instanceof BigInteger) {
			return new BigDecimal((BigInteger) obj);
		} else if (obj instanceof BigDecimal) {
			return (BigDecimal) obj;
		} else if (obj instanceof Float) {
			return new BigDecimal((Float) obj);
		} else if (obj instanceof Double) {
			return new BigDecimal((Double) obj);
		} else if (obj instanceof String) {
			return new BigDecimal((String) obj);
		} else {
			Message message = fjfe00601.createMessage(new String[] { str });
			logger.error(message, new ClassCastException());
			throw new SystemException(message, new ClassCastException());
		}
	}

	/**
	 * Method to get Date.
	 * 
	 * @param str
	 *            column name
	 * @return Date column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004
	 *             fjfe00004})
	 *             <li>If object not found for the given str (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601
	 *             fjfe00601})
	 *             <li>If the object is not an instance of Date type (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00602
	 *             fjfe00602})
	 *             </ul>
	 */
	public Date getDate(final String str) {
		Object obj = get(str);

		// temporary
		if (obj == null) {
			return null;
		}

		if (obj instanceof Date) {
			return (Date) obj;
		} else {
			try {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date retDate = df.parse(obj.toString());
				return retDate;
			} catch (Exception e) {
				Message message = fjfe00602.createMessage(new String[] { str, Date.class.getName(),
						obj.getClass().getName() });
				logger.error(message, new ClassCastException());
				throw new SystemException(message, new ClassCastException());
			}
		}
	}

	/**
	 * Method to get TimeStamp (DateTime).
	 * 
	 * @param str
	 *            column name
	 * @return TimeStamp column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null return null
	 *             <li>If object not found for the given str (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601
	 *             fjfe00601})
	 *             </ul>
	 */
	public Timestamp getTimeStamp(final String str) {
		Object obj = get(str);

		// temporary
		if (obj == null) {
			return null;
		}

		if (obj instanceof Timestamp) {
			return (Timestamp) obj;
		} else {
			Message message = fjfe00602.createMessage(new String[] { str, Date.class.getName(),
					obj.getClass().getName() });
			logger.error(message, new ClassCastException());
			throw new SystemException(message, new ClassCastException());
		}

	}

	/**
	 * Method to get Blob object {@link ResultSetProxyForSqlServer#getBlob} is used
	 * or reading Blob content from ResultSet.
	 * 
	 * @param str
	 *            column name
	 * @return Blob column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004
	 *             fjfe00004})
	 *             <li>If object not found for the given str (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601
	 *             fjfe00601})
	 *             </ul>
	 */
	public Object getBlob(final String str) {
		return get(str);
	}

	/**
	 * Method to get Clob object. {@link ResultSetProxyForSqlServer#getClob} is
	 * used or reading Clob content from ResultSet.
	 * 
	 * @param str
	 *            column name
	 * @return Clob column value, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004
	 *             fjfe00004})
	 *             <li>If object not found for the given str (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601
	 *             fjfe00601})
	 *             </ul>
	 */
	public String getClob(final String str) {
		return getString(str);
	}

	/**
	 * Not Supported in SQLServer.
	 * 
	 * @param str
	 *            column name
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>Not Supported (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00613
	 *             fjfe00613})
	 *             </ul>
	 */
	public String getLongText(final String str) {
		String[] param = { "LongText", "SQLServer" };
		Message message = fjfe00613.createMessage(param);
		logger.error(message);
		throw new SystemException(message);
	}

	/**
	 * Method to get keys (Column names) as set.
	 * 
	 * @return Set
	 * 
	 */
	public Set keySet() {
		return map.keySet();
	}

	/**
	 * Method to get object from the map for given string.
	 * 
	 * @param str
	 *            String
	 * @return Object, In-case not found it will return null.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004
	 *             fjfe00004})
	 *             <li>If object not found for the given str (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00601
	 *             fjfe00601})
	 *             </ul>
	 */
	private Object get(final String str) {

		if (str == null || str.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(), str };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (!map.containsKey(str.toUpperCase())) {
			String[] params = new String[] { str };
			Message message = fjfe00601.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		return map.get(str.toUpperCase());
	}
}
