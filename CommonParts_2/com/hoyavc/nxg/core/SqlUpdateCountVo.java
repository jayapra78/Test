/*
 * Copyright 2006 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/05/31 <Change NO.0001> created by Alex (SRM) 
 */
package com.hoyavc.nxg.core;

/**
 * Value object holds information about number of rows affected by execution of
 * INSERT / UPDATE /DELETE SQL.
 * 
 */
public class SqlUpdateCountVo implements ValueObject {

	/** use serialVersionUID from JDK 1.0.2 for interoperability. */
	private static final long serialVersionUID = 1L;

	/** number of rows affected by execution of INSERT / UPDATE /DELETE SQL. */
	private int count;

	/**
	 * Constructor.
	 */
	public SqlUpdateCountVo() {

	}

	/**
	 * Setter method for count.
	 * 
	 * @param count
	 *            Number of rows affected.
	 */
	public void setCount(final int count) {
		this.count = count;
	}

	/**
	 * Getter method for count.
	 * 
	 * @return Number of rows affected.
	 */
	public int getCount() {
		return count;
	}
}
