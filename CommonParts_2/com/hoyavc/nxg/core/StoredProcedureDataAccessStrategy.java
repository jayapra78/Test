/*
 * Copyright 2006 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/05/17 <Change NO.0001> created by Alex (SRM) 
 *  2007/05/21 <Change NO.0002> Added JavaDoc by Alex (SRM)  
 */
package com.hoyavc.nxg.core;

/**
 * Interface for stored procedure access strategy.
 * 
 */
public interface StoredProcedureDataAccessStrategy {

	/**
	 * Method to create StoredProcedureParameterList.
	 * 
	 * @return StoredProcedureParameterList
	 */
	StoredProcedureParameterList createStoredProcedureParameterList();

	/**
	 * Method to execute stored procedures.
	 * 
	 * @param sql
	 *            Sql Query for calling stored procedure.
	 * @param paramList
	 *            StoredProcedureParameterList
	 * @return StoredProcedureReturnMap
	 */
	StoredProcedureReturnMap callProcedure(final String sql,
			final StoredProcedureParameterList paramList);

}
