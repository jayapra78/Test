/*
 * Copyright 2006 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/05/17 <Change NO.0001> created by Alex (SRM) 
 *  2007/05/21 <Change NO.0002> Added JavaDoc by Alex (SRM)  
 *  2008/11/07 <Change NO.0003> resultSetProxyFactoryForOracle has been added for perfomance improvement by N.Fukuda (VCIT) 
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00005;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00651;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.ResultSetHandler;

import com.hoyavc.nxg.core.StoredProcedureParameterList.StoredProcedureInOutParam;
import com.hoyavc.nxg.core.StoredProcedureParameterList.StoredProcedureInParam;
import com.hoyavc.nxg.core.StoredProcedureParameterList.StoredProcedureOutParam;
import com.hoyavc.nxg.core.StoredProcedureParameterList.StoredProcedureParam;

/**
 * Implementation of StoredProcedureDataAccessStrategy interface. Executes the
 * stored procedure and returns the result as {@link StoredProcedureReturnMap}.
 * 
 */
class StoredProcedureDataAccessStrategyForOracle implements
		StoredProcedureDataAccessStrategy {

	/** database connection manager for DAO. */
	private DBConnectionManager dbConnectionManager;

	/** ResultSetHandler. */
	private ResultSetHandler resultSetHandler;

	/** NXGLogger for logging messages. */
	private final NXGLogger logger = DefaultLogger.getInstance(this.getClass());
	
	
	/** ResultSet Proxy Factory for oracle. */
	private ResultSetProxyFactoryForOracle resultSetProxyFactoryForOracle;

	
	/**
	 * Method to execute stored procedure with inputed parameter list and
	 * returns the result of stored procedure.
	 * 
	 * @param sql
	 *            Sql query for stored procedure.
	 * @param paramList
	 *            StoredProcedureParameterList
	 * 
	 * @return StoredProcedureReturnMap Map contains result of stored procedure.
	 * @throws SystemException
	 *             any of the following cases
	 *             <ul>
	 *             <li>Sql query is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>SQLException occurs while executing stored procedure ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00651 fjfe00651})
	 *             <li>SQLException occurs while closing the connection ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 *             </ul>
	 * 
	 */
	public StoredProcedureReturnMap callProcedure(final String sql,
			final StoredProcedureParameterList paramList) {

		// validating parameter
		if (sql == null || sql.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(), sql };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		StoredProcedureReturnMap result = null;
		Connection conn = null;

		try {
			conn = dbConnectionManager.getConnection();

			CallableStatement cstmt = conn.prepareCall(sql);

			setInOutParameters(cstmt, paramList);

			cstmt.execute();

			result = processProcedureResult(cstmt, paramList);

		} catch (SQLException sqlEx) {
			Message message = fjfe00651.createMessage(new String[] {
					sqlEx.getMessage(), sql, String.valueOf(paramList) });
			logger.error(message, sqlEx);
			throw new SystemException(message, sqlEx);
		} finally {
			closeConnection(conn);
			conn = null;

		}

		return result;

	}

	/**
	 * Sets the value of the designated parameters. If the parameter is IN
	 * parameter it sets the value of the designated parameter. If the parameter
	 * is OUT parameter it registers the SQL type of the designated parameter.
	 * If the parameter is INOUT parameter it sets the value of the designated
	 * parameter and registers the SQL type of the designated parameter.
	 * 
	 * @param cstmt
	 *            CallableStatement object
	 * @param paramList
	 *            StoredProcedureParameterList
	 * @throws SQLException
	 *             If CallableStatement#setXXX() or registerOutParameter()
	 *             throws SQLException.
	 * 
	 */
	private void setInOutParameters(final CallableStatement cstmt,
			final StoredProcedureParameterList paramList) throws SQLException {

		if (paramList == null || paramList.getParamList().size() == 0) {
			return;
		}

		int parameterIndex = 1;

		for (StoredProcedureParam param : paramList.getParamList()) {
			if (param instanceof StoredProcedureInParam) {

				StoredProcedureInParam inParam = (StoredProcedureInParam) param;
				setInputParameters(cstmt, parameterIndex, inParam);
			} else if (param instanceof StoredProcedureOutParam) {

				StoredProcedureOutParam outParam = (StoredProcedureOutParam) param;
				cstmt.registerOutParameter(parameterIndex, outParam
						.getSqlType());
			} else if (param instanceof StoredProcedureInOutParam) {

				StoredProcedureInOutParam inOutParam = (StoredProcedureInOutParam) param;
				setInputParameters(cstmt, parameterIndex, inOutParam
						.getInParam());
				cstmt.registerOutParameter(parameterIndex, inOutParam
						.getOutParam().getSqlType());
			}

			parameterIndex++;
		}

	}

	/**
	 * Sets the value of the designated parameters. If value is null then set
	 * null with appropriate sql type.
	 * 
	 * @param cstmt
	 *            CallableStatement object
	 * @param parameterIndex
	 *            Parameter index
	 * @param inParam
	 *            StoredProcedureInParam
	 * @throws SQLException
	 *             If CallableStatement#setXXX() throws SQLException.
	 */
	private void setInputParameters(final CallableStatement cstmt,
			final int parameterIndex, final StoredProcedureInParam inParam)
			throws SQLException {
		if (inParam.getValue() != null) {
			cstmt.setObject(parameterIndex, inParam.getValue());
		} else {
			cstmt.setNull(parameterIndex, inParam.getSqlType());
		}
	}

	/**
	 * Process the results of stored procedure execution. It creates Map and
	 * assign the values which are retrieved from ResultSet. Map is assigned to
	 * {@link SqlReturnMapForOracle}. If value retrieved from the ResultSet is
	 * ResultSet Object then it process the ResultSet object by using
	 * {@link ResultSetHandler} and assign the result (List contains
	 * {@link SqlReturnMap}) into parent map.
	 * 
	 * @param cstmt
	 *            CallableStatement object
	 * @param paramList
	 *            StoredProcedureParameterList
	 * 
	 * @return StoredProcedureReturnMap contains {@link SqlReturnMap}. If
	 *         procedure OUT/INOUT argument is cursor then it may contain
	 *         {@link SqlReturnMap} as child map.
	 * 
	 * @throws SQLException
	 *             If CallableStatement#getXXX() or
	 *             {@link ResultSetHandler#handle(ResultSet)}throws
	 *             SQLException.
	 * 
	 */
	@SuppressWarnings("unchecked")
	private StoredProcedureReturnMap processProcedureResult(
			final CallableStatement cstmt,
			final StoredProcedureParameterList paramList) throws SQLException {

		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, List<SqlReturnMap>> cursorMap = new HashMap<String, List<SqlReturnMap>>();

		if (paramList == null || paramList.getParamList().size() == 0) {
			return new StoredProcedureReturnMapForOracle(
					new SqlReturnMapForOracle(result), cursorMap);
		}

		int parameterIndex = 1;

		final ResultSetProxyForOracle resultSetProxyForOracle = resultSetProxyFactoryForOracle
				.create(null);
		for (StoredProcedureParam param : paramList.getParamList()) {

			if (param instanceof StoredProcedureOutParam
					|| param instanceof StoredProcedureInOutParam) {
				StoredProcedureOutParam paramOutVo = null;

				if (param instanceof StoredProcedureOutParam) {
					paramOutVo = (StoredProcedureOutParam) param;
				} else {
					paramOutVo = ((StoredProcedureInOutParam) param)
							.getOutParam();
				}

				Object object = cstmt.getObject(parameterIndex);

				if (object instanceof ResultSet) {
					List<SqlReturnMap> cursorResultList = (List<SqlReturnMap>) resultSetHandler
							.handle((ResultSet) object);
					cursorMap.put(paramOutVo.getParamName().toUpperCase(),
							cursorResultList);
				} else {
					result.put(paramOutVo.getParamName().toUpperCase(),
							resultSetProxyForOracle.handleLob(object, paramOutVo
									.getParamName()));
				}
			}

			parameterIndex++;
		}

		StoredProcedureReturnMap resultMap = new StoredProcedureReturnMapForOracle(
				new SqlReturnMapForOracle(result), cursorMap);

		return resultMap;
	}

	/**
	 * Method to create StoredProcedureParameterList.
	 * 
	 * @return StoredProcedureParameterList
	 */
	public StoredProcedureParameterList createStoredProcedureParameterList() {
		return new StoredProcedureParameterListForOracle();
	}

	/**
	 * Method to close the database connection.
	 * 
	 * @param con
	 *            Database Connection
	 * @throws SystemException
	 *             if any SQLException ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00005 fjfe00005})
	 */
	private void closeConnection(final Connection con) {

		if (con != null) {
			try {
				con.close();
			} catch (SQLException se) {
				Message message = fjfe00005.createMessage(new String[] { se
						.getMessage() });

				logger.error(message, se);
				throw new SystemException(message, se);
			}
		}
	}

	/**
	 * getter method for DBConnectionManager.
	 * 
	 * @return DBConnectionManager
	 */
	public DBConnectionManager getDbConnectionManager() {
		return dbConnectionManager;
	}

	/**
	 * setter method for DBConnectionManager.
	 * 
	 * @param dbConnectionManager
	 *            DBConnectionManager
	 * @throws SystemException
	 *             if dbConnectionManager is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setDbConnectionManager(
			final DBConnectionManager dbConnectionManager) {

		if (dbConnectionManager == null) {
			String[] params = new String[] { DBConnectionManager.class
					.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		this.dbConnectionManager = dbConnectionManager;
	}

	/**
	 * getter method for ResultSetHandler.
	 * 
	 * @return resultSetHandler ResultSetHandler
	 */
	public ResultSetHandler getResultSetHandler() {
		return resultSetHandler;
	}

	/**
	 * setter method for ResultSetHandler.
	 * 
	 * @param resultSetHandler
	 *            Result Set Handler
	 * @throws SystemException
	 *             if resultSetHandler is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setResultSetHandler(final ResultSetHandler resultSetHandler) {

		if (resultSetHandler == null) {
			String[] params = new String[] { ResultSetHandler.class.getName() };
			Message message = fjff40001.createMessage(params);
			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		this.resultSetHandler = resultSetHandler;
	}
	
	/**
	 * Getter method for resultSetProxyFactoryForOracle.
	 * 
	 * @return the ResultSetProxyFactoryForOracle
	 */

	public ResultSetProxyFactoryForOracle getResultSetProxyFactoryForOracle() {
		return resultSetProxyFactoryForOracle;
	}

	/**
	 * Setter method for resultSetProxyFactoryForOracle.
	 * 
	 * @param resultSetProxyFactoryForOracle
	 *            the ResultSetProxyFactoryForOracle to set
	 * @throws SystemException
	 *             if resultSetProxyFactoryForOracle is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setResultSetProxyFactoryForOracle(
			final ResultSetProxyFactoryForOracle resultSetProxyFactoryForOracle) {
		
		if (resultSetProxyFactoryForOracle == null) {
			String[] params = new String[] { ResultSetProxyFactoryForOracle.class
					.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		this.resultSetProxyFactoryForOracle = resultSetProxyFactoryForOracle;
	}

}
