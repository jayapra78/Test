/*
 * Copyright 2006 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                                
 *  2007/05/17 <Change NO.0001> created by Alex (SRM) 
 *  2007/05/21 <Change NO.0002> Added JavaDoc by Alex (SRM)
 *  2010/07/20 <Change NO.0004> LongText methods added by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 
 * Interface for stored procedure parameter list.
 * 
 */
public interface StoredProcedureParameterList {

	/**
	 * Method to set StoredProcedure's IN Numeric parameter.
	 * 
	 * @param num
	 *            Number
	 */
	void setInNumber(final Number num);

	/**
	 * Method to set StoredProcedure's IN char, varchar parameter.
	 * 
	 * @param str
	 *            String
	 */
	void setInString(final String str);

	/**
	 * Method to set StoredProcedure's IN Date parameter.
	 * 
	 * @param date
	 *            Date
	 */
	void setInDate(final Date date);

	/**
	 * Method to set StoredProcedure's IN TimeStamp parameter.
	 * 
	 * @param date
	 *            Date
	 */
	void setInTimeStamp(final Date date);

	/**
	 * Method to set StoredProcedure's IN Blob parameter.
	 * 
	 * @param object
	 *            Serializable Object
	 */
	void setInBlob(final Serializable object);

	/**
	 * Method to set StoredProcedure's IN Clob parameter.
	 * 
	 * @param clobContent
	 *            Clob content as String
	 */
	void setInClob(final String clobContent);
	
	/**
	 * Method to set StoredProcedure's IN LongText parameter.
	 * 
	 * @param longText
	 *            LongText content as String
	 */
	void setInLongText(final String longText);

	/**
	 * Method to set StoredProcedure's OUT Numeric parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 */
	void setOutNumber(final String paramName);

	/**
	 * Method to set StoredProcedure's OUT char, varchar parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 */
	void setOutString(final String paramName);

	/**
	 * Method to set StoredProcedure's OUT Date parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 */
	void setOutDate(final String paramName);

	/**
	 * Method to set StoredProcedure's OUT TimeStamp parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 */
	void setOutTimeStamp(final String paramName);

	/**
	 * Method to set StoredProcedure's OUT Blob parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 */
	void setOutBlob(final String paramName);

	/**
	 * Method to set StoredProcedure's OUT Clob parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 */
	void setOutClob(final String paramName);
	
	/**
	 * Method to set StoredProcedure's OUT LongText parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 */
	void setOutLongText(final String paramName);

	/**
	 * Method to set StoredProcedure's IN OUT Numeric parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * @param num
	 *            Number
	 */
	void setInOutNumber(final String paramName, final Number num);

	/**
	 * Method to set StoredProcedure's IN OUT char, varchar parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * @param str
	 *            String
	 */
	void setInOutString(final String paramName, final String str);

	/**
	 * Method to set StoredProcedure's IN OUT Date parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * @param date
	 *            Date
	 */
	void setInOutDate(final String paramName, final Date date);

	/**
	 * Method to set StoredProcedure's IN OUT TimeStamp parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * @param date
	 *            Date
	 */
	void setInOutTimeStamp(final String paramName, final Date date);

//	/**
//	 * Method to set StoredProcedure's IN OUT Blob parameter.
//	 * 
//	 * @param paramName
//	 *            StoredProcedure Parameter name
//	 * @param object
//	 *            Serializable Object
//	 */
//	 // JDBC Oracle stored procedure Blob IN OUT parameter problem (java.sql.SQLException: Parameter Type Conflict).
//	 // For time being it is commented out.
//	void setInOutBlob(final String paramName, final Serializable object);

	/**
	 * Method to set StoredProcedure's IN OUT Clob parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * @param clobContent
	 *            Clob content as String
	 */
	void setInOutClob(final String paramName, final String clobContent);
	
	/**
	 * Method to set StoredProcedure's IN OUT LongText parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * @param longText
	 *            LongText content as String
	 */
	void setInOutLongText(final String paramName, final String longText);

	/**
	 * Method to set StoredProcedure's Oracle Cursor parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 */
	void setCursor(final String paramName);

	/**
	 * To get the stored procedure parameter list.
	 * 
	 * @return List contains StoredProcedureParam instances.
	 */
	List<StoredProcedureParam> getParamList();

	/**
	 * 
	 * Inner interface for stored procedure parameters.
	 * 
	 */
	static interface StoredProcedureParam {

	}

	/**
	 * Inner class which holds information about stored procedure IN parameters.
	 */
	static class StoredProcedureInParam implements StoredProcedureParam {
		/** stored procedure IN parameter value. */
		private Object value;

		/** stored procedure IN parameter Sql type. */
		private int sqlType;

		/**
		 * Getter method for value.
		 * 
		 * @return the value
		 */
		public Object getValue() {
			return value;
		}

		/**
		 * Setter method for value.
		 * 
		 * @param value
		 *            the value to set
		 */
		public void setValue(final Object value) {
			this.value = value;
		}

		/**
		 * Getter method for sqlType.
		 * 
		 * @return the sqlType
		 */
		public int getSqlType() {
			return sqlType;
		}

		/**
		 * Setter method for sqlType.
		 * 
		 * @param sqlType
		 *            the sqlType to set
		 */
		public void setSqlType(final int sqlType) {
			this.sqlType = sqlType;
		}

	}

	/**
	 * Inner class which holds information about stored procedure IN parameters.
	 */
	static class StoredProcedureOutParam implements StoredProcedureParam {
		/** stored procedure OUT parameter name. */
		private String paramName;

		/** stored procedure OUT parameter Sql type. */
		private int sqlType;

		/**
		 * Getter method for paramName.
		 * 
		 * @return the paramName
		 */
		public String getParamName() {
			return paramName;
		}

		/**
		 * Setter method for paramName.
		 * 
		 * @param paramName
		 *            the paramName to set
		 */
		public void setParamName(final String paramName) {
			this.paramName = paramName;
		}

		/**
		 * Getter method for sqlType.
		 * 
		 * @return the sqlType
		 */
		public int getSqlType() {
			return sqlType;
		}

		/**
		 * Setter method for sqlType.
		 * 
		 * @param sqlType
		 *            the sqlType to set
		 */
		public void setSqlType(final int sqlType) {
			this.sqlType = sqlType;
		}

	}

	/**
	 * Inner class which holds information about stored procedure IN OUT
	 * parameters.
	 */
	static class StoredProcedureInOutParam implements StoredProcedureParam {
		/** StoredProcedureInParam instance. */
		private StoredProcedureInParam inParam;

		/** StoredProcedureOutParam instance. */
		private StoredProcedureOutParam outParam;

		/**
		 * Getter method for inParam.
		 * 
		 * @return the inParam
		 */
		public StoredProcedureInParam getInParam() {
			return inParam;
		}

		/**
		 * Setter method for inParam.
		 * 
		 * @param inParam
		 *            the inParam to set
		 */
		public void setInParam(final StoredProcedureInParam inParam) {
			this.inParam = inParam;
		}

		/**
		 * Getter method for outParam.
		 * 
		 * @return the outParam
		 */
		public StoredProcedureOutParam getOutParam() {
			return outParam;
		}

		/**
		 * Setter method for outParam.
		 * 
		 * @param outParam
		 *            the outParam to set
		 */
		public void setOutParam(final StoredProcedureOutParam outParam) {
			this.outParam = outParam;
		}

	}

}
