/*
 * Copyright 2006 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                            
 *  2007/05/17 <Change NO.0001> created by Alex (SRM) 
 *  2007/05/21 <Change NO.0002> Added JavaDoc by Alex (SRM)
 *  2008/11/07 <Change NO.0003> changed logger as static. by N.Fukuda (VCIT) 
 *  2010/07/20 <Change NO.0004> LongText methods implemented by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00605;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00613;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00652;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Implementation for {@link StoredProcedureParameterList}.
 * 
 */
class StoredProcedureParameterListForOracle implements
		StoredProcedureParameterList {

	/** OracleTypes class. */
	private static String ORACLETYPES_CLASS = "oracle.jdbc.driver.OracleTypes";

	/** OracleTypes class' CURSOR field name. */
	private static String ORACLETYPES_CURSOR_FIELD = "CURSOR";

	/** NXGLogger for logging messages. */
	private static final NXGLogger logger = DefaultLogger.getInstance(StoredProcedureParameterListForOracle.class);
	
	/** ArrayList holds StoredProcedureParam instances. */
	private final List<StoredProcedureParam> paramList = new ArrayList<StoredProcedureParam>();

	/**
	 * Method to set stored procedure's IN Date parameter.
	 * 
	 * @param date
	 *            Date Object
	 * 
	 */
	public void setInDate(final Date date) {
		java.sql.Date sqlDate = (date == null) ? null : new java.sql.Date(date
				.getTime());
		addInParam(sqlDate, Types.DATE);
	}

	/**
	 * Method to set stored procedure's IN Numeric parameter.
	 * 
	 * @param num
	 *            Number
	 * 
	 */
	public void setInNumber(final Number num) {
		addInParam(num, Types.NUMERIC);

	}

	/**
	 * Method to set stored procedure's IN Char, Varchar parameter.
	 * 
	 * @param str
	 *            String
	 */
	public void setInString(final String str) {
		addInParam(str, Types.VARCHAR);
	}

	/**
	 * Method to set stored procedure's IN TimeStamp parameter.
	 * 
	 * @param date
	 *            Date
	 */
	public void setInTimeStamp(final Date date) {
		java.sql.Timestamp sqlTimestamp = (date == null) ? null
				: new java.sql.Timestamp(date.getTime());
		addInParam(sqlTimestamp, Types.TIMESTAMP);
	}

	/**
	 * Method to set stored procedure's IN Blob parameter.
	 * 
	 * @param object
	 *            Serializable Object
	 */
	public void setInBlob(final Serializable object) {

		addInParam(getByteArrayFromObject(object), Types.BLOB);
	}

	/**
	 * Method to set stored procedure's IN Clob parameter.
	 * 
	 * @param clobContent
	 *            Clob content as String
	 */
	public void setInClob(final String clobContent) {
		addInParam(clobContent, Types.VARCHAR);
	}

	/**
	 * Method to set stored procedure's OUT Date parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setOutDate(final String paramName) {
		addOutParam(paramName, Types.DATE);
	}

	/**
	 * Method to set stored procedure's OUT Numeric parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setOutNumber(final String paramName) {
		addOutParam(paramName, Types.NUMERIC);

	}

	/**
	 * Method to set stored procedure's OUT Char, Varchar parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setOutString(final String paramName) {
		addOutParam(paramName, Types.VARCHAR);
	}

	/**
	 * Method to set stored procedure's OUT TimeStamp parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setOutTimeStamp(final String paramName) {
		addOutParam(paramName, Types.TIMESTAMP);
	}

	/**
	 * Method to set stored procedure's OUT Blob parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setOutBlob(final String paramName) {

		addOutParam(paramName, Types.BLOB);
	}

	/**
	 * Method to set stored procedure's OUT Clob parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setOutClob(final String paramName) {
		addOutParam(paramName, Types.VARCHAR);
	}

	/**
	 * Method to set stored procedure's IN OUT Date parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * @param date
	 *            Date Object
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setInOutDate(final String paramName, final Date date) {
		java.sql.Date sqlDate = (date == null) ? null : new java.sql.Date(date
				.getTime());
		addInOutParam(paramName, sqlDate, Types.DATE);
	}

	/**
	 * Method to set stored procedure's IN OUT Numeric parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * @param num
	 *            Number
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setInOutNumber(final String paramName, final Number num) {
		addInOutParam(paramName, num, Types.NUMERIC);

	}

	/**
	 * Method to set stored procedure's IN OUT Char, Varchar parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * @param str
	 *            String
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setInOutString(final String paramName, final String str) {
		addInOutParam(paramName, str, Types.VARCHAR);
	}

	/**
	 * Method to set stored procedure's IN OUT TimeStamp parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * @param date
	 *            Date
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setInOutTimeStamp(final String paramName, final Date date) {
		java.sql.Timestamp sqlTimestamp = (date == null) ? null
				: new java.sql.Timestamp(date.getTime());
		addInOutParam(paramName, sqlTimestamp, Types.TIMESTAMP);
	}

//	/**
//	 * Method to set stored procedure's IN OUT Blob parameter.
//	 * 
//	 * @param paramName
//	 *            StoredProcedure Parameter name
//	 * @param object
//	 *            Serializable Object
//	 * 
//	 * @throws SystemException
//	 *             If any of the following cases occurs
//	 *             <ul>
//	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
//	 *             </ul>
//	 */
//	// JDBC Oracle stored procedure Blob IN OUT parameter problem (java.sql.SQLException: Parameter Type Conflict).
//	// For time being it is commented out.
//	public void setInOutBlob(final String paramName, final Serializable object) {
//		addInOutParam(paramName, getByteArrayFromObject(object), Types.BLOB);
//	}

	/**
	 * Method to set stored procedure's IN OUT Clob parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * @param clobContent
	 *            Clob content as String
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setInOutClob(final String paramName, final String clobContent) {
		addInOutParam(paramName, clobContent, Types.VARCHAR);
	}

	/**
	 * Method to set stored procedure's Cursor parameter.
	 * 
	 * @param paramName
	 *            StoredProcedure Parameter name
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	public void setCursor(final String paramName) {
		try {
			// To avoid compile time dependencies
			Class classObj = Class.forName(ORACLETYPES_CLASS);
			Field field = classObj.getField(ORACLETYPES_CURSOR_FIELD);
			int cursorType = (Integer) field.get(classObj);

			addOutParam(paramName, cursorType);
		} catch (ClassNotFoundException clsEx) {
			Message message = fjfe00652.createMessage(new String[] { clsEx
					.getMessage() });
			logger.error(message, clsEx);
			throw new SystemException(message, clsEx);
		} catch (NoSuchFieldException noFieldEx) {
			Message message = fjfe00652.createMessage(new String[] { noFieldEx
					.getMessage() });
			logger.error(message, noFieldEx);
			throw new SystemException(message, noFieldEx);
		} catch (IllegalAccessException illEx) {
			Message message = fjfe00652.createMessage(new String[] { illEx
					.getMessage() });
			logger.error(message, illEx);
			throw new SystemException(message, illEx);
		}
	}

	/**
	 * Creates StoredProcedureInParam and sets the value and sql type. Add the
	 * StoredProcedureInParam object into list.
	 * 
	 * @param object
	 *            Parameter's value.
	 * @param sqlType
	 *            Jdbc Sql column type.
	 */
	private void addInParam(final Object object, final int sqlType) {
		StoredProcedureInParam inParam = new StoredProcedureInParam();
		inParam.setValue(object);
		inParam.setSqlType(sqlType);
		paramList.add(inParam);
	}

	/**
	 * Creates StoredProcedureOutParam and sets the parameter name and sql type.
	 * Add the StoredProcedureInParam object into list.
	 * 
	 * @param paramName
	 *            Parameter's name.
	 * @param sqlType
	 *            Jdbc Sql column type.
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	private void addOutParam(final String paramName, final int sqlType) {
		if (paramName == null || paramName.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(), paramName };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		StoredProcedureOutParam outParam = new StoredProcedureOutParam();
		outParam.setParamName(paramName);
		outParam.setSqlType(sqlType);
		paramList.add(outParam);
	}

	/**
	 * Creates StoredProcedureInParam and sets the value and sql type. Creates
	 * StoredProcedureOutParam and sets the parameter name and sql type. Creates
	 * StoredProcedureInOutParam object and set the StoredProcedureInParam
	 * object and StoredProcedureOutParam object. Add the
	 * StoredProcedureInOutParam object into list.
	 * 
	 * @param paramName
	 *            Parameter's name.
	 * @param value
	 *            Parameter's value.
	 * @param sqlType
	 *            Jdbc Sql column type.
	 * 
	 * @throws SystemException
	 *             If any of the following cases occurs
	 *             <ul>
	 *             <li>if paramName is null or empty. ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             </ul>
	 */
	private void addInOutParam(final String paramName, final Object value,
			final int sqlType) {
		if (paramName == null || paramName.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(), paramName };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		StoredProcedureInParam inParam = new StoredProcedureInParam();
		inParam.setValue(value);
		inParam.setSqlType(sqlType);

		StoredProcedureOutParam outParam = new StoredProcedureOutParam();
		outParam.setParamName(paramName);
		outParam.setSqlType(sqlType);

		StoredProcedureInOutParam inOutParam = new StoredProcedureInOutParam();
		inOutParam.setInParam(inParam);
		inOutParam.setOutParam(outParam);

		paramList.add(inOutParam);
	}

	/**
	 * Method to convert Serializable Object into byte array.
	 * 
	 * @param object
	 *            Serializable Object
	 * @return byte array
	 * 
	 * @throws SystemException
	 *             If IOException is thrown ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00605 fjfe00605})
	 */
	private byte[] getByteArrayFromObject(final Serializable object) {

		if (object == null) {
			return null;
		}

		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(bao);
			oos.writeObject(object);
			oos.flush();

		} catch (IOException e) {
			Message message = fjfe00605.createMessage(new String[] { e
					.getMessage() });
			logger.error(message, e);
			throw new SystemException(message, e);
		} finally {

			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					Message message = fjfe00605.createMessage(new String[] { e
							.getMessage() });
					logger.error(message, e);
					throw new SystemException(message, e);
				}

			}
		}

		return bao.toByteArray();
	}

	/**
	 * Returns the parameters list.
	 * 
	 * @return the paramList
	 */
	public List<StoredProcedureParam> getParamList() {
		return paramList;
	}

	/**
	 * Construct String which contains parameters from parameter list in comma
	 * separated value format and returns it. If parameter list is empty returns
	 * null;
	 * 
	 * @return String which contains parameters in comma separated value format.
	 */
	@Override
	public String toString() {
		if (paramList.isEmpty()) {
			return "";
		}

		StringBuilder paramBuffer = new StringBuilder();

		for (StoredProcedureParam param : paramList) {
			if (param instanceof StoredProcedureInParam) {

				StoredProcedureInParam inParam = (StoredProcedureInParam) param;
				paramBuffer.append("value=");
				paramBuffer.append(inParam.getValue());
			} else if (param instanceof StoredProcedureOutParam) {

				StoredProcedureOutParam outParam = (StoredProcedureOutParam) param;
				paramBuffer.append("paramName=");
				paramBuffer.append(outParam.getParamName());
			} else if (param instanceof StoredProcedureInOutParam) {

				StoredProcedureInOutParam inOutParam = (StoredProcedureInOutParam) param;
				paramBuffer.append("value=");
				paramBuffer.append(inOutParam.getInParam().getValue());
				paramBuffer.append(":");
				paramBuffer.append("paramName=");
				paramBuffer.append(inOutParam.getOutParam().getParamName());
			}

			paramBuffer.append(", ");

		}

		return paramBuffer.substring(0, paramBuffer.lastIndexOf(","));
	}

	/**
	 * Not Supported in Oracle.
	 * 
	 * @param paramName
	 *            column name
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>Not Supported ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00613 fjfe00613})
	 *             </ul>
	 */
	@Override
	public void setInLongText(String longText) {
		String[] param = {"LongText","Oracle"};
		Message message = fjfe00613.createMessage(param);
		logger.error(message);
		throw new SystemException(message);		
	}

	/**
	 * Not Supported in Oracle.
	 * 
	 * @param paramName
	 *            column name
	 * @param longText Value.
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>Not Supported ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00613 fjfe00613})
	 *             </ul>
	 */
	@Override
	public void setInOutLongText(String paramName, String longText) {
		String[] param = {"LongText","Oracle"};
		Message message = fjfe00613.createMessage(param);
		logger.error(message);
		throw new SystemException(message);
	}

	/**
	 * Not Supported in Oracle.
	 * 
	 * @param paramName
	 *            column name
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>Not Supported ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00613 fjfe00613})
	 *             </ul>
	 */
	@Override
	public void setOutLongText(String paramName) {
		String[] param = {"LongText","Oracle"};
		Message message = fjfe00613.createMessage(param);
		logger.error(message);
		throw new SystemException(message);
	}

}
