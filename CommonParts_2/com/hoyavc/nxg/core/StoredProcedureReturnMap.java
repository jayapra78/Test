/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                                
 *  2007/05/17 <Change NO.0001> created by Alex (SRM) 
 *  2007/05/21 <Change NO.0002> Added JavaDoc by Alex (SRM)
 *  2010/07/20 <Change NO.0003> LongText added by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Interface for stored procedure return map.
 */
public interface StoredProcedureReturnMap {

	/**
	 * Method to get String.
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return String type value
	 */
	String getString(final String str);

	/**
	 * Method to get Number.
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Numeric type value
	 */
	BigDecimal getNumber(final String str);

	/**
	 * Method to get Date.
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Date type value
	 */
	Date getDate(final String str);

	/**
	 * Method to get TimeStamp.
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return TimeStamp type value
	 */
	Timestamp getTimeStamp(final String str);

	/**
	 * Method to get Cursor Result.
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return List contains SqlReturnMap.
	 */
	List<SqlReturnMap> getCursorResult(final String str);

	/**
	 * Method to get Blob.
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Blob type value.
	 */
	Object getBlob(final String str);

	/**
	 * Method to get Clob.
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Clob type value (String content).
	 * 
	 */
	String getClob(final String str);
	/**
	 * Method to get LongText.
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return LongText type value (String content).
	 * 
	 */
	String getLongText(final String str);
}
