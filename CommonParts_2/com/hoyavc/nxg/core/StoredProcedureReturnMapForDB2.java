/*
 * Copyright 2007-2013 by NXG Project, All rights reserved.
 *
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00613;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00653;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00654;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Implementation for StoredProcedureReturnMap. {@link SqlReturnMap} used for
 * store StoredProcedure Result values for other than Cursor type. Map used for
 * store StoredProcedure Result values for Cursor type.
 * 
 */
class StoredProcedureReturnMapForDB2 implements StoredProcedureReturnMap {

	/** NXGLogger for logging messages. */
	private static final NXGLogger logger = DefaultLogger.getInstance(StoredProcedureReturnMapForDB2.class);

	/** StoredProcedure Result map for other than Cursor type. */
	private final SqlReturnMap sqlReturnMap;

	/** StoredProcedure Result map for Cursor type. */
	private final Map<String, List<SqlReturnMap>> cursorResultMap;

	/**
	 * Constructor setting stored procedure result.
	 * 
	 * @param sqlReturnMap
	 *            Map contains Stored procedure result for types other than
	 *            cursor.
	 * @param cursorResultMap
	 *            Map contains Stored procedure result for cursor type.
	 * 
	 * @throws SystemException
	 *             If either sqlReturnMap or cursorResultMap is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00654 fjfe00654})
	 */
	StoredProcedureReturnMapForDB2(final SqlReturnMap sqlReturnMap,
			final Map<String, List<SqlReturnMap>> cursorResultMap) {
		if (sqlReturnMap == null || cursorResultMap == null) {
			String[] params = new String[] { SqlReturnMap.class.getName() };
			Message message = fjfe00654.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		this.sqlReturnMap = sqlReturnMap;
		this.cursorResultMap = cursorResultMap;
	}

	/**
	 * Method to get Date. Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForDB2
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Date parameter value.
	 * 
	 */
	public Date getDate(final String str) {
		return sqlReturnMap.getDate(str);
	}

	/**
	 * Method to get BigDecimal Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForDB2
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Numeric type parameter value.
	 * 
	 */
	public BigDecimal getNumber(final String str) {
		return sqlReturnMap.getNumber(str);
	}

	/**
	 * Method to get String Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForDB2
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return char and Varchar type parameter value.
	 * 
	 */
	public String getString(final String str) {
		return sqlReturnMap.getString(str);
	}

	/**
	 * Method to get TimeStamp Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForDB2
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return TimeStamp type parameter value.
	 * 
	 */
	public Timestamp getTimeStamp(final String str) {
		return sqlReturnMap.getTimeStamp(str);
	}

	/**
	 * Method to get Blob Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForDB2
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Blob type parameter value.
	 * 
	 */
	public Object getBlob(final String str) {
		return sqlReturnMap.getBlob(str);
	}

	/**
	 * Method to get Clob Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForDB2
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Clob type parameter value (String content).
	 * 
	 */
	public String getClob(final String str) {
		return sqlReturnMap.getClob(str);
	}
	
	/**
	 * Not Supported in DB2.
	 * 
	 * @param str
	 *            column name
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>Not Supported ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00613 fjfe00613})
	 *             </ul>
	 */
	public String getLongText(final String str) {
		String[] param = {"LongText","DB2"};
		Message message = fjfe00613.createMessage(param);
		logger.error(message);
		throw new SystemException(message);
	}
	/**
	 * Method to get Cursor result.
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Cursor type parameter value (List contains SqlReturnMap).
	 * 
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If str is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 *             <li>If No matching key for given str in cursorResultMap ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00653 fjfe00653})
	 *             </ul>
	 */
	public List<SqlReturnMap> getCursorResult(final String str) {
		if (str == null || str.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(), str };
			Message message = fjfe00004.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (!cursorResultMap.containsKey(str.toUpperCase())) {
			String[] params = new String[] { str };
			Message message = fjfe00653.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		return cursorResultMap.get(str.toUpperCase());
	}

}
