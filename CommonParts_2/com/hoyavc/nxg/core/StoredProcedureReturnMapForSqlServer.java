/*
 * Copyright(C)2007-2011 Hoya corporation All rights reserved.
 *
 *  Change Tracking                                         
 *  2011/02/23 <Change NO.0001> created by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00613;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00654;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * Implementation for StoredProcedureReturnMap. {@link SqlReturnMap} used for
 * store StoredProcedure Result values.
 * 
 */
class StoredProcedureReturnMapForSqlServer implements StoredProcedureReturnMap {

	/** NXGLogger for logging messages. */
	private static final NXGLogger logger = DefaultLogger.getInstance(StoredProcedureReturnMapForSqlServer.class);

	/** StoredProcedure Result map . */
	private final SqlReturnMap sqlReturnMap;

	/**
	 * Constructor setting stored procedure result.
	 * 
	 * @param sqlReturnMap
	 *            Map contains Stored procedure result.
	 * @throws SystemException
	 *             If sqlReturnMap is null (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00654
	 *             fjfe00654})
	 */
	StoredProcedureReturnMapForSqlServer(final SqlReturnMap sqlReturnMap) {
		if (sqlReturnMap == null) {
			String[] params = new String[] { SqlReturnMap.class.getName() };
			Message message = fjfe00654.createMessage(params);

			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}
		this.sqlReturnMap = sqlReturnMap;
	}

	/**
	 * Method to get Date. Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForSqlServer
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Date parameter value.
	 * 
	 */
	public Date getDate(final String str) {
		return sqlReturnMap.getDate(str);
	}

	/**
	 * Method to get BigDecimal Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForSqlServer
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Numeric type parameter value.
	 * 
	 */
	public BigDecimal getNumber(final String str) {
		return sqlReturnMap.getNumber(str);
	}

	/**
	 * Method to get String Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForSqlServer
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return char and Varchar type parameter value.
	 * 
	 */
	public String getString(final String str) {
		return sqlReturnMap.getString(str);
	}

	/**
	 * Method to get TimeStamp Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForSqlServer
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return TimeStamp type parameter value.
	 * 
	 */
	public Timestamp getTimeStamp(final String str) {
		return sqlReturnMap.getTimeStamp(str);
	}

	/**
	 * Method to get Blob Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForSqlServer
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Blob type parameter value.
	 * 
	 */
	public Object getBlob(final String str) {
		return sqlReturnMap.getBlob(str);
	}

	/**
	 * Method to get Clob Internally using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForSqlServer
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @return Clob type parameter value (String content).
	 * 
	 */
	public String getClob(final String str) {
		return sqlReturnMap.getClob(str);
	}

	/**
	 * Not Supported in SqlServer.
	 * 
	 * @param str
	 *            column name
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>Not Supported (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00613
	 *             fjfe00613})
	 *             </ul>
	 */
	public String getLongText(final String str) {
		String[] param = { "LongText", "SqlServer" };
		Message message = fjfe00613.createMessage(param);
		logger.error(message);
		throw new SystemException(message);
	}

	/**
	 * Method to get CursorResult using SqlReturnMap.
	 * 
	 * @see SqlReturnMapForMySql
	 * 
	 * @param str
	 *            StoredProcedure parameter name
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>Not Supported (
	 *             {@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00613
	 *             fjfe00613})
	 *             </ul>
	 * 
	 */
	public List<SqlReturnMap> getCursorResult(String str) {
		String[] param = { "Cursor", "SqlServer" };
		Message message = fjfe00613.createMessage(param);
		logger.error(message);
		throw new SystemException(message);
	}

}
