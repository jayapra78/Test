/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT)
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)
 *  2007/07/25 <Change NO.0003> Modified getTransactionContext by Mitsuhashi (VCIT)
 *  2008/08/07 <Change NO.0004> Added JVM Transaction Context by Thangaraj S (SRM)
 *  2010/06/14 <Change NO.0005> Added REST Transaction Context by Baskaran (SRM)
 */
package com.hoyavc.nxg.core;

/**
 * Class to store and retrieve TransactionContext across a {@link Thread} using
 * {@link ThreadLocal}.
 * 
 * <p>
 * In NXG WEB application,
 * {@link com.hoyavc.nxg.core.web.DefaultFilter DefaultFilter} creates and store
 * TransactionContext in ThreadLocalContainer which will be accessed by NXG
 * applications and business classes. After completing the execution it resets
 * it to null.
 * </p>
 * <p>
 * In NXG EJB application,
 * {@link com.hoyavc.nxg.core.RequiresNewSessionBean RequiresNewSessionBean}
 * store TransactionContext in ThreadLocalContainer. After completing the
 * execution it resets it to null.
 * </p>
 * <p>
 * In NXG WEBSERVICE application,
 * {@link com.hoyavc.nxg.core.webservice.AbstractCbsInvokeService AbstractCbsInvokeService}
 * creates and store TransactionContext in ThreadLocalContainer.
 * After completing the execution it resets it to null.
 * </p>
 * <p>
 * In NXG REST application,
 * {@link com.hoyavc.nxg.core.rest.AbstractRestCbsInvokeService AbstractRestCbsInvokeService}
 * creates and store TransactionContext in ThreadLocalContainer.
 * After completing the execution it resets it to null.
 * </p>
 * <p>
 * In NXG JVM application,
 * creates and store TransactionContext in ThreadLocalContainer.
 * After completing the execution it resets it to null.
 * </p>
 
 * <p>
 * In NXG logging, {@link DefaultLogger} and {@link InitializationErrorLogger}
 * uses ThreadLocalContainer to get TransactionContext
 * </p>
 */
public class ThreadLocalContainer {

	/** Private Constructor. */
	private ThreadLocalContainer() {
	}
	
	/** ThreadLocal to store TransactionContext by EJB. */
	private static final ThreadLocal<TransactionContext> threadTransactionContext_EJB = new ThreadLocal<TransactionContext>();

	/** ThreadLocal to store TransactionContext by WEB. */
	private static final ThreadLocal<TransactionContext> threadTransactionContext_WEB = new ThreadLocal<TransactionContext>();

	/** ThreadLocal to store TransactionContext by WEBSERVICE. */
	private static final ThreadLocal<TransactionContext> threadTransactionContext_WEBSERVICE = new ThreadLocal<TransactionContext>();
	
	/** ThreadLocal to store TransactionContext by REST. */
	private static final ThreadLocal<TransactionContext> threadTransactionContext_REST = new ThreadLocal<TransactionContext>();
	
	/** ThreadLocal to store TransactionContext by JVM. */
	private static final ThreadLocal<TransactionContext> threadTransactionContext_JVM = new ThreadLocal<TransactionContext>();

	/**
	 * getter method to get TransactionContext from EJB stored ThreadLocal.
	 * 
	 * @return TransactionContext
	 */
	static TransactionContext getTransactionContext_EJB() {
		return threadTransactionContext_EJB.get();
	}

	/**
	 * setter method to set TransactionContext to ThreadLocal by EJB.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 */
	static void setTransactionContext_EJB(TransactionContext trxContext) {
		threadTransactionContext_EJB.set(trxContext);
	}

	/**
	 * getter method to get TransactionContext from WEB stored ThreadLocal.
	 * 
	 * @return TransactionContext
	 */
	public static TransactionContext getTransactionContext_WEB() {
		return threadTransactionContext_WEB.get();
	}

	/**
	 * setter method to set TransactionContext to ThreadLocal by WEB.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 */
	public static void setTransactionContext_WEB(TransactionContext trxContext) {
		threadTransactionContext_WEB.set(trxContext);
	}

	/**
	 * Getter method to get TransactionContext from WEBSERVICE stored
	 * ThreadLocal This method is provided only for Framework team. Business
	 * application team should not use this method.
	 * 
	 * @return TransactionContext
	 */
	public static TransactionContext getTransactionContext_WEBSERVICE() {
		return threadTransactionContext_WEBSERVICE.get();
	}

	/**
	 * Setter method to set TransactionContext to ThreadLocal by WEBSERVICE This
	 * method is provided only for Framework team. Business application team
	 * should not use this method.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 */
	public static void setTransactionContext_WEBSERVICE(
			TransactionContext trxContext) {
		threadTransactionContext_WEBSERVICE.set(trxContext);
	}
	
	
	/**
	 * getter method to get TransactionContext from JVM stored ThreadLocal.
	 * 
	 * @return TransactionContext
	 */
	public static TransactionContext getTransactionContext_JVM() {
		return threadTransactionContext_JVM.get();
	}

	/**
	 * setter method to set TransactionContext to ThreadLocal by JVM.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 */
	public static void setTransactionContext_JVM(TransactionContext trxContext) {
		threadTransactionContext_JVM.set(trxContext);
	}

	/**
	 * getter method to get TransactionContext from REST stored ThreadLocal.
	 * 
	 * @return TransactionContext
	 */
	public static TransactionContext getTransactionContext_REST() {
		return threadTransactionContext_REST.get();
	}

	/**
	 * setter method to set TransactionContext to ThreadLocal by REST.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 */
	public static void setTransactionContext_REST(TransactionContext trxContext) {
		threadTransactionContext_REST.set(trxContext);
	}

	/**
	 * getter method to get TransactionContext.
	 * 
	 * <p>
	 * If WEB stored TransactionContext is not null then
	 * it takes from WEB stored TransactionContext.
	 * Else If EJB stored TransactionContext is not null then
	 * it takes from EJB stored TransactionContext.
	 * Else If WEBSERVICE stored TransactionContext is not null then
	 * it takes from WEBSERVICE stored TransactionContext.
	 * Else If REST stored TransactionContext is not null then
	 * it takes from REST stored TransactionContext.
	 * Else 
	 * it takes from JVM stored TransactionContext.
	 * 
	 * </p>
	 * 
	 * @return TransactionContext
	 */
	static TransactionContext getTransactionContext() {
		TransactionContext trxContextWEB = getTransactionContext_WEB();
		TransactionContext trxContextEJB = getTransactionContext_EJB();
		TransactionContext trxContextWEBSERVICE = getTransactionContext_WEBSERVICE();
		TransactionContext trxContextREST = getTransactionContext_REST();
		
		if (trxContextWEB != null ) {
			return trxContextWEB;
		} else if (trxContextEJB != null) {
			return trxContextEJB;
		} else if (trxContextWEBSERVICE != null) {
			return trxContextWEBSERVICE;	
		} else if (trxContextREST != null) {
			return trxContextREST;	
		} else {
			return getTransactionContext_JVM();
		}
	}
	
	

}
