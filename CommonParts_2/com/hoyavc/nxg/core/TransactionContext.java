/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00001;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00501;
import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00502;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * TransactionContext stores UserData(Current user information) which is passed
 * to NXG applications for processing.
 * 
 */
public class TransactionContext implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** Warning List. */
	private List<Message> warningList = new ArrayList<Message>();

	/** Processing GMT time. */
	private String processingGMTTime;
	
	/** Processing GMT time For Parent Transaction if any. */
	private String processingGMTTimeParent;

	/** User Information. */
	private UserData userData;
	
	private boolean retryFlag;

	/**
	 * Constructor with UserData as argument.
	 * 
	 * @param userData
	 *            UserData
	 * @throws SystemException
	 *             If userData is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 */
	// public temporary
	public TransactionContext(final UserData userData) {

		if (userData == null) {
			String[] params = new String[] { UserData.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			// NXG Logger is not serializable
			NXGLogger logger = DefaultLogger.getInstance(this.getClass());
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.userData = userData;
	}

	/**
	 * Method to get new transaction context. It adds UserData and
	 * ProcessingGMTTime from given transaction context.
	 * 
	 * @param trxContext
	 *            TransactionContext
	 * @return new TransactionContext
	 * @throws SystemException
	 *             If trxContext is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00001 fjfe00001})
	 */
	static TransactionContext getNewTransactionContext(
			TransactionContext trxContext) {

		if (trxContext == null) {
			Message message = fjfe00001.createMessage();
			// NXG Logger is not serializable
			NXGLogger logger = DefaultLogger
					.getInstance(TransactionContext.class.getName());
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		TransactionContext newTrxContext = new TransactionContext(
				trxContext.userData);
		newTrxContext.setRetryFlag(trxContext.isRetryFlag());
		newTrxContext.processingGMTTime = trxContext.processingGMTTime;
		newTrxContext.processingGMTTimeParent = trxContext.processingGMTTimeParent;

		return newTrxContext;
	}

	/**
	 * Getter method for UserData.
	 * 
	 * @return UserData
	 */
	public UserData getUserData() {
		return userData;
	}

	/**
	 * Method to add warning to the warning list.
	 * 
	 * @param warning
	 *            Waring
	 * @throws SystemException
	 *             If warning is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 */
	public void addWarning(final Message warning) {

		if (warning == null) {
			String[] params = new String[] { Message.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			// NXG Logger is not serializable
			NXGLogger logger = DefaultLogger.getInstance(this.getClass());
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		warningList.add(warning);
	}

	/**
	 * Method for adding WarningList.
	 * 
	 * @param warningList
	 *            Warning List
	 * @throws SystemException
	 *             If warningList is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 */
	public void addWarningList(final List<Message> warningList) {

		// validating parameter
		if (warningList == null) {
			String[] params = new String[] { List.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			// NXG Logger is not serializable
			NXGLogger logger = DefaultLogger.getInstance(this.getClass());
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.warningList.addAll(warningList);
	}

	/**
	 * getter method for WarningList, it returns a clone of warningList.
	 * 
	 * @return List<Message>
	 */
	public List<Message> getCurrentWarningList() {
		// clone
		return new ArrayList<Message>(warningList);
	}

	/**
	 * Getter Method for processingGMTTime.
	 * 
	 * @return processingGMTTime
	 */
	public String getProcessingGMTTime() {
		return processingGMTTime;
	}

	/**
	 * setter method for processingGMTTime.
	 * 
	 * @param processingGMTTime
	 *            Processing GMT Time
	 * @throws SystemException
	 *             If any of the case
	 *             <ul>
	 *             <li>If processingGMTTime is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00501 fjfe00501})
	 *             <li>If processingGMTTime is not equal to 17 digit ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00502 fjfe00502})
	 *             </ul>
	 */
	public void setProcessingGMTTime(final String processingGMTTime) {

		// validating parameter
		if (processingGMTTime == null || processingGMTTime.trim().length() <= 0) {

			String[] params = new String[] { processingGMTTime };
			Message message = fjfe00501.createMessage(params);

			// NXG Logger is not serializable
			NXGLogger logger = DefaultLogger.getInstance(this.getClass());
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (processingGMTTime.trim().length() != 17) {

			String[] params = new String[] { processingGMTTime };
			Message message = fjfe00502.createMessage(params);

			// NXG Logger is not serializable
			NXGLogger logger = DefaultLogger.getInstance(this.getClass());
			logger.error(message, new IllegalArgumentException());
			throw new SystemException(message, new IllegalArgumentException());
		}

		this.processingGMTTime = processingGMTTime;
	}

	/**
	 * Getter Method for processingGMTTimeParent.
	 * 
	 * @return processingGMTTimeParent
	 */
	String getProcessingGMTTimeParent() {
		return processingGMTTimeParent;
	}

	/**
	 * setter method for processingGMTTimeParent.
	 * 
	 * @param processingGMTTimeParent
	 *            Processing GMT Time parent
	 * @throws SystemException
	 *             If any of the case
	 *             <ul>
	 *             <li>If processingGMTTime is null or empty ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00501 fjfe00501})
	 *             <li>If processingGMTTime is not equal to 17 digit ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00502 fjfe00502})
	 *             </ul>
	 */
	void setProcessingGMTTimeParent(String processingGMTTimeParent) {
		// validating parameter
		if (processingGMTTimeParent == null || processingGMTTimeParent.trim().length() <= 0) {

			String[] params = new String[] { processingGMTTimeParent };
			Message message = fjfe00501.createMessage(params);

			// NXG Logger is not serializable
			NXGLogger logger = DefaultLogger.getInstance(this.getClass());
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		if (processingGMTTimeParent.trim().length() != 17) {

			String[] params = new String[] { processingGMTTimeParent };
			Message message = fjfe00502.createMessage(params);

			// NXG Logger is not serializable
			NXGLogger logger = DefaultLogger.getInstance(this.getClass());
			logger.error(message, new IllegalArgumentException());
			throw new SystemException(message, new IllegalArgumentException());
		}
		this.processingGMTTimeParent = processingGMTTimeParent;
	}

	/**
	 * @return the retryFlag
	 */
	public final boolean isRetryFlag() {
		return retryFlag;
	}

	/**
	 * @param retryFlag the retryFlag to set
	 */
	public final void setRetryFlag(boolean retryFlag) {
		this.retryFlag = retryFlag;
	}

}
