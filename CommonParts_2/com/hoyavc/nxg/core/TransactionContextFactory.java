/*
 * Copyright 2006 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/07/06 <Change NO.0001> created by Nobuo Fukuda (VCIT)
 *  2007/07/06 <Change NO.0001> Added javadoc by Alex (SRM) 
 */
package com.hoyavc.nxg.core;

/**
 * Interface for validating user's login status.
 */
public interface TransactionContextFactory {

	/**
	 * Validating user's login status and creating transaction context.
	 * 
	 * @param trxContext
	 *            Dummy TransactionContext
	 * @param vo
	 *            Contains User's login session id.
	 * 
	 * @return TransactionContext with user information.
	 * 
	 * @throws ApplicationException Application Exception
	 */
	TransactionContext createTransactionContext(
			TransactionContext trxContext, ValueObject vo)
			throws ApplicationException;

}
