/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/01/11 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/05/21 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 *  2007/08/02 <Change NO.0003> Modified getAdditionalUserData by T.Mitsuhashi (VCIT)  
 *  2010/08/02 <Change NO.0004> companyCode,companyNameAbbr,loginPlace are added by Kalai.P (SRM)
 *  2010/09/08 <Change NO.0005> employee_code,company_code and company_name are removed
 *  	and login_user_name added by Kalai.P (SRM)
 *  2010/09/16 <Change NO.0006> roleIdList added by Kalai.P (SRM)
 *  2010/11/29 <Change NO.0007> lastMonthPostingPossibleFlag added by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core;

import java.io.Serializable;
import java.util.*;

/**
 * Class to handle the UserData, It handles login user's information.
 */
public class UserData implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** User ID. */
	private String userId;

	/** Corporation code.  */
	private String corporationCode;

	/** Corporation name abbreviation. */
	private String corporationNameAbbr;

	/** loginUserName. */
	private String loginUserName;

	/** Time zone. */
	private TimeZone timezone;

	/** Language code. */
	private String languageCode;

	/** Country code. */
	private String countryCode;

	/** Locale. */
	private Locale locale;

	/** IP Address. */
	private String ipAddress;
	
	/** LoginPlace Name. */
	private String loginPlace;
	
	/** profileId. */
	private String profileId;
	
	/** roleIdList. */
	private List<String> roleIdList;
	
	/** Last Month Posting Possible Flag. */
	private String lastMonthPostingPossibleFlag;

	/**
	 * User's additional information, such as subsystem vo.
	 */
	private Map<String, AdditionalUserData> addtionalUserDataMap = new HashMap<String, AdditionalUserData>();
	
	/** client Version. */
	private String clientVersion;
	
	/**
	 * Constructor.
	 */
	public UserData() {
	}

	/**
	 * Get corporationCode.
	 * 
	 * @return the corporationCode
	 */
	public String getCorporationCode() {
		return corporationCode;
	}

	/**
	 * Set corporationCode.
	 * 
	 * @param corporationCode
	 *            the corporationCode to set
	 */
	public void setCorporationCode(String corporationCode) {
		this.corporationCode = corporationCode;
	}

	/**
	 * Get corporationNameAbbr.
	 * 
	 * @return the corporationNameAbbr
	 */
	public String getCorporationNameAbbr() {
		return corporationNameAbbr;
	}

	/**
	 * Set corporationNameAbbr.
	 * 
	 * @param corporationNameAbbr
	 *            the corporationNameAbbr to set
	 */
	public void setCorporationNameAbbr(String corporationNameAbbr) {
		this.corporationNameAbbr = corporationNameAbbr;
	}

	/**
	 * Get countryCode.
	 * 
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * Set countryCode.
	 * 
	 * @param countryCode
	 *            the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the loginUserName
	 */
	public String getLoginUserName() {
		return loginUserName;
	}

	/**
	 * @param loginUserName the loginUserName to set
	 */
	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}

	/**
	 * Get ipAddress.
	 * 
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * Set ipAddress.
	 * 
	 * @param ipAddress
	 *            the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * Get languageCode.
	 * 
	 * @return the languageCode
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * Set languageCode.
	 * 
	 * @param languageCode
	 *            the languageCode to set
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * Get locale.
	 * 
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Set locale.
	 * 
	 * @param locale
	 *            the locale to set
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * Get timezone.
	 * 
	 * @return the timezone
	 */
	public TimeZone getTimezone() {
		return timezone;
	}

	/**
	 * Set timezone.
	 * 
	 * @param timezone
	 *            the timezone to set
	 */
	public void setTimezone(TimeZone timezone) {
		this.timezone = timezone;
	}

	/**
	 * Get userId.
	 * 
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Set userId.
	 * 
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Get AdditionalUserDataMap.
	 * 
	 * @return additionalUserDataMap
	 */
	public Map<String, AdditionalUserData> getAddtionalUserDataMap() {
		return addtionalUserDataMap;
	}

	/**
	 * Set AdditionalUserDataMap.
	 * 
	 * @param addtionalUserDataMap
	 */
	public void setAddtionalUserDataMap(
			Map<String, AdditionalUserData> addtionalUserDataMap) {
		this.addtionalUserDataMap = addtionalUserDataMap;
	}

	/**
	 * return AdditionalUserData for each sub system.
	 * 
	 * @param additionalUserDataClass
	 *            class of AdditionalUserData for each sub system.
	 * @return AdditionalUserData
	 */
	@SuppressWarnings("unchecked")
	public <E extends AdditionalUserData> E getAdditionalUserData(
			Class<E> additionalUserDataClass) {

		AdditionalUserData additionalUserData = addtionalUserDataMap
			.get(additionalUserDataClass.getName());
		
		return (E) additionalUserData;
	}

	/**
	 * @return the loginPlace
	 */
	public String getLoginPlace() {
		return loginPlace;
	}

	/**
	 * @param loginPlace the loginPlace to set
	 */
	public void setLoginPlace(String loginPlace) {
		this.loginPlace = loginPlace;
	}

	/**
	 * @return the profileId
	 */
	public String getProfileId() {
		return profileId;
	}

	/**
	 * @param profileId the profileId to set
	 */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	/**
	 * @return the roleIdList
	 */
	public List<String> getRoleIdList() {
		return roleIdList;
	}

	/**
	 * @param roleIdList the roleIdList to set
	 */
	public void setRoleIdList(List<String> roleIdList) {
		this.roleIdList = roleIdList;
	}

	/**
	 * @return the lastMonthPostingPossibleFlag
	 */
	public String getLastMonthPostingPossibleFlag() {
		return lastMonthPostingPossibleFlag;
	}

	/**
	 * @param lastMonthPostingPossibleFlag the lastMonthPostingPossibleFlag to set
	 */
	public void setLastMonthPostingPossibleFlag(String lastMonthPostingPossibleFlag) {
		this.lastMonthPostingPossibleFlag = lastMonthPostingPossibleFlag;
	}

	/**
	 * @return the clientVersion
	 */
	public String getClientVersion() {
		return clientVersion;
	}

	/**
	 * @param clientVersion the clientVersion to set
	 */
	public void setClientVersion(String clientVersion) {
		this.clientVersion = clientVersion;
	}
	
}
