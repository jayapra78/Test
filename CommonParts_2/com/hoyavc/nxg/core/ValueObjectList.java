/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core;

import static com.hoyavc.nxg.core.CoreMessageCodeEnum.fjfe00004;

import java.util.LinkedList;
import java.util.List;

/**
 * Class to store list of ValueObject.
 * 
 * @param <E
 *            extends ValueObject>
 */
public class ValueObjectList<E extends ValueObject> implements ValueObject {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** ValueObject List. */
	private List<E> list = new LinkedList<E>();

	/**
	 * Get current ValueObject list. 
	 * It clones the existing list and returns the cloned list.
	 * 
	 * @return List<E> clone of list contains ValueObjects
	 */
	public List<E> getCurrentList() {
		// clone
		return new LinkedList<E>(list);
	}
	
	/**
	 * Get ValueObject list.
	 * 
	 * @return List<E> List contains ValueObjects
	 */
	public List<E> getList() {
		return list;
	}

	/**
	 * Method to set new list of ValueObjects.
	 * 
	 * @param list
	 *            List
	 * @throws SystemException
	 *             If list is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 */
	public void setNewList(final List<E> list) {

		// validating parameter
		if (list == null) {
			String[] params = new String[] { List.class.getName(), null };
			Message message = fjfe00004.createMessage(params);

			// NXG Logger is not Serializable
			NXGLogger logger = DefaultLogger.getInstance(this.getClass());
			logger.error(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.list = new LinkedList<E>(list);
	}

	/**
	 * Adding a ValueObject to the existing list.
	 * 
	 * @param e
	 *            ValueObject of type<E>. (null is allowed)
	 * @return true (as per the general contract of Collection.add).
	 */
	public boolean add(final E e) {

		return list.add(e);
	}
}
