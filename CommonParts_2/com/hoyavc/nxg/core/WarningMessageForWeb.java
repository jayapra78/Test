/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/06/11 <Change NO.0001> created by Alex (SRM)   
 */
package com.hoyavc.nxg.core;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Message class which encapsulating MessageCode, message and its Localized
 * message. NXG framework(Web Client) uses this class.
 * 
 */
@XmlRootElement(name = "WarningMessageForWeb")
public class WarningMessageForWeb implements Serializable {

	/** Serial ID. */
	private static final long serialVersionUID = 1L;

	/** message code. */
	private String messageCode;

	/** message string. */
	private String message;

	/** Localized message. */
	private String localizedMessage;

	/**
	 * Empty Constructor.
	 */
	public WarningMessageForWeb() {
	}

	/**
	 * Getter method for message string.
	 * 
	 * @return message String
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Getter method for message code.
	 * 
	 * @return message code
	 */
	public String getMessageCode() {
		return messageCode;

	}

	/**
	 * @return the localizedMessage.
	 */
	public String getLocalizedMessage() {
		return localizedMessage;
	}

	/**
	 * @param localizedMessage
	 *            the localizedMessage to set
	 */
	public void setLocalizedMessage(String localizedMessage) {
		this.localizedMessage = localizedMessage;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @param messageCode
	 *            the messageCode to set
	 */
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	/**
	 * Override method to return messageCode and message.
	 * 
	 * @return String
	 * @see Object#toString()
	 */
	@Override
	public String toString() {
		return messageCode + " " + message + " " + localizedMessage;
	}

}
