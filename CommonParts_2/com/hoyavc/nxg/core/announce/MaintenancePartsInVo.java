/*
 * Copyright(C)2007-2012 Hoya corporation All rights reserved.
 *
 *  Change Tracking                                         
 *  2012/10/19 <Change NO.0001> created by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core.announce;

import javax.xml.bind.annotation.XmlRootElement;

import com.hoyavc.nxg.core.ValueObject;

/**
 * Maintenance Parts InVo.
 */
@XmlRootElement(name="com.hoyavc.nxg.core.announce.MaintenancePartsInVo")
public class MaintenancePartsInVo implements ValueObject {

	/** Serial Version ID. */
	private static final long serialVersionUID = 1L;

	/** request URI */
	private String requestURI;

	/**
	 * @return the requestURI
	 */
	public String getRequestURI() {
		return requestURI;
	}

	/**
	 * @param requestURI the requestURI to set
	 */
	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}

}
