/*
* Copyright 2006 by NXG Project, All rights reserved.
*
*  Change Tracking                                         
*  2007/06/18 <Change NO.0001> created by Alex (SRM) 
*  2007/01/28 <Change NO.0002> Added sessionId by Nobuo Fukuda (VCIT)
*  2010/09/17 <Change NO.0003> corporationCode added by Kalai.P[SRM] 
*/
package com.hoyavc.nxg.core.login;

import com.hoyavc.nxg.core.ValueObject;

/**
 * ValueObject class used in common batch. 
 * It holds userId, password, ipAddress, corporation code of user.
 * This Vo is provided for FrameWork team.
 * Business application team should not use this VO.
 */
public class BatchLoginInVo implements ValueObject {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** User Id. */
	private String userId;
	
	/** Password. */
	private String password;

	/** IpAddress. */
	private String ipAddress;
	
	/** User's session id. */
	private String sessionId;
	
	/** CorporationCode. */
	private String corporationCode;

	/**
	 * getter method for User Id.
	 * 
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * setter method for User Id.
	 * 
	 * @param userId
	 *            User Id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Getter method for password.
	 * 
	 * @return Password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Setter method for password.
	 * 
	 * @param password Password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Getter method for ipAddress.
	 * 
	 * @return IpAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * Setter method for ipAddress.
	 * 
	 * @param ipAddress IpAddress
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	/**
	 * Getter method for User's session id.
	 * 
	 * @return User's session id.
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Setter method for User's session id.
	 * 
	 * @param sessionId
	 *            User's session id
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the corporationCode
	 */
	public String getCorporationCode() {
		return corporationCode;
	}

	/**
	 * @param corporationCode the corporationCode to set
	 */
	public void setCorporationCode(String corporationCode) {
		this.corporationCode = corporationCode;
	}
	
}
