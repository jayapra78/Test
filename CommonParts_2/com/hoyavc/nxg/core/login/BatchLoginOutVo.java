/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/06/18 <Change NO.0001> created by Alex (SRM)
 *  2007/01/28 <Change NO.0002> Added sessionId by Nobuo Fukuda (VCIT)
 */
package com.hoyavc.nxg.core.login;

import com.hoyavc.nxg.core.UserData;
import com.hoyavc.nxg.core.ValueObject;

/**
 * ValueObject class used in common batch. 
 * ValueObject contains Login user information.
 * This Vo is provided for FrameWork team.
 * Business application team should not use this VO.
 *
 */
public class BatchLoginOutVo implements ValueObject {

	/** SerialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** Login user information. */
	private UserData userData;
	
	/** User's session id. */
	private String sessionId;
	
	
	/**
	 * Getter method for user information.
	 * 
	 * @return UserData
	 */
	public UserData getUserData() {
		return userData;
	}

	/**
	 * Setter method for user information.
	 * 
	 * @param userData User information
	 */
	public void setUserData(UserData userData) {
		this.userData = userData;
	}
	
	/**
	 * Getter method for User's session id.
	 * 
	 * @return User's session id.
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Setter method for User's session id.
	 * 
	 * @param sessionId
	 *            User's session id
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
}
