/*
 * Copyright 2006 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/06/15 <Change NO.0001> created by Alex (SRM) 
 *  2010/08/27 <Change NO.0002> fromBatch added to fix do not create(insert) batch login data  by Kalai.P (SRM)
 *  2010/10/12 <Change NO.0003> jbossSessionId added by Kalai.P[SRM]
 */
package com.hoyavc.nxg.core.login;

import javax.xml.bind.annotation.XmlRootElement;

import com.hoyavc.nxg.core.CommonWebInputFields;
import com.hoyavc.nxg.core.ValueObject;

/**
 * ValueObject contains login user session id. This Vo is provided for FrameWork
 * team. Business application team should not use this VO.
 * 
 */
@XmlRootElement(name="com.hoyavc.nxg.core.login.LoginSessionIdVo")
public class LoginSessionIdVo implements ValueObject,
		CommonWebInputFields {

	/** use serialVersionUID from JDK 1.0.2 for interoperability. */
	private static final long serialVersionUID = 1L;

	/** User's session id. */
	private String sessionId;
	
	/** Variable to store whether fromBatch or not */
	private boolean fromBatch;
	
	/**
	 * Getter method for User's session id.
	 * 
	 * @return User's session id.
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Setter method for User's session id.
	 * 
	 * @param sessionId
	 *            User's session id
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the fromBatch
	 */
	public boolean isFromBatch() {
		return fromBatch;
	}

	/**
	 * @param fromBatch the fromBatch to set
	 */
	public void setFromBatch(boolean fromBatch) {
		this.fromBatch = fromBatch;
	}

}
