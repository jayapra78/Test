/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2007/06/14 <Change NO.0001> created by Alex (SRM)
 */
package com.hoyavc.nxg.core.login;

import com.hoyavc.nxg.core.UserData;
import com.hoyavc.nxg.core.ValueObject;

/**
 * ValueObject contains Login user information.
 * This Vo is provided for FrameWork team.
 * Business application team should not use this VO.
 *
 */
public class LoginUserDataVo implements ValueObject {

	/** SerialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** Login user information. */
	private UserData userData;
	
	/**
	 * Getter method for user information.
	 * 
	 * @return UserData
	 */
	public UserData getUserData() {
		return userData;
	}

	/**
	 * Setter method for user information.
	 * 
	 * @param userData User information
	 */
	public void setUserData(UserData userData) {
		this.userData = userData;
	}
	
}
