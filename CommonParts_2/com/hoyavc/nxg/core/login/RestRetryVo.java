/*
 * Copyright(C)2007-2011 Hoya corporation All rights reserved.
 *
 *  Change Tracking                                         
 *  2011/08/15 <Change NO.0001> created by Kalai.P (SRM)
 */
package com.hoyavc.nxg.core.login;

import javax.xml.bind.annotation.XmlRootElement;

import com.hoyavc.nxg.core.ValueObject;

/**
 * InVO to get rest retry details.
 * 
 */
@XmlRootElement(name = "com.hoyavc.nxg.core.login.RestRetryVo")
public class RestRetryVo implements ValueObject {

	/** use serialVersionUID from JDK 1.0.2 for interoperability. */
	private static final long serialVersionUID = 1L;
	
	/** session id.*/
	private String sessionId;
	
	/** sequence number.*/
	private String sequenceNumber;

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the sequenceNumber
	 */
	public String getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * @param sequenceNumber the sequenceNumber to set
	 */
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
}
