/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core.message;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;

import com.hoyavc.nxg.core.InitializationErrorLogger;
import com.hoyavc.nxg.core.Message;
import com.hoyavc.nxg.core.MessageCode;
import com.hoyavc.nxg.core.MessageManager;
import com.hoyavc.nxg.core.NXGLogger;
import com.hoyavc.nxg.core.SystemException;

/**
 * Class to get message depends on the messageCode and localeKey. The messages
 * can be formatted with the message parameters.
 * 
 * @see MessageContainerWithStream
 */
class DefaultMessageManager implements MessageManager {

	/** message container. */
	private MessageContainer messageContainer;

	/** message formatter. */
	private MessageFormatter messageFormatter;

	/**
	 * Method to get the message for the messageCode and localeKey.
	 * 
	 * <p>
	 * <ul>
	 * <li>If messageCode is null then returns ""
	 * <li>If localeKey is null then returns ""
	 * <li>If localeKey don't match with available locale then returns default
	 * message from default message properties file.
	 * <li>If messageCode is not found for the given localeKey then returns
	 * default message from default message properties file. In case not
	 * available in default also, it returns "".
	 * </ul>
	 * </p>
	 * 
	 * @see MessageContainerWithStream
	 * 
	 * @param messageCode
	 *            MessageCode
	 * @param localeKey
	 *            LocaleKay
	 * @return String message
	 */
	public String getMessage(final MessageCode messageCode,
			final String localeKey) {
		return findMessage(messageCode, localeKey);
	}

	/**
	 * Method to get the message for the messageCode and localeKey, format the
	 * message with the given messageParam.
	 * 
	 * <p>
	 * <ul>
	 * <li>If messageCode is null then returns ""
	 * <li>If localeKey is null then returns ""
	 * <li>If localeKey don't match with available locale then returns default
	 * message from default message properties file.
	 * <li>If messageCode is not found for the given localeKey then returns
	 * default message from default message properties file. In case not
	 * available in default also, it returns "".
	 * </ul>
	 * </p>
	 * 
	 * @see MessageContainerWithStream
	 * 
	 * @param messageCode
	 *            MessageCode
	 * @param localeKey
	 *            LocaleKay
	 * @param messageParam
	 *            MessageParameter
	 * @return String formatted message
	 */
	public String getMessage(final MessageCode messageCode,
			final String localeKey, final String[] messageParam) {

		String rowMessage = findMessage(messageCode, localeKey);

		if (messageParam == null || messageParam.length <= 0) {
			return rowMessage;
		}
		return formatMessage(rowMessage, messageParam);

	}

	/**
	 * Method to find message string.
	 * 
	 * <p>
	 * <ul>
	 * <li>If messageCode is null then returns ""
	 * <li>If localeKey is null then returns ""
	 * <li>If localeKey don't match with available locale then returns default
	 * message from default message properties file.
	 * <li>If messageCode is not found for the given localeKey then returns
	 * default message from default message properties file. In case not
	 * available in default also, it returns "".
	 * </ul>
	 * </p>
	 * 
	 * @see MessageContainerWithStream
	 * 
	 * @param messageCode
	 *            MessageCode
	 * @param localeKey
	 *            LocaleKey
	 * @return string
	 */
	private String findMessage(final MessageCode messageCode,
			final String localeKey) {

		if (!isValidParams(messageCode, localeKey)) {
			return "";
		}

		String message = messageContainer.getMessage(messageCode, localeKey);

		if (message == null) {
			return "";
		} else {
			return message;
		}

	}

	/**
	 * Method to format message.
	 * 
	 * @param rowMessage
	 *            Message string
	 * @param messageParam
	 *            message parameters
	 * @return String formatted string
	 */
	private String formatMessage(final String rowMessage,
			final String[] messageParam) {

		return messageFormatter.format(rowMessage, messageParam);
	}

	/**
	 * Method to Validating parameter.
	 * 
	 * @param messageCode
	 *            MessageCode
	 * @param localeKey
	 *            Localekey
	 * @return boolean true if Valid, else false in any case
	 *         <ul>
	 *         <li>If messageCode is null
	 *         <li>If localeKey is null
	 *         </ul>
	 */
	private boolean isValidParams(final MessageCode messageCode,
			final String localeKey) {
		if (messageCode == null) {
			return false;
		}

		if (localeKey == null) {
			return false;
		}

		return true;
	}

	/**
	 * getter method for messageContainer.
	 * 
	 * @return messageContainer
	 */
	public MessageContainer getMessageContainer() {
		return messageContainer;
	}

	/**
	 * setter method for MessageContainer.
	 * 
	 * @param messageContainer
	 *            Message Container
	 * @throws SystemException
	 *             If messageContainer is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setMessageContainer(final MessageContainer messageContainer) {

		if (messageContainer == null) {
			String[] params = new String[] { MessageContainer.class.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.messageContainer = messageContainer;
	}

	/**
	 * getter method for messageFormatter.
	 * 
	 * @return messageFormatter
	 */
	public MessageFormatter getMessageFormatter() {
		return messageFormatter;
	}

	/**
	 * setter method for messageFormatter.
	 * 
	 * @param messageFormatter
	 *            Message Formatter
	 * @throws SystemException
	 *             If messageFormatter is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setMessageFormatter(final MessageFormatter messageFormatter) {

		if (messageFormatter == null) {
			String[] params = new String[] { MessageFormatter.class.getName() };
			Message message = fjff40001.createMessage(params);

			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.messageFormatter = messageFormatter;
	}

}
