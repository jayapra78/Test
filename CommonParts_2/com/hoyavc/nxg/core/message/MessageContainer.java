/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core.message;

import com.hoyavc.nxg.core.MessageCode;

/**
 * Interface for the MessageContainer classes.
 * 
 */
interface MessageContainer {

	/**
	 * Method to get the message for the given message code and localeKey.
	 * 
	 * @param messageCode
	 *            Message Code
	 * @param localeKey
	 *            Locale
	 * @return String message
	 */
	String getMessage(final MessageCode messageCode, final String localeKey);

}
