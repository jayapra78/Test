/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)
 *  2010/11/12 <Change NO.0004> Modified to load message_fw.properties file by Kalai.P (SRM)  
 *  2011/11/07 <Change NO.0005> Modified to load message_fw_(locale).properties file by Kalai.P (SRM)  
 */
package com.hoyavc.nxg.core.message;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40001;
import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40002;
import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjff40003;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import com.hoyavc.nxg.core.InitialProcess;
import com.hoyavc.nxg.core.InitializationErrorLogger;
import com.hoyavc.nxg.core.Message;
import com.hoyavc.nxg.core.MessageCode;
import com.hoyavc.nxg.core.NXGLogger;
import com.hoyavc.nxg.core.SystemException;

/**
 * Class to manage the message, messageCode and localeKey as HashMap.
 * 
 * <p>
 * Spring framework loads all the message into HashMap at the initialization
 * time. NXG framework uses this class to get the message for the specific
 * messageCode and localeKey.
 * </p>
 * 
 * 
 * Message Properties Files
 * <p>
 * The properties file
 * {@link MessageContainerWithStream#filePathProperties filePathProperties}
 * contains all the message properties file name as Locale as key and file name
 * as value
 * 
 * <pre>
 *                     example 
 *                     : /com/hoyavc/nxg/message/message.properties
 *                     fw = /com/hoyavc/nxg/message/message_fw.properties
 *                     jp = /com/hoyavc/nxg/message/message_jp.properties 
 * </pre>
 * 
 * <p>
 * Each properties files contains &quot;messagecode = message&quot; pairs
 * 
 * <pre>
 *                     example 
 *                     fjfe00520 = key is null or empty 
 *                     fjfe00521 = Column is not Number Format {0} 
 *                     fjfe00522 = Column is not Date Type {0}
 * </pre>
 * 
 */
public class MessageContainerWithStream implements InitialProcess,
		MessageContainer {

	/** Properties file path. */
	private String filePathProperties;

	/** each locale's properties file path. */
	private Map<String, String> filePathMap;

	/** Map containing all locale maps. */
	private Map<String, Map<String, String>> messageFileMap;

	/** NXGLogger for logging messages. */
	private NXGLogger initializationErrorLogger = InitializationErrorLogger
			.getInstance(this.getClass());
	
	/** To store constant for framework property file key. */
	private static final String FW_MESSAGE_PROP_FILE_KEY = "fw"; 

	/**
	 * getter method for filePathProperties.
	 * 
	 * @return filePathProperties
	 */
	public String getFilePathProperties() {
		return filePathProperties;
	}

	/**
	 * setter method for filePathProperties.
	 * 
	 * @param filePathProperties
	 *            File Path Properties
	 * @throws SystemException
	 *             If filePathProperties is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40001 fjff40001})
	 */
	public void setFilePathProperties(final String filePathProperties) {

		if (filePathProperties == null
				|| filePathProperties.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName() };
			Message message = fjff40001.createMessage(params);

			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		this.filePathProperties = filePathProperties;
	}

	// public Map<String,String> getFilePathMap() {
	// return filePathMap;
	// }
	//
	// public void setFilePathMap(Map<String,String> messageFileMap) {
	// this.filePathMap = messageFileMap;
	// }

	/**
	 * Init method which the spring framework, invokes this at the initialization
	 * time.
	 * <p>
	 * filePathProperties which contains all the properties files with the key
	 * locale. It loads each properties file into {@link HashMap} as
	 * Key(MessageCode) and Object(Message). All these HashMaps gather into a
	 * single HashMap as Key(LocaleKey) and Object(locale HashMap).
	 * </p>
	 */
	public void init() {

		filePathMap = createFilePathMap(filePathProperties);

		messageFileMap = new HashMap<String, Map<String, String>>();

		for (String fileKey : filePathMap.keySet()) {
			
			String fileName = filePathMap.get(fileKey);

			Map<String, String> messageMap = this.createMessageMap(fileName);

			if(FW_MESSAGE_PROP_FILE_KEY.equals(fileKey)){
				fileKey = "";
			} else if(fileKey.contains(FW_MESSAGE_PROP_FILE_KEY)){
				StringBuilder sblReplace = new StringBuilder(FW_MESSAGE_PROP_FILE_KEY);
				sblReplace.append("_");
				fileKey = fileKey.replace(sblReplace.toString(), "");
			}
			Map<String, String> previousMessageMap = messageFileMap.get(fileKey);
			if (previousMessageMap != null) {
				previousMessageMap.putAll(messageMap);
			} else {
				// put
				messageFileMap.put(fileKey, messageMap);
			}

		}

	}

	/**
	 * Method to create {@link HashMap} for the properties file
	 * <code>filePathProperties</code>.
	 * 
	 * @param filePathProperties
	 *            File Path Properties
	 * @return Map<String, String>
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If filePathProperties is null or empty ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40002 fjff40002})
	 *             <li>If MissingResourceException occurs ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40003 fjff40003})
	 *             </ul>
	 */
	private Map<String, String> createFilePathMap(
			final String filePathProperties) {

		// validating parameter
		if (filePathProperties == null
				|| filePathProperties.trim().length() <= 0) {

			String[] params = new String[] { String.class.getName(), null };
			Message message = fjff40002.createMessage(params);

			initializationErrorLogger
					.fatal(message, new NullPointerException());

			throw new SystemException(message, new NullPointerException());
		}

		ResourceBundle bundle;
		try {
			bundle = ResourceBundle.getBundle(filePathProperties);
		} catch (MissingResourceException e) {
			String[] params = new String[] { filePathProperties };
			Message message = fjff40003.createMessage(params);

			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		Map<String, String> filePathMap = new HashMap<String, String>();

		for (String key : Collections.list(bundle.getKeys())) {
			filePathMap.put(key, bundle.getString(key));
		}

		return filePathMap;
	}

	/**
	 * Creating Message map for the given properties file.
	 * 
	 * MessageCode as key and message string as object for the HashMap.
	 * 
	 * @param fileName
	 *            Properties file name
	 * @return Map<String, String>
	 * @throws SystemException
	 *             any of the case
	 *             <ul>
	 *             <li>If fileName is null or empty ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40002 fjff40002})
	 *             <li>If read file stream is null ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40003 fjff40003})
	 *             <li>If IOException occurs ({@link com.hoyavc.nxg.core.CoreFatalMessageCodeEnum#fjff40003 fjff40003})
	 *             </ul>
	 */
	@SuppressWarnings("unchecked")
	private Map<String, String> createMessageMap(final String fileName) {

		// validating parameter
		if (fileName == null || fileName.trim().length() <= 0) {
			String[] params = new String[] { String.class.getName(), null };
			Message message = fjff40002.createMessage(params);

			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());
		}

		// reading properties file into stream
		InputStream is = MessageContainerWithStream.class
				.getResourceAsStream(fileName);

		if (is == null) {
			String[] params = new String[] { fileName };
			Message message = fjff40003.createMessage(params);

			initializationErrorLogger
					.fatal(message, new NullPointerException());
			throw new SystemException(message, new NullPointerException());

		}
		Map<String, String> messageMap = null;

		BufferedInputStream bufferedInputStream = null;
		try {
			bufferedInputStream = new BufferedInputStream(is);
			Properties properties = new Properties();
			properties.load(bufferedInputStream);
			messageMap = new HashMap(properties);
		} catch (IOException e) {
			String[] params = new String[] { fileName };
			Message message = fjff40003.createMessage(params);

			initializationErrorLogger.fatal(message, e);
			throw new SystemException(message, e);
		} finally {
			if (bufferedInputStream != null) {
				try {
					bufferedInputStream.close();
				} catch (IOException e) {
					String[] params = new String[] { fileName };
					Message message = fjff40003.createMessage(params);

					initializationErrorLogger.fatal(message, e);
					throw new SystemException(message, e);
				}
			}
		}

		return messageMap;

	}

	/**
	 * Method to get Message using message code and locale key.
	 * 
	 * <p>
	 * This method gets the message string from the given locale key, if the
	 * value is not available it parse the locale key and search. Finally if not
	 * available in the locale key, it will return the default message string
	 * for the message code.
	 * 
	 * Example: <br>
	 * given: message code - <code>fjff40003</code>, locale key -
	 * <code>us_en</code> <br>
	 * 1) Search in <code>us_en</code> - if available it returns, else <br>
	 * 2) Search in <code>us</code> - if available it returns, else <br>
	 * 3) Search in <code>""</code> - if available it returns, else <br>
	 * 4) returns null<br>
	 * </p>
	 * 
	 * @param messageCode
	 *            Message Code
	 * @param localeKey
	 *            Locale Key
	 * @return String relevant message. Otherwise it will return null.
	 */
	public String getMessage(final MessageCode messageCode,
			final String localeKey) {

		if (messageCode == null || localeKey == null) {
			return null;
		}

		Map<String, String> messageMap = messageFileMap.get(localeKey);

		String message = messageMap == null ? null : messageMap.get(messageCode
				.toString());

		if (message == null) {
			if (localeKey.equals("")) {
				return null;
			}

			int _index = localeKey.indexOf("_");
			if (_index > 0) {
				// recursive call
				return getMessage(messageCode, localeKey.substring(0, _index));
			} else {
				// recursive call
				return getMessage(messageCode, ""); // default
			}

		}

		return message;
	}

}
