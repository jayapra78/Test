/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/03/13 <Change NO.0002> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core.message;

/**
 * Interface for message formatter classes.
 */
interface MessageFormatter {

	/**
	 * Method to format the message string with message parameters.
	 * 
	 * @param rowMessage
	 *            Message String
	 * @param messageParam
	 *            Message Parameter
	 * @return String Formatted message
	 */
	String format(final String rowMessage,
			final String[] messageParam);

}
