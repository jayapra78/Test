/*
 * Copyright 2007 by NXG Project, All rights reserved.
 *
 *  Change Tracking                                         
 *  2006/11/28 <Change NO.0001> created by Nobuo Fukuda (VCIT) 
 *  2007/02/20 <Change NO.0002> Add Param Validation by Baskaran (SRM)  
 *  2007/03/13 <Change NO.0003> Add JavaDoc by Baskaran (SRM)  
 */
package com.hoyavc.nxg.core.message;

import static com.hoyavc.nxg.core.CoreFatalMessageCodeEnum.fjfw50001;

import java.text.MessageFormat;

import com.hoyavc.nxg.core.InitializationErrorLogger;
import com.hoyavc.nxg.core.Message;
import com.hoyavc.nxg.core.NXGLogger;

/**
 * Class to format the message string with message parameters.
 * 
 */
class SimpleMessageFormatter implements MessageFormatter {

	/**
	 * Method to format the message string with message parameters.
	 * <p>
	 * <ul>
	 * <li>If rowMessage is null or empty then returns the rowMessage as it is.
	 * <li>If messageParam is null or 0 length then returns the rowMessage as
	 * it is.
	 * <li>If IllegalArgumentException occurs then returns the rowMessage as it
	 * is.
	 * </ul>
	 * </p>
	 * 
	 * @param rowMessage
	 *            Message String
	 * @param messageParam
	 *            Message Parameter
	 * @return String Formatted message
	 */
	public String format(final String rowMessage, final String[] messageParam) {

		// parameter validating
		if (rowMessage == null || rowMessage.trim().length() <= 0) {
			return rowMessage;
		}

		if (messageParam == null || messageParam.length <= 0) {
			return rowMessage;
		}

		// MessageFormat mf = new MessageFormat(rowMessage);

		// no need pass Locle since a format like "{1,time,yyyy}" can not be
		// used because parameter is String array.
		try {
			if (rowMessage.indexOf("'") >= 0) {
				String tempRowMessage = rowMessage.replaceAll("'", "''");
				return MessageFormat.format(tempRowMessage,
						(Object[]) messageParam);
			} else {
				return MessageFormat
						.format(rowMessage, (Object[]) messageParam);
			}
		} catch (IllegalArgumentException e) {
			NXGLogger initializationErrorLogger = InitializationErrorLogger
					.getInstance(this.getClass());
			Message message = fjfw50001
					.createMessage(new String[] { rowMessage });
			initializationErrorLogger.warn(message, e);

			return rowMessage;
		}

	}
}
