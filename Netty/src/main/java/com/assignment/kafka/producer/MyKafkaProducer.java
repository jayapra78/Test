package com.assignment.kafka.producer;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.simple.JSONObject;

public class MyKafkaProducer {

	static MyKafkaProducer instance = null;
	Properties props = new Properties();
	
	Producer<String, JSONObject> producer = null;
	
	private MyKafkaProducer() {
		init();
	}

	private void init() {
		
		props.put("bootstrap.servers", "localhost:9092");
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		producer = new KafkaProducer<String, JSONObject>(props);
	}
	
	public static MyKafkaProducer getInstance(){
		
		if(null == instance){
			instance = new MyKafkaProducer();
		}
		return instance;
	}
	
	public void send(JSONObject dataToSend) throws Exception {
		try{
			producer.send(new ProducerRecord<String, JSONObject>("myTopic", Integer.toString(1), dataToSend));
			System.out.println("Sent to Kafka Queue Success");
		}catch(Exception e){
			throw e;
		}
	}
	
	public void close() {
		producer.close();;
	}
	
	protected void finalize() throws Throwable{
		producer.close();;
	}
}
