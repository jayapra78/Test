package com.assignment.nettyserver;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import com.assignment.kafka.producer.MyKafkaProducer;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.QueryStringDecoder;

public class HttpServerHandler extends ChannelInboundHandlerAdapter {

	MyKafkaProducer prod = null;
	
	public HttpServerHandler() {
		prod = MyKafkaProducer.getInstance();
	}
	
	@Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception
    {
      if (msg instanceof FullHttpRequest)
      {
        final FullHttpRequest request = (FullHttpRequest) msg;
        
        //create JSON object from request URI
        JSONObject json = this.createJsonFromQueryParam(request);
        
        if(json == null) return;
        
        String responseMessage = null;
        //send JSON to KafkaQueue
        if(this.sendJsonToKafkaQueue(json)){
        	responseMessage = this.createResponseMessage(json, true);
        }else{
        	responseMessage = this.createResponseMessage(json, false);
        }
        
		FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK,
					Unpooled.copiedBuffer(responseMessage.getBytes()));

        if (HttpHeaders.isKeepAlive(request))
        {
          response.headers().set(
            HttpHeaders.Names.CONNECTION,
            HttpHeaders.Values.KEEP_ALIVE
          );
        }
        response.headers().set(HttpHeaders.Names.CONTENT_TYPE, "text/plain");
        response.headers().set(HttpHeaders.Names.CONTENT_LENGTH, responseMessage.length());
                            
        ctx.writeAndFlush(response);
        System.out.println("HTTP Responce sent..");
      }
      else
      {
        super.channelRead(ctx, msg);
      }
    }
    
    @SuppressWarnings("unchecked")
	public JSONObject createJsonFromQueryParam(final FullHttpRequest request){
    	
    	System.out.println("Received Uri : " + request.getUri());
    	QueryStringDecoder queryDecoder = new QueryStringDecoder(request.getUri(),true);
        Map<String, List<String>> parameters = queryDecoder.parameters();
        if(parameters.keySet().size() == 0) return null;
        JSONObject json = new JSONObject();
        
        for(String key: parameters.keySet()){
        	json.put(key, parameters.get(key).get(0));
        }
        System.out.println("Created JSON String : " + System.lineSeparator() + json.toJSONString());
        return json;
    }
    
    public boolean sendJsonToKafkaQueue(JSONObject json) {
    	try{
    		prod.send(json);
    	}catch(Exception e){
    		System.out.println("Error occured while sendin JSON object :" + e.getMessage());
    		return false;
    	}
    	return true;
	}
    
    public String createResponseMessage(JSONObject json, boolean isSendSuccess) {
		StringBuilder responseHeaderText = new StringBuilder();
		if(isSendSuccess){
			responseHeaderText.append("Successfully sent to Kafka Queue");
		}else{
			responseHeaderText.append("Error occured while sendin JSON object");
		}
		
		responseHeaderText.append(System.lineSeparator());
		responseHeaderText.append("--------------------------");
		responseHeaderText.append(System.lineSeparator());
		
		return responseHeaderText.append(json.toJSONString()).toString();
	}

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx)
      throws Exception
    {
      ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx,
      Throwable cause) throws Exception
    {
      ctx.writeAndFlush(new DefaultFullHttpResponse(
        HttpVersion.HTTP_1_1,
        HttpResponseStatus.INTERNAL_SERVER_ERROR,
        Unpooled.copiedBuffer(cause.getMessage().getBytes())
      ));
      
    }          
    
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        ctx.fireChannelUnregistered();
        prod.close();
    }
}
