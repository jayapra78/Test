package com.assignment.nettyserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NettyHttpServer
{
  private ChannelFuture channel;
  private final EventLoopGroup masterGroup;
  private final EventLoopGroup slaveGroup;
  private final Integer port = 8081;
  
  public NettyHttpServer()
  {
    masterGroup = new NioEventLoopGroup();
    slaveGroup = new NioEventLoopGroup();        
  }

  public void start() 
  {
	  
    Runtime.getRuntime().addShutdownHook(new Thread()
    {
      @Override
      public void run() { shutdown(); }
    });
        
    try
    {
      final ServerBootstrap bootstrap = new ServerBootstrap();
      bootstrap.group(masterGroup, slaveGroup);
      bootstrap.channel(NioServerSocketChannel.class);
      bootstrap.childHandler(new HTTPInitializer());
      
      bootstrap.option(ChannelOption.SO_BACKLOG, 128);
      bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
      channel = bootstrap.bind(port).sync();
      
      System.out.println("NettyHttpServer Started at port :" + port);
    }
    catch (final InterruptedException e) { 
    	System.out.println("Exception in: " + this.getClass().getName() + " - " + e.getMessage());
    }
  }
    
  public void shutdown() 
  {
    slaveGroup.shutdownGracefully();
    masterGroup.shutdownGracefully();

    try
    {
      channel.channel().closeFuture().sync();
      System.out.println("Shutdown Success..");
    }
    catch (InterruptedException e) {
    	System.out.println("Exception in: " + this.getClass().getName() + " - " + e.getMessage());
    }
  }

  //invoke main to start server.
  public static void main(String[] args)
  {
    new NettyHttpServer().start();
  }
}