package com.practice.netty.Netty;

public class Test {
	public static void main() {
		Animal animal = new Animal();animal.methodA();animal.methodB();
		Animal ani_dog = new Dog();ani_dog.methodA();ani_dog.methodB();
		Animal ani_cat = new Cat();ani_cat.methodA();ani_cat.methodB();
		
		//Dog dog = new Animal();//Type mismatch: cannot convert from Animal to Dog
		Dog dog = (Dog) new Animal();dog.methodA();dog.methodB();dog.methodC();
		Cat cat = (Cat) new Animal();cat.methodA();cat.methodB();cat.methodD();
		
		//Dog dog1 = new Cat();
	}
}

class Animal{
	public void methodA(){
		System.out.println("Animal -> methodA"  );
	}
	public void methodB() {
		System.out.println("Animal -> methodB"  );
	}
}
class Dog extends Animal{
	public void methodA(){
		System.out.println("Dog -> methodA"  );
	}
	public void methodC() {
		System.out.println("Dog -> methodC"  );
	}
}
class Cat extends Animal{
	public void methodB() {
		System.out.println("Cat -> methodB"  );
	}
	public void methodD() {
		System.out.println("Cat -> methodD"  );
	}
}
