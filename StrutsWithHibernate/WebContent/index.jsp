<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>

<script>
	function changeToEditable(){
		var pass = document.createElement('test');
		pass.type = 'password';
		alert("s");
		document.body.appendChild(pass);
		pass.type = 'text';
		pass.value = 'Password';
	}
</script>
</head>
<body>
	
	<s:form action="addEmployee">
		<s:textfield name="empNo" label="Emp No"></s:textfield>
		<s:textfield name="empName" label="Emp Name"></s:textfield>
		<s:textfield name="city" label="City"></s:textfield>
		<s:textfield name="state" label="State"></s:textfield>
		<s:textfield name="pincode" label="Pincode"></s:textfield>

		<s:submit value="Save" method="execute"></s:submit>
		<s:submit value="Show" method="show"></s:submit>
		<s:submit value="ShowAll" method="showAll"></s:submit>
		
	</s:form>
	<table>
		<tr>
			<td width="15%">Emp No:</td>
			<td width="15%">Emp Name:</td>
			<td width="15%">City :</td>
			<td width="15%">State:</td>
			<td width="15%">PinCode:</td>
			<td width="30%"></td>
			<td width="30%"></td>
		</tr>
		<s:iterator value="employee">
			<tr>
				<td width="15%"><s:property value="empNo"></s:property></td>
				<td width="15%"><s:property value="empName"></s:property></td>
				<td width="15%"><s:property value="city"></s:property></td>
				<td width="15%"><s:property value="state"></s:property></td>
				<td width="15%"><s:property value="pincode"></s:property></td>
				<%-- <td width="30%"><s:submit value="Update" method="update"></s:submit></td>
					<td width="30%"><s:submit value="Delete" method="delete"></s:submit></td> --%>
				<td width="30%"><a onclick="changeToEditable()" href="#">Edit</a></td>
				<td width="30%"><a href="<s:url action="" />">Delete</a></td>
			</tr>
		</s:iterator>
	</table>

</body>
</html>