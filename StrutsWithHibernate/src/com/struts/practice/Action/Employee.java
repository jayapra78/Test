package com.struts.practice.Action;

import java.util.List;

import com.struts.practice.ActionDao.EmployeeDao;

public class Employee {

	private long empNo;
	private String empName;
	private String city, state, pincode;
	private List<Employee> employee;
	
	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	public long getEmpNo() {	
		return empNo;
	}

	public void setEmpNo(long empNo) {
		this.empNo = empNo;
	}

	public List<Employee> getEmployee() {
		return employee;
	}

	public void setEmployee(List<Employee> employee) {
		this.employee = employee;
	}
	
	public String execute() {
		EmployeeDao.store(this);
		showAll();
		return "success";
	}
	public String show() {
		employee = EmployeeDao.show(this);
		return "success";
	}
	public String showAll() {
		employee = EmployeeDao.showAll(this);
		return "success";
	}
	
}
