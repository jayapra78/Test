package com.struts.practice.ActionDao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.jdt.internal.compiler.ast.SuperReference;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.struts.practice.Action.Employee;
import com.struts.practice.commonParts.ContextLoaderListener;
import com.struts.practice.commonParts.HibernateConfiguration;

public class EmployeeDao extends ContextLoaderListener{

	public static String store(Employee emp) {

		Session session = HibernateConfiguration.getSessionFactoryInstance().openSession();
		Transaction t = session.beginTransaction();
		
		session.persist(emp);
		
		t.commit();
		session.close();
		
		
		return "success";
	}
	
	public static List<Employee> show(Employee emp) {
		Session session = HibernateConfiguration.getSessionFactoryInstance().openSession();
		Transaction t = session.beginTransaction();
		
	//	Employee ret = (Employee)session.load(Employee.class, emp.getEmpNo());
		Criteria cr = session.createCriteria(Employee.class);
		cr.add(Restrictions.eq("empNo", emp.getEmpNo()));
		Employee ret = (Employee)cr.uniqueResult();
		List<Employee> list = new ArrayList<Employee>();
		list.add(ret);
		t.commit();
		session.close();
	
		return list;
	}
	
	public static List<Employee> showAll(Employee emp) {
		Session session = HibernateConfiguration.getSessionFactoryInstance().openSession();
		Transaction t = session.beginTransaction();
		
		Query query = session.createQuery("from Employee");
		
		List<Employee> list = query.list();
		t.commit();
		session.close();
	
		return list;
	}
}
