package com.struts.practice.commonParts;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ContextLoaderListener implements ServletContextListener {

	public SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("\n\n\n\n   *******       ContextLoaderListener has started        *******  \n\n\n\n ");
		//sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		HibernateConfiguration.getSessionFactoryInstance();
		System.out.println("\n\n\n\n   *******       ContextLoaderListener has finished        *******  \n\n\n\n ");
	}
	
}
