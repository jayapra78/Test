package com.struts.practice.commonParts;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateConfiguration {
	private static SessionFactory sessionFactory = null;
	
	private HibernateConfiguration(){
		
	}
	
	public static SessionFactory getSessionFactoryInstance() {
		if (sessionFactory == null) {
			sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		}
		return sessionFactory;
	}
}
