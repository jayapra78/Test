var modalWindow = {
	parent : "body",
	windowId : null,
	content : null,
	width : null,
	height : null,
	marginHeight : null,
	close : function() {
		jQuery(".modal-window").remove();
		jQuery(".modal-overlay").remove();
	},
	open : function() {
		var modal = "";
		modal += "<div class=\"modal-overlay\"></div>";
		modal += "<div id=\"" + this.windowId
				+ "\" class=\"modal-window\" style=\"width:" + this.width
				+ "px; height:" + this.height + "px; margin-top:-"
				+ (this.marginHeight-50) + "px; margin-left:-" + ((this.width / 2)-50)
				+ "px;\">";
		modal += this.content;
		modal += "</div>";

		jQuery(this.parent).append(modal);
		//jQuery(".modal-window").append("<a class='close-window' id='closeWindow'></a>");
		//jQuery(".close-window").click(function() {
			//modalWindow.close();
		//});
		//jQuery(".modal-overlay").click(function(){modalWindow.close();});
	}
};
var funOpenWindow = function(actionName, windowName, height, width, type,
		comboName, hiddenName, hiddenParam, marginHeight, modifyOneRecord,
		modifyRecord, title, OkButton, CancelButton) {

	var jsCheckBoxValue = "";
	var source = "";
	if (type == 'mod') {

		for ( var a = 1;; a++) {
			var jsCombo = document.getElementById(comboName + a);
			var jsHiddenValue = document.getElementById(hiddenName + a);
			if (jsCombo != 'undefined' && jsCombo != null) {
				if (jsCombo.checked) {
					if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
						jsCheckBoxValue = jsHiddenValue.value + ","
								+ jsCheckBoxValue;
					}
				}

			} else {
				break;
			}

		}

		if (jsCheckBoxValue.length > 1) {

			jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
					jsCheckBoxValue.length - 1);
			var jsSplitValue = jsCheckBoxValue.split(",");

			if (jsSplitValue.length > 1) {

				jAlert(modifyOneRecord, title, '', OkButton);
				return false;
			} else {
				actionName = actionName + "?" + hiddenParam + "="
						+ jsSplitValue;
				source = actionName;
			}
		} else {
			jAlert(modifyRecord, title, '', OkButton);
			return false;
		}

	} else {
		source = actionName;

	}

	modalWindow.windowId = windowName;
	modalWindow.width = width;
	modalWindow.height = height;
	modalWindow.marginHeight = marginHeight;
	modalWindow.content = "<iframe width='"
			+ width
			+ "' height='"
			+ height
			+ "'  frameborder='0' scrolling='auto' allowtransparency='true' src='"
			+ source + "'></iframe>";
	modalWindow.open();
};

function funSubmitForm(obj, action) {
	obj.action = action;
	obj.method = "POST";
	obj.submit();
}

function closeWindow() {
	parent.modalWindow.close();
}

function funCheckAll(checkAllComboName, comboName, hiddenName) {
	if (document.getElementById(checkAllComboName).checked == true) {
		for ( var a = 1;; a++) {
			var jsCombo = document.getElementById(comboName + a);
			var jsHiddenValue = document.getElementById(hiddenName + a);
			if (jsCombo != 'undefined' && jsCombo != null) {
				jsCombo.checked = true;
			} else {
				break;
			}

		}
	} else {
		for ( var a = 1;; a++) {
			var jsCombo = document.getElementById(comboName + a);
			var jsHiddenValue = document.getElementById(hiddenName + a);
			if (jsCombo != 'undefined' && jsCombo != null) {
				jsCombo.checked = false;
			} else {
				break;
			}

		}
	}

}

function removeSelectAll() {
	document.getElementById("checkAll").checked = false;
}

function funSubmitDelete(comboName, hiddenName, hiddenDeleteTagName, obj,
		action, confirmDelete, deleteRecord, confirmtitle, title, closeButton, okButton,
		cancelButton) {

	var jsCheckBoxValue = "";
	for ( var a = 1;; a++) {
		var jsCombo = document.getElementById(comboName + a);
		var jsHiddenValue = document.getElementById(hiddenName + a);
		if (jsCombo != 'undefined' && jsCombo != null) {
			if (jsCombo.checked) {
				if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
					jsCheckBoxValue = jsHiddenValue.value + ","
							+ jsCheckBoxValue;
				}
			}

		} else {
			break;
		}
	}

	if (jsCheckBoxValue.length > 1) {

		jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
				jsCheckBoxValue.length - 1);
		var jsSplitValue = jsCheckBoxValue.split(",");
		document.getElementById(hiddenDeleteTagName).value = jsSplitValue;
		jConfirm(confirmDelete, confirmtitle, okButton, cancelButton, function(
				r) {

			if (r) {
				obj.action = action;
				obj.method = "POST";
				obj.submit();
			} else {
				return false;
			}

		});

	} else {
		jAlert(deleteRecord, title, '', closeButton);
		return false;
	}

}

function funSubmitPermDelete(comboName, hiddenName, hiddenDeleteTagName, obj,
		action, confirmPermanentDelete, deleteRecord, Confirmtitle, title,
		OkButton, CancelButton) {
	var jsCheckBoxValue = "";
	for ( var a = 1;; a++) {
		var jsCombo = document.getElementById(comboName + a);
		var jsHiddenValue = document.getElementById(hiddenName + a);
		if (jsCombo != 'undefined' && jsCombo != null) {
			if (jsCombo.checked) {
				if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
					jsCheckBoxValue = jsHiddenValue.value + ","
							+ jsCheckBoxValue;
				}
			}

		} else {
			break;
		}
	}

	if (jsCheckBoxValue.length > 1) {

		jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
				jsCheckBoxValue.length - 1);
		var jsSplitValue = jsCheckBoxValue.split(",");
		document.getElementById(hiddenDeleteTagName).value = jsSplitValue;

		jConfirm(confirmPermanentDelete, Confirmtitle, OkButton, CancelButton,
				function(r) {

					if (r) {
						obj.action = action;
						obj.method = "POST";
						obj.submit();
					} else {
						return false;
					}

				});

	} else {
		jAlert(deleteRecord, title, '', OkButton);
		return false;
	}

}

function funSubmitRecover(comboName, hiddenName, hiddenDeleteTagName, obj,
		action, confirmRecover, recoverRecord, Confirmtitle, title, OkButton,
		CancelButton) {
	var jsCheckBoxValue = "";
	for ( var a = 1;; a++) {
		var jsCombo = document.getElementById(comboName + a);
		var jsHiddenValue = document.getElementById(hiddenName + a);
		if (jsCombo != 'undefined' && jsCombo != null) {
			if (jsCombo.checked) {
				if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
					jsCheckBoxValue = jsHiddenValue.value + ","
							+ jsCheckBoxValue;
				}
			}

		} else {
			break;
		}
	}

	if (jsCheckBoxValue.length > 1) {

		jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
				jsCheckBoxValue.length - 1);
		var jsSplitValue = jsCheckBoxValue.split(",");
		document.getElementById(hiddenDeleteTagName).value = jsSplitValue;

		jConfirm(confirmRecover, Confirmtitle, OkButton, CancelButton,
				function(r) {

					if (r) {
						obj.action = action;
						obj.method = "POST";
						obj.submit();
					} else {
						return false;
					}

				});

	} else {
		jAlert(recoverRecord, title, '', OkButton);
		return false;
	}

}

var funViewData = function(url, windowName, height, width, marginHeight) {
	source = url;
	modalWindow.windowId = windowName;
	modalWindow.width = width;
	modalWindow.height = height;
	modalWindow.marginHeight = marginHeight;
	modalWindow.content = "<iframe width='"
			+ width
			+ "' height='"
			+ height
			+ "'  frameborder='0' scrolling='auto' allowtransparency='true' src='"
			+ source + "'></iframe>";
	modalWindow.open();
}
/*
 * function funViewData1(url,pagename) { openWindow =
 * window.open(url,pagename,'height=900,width=800,status=yes,toolbar=no,menubar=no,location=no');
 * openWindow.focus(); }
 */

var funRecoverWindow = function(actionName, windowName, height, width,
		marginHeight) {

	source = actionName;

	modalWindow.windowId = windowName;
	modalWindow.width = width;
	modalWindow.height = height;
	modalWindow.marginHeight = marginHeight;
	modalWindow.content = "<iframe width='"
			+ width
			+ "' height='"
			+ height
			+ "'  frameborder='0' scrolling='auto' allowtransparency='true' src='"
			+ source + "'></iframe>";
	modalWindow.open();
};

// For Freeze and Scroll

function reposHead(e) {
	var h = document.getElementById('headscroll');
	h.scrollLeft = e.scrollLeft;
	var f = document.getElementById('divfrozen');
	f.scrollTop = e.scrollTop;
}
function reposHorizontal(e) {
	var h = document.getElementById('headscroll');
	var c = document.getElementById('contentscroll');
	h.scrollLeft = e.scrollLeft;
	c.scrollLeft = e.scrollLeft;

	// var sh = document.getElementById('hscrollpos');
	// sh.innerHTML = e.scrollLeft;

	var ch = document.getElementById('contentwidth');
	var ic = document.getElementById('innercontent');
	// ch.innerHTML = ic.clientWidth; //c.scrollWidth;

	var ch2 = document.getElementById('contentheight');
	// ch2.innerHTML = ic.clientHeight; //c.scrollWidth;

	var sp = document.getElementById('scrollwidth');
	// sp.innerHTML = e.scrollWidth;
}
function reposVertical(e) {
	var h = document.getElementById('divfrozen');
	var c = document.getElementById('contentscroll');
	h.scrollTop = e.scrollTop;
	c.scrollTop = e.scrollTop;

	// var sh = document.getElementById('vscrollpos');
	// sh.innerHTML = e.scrollTop;

	// var ch = document.getElementById('contentheight');
	// ch.innerHTML = c.scrollHeight;

	var sp = document.getElementById('scrollheight');
	// sp.innerHTML = e.scrollHeight;

}

function somefuncion() {
	var intFrezze = new Array();
	var intContent = new Array();
	var childs;
	if (document.getElementById('contentscroll') != null
			&& document.getElementById('contentscroll') != 'undefined') {
		childs = document.getElementById('contentscroll').childNodes;
	}

	var childs1;
	if (document.getElementById('divfrozen') != null
			&& document.getElementById('divfrozen') != 'undefined') {
		childs1 = document.getElementById('divfrozen').childNodes;
	}

	var bigheight1 = 0;
	var nil_count1 = 0;

	var bigheight = 0;
	var nil_count = 0;

	var nil_count2 = 0;

	if (childs != null && childs1 != null) {
		for ( var bnm = 0; bnm < (childs.length); bnm++) {

			if (childs[bnm].tagName == 'TABLE') {
				var tablechilds = childs[bnm].childNodes;
				var tablechilds1 = childs1[bnm].childNodes;

				for ( var ghj = 0; ghj < (tablechilds1.length); ghj++) {
					if (tablechilds1[ghj].tagName == 'TBODY') {
						var gettrs = tablechilds1[ghj].childNodes;
						for ( var dfg = 0; dfg < (gettrs.length); dfg++) {
							var trchilds = gettrs[dfg].childNodes;

							if (gettrs[dfg].tagName == 'TR') {
								nil_count1 = nil_count1 + 1;
								for ( var zxc = 0; zxc < (trchilds.length); zxc++) {

									if (trchilds[zxc].tagName == 'TD') {
										// alert(trchilds[zxc].offsetHeight);
										bigheight1 = trchilds[zxc].offsetHeight;
										if (trchilds[zxc].offsetHeight > bigheight1) {
											bigheight1 = trchilds[zxc].offsetHeight;
										} else {
											bigheight1 = bigheight1;
										}
									}
								}
								intFrezze[dfg] = bigheight1;
								// alert(bigheight1+'=='+nil_count);

								// $('#content-lines'+nil_count1).height(bigheight1+'px');
							}

						}
					}

				}

				for ( var ghj = 0; ghj < (tablechilds.length); ghj++) {
					if (tablechilds[ghj].tagName == 'TBODY') {
						var gettrs = tablechilds[ghj].childNodes;
						for ( var dfg = 0; dfg < (gettrs.length); dfg++) {
							var trchilds = gettrs[dfg].childNodes;

							if (gettrs[dfg].tagName == 'TR') {
								nil_count = nil_count + 1;
								for ( var zxc = 0; zxc < (trchilds.length); zxc++) {

									if (trchilds[zxc].tagName == 'TD') {
										// alert(trchilds[zxc].offsetHeight);
										bigheight = trchilds[zxc].offsetHeight;
										if (trchilds[zxc].offsetHeight > bigheight) {
											bigheight = trchilds[zxc].offsetHeight;
										} else {
											bigheight = bigheight;
										}
									}
								}
								// alert(bigheight+'=='+nil_count);
								intContent[dfg] = bigheight;
								// $('#freeze-lines'+nil_count).height(bigheight+'px');
							}

						}
					}

				}

				for ( var ghj = 0; ghj < (tablechilds.length); ghj++) {
					if (tablechilds[ghj].tagName == 'TBODY') {
						var gettrs = tablechilds[ghj].childNodes;
						for ( var dfg = 0; dfg < (gettrs.length); dfg++) {
							var trchilds = gettrs[dfg].childNodes;

							if (gettrs[dfg].tagName == 'TR') {
								nil_count2 = nil_count2 + 1;
								if (intFrezze[dfg] != intContent[dfg]) {
									if (intFrezze[dfg] < intContent[dfg]) {
										$('#freeze-lines' + nil_count2).height(
												intContent[dfg] + 'px');
									} else {
										$('#content-lines' + nil_count2)
												.height(intFrezze[dfg] + 'px');
									}
								}
							}

						}
					}

				}

			}
		}
	}

	var gotheight = $('#inner-lines').height();
	$('#freeze-lines').height(gotheight + 'px');
}

function numbersonly(e) {

	var unicode = e.charCode ? e.charCode : e.keyCode
	if (unicode != 8 && unicode != 9) { // if the key isn't the backspace key
		// and Tab Key (which we should allow)
		if (unicode < 48 || unicode > 57) // if not a number
			return false // disable key press
	}
}

function funSubmitFormModify(obj, action, confirmModifyPhoto, Confirmtitle,
		OkButton, CancelButton) {
	jConfirm(confirmModifyPhoto, Confirmtitle, OkButton, CancelButton,
			function(r) {

				if (r) {
					obj.action = action;
					obj.method = "POST";
					obj.submit();
				} else {
					return false;
				}

			});

}

function funSubmitRecoverString(comboName, hiddenName, hiddenDeleteTagName,
		obj, action, confirmRecover, recoverRecord, Confirmtitle, title,
		OkButton, CancelButton) {
	var jsCheckBoxValue = "";
	for ( var a = 1;; a++) {
		var jsCombo = document.getElementById(comboName + a);
		var jsHiddenValue = document.getElementById(hiddenName + a);
		if (jsCombo != 'undefined' && jsCombo != null) {
			if (jsCombo.checked) {
				if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
					jsCheckBoxValue = "'" + jsHiddenValue.value + "'" + ","
							+ jsCheckBoxValue;
				}
			}

		} else {
			break;
		}
	}

	if (jsCheckBoxValue.length > 1) {

		jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
				jsCheckBoxValue.length - 1);
		var jsSplitValue = jsCheckBoxValue.split(",");
		document.getElementById(hiddenDeleteTagName).value = jsSplitValue;
		jConfirm(confirmRecover, Confirmtitle, OkButton, CancelButton,
				function(r) {

					if (r) {
						obj.action = action;
						obj.method = "POST";
						obj.submit();
					} else {
						return false;
					}

				});

	} else {
		jAlert(recoverRecord, title, '', OkButton);

		return false;
	}

}

function funSubmitPermDeleteString(comboName, hiddenName, hiddenDeleteTagName,
		obj, action, confirmPermanentDelete, deleteRecord, confirmtitle, title,
		okButton, cancelButton) {
	var jsCheckBoxValue = "";
	for ( var a = 1;; a++) {
		var jsCombo = document.getElementById(comboName + a);
		var jsHiddenValue = document.getElementById(hiddenName + a);
		if (jsCombo != 'undefined' && jsCombo != null) {
			if (jsCombo.checked) {
				if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
					jsCheckBoxValue = "'" + jsHiddenValue.value + "'" + ","
							+ jsCheckBoxValue;
				}
			}

		} else {
			break;
		}
	}

	if (jsCheckBoxValue.length > 1) {

		jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
				jsCheckBoxValue.length - 1);
		var jsSplitValue = jsCheckBoxValue.split(",");
		document.getElementById(hiddenDeleteTagName).value = jsSplitValue;
		jConfirm(confirmPermanentDelete, confirmtitle, okButton, cancelButton,
				function(r) {

					if (r) {
						obj.action = action;
						obj.method = "POST";
						obj.submit();
					} else {
						return false;
					}

				});

	} else {
		jAlert(deleteRecord, title, '', okButton);
		return false;
	}

}

function funSubmitDeleteString(comboName, hiddenName, hiddenDeleteTagName, obj,
		action, confirmDelete, deleteRecord, confirmtitle, title, closeButton, okButton,
		cancelButton) {
	var jsCheckBoxValue = "";
	for ( var a = 1;; a++) {
		var jsCombo = document.getElementById(comboName + a);
		var jsHiddenValue = document.getElementById(hiddenName + a);
		if (jsCombo != 'undefined' && jsCombo != null) {
			if (jsCombo.checked) {
				if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
					jsCheckBoxValue = "'" + jsHiddenValue.value + "'" + ","
							+ jsCheckBoxValue;
				}
			}

		} else {
			break;
		}
	}

	if (jsCheckBoxValue.length > 1) {

		jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
				jsCheckBoxValue.length - 1);
		var jsSplitValue = jsCheckBoxValue.split(",");
		document.getElementById(hiddenDeleteTagName).value = jsSplitValue;

		jConfirm(confirmDelete, confirmtitle, okButton, cancelButton, function(
				r) {

			if (r) {
				obj.action = action;
				obj.method = "POST";
				obj.submit();
			} else {
				return false;
			}

		});

	} else {
		jAlert(deleteRecord, title, '', closeButton);
		return false;
	}
}

function funSubmitDeleteStringEmployee(selfId, comboName, hiddenName, hiddenDeleteTagName, obj,
		action, confirmSelfDelete, confirmDelete, deleteRecord, confirmtitle, title, okButton,
		cancelButton) {
	var selfDelete = false;
	var selfIdValue = document.getElementById(selfId).value;
	var jsCheckBoxValue = "";
	for ( var a = 1;; a++) {
		var jsCombo = document.getElementById(comboName + a);
		var jsHiddenValue = document.getElementById(hiddenName + a);
		if (jsCombo != 'undefined' && jsCombo != null) {
			if (jsCombo.checked) {
				if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
					jsCheckBoxValue = "'" + jsHiddenValue.value + "'" + ","
							+ jsCheckBoxValue;
					if(jsHiddenValue.value == selfIdValue){
						selfDelete = true;
					}
				}
			}

		} else {
			break;
		}
	}
	if(selfDelete){
		jAlert(confirmSelfDelete + selfIdValue, title, '', okButton);
		return false;
	}else{
		if (jsCheckBoxValue.length > 1) {
	
			jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
					jsCheckBoxValue.length - 1);
			var jsSplitValue = jsCheckBoxValue.split(",");
			document.getElementById(hiddenDeleteTagName).value = jsSplitValue;
	
			jConfirm(confirmDelete, confirmtitle, okButton, cancelButton, function(
					r) {
	
				if (r) {
					obj.action = action;
					obj.method = "POST";
					obj.submit();
				} else {
					return false;
				}
	
			});
	
		} else {
			jAlert(deleteRecord, title, '', okButton);
			return false;
		}
	}
}

function autoChange(firstNoName, secondNoName, maxNo) {
	var firstNo = document.getElementById(firstNoName).value;
	if (firstNo.length == maxNo) {
		document.getElementById(secondNoName).focus();
	}
}

var funopenwindow = function(url, windowName, height, width, marginHeight) {
	window.open(url, windowName);
}

var funopenwindowReport = function(url, windowName) {
	window
			.open(
					url,
					windowName,
					"toolbar=no,location=no,status=no,maximize=yes,menubar=no,scrollbars=yes,resizable=1,left=430,top=23");
}

var funWindowOpen = function(actionName, windowName, height, width, type,
		comboName, hiddenName, hiddenParam, marginHeight, modifyOneRecord,
		modifyRecord, title, closeButton, OkButton, CancelButton) {

	var jsCheckBoxValue = "";
	var source = "";
	if (type == 'mod') {

		for ( var a = 1;; a++) {
			var jsCombo = document.getElementById(comboName + a);
			var jsHiddenValue = document.getElementById(hiddenName + a);
			if (jsCombo != 'undefined' && jsCombo != null) {
				if (jsCombo.checked) {
					if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
						jsCheckBoxValue = jsHiddenValue.value + ","
								+ jsCheckBoxValue;
					}
				}

			} else {
				break;
			}

		}

		if (jsCheckBoxValue.length > 1) {

			jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
					jsCheckBoxValue.length - 1);
			var jsSplitValue = jsCheckBoxValue.split(",");

			if (jsSplitValue.length > 1) {
				jAlert(modifyOneRecord, title, '', OkButton);

				return false;
			} else {
				actionName = actionName + "&" + hiddenParam + "="
						+ jsSplitValue;
				source = actionName;
			}
		} else {
			jAlert(modifyRecord, title, '', closeButton);

			return false;
		}

	} else {
		source = actionName;
	}

	modalWindow.windowId = windowName;
	modalWindow.width = width;
	modalWindow.height = height;
	modalWindow.marginHeight = marginHeight;
	modalWindow.content = "<iframe width='"
			+ width
			+ "' height='"
			+ height
			+ "'  frameborder='0' scrolling='auto' allowtransparency='true' src='"
			+ source + "'></iframe>";
	modalWindow.open();
};

function funAlertWindow(action, confirmPermanentDelete, Confirmtitle, OkButton,
		CancelButton) {
	jConfirm(confirmPermanentDelete, Confirmtitle, OkButton, CancelButton,
			function(r) {
				if (r) {
					window.location = action;
				} else {
					return false;
				}
			});
};

function createCookie(cookieName, cookieValue) {
	var theDate = new Date();
	theDate.setFullYear(theDate.getFullYear() + 1);
	cookieName = escape(cookieName);
	cookieValue = escape(cookieValue);
	if (document.cookie != document.cookie) {
		index = document.cookie.indexOf(cookieName);
	} else {
		index = -1;
	}

	if (index == -1) {
		document.cookie = cookieName + "=" + cookieValue + "; expires="
				+ theDate;
	}
}

function getCookie(name) {
	name = escape(name)
	if (document.cookie) {
		index = document.cookie.indexOf("; " + name + "=");
		if (index < 0 && document.cookie.indexOf(name + "=") == 0)
			index = -2;
		else if (index < 0)
			return false;
		index += 2;
		if (index != -1) {
			cookieNameStart = (document.cookie.indexOf("=", index) + 1);
			cookieNameEnd = document.cookie.indexOf(";", index);
			if (cookieNameEnd == -1) {
				cookieNameEnd = document.cookie.length;
			}
			return unescape(document.cookie.substring(cookieNameStart,
					cookieNameEnd));
		}
	}
}

function newCookies() {
	var myUserName = document.getElementById('loginNameId').value;
	var myLanguage = document.getElementsByName('loginValue.language');
	for ( var x = 0; x < myLanguage.length; x++) {
		if (myLanguage[x].checked) {
			createCookie(myUserName, myLanguage[x].value);
		}
	}

}
function getLanguage() {
	var myUserName = document.getElementById('loginNameId').value;
	var selectedOption = getCookie(myUserName);
	if (selectedOption == 'en') {
		document.getElementById("languageIden").checked = true;
	} else if (selectedOption == 'ja') {
		document.getElementById("languageIdja").checked = true;
	}
}

/*
 * function del_cookie() { document.cookie = name +'=; expires=Thu, 01-Jan-70
 * 00:00:01 GMT;'; }
 */

function changeButtonFont(controlId) {
	document.getElementById(controlId).className = "btn-bg-home_focus";
}

function resetClass(controlId) {
	document.getElementById(controlId).className = "btn-bg-home";

}

function changeButtonPopup(controlId) {
	document.getElementById(controlId).className = "btn-bg_focus";
}

function resetClassPopup(controlId) {

	document.getElementById(controlId).className = "btn-bg";

}

// /////////////////Date Validation in Photo Master //////////////////////////

function isDate(dateStr, message, title, button) {

	var datePat = /^(\d{4})-(\d{2})-(\d{2})$/;

	var matchArray = dateStr.match(datePat); // is the format ok?
	if (matchArray == null) {
		jAlert(message, title, '', button);

		return false;
	}
	month = matchArray[2];
	year = matchArray[1];
	day = matchArray[3];

	if (month < 1 || month > 12) { // check month range
		jAlert(message, title, '', button);
		return false;
	}
	if (day < 1 || day > 31) {
		jAlert(message, title, '', button);
		return false;
	}
	if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
		jAlert(message, title, '', button);

		return false
	}
	if (month == 2) { // check for february 29th
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day > 29 || (day == 29 && !isleap)) {
			jAlert(message, title, '', button);

			return false;
		}
	}
	return true; // date is valid
}