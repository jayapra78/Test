/************************************************************************/
/*	File Name			: TAS.js									    */
/*	Author           	: Saravanan S                                   */
/*	Date of creation 	: 27/10/2004	                                */
/*	Description	    	: Methods for TAS Related operations	        */
/*	Modified By 	  	: Ragavan A S 						  			*/
/*	Revision Date    	: 31/01/2008									*/
/*	Revision History 	: 												*/
/*	 	1) Added "getProjectId" method for displaying Project Selection */
/*			Popup window												*/
/*	 	2) Added "setProjectCode" method for setting Project Code in 	*/
/*			the TimeSheet Entry Screen									*/
/*	 	3) Modified code in  clearRow method 							*/
/************************************************************************/



//**************************************************************************

//					 Methods for frmTimeSheetEntry 

//*************************************************************************** 


function cancelClick()
{
	document.frmTimeSheetEntry.method="post";
	document.frmTimeSheetEntry.action = 'frmTimeSheetDetails.jsp?mode=';
	document.frmTimeSheetEntry.submit();
}

//Open the Activity  popup Window
function Activity(intIndex)
{
	var layout ;	
	if ( navigator.appName == 'Netscape' )
	{
		layout = "width=590,height=400,maximize=no,noresize,scrollbars=no,top=200,left=170"
	}
	else
	{
		layout = "width=600,height=350,maximize=no,noresize,scrollbars=no,top=200,left=170"
	}
	var url = 'frmActivity.jsp?index='+intIndex;
	top.newWin = window.open(url,"activity",layout);
	//objActWindow=window.open(url,"activity",layout);
	objActWindow=top.newWin;
}

//Close the pop up windows while closing the forms
function EntryFormClose(objWin,objActWindow)
{
	if ( objWin != ""){
		top.newWin.close();
	}

	if (objActWindow !="" )
	{
		objActWindow.close();
	}
}



//clear the contents of each rows
function ClearRow(intIndex)
{

	var objPrjId1="";	
	var objPrjCode1="";
	var objActDec1="";
	var objDate1 = "";
	var objStartTime1 = "";
	var objEndTime1 = "";
	var objWorkItem1="";
	var objDetails1="";
	var objSize1="";
	var objUnit1="";
	var intRow = intIndex;
	
	objPrjId1 = eval("document.frmTimeSheetEntry.hProj"+intRow);
	objPrjCode1 = eval("document.frmTimeSheetEntry.txtProj"+intRow);
	objActDec1 = eval("document.frmTimeSheetEntry.txtAct"+intRow);
	objDate1 = eval("document.frmTimeSheetEntry.txtDate"+intRow);
	objStartTime1 = eval("document.frmTimeSheetEntry.lstStart"+intRow);
	objEndTime1 = eval("document.frmTimeSheetEntry.lstEnd"+intRow);
	objWorkItem1 = eval("document.frmTimeSheetEntry.txtWorkItem"+intRow);
	objDetails1 = eval("document.frmTimeSheetEntry.txtDetails"+intRow);
    objSize1 = eval("document.frmTimeSheetEntry.txtSize"+intRow);
    objUnit1 = eval("document.frmTimeSheetEntry.cboUnit"+intRow);

	objPrjId1.value = "";
	objPrjCode1.value = "";
	objActDec1.value="";
	objDate1.value="";

	objStartTime1.value=37;
	objEndTime1.value=38;

	objWorkItem1.value="";
	objDetails1.value="";
	objSize1.value="";
    objUnit1.value= 0;
}

 //Set the EndTime Grater than the StartTime while selecting the StartTime

function checkValue(intNo)
{

	
	var objStartTime1 = "";
	var objEndTime1 = "";
	var intRow=intNo;

	

	objStartTime1 =  eval("document.frmTimeSheetEntry.lstStart"+intRow);
	objEndTime1   =  eval("document.frmTimeSheetEntry.lstEnd"+intRow);

	

	if ( eval(objStartTime1.value) < 95){
		
		objEndTime1.value=eval(objStartTime1.value) + 1;
	}

	else
	{
		objEndTime1.value=eval(objStartTime1.value) + 0;
		
	}
	
}


//**************************************************************************

//					 Methods for frmTimeSheetDetails 

//*************************************************************************** 

//check and confirm the deleting Record is Selected or Not

function delData()
{
	intCount = document.frmTimeSheetDetails.hdnCnt.value;

	if ( intCount == 0 )
	{
		alert("There is no record selected to delete ! ");
		return;
	}
	var intLen = document.frmTimeSheetDetails.chkBox.length;
	var intFlag = 0;
	if ( document.frmTimeSheetDetails.chkBox.checked == true )
		{
			intFlag = 1;
	}
	for ( var intCtr=0;intCtr < intLen; intCtr++)
	{
		if ( document.frmTimeSheetDetails.chkBox[intCtr].checked == true)
		{
			intFlag = 1;
			break;
		}
	}
	if ( intFlag == 1 )
	{
		var confDelete=confirm(" Do you want to delete ? ");
		if(confDelete==true)
		{
			document.frmTimeSheetDetails.mode.value="Del";
			document.frmTimeSheetDetails.method="post";
			document.frmTimeSheetDetails.action = 'frmTimeSheetDetails.jsp?mode=Del';
			document.frmTimeSheetDetails.submit();
		}
	}
	else
	{
		alert("There is no record selected to delete ! ");
	}


}

//check and confirm the Modyfying Record is Selected or Not

function modifyData()
{
	intCount = document.frmTimeSheetDetails.hdnCnt.value;
	if ( intCount == 0 )
	{
		alert("There is no record selected to Modify ! ");
		return;
	}
	var intLen = document.frmTimeSheetDetails.chkBox.length;
	var intFlag = 0;
	if ( document.frmTimeSheetDetails.chkBox.checked == true )
	{
		intFlag = 1;
	}
	for ( var intCtr=0;intCtr < intLen; intCtr++)
	{
		if ( document.frmTimeSheetDetails.chkBox[intCtr].checked == true)
		{
			intFlag = 1;
			break;
		}
	}
	if ( intFlag == 1 )
	{
		document.frmTimeSheetDetails.action = 'frmTimeSheetEntry.jsp?mode=modify';
		
		document.frmTimeSheetDetails.method="post";
		document.frmTimeSheetDetails.mode.value="modify";
		document.frmTimeSheetDetails.submit();
	}
	else
	{
		alert("There is no record selected to Modify ! ");
	}

}
function addData()
{
	document.frmTimeSheetDetails.method="post";
	document.frmTimeSheetDetails.mode.value="";
	document.frmTimeSheetDetails.action = 'frmTimeSheetEntry.jsp?mode=';
	document.frmTimeSheetDetails.submit();
}

//set the Date
function setDate(obj)
{
	setDateField(obj);
	top.newWin = window.open('../include/calendar.jsp', 'cal', 'width=210,height=230,dependent=yes,width=210,height=290,left=330,top=230,titlebar=yes');
	objWin=top.newWin;
	//objWin=window.open('../include/calendar.jsp', 'cal', //'width=210,height=230,dependent=yes,width=210,height=290,left=330,top=230,titlebar=yes')
	

}
//closing the windows
function FormClose(objWin)
{
	if ( objWin != "")
	{
		top.newWin.close();
	}
}




//**************************************************************************

//					 Methods for frmActivity 

//*************************************************************************** 

//Set the Activuty in the Activity text while clicking the activity in the PopUp window 
function setActivity(intSelIndex)
{
	var strArr = new Array();
	var strVal ;
	var intIndex = document.frmActivity.lstAct.selectedIndex ;

	if (intIndex < 0 )
	{
		alert("Select any activity") ;
		document.frmActivity.lstAct.focus();
		return false;
	}

	strVal = document.frmActivity.lstAct.options[intIndex].value ;

	strArr = strVal.split(";")
	
	
	var len = window.opener.document.frmTimeSheetEntry.elements.length ;
	var index = 0 ;
	for(index=0; index<len; index++)
	{ 

		var intStart = window.opener.document.frmTimeSheetEntry.elements[index].name.indexOf("txtAct");
		var intLast = window.opener.document.frmTimeSheetEntry.elements[index].name.length;
		if ( intStart != -1)	
		{ 	
		if (  window.opener.document.frmTimeSheetEntry.elements[index].name.substring(6,intLast) == intSelIndex ) 
			{ 
				window.opener.document.frmTimeSheetEntry.elements[index].value=strArr[1];					
			} 	

		}
		
	}
	window.close();
		
}


function setEvent(cmb,idx) {
if(event.keyCode ==	13)
{
event.keyCode =	0;
setActivity(idx);
}

}

/**************************************************************************

					 Methods for lastEntry 

***************************************************************************/


//Set the Project Code

	function setCode()
	{
		var projName = document.LastEntry.selectProj.value;
		if(projName == "null"){
			document.LastEntry.txtCode.value = "";	
		}else{
			document.LastEntry.txtCode.value = projName;
		}
	}



	function ChkMethod()
	{
		var projName = document.LastEntry.selectProj.value;
		var projCode = document.LastEntry.txtCode.value;
		
		
				if(projCode == ""){
					alert(" Please Enter Project Code ");
					document.LastEntry.txtCode.focus();
					return false;
				}else{
					document.LastEntry.txtFlag.value=1;
					document.LastEntry.submit();
				}
			
				document.LastEntry.txtFlag.value=1;
				document.LastEntry.submit();
			
   }


   
/**************************************************************************

					 Methods for WeeklyReport

***************************************************************************/

//Converts the LowerCase Characters  to UpperCase

function convertToUpper(objEvent) 
{
	if ( ( objEvent.keyCode > 96 ) && ( objEvent.keyCode < 124 ) )
	{
		objEvent.keyCode = objEvent.keyCode - 32;
	}
}


function GetEmpStatus()
{
	if (document.frmTasReport.radio1[0].checked == true){
		document.frmTasReport.txtEmpId.disabled= true;
		document.frmTasReport.txtEmpId.value = "";
		document.frmTasReport.txtEmpOp.value="ALL";
	}
	else if(document.frmTasReport.radio1[1].checked == true){
		document.frmTasReport.txtEmpId.disabled= false;
		document.frmTasReport.txtEmpOp.value="IN";
	}
}

function GetActStatus()
{
	if (document.frmTasReport.radio2[0].checked == true){
		document.frmTasReport.txtActOp.value = "";
		document.frmTasReport.txtActOp.value = "ma";
	}
	else if(document.frmTasReport.radio2[1].checked == true){
		document.frmTasReport.txtActOp.value = "";
		document.frmTasReport.txtActOp.value = "mi";
	}
}

//Open the Project popup Window
function getProjectId(intIndex)
{
	var layout ;	
	if ( navigator.appName == 'Netscape' )
	{
		layout = "width=590,height=400,maximize=no,noresize,scrollbars=no,top=200,left=170"
	}
	else
	{
		layout = "width=500,height=400,maximize=no,noresize,scrollbars=no,top=200,left=170"
	}
	var url = 'frmProject.jsp?index='+intIndex;
	top.newWin = window.open(url,"projectid",layout);
	objActWindow=top.newWin;
}

//Set the Project Code on doubleclicking the project in the PopUp window 
function setProjectCode(intSelIndex)
{
	var strVal ;
	var intIndex = document.frmProject.lstProject.selectedIndex ;

	if (intIndex < 0 )
	{
		alert("Select any Project") ;
		document.frmProject.lstProject.focus();
		return false;
	}
	
	strVal = document.frmProject.lstProject.options[intIndex].value ;

	var doc = window.opener.document;
	var objPrjCode = doc.getElementsByName('txtProj' + intSelIndex)[0];
	var objPrjId = doc.getElementsByName('hProj' + intSelIndex)[0];
	var arrVal = strVal.split(",");

	objPrjId.value = arrVal[0];;
	objPrjCode.value = arrVal[1];
	
	window.close();
		
}

function setPrjEvent(cmb,idx) 
{
	if(event.keyCode ==	13)
	{
		event.keyCode =	0;
		setProjectCode(idx);
	}
}