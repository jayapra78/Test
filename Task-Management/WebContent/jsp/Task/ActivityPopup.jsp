<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<title>Activity Selection Screen</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel='stylesheet' type='text/css' href='../style/sofil.css'>
</head>
<script LANGUAGE="JavaScript" SRC="../include/TAS.js"></script>
<SCRIPT language = "javascript">
	function setActivity(intSelIndex){
		var strVal ;
		var intIndex = document.frmActivity.lstAct.selectedIndex ;
	
		if (intIndex < 0 ){
			alert("Select any Activity") ;
			document.frmProject.frmActivity.focus();
			return false;
		}
		
		strVal = document.frmActivity.lstAct.options[intIndex].value ;
	
		var doc = window.opener.document;
		//var objPrjCode = doc.getElementsByName('txtProj' + intSelIndex)[0];
		//var objPrjId = doc.getElementsByName('hProj' + intSelIndex)[0];
		
		var objPrjCode = doc.getElementById("${projectListId}");
		var objPrjId = doc.getElementById("${projectCodeId}");
		
		var arrVal = strVal.split(",");
	
		objPrjId.value = arrVal[0];
		objPrjCode.value = arrVal[1];
		
		window.close();
	}
	function isEnterKey(event){
		var keyCode = event.charCode ? event.charCode : event.keyCode;
		if (keyCode == 13) {
			return true;
		}
		return false;
	}
</SCRIPT>
<body class=lbody scroll="no" onKeyPress="setEvent(this,'1')" onLoad="JavaScript:frmActivity.lstAct.focus();" style="overflow-x:hidden;overflow-y:hidden;">
<FORM name = "frmActivity">
	<TABLE border = 0 width = 540>
		<TR>
			<TD>
				<SELECT name="lstAct" size=21 width=520 style="HEIGHT: 352px; WIDTH: 600px;" onkeypress="return (isEnterKey(event) && setActivity(1)) || true;" ondblClick="setActivity(1);" class=selactctrls>
				
					<s:iterator value="activityList" status="IteratorStatus">
    					<option value='${idValue},${key}'
    					<c:if test="${param.activityCode eq key}">selected="selected"</c:if>
    					>${name}</option>
    				</s:iterator>
    					
				</SELECT>
			</TD>
		</TR>
		
	</TABLE>
</FORM>
</body>
</html>
