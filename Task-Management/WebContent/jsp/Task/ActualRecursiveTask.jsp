﻿<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader("Expires", 0);
        response.setLocale(java.util.Locale.ENGLISH);
%>
<%  
	/* Getting Context Path */
	String contextPath = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Task Sheet-SRM Technologies</title>
		<sj:head  compressed="false"/>
		<sx:head debug="false" />
		<s:head theme="simple"/>
		<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/shirai.css" />
		<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
		<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="<%=contextPath%>/js/Popup.js"></script>
		<script type="text/javascript" src="<%=contextPath%>/js/Shirai.js"></script>
		<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/popup.css" />
		<style type="text/css">
.button_style{
	background-color : black;
	color: white;
	cursor: pointer;
	}
.logout_style {
	background: transparent;
	color: white;
	cursor: pointer;
}
</style>
		
<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }

</script>
<style type="text/css">
body{
	background-color:none;
	width:100%;
	margin:0px;
}
.main{
	width:1148px;
	margin:0px auto;
	background-image:url(../images/main-bg.png);
}
.page{
	margin:0px auto;
	width:1148px;
}

.top-header{
	width:1148px;
	float:left;
	background-image:url(../images/header-bg.png);
	height:75px;
}
.emp-details-part{
	width:1148px;
	float:left;
	background-color:#DDE4E8;
	height:105px;
}
.error-msg{
	width:1148px;
	float:left;
	background-color:#DDE4E8;
}
.main-table-part{
	width:1146px;
	float:left;
	background-color:#fff;
	border:#c8c6c6 solid 1px;
}
.save-btns-part{
	float:left;
	width:1148px;
	background-color:#f5f3f3;
}
.plan-lead-logo {
	float: left;
	width: 210px;
	height: 65px;
	padding: 0px 600px 0px 0px;
}

</style> 
</head>
<body>
<script>
	
	var currentDate = "${currentDateStr}";
	var currentTime = "${currentTime}";
	var sysTime     = parseFloat(currentTime.replace(/:/g,''));
	
	var projectAry = new Array();
	<c:forEach items="${allProjectList}" var="entry" varStatus="row">
		projectAry["${entry.key}"] = new Array();
		<c:forEach items="${entry.value}" var="project">
			projectAry["${entry.key}"]["${project.key}"] = "${project.idValue}:${project.refId}:${project.refId2}";
		</c:forEach>
	</c:forEach>
    
    var activityAry = new Array();
    <s:iterator value="activityList" status="IteratorStatus">
    	activityAry["${key}"] = "${idValue}";
    </s:iterator>
	
	function openSearchProjectPopup(){
		var projectTypeId = document.getElementById("projectTypeId").value;
		var title = "";
		if(projectTypeId=="0"){
			alert("Please select project type");
			return;
		}
		if(projectTypeId=="1"){
			title = "Project Selection Screen";
		}
		else if(projectTypeId=="2"){
			title = "Practice Team Selection Screen";
		}
		var selectedProjectCode = document.getElementById("projectCodeId").value;
		newwindow=window.open("mappedProjectList?projectListId=projectId&projectCodeId=projectCodeId&projectType="+projectTypeId+"&selectedProjectCode="+selectedProjectCode+"&statusId=0&title="+title,'newwindow','scrollbars=no,location=no,toolbar=no,resizable=no,menubar=0,height=430,width=517,left=200,top=200');
	}
    
	function openProjectPopup(projectCodeId,projectId,rowNo){
		
		var projectType = "1";
		var projectTypeId = document.getElementById("addTask_task_"+rowNo+"__projectTypeActionVo_projectTypeId");
		var title = "";
		if(projectTypeId){
			if(projectTypeId.value=="0"){
				alert("Please select project type");
				return;
			}
			projectType = document.getElementById("addTask_task_"+rowNo+"__projectTypeActionVo_projectTypeId").value;
			if(projectTypeId.value=="1"){
				title = "Project Selection Screen";
			}
			else if(projectTypeId.value=="2"){
				title = "Practice Team Selection Screen";
			}
		}
		selectedProjectCode = document.getElementById(projectCodeId).value;
		newwindow=window.open("mappedProjectList?projectListId="+projectId+"&projectCodeId="+projectCodeId+"&projectType="+projectType+"&selectedProjectCode="+selectedProjectCode+"&statusId=3&title="+title,'newwindow','scrollbars=no,location=0,toolbar=no,resizable=no,menubar=0,height=430,width=517,left=200,top=200');
	}
	
	function openActivityPopup(projectId,projectCodeId){
		var activityCode = document.getElementById(projectId).value;
		newwindow=window.open("activityList?projectListId="+projectId+"&projectCodeId="+projectCodeId+"&activityCode="+activityCode,'newwindow','scrollbars=no,location=0,toolbar=no,resizable=no,menubar=0,height=365,width=617,left=200,top=200');
	}
	function openRecursivePopup(url,projectCodeId){
		newwindow=window.open(url, 'newwindow','scrollbars=no,dependent=yes, titlebar=yes,location=0, menubar=0, height=400, width=700, left=200, top=200');
	}
	
	function updateManager(projectCode){
		toUpper(projectCode);
		var row = projectCode.id.substring(12);
		var projectTypeId = document.getElementById("addTask_task_"+row+"__projectTypeActionVo_projectTypeId").value;
		if(projectTypeId != "0" && projectAry[projectTypeId][projectCode.value]){
			var manager = projectAry[projectTypeId][projectCode.value].split(":")[1];
			if(manager!=""){
				document.getElementById("addTask_task_"+row+"__reportempId").value = manager;
				if(document.getElementById("addTask_task_"+row+"__reportempId").value==""){
					document.getElementById("addTask_task_"+row+"__reportempId").options[0].selected=true;
				}
			}
		}
	}
	function validateProjectAndActivity(submitObj){
		try{
			if(pageSubmitted){
				return false;
			}
			var rows = parseInt(document.getElementById("rows").value);
			var isInvalid = false;
			var errMsg = "";
			for(var row=0;row<rows;row++){
				if(document.getElementById("projectCode_"+row)){
					var startDateStrObj		= document.getElementById("startDateStrId");
					var endDateStrObj 		= document.getElementById("endDateStrId");
					var projectTypeIdObj	= document.getElementById("addTask_task_"+row+"__projectTypeActionVo_projectTypeId");
					var projectCodeObj		= document.getElementById("projectCode_"+row);
					var reportempIdObj 		= document.getElementById("addTask_task_"+row+"__reportempId");
					var planTaskObj    		= document.getElementById("planTask_"+row);
					var planStartTimeObj 	= document.getElementById("addTask_task_"+row+"__planStartTime");
					var planEndTimeObj 		= document.getElementById("addTask_task_"+row+"__planEndTime");
					var actualTaskObj 		= document.getElementById("actualTask_"+row);
					var actualStartTimeObj	= document.getElementById("addTask_task_"+row+"__actualStartTime");
					var actualEndTimeObj 	= document.getElementById("addTask_task_"+row+"__actualEndTime");
					var activityCodeObj 	= document.getElementById("activityCode_"+row);
					var workItemObj 		= document.getElementById("workItem_"+row);
					var size                = document.getElementById("size_"+row);
					var unitId              = document.getElementById("addTask_task_"+row+"__unitId");
					var associatedProjectObj= document.getElementById("associatedProjectId_"+row);
					var rowExist    = false;
					var planExist   = false;
					var actualExist = false;
					
					if(planTaskObj.value!="" || planStartTimeObj.value!="" || planEndTimeObj.value!=""){
						planExist = true;
					}
					if(actualTaskObj.value!="" || actualStartTimeObj.value!="" || actualEndTimeObj.value!="" || 
					   activityCodeObj.value!="" || workItemObj.value!="" || (size.value!="" && size.value!="0.0" && size.value!="0") || 
					   unitId.value!="0"){
						actualExist = true;
					}
					
					if(startDateStrObj.value==""){
						errMsg += parseErrorMsg("Start Date is required");
						isInvalid = true;
					} else if(!isValidDate(startDateStrObj)){
						errMsg += parseErrorMsg("Start Date is invalid. (Format - dd/mm/yyyy)");
						isInvalid = true;
					}
					if(endDateStrObj.value==""){
						errMsg += parseErrorMsg("End Date is required");
						isInvalid = true;
					} else if(!isValidDate(endDateStrObj)){
						errMsg += parseErrorMsg("End Date is invalid. (Format - dd/mm/yyyy)");
						isInvalid = true;
					}
					if(!isInvalid && compareDate(startDateStrObj.value,endDateStrObj.value).d1gtd2){
						errMsg += parseErrorMsg("Invalid Date range.");
						isInvalid = true;
					}
					
					if(planExist && compareDate(currentDate,startDateStrObj.value).d1gtd2){ // for plan
						errMsg += parseErrorMsg("For plan, date should be greater than or equal to current date");
						isInvalid = true;
					}
					if(actualExist && compareDate(endDateStrObj.value,currentDate).d1gtd2){ // for actual
						errMsg += parseErrorMsg("For actual, date should be less than or equal to current date");
						isInvalid = true;
					}
					
					var projectCode = document.getElementById("projectCode_"+row).value;
					var projectId   = document.getElementById("project_"+row);
					projectId.value = "";
					
					if(projectCode!="" && projectTypeIdObj.value=="0"){
						errMsg += parseErrorMsg("Project type is required");
						isInvalid = true;
					}
					if(projectTypeIdObj.value != "0" && projectAry[projectTypeIdObj.value][projectCode]){
						projectId.value = projectAry[projectTypeIdObj.value][projectCode].split(":")[0];
						if(projectTypeIdObj.value=="2"){
							associatedProjectObj.value=projectAry[projectTypeIdObj.value][projectCode].split(":")[2];
						}
					}
					else if(projectCode!=""){
						errMsg += parseErrorMsg("Project code is invalid");
						isInvalid = true;
					}
					if(reportempIdObj.value=="0"){
						errMsg += parseErrorMsg("Manager is required");
						isInvalid = true;
					}
					
					if(planExist){
						if(planTaskObj.value.replace(/ /g,"")==""){
							planTaskObj.value = "";
							errMsg += parseErrorMsg("Planned task description is required");
							isInvalid = true;
						}
						if(planTaskObj.value.length>128){
							errMsg += parseErrorMsg("Planned task description should not exceed 128 character");
							isInvalid = true;
						}
						if(planStartTimeObj.value==""){
							errMsg += parseErrorMsg("Planned start time is required");
							isInvalid = true;
						}
						if(planEndTimeObj.value==""){
							errMsg += parseErrorMsg("Planned end time is required");
							isInvalid = true;
						}
					}
					if(actualExist){
						if(actualTaskObj.value.replace(/ /g,"")==""){
							actualTaskObj.value = "";
							errMsg += parseErrorMsg("Actual task description is required");
							isInvalid = true;
						}
						if(actualTaskObj.value.length>128){
							errMsg += parseErrorMsg("Actual task description should not exceed 128 character");
							isInvalid = true;
						}
						if(actualStartTimeObj.value==""){
							errMsg += parseErrorMsg("Actual start time is required");
							isInvalid = true;
						}
						if(actualEndTimeObj.value==""){
							errMsg += parseErrorMsg("Actual end time is required");
							isInvalid = true;
						}
						if(activityCodeObj.value==""){
							errMsg += parseErrorMsg("Activity is required");
							isInvalid = true;
						}
						
						var activityCode = document.getElementById("activityCode_"+row).value;
						var activityId   = document.getElementById("activityId_"+row);
						activityId.value="";
						if(activityAry[activityCode]){
							activityId.value = activityAry[activityCode];
						}
						else if(activityCode!=""){
							errMsg += parseErrorMsg("Activity code is invalid");
							isInvalid = true;
						}
						
						if(workItemObj.value==""){
							errMsg += parseErrorMsg("Work Item is required");
							isInvalid = true;
						}
						if(size.value=="" && unitId.value!="0"){
							errMsg += parseErrorMsg("Size is required");
							isInvalid = true;
						}
						if(parseFloat(size.value)>=1000000){
							errMsg += parseErrorMsg("Size should be less than 1000000");
							isInvalid = true;
						}
						else if(size.value.indexOf(".")==-1 && size.value!=""){
							size.value=size.value+".0";
						}
						if(size.value.indexOf(".")==size.value.length-1 && size.value!=""){
							size.value=size.value+"0";
						}
						if((size.value!="" && size.value!="0.0" && size.value!="0") && unitId.value=="0"){
							errMsg += parseErrorMsg("Unit is required");
							isInvalid = true;
						}
					}
					if(!planExist && !actualExist){
						errMsg += parseErrorMsg("Plan or Actual is required");
						isInvalid = true;
					}
				}
			}
			
			if(isInvalid){
				showErrorMsg(errMsg);
				return false;
			}
			else{
					document.getElementById("saveAndSend").value="false";
					return submitPage();
			}
		}
		catch(e){
			alert(e);
		}
	}
	var pageSubmitted = false;
	function submitPage(){
		if(pageSubmitted==false){
			pageSubmitted = true;
			var rows = parseInt(document.getElementById("rows").value);
			for(var row=0;row<rows;row++){
				var size1  = document.getElementById("size_"+row);
				if(size1.value=="" || size1.value=="0"){
					size1.value="0.0";
				}
			}
			document.getElementById("addTask").submit();
			return true;
		}
	}
	
	function parseErrorMsg(msg){
		return "<li><span>" + msg + "</span></li>";
	}
	function hideErrorMsg(){
		document.getElementById("errorMessageDiv").style.display = "none";
        document.getElementById("successMessageDiv").style.display = "none";
	}
	function showErrorMsg(err){
		var msg = "<ul id=\"addTask_\" class=\"error\">";
        msg += err;
        msg += "</ul>";
        document.getElementById("errorMessageDiv").style.display = "";
        document.getElementById("successMessageDiv").style.display = "";
        document.getElementById("errorMessageDiv").innerHTML = msg;
        document.getElementById("successMessageDiv").innerHTML = "";
	}
	
</script>
<center>
<div class="main">
    <div class="page">
        <div class="top-header">
        	<div class="title-heading">
            	<div class="plan-lead-logo"><img src="<%=contextPath%>/images/plan-lead-logo.png" width="210" height="65" border="0" alt="Logo" title="Plan-Lead" />
                </div>
                <div class="tech-trends-logo"><img src="<%=contextPath%>/images/tech-trends-logo.png" width="195" height="65" border="0" alt="Logo" title="Tech-Trend" />
                </div>
             	<div class="logout"><a href="logOut"> Logout</a>
				</div>
             
            </div>
        </div>
        <s:form name="addTask" id="addTask" action="addBulkTask" method="post">
        	<div class="emp-details-part">
        	<s:hidden name="token" id="token"/>
        	
	        <table width="1146px" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            
	            <td class="emp-details-table" width="325px">
	                <table width="225px" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                    <td class="emp-details-field-name">Start date</td>
	                    <td colspan="2" nowrap="nowrap">
	                    	<sj:datepicker displayFormat="dd/mm/yy" tabindex="4" id="startDateStrId" onchange="isValidDate(this)"
							 cssStyle="width:100px;" onChangeTopics="onChangeDate" maxlength="10" buttonImageOnly="true" 
							 name="taskStartDate" readonly="false"/>
	                    </td>
	                  </tr>
	                  <tr>
	                    <td class="emp-details-field-name">End date</td>
	                    <td colspan="2" nowrap="nowrap">
	                    	<sj:datepicker displayFormat="dd/mm/yy" tabindex="5" onchange="isValidDate(this)"
							id="endDateStrId" cssStyle="width:100px;" cssClass="txt" buttonImageOnly="true"
							onChangeTopics="onChangeDate" maxlength="10"
							name="endDate" readonly="false"/>
						</td>
	                  </tr>
	                  
	                </table>
	            </td>
	            <td class="emp-details-table" width="500px"></td>
	            <td class="emp-details-table">
	                <table width="225px" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                    <td style="padding: 0 0 0 0;text-align:left;" class="emp-details-field-name" align="left" valign="bottom" ><s:include value="/jsp/menu/MainMenu.jsp"></s:include></td>
	                  </tr>
	                  <tr>
	                    <td class="emp-details-field-name">&nbsp;</td>
	                  </tr>
	                  
	                  <tr>
	                  	<td class="emp-details-field-name" style="padding: 0 0 0 3px;text-align:left;">
						<s:a href="javascript:funAddOrModWindow('recursiveTask.action','addTask','700','810','add','','','','270')" tabindex="7">
							<img src="<%=contextPath%>/images/recursive-task.png" width="143" height="32" border="0" alt="Icon" title="Recursive Task"/>
						</s:a>
						
						
						</td>
	                   
	                  </tr>
	                  
	                </table>
	            </td>
	          </tr>
	           <tr>
					<td height="10"><s:hidden name="taskDisplayMsg" id="taskDisplayMsg" ></s:hidden><s:hidden name="errorMsg" id="errorMsg"></s:hidden></td>
			 </tr>
	        </table>
        </div>
        
 			<div class="error-msg">&nbsp;
		        <table width="1148px">
					<tr>
						<td>
							<table width="1148px" border="0" cellspacing="0" cellpadding="0"
								align="left">
								<tr>
									<td nowrap="nowrap" align="left">
										<div id="errorMessageDiv">
											<s:if test="hasActionErrors()">
												<s:actionerror cssClass="error" />
											</s:if>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="1148px" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									<td nowrap="nowrap" align="center">
										<div id="successMessageDiv">
											<s:if test="hasActionMessages()">
												<s:actionmessage cssClass="success" />
											</s:if>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
	        <div class="main-table-part">
	        	<table width="1148px" border="0" cellspacing="0" cellpadding="0" style="margin:0px 0px 100px 0px;">
	              <tr>
	                <td>
	                <table width="1146px" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                  	<td style="width:316px; border-right:#f5efe7 solid 3px;" class="table-title">Project Details</td>
	                    <td style="width:230px; border-right:#f5efe7 solid 3px;" class="table-title">Plan</td>
	                    <td style="width:600px;" class="table-title">Actual</td>
	                  </tr>
	                </table>
	                </td>
	              </tr>
	              <tr>
	              	<td>
	                    <table width="1146px" border="0" cellspacing="0" cellpadding="0" style="background-color:#2d4a59;" id="table">
	                      <tr>
	                        <td class="table-title-text" style="width:80px;">Project Type</td>
	                        <td class="table-title-text" style="width:90px;">Project Code</td>
	                        <td class="table-title-text" style="width:90px;">Manager*</td>
	                        <td class="table-title-text" style="width:125px;">Task Description</td>
	                        <td class="table-title-text" style="width:65px;">Time</td>
	                        <td class="table-title-text" style="width:3px;">&nbsp;</td>
	                        <td class="table-title-text" style="width:125px;">Task Description</td>
	                        <td class="table-title-text" style="width:65px;">Time</td>
	                        <td class="table-title-text" style="width:70px;">Activity</td>
	                        <td class="table-title-text" style="width:90px;">Work Item</td>
	                        <td class="table-title-text" style="width:70px;">Size</td>
	                        <td class="table-title-text" style="width:68px;">Unit</td>
	                      </tr>
	                      
	                      <s:iterator value="responseVo.responseList" status="IteratorStatus">
	                      
	                      <tr>
	                        <td class="table-title-content">
	                        	<s:select name="task[%{#IteratorStatus.index}].projectTypeActionVo.projectTypeId" list="projectTypeVoList" listKey="projectTypeId" listValue="projectType" value="projectTypeActionVo.projectTypeId" cssClass="list-box" style="width:105px" onchange="resetProjectAndManager(this);"/>
								<input type="hidden" name="task[${IteratorStatus.index}].taskId" id="taskId_${IteratorStatus.index}" value="${taskId}"/>
								<input type="hidden" name="task[${IteratorStatus.index}].timesheetId" id="timesheetId_${IteratorStatus.index}" value="${timesheetId}"/>
								<input type="hidden" name="task[${IteratorStatus.index}].associatedProjectId" id="associatedProjectId_${IteratorStatus.index}" value="${associatedProjectId}"/>
	                        </td>
	                        <td class="table-title-content" nowrap="nowrap">
	                        	<input type="text" name="task[${IteratorStatus.index}].projectCode" id="projectCode_${IteratorStatus.index}" style="width:55px;vertical-align: middle;" class="txt" value="${projectCode}" onchange="updateManager(this);" onkeypress="return !isEnterKey(event)"/>
	                        		<a id="projectPopup_${IteratorStatus.index}" href="javascript:openProjectPopup('projectCode_${IteratorStatus.index}','project_${IteratorStatus.index}',${IteratorStatus.index})">
	                        			<img src="<%=contextPath%>/images/select-icon1.png" width="18" height="18" border="0" alt="Icon" title="Project-Select" style="padding:0px 0px 0px 0px; vertical-align: middle;"/>
	                        		</a>
								<input type="hidden" name="task[${IteratorStatus.index}].projectId" id="project_${IteratorStatus.index}" class="txt" value="${projectId}"/>
	                        </td>
	                        <td class="table-title-content">
	                        	<s:select name="task[%{#IteratorStatus.index}].reportempId" list="projectManagerList" listKey="idValue" listValue="name" cssClass="list-box" value="reportempId" headerKey="0" headerValue="Select" cssStyle="width:90px"/>
	                        </td>
	                        <td class="table-title-content">
	                        	<textarea rows="2" cols="10" name="task[${IteratorStatus.index}].planTaskDesc" id="planTask_${IteratorStatus.index}" style="width:120px; height:33px; resize:none; margin:15px 0px 0px 0px; overflow:auto;">${planTaskDesc}</textarea>
	                        </td>
	                        <td class="table-title-content">
	                        	<s:select name="task[%{#IteratorStatus.index}].planStartTime" list="timeList" listKey="key" listValue="name" value="planStartTime" cssClass="list-box" cssStyle="width:60px; margin:20px 0px 0px 0px;" onchange="return checkOverLappingValidation(this);"/>
	                        	<s:select name="task[%{#IteratorStatus.index}].planEndTime" list="timeList" listKey="key" listValue="name" value="planEndTime" cssClass="list-box" cssStyle="width:60px;" onchange="return checkOverLappingValidation(this);"/>
	                        </td>
	                        <td class="table-title-content" style="padding:15px 0px 0px 0px;">
	                        	<a href="#" id="copy_${IteratorStatus.index}" onclick="return copyPlanToActual();" title="copy plan to actual"><img src="<%=contextPath%>/images/right-arrow.png" align="middle" height="20" width="20" border="none"/></a>
	                        </td>
	                        <td class="table-title-content">
	                        	<textarea rows="2" cols="10" name="task[${IteratorStatus.index}].actualTaskDesc" id="actualTask_${IteratorStatus.index}" style="width:120px; height:33px; resize:none; margin:15px 0px 0px 0px; overflow:auto;">${actualTaskDesc}</textarea>
	                        </td>
	                        <td class="table-title-content">
	                        	<s:select name="task[%{#IteratorStatus.index}].actualStartTime" list="timeList" listKey="key" listValue="name" value="actualStartTime" cssClass="list-box" cssStyle="width:60px; margin:20px 0px 0px 0px;" onchange="return checkOverLappingValidation(this);"/>
	                        	<s:select name="task[%{#IteratorStatus.index}].actualEndTime" list="timeList" listKey="key" listValue="name" value="actualEndTime" cssClass="list-box" cssStyle="width:60px;" onchange="return checkOverLappingValidation(this);"/>
	                        </td>
	                        <td class="table-title-content" align="left" nowrap="nowrap">
	                        	<input type="text" name="task[${IteratorStatus.index}].activity" id="activityCode_${IteratorStatus.index}" class="txt" value="${activity}" style="width:40px;vertical-align: middle;" maxlength="4" onkeypress="return !isEnterKey(event)" onchange="toUpper(this)"/>
	                        	<a id="activityPopup_${IteratorStatus.index}" href="javascript:openActivityPopup('activityCode_${IteratorStatus.index}','activityId_${IteratorStatus.index}')">
	                        		<img src="<%=contextPath%>/images/select-icon1.png" width="18" height="18" border="0" alt="Icon" title="Activity-Select" style="padding:0px 0px 0px 0px; vertical-align: middle;"/>
	                        	</a>
	                        	<input type="hidden" name="task[${IteratorStatus.index}].activityId" id="activityId_${IteratorStatus.index}" class="txt" value="${activityId}"/>
	                        </td>
	                        <td class="table-title-content">
	                        	<input type="text" name="task[${IteratorStatus.index}].workItem" id="workItem_${IteratorStatus.index}" class="txt" value="${workItem}" size="7" style="width:80px;" maxlength="10" onkeypress="return !isEnterKey(event)"/>
	                        </td>
	                        <td class="table-title-content">
	                        	<input type="text" name="task[${IteratorStatus.index}].size" id="size_${IteratorStatus.index}" style="width:50px;" class="txt" value="${size}" onkeydown="return decimalValidation(this,event);" onkeypress="return !isEnterKey(event)" maxlength="8"/>
	                        </td>
	                        <td class="table-title-content" valign="top">
	                        	<s:select name="task[%{#IteratorStatus.index}].unitId" list="unitList" listKey="idValue" listValue="name" headerKey="0" headerValue="Select" 
	                        	value="unitId" cssClass="list-box" cssStyle="width:106px; margin:20px 0px 0px 0px;"/>
	                        </td>
	                      </tr>
	                      
	                      </s:iterator>
	                      
	                    </table>
	                </td>
	              </tr>
	            </table>
	        </div>
	        
	        <s:hidden name="projectId"/>
	        <s:hidden name="projectCodeId" style="width:120px;"/>
	        <s:hidden name="projectTypeId"/>
	        <s:hidden name="cc" id="cc"/>
	        
        	<input type="hidden" name="rows" id="rows" value="<s:property value="responseVo.responseList.size"/>"/>
        	<input type="hidden" name="saveAndSend" id="saveAndSend" value="false"/>
        	<s:hidden name="availableRecords" id="availableRecords"/>
		        <div class="save-btns-part">
		        <table width="1148px" border="0" cellspacing="0" cellpadding="0">
		          <tr>
		            <td style="width:40%;"></td>
		            <td>
		            	<a href="#" tabindex="1003" title="Save">
	                      	<img src="<%=contextPath%>/images/save-btn-new.png" align="middle" border="none" alt="Save"
	                      	onclick="return validateProjectAndActivity('saveId');"
	                      	/>
	                    </a>
					</td>
		            <td style="width:40%;"></td>
		          </tr>
		        </table>
		        </div>
        </s:form>
    </div>
</div>
</center>
<script>
	
	function toUpper(obj){
		obj.value=obj.value.toUpperCase();
	}
	
	function resetSearchProject(){
		document.getElementById("projectCodeId").value="";
		document.getElementById("projectId").value="";
	}
	function copyPlanToActual(){
		document.getElementById("actualTask_0").value = document.getElementById("planTask_0").value;
		document.getElementById("addTask_task_0__actualStartTime").value = document.getElementById("addTask_task_0__planStartTime").value;
		document.getElementById("addTask_task_0__actualEndTime").value = document.getElementById("addTask_task_0__planEndTime").value;
	}
	function checkOverLappingValidation(obj){
		var rowNo   = "0";
		var isPlan  = (obj.id.indexOf("__plan")!=-1);
		var isStartTime = (obj.id.indexOf("StartTime")!=-1);
		
		var taskDate1  = obj.value;
		
		var	planStartTime1      = document.getElementById("addTask_task_"+rowNo+"__planStartTime").value.replace(/:/g,'');
		var	planStartTime1Exist = planStartTime1==""?false:true;
	        planStartTime1      = isNaN(planStartTime1)? 0 : parseFloat(planStartTime1);
	    
	    var planEndTime1      	= document.getElementById("addTask_task_"+rowNo+"__planEndTime").value.replace(/:/g,'');
	    var	planEndTime1Exist 	= planEndTime1==""?false:true;
	        planEndTime1      	= isNaN(planEndTime1)? 0 : parseFloat(planEndTime1);
		
		var actualStartTime1      = document.getElementById("addTask_task_"+rowNo+"__actualStartTime").value.replace(/:/g,'');
		var	actualStartTime1Exist = actualStartTime1==""?false:true;
			actualStartTime1      = isNaN(actualStartTime1)? 0 : parseFloat(actualStartTime1);
		
		var actualEndTime1        = document.getElementById("addTask_task_"+rowNo+"__actualEndTime").value.replace(/:/g,'');
		var	actualEndTime1Exist   = actualEndTime1==""?false:true;
			actualEndTime1        = isNaN(actualEndTime1)? 0 : parseFloat(actualEndTime1);
		
		if (isPlan && planStartTime1Exist && planEndTime1Exist && planStartTime1>=planEndTime1){
			if(isStartTime){
				alert("Plan start time is invalid.");
			} else {
				alert("Plan end time is invalid.");
			}
			obj.value="";
			return false;
		}
		if (!isPlan && actualStartTime1Exist && actualEndTime1Exist && actualStartTime1>=actualEndTime1){
			if(isStartTime){
				alert("Actual start time is invalid.");
			} else {
				alert("Actual end time is invalid.");
			}
			obj.value="";
			return false;
		}
		return true;
	}
	function overLappingValidation(obj,msg){
		
		var isPlan  = (obj.id.indexOf("__plan")!=-1);
		var isStartTime = (obj.id.indexOf("StartTime")!=-1);
		var rowNo   = parseInt(obj.id.substring(13,obj.id.indexOf("__")));
		var dateObj = document.getElementById("taskDate_"+rowNo);
		
		if(isStartTime){
			var endId = obj.id.replace("Start","End");
			var endObj = document.getElementById(endId);
			endObj.value="";
		}
		var result = checkOverLappingValidation(dateObj);
		var validation = true;
		if(isPlan && isStartTime && result.isPlanStartTimeInvalid){
			alert("Planned time should be greater than Current time");
			obj.options[0].selected = true;
			validation = false;
		}
		else if(isPlan && !isStartTime && result.isPlanEndTimeInvalid){
			alert("Planned time should be greater than Current time");
			obj.options[0].selected = true;
			validation = false;
		}
		else if(result.isActualTimeInvalid && !isPlan){
			alert("Actual time should be less than Current time");
			obj.options[0].selected = true;
			validation = false;
		}
		else if((isPlan && result.isPlanInvalid) || (!isPlan && result.isActualInvalid)){
			alert("Start time should be less than End time");
			obj.options[0].selected = true;
			validation = false;
		}
		else if(isPlan && result.planOverLap){
			obj.options[0].selected = true;
			alert("Plan time is overlapping with row "+result.row);
			validation = false;
		}
		else if(!isPlan && result.actualOverLap){
			obj.options[0].selected = true;
			alert("Actual time is overlapping with row "+result.row);
			validation = false;
		}
		if(validation && isStartTime){
			var endId = obj.id.replace("Start","End");
			var endObj = document.getElementById(endId);
			if(endObj.value==""){
				endObj.options[obj.selectedIndex+1].selected = true;
				var result1 = checkOverLappingValidation(dateObj);
				if(isPlan && (result1.isPlanEndTimeInvalid || result1.planOverLap)){
					endObj.options[0].selected = true;
				}
				if(!isPlan && (result1.isActualTimeInvalid || result1.actualOverLap)){
					endObj.options[0].selected = true;
				}
			}
		}
	}
</script>
<script>
	window.onload = function(){
		document.getElementById("startDateStrId").value = '${taskStartDate}';
		document.getElementById("endDateStrId").value = '${endDate}';
	}
	$(document).ready(function(){
  	  $(".datepick").datepicker('destroy');
  	  $('.datepick').each(function(i) {
			this.id    = 'taskDate_' + i;
			this.name  = 'task[' + i + '].taskDateStr';
			this.maxLength = '10';
			this.style.width = '75px';
			
 		}).datepicker({
             dateFormat: 'dd/mm/yy',
             buttonImage: '<%=contextPath%>/struts/js/calendar.gif',
             buttonImageOnly: true,
             showOn: "both",
             onSelect: function(dateText, inst) {
            	dateOverlappingValidation(inst,this);
        	}
         });
    });
    function doGetCaretPosition (oField) {
    	var iCaretPos = 0;
    	if(document.selection) { 
        	oField.focus ();
        	var oSel = document.selection.createRange ();
        	oSel.moveStart ('character', -oField.value.length);
        	iCaretPos = oSel.text.length;
        }
        else if(oField.selectionStart || oField.selectionStart == '0'){
        	iCaretPos = oField.selectionStart;
        }
        return (iCaretPos);
    }
    function decimalValidation(obj,event){
    	var value = obj.value;
    	var keyCode = event.charCode ? event.charCode : event.keyCode;
    	
    	if(keyCode==8 || keyCode==35 || keyCode==36 || keyCode==37 || keyCode==39 || keyCode==46 || keyCode==9){
    		return true;
    	}
    	var caretPos = doGetCaretPosition(obj);
    	var isNumber = (keyCode>47 && keyCode<58);
    	var isDot    = keyCode == 190;
    	
		if ( keyCode == 13 || //enter
		    (isDot && caretPos==7) || // dot at last
			(isDot && value.indexOf(".")!=-1)) { // double dot
			return false;
		}
		if(isDot && value.length-caretPos>1){//decimal 1
			return false;
		}
		if(value.indexOf(".")==-1 && value.length==6 && isNumber){
			return false;
		}
		if(value.indexOf(".")!=-1 && caretPos>(value.indexOf(".")+1) && isNumber){
			return false;
		}
		if(parseFloat(value)>1000000 && isNumber){
			return false;
		}
		if(isDot || isNumber){
			return true;
		}
		return false;
    }
    
    function isValidDate(date){
		var matches = /^(\d{2}|\d{1})[\/\/](\d{2}|\d{1})[\/\/](\d{4})$/.exec(date.value);
	    if (matches == null) return false;
	    var d = matches[1];
	    var m = matches[2] - 1;
	    var y = matches[3];
	    var composedDate = new Date(y, m, d);
	    var result = composedDate.getDate() == d &&
	           composedDate.getMonth() == m &&
	           composedDate.getFullYear() == y &&
	           (parseInt(y)>=2000);
	    if(result){
	    	if(d.length==1) d="0"+d;
	    	if(m<9) m="0"+(m+1);
	    	else m=(m+1);
	    	date.value = d+"/"+m+"/"+y;
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}
	function dateValidation(obj){
		if(obj.value!=""){
			if(isValidDate(obj)){
				dateOverlappingValidation({"lastVal":""},obj);
				return true;
			}
			else{
				alert("Date is invalid. (Format - dd/mm/yyyy)");
				return false;
			}
		}
	}
    
    function compareDate(date1,date2){
    	try{
    		var dt1  = parseInt(date1.substring(0,2),10);
    	    var mon1 = parseInt(date1.substring(3,5),10);
    	    var yr1  = parseInt(date1.substring(6,10),10);
    	    var dt2  = parseInt(date2.substring(0,2),10);
    	    var mon2 = parseInt(date2.substring(3,5),10);
    	    var yr2  = parseInt(date2.substring(6,10),10);
    	    
    	    var d1 = new Date(yr1, mon1, dt1);
    	    var d2 = new Date(yr2, mon2, dt2);
    		
	    	if(d1.getTime()>d2.getTime()){
	    		return {"equal":false,"d1gtd2":true,error:false}
	    	}
	    	else if(date1 == date2){
	    		return {"equal":true,"d1gtd2":false,error:false}
	    	}
	    	return {"equal":false,"d1gtd2":false,error:false}
    	}
    	catch(e){
    		return {"equal":false,"d1gtd2":false,error:true};
    	}
	}
	
	function isEnterKey(event){
		var keyCode = event.charCode ? event.charCode : event.keyCode;
		if (keyCode == 13) {
			return true;
		}
		return false;
	}
	function funRefreshParent()
	{
	 document.getElementById("searchTask").action="searchTask.action";
	 document.getElementById("searchTask").method="POST";
	 document.getElementById("searchTask").submit();	
	}
	
	function autoLogout(){
		
	}
	
</script>
</center>
</body>
</html>
