﻿<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader("Expires", 0);
        response.setLocale(java.util.Locale.ENGLISH);
%>
<%
	/* Getting Context Path */
	String contextPath = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Task Sheet-SRM Technologies</title>
		<sj:head  compressed="false"/>
		<sx:head debug="false" />
		<s:head theme="simple"/>
		<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/shirai.css" />
		<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
		<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="<%=contextPath%>/js/Popup.js"></script>
		<script type="text/javascript" src="<%=contextPath%>/js/Shirai.js"></script>
		<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/popup.css" />
		<style type="text/css">
.button_style{
	background-color : black;
	color: white;
	cursor: pointer;
	}
.logout_style {
	background: transparent;
	color: white;
	cursor: pointer;
}
</style>

<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }

</script>
<style type="text/css">
body{
	background-color:none;
	width:100%;
	margin:0px;
}
.main{
	width:1200px;
	margin:0px auto;
	background-image:url(../images/main-bg.png);
}
.page{
	margin:0px auto;
	width:1148px;
}

.top-header{
	width:1148px;
	float:left;
	background-image:url(../images/header-bg.png);
	height:75px;
}
.emp-details-part{
	width:1148px;
	float:left;
	background-color:#DDE4E8;
	height:105px;
}
.error-msg{
	width:1148px;
	float:left;
	background-color:#DDE4E8;
}
.main-table-part{
	width:1146px;
	float:left;
	background-color:#fff;
	border:#c8c6c6 solid 1px;
}
.save-btns-part{
	float:left;
	width:1148px;
	background-color:#f5f3f3;
}
.plan-lead-logo {
	float: left;
	width: 210px;
	height: 65px;
	padding: 0px 600px 0px 0px;
}

</style>
</head>
<body>
<script>
	var currentDate = "${currentDateStr}";
	var currentTime = "${currentTime}";
	var sysTime     = parseFloat(currentTime.replace(/:/g,''));

	var projectAry = new Array();
	<c:forEach items="${allProjectList}" var="entry" varStatus="row">
		projectAry["${entry.key}"] = new Array();
		<c:forEach items="${entry.value}" var="project">
			projectAry["${entry.key}"]["${project.key}"] = "${project.idValue}:${project.refId}:${project.refId2}";
		</c:forEach>
	</c:forEach>

    var activityAry = new Array();
    <s:iterator value="activityList" status="IteratorStatus">
    	activityAry["${key}"] = "${idValue}";
    </s:iterator>

	function openSearchProjectPopup(){
		var projectTypeId = document.getElementById("projectTypeId").value;
		var title = "";
		if(projectTypeId=="0"){
			alert("Please select project type");
			return;
		}
		if(projectTypeId=="1"){
			title = "Project Selection Screen";
		}
		else if(projectTypeId=="2"){
			title = "Practice Team Selection Screen";
		}
		var selectedProjectCode = document.getElementById("projectCodeId").value;
		newwindow=window.open("mappedProjectList?projectListId=projectId&projectCodeId=projectCodeId&projectType="+projectTypeId+"&selectedProjectCode="+selectedProjectCode+"&statusId=0&title="+title,'newwindow','scrollbars=no,location=no,toolbar=no,resizable=no,menubar=0,height=430,width=517,left=200,top=200');
	}

	function openProjectPopup(projectCodeId,projectId,rowNo){

		var projectType = "1";
		var projectTypeId = document.getElementById("addTask_task_"+rowNo+"__projectTypeActionVo_projectTypeId");
		var title = "";
		if(projectTypeId){
			if(projectTypeId.value=="0"){
				alert("Please select project type");
				return;
			}
			projectType = document.getElementById("addTask_task_"+rowNo+"__projectTypeActionVo_projectTypeId").value;
			if(projectTypeId.value=="1"){
				title = "Project Selection Screen";
			}
			else if(projectTypeId.value=="2"){
				title = "Practice Team Selection Screen";
			}
		}
		selectedProjectCode = document.getElementById(projectCodeId).value;
		newwindow=window.open("mappedProjectList?projectListId="+projectId+"&projectCodeId="+projectCodeId+"&projectType="+projectType+"&selectedProjectCode="+selectedProjectCode+"&statusId=3&title="+title,'newwindow','scrollbars=no,location=0,toolbar=no,resizable=no,menubar=0,height=430,width=517,left=200,top=200');
	}

	function openActivityPopup(projectId,projectCodeId){
		var activityCode = document.getElementById(projectId).value;
		newwindow=window.open("activityList?projectListId="+projectId+"&projectCodeId="+projectCodeId+"&activityCode="+activityCode,'newwindow','scrollbars=no,location=0,toolbar=no,resizable=no,menubar=0,height=365,width=617,left=200,top=200');
	}
	function openRecursivePopup(url,projectCodeId){
		newwindow=window.open(url, 'newwindow','scrollbars=no,dependent=yes, titlebar=yes,location=0, menubar=0, height=400, width=700, left=200, top=200');
	}
	function searchValidation(){
		try{
			var projectTypeId = document.getElementById("projectTypeId").value;
			var projectCode = document.getElementById("projectCodeId").value.replace(/ /g,'');
			var projectId = document.getElementById("projectId").value;
			var startDate = document.getElementById("startDateStrId");
			var endDate = document.getElementById("endDateStrId");

			if(projectTypeId=="0" || !projectAry[projectTypeId]){
				document.getElementById("projectId").value ="";
				if(projectCode!=""){
					alert("Project type is required");
					return false;
				}
			}
			if(projectCode!=""){
				if(!projectAry[projectTypeId][projectCode]){
					alert("Project code is invalid");
					document.getElementById("projectId").value ="";
					return false;
				}
				else{
					document.getElementById("projectId").value = projectAry[projectTypeId][projectCode].split(":")[0];
				}
			}

			if(projectCode=="0"){
				document.getElementById("projectId").value ="";
			}

			if(startDate.value!="" && !isValidDate(startDate)){
				alert("Start Date is invalid. (Format - dd/mm/yyyy)");
				return false;
			}

			if(endDate.value!="" && !isValidDate(endDate)){
				alert("End Date is invalid. (Format - dd/mm/yyyy)");
				return false;
			}
			startDate = startDate.value;
			startDate = startDate.split("/");
			var startDate1 = new Date();
			startDate1.setFullYear(parseInt(startDate[2]));
			startDate1.setMonth(parseInt(startDate[1]));
			startDate1.setDate(parseInt(startDate[0]));

			endDate = endDate.value;
			endDate = endDate.split("/");
			var endDate1 = new Date();
			endDate1.setFullYear(parseInt(endDate[2]));
			endDate1.setMonth(parseInt(endDate[1]));
			endDate1.setDate(parseInt(endDate[0]));

			if (startDate1 > endDate1) {
				alert("End Date should be greater or equal to the Start Date");
				return false;
			}
			searchTask.submit();
		}
		catch(e){
			alert(e);
		}
		return true;
	}
	function updateManager(projectCode){

		toUpper(projectCode);

		var row = projectCode.id.substring(12);
		var projectTypeId = document.getElementById("addTask_task_"+row+"__projectTypeActionVo_projectTypeId").value;

		if(projectTypeId != "0" && projectAry[projectTypeId][projectCode.value]){
			var manager = projectAry[projectTypeId][projectCode.value].split(":")[1];
			if(manager!=""){
				document.getElementById("addTask_task_"+row+"__reportempId").value = manager;
				if(document.getElementById("addTask_task_"+row+"__reportempId").value==""){
					document.getElementById("addTask_task_"+row+"__reportempId").options[0].selected=true;
				}
			}
		}
	}
	function validateProjectAndActivity(submitObj){
		try{
			if(pageSubmitted){
				return false;
			}
			var rows = parseInt(document.getElementById("rows").value);
			var isInvalid = false;
			var errMsg = "";
			var atleastOneRowExist = false;
			for(var row=0;row<rows;row++){
				if(document.getElementById("projectCode_"+row)){

					var taskFor             = document.getElementById("taskDate_"+row+"_taskfor").value;

					var taskDateObj  		= document.getElementById("taskDate_"+row);
					var projectTypeIdObj	= document.getElementById("addTask_task_"+row+"__projectTypeActionVo_projectTypeId");
					var projectCodeObj		= document.getElementById("projectCode_"+row);
					var reportempIdObj 		= document.getElementById("addTask_task_"+row+"__reportempId");
					var planTaskObj    		= document.getElementById("planTask_"+row);
					var planStartTimeObj 	= document.getElementById("addTask_task_"+row+"__planStartTime");
					var planEndTimeObj 		= document.getElementById("addTask_task_"+row+"__planEndTime");
					var actualTaskObj 		= document.getElementById("actualTask_"+row);
					var actualStartTimeObj	= document.getElementById("addTask_task_"+row+"__actualStartTime");
					var actualEndTimeObj 	= document.getElementById("addTask_task_"+row+"__actualEndTime");
					var activityCodeObj 	= document.getElementById("activityCode_"+row);
					var workItemObj 		= document.getElementById("workItem_"+row);
					var size                = document.getElementById("size_"+row);
					var module              = document.getElementById("module_"+row);
					var unitId              = document.getElementById("addTask_task_"+row+"__unitId");
					var associatedProjectObj= document.getElementById("associatedProjectId_"+row);

					var rowExist    = false;
					var planExist   = false;
					var actualExist = false;

					if(taskDateObj.value!="" || projectTypeIdObj.value!="0" || projectCodeObj.value!="" ||
						reportempIdObj.value!="0" || planTaskObj.value!="" || planStartTimeObj.value!="" ||
						planEndTimeObj.value!="" || actualTaskObj.value!="" || actualStartTimeObj.value!="" ||
						actualEndTimeObj.value!="" || activityCodeObj.value!="" || workItemObj.value!="" || module.value!="" ||
						(size.value!="" && size.value!="0.0" && size.value!="0") || unitId.value!="0"){
						rowExist = true;
					}

					if(planTaskObj.value!="" || planStartTimeObj.value!="" || planEndTimeObj.value!=""){
						planExist = true;
					}
					if(actualTaskObj.value!="" || actualStartTimeObj.value!="" || actualEndTimeObj.value!="" ||
					   activityCodeObj.value!="" || workItemObj.value!="" || module.value!="" || (size.value!="" && size.value!="0.0" && size.value!="0") ||
					   unitId.value!="0"){
						actualExist = true;
					}
					if(rowExist){
						atleastOneRowExist = true;
						if(taskDateObj.value==""){
							errMsg += parseErrorMsg("Date is required for row "+(row+1));
							isInvalid = true;
						}
						else if(!isValidDate(taskDateObj)){
							errMsg += parseErrorMsg("Date is invalid for row "+(row+1)+". (Format - dd/mm/yyyy)");
							isInvalid = true;
						}
						var projectCode = document.getElementById("projectCode_"+row).value;
						var projectId   = document.getElementById("project_"+row);
						projectId.value = "";

						if(projectCode!="" && projectTypeIdObj.value=="0"){
							errMsg += parseErrorMsg("Project type is required for row "+(row+1));
							isInvalid = true;
						}

						if(projectTypeIdObj.value != "0" && projectAry[projectTypeIdObj.value][projectCode]){
							projectId.value = projectAry[projectTypeIdObj.value][projectCode].split(":")[0];
							if(projectTypeIdObj.value=="2"){
								associatedProjectObj.value=projectAry[projectTypeIdObj.value][projectCode].split(":")[2];
							}
						}
						else if(projectCode!=""){
							errMsg += parseErrorMsg("Project code is invalid for row "+(row+1));
							isInvalid = true;
						}
						if(reportempIdObj.value=="0"){
							errMsg += parseErrorMsg("Manager is required for row "+(row+1));
							isInvalid = true;
						}

						if(taskFor == "plan" || planExist){
							if(planTaskObj.value.replace(/ /g,"")==""){
								planTaskObj.value = "";
								errMsg += parseErrorMsg("Planned task description is required for row "+(row+1));
								isInvalid = true;
							}
							if(planTaskObj.value.length>128){
								errMsg += parseErrorMsg("Planned task description should not exceed 128 character for row "+(row+1));
								isInvalid = true;
							}
							if(planStartTimeObj.value==""){
								errMsg += parseErrorMsg("Planned start time is required for row "+(row+1));
								isInvalid = true;
							}
							if(planEndTimeObj.value==""){
								errMsg += parseErrorMsg("Planned end time is required for row "+(row+1));
								isInvalid = true;
							}
						}
						if(actualExist){
							if(actualTaskObj.value.replace(/ /g,"")==""){
								actualTaskObj.value = "";
								errMsg += parseErrorMsg("Actual task description is required for row "+(row+1));
								isInvalid = true;
							}
							if(actualTaskObj.value.length>128){
								errMsg += parseErrorMsg("Actual task description should not exceed 128 character for row "+(row+1));
								isInvalid = true;
							}
							if(actualStartTimeObj.value==""){
								errMsg += parseErrorMsg("Actual start time is required for row "+(row+1));
								isInvalid = true;
							}
							if(actualEndTimeObj.value==""){
								errMsg += parseErrorMsg("Actual end time is required for row "+(row+1));
								isInvalid = true;
							}
							if(activityCodeObj.value==""){
								errMsg += parseErrorMsg("Activity is required for row "+(row+1));
								isInvalid = true;
							}

							var activityCode = document.getElementById("activityCode_"+row).value;
							var activityId   = document.getElementById("activityId_"+row);
							activityId.value="";
							if(activityAry[activityCode]){
								activityId.value = activityAry[activityCode];
							}
							else if(activityCode!=""){
								errMsg += parseErrorMsg("Activity code is invalid for row "+(row+1));
								isInvalid = true;
							}

							if(workItemObj.value==""){
								errMsg += parseErrorMsg("Work Item is required for row "+(row+1));
								isInvalid = true;
							}
							if(size.value=="" && unitId.value!="0"){
								errMsg += parseErrorMsg("Size is required for row "+(row+1));
								isInvalid = true;
							}
							if(parseFloat(size.value)>=1000000){
								errMsg += parseErrorMsg("Size should be less than 1000000 for row "+(row+1));
								isInvalid = true;
							}
							else if(size.value.indexOf(".")==-1 && size.value!=""){
								size.value=size.value+".0";
							}
							if(size.value.indexOf(".")==size.value.length-1 && size.value!=""){
								size.value=size.value+"0";
							}
							if((size.value!="" && size.value!="0.0" && size.value!="0") && unitId.value=="0"){
								errMsg += parseErrorMsg("Unit is required for row "+(row+1));
								isInvalid = true;
							}
						}
						if(rowExist && (taskFor=="both" || taskFor=="") && !planExist && !actualExist){
							errMsg += parseErrorMsg("Plan or Actual is required for row "+(row+1));
							isInvalid = true;
						}
					}
				}
			}
			if(!atleastOneRowExist){
				errMsg += parseErrorMsg("Please enter atleast one record");
				isInvalid = true;
			}
			if(isInvalid){
				showErrorMsg(errMsg);
				return false;
			}
			else{
				if(submitObj=="saveId"){
					document.getElementById("saveAndSend").value="false";
					return submitPage();
				}
				else{
					document.getElementById("saveAndSend").value="true";
					funAddOrModWindow('ccList.action','ccList','700','810','add','','','','270');
					return false;
				}
			}
		}
		catch(e){
			alert(e);
		}
	}
	var pageSubmitted = false;
	function submitPage(){
		<c:if test="${sessionScope.user.guest eq true}">
			hideErrorMsg();
		</c:if>
		<c:if test="${sessionScope.user.guest eq false}">
		if(pageSubmitted==false){
			pageSubmitted = true;
			var rows = parseInt(document.getElementById("rows").value);
			for(var row=0;row<rows;row++){
				var size1  = document.getElementById("size_"+row);
				if(size1.value==""){
					size1.value="0.0";
				}
			}
			document.getElementById("trnsBack").style.height=(460+60*rows)+"px";
			document.getElementById("trnsBack").style.display="block";
			enableAllRows();
			document.getElementById("addTask").submit();
			return true;
		}
		</c:if>
	}

	function parseErrorMsg(msg){
		return "<li><span>" + msg + "</span></li>";
	}
	function hideErrorMsg(){
		document.getElementById("errorMessageDiv").style.display = "none";
        document.getElementById("successMessageDiv").style.display = "none";
	}
	function showErrorMsg(err){
		var msg = "<ul id=\"addTask_\" class=\"error\">";
        msg += err;
        msg += "</ul>";
        document.getElementById("errorMessageDiv").style.display = "";
        document.getElementById("successMessageDiv").style.display = "";
        document.getElementById("errorMessageDiv").innerHTML = msg;
        document.getElementById("successMessageDiv").innerHTML = "";
	}
</script>
<center>
<div id="trnsBack" style="display:block;position: absolute;top: 0px;left:0px;width: 1148px;height: ${(460+60*availableRecords)}px;z-index: 100"><img src="<%=contextPath%>/images/trans-back.png"/></div>
<div class="main">
    <div class="page" style="width: 1200px;">
        <div class="top-header" style="width: 1200px;">
        	<div class="title-heading">
            	<div class="plan-lead-logo"><img src="<%=contextPath%>/images/plan-lead-logo.png" width="210" height="65" border="0" alt="Logo" title="Plan-Lead" />
                </div>
                <div class="tech-trends-logo"><img src="<%=contextPath%>/images/tech-trends-logo.png" width="195" height="65" border="0" alt="Logo" title="Tech-Trend" />
                </div>
             	<div class="logout"><a href="logOut"> Logout</a>
				</div>

            </div>
        </div>
        <div class="emp-details-part" style="width: 1200px;">
        <s:form name="searchTask" id="searchTask" action="searchTask" method="post">

        	<s:hidden name="token" id="token"/>

	        <table width="1200px" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td class="emp-details-table">
	                <table width="325px" border="0" cellspacing="0" cellpadding="0" >
	                  <tr>
	                    <td class="emp-details-field-name" >Employee Name :</td>
	                    <td colspan="2" style="width:140px;" >
	                    	<input type="text"  value="${sessionScope.user.userName}" style="width:144px" readonly="readonly" tabindex="1" onfocus="false"  onkeypress="return !isEnterKey(event)"/>
	                    </td>
	                  </tr>
	                  <tr>
	                    <td class="emp-details-field-name">Project Type :</td>
	                    <td colspan="2">
	                    	<s:select name="projectTypeId" id="projectTypeId" list="projectTypeVoList" listKey="projectTypeId" listValue="projectType" tabindex="2"
	                    	 cssClass="list-box" style="width:147px" onchange="resetSearchProject();"/>
	                    </td>
	                  </tr>
	                  <tr>
	                    <td class="emp-details-field-name">Project Code :</td>
	                    <td style="width:100px;" >
	                    	<s:hidden name="projectId" id="projectId"/>
	                    	<s:textfield name="projectCodeId" id="projectCodeId" style="width:120px;" tabindex="3" onchange="toUpper(this)" onkeypress="return !isEnterKey(event)"/>
	                    </td>
	                    <td class="select-icon" style="width:20px;">

	                    	<a id="projectPopup" href="javascript:openSearchProjectPopup()" tabindex="3.1">
	                        	<img src="<%=contextPath%>/images/select-icon.png" width="20" height="20" border="0" title="Select Project"/>
	                        </a>

	                    </td>
	                  </tr>
	                </table>
	            </td>
	            <td class="emp-details-table">
	                <table width="325px" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                    <td class="emp-details-field-name">Start Date :</td>
	                    <td colspan="2" nowrap="nowrap">
	                    	<sj:datepicker displayFormat="dd/mm/yy" tabindex="4" id="startDateStrId" onchange="isValidDate(this)"
							 cssStyle="width:100px;" onChangeTopics="onChangeDate" maxlength="10" buttonImageOnly="true"
							 name="taskStartDate" readonly="false"/>
	                    </td>
	                  </tr>
	                  <tr>
	                    <td class="emp-details-field-name">End Date :</td>
	                    <td colspan="2" nowrap="nowrap">
	                    	<sj:datepicker displayFormat="dd/mm/yy" tabindex="5" onchange="isValidDate(this)"
							id="endDateStrId" cssStyle="width:100px;" cssClass="txt" buttonImageOnly="true"
							onChangeTopics="onChangeDate" maxlength="10"
							name="endDate" readonly="false"/>
						</td>
	                  </tr>

	                  <tr>
	                  	<td class="emp-details-field-name">&nbsp;</td>
	                    <td colspan="2" class="search-btn">
	                    	<a href="#" tabindex="6" title="Search" id="searchlinkid" onclick="return searchValidation();">
	                        	<img src="<%=contextPath%>/images/search-btn-new.png" align="middle" border="none" alt="Search"/>
	                        </a>
	                    	<%--<input tabindex="6" type="image" src="<%=contextPath%>/images/search-btn-new.png" onclick="return searchValidation();"/>--%>
	                    </td>
	                  </tr>

	                </table>
	            </td>
	            <td class="emp-details-table">
	                <table width="225px" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                    <td style="padding: 0 0 0 0;text-align:left;" class="emp-details-field-name" align="left" valign="bottom" ><s:include value="/jsp/menu/MainMenu.jsp"></s:include></td>
	                  </tr>
	                  <tr>
	                    <td class="emp-details-field-name">&nbsp;</td>
	                  </tr>

	                  <tr>
	                  	<td class="emp-details-field-name" style="padding: 0 0 0 3px;text-align:left;">
						<s:a href="javascript:funAddOrModWindow('recursiveTask.action','addTask','700','810','add','','','','270')" tabindex="7">
							<img src="<%=contextPath%>/images/recursive-task.png" width="143" height="32" border="0" alt="Icon" title="Recursive Task"/>
						</s:a>
						</td>

	                  </tr>

	                </table>
	            </td>
	          </tr>
	           <tr>
					<td height="10"><s:hidden name="taskDisplayMsg" id="taskDisplayMsg" ></s:hidden><s:hidden name="errorMsg" id="errorMsg"></s:hidden></td>
			 </tr>
	        </table>
        </s:form>
     	</div>

 		<s:form name="addTask" id="addTask" action="addTask" method="post">
 			<div class="error-msg" style="width: 1200px;">&nbsp;
		        <table width="1200px">
					<tr>
						<td>
							<table width="1193px" border="0" cellspacing="0" cellpadding="0"
								align="left">
								<tr>
									<td nowrap="nowrap" align="left">
										<div id="errorMessageDiv">
											<s:if test="hasActionErrors()">
												<s:actionerror cssClass="error" />
											</s:if>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="1193px" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									<td nowrap="nowrap" align="center">
										<div id="successMessageDiv">
											<s:if test="hasActionMessages()">
												<s:actionmessage cssClass="success" />
											</s:if>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
	        <div class="main-table-part" style="width: 1200px;">
	        	<table width="1200px" border="0" cellspacing="0" cellpadding="0" style="margin:0px 0px 100px 0px;">
	              <tr>
	                <td>
	                <table width="1200px" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                  	<td style="width:387px; border-right:#f5efe7 solid 3px;" class="table-title">Project Details</td>
	                    <td style="width:190px; border-right:#f5efe7 solid 3px;" class="table-title">Plan</td>
	                    <td style="width:490px;" class="table-title">Actual</td>
	                  </tr>
	                </table>
	                </td>
	              </tr>
	              <tr>
	              	<td>
	                    <table width="1200px" border="0" cellspacing="0" cellpadding="0" style="background-color:#2d4a59;" id="table">
	                      <tr>
	                        <td class="table-title-text" style="width:30px;">S.no</td>
	                        <td class="table-title-text" style="width:85px;">Date*</td>
	                        <td class="table-title-text" style="width:80px;">Project Type</td>
	                        <td class="table-title-text" style="width:90px;">Project Code</td>
	                        <td class="table-title-text" style="width:90px;">Manager*</td>
	                        <td class="table-title-text" style="width:125px;">Task Description</td>
	                        <td class="table-title-text" style="width:65px;">Time</td>
	                        <td class="table-title-text" style="width:3px;">&nbsp;</td>
	                        <td class="table-title-text" style="width:125px;">Task Description</td>
	                        <td class="table-title-text" style="width:65px;">Time</td>
	                        <td class="table-title-text" style="width:70px;">Activity</td>
	                        <td class="table-title-text" style="width:80px;">Work Item</td>
	                        <td class="table-title-text" style="width:48px;">Module</td>
	                        <td class="table-title-text" style="width:50px;">Size</td>
	                        <td class="table-title-text" style="width:60px;">Unit</td>
	                      </tr>

	                      <s:iterator value="responseVo.responseList" status="IteratorStatus">

	                      <tr>
	                        <td class="table-title-content" style="text-align:center;">
	                        	${IteratorStatus.index+1}
	                        </td>

	                        <td class="table-title-content" style="text-align:left;" nowrap="nowrap">
	                        	<sj:datepicker tabindex="4"
								id="taskDate_%{#IteratorStatus.index}" cssClass="datepick"
								onChangeTopics="onChangeDate" maxlength="10" style="width:65px !important;"
								buttonImageOnly="true" onchange="dateValidation(this)"
								name="task[%{#IteratorStatus.index}].taskDateStr" readonly="false"
								value="%{taskDateStr}"
								onpaste="return true;" />
								
								<input type="hidden" id="taskDateBackup_${IteratorStatus.index}" value="${taskDateStr}"/>
								
								<s:if test="taskDate.before(currentDate)">
									<input type="hidden" id="taskDate_${IteratorStatus.index}_taskfor" value="actual"/>
						  		</s:if>
						  		<s:if test="taskDate.after(currentDate)">
									<input type="hidden" id="taskDate_${IteratorStatus.index}_taskfor" value="plan"/>
						  		</s:if>
						  		<s:if test="taskDate.equals(currentDate)">
									<input type="hidden" id="taskDate_${IteratorStatus.index}_taskfor" value="both"/>
						  		</s:if>
						  		<s:if test="taskDate == null">
						  			<input type="hidden" id="taskDate_${IteratorStatus.index}_taskfor" value="both"/>
						  		</s:if>
	                        </td>
	                        <td class="table-title-content">
	                        	<s:select name="task[%{#IteratorStatus.index}].projectTypeActionVo.projectTypeId" list="projectTypeVoList" listKey="projectTypeId" listValue="projectType" value="projectTypeActionVo.projectTypeId" cssClass="list-box" style="width:105px" onchange="resetProjectAndManager(this);"/>
								<input type="hidden" name="task[${IteratorStatus.index}].taskId" id="taskId_${IteratorStatus.index}" value="${taskId}"/>
								<input type="hidden" name="task[${IteratorStatus.index}].timesheetId" id="timesheetId_${IteratorStatus.index}" value="${timesheetId}"/>
								<input type="hidden" name="task[${IteratorStatus.index}].associatedProjectId" id="associatedProjectId_${IteratorStatus.index}" value="${associatedProjectId}"/>
	                        </td>
	                        <td class="table-title-content" nowrap="nowrap">
	                        	<input type="text" name="task[${IteratorStatus.index}].projectCode" id="projectCode_${IteratorStatus.index}" style="width:55px;vertical-align: middle;" class="txt" value="${projectCode}" onchange="updateManager(this);" onkeypress="return !isEnterKey(event)"/>
	                        		<a id="projectPopup_${IteratorStatus.index}" href="javascript:openProjectPopup('projectCode_${IteratorStatus.index}','project_${IteratorStatus.index}',${IteratorStatus.index})">
	                        			<img src="<%=contextPath%>/images/select-icon1.png" width="18" height="18" border="0" alt="Icon" title="Project-Select" style="padding:0px 0px 0px 0px; vertical-align: middle;"/>
	                        		</a>
								<input type="hidden" name="task[${IteratorStatus.index}].projectId" id="project_${IteratorStatus.index}" class="txt" value="${projectId}"/>
	                        </td>
	                        <td class="table-title-content">
	                        	<s:select name="task[%{#IteratorStatus.index}].reportempId" list="projectManagerList" listKey="idValue" listValue="name" cssClass="list-box" value="reportempId" headerKey="0" headerValue="Select" cssStyle="width:90px"/>
	                        </td>
	                        <td class="table-title-content">
	                        	<textarea rows="2" cols="10" name="task[${IteratorStatus.index}].planTaskDesc" id="planTask_${IteratorStatus.index}" style="width:120px; height:33px; resize:none; margin:15px 0px 0px 0px; overflow:auto;">${planTaskDesc}</textarea>
	                        </td>
	                        <td class="table-title-content">
	                        	<s:select name="task[%{#IteratorStatus.index}].planStartTime" list="timeList" listKey="key" listValue="name" value="planStartTime" cssClass="list-box" cssStyle="width:60px; margin:20px 0px 0px 0px;" onchange="return overLappingValidation(this);"/>
	                        	<s:select name="task[%{#IteratorStatus.index}].planEndTime" list="timeList" listKey="key" listValue="name" value="planEndTime" cssClass="list-box" cssStyle="width:60px;" onchange="return overLappingValidation(this);"/>
	                        </td>
	                        <td class="table-title-content" style="padding:15px 0px 0px 0px;">
	                        	<a href="#" id="copy_${IteratorStatus.index}" onclick="return copyPlanToActual(this.id);" title="copy plan to actual"><img src="<%=contextPath%>/images/right-arrow.png" align="middle" height="20" width="20" border="none"/></a>
	                        </td>
	                        <td class="table-title-content">
	                        	<textarea rows="2" cols="10" name="task[${IteratorStatus.index}].actualTaskDesc" id="actualTask_${IteratorStatus.index}" style="width:120px; height:33px; resize:none; margin:15px 0px 0px 0px; overflow:auto;">${actualTaskDesc}</textarea>
	                        </td>
	                        <td class="table-title-content">
	                        	<s:select name="task[%{#IteratorStatus.index}].actualStartTime" list="timeList" listKey="key" listValue="name" value="actualStartTime" cssClass="list-box" cssStyle="width:60px; margin:20px 0px 0px 0px;" onchange="return overLappingValidation(this);"/>
	                        	<s:select name="task[%{#IteratorStatus.index}].actualEndTime" list="timeList" listKey="key" listValue="name" value="actualEndTime" cssClass="list-box" cssStyle="width:60px;" onchange="return overLappingValidation(this);"/>
	                        </td>
	                        <td class="table-title-content" align="left" nowrap="nowrap">
	                        	<input type="text" name="task[${IteratorStatus.index}].activity" id="activityCode_${IteratorStatus.index}" class="txt" value="${activity}" style="width:40px;vertical-align: middle;" maxlength="4" onkeypress="return !isEnterKey(event)" onchange="toUpper(this)"/>
	                        	<a id="activityPopup_${IteratorStatus.index}" href="javascript:openActivityPopup('activityCode_${IteratorStatus.index}','activityId_${IteratorStatus.index}')">
	                        		<img src="<%=contextPath%>/images/select-icon1.png" width="18" height="18" border="0" alt="Icon" title="Activity-Select" style="padding:0px 0px 0px 0px; vertical-align: middle;"/>
	                        	</a>
	                        	<input type="hidden" name="task[${IteratorStatus.index}].activityId" id="activityId_${IteratorStatus.index}" class="txt" value="${activityId}"/>
	                        </td>
	                        <td class="table-title-content">
	                        	<input type="text" name="task[${IteratorStatus.index}].workItem" id="workItem_${IteratorStatus.index}" class="txt" value="${workItem}" size="7" style="width:80px;" maxlength="10" onkeypress="return !isEnterKey(event)"/>
	                        </td>
	                        <td class="table-title-content">
	                        	<input type="text" name="task[${IteratorStatus.index}].module" id="module_${IteratorStatus.index}" style="width:50px;" class="txt" value="${module}"  onkeypress="return !isEnterKey(event)" maxlength="100"/>
	                        </td>
	                        <td class="table-title-content">
	                        	<input type="text" name="task[${IteratorStatus.index}].size" id="size_${IteratorStatus.index}" style="width:50px;" class="txt" value="${size}" onkeydown="return decimalValidation(this,event);" onkeypress="return !isEnterKey(event)" maxlength="8"/>
	                        </td>
	                        <td class="table-title-content" valign="top">
	                        	<s:select name="task[%{#IteratorStatus.index}].unitId" list="unitList" listKey="idValue" listValue="name" headerKey="0" headerValue="Select"
	                        	value="unitId" cssClass="list-box" cssStyle="width:100px; margin:20px 0px 0px 0px;"/>
	                        </td>
	                      </tr>

	                      </s:iterator>

	                    </table>
	                </td>
	              </tr>
	              <tr>
	                <td>
	                    <table width="1200px" border="0" cellspacing="0" cellpadding="0">
	                      <tr>
	                        <td style="width:880px;">&nbsp;</td>

	                        <td class="plus-btns">
		                        <a href="#" tabindex="1000" title="Copy to new row" id="copy_${IteratorStatus.index}" onclick="return copyAddRow();">
		                        	<img src="<%=contextPath%>/images/plus-copy.png" align="middle" border="none" alt="Copy to new row"/>
		                        </a>
	                       	</td>

	                        <td class="plus-btns">
		                        <a href="#" tabindex="1001" title="Add Row" id="copy_${IteratorStatus.index}" onclick="return addRow();">
		                        	<img src="<%=contextPath%>/images/plus-btn.png" align="middle" border="none" alt="Plus"/>
		                        </a>
	                       	</td>
	                        <td class="plus-btns">
		                        <a href="#" tabindex="1002" title="Remove Row" id="copy_${IteratorStatus.index}" onclick="return removeRow();">
		                        	<img src="<%=contextPath%>/images/minus-btn.png" align="middle" border="none" alt="Minus"/>
		                        </a>
	                       	</td>
	                      </tr>
	                    </table>
	                </td>
	              </tr>
	            </table>
	        </div>

	        <s:hidden name="projectId"/>
	        <s:hidden name="projectCodeId" style="width:120px;"/>
	        <s:hidden name="taskStartDate"/>
	        <s:hidden name="endDate"/>
	        <s:hidden name="projectTypeId"/>
	        <s:hidden name="token" id="token"/>
	        <s:hidden name="cc" id="cc"/>

        	<input type="hidden" name="rows" id="rows" value="<s:property value="responseVo.responseList.size"/>"/>
        	<input type="hidden" name="saveAndSend" id="saveAndSend" value="false"/>
        	<s:hidden name="availableRecords" id="availableRecords"/>
		        <div class="save-btns-part" style="width: 1200px;">
		        <table width="1200px" border="0" cellspacing="0" cellpadding="0">
		          <tr>
		            <td style="width:40%;"></td>
		            <td>
		            	<a href="#" tabindex="1003" title="Save">
	                      	<img src="<%=contextPath%>/images/save-btn-new.png" align="middle" border="none" alt="Save"
	                      	onclick="return validateProjectAndActivity('saveId');"
	                      	/>
	                    </a>
					</td>
					<td>
						<a href="#" tabindex="1004" title="Save & Send">
	                      	<img src="<%=contextPath%>/images/save-send-btn-new.png" align="middle" border="none" alt="Save & Send Mail"
	                      	onclick="return validateProjectAndActivity('');"
	                      	/>
	                    </a>
		            </td>
		            <td style="width:40%;"></td>
		          </tr>
		        </table>
		        </div>
        </s:form>
    </div>
</div>
</center>
<script>

	function resetProjectAndManager(obj){
		var rowNo = obj.id.substring(13,obj.id.indexOf("__"));
		document.getElementById("projectCode_"+rowNo).value="";
		document.getElementById("project_"+rowNo).value="";
		document.getElementById("addTask_task_"+rowNo+"__reportempId").disabled = false;
		document.getElementById("addTask_task_"+rowNo+"__reportempId").options[0].selected=true;
	}

	function toUpper(obj){
		obj.value=obj.value.toUpperCase();
	}

	function resetSearchProject(){
		document.getElementById("projectCodeId").value="";
		document.getElementById("projectId").value="";
	}

	function checkOverLappingValidation(obj){

		var rows   = parseInt(document.getElementById("rows").value);
		var rowNo  = parseInt(obj.id.substring(9));

		var taskDate1  = obj.value;

		var	planStartTime1      = document.getElementById("addTask_task_"+rowNo+"__planStartTime").value.replace(/:/g,'');
		var	planStartTime1Exist = planStartTime1==""?false:true;
	        planStartTime1      = isNaN(planStartTime1)? 0 : parseFloat(planStartTime1);

	    var planEndTime1      	= document.getElementById("addTask_task_"+rowNo+"__planEndTime").value.replace(/:/g,'');
	    var	planEndTime1Exist 	= planEndTime1==""?false:true;
	        planEndTime1      	= isNaN(planEndTime1)? 0 : parseFloat(planEndTime1);

		var actualStartTime1      = document.getElementById("addTask_task_"+rowNo+"__actualStartTime").value.replace(/:/g,'');
		var	actualStartTime1Exist = actualStartTime1==""?false:true;
			actualStartTime1      = isNaN(actualStartTime1)? 0 : parseFloat(actualStartTime1);

		var actualEndTime1        = document.getElementById("addTask_task_"+rowNo+"__actualEndTime").value.replace(/:/g,'');
		var	actualEndTime1Exist   = actualEndTime1==""?false:true;
			actualEndTime1        = isNaN(actualEndTime1)? 0 : parseFloat(actualEndTime1);

		var isPlanInvalid         = (planStartTime1Exist && planEndTime1Exist && planStartTime1>=planEndTime1);
		var isActualInvalid       = (actualStartTime1Exist && actualEndTime1Exist && actualStartTime1>=actualEndTime1);

		var isPlanTimeInvalid     = (currentDate == taskDate1 && ((planStartTime1Exist && sysTime>=planStartTime1) || (planEndTime1Exist && sysTime>=planEndTime1)));
		var isPlanStartTimeInvalid  = (currentDate == taskDate1 && planStartTime1Exist && sysTime>=planStartTime1);
		var isPlanEndTimeInvalid    = (currentDate == taskDate1 && planEndTime1Exist && sysTime>=planEndTime1);

		var isActualTimeInvalid   = (currentDate == taskDate1 && ((actualStartTime1Exist && sysTime<=actualStartTime1) || (actualEndTime1Exist && sysTime<=actualEndTime1)));

		for(var i=0;i<rows;i++){
			if(i!=rowNo){
				var taskDate2 = document.getElementById("taskDate_"+i).value;
				if(taskDate1 == taskDate2){

					var planOverLap   = false;
					var actualOverLap = false;

					var	planStartTime2      = document.getElementById("addTask_task_"+i+"__planStartTime").value.replace(/:/g,'');
					var	planStartTime2Exist = planStartTime2==""?false:true;
				        planStartTime2      = isNaN(planStartTime2)? 0 : parseFloat(planStartTime2);

				    var planEndTime2      	= document.getElementById("addTask_task_"+i+"__planEndTime").value.replace(/:/g,'');
				    var	planEndTime2Exist 	= planEndTime2==""?false:true;
				        planEndTime2      	= isNaN(planEndTime2)? 0 : parseFloat(planEndTime2);

					var actualStartTime2      = document.getElementById("addTask_task_"+i+"__actualStartTime").value.replace(/:/g,'');
					var	actualStartTime2Exist = actualStartTime2==""?false:true;
						actualStartTime2      = isNaN(actualStartTime2)? 0 : parseFloat(actualStartTime2);

					var actualEndTime2        = document.getElementById("addTask_task_"+i+"__actualEndTime").value.replace(/:/g,'');
					var	actualEndTime2Exist   = actualEndTime2==""?false:true;
						actualEndTime2        = isNaN(actualEndTime2)? 0 : parseFloat(actualEndTime2);

					//plan
					if(planStartTime2Exist && planEndTime2Exist){
						if(planStartTime1Exist && planStartTime2<planStartTime1 && planStartTime1<planEndTime2){
							planOverLap = true;
						}
						else if(planEndTime1Exist && planStartTime2<planEndTime1 && planEndTime1<planEndTime2){
							planOverLap = true;
						}
					}
					if(planStartTime1Exist && planEndTime1Exist){
						if(planStartTime2Exist && planStartTime1<planStartTime2 && planStartTime2<planEndTime1){
							planOverLap = true;
						}
						else if(planEndTime2Exist && planStartTime1<planEndTime2 && planEndTime2<planEndTime1){
							planOverLap = true;
						}
					}
					if(planStartTime1Exist && planEndTime1Exist && planStartTime2Exist && planEndTime2Exist){
						if(planStartTime1==planStartTime2 && planEndTime1==planEndTime2){
							planOverLap = true;
						}
					}

					//actual
					if(actualStartTime2Exist && actualEndTime2Exist){
						if(actualStartTime1Exist && actualStartTime2<actualStartTime1 && actualStartTime1<actualEndTime2){
							actualOverLap = true;
						}
						else if(actualEndTime1Exist && actualStartTime2<actualEndTime1 && actualEndTime1<actualEndTime2){
							actualOverLap = true;
						}
					}
					if(actualStartTime1Exist && actualEndTime1Exist){
						if(actualStartTime2Exist && actualStartTime1<actualStartTime2 && actualStartTime2<actualEndTime1){
							actualOverLap = true;
						}
						else if(actualEndTime2Exist && actualStartTime1<actualEndTime2 && actualEndTime2<actualEndTime1){
							actualOverLap = true;
						}
					}
					if(actualStartTime1Exist && actualEndTime1Exist && actualStartTime2Exist && actualEndTime2Exist){
						if(actualStartTime1==actualStartTime2 && actualEndTime1==actualEndTime2){
							actualOverLap = true;
						}
					}
					if(planOverLap || actualOverLap){
						return {"planOverLap":planOverLap,"actualOverLap":actualOverLap,"row":(i+1),
							"isPlanInvalid":isPlanInvalid,"isActualInvalid":isActualInvalid,"rowNo":rowNo,
							"isPlanTimeInvalid":isPlanTimeInvalid,"isPlanStartTimeInvalid":isPlanStartTimeInvalid,"isPlanEndTimeInvalid":isPlanEndTimeInvalid,
							"isActualTimeInvalid":isActualTimeInvalid};
					}
				}
			}
		}
		return {"planOverLap":false,"actualOverLap":false,"row":0,
			"isPlanInvalid":isPlanInvalid,"isActualInvalid":isActualInvalid,"rowNo":rowNo,
			"isPlanTimeInvalid":isPlanTimeInvalid,"isPlanStartTimeInvalid":isPlanStartTimeInvalid,"isPlanEndTimeInvalid":isPlanEndTimeInvalid,
			"isActualTimeInvalid":isActualTimeInvalid};
	}
	function overLappingValidation(obj,msg){

		var isPlan  = (obj.id.indexOf("__plan")!=-1);
		var isStartTime = (obj.id.indexOf("StartTime")!=-1);
		var rowNo   = parseInt(obj.id.substring(13,obj.id.indexOf("__")));
		var dateObj = document.getElementById("taskDate_"+rowNo);

		if(isStartTime){
			var endId = obj.id.replace("Start","End");
			var endObj = document.getElementById(endId);
			endObj.value="";
		}
		var result = checkOverLappingValidation(dateObj);
		var validation = true;
		if(isPlan && isStartTime && result.isPlanStartTimeInvalid){
			alert("Planned time should be greater than Current time");
			obj.options[0].selected = true;
			validation = false;
		}
		else if(isPlan && !isStartTime && result.isPlanEndTimeInvalid){
			alert("Planned time should be greater than Current time");
			obj.options[0].selected = true;
			validation = false;
		}
		else if(result.isActualTimeInvalid && !isPlan){
			alert("Actual time should be less than Current time");
			obj.options[0].selected = true;
			validation = false;
		}
		else if((isPlan && result.isPlanInvalid) || (!isPlan && result.isActualInvalid)){
			alert("Start time should be less than End time");
			obj.options[0].selected = true;
			validation = false;
		}
		else if(isPlan && result.planOverLap){
			obj.options[0].selected = true;
			alert("Plan time is overlapping with row "+result.row);
			validation = false;
		}
		else if(!isPlan && result.actualOverLap){
			obj.options[0].selected = true;
			alert("Actual time is overlapping with row "+result.row);
			validation = false;
		}
		if(validation && isStartTime){
			var endId = obj.id.replace("Start","End");
			var endObj = document.getElementById(endId);
			if(endObj.value==""){
				endObj.options[obj.selectedIndex+1].selected = true;
				var result1 = checkOverLappingValidation(dateObj);
				if(isPlan && (result1.isPlanEndTimeInvalid || result1.planOverLap)){
					endObj.options[0].selected = true;
				}
				if(!isPlan && (result1.isActualTimeInvalid || result1.actualOverLap)){
					endObj.options[0].selected = true;
				}
			}
		}
	}
</script>
<script>

	window.onload = function(){

		var table = document.getElementById("table");
		var rows = parseInt(document.getElementById("rows").value);
		var firstRow = 1;
		for(var i=firstRow;i<=rows;i++){
			var taskFor = document.getElementById("taskDate_"+(i-1)+"_taskfor").value;
			var savedTask = document.getElementById("taskId_"+(i-1)).value!="";
			var row = table.rows[i];
			if(taskFor == "plan"){
				disableCell(row.cells[7]);
				disableCell(row.cells[8]);
				disableCell(row.cells[9]);
				disableCell(row.cells[10]);
				disableCell(row.cells[11]);
				disableCell(row.cells[12]);
				disableCell(row.cells[13]);
			}
			else if (taskFor == "actual"){
				if(savedTask){
					disableCell(row.cells[1]);
				}
				disableCell(row.cells[5]);
				disableCell(row.cells[6]);
			}
			else if (taskFor == "both"){

				var planStartTime = document.getElementById("addTask_task_"+(i-1)+"__planStartTime").value;
				if(planStartTime!="" && parseFloat(planStartTime.replace(/:/g,''))<sysTime){
					disableCell(row.cells[1]);
					document.getElementById("addTask_task_"+(i-1)+"__planStartTime").disabled="true";
				}

				var planEndTime = document.getElementById("addTask_task_"+(i-1)+"__planEndTime").value;
				if(planEndTime!="" && parseFloat(planEndTime.replace(/:/g,''))<sysTime){
					if(savedTask){
						disableCell(row.cells[1]);
					}
					disableCell(row.cells[5]);
					disableCell(row.cells[6]);
				}
			}
			//updateManager(document.getElementById("projectCode_"+(i-1)));
			clearSize(i-1);
			setTabIndex(i-1);
		}
		document.getElementById("startDateStrId").value = '${taskStartDate}';
		document.getElementById("endDateStrId").value = '${endDate}';
		document.getElementById("trnsBack").style.display="none";
	}
    $(document).ready(function(){
  	  $(".datepick").datepicker('destroy');
  	  $('.datepick').each(function(i) {
			this.id    = 'taskDate_' + i;
			this.name  = 'task[' + i + '].taskDateStr';
			this.maxLength = '10';
			this.style.width = '75px';
			this.value = document.getElementById('taskDateBackup_' + i).value;
 		}).datepicker({
             dateFormat: 'dd/mm/yy',
             buttonImage: '<%=contextPath%>/struts/js/calendar.gif',
             buttonImageOnly: true,
             showOn: "both",
             onSelect: function(dateText, inst) {
            	dateOverlappingValidation(inst,this);
        	}
         });
    });
    function clearSize(row){
    	var sizeObj = document.getElementById("size_"+row);
    	var unitObj = document.getElementById("addTask_task_"+row+"__unitId");
    	if(unitObj.value=="0"){
    		sizeObj.value="";
    	}
    }
    function doGetCaretPosition (oField) {
    	var iCaretPos = 0;
    	if(document.selection) {
        	oField.focus ();
        	var oSel = document.selection.createRange ();
        	oSel.moveStart ('character', -oField.value.length);
        	iCaretPos = oSel.text.length;
        }
        else if(oField.selectionStart || oField.selectionStart == '0'){
        	iCaretPos = oField.selectionStart;
        }
        return (iCaretPos);
    }
    function decimalValidation(obj,event){
    	var value = obj.value;
    	var keyCode = event.charCode ? event.charCode : event.keyCode;

    	if(keyCode==8 || keyCode==35 || keyCode==36 || keyCode==37 || keyCode==39 || keyCode==46 || keyCode==9){
    		return true;
    	}
    	var caretPos = doGetCaretPosition(obj);
    	var isNumber = (keyCode>47 && keyCode<58);
    	var isDot    = keyCode == 190;

		if ( keyCode == 13 || //enter
		    (isDot && caretPos==7) || // dot at last
			(isDot && value.indexOf(".")!=-1)) { // double dot
			return false;
		}
		if(isDot && value.length-caretPos>1){//decimal 1
			return false;
		}
		if(value.indexOf(".")==-1 && value.length==6 && isNumber){
			return false;
		}
		if(value.indexOf(".")!=-1 && caretPos>(value.indexOf(".")+1) && isNumber){
			return false;
		}
		if(parseFloat(value)>1000000 && isNumber){
			return false;
		}
		if(isDot || isNumber){
			return true;
		}
		return false;
    }
    function setTabIndex(row){
    	var searchTabIndex = 7;
    	var rowsTab = row * 17;
    	var index=0;
    	document.getElementById("taskDate_"+row).tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("addTask_task_"+row+"__projectTypeActionVo_projectTypeId").tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("projectCode_"+row).tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("projectPopup_"+row).tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("addTask_task_"+row+"__reportempId").tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("planTask_"+row).tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("addTask_task_"+row+"__planStartTime").tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("addTask_task_"+row+"__planEndTime").tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("copy_"+row).tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("actualTask_"+row).tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("addTask_task_"+row+"__actualStartTime").tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("addTask_task_"+row+"__actualEndTime").tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("activityCode_"+row).tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("activityPopup_"+row).tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("workItem_"+row).tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("module_"+row).tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("size_"+row).tabIndex = searchTabIndex + rowsTab + index++;
    	document.getElementById("addTask_task_"+row+"__unitId").tabIndex = searchTabIndex + rowsTab + index++;
    }
    function copyPlanToActual(copyId){
    	var row = parseInt(copyId.substring(5));
    	var planTaskObj = document.getElementById("planTask_"+row);
    	var actualTaskObj = document.getElementById("actualTask_"+row);
    	var planStartTime = document.getElementById("addTask_task_"+row+"__planStartTime").value;
    	var planEndTime   = document.getElementById("addTask_task_"+row+"__planEndTime").value;

    	var actualStartTimeObj = document.getElementById("addTask_task_"+row+"__actualStartTime");
    	var actualEndTimeObj   = document.getElementById("addTask_task_"+row+"__actualEndTime");
    	var taskDateObj        = document.getElementById("taskDate_"+row);

    	actualTaskObj.value = planTaskObj.value;
    	actualStartTimeObj.value = planStartTime;
    	actualEndTimeObj.value   = planEndTime;
    	var result = checkOverLappingValidation(taskDateObj);
    	if(result.isActualTimeInvalid){
			actualStartTimeObj.options[0].selected = true;
			actualEndTimeObj.options[0].selected = true;
			alert("Actual time cannot copy from plan, because time is greater than Current time");
		}
    	else if(result.actualOverLap){
			actualStartTimeObj.options[0].selected = true;
			actualEndTimeObj.options[0].selected = true;
			alert("Actual time cannot copy from plan,because time is overlapping with row "+result.row);
		}
    	return false;
    }
    function isValidDate(date){
		var matches = /^(\d{2}|\d{1})[\/\/](\d{2}|\d{1})[\/\/](\d{4})$/.exec(date.value);
	    if (matches == null) return false;
	    var d = matches[1];
	    var m = matches[2] - 1;
	    var y = matches[3];
	    var composedDate = new Date(y, m, d);
	    var result = composedDate.getDate() == d &&
	           composedDate.getMonth() == m &&
	           composedDate.getFullYear() == y &&
	           (parseInt(y)>=2000);
	    if(result){
	    	if(d.length==1) d="0"+d;
	    	if(m<9) m="0"+(m+1);
	    	else m=(m+1);
	    	date.value = d+"/"+m+"/"+y;
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}
	function dateValidation(obj){
		if(obj.value!=""){
			if(isValidDate(obj)){
				dateOverlappingValidation({"lastVal":""},obj);
				return true;
			}
			else{
				alert("Date is invalid. (Format - dd/mm/yyyy)");
				return false;
			}
		}
	}
    function dateOverlappingValidation(inst,dtObj){
    	var result = checkOverLappingValidation(dtObj);
    	if(result.isPlanTimeInvalid){
			alert("Planned time should be greater than Current time");
		}
		else if(result.isActualTimeInvalid){
			alert("Actual time should be less than Current time");
		}
		else if(result.planOverLap && result.actualOverLap){
    		alert("Planned date time and Actual date time is overlapping with row "+result.row);
    	}
    	else if(result.planOverLap){
    		alert("Planned date time is overlapping with row "+result.row);
    	}
    	else if(result.actualOverLap){
    		alert("Actual date time is overlapping with row "+result.row);
    	}
    	if(result.planOverLap || result.actualOverLap || result.isPlanTimeInvalid || result.isActualTimeInvalid){
    		dtObj.value = inst.lastVal;
    	}
    	else{
    		var table = document.getElementById("table");
    		var row = table.rows[result.rowNo+1];
    		var result1 = compareDate(dtObj.value,currentDate);
    		enableRow(row);
    		if(dtObj.value!=""){
	    		if(result1.equal){
	    			//both
	    			enableRow(row);
	    			document.getElementById("taskDate_"+result.rowNo+"_taskfor").value = "both";
	    		}
	    		else if(result1.d1gtd2){
	    			//Plan only
	    			disableActual(row);
	    			document.getElementById("taskDate_"+result.rowNo+"_taskfor").value = "plan";
	    		}
	    		else{
	    			//Actual only
	    			disablePlan(row);
	    			document.getElementById("taskDate_"+result.rowNo+"_taskfor").value = "actual";
	    		}
    		}
    	}
    }
    function disablePlan(row){
    	disableCellAndReset(row.cells[5]);
    	disableCellAndReset(row.cells[6]);
    }
    function disableActual(row){
    	disableCellAndReset(row.cells[7]);
    	disableCellAndReset(row.cells[8]);
    	disableCellAndReset(row.cells[9]);
    	disableCellAndReset(row.cells[10]);
    	disableCellAndReset(row.cells[11]);
    	disableCellAndReset(row.cells[12]);
    	disableCellAndReset(row.cells[13]);
    }
    function compareDate(date1,date2){
    	try{
    		var dt1  = parseInt(date1.substring(0,2),10);
    	    var mon1 = parseInt(date1.substring(3,5),10);
    	    var yr1  = parseInt(date1.substring(6,10),10);
    	    var dt2  = parseInt(date2.substring(0,2),10);
    	    var mon2 = parseInt(date2.substring(3,5),10);
    	    var yr2  = parseInt(date2.substring(6,10),10);

    	    var d1 = new Date(yr1, mon1, dt1);
    	    var d2 = new Date(yr2, mon2, dt2);

	    	if(d1.getTime()>d2.getTime()){
	    		return {"equal":false,"d1gtd2":true,error:false}
	    	}
	    	else if(date1 == date2){
	    		return {"equal":true,"d1gtd2":false,error:false}
	    	}
	    	return {"equal":false,"d1gtd2":false,error:false}
    	}
    	catch(e){
    		return {"equal":false,"d1gtd2":false,error:true};
    	}
	}
    function disableCellAndReset(cell){
		var txtArea = cell.getElementsByTagName("textarea");
		for(var i=0;i<txtArea.length;i++){
			txtArea[i].disabled = true;
			txtArea[i].value = "";
		}
		var select = cell.getElementsByTagName("select");
		for(var i=0;i<select.length;i++){
			select[i].disabled = true;
			select[i].options[0].selected=true;
		}
		var input = cell.getElementsByTagName("input");
		for(var i=0;i<input.length;i++){
			input[i].disabled = true;
			input[i].value = "";
		}
		var img = cell.getElementsByTagName("img");
		for(var i=0;i<img.length;i++){
			img[i].style.display = "none";
		}
	}
	function disableCell(cell){
		var txtArea = cell.getElementsByTagName("textarea");
		for(var i=0;i<txtArea.length;i++) txtArea[i].disabled = true;
		var select = cell.getElementsByTagName("select");
		for(var i=0;i<select.length;i++) select[i].disabled = true;
		var input = cell.getElementsByTagName("input");
		for(var i=0;i<input.length;i++) input[i].disabled = true;
		var img = cell.getElementsByTagName("img");
		for(var i=0;i<img.length;i++) img[i].style.display = "none";
	}
	function enableAllRows(){
		var table = document.getElementById("table");
		var rows = parseInt(document.getElementById("rows").value);
		var firstRow = 1;
		for(var i=firstRow;i<=rows;i++){
			var row = table.rows[i];
			enableRow(row);
			enableCell(row.cells[4]);
		}
	}
	function enableRow(row){
		enableCell(row.cells[1]);
		enableCell(row.cells[5]);
		enableCell(row.cells[6]);
		enableCell(row.cells[7]);
		enableCell(row.cells[8]);
		enableCell(row.cells[9]);
		enableCell(row.cells[10]);
		enableCell(row.cells[11]);
		enableCell(row.cells[12]);
		enableCell(row.cells[13]);
	}
	function enableCell(cell){
		var txtArea = cell.getElementsByTagName("textarea");
		for(var i=0;i<txtArea.length;i++) txtArea[i].disabled = false;
		var select = cell.getElementsByTagName("select");
		for(var i=0;i<select.length;i++) select[i].disabled = false;
		var input = cell.getElementsByTagName("input");
		for(var i=0;i<input.length;i++) input[i].disabled = false;
		var img = cell.getElementsByTagName("img");
		for(var i=0;i<img.length;i++) img[i].style.display = "";
	}
	function copyAddRow(){
		addRow();
		var rows = parseInt(document.getElementById("rows").value);
		if(rows && rows>1){
			var fromRow = rows-2;
			var toRow   = rows-1;

			var taskDateFromObj    = document.getElementById("taskDate_"+fromRow);
			var taskforFromObj     = document.getElementById("taskDate_"+fromRow+"_taskfor");
			var projectTypeFromObj = document.getElementById("addTask_task_"+fromRow+"__projectTypeActionVo_projectTypeId");
			var projectCodeFromObj = document.getElementById("projectCode_"+fromRow);
			var projectFromObj     = document.getElementById("project_"+fromRow);
			var reportempIdFromObj = document.getElementById("addTask_task_"+fromRow+"__reportempId");

			var taskDateToObj    = document.getElementById("taskDate_"+toRow);
			var taskforToObj     = document.getElementById("taskDate_"+toRow+"_taskfor");
			var projectTypeToObj = document.getElementById("addTask_task_"+toRow+"__projectTypeActionVo_projectTypeId");
			var projectCodeToObj = document.getElementById("projectCode_"+toRow);
			var projectToObj     = document.getElementById("project_"+toRow);
			var reportempIdToObj = document.getElementById("addTask_task_"+toRow+"__reportempId");
			taskDateToObj.value 	= taskDateFromObj.value;
			taskforToObj.value 		= taskforFromObj.value;
			projectTypeToObj.value 	= projectTypeFromObj.value;
			projectCodeToObj.value 	= projectCodeFromObj.value;
			projectToObj.value 		= projectFromObj.value;
			reportempIdToObj.value 	= reportempIdFromObj.value;
			var result1 = compareDate(taskDateToObj.value,currentDate);
			var table = document.getElementById("table");
			var row = table.rows[table.rows.length-1];
			enableRow(row);
			if(taskDateToObj.value!=""){
	    		if(result1.equal){
	    			//both
	    			enableRow(row);
	    			document.getElementById("taskDate_"+toRow+"_taskfor").value = "both";
	    		}
	    		else if(result1.d1gtd2){
	    			//Plan only
	    			disableActual(row);
	    			document.getElementById("taskDate_"+toRow+"_taskfor").value = "plan";
	    		}
	    		else{
	    			//Actual only
	    			disablePlan(row);
	    			document.getElementById("taskDate_"+toRow+"_taskfor").value = "actual";
	    		}
			}
			return false;
		}
	}
	function addRow(){

		var table = document.getElementById("table");
		var rowCount = table.rows.length;
		var currentRow  = rowCount-1;
		var innerHTML = "";
		var row = table.insertRow(rowCount);

		var cellNo = 0;
		var sourceRow = 1;

		var cell = row.insertCell(cellNo);
		cell.align =  "center";
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		cell.innerHTML = currentRow+1;
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.align =  "left";
		cell.style.whiteSpace = "nowrap";
		cell.className = 'table-title-content';
		innerHTML = '<input type="text" class="datepick" size="11">'
				  + '<input type="hidden" id="taskDate_'+currentRow+'_taskfor"/>';
		cell.innerHTML = innerHTML;
		$('.datepick').each(function(i) {
			this.id    = 'taskDate_' + i;
			this.name  = 'task[' + i + '].taskDateStr';
			this.maxLength = '10';
			this.style.width = '75px';
		}).datepicker({
            dateFormat: 'dd/mm/yy',
            buttonImage: '<%=contextPath%>/struts/js/calendar.gif',
            buttonImageOnly: true,
            showOn: "both",
            onSelect: function(dateText, inst) {
            	dateOverlappingValidation(inst,this);
            	return false;
       		}
        }).change(function() {
        	dateValidation(this)
        });

		document.getElementById("taskDate_"+currentRow).value="";
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/addTask_task_0/g,"addTask_task_"+currentRow);
		innerHTML = innerHTML.replace(/taskId_0/g,"taskId_"+currentRow);
		innerHTML = innerHTML.replace(/timesheetId_0/g,"timesheetId_"+currentRow);
		innerHTML = innerHTML.replace(/associatedProjectId_0/g,"associatedProjectId_"+currentRow);
		cell.innerHTML = innerHTML;
		document.getElementById("addTask_task_"+currentRow+"__projectTypeActionVo_projectTypeId").options[0].selected=true;
		document.getElementById("taskId_"+currentRow).value="";
		document.getElementById("timesheetId_"+currentRow).value="";
		document.getElementById("associatedProjectId_"+currentRow).value="";
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/projectCode_0/g,"projectCode_"+currentRow);
		innerHTML = innerHTML.replace(/project_0/g,"project_"+currentRow);
		innerHTML = innerHTML.replace(/projectPopup_0/g,"projectPopup_"+currentRow);
		innerHTML = innerHTML.replace(",'project_"+currentRow+"',0)" , ",'project_"+currentRow+"',"+currentRow+")");
		cell.innerHTML = innerHTML;
		document.getElementById("project_"+currentRow).value = "";
		document.getElementById("projectCode_"+currentRow).value = "";
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/addTask_task_0/g,"addTask_task_"+currentRow);
		cell.innerHTML = innerHTML;
		document.getElementById("addTask_task_"+currentRow+"__reportempId").options[0].selected=true;
		document.getElementById("addTask_task_"+currentRow+"__reportempId").disabled=false;
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/planTask_0/g,"planTask_"+currentRow);
		cell.innerHTML = innerHTML;
		document.getElementById("planTask_"+currentRow).value = "";
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/addTask_task_0/g,"addTask_task_"+currentRow);
		cell.innerHTML = innerHTML;
		document.getElementById("addTask_task_"+currentRow+"__planStartTime").options[0].selected=true;
		document.getElementById("addTask_task_"+currentRow+"__planEndTime").options[0].selected=true;
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		cell.style.padding=table.rows[sourceRow].cells[cellNo].style.padding;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/copy_0/g,"copy_"+currentRow);
		cell.innerHTML = innerHTML;
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/actualTask_0/g,"actualTask_"+currentRow);
		cell.innerHTML = innerHTML;
		document.getElementById("actualTask_"+currentRow).value = "";
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/addTask_task_0/g,"addTask_task_"+currentRow);
		cell.innerHTML = innerHTML;
		document.getElementById("addTask_task_"+currentRow+"__actualStartTime").options[0].selected=true;
		document.getElementById("addTask_task_"+currentRow+"__actualEndTime").options[0].selected=true;
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.align =  "left";
		cell.style.whiteSpace = "nowrap";
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/activityCode_0/g,"activityCode_"+currentRow);
		innerHTML = innerHTML.replace(/activityId_0/g,"activityId_"+currentRow);
		innerHTML = innerHTML.replace(/activityPopup_0/g,"activityPopup_"+currentRow);
		cell.innerHTML = innerHTML;
		document.getElementById("activityCode_"+currentRow).value="";
		document.getElementById("activityId_"+currentRow).value="";
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.align =  "center";
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/workItem_0/g,"workItem_"+currentRow);
		cell.innerHTML = innerHTML;
		document.getElementById("workItem_"+currentRow).value = "";
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.align =  "center";
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/module_0/g,"module_"+currentRow);
		cell.innerHTML = innerHTML;
		document.getElementById("module_"+currentRow).value = "";
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.align =  "center";
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/size_0/g,"size_"+currentRow);
		cell.innerHTML = innerHTML;
		document.getElementById("size_"+currentRow).value = "";
		cellNo++;

		cell = row.insertCell(cellNo);
		cell.align =  "center";
		cell.style.verticalAlign="top";
		cell.className = table.rows[sourceRow].cells[cellNo].className;
		innerHTML = table.rows[sourceRow].cells[cellNo].innerHTML;
		innerHTML = innerHTML.replace(/task\[0\]/g,"task["+currentRow+"]");
		innerHTML = innerHTML.replace(/addTask_task_0__unitId/g,"addTask_task_"+currentRow+"__unitId");
		cell.innerHTML = innerHTML;
		document.getElementById("addTask_task_"+currentRow+"__unitId").options[0].selected=true;
		cellNo++;

		document.getElementById("rows").value = currentRow+1;
		enableRow(row);
		setTabIndex(currentRow);
		return false;
	}
	function removeRow(){
		var table = document.getElementById("table");
		var rowCount = table.rows.length;
		var availableRecords = parseInt(document.getElementById("availableRecords").value);
		if(rowCount>2 && rowCount>availableRecords+1){
			rowCount = rowCount -1;
			table.deleteRow(rowCount);
			document.getElementById("rows").value = rowCount-1;
		}
		return false;
	}

	function isEnterKey(event){
		var keyCode = event.charCode ? event.charCode : event.keyCode;
		if (keyCode == 13) {
			return true;
		}
		return false;
	}
	function funRefreshParent()
	{
	 document.getElementById("searchTask").action="searchTask.action";
	 document.getElementById("searchTask").method="POST";
	 document.getElementById("searchTask").submit();
	}
	function funAddOrModWindow(url,windowName,height,width,type,comboName,hiddenName,hiddenParam,marginHeight,modifyOneRecord,modifyRecord,Title,closeButton, Okbutton,CancelButton){
		funWindowOpen(url,windowName,height,width,type,comboName,hiddenName,hiddenParam,marginHeight,modifyOneRecord,modifyRecord,Title,closeButton, Okbutton,CancelButton);
	}

	function autoLogout(){

	}
</script>
</center>
</body>
</html>
