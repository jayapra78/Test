<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader("Expires", 0);
        response.setLocale(java.util.Locale.ENGLISH);
%>
<%
	/* Getting Context Path */
	String contextPath = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Leave Form-SRM Technologies</title>
		<sj:head  compressed="false"/>
		<sx:head debug="false" />
		<s:head theme="simple"/>
		<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/shirai.css" />
		<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
		<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="<%=contextPath%>/js/Popup.js"></script>
		<script type="text/javascript" src="<%=contextPath%>/js/Shirai.js"></script>
		<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/popup.css" />
		<style type="text/css">
.button_style{
	background-color : black;
	color: white;
	cursor: pointer;
	}
.logout_style {
	background: transparent;
	color: white;
	cursor: pointer;
}
</style>

<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }

</script>
</head>
<body>
<div class="main">
    <div class="page">
        <div class="top-header">
				<div class="title-heading">
					<div class="plan-lead-logo">
						<img src="<%=contextPath%>/images/plan-lead-logo.png" width="210"
							height="65" border="0" alt="Logo" title="Plan-Lead" />
					</div>
					<div class="tech-trends-logo">
						<img src="<%=contextPath%>/images/tech-trends-logo.png"
							width="195" height="65" border="0" alt="Logo" title="Tech-Trend" />
					</div>
					<div class="logout">
						<a href="logOut">Logout</a>
					</div>

				</div>
			</div>
		<div class="emp-details-part">
		<s:form name="updateLeave" id="updateLeave" action="updateLeave" method="POST" theme="simple">
        <div class="main_heading">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="33%">&nbsp;</td>
						<td width="34%">Leave Approval</td>
						<td width="33%"> <s:include value="/jsp/menu/MainMenu.jsp"></s:include></td>
					</tr>
				</table>
			</div>
        <div>
	        <table width="100%">
				<tr>
					<td nowrap="nowrap" align="left">
						<div id="errorMessageDiv">
							<s:if test="hasActionErrors()">
								<s:actionerror cssClass="error" />
							</s:if>
						</div>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap" align="center">
						<div id="successMessageDiv">
							<s:if test="hasActionMessages()">
								<s:actionmessage cssClass="success" />
							</s:if>
						</div>
					</td>
				</tr>
	      	</table>
        </div>

        <s:hidden name="leaveActionVo.empLeaveDetailId" value="%{leaveActionVo.empLeaveDetailId}" > </s:hidden>
        <s:hidden name="leaveActionVo.employeeId" value="%{leaveActionVo.employeeId}" > </s:hidden>
        <s:hidden name="leaveActionVo.reportingManager" value="%{leaveActionVo.reportingManager}" > </s:hidden>
        <s:hidden name="leaveActionVo.leaveTypeActionVo.leaveTypeId" value="%{leaveActionVo.leaveTypeActionVo.leaveTypeId}" > </s:hidden>
        <s:hidden name="leaveActionVo.createdBy" value="%{leaveActionVo.createdBy}" > </s:hidden>
		<s:hidden name="leaveActionVo.createdOn" value="%{leaveActionVo.createdOn}" > </s:hidden>

            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #DDE4E8">
              <tr>
                <td style="width:200px;"></td>
                <td align="center">
                <div style="background-color:#fff; width:100%; padding:5px 5px 20px; border:#8c8f91 solid 1px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       <tr bgcolor="#989898">
                           <td style="width:60%;"></td>
                           <td class="pro-details-field-name" style="color:#fff; font-weight:bold;">Reporting Manager </td>
                           <td style="">

	                    		<s:select name="leaveActionVo.reportingManager" list="employeeList" listKey="idValue" listValue="name" headerKey="0" headerValue="Select"
									cssClass="list-box"  id="reportingManagerId" disabled="true"
									style="width:150px; vertical-align: middle;"  tabindex="1"/>

	                    	</td>
                           <td style=" width:5%;"></td>
                          </tr>
                          <tr><td>&nbsp;</td></tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="vertical-align:top; width:50px;"></td>
                        <td style="vertical-align:top;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td class="leave-form-field-name" nowrap="nowrap">Kind of Leave </td>

                                <td colspan="2" align="left">
								<s:select list="kindOfLeaveList" listKey="idValue" listValue="name" headerKey="0" headerValue="Select"
									cssClass="list-box" name="leaveActionVo.leaveTypeActionVo.leaveTypeId" id="leaveTypeId" disabled="true"
									style="width:151px; vertical-align: middle;" value="%{leaveActionVo.leaveTypeActionVo.leaveTypeId}"
									tabindex="2"/>
								</td>
                              </tr>
                              <tr>
                                <td class="leave-form-field-name" style="width:100px;">From Date </td>
                                <td class="leave-form-textbox">
		                    	<sj:datepicker displayFormat="dd/mm/yy" tabindex="3" id="startDateStrId" onchange="dateValidation(this)"  showOn="false"
								 cssStyle="width:148px;" onChangeTopics="onChangeDate" maxlength="10" buttonImageOnly="true" value="%{leaveActionVo.fromDate}"
								 name="leaveActionVo.fromDateStr" readonly="true" onpaste="return false;"/>
		                    	</td>

                              </tr>
                              <tr>
                                <td class="leave-form-field-name">To Date </td>
                                <td><sj:datepicker displayFormat="dd/mm/yy" tabindex="4" onchange="dateValidation(this)"
								id="endDateStrId" cssStyle="width:148px;" cssClass="txt" buttonImageOnly="true" readonly="true" showOn="false"
								onChangeTopics="onChangeDate" maxlength="10" onpaste="return false;"   value="%{leaveActionVo.toDate}"
								name="leaveActionVo.toDateStr" /></td>

                              </tr>
                              <tr>
                                <td class="leave-form-field-name">No of Days </td>
								<td colspan="2"><s:textfield
										name="leaveActionVo.noOfDays" id="noOfDaysId" size="20"
										tabindex="5" maxlength="3" value="%{leaveActionVo.noOfDays}" readonly="true"
										onkeypress="return numbersonly(event)" cssStyle="margin:4px 0px 0px 0px"/></td>
							</tr>
                              <tr>
                                <td class="leave-form-field-name" nowrap="nowrap" >Date Of Rejoining  &nbsp;</td>
                                <td nowrap="nowrap">

                                <sj:datepicker displayFormat="dd/mm/yy" tabindex="6" onchange="dateValidation(this)"
								id="rejoinDateId" cssStyle="width:148px;" cssClass="txt" buttonImageOnly="true" readonly="true" showOn="false"
								onChangeTopics="onChangeDate" maxlength="10" onpaste="return false;" value="%{leaveActionVo.rejoiningDate}"
								name="leaveActionVo.rejoiningDateStr" />
								</td>

                              </tr>
                              <tr>
                                <td class="leave-form-field-name" valign="top">Remarks</td>
                                <td colspan="2" align="left">
									<s:textarea id="remarkId"
										name="leaveActionVo.empRemarks" rows="8" cols="50"
										maximumLength="400" tabindex="7" readonly="true"
										cssStyle="width:148px; height:40px;resize:none;margin:4px 0px 0px 0px; overflow:auto;scroll:auto" />
								</td>
                              </tr>
                            </table>
                        </td>
                        <td style="width:30px;"></td>
                        <td style="vertical-align:top;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td class="leave-form-field-name" style="vertical-align:top;">Reason for Leave </td>
                                <td colspan="2" align="left">
                                <s:textarea id="reasonId"
										name="leaveActionVo.reasonForLeave" rows="8" cols="50"
										maximumLength="400" tabindex="8" readonly="true"
										cssStyle="width:200px; height:40px;resize:none;margin:4px 0px 0px 0px; overflow:auto;scroll:auto" />
                                </td>
                              </tr>
                              <tr>
                                <td class="leave-form-field-name" valign="top" nowrap="nowrap">Address During Leave &nbsp;</td>
								<td colspan="2" align="left"><s:textarea id="addressId"
										name="leaveActionVo.empAddress" rows="8" cols="50"
										maximumLength="400" tabindex="9" readonly="true"
										cssStyle="width:200px; height:40px;resize:none;margin:4px 0px 0px 0px; overflow:auto;scroll:auto" />
								</td>
							</tr>
                              <tr>
                                <td class="leave-form-field-name">Tel. No </td>
                                <td colspan="2" align="left">
                                <s:textfield cssStyle="margin:4px 0px 0px 0px" readonly="true" name="leaveActionVo.contactNumber" value="%{leaveActionVo.contactNumber}"  id="contactNumberId"  size="29" tabindex="10" maxlength="10" onkeypress="return numbersonly(event)" />
                                </td>
                              </tr>
                              <tr>
                                <td class="leave-form-field-name">Email id </td>
                                <td colspan="2" align="left"><s:textfield cssStyle="margin:4px 0px 0px 0px" readonly="true" name="leaveActionVo.contactEmailId" value="%{leaveActionVo.contactEmailId}" id="contactEmailId"  size="29" tabindex="11" maxlength="50" /></td>
                              </tr>
                              <tr>
                                <td class="leave-form-field-name">Leave Approval *</td>
                                <td colspan="2" align="left">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <s:select list="leaveStatusList" listKey="idValue" listValue="name" headerKey="0" headerValue="Select"
												cssClass="list-box" name="leaveActionVo.leaveStatusActionVo.leaveStatusId" id="leaveStatusId"
												style="width:200px; vertical-align: middle;" tabindex="12"/>
                                      </tr>
                                    </table>
                                </td>
                              </tr>
                              <tr>
                                <td class="leave-form-field-name">Reject Reason</td>
                                <td colspan="2" align="left">
									<s:textfield cssStyle="margin:4px 0px 0px 0px" name="leaveActionVo.supervisorComments" id="supervisorCommentsId"  size="29" tabindex="13" maxlength="50" />
								</td>
                              </tr>
                            </table>
                        </td>
                        <td style="vertical-align:top; width:40px;"></td>
                      </tr>
                      <tr>
                      <td colspan="5">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td style="vertical-align:top; width:36%;"></td>
                             <s:url action="saveLeave" var="saveLeave">

                             </s:url>
                             <td class="save-btn" align="center">
                              <s:a href="#"><input align="middle" type="image" name="saveId" id="saveId" tabindex="14"
										   src="<%=contextPath%>/images/update.png" onclick="return addValidation();"/>  </s:a>

								</td>

								<td style="width:20px;"></td>

								<td class="save-btn">
									<input align="middle" type="image" onclick="return closeWindow()" tabindex="15"
										   src="<%=contextPath%>/images/cancel-btn-new.png"/>
				            </td>

                            <td style="vertical-align:top; width:36%;"></td>
                          </tr>
                        </table>
                       </td>
                      </tr>
                    </table>

                </div>
                </td>
                <td style="width:200px; height: 100px"></td>
              </tr>
            </table>
			<table width="100%" style="background-color: #DDE4E8">
				<tr>
					<td height="80px">&nbsp;</td>
				</tr>
			</table>
        </s:form>
        <div class="footer-part">Copyright © 2012 SRM Technologies Private Limited
        </div>
        </div>
    </div>
</div>
<script>
	var conPath = "<%=contextPath%>";
	function closeWindow(){
		document.updateLeave.action = conPath + "/app/listLeave";
		return true;
	}

	function parseErrorMsg(msg){
		return "<li><span>" + msg + "</span></li>";
	}

	function addValidation(){
		var errMsg = "";
		var isInvalid = false;

		try{
			var startDate = document.getElementById("startDateStrId");
			var endDate = document.getElementById("endDateStrId");
			var rejoinDate = document.getElementById("rejoinDateId");


			var leaveApproval = document.getElementById("leaveStatusId");

			if(leaveApproval.value == "0"){
				errMsg += parseErrorMsg("Leave Approval is required");
				isInvalid = true;
			}


			if(startDate.value!="" && !isValidDate(startDate)){
				alert("From Date is invalid. (Format - dd/mm/yyyy)");
				return false;
			}

			if(endDate.value!="" && !isValidDate(endDate)){
				alert("To Date is invalid. (Format - dd/mm/yyyy)");
				return false;
			}
			startDate = startDate.value;
			startDate = startDate.split("/");
			var startDate1 = new Date();
			startDate1.setFullYear(parseInt(startDate[2]));
			startDate1.setMonth(parseInt(startDate[1]));
			startDate1.setDate(parseInt(startDate[0]));

			endDate = endDate.value;
			endDate = endDate.split("/");
			var endDate1 = new Date();
			endDate1.setFullYear(parseInt(endDate[2]));
			endDate1.setMonth(parseInt(endDate[1]));
			endDate1.setDate(parseInt(endDate[0]));

			if (startDate1 > endDate1) {
				alert("To Date should be greater or equal to the From Date");
				return false;
			}

			rejoinDate = rejoinDate.value;
			rejoinDate = rejoinDate.split("/");
			var rejoinDate1 = new Date();
			rejoinDate1.setFullYear(parseInt(rejoinDate[2]));
			rejoinDate1.setMonth(parseInt(rejoinDate[1]));
			rejoinDate1.setDate(parseInt(rejoinDate[0]));
			if (rejoinDate1 < endDate1) {
				alert("Date of rejoining should be greater than To Date");
				return false;
			}
			if(isInvalid){
			showErrorMsg(errMsg);
			return false;
		}
			updateLeave.submit();
		}
		catch(e){
			alert(e);
		}
		return true;
	}

	function showErrorMsg(err){
		var msg = "<ul id=\"addTask_\" class=\"error\">";
        msg += err;
        msg += "</ul>";
        document.getElementById("errorMessageDiv").innerHTML = msg;
	}

	function dayCalculation(){
		var startDate = document.getElementById("startDateStrId").value;
		var endDate = document.getElementById("endDateStrId").value;

		var date1 = Date.parse(startDate);
		var date2 = Date.parse(endDate);
		var dayDiff = (date2 / (1000*60*60*24)) - (date1 / (1000*60*60*24));
	}


	function funRefreshParent()
	{
	 document.getElementById("saveLeave").action="saveLeave.action";
	 document.getElementById("saveLeave").method="POST";
	 document.getElementById("saveLeave").submit();
	}

	function isValidEmail(email) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        return reg.test(email);
	}

	function dateValidation(obj){
		if(obj.value!=""){
			if(isValidDate(obj)){
				dateOverlappingValidation({"lastVal":""},obj);
				return true;
			}
			else{
				alert("Date is invalid. (Format - dd/mm/yyyy)");
				return false;
			}
		}
	}
	function isValidDate(date){
		var matches = /^(\d{2}|\d{1})[\/\/](\d{2}|\d{1})[\/\/](\d{4})$/.exec(date.value);
	    if (matches == null) return false;
	    var d = matches[1];
	    var m = matches[2] - 1;
	    var y = matches[3];
	    var composedDate = new Date(y, m, d);
	    var result = composedDate.getDate() == d &&
	           composedDate.getMonth() == m &&
	           composedDate.getFullYear() == y &&
	           (parseInt(y)>=2000);
	    if(result){
	    	if(d.length==1) d="0"+d;
	    	if(m<10) m="0"+(m+1);
	    	else m=(m+1);
	    	date.value = d+"/"+m+"/"+y;
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}

	function numbersonly(e) {
		var unicode = e.charCode ? e.charCode : e.keyCode
				if(unicode == 9)
					return true;
		if (unicode != 8) { //if the key isn't the backspace key (which we should allow)
			if (unicode<48||unicode>57) //if not a number
				return false //disable key press
		}
	}
</script>
</body>
</html>
