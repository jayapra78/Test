<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<sj:head compressed="false" />
<s:head theme="simple" />
<%
	/* Getting Context Path */
	String contextPath = request.getContextPath();
%>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Task Sheet-SRM Technologies</title>

<!--pop-up start Here Created by K.K.Gopinath -->
 <link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/login-page.css" />
 <link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
<!--pop-up End Here -->
<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
<style type="text/css">
	body{
		background-color:transparent;
		width:100%;
		margin:0px auto;
	}
	.page {
		margin: 10px;
		width: 600px;
	}
	.main{
		width:640px;
		margin:0px;
		text-align:left;
		float:left;
		background-image:none;
	}
	.login-field-part {
		width: 600px;
		float: left;
		padding: 10px;
		background-color: #fcfcfc;
		border: #C0C0C0 solid 5px;
		margin: 0px 0px 0px 0px;
	}
	#popover {
		height: 324px;
		margin: 0px;
		z-index: 9999;
		width: 600px;
	}
</style> 
</head>

<body>
	<div class="main">
	    <div class="page">
	         <div id="popover">
	           <div class="pop-main">
	                    <div class="login-field-part">
	                            <div class="plan-details-part">
	                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                            <tr>
	                            	<td class="plan-header">
	                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                  <tr>
	                                    <td class="plan-header-title">Select additional email if required</td>
	                                    <td>&nbsp;</td>
	                                    <td class="close-btn">
	                                    	<a href="#" onclick="parent.modalWindow.close();" class="close-btn" tabindex="9">Close</a>
	                                    </td>
	                                  </tr>
	                                </table>
	                                </td>
	                            </tr>
	                            <tr>
	                            	<td>
	                            	<div style="overflow:auto;top:0px;left:0px;width:600px;height:300px;">
		                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                                <c:forEach items="${ccList}" var="entry" varStatus="row">
		                                	<tr class="<c:if test="${row.index % 2 == 0}">odd</c:if><c:if test="${row.index % 2 != 0}">even</c:if>">
		                                		<td>
		                                			<input type="checkbox" id="checked_${row.index}" name="cc" value='${entry.commonName}' onchange="return selectRow(this)"/>
												</td>
												<td>${entry.name}</td>
												<td>${entry.commonName}</td>
		                                	</tr>
		                                </c:forEach>
		                                </table>
	                                </div>
	                                </td>
	                              </tr>
	                            </table>
	                            </div>
	                            <div class="plan-btns-part">
	                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                              <tr>
	                                <td style="width:39%;">* Maximum 3 email id's allowed</td>
	                                <td class="save-btn" style="width:10%;height:35px">
	                                	<input onclick="return funFormSubmit();" type="image" src="<%=contextPath%>/images/save-send-btn-new.png" style="height:35px" title="Plan" alt="Button"/>
	                                </td>
	                                <td style="width:3%;"></td>
	                                <td class="save-btn" style="width:10%;height:35px">
	                                	<input onclick="javascript:parent.modalWindow.close();" type="image" src="<%=contextPath%>/images/cancel-btn-new.png" style="height:35px" title="Cancel" alt="Cancel"/>
	                                </td>
	                                <td style="width:38%;"></td>
	                              </tr>
	                            </table>
	                            </div>
	                    </div>
	            </div>        
	    	</div>
		</div>
	</div>
	<script>
		var rows = ${fn:length(ccList)};
		function updatePopupHeight(){
			document.getElementById('popover').style.height= 324+"px";
		}
		function selectRow(obj){
			var selectedRows = 0;
			for(var i=0;i<rows;i++){
				if(document.getElementById("checked_"+i).checked){
					selectedRows = selectedRows+1;
				}
			}
			if(selectedRows>3){
				obj.checked=false;
				return false;
			}
			else{
				return true;
			}
		}
		window.onload=function(){
			var cc = parent.document.getElementById("cc");
			if(cc.value != ""){
				for(var i=0;i<rows;i++){
					var ccValue = cc.value.split(",");
					for(var j=0;j<ccValue.length;j++){
						if(document.getElementById("checked_"+i).value == ccValue[j]){
							document.getElementById("checked_"+i).checked = true;
						}
					}
				}
			}
		}
		function funFormSubmit(){
			var cc = parent.document.getElementById("cc");
	 		cc.value="";
	 		var selectedValue = "";
			for(var i=0;i<rows;i++){
				if(document.getElementById("checked_"+i).checked){
					selectedValue += "," + document.getElementById("checked_"+i).value;
				}
			}
	 		if(selectedValue!=""){
	 			cc.value=selectedValue.substring(1);
	 		}
	 		if(confirm("Are you sure your today task is completed to send mail?")){
	 			parent.submitPage();
			}
	 		parent.modalWindow.close();
	 		return false;
		}
			
		function targetopener(mylink, closeme, closeonly){
			if (! (window.focus && window.opener))return true;
			window.opener.focus();
			if (! closeonly)window.opener.location.href=mylink.href;
			if (closeme)window.close();
			return false;
		}
	</script> 
</body>
</html>
