<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
	response.setLocale(java.util.Locale.ENGLISH);
%>
<%
	/* Getting Context Path */
	String contextPath = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }
</script>
<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Report-SRM Technologies</title>
<sj:head compressed="false" />
<sx:head debug="false" />
<s:head theme="simple" />

<link href="<%=contextPath%>/css/jquery.alerts.css" rel="stylesheet" type="text/css"/>
<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/home.css" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/scrolling.css" /> 
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/shirai.css" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/demo_table.css" />
<script type='text/javascript' src="<%=contextPath%>/js/DataTables.js"></script>
<style type="text/css">
.dataTable{
	height: 10px;
}
.dataTable thead th {
    height: 20px;
}
table.display thead th {
    cursor: pointer;
    font-weight: bold;
    padding: 3px 18px 3px 10px;
    background-color: #C9D1D5;
    border: 1px solid #A1A2A2;
    }
.button_style {
	background-color: black;
	color: white;
	cursor: pointer;
}

.logout_style {
	background: transparent;
	color: white;
	cursor: pointer;
}

.msgcontainer {
	width: 120px;
}

.wrapword {
	white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */
	white-space: -pre-wrap; /* Opera 4-6 */
	white-space: -o-pre-wrap; /* Opera 7 */
	white-space: pre-wrap; /* css-3 */
	white-space: normal;
	word-wrap: break-word; /* Internet Explorer 5.5+ */
}

.footer-part{
	width : 100%;
	height:30px;
	background-color:#2d4a59;
	line-height:30px;
	font-family:Arial, verdena, sans-serif;
	font-size:11px;
	color:#fff;
	text-align:center;
	float:left;
}
</style>
<%-- <script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#resultTable').dataTable( {
		"sScrollY"			: 170,
		"sScrollX"			: "100%",
		"sPaginationType"	: "full_numbers",
		"bSort"				: false
	});
} );
</script> --%>

<script>
	
	var projectAry = new Array();
	<c:forEach items="${allProjectList}" var="entry" varStatus="row">
		projectAry["${entry.key}"] = new Array();
		<c:forEach items="${entry.value}" var="project">
			projectAry["${entry.key}"]["${project.key}"] = "${project.idValue}:${project.refId}";
		</c:forEach>
	</c:forEach>
	
	function openSearchProjectPopup(){
		var projectTypeId = document.getElementById("projectTypeId").value;
		if(projectTypeId=="0"){
			alert("Please select project type");
			return;
		}
		var selectedProjectCode = document.getElementById("projectCodeId").value;
		newwindow=window.open("mappedMultiProjectList?projectListId=projectId&projectCodeId=projectCodeId&projectType="+projectTypeId+"&selectedProjectCode="+selectedProjectCode+"&statusId=0",'newwindow','scrollbars=no,location=no,toolbar=no,resizable=no,menubar=0,height=430,width=517,left=200,top=200');
	}

	function resetSearchProject(){
		document.getElementById("projectCodeId").value="";
		document.getElementById("projectId").value="";
	}

	function validate(isOverall) {
		
		var projectTypeId = document.getElementById("projectTypeId").value;
		var projectCode   = document.getElementById("projectCodeId").value.replace(/ /g,'');
		var projectId     = document.getElementById("projectId").value;
		var overall       = document.getElementById("overall");
		overall.value     = isOverall;
		
		if(projectCode!=""){
			var projectCodes = projectCode.split(",");
			document.getElementById("projectId").value = "";
			for(var i=0;i<projectCodes.length;i++){
				if(!projectAry[projectTypeId] || !projectAry[projectTypeId][projectCodes[i]]){
					alert("Project code("+projectCodes[i]+") is invalid");
					document.getElementById("projectId").value ="";
					return false;
				}
				else{
					if(document.getElementById("projectId").value!=""){
						document.getElementById("projectId").value = document.getElementById("projectId").value+",";
					}
					document.getElementById("projectId").value = document.getElementById("projectId").value + projectAry[projectTypeId][projectCodes[i]].split(":")[0];
				}
			}
		}
			
		if(projectCode=="0"){
			document.getElementById("projectId").value ="";
		}
		return dateValidate();
	}

	function dateValidate(){
		var today = new Date();
		var startDate = document.getElementById("startDateStrId");
		var endDate = document.getElementById("endDateStrId");
		
		if(startDate.value!="" && endDate.value==""){
			alert("End Date is required.");
			return false;
		}
		if(endDate.value!="" && startDate.value==""){
			alert("Start Date is required.");
			return false;
		}
		if(startDate.value!="" && !isValidDate(startDate)){
			alert("Start Date is invalid. (Format - dd/mm/yyyy)");
			return false;
		}
		if(endDate.value!="" && !isValidDate(endDate)){
			alert("End Date is invalid. (Format - dd/mm/yyyy)");
			return false;
		}
		
		startDate = startDate.value;
		startDate = startDate.split("/");
		startDate = new Date(startDate[2]+"-"+startDate[1]+"-"+startDate[0]);
		
		endDate = endDate.value;
		endDate = endDate.split("/");
		endDate = new Date(endDate[2]+"-"+endDate[1]+"-"+endDate[0]);
		
		if (startDate > endDate){
			alert("Start date should be less than or equal to end date");
			return false;
		}
		return true;
	}
	
	function isValidDate(date){
		var matches = /^(\d{2}|\d{1})[\/\/](\d{2}|\d{1})[\/\/](\d{4})$/.exec(date.value);
	    if (matches == null) return false;
	    var d = matches[1];
	    var m = matches[2] - 1;
	    var y = matches[3];
	    var composedDate = new Date(y, m, d);
	    var result = composedDate.getDate() == d &&
	           composedDate.getMonth() == m &&
	           composedDate.getFullYear() == y &&
	           (parseInt(y)>=2000);
	    if(result){
	    	if(d.length==1) d="0"+d;
	    	if(m<10) m="0"+(m+1);
	    	else m=(m+1);
	    	date.value = d+"/"+m+"/"+y;
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}
	function toUpper(obj){
		obj.value=obj.value.toUpperCase();
	}
</script>


</head>



<body>
	<div class="main">
		<div class="page">
			<div class="top-header">
				<div class="title-heading">
					<div class="plan-lead-logo">
						<img src="<%=contextPath%>/images/plan-lead-logo.png" width="210"
							height="65" border="0" alt="Logo" title="Plan-Lead" />
					</div>
					<div class="tech-trends-logo">
						<img src="<%=contextPath%>/images/tech-trends-logo.png"
							width="195" height="65" border="0" alt="Logo" title="Tech-Trend" />
					</div>
					<div class="logout">
						<a href="logOut">Logout</a>
					</div>

				</div>
			</div>
			<div class="emp-details-part" style="height: 600px;">
				<s:form name="workDetailReport" id="workDetailReport" action="empWorkDetailReport" method="post" theme="simple">
					<s:hidden name="overall" id="overall"/>
					<div class="main_heading">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="33%">&nbsp;</td>
								<td width="34%">Employee Work Detail Report</td>
								<td width="33%"> <s:include value="/jsp/menu/MainMenu.jsp"></s:include></td>
							</tr>
						</table>
					</div>
					<table width="75%" border="0" cellspacing="0" cellpadding="0">
						<tr style="display: hidden;" id = "errorRow">
							<td width="20%" nowrap="nowrap" align="left" style="padding:0 0 0 250px;">
								<div id="errorMessageDiv">
									<s:if test="hasActionErrors()">
										<s:actionerror cssClass="error" />
									</s:if>
								</div>
							</td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="5" cellpadding="0" style="height: 300px;">

						<tr><td colspan="4">&nbsp;</td></tr>
						<tr><td colspan="4">&nbsp;</td></tr>
						<tr>
							<td class="report-labels" align="right">Employee
								Name&nbsp;:&nbsp;</td>
							<td><s:hidden name="employeeId" id="employeeId" /> <s:select
									list="employeeList" listKey="idValue" listValue="name"
									headerKey="0" headerValue="Select" tabindex="4"
									cssClass="list-box" name="empId" id="empId"
									style="width:145px; vertical-align: middle;" /></td>
							<td colspan="2">&nbsp;</td>
						</tr>
						
						<tr>
							<td width="25%" class="report-labels" align="right">Project
								Type&nbsp;:&nbsp;</td>
							<td width="25%" class="report-controls"><s:select
									name="projectTypeId" id="projectTypeId"
									list="projectTypeVoList" listKey="projectTypeId" tabindex="1"
									listValue="projectType" cssClass="list-box" style="width:110px"
									onchange="resetSearchProject();" /></td>
							<td width="25%" class="report-labels" align="right">Project
								Code&nbsp;:&nbsp;</td>
							<td width="25%" class="report-controls" align="left"
								nowrap="nowrap" valign="middle"><s:hidden name="projectId"
									id="projectId" /> <s:textfield name="projectCodeId"
									id="projectCodeId" style="width:70px;height:13px;"
									onchange="toUpper(this)" tabindex="2" /> <a id="projectPopup"
								href="javascript:openSearchProjectPopup()" tabindex="3"> <img
									src="<%=contextPath%>/images/select-icon.png" width="18"
									height="18" border="0"
									style="cursor: pointer; padding 0 0 0 0; vertical-align: middle;"
									title="Select Project" />
							</a></td>
						</tr>
						
						<tr>
							<td class="report-labels" align="right">Start Date
								&nbsp;:&nbsp;</td>
							<td class="report-controls" align="left" nowrap="nowrap"><sj:datepicker
									displayFormat="dd/mm/yy" tabindex="5" maxlength="10"
									id="startDateStrId" cssStyle="width:80px;"
									onChangeTopics="onChangeDate" buttonImageOnly="true"
									name="taskStartDate" readonly="false" onpaste="return false;" />

							</td>
							<td class="report-labels" align="right">End Date
								&nbsp;:&nbsp;</td>
							<td class="report-controls" align="left" nowrap="nowrap"><sj:datepicker
									displayFormat="dd/mm/yy" tabindex="6" maxlength="10"
									id="endDateStrId" cssStyle="width:80px;" cssClass="txt"
									buttonImageOnly="true" onChangeTopics="onChangeDate"
									name="taskEndDate" readonly="false" onpaste="return false;" />
							</td>
						</tr>

						<tr>
							<td class="report-labels" align="right">Report Format&nbsp;:&nbsp;</td>
							<td><s:radio name="reportFormat"
									list="#{'1':'PDF','2':'EXCEL'}" value="1"
									cssStyle="padding-right:25px;padding-left:10px;"></s:radio></td>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr><td colspan="4">&nbsp;</td></tr>
						<tr><td colspan="4">&nbsp;</td></tr>
						<tr>
							<td colspan="4" align="center"><input type="image" name="saveId" id="saveId" onclick="return validate(false)" tabindex="7"
									tabindex="8" title="Excel report" src="<%=contextPath%>/images/generate-report.png"/></td>
						</tr>

					</table>
					<table><tr><td style="line-height: 430px;">&nbsp;</td></tr></table>
					<div class ="footer-part">Copyright © SRM Technologies Private Limited </div>
				</s:form>
				<!-- <div style="background-color:#DDE4E8;"> -->
				
			</div>
		</div>
	</div>
	
</body>
</html>
