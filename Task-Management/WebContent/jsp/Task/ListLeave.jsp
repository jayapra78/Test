<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
	response.setLocale(java.util.Locale.ENGLISH);
%>
<%
	/* Getting Context Path */
	String contextPath = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<sj:head compressed="false" />
<sx:head debug="false" />
<s:head theme="simple" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
<link href="<%=contextPath%>/css/jquery.alerts.css" rel="stylesheet" type="text/css"/>
<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/home.css" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/shirai.css" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/scrolling.css" />


<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />

<title>Leave List-SRM Technologies</title>


<style type="text/css">
	.button_style {
		background-color: black;
		color: white;
		cursor: pointer;
	}
	.logout_style {
		background: transparent;
		color: white;
		cursor: pointer;
	}
</style>

<script type="text/javascript" src="<%=contextPath%>/js/Shirai.js"></script>
<script src="<%=contextPath%>/js/jquery.alerts.js"></script>

<style type="text/css">
/* Word Wrap */
.msgcontainer {
	width: 200px;
	height: auto;

}

.wrapword {
	white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */
	white-space: -pre-wrap; /* Opera 4-6 */
	white-space: -o-pre-wrap; /* Opera 7 */
	white-space: pre-wrap; /* css-3 */
	white-space: normal;
	word-wrap: break-word; /* Internet Explorer 5.5+ */

}
.content1{
    overflow: auto;
    overflow-x: hidden;
    height: 290px;
	font-family:Arial, Helvetica, sans-serif, Arial Unicode MS; font-size:11px;
}
</style>

</head>
<body>
	<div class="main">
		<div class="page">
			<div class="top-header">
				<div class="title-heading">
					<div class="plan-lead-logo">
						<img src="<%=contextPath%>/images/plan-lead-logo.png" width="210"
							height="65" border="0" alt="Logo" title="Plan-Lead" />
					</div>
					<div class="tech-trends-logo">
						<img src="<%=contextPath%>/images/tech-trends-logo.png"
							width="195" height="65" border="0" alt="Logo" title="Tech-Trend" />
					</div>
					<div class="logout">
						<a href="logOut">Logout</a>
					</div>

				</div>
			</div>
			<div class="emp-details-part">
			<s:form name="searchLeave" id="searchLeave" action="searchLeave"
					method="post" theme="simple">
			<div class="main_heading">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="33%">&nbsp;</td>
						<td width="34%">Leave List</td>
						<td width="33%"> <s:include value="/jsp/menu/MainMenu.jsp"></s:include></td>
					</tr>
					<tr><td>&nbsp;</td></tr>
				</table>
			</div>

					<s:hidden name="userSecureId" id="userSecureId"> </s:hidden>
        			<s:hidden name="deleteValue" id="deleteValue"></s:hidden>
        			<s:hidden name="token" id="token"/>
					<table width="70%" border="0" cellspacing="5" cellpadding="0"
						style="background-color: #DDE4E8;">

						<tr>
							<td width="20%" align="right"> From Date &nbsp;:&nbsp;</td>
							<td width="20%" align="left">
								<sj:datepicker displayFormat="dd/mm/yy" tabindex="3" id="startDateStrId"
								 cssStyle="width:78px;"  maxlength="10" buttonImageOnly="true"
								 name="leaveSearchVo.fromDateStr" readonly="false" onpaste="return false;"/>
							</td>
							<td width="20%" align="right"> To Date &nbsp;:&nbsp;</td>
							<td width="20%" align="left">
								<sj:datepicker displayFormat="dd/mm/yy" tabindex="4"
								id="endDateStrId" cssStyle="width:78px;"  buttonImageOnly="true"
								 maxlength="10" onpaste="return false;"
								name="leaveSearchVo.toDateStr" readonly="false" />
							</td>
							<td width="20%" align="center" class="search-btn">

							<a tabindex="5" href="#"
								onclick="javascript:funFormSubmit(document.forms[0],'searchLeave');"
								style="text-decoration: none;">
								<img src="<%=contextPath%>/images/search-btn-new.png" border="none"/>
							</a>

							</td>
						</tr>

	        		</table>
	        		<table width="100%">
						<tr>
							<td nowrap="nowrap" align="left">
								<div id="errorMessageDiv">
									<s:if test="hasActionErrors()">
										<s:actionerror cssClass="error" />
									</s:if>
								</div>
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap" align="center">
								<div id="successMessageDiv">
									<s:if test="hasActionMessages()">
										<s:actionmessage cssClass="success" />
									</s:if>
								</div>
							</td>
						</tr>
	        		</table>

	        		<table width="100%" border="0" cellpadding="0" cellspacing="0" >
						<tr>

							<td class='tablecontent' valign="top">
								<div id='headscroll' class='divhead'>
									<table border="0" cellpadding="0" cellspacing="0"
										class='head-normal'>
										<tr>
											<s:if test="responseVo.responseList.size > 0">
												<td width="5%" class='inner frozencol colwidth head' align="center"><s:checkbox name="checkAll"
														id="checkAll" tabindex="5"
														onclick="funCheckAll('checkAll','checkFlag','hiddenEmpLeaveDetailId')"></s:checkbox>
												</td>
											</s:if>
											<s:else>
												<td width="5%" class='inner frozencol colwidth head' align="center"><s:checkbox name="checkAll"
														id="checkAll" tabindex="5" disabled="true"></s:checkbox>
												</td>
											</s:else>

											<td width="5%" align="left" class='inner frozencol colwidth head'> <s:text name="Sl.No."/> </td>
											<td width="20%" align="left" class='inner col2 head'><s:text name="Employee Name"></s:text></td>
											<td width="10%" align="left" class='inner col3 head'><s:text name="From Date"></s:text></td>
											<td width="10%" align="left" class='inner col4 head'><s:text name="To Date"></s:text></td>
											<td width="15%" align="left" class='inner col5 head'><s:text name="Leave Status"></s:text></td>
											<td width="20%" align="left" class='inner col6 head'><s:text name="Reason for Leave"></s:text></td>
											<td width="15%" align="left" class='inner col6 head'><s:text name="Reject Reason"></s:text></td>

										</tr>
									</table>
								</div>


								<div id='contentscroll' class='content1'
									onscroll='reposHead(this);'>

									<table border="0" cellpadding="0" cellspacing="0"
										class='content-normal' id='innercontent'>

										<s:iterator value="responseVo.responseList"
											status="IteratorStatus">
											<tr>

												<td width="5%"
													class=<s:if test="#IteratorStatus.even">'inner frozencol2'</s:if>
													<s:else>'inner frozencol1'</s:else> width="15%"
													align="center"><s:checkbox name="checkFlag" tabindex="7"
														id="checkFlag%{#IteratorStatus.index+1}" onclick="removeSelectAll()"></s:checkbox>
													<s:hidden name="hiddenEmpLeaveDetailId"
														id="hiddenEmpLeaveDetailId%{#IteratorStatus.index+1}"
														value="%{empLeaveDetailId}">
													</s:hidden></td>

												<td width="5%" align="left"
													class=<s:if test="#IteratorStatus.even">'inner frozencol2'</s:if>
													<s:else>'inner frozencol1'</s:else>>&nbsp;&nbsp;&nbsp;<s:property
														value="(#IteratorStatus.index) + (responseVo.pagingVo.startRow+1 ) " />
												</td>

												<td width="20%"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else>><div align="left" class="msgcontainer wrapword">
													<s:property value="employeeName" />
														</div></td>

												<td width="10%" nowrap="nowrap"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else> width="55%" >
														<s:date name="fromDate" format="dd/MM/yyyy"/>
													</td>

												<td width="10%"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else>><div align="left" class="msgcontainer wrapword">
													<s:date name="toDate" format="dd/MM/yyyy"/>
													</div></td>

												<td width="15%"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else>><div align="left" class="msgcontainer wrapword"><s:property
														value="leaveStatusActionVo.leaveStatus" /></div></td>

												<td width="20%"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else>><div align="left" class="msgcontainer wrapword">
													<s:property value="reasonForLeave" /></div></td>

												<td width="15%"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else>><div align="left" class="msgcontainer wrapword">
													<s:property value="supervisorComments" />

													</div></td>

											</tr>
										</s:iterator>
									</table>
								</div>
							</td>

						</tr>
						<tr>
							<td colspan="3">
								<div class='horizontal-scroll-normal'
									onscroll='reposHorizontal(this);'>
									<div></div>
								</div>
							</td>
						</tr>
					</table>
					<table align="right" border="0" width="100%" cellspacing="1" cellpadding="0">
						<tr align="right">
							<td align="right">
								<jsp:include page="/jsp/common/Pagination.jsp" />
							</td>
						</tr>
					</table>
					<br/>
					<table width="100%" border="0" align="center">
						<tr>
						<td width="35%">&nbsp;</td>
							<td width="11%" align="center">
								<a tabindex="9" onclick="applyLeave()" href="#">
									<img src="<%=contextPath%>/images/apply.png" style="cursor: pointer;" border="none"/>
								</a>
							</td>

								<td width="11%" align="center" id="approveButtonId" style="display: none;">

									<a tabindex="10" href="#"
									onclick="funAddOrModWindow('approveLeave.action','approveLeave','700','810','mod','checkFlag','hiddenEmpLeaveDetailId','hiddenEmpLeaveDetailId','270',
											'<s:text name="ApproveOneLeave"/>','<s:text name="ApproveLeave"/>','<s:text name="ApproveTitle"/>','<s:text name="CloseButton"/>','<s:text name="Ok"/>','<s:text name="Cancel"/>')">
										<img src="<%=contextPath%>/images/approve.png" style="cursor: pointer;" border="none"/>
									</a>
								</td>

								<td width="34%">&nbsp;</td>

								<td width="40%">&nbsp;</td>

						</tr>
					</table>
				</s:form>
				<div class="footer-part">Copyright © SRM Technologies Private Limited </div>
			</div>
		</div>
	</div>

<script>
	var desgId = ${sessionScope.user.desgId};
	if(desgId == 25){
		document.getElementById("approveButtonId").style.display ='';
	}
	<c:forEach items="${projectManagerList}" var="entry" varStatus="row"><c:if test="${entry.idValue eq sessionScope.user.employeeId}">
		document.getElementById("approveButtonId").style.display ='';
	</c:if></c:forEach>
</script>

<script>
	var conPath = "<%=contextPath%>";
	function applyLeave(){
		document.searchLeave.action = conPath +"/app/applyLeave";
		document.searchLeave.submit();
	}

	function dateValidation(obj){
		if(obj.value!=""){
			if(isValidDate(obj)){
				dateOverlappingValidation({"lastVal":""},obj);
				return true;
			}
			else{
				alert("Date is invalid. (Format - dd/mm/yyyy)");
				return false;
			}
		}
	}
	function isValidDate(date){
		var matches = /^(\d{2}|\d{1})[\/\/](\d{2}|\d{1})[\/\/](\d{4})$/.exec(date.value);
	    if (matches == null) return false;
	    var d = matches[1];
	    var m = matches[2] - 1;
	    var y = matches[3];
	    var composedDate = new Date(y, m, d);
	    var result = composedDate.getDate() == d &&
	           composedDate.getMonth() == m &&
	           composedDate.getFullYear() == y &&
	           (parseInt(y)>=2000);
	    if(result){
	    	if(d.length==1) d="0"+d;
	    	if(m<9) m="0"+(m+1);
	    	else m=(m+1);
	    	date.value = d+"/"+m+"/"+y;
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}



	function funAddOrModWindow(url,windowName,height,width,type,comboName,hiddenName,hiddenParam,marginHeight,modifyOneRecord,modifyRecord,Title,closeButton, Okbutton,CancelButton){

			 var source = url+"?userSecureId="+document.getElementById('userSecureId').value;
			 funWindowOpenUrl(source,windowName,height,width,type,comboName,hiddenName,hiddenParam,marginHeight,modifyOneRecord,modifyRecord,Title,closeButton, Okbutton,CancelButton);

		}

	var isMultiSubmit = false;
	function funFormSubmit(obj,action)
	   {
	   	   if(isMultiSubmit==false){
	   		 isMultiSubmit = true;
	   		 funSubmitForm(obj,action);
	   		}

	   }
	   function addValidation(){
		var errMsg = "";
		var isInvalid = false;

		try{
			var startDate = document.getElementById("startDateStrId");
			var endDate = document.getElementById("endDateStrId");
			var rejoinDate = document.getElementById("rejoinDateId");

			var kindOfLeave = document.getElementById("leaveTypeId");
			var noOfDay = document.getElementById("noOfDaysId");
			var reasonOfLeave = document.getElementById("reasonId");
			var addressDuringLeave = document.getElementById("addressId");
			var contactNumber = document.getElementById("contactNumberId");
			var contactEmail = document.getElementById("contactEmailId");
			var reportingManager = document.getElementById("reportingManagerId");
			var leaveApproval = document.getElementById("leaveStatusId");

			if(leaveApproval.value == "0" && leaveApproval.value == "1"){
				errMsg += parseErrorMsg("Leave Approval is required");
				isInvalid = true;
			}


			if(startDate.value!="" && !isValidDate(startDate)){
				alert("From Date is invalid. (Format - dd/mm/yyyy)");
				return false;
			}

			if(endDate.value!="" && !isValidDate(endDate)){
				alert("To Date is invalid. (Format - dd/mm/yyyy)");
				return false;
			}
			startDate = startDate.value;
			startDate = startDate.split("/");
			var startDate1 = new Date();
			startDate1.setFullYear(parseInt(startDate[2]));
			startDate1.setMonth(parseInt(startDate[1]));
			startDate1.setDate(parseInt(startDate[0]));

			endDate = endDate.value;
			endDate = endDate.split("/");
			var endDate1 = new Date();
			endDate1.setFullYear(parseInt(endDate[2]));
			endDate1.setMonth(parseInt(endDate[1]));
			endDate1.setDate(parseInt(endDate[0]));

			if (startDate1 > endDate1) {
				alert("To Date should be greater or equal to the From Date");
				return false;
			}

			rejoinDate = rejoinDate.value;
			rejoinDate = rejoinDate.split("/");
			var rejoinDate1 = new Date();
			rejoinDate1.setFullYear(parseInt(rejoinDate[2]));
			rejoinDate1.setMonth(parseInt(rejoinDate[1]));
			rejoinDate1.setDate(parseInt(rejoinDate[0]));
			if (rejoinDate1 < endDate1) {
				alert("Date of rejoining should be greater than To Date");
				return false;
			}
			if(isInvalid){
			showErrorMsg(errMsg);
			return false;
		}
			updateLeave.submit();
		}
		catch(e){
			alert(e);
		}
		return true;
	}


var funWindowOpenUrl = function(actionName, windowName, height, width, type,
		comboName, hiddenName, hiddenParam, marginHeight, modifyOneRecord,
		modifyRecord, title, closeButton, OkButton, CancelButton) {

	var jsCheckBoxValue = "";
	var source = "";
	if (type == 'mod') {

		for ( var a = 1;; a++) {
			var jsCombo = document.getElementById(comboName + a);
			var jsHiddenValue = document.getElementById(hiddenName + a);
			if (jsCombo != 'undefined' && jsCombo != null) {
				if (jsCombo.checked) {
					if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
						jsCheckBoxValue = jsHiddenValue.value + ","
								+ jsCheckBoxValue;
					}
				}

			} else {
				break;
			}

		}

		if (jsCheckBoxValue.length > 1) {

			jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
					jsCheckBoxValue.length - 1);
			var jsSplitValue = jsCheckBoxValue.split(",");

			if (jsSplitValue.length > 1) {
				jAlert(modifyOneRecord, title, '', OkButton);

				return false;
			} else {
				actionName = actionName + "&" + hiddenParam + "="
						+ jsSplitValue;
				source = actionName;
			}
		} else {
			jAlert(modifyRecord, title, '', closeButton);

			return false;
		}

	} else {
		source = actionName;

	}
	window.location.href = actionName;
};
</script>

</body>
</html>
