<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- <%@ taglib prefix="sj" uri="/struts-jquery-tags"%> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
	response.setLocale(java.util.Locale.ENGLISH);
%>
<%
	/* Getting Context Path */
	String contextPath = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<%-- <sj:head compressed="false" /> --%>
<sx:head debug="false" />
<s:head theme="simple" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
<link href="<%=contextPath%>/css/jquery.alerts.css" rel="stylesheet" type="text/css"/>
<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/home.css" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/shirai.css" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/scrolling.css" />


<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />

<title>Project Owner Mapping-SRM Technologies</title>


<style type="text/css">
	.button_style {
		background-color: black;
		color: white;
		cursor: pointer;
	}
	.logout_style {
		background: transparent;
		color: white;
		cursor: pointer;
	}
</style>

<script src="<%=contextPath%>/js/jquery.js"></script>
<script src="<%=contextPath%>/js/jquery.alerts.js"></script>
<script src="<%=contextPath%>/js/jquery.ui.draggable.js"></script>
<script type="text/javascript" src="<%=contextPath%>/js/Shirai.js"></script>
<script language='javascript' type='text/javascript'
	src="<%=contextPath%>/js/jquery.min.js"></script>

<style type="text/css">
/* Word Wrap */
.msgcontainer {
	width: 200px;
	height: auto;

}

.wrapword {
	white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */
	white-space: -pre-wrap; /* Opera 4-6 */
	white-space: -o-pre-wrap; /* Opera 7 */
	white-space: pre-wrap; /* css-3 */
	white-space: normal;
	word-wrap: break-word; /* Internet Explorer 5.5+ */

}
.content1{
    overflow: auto;
    overflow-x: hidden;
    height: 290px;
	font-family:Arial, Helvetica, sans-serif, Arial Unicode MS; font-size:11px;
}
</style>

</head>
<body>
	<div class="main">
		<div class="page">
			<div class="top-header">
				<div class="title-heading">
					<div class="plan-lead-logo">
						<img src="<%=contextPath%>/images/plan-lead-logo.png" width="210"
							height="65" border="0" alt="Logo" title="Plan-Lead" />
					</div>
					<div class="tech-trends-logo">
						<img src="<%=contextPath%>/images/tech-trends-logo.png"
							width="195" height="65" border="0" alt="Logo" title="Tech-Trend" />
					</div>
					<div class="logout">
						<a href="logOut">Logout</a>
					</div>

				</div>
			</div>
			<div class="emp-details-part">
			<s:form name="searchTask" id="searchTask" action="searchProjectOwnerMapping"
					method="post" theme="simple">
			<div class="main_heading">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="33%">&nbsp;</td>
						<td width="34%">Project List</td>
						<td width="33%"> <s:include value="/jsp/menu/MainMenu.jsp"></s:include></td>
					</tr>
				</table>
			</div>

					<s:hidden name="userSecureId" id="userSecureId"> </s:hidden>
        			<s:hidden name="deleteValue" id="deleteValue"></s:hidden>
        			<s:hidden name="token" id="token"/>
					<table width="100%" border="0" cellspacing="5" cellpadding="0"
						style="background-color: #DDE4E8;">
						<tr>
							<td width="20%" align="right"> Project Type &nbsp;:&nbsp;</td>
							<td width="20%" align="left">
								<s:select name="projectOwnerSearchVo.projectTypeId" list="projectTypeVoList" listKey="projectTypeId" listValue="projectType"
                  					 cssClass="list-box" style="width:147px" onchange="resetSearchProject();" id="projectTypeId" tabindex="1"/>
							</td>
							<td width="20%" align="right"> Project Code &nbsp;:&nbsp;</td>
							<td width="20%" align="left">
								<s:hidden name="projectOwnerSearchVo.projectCodeId" id="projectId"/>
                  					<s:textfield name="projectOwnerSearchVo.projectCode" id="projectCodeId" style="width:120px; vertical-align: middle;" tabindex="2" maxlength="50"/>

		                    	<a id="projectPopup" href="javascript:openSearchProjectPopup()" tabindex="2.1">
	                        	<img src="<%=contextPath%>/images/select-icon.png" width="20" height="20" border="0" title="Select Project" style="cursor: pointer;vertical-align: middle; "/>
	                        </a>
							</td>
							<td width="20%">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" align="right"> PM Name &nbsp;:&nbsp;</td>
							<td width="20%" align="left">
								<s:select headerKey="-1" headerValue="select" name="projectOwnerSearchVo.pmId" list="pmNameList" listKey="idValue" listValue="name"
                  					 cssClass="list-box" style="width:147px" tabindex="3"/>
							</td>
							<td width="20%" align="right"> PL Name &nbsp;:&nbsp;</td>
							<td width="20%" align="left">
								<s:select headerKey="-1" headerValue="select" name="projectOwnerSearchVo.plId" list="plNameList" listKey="idValue" listValue="name"
                  					 cssClass="list-box" style="width:147px" tabindex="4"/>
							</td>
							<td width="20%" align="center" class="search-btn">

							<a tabindex="5" href="#"
								onclick="javascript:funFormSubmit(document.forms[0],'searchProjectOwnerMapping');"
								style="text-decoration: none;">
								<img src="<%=contextPath%>/images/search-btn-new.png" border="none"/>
							</a>

							</td>
						</tr>

	        		</table>
	        		<table width="100%">
						<tr>
							<td nowrap="nowrap" align="left">
								<div id="errorMessageDiv">
									<s:if test="hasActionErrors()">
										<s:actionerror cssClass="error" />
									</s:if>
								</div>
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap" align="center">
								<div id="successMessageDiv">
									<s:if test="hasActionMessages()">
										<s:actionmessage cssClass="success" />
									</s:if>
								</div>
							</td>
						</tr>
	        		</table>

	        		<table width="100%" border="0" cellpadding="0" cellspacing="0" >
						<tr>

							<td class='tablecontent' valign="top">
								<div id='headscroll' class='divhead'>
									<table border="0" cellpadding="0" cellspacing="0"
										class='head-normal'>
										<tr>
											<s:if test="responseVo.responseList.size > 0">
												<td width="5%" class='inner frozencol colwidth head' align="center"><s:checkbox name="checkAll"
														id="checkAll" tabindex="5"
														onclick="funCheckAll('checkAll','checkFlag','hiddenProjectOwnerMappingId')"></s:checkbox>
												</td>
											</s:if>
											<s:else>
												<td width="5%" class='inner frozencol colwidth head' align="center"><s:checkbox name="checkAll"
														id="checkAll" tabindex="5" disabled="true"></s:checkbox>
												</td>
											</s:else>

											<td width="5%" align="left" class='inner frozencol colwidth head'> <s:text name="Sl.No."/> </td>
											<td width="10%" align="left" class='inner col2 head'><s:text name="Project Type"></s:text></td>
											<td width="26%" align="left" class='inner col3 head'><s:text name="Project Name"></s:text></td>
											<td width="12%" align="left" class='inner col4 head'><s:text name="Project Code"></s:text></td>
											<td width="15%" align="left" class='inner col5 head'><s:text name="Project Manager"></s:text></td>
											<td width="15%" align="left" class='inner col6 head'><s:text name="Project Leader"></s:text></td>
											<td width="12%" align="left" class='inner col3 head'><s:text name="Project Status"></s:text></td>

										</tr>
									</table>
								</div>


								<div id='contentscroll' class='content1'
									onscroll='reposHead(this);'>

									<table border="0" cellpadding="0" cellspacing="0"
										class='content-normal' id='innercontent'>

										<s:iterator value="responseVo.responseList"
											status="IteratorStatus">
											<tr>

												<td width="5%"
													class=<s:if test="#IteratorStatus.even">'inner frozencol2'</s:if>
													<s:else>'inner frozencol1'</s:else> width="15%"
													align="center"><s:checkbox name="checkFlag" tabindex="7"
														id="checkFlag%{#IteratorStatus.index+1}" onclick="removeSelectAll()"></s:checkbox>
													<s:hidden name="hiddenProjectOwnerMappingId"
														id="hiddenProjectOwnerMappingId%{#IteratorStatus.index+1}"
														value="%{projectOwnerMappingId}">
													</s:hidden></td>

												<td width="5%" align="left"
													class=<s:if test="#IteratorStatus.even">'inner frozencol2'</s:if>
													<s:else>'inner frozencol1'</s:else>>&nbsp;&nbsp;&nbsp;<s:property
														value="(#IteratorStatus.index) + (responseVo.pagingVo.startRow+1 ) " />
												</td>

												<td width="10%"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else>><div align="left" class="msgcontainer wrapword">
													<s:property value="projectType" />
														</div></td>

												<td width="26%" nowrap="nowrap"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else> width="55%" ><s:url var="viewProjectOwnerMapping"
														action="viewProjectOwnerMapping">
														<s:param name="ProjectOwnerMappingId"
															value="%{ProjectOwnerMappingId}"></s:param>
													</s:url> <div align="left" class="msgcontainer wrapword">

													<s:a href="%{viewProjectOwnerMapping}" tabindex="8">
														<s:property value="projectName" />
													</s:a></div></td>

												<td width="12%"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else>><div align="left" class="msgcontainer wrapword"><s:property
														value="projectCode" /></div></td>

												<td width="15%"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else>><div align="left" class="msgcontainer wrapword"><s:property
														value="pmName" /></div></td>

												<td width="15%"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else>><div align="left" class="msgcontainer wrapword"><s:property
														value="plName" /></div></td>

												<td width="12%"
													class=<s:if test="#IteratorStatus.even">'inner1'</s:if>
													<s:else>'inner'</s:else>><div align="left" class="msgcontainer wrapword"><s:property
														value="projectStatusActionVo.statusName" /></div></td>
											</tr>
										</s:iterator>
									</table>
								</div>
							</td>

						</tr>
						<tr>
							<td colspan="3">
								<div class='horizontal-scroll-normal'
									onscroll='reposHorizontal(this);'>
									<div></div>
								</div>
							</td>
						</tr>
					</table>
					<table align="right" border="0" width="100%" cellspacing="1" cellpadding="0">
						<tr align="right">
							<td align="right">
								<jsp:include page="/jsp/common/Pagination.jsp" />
							</td>
						</tr>
					</table>
					<br/>
					<table width="100%" border="0" align="center">
						<tr>
						<td width="35%">&nbsp;</td>
							<td width="11%" align="center">
								<a tabindex="9" onclick="addFunction()" href="#">
									<img src="<%=contextPath%>/images/add.png" style="cursor: pointer;" border="none"/>
								</a>
							</td>

							<s:if test="responseVo.responseList.size > 0">
								<td width="11%" align="center">

									<a tabindex="10" href="#"
									onclick="funAddOrModWindow('modifyProjectOwnerMapping.action','modifyProjectOwnerMapping','700','810','mod','checkFlag','hiddenProjectOwnerMappingId','hiddenProjectOwnerMappingId','270',
											'<s:text name="ProjectOwnerModifyOneRecord"/>','<s:text name="ProjectOwnerModifyRecord"/>','<s:text name="ModifyTitle"/>','<s:text name="CloseButton"/>','<s:text name="Ok"/>','<s:text name="Cancel"/>')">
										<img src="<%=contextPath%>/images/modify.png" style="cursor: pointer;" border="none"/>
									</a>
								</td>

								<td width="11%" align="center">

									<a tabindex="11" onclick="funSubmitDelete('checkFlag','hiddenProjectOwnerMappingId', 'deleteValue',document.forms[0],'deleteProjectOwnerMapping',
									'<s:text name="ProjectOwnerConfirmDelete"/>','<s:text name="ProjectOwnerDeleteRecord"/>','<s:text name="ConfirmationDeleteTitle"/>','<s:text name="DeleteTitle"/>','<s:text name="CloseButton"/>','<s:text name="Ok"/>','<s:text name="Cancel"/>');" href="#">
											<img src="<%=contextPath%>/images/delete.png" style="cursor: pointer;" border="none"/>
									</a>
								</td>
								<td width="34%">&nbsp;</td>
							</s:if>
							<s:else>
								<td width="40%">&nbsp;</td>
							</s:else>

						</tr>
					</table>
				</s:form>
				<div class="footer-part">Copyright © SRM Technologies Private Limited </div>
			</div>
		</div>
	</div>

<script>
	var conPath = "<%=contextPath%>";
	function addFunction(){
		document.searchTask.action = conPath + "/app/addProjectOwnerMapping";
		document.searchTask.submit();
	}
	function openSearchProjectPopup(){
		var projectTypeId = document.getElementById("projectTypeId").value;
		if(projectTypeId=="0"){
			alert("Please select project type");
			return;
		}
		var selectedProjectCode = document.getElementById("projectCodeId").value;
		newwindow=window.open("projectList?projectListId=projectCodeId&projectCodeId=projectId&projectType="+projectTypeId+"&selectedProjectCode="+selectedProjectCode,'newwindow','scrollbars=yes,menubar=0,height=410,width=510,left=200,top=200');
	}

	function resetSearchProject(){
		document.getElementById("projectCodeId").value="";
		document.getElementById("projectId").value="";
	}

	function funAddOrModWindow(url,windowName,height,width,type,comboName,hiddenName,hiddenParam,marginHeight,modifyOneRecord,modifyRecord,Title,closeButton, Okbutton,CancelButton){
			 var source = url+"?userSecureId="+document.getElementById('userSecureId').value;
			 funWindowOpenUrl(source,windowName,height,width,type,comboName,hiddenName,hiddenParam,marginHeight,modifyOneRecord,modifyRecord,Title,closeButton, Okbutton,CancelButton);

		}

	var isMultiSubmit = false;
	function funFormSubmit(obj,action)
	   {
	   	   if(isMultiSubmit==false){
	   		 isMultiSubmit = true;
	   		 funSubmitForm(obj,action);
	   		}
	   }

var funWindowOpenUrl = function(actionName, windowName, height, width, type,
		comboName, hiddenName, hiddenParam, marginHeight, modifyOneRecord,
		modifyRecord, title, closeButton, OkButton, CancelButton) {

	var jsCheckBoxValue = "";
	var source = "";
	if (type == 'mod') {

		for ( var a = 1;; a++) {
			var jsCombo = document.getElementById(comboName + a);
			var jsHiddenValue = document.getElementById(hiddenName + a);
			if (jsCombo != 'undefined' && jsCombo != null) {
				if (jsCombo.checked) {
					if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
						jsCheckBoxValue = jsHiddenValue.value + ","
								+ jsCheckBoxValue;
					}
				}

			} else {
				break;
			}

		}

		if (jsCheckBoxValue.length > 1) {

			jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
					jsCheckBoxValue.length - 1);
			var jsSplitValue = jsCheckBoxValue.split(",");

			if (jsSplitValue.length > 1) {
				jAlert(modifyOneRecord, title, '', OkButton);

				return false;
			} else {
				actionName = actionName + "&" + hiddenParam + "="
						+ jsSplitValue;
				source = actionName;
			}
		} else {
			jAlert(modifyRecord, title, '', closeButton);

			return false;
		}

	} else {
		source = actionName;

	}
	window.location.href = actionName;
};

<c:if test="${sessionScope.user.guest eq true}">
function funSubmitDelete(comboName, hiddenName, hiddenDeleteTagName, obj,
		action, confirmDelete, deleteRecord, confirmtitle, title, closeButton, okButton,
		cancelButton) {

	var jsCheckBoxValue = "";
	for ( var a = 1;; a++) {
		var jsCombo = document.getElementById(comboName + a);
		var jsHiddenValue = document.getElementById(hiddenName + a);
		if (jsCombo != 'undefined' && jsCombo != null) {
			if (jsCombo.checked) {
				if (jsHiddenValue != 'undefined' && jsHiddenValue != null) {
					jsCheckBoxValue = jsHiddenValue.value + ","
							+ jsCheckBoxValue;
				}
			}

		} else {
			break;
		}
	}

	if (jsCheckBoxValue.length > 1) {

		jsCheckBoxValue = jsCheckBoxValue.substring(jsCheckBoxValue,
				jsCheckBoxValue.length - 1);
		var jsSplitValue = jsCheckBoxValue.split(",");
		document.getElementById(hiddenDeleteTagName).value = jsSplitValue;
		jConfirm(confirmDelete, confirmtitle, okButton, cancelButton, function(
				r) {

			if (r) {
				return false;
			} else {
				return false;
			}

		});

	} else {
		jAlert(deleteRecord, title, '', closeButton);
		return false;
	}

}
</c:if>

</script>

</body>
</html>
