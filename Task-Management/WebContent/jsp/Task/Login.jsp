﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--  CLEARING THE CACHE -->
<%
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader("Expires", 0);
        response.setLocale(java.util.Locale.ENGLISH);
%>
<%
	String contextPath = request.getContextPath();
%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Task Sheet-SRM Technologies</title>
	<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
	<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
	<script type="text/javascript">
		//window.onkeypress = handleKeyPress;
		function handleKeyPress(e) {
			var eventInstance = window.event ? event : e;
			var keyCode = eventInstance.charCode ? eventInstance.charCode
					: eventInstance.keyCode;
			if (keyCode == 13) {
				funSubmitForm(document.forms[0], 'loginUser');
				return false;
			}
		}
	
	    window.history.forward();
	    function noBack() { window.history.forward(); }
	</SCRIPT>
	
</head>
<body onload="onloading(),noBack()" onpageshow="if (event.persisted) noBack();" onunload="">
<div class="main">
    <div class="page">
        <div class="top-header">
        	<div class="title-heading">
            	<div class="plan-lead-logo"><img src="<%=contextPath%>/images/plan-lead-logo.png" width="210" height="65" border="0" alt="Logo" title="Plan-Lead" />
                </div>
                <div class="tech-trends-logo"><img src="<%=contextPath%>/images/tech-trends-logo.png" width="195" height="65" border="0" alt="Logo" title="Tech-Trend" />
                </div>
            </div>
        </div>
	   <%--
        <marquee scrolldelay="70" truespeed scrollamount="3">
        	<font style="font-weight: bold;color: blue;">
        		MESSAGE FROM SBU HEAD (JANAKIRAMAN K): DEAR ALL, I REQUEST EVERYBODY TO ATTEND THE TECH DAY FUNCTION ON (23-03-2013) WITHOUT FAIL. THANKS FOR YOUR SUPPORT.
        	</font>
        </marquee>
         --%>
        <div class="login-whole-part">
           	<div class="login-part">
           		<s:form name="loginForm" action="loginUser" validate="true"
					method="POST" theme="simple" focusElement="loginNameId">
					<s:hidden id="invalidate" name="invalidate" value="%{#session['invalidate']}"></s:hidden>
	                <table width="480px" border="0" cellspacing="0" cellpadding="0">
	                  <tr>
	                    <td>
	                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                      <tr>
	                        <td class="login-title">Login</td>
	                      </tr>
	                      <tr>
	                        <td>
	                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                        	<tr>
									<s:if test="hasActionErrors()">
									<td colspan="2">&nbsp;</td>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
											<tr>
												<br>
												<td nowrap="nowrap" align="left">
													   <div>
													      <s:actionerror cssClass="error"/>
													   </div>
												</td>
											</tr>
										</table>
									</td>
									</s:if>
									<s:else>
									<td class="spacer"></td>
									</s:else>
								</tr>
	                        	<tr>
	                            <td class="4" height="40px">&nbsp;</td>
	                            </tr>
	                              <tr>
	                                <td width="20%">&nbsp;</td>
	                                <td class="login-text">User Name :</td>
	                                <td class="login-field"><s:textfield id="loginNameId" 
															name="loginValue.loginName"
															cssStyle="width:120px; height:14px;ime-mode:disabled"
															tabindex="1" maxLength="30"/></td>
	                                <td width="20%">&nbsp;</td>
	                              </tr>
	                        	<tr>
	                            <td class="4" height="20px">&nbsp;</td>
	                            </tr>
	                              <tr>
	                                <td width="20%">&nbsp;</td>
	                                <td class="login-text">Password :</td>
	                                <td class="login-field"><s:password id="passwordId" 
															name="loginValue.password" showPassword="true"
															cssStyle="width:120px; height:14px;ime-mode:disabled" onkeypress="return handleKeyPress(event);"
															tabindex="2" maxLength="10" oncopy="return false;" onpaste="return false;" oncut="return false;"/></td>
	                                <td width="20%">&nbsp;</td>
	                              </tr>
	                        	<tr>
	                            <td class="4" height="20px">&nbsp;</td>
	                            </tr>
	                        	<tr>
	                            <td colspan="4">
	                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                    <tr>                            
	                                    <td width="30%">&nbsp;</td>
	                                    <td>
	                                    <input type="image" tabindex="3" src="<%=contextPath%>/images/login-btn-new.png"/>
										</td>
										<td>
										
										<input type="image" tabindex="4" src="<%=contextPath%>/images/cancel-btn-new.png" onclick="return resetForm();" onkeypress="return resetForm1(event);"/>
										
	                                    </td>
	                                    <td width="30%">&nbsp;</td>
	                                    </tr>
	                                </table>
	                                <s:hidden id="errorMsg" name="errorMsg" value="%{errorMsg}"></s:hidden>
	                            </td>
	                            </tr>
	                        </table>
	                        </td>
	                      </tr>
	                    </table>
	                    </td>
	                  </tr>
	                </table>
	                <script type="text/javascript">
	              	//window.onkeypress = handleKeyPress;
	            	function handleKeyPress(e) {
	            		var eventInstance = window.event ? event : e;
	            		var keyCode = eventInstance.charCode ? eventInstance.charCode
	            				: eventInstance.keyCode;
	            		if (keyCode == 13) {
	            			loginForm.submit();
	            			return false;
	            		}
	            	}
	            	function resetForm1(e){
	            		var eventInstance = window.event ? event : e;
	            		var keyCode = eventInstance.charCode ? eventInstance.charCode
	            				: eventInstance.keyCode;
	            		if (keyCode == 13) {
	            			return resetForm();
	            		}
	            		return false;
	            	}
	                function resetForm(){
	                	document.getElementById("loginNameId").value="";
	                	document.getElementById("passwordId").value="";
	                	document.getElementById("loginNameId").focus();
	                	return false;
	                }
	                
					function changeButtonFont(controlId){
						document.getElementById(controlId).className="btn1-bg-focus";
					}

					function resetClass(controlId){
						document.getElementById(controlId).className="btn1-bg";
						
					}
						if (document.getElementById("invalidate") != 'undefined') {
							var userId = document.getElementById("invalidate").value;
							if (userId != null && userId != '') {
								parent.funRefreshParent();
								parent.modalWindow.close();
							}
						}
						function removeErrorField(objValue){
							var newDiv = document.getElementById(objValue);
							newDiv.innerHTML = "";
						}
						
						function onloading(){
							document.getElementById('passwordId').value = "";
							document.getElementById('loginNameId').focus();							
						}	
					</script>
				</s:form>
            </div>
            <div class="footer-part">Copyright © SRM Technologies Private Limited
            </div>
        </div>
    </div>
</div>
</body>
</html>