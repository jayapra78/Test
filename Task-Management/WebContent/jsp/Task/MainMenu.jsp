
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
	/* Getting Context Path */
	String contextPath = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Plan 2 Lead</title>

<style type="text/css">
/* body {
	font: normal .8em/1.5em Arial, Helvetica, sans-serif;
	background: #ebebeb;
	width: 900px;
	margin: 100px auto;
	color: #666;
} */
#nav {
	margin: 0;
	line-height: 100%;
	float:left;
	/*border:#000 solid 1px;*/
}
#nav li {
	margin: 0 5px;
	padding: 0px 0 8px;
	float: left;
	position: relative;
	list-style: none;
}
/* main level link */
#nav a {
	font-weight: bold;
	color: #e7e5e5;
	text-decoration: none;
	display: block;
	padding:  8px 20px;
	margin: 0;
}
/* main level link hover */
#nav .current a, #nav li:hover > a {
	color: #444;
}
/* sub levels link hover */
#nav ul li:hover a, #nav li:hover li a {
	background: none;
	border: none;
	color: #000;
	font-weight:bold;
}
#nav ul a:hover {
	background: #0b3e59 !important; /* for non-css3 browsers */
	color: #fff !important;
}
/* level 2 list */
#nav ul {
	background: #ddd; /* for non-css3 browsers */
	display: none;
	margin: 5px 0px 0px 0px;
	padding: 0;
	width: 215px;
	position: absolute;
	top: 35px;
	left: 0;
	border: solid 1px #b4b4b4;
}
/* dropdown */
#nav li:hover > ul {
	display: block;
}
#nav ul li {
	float: none;
	margin: 0;
	padding: 0;
}
#nav ul a {
	font-weight: normal;
	color:#fff;
}
/* level 3+ list */
#nav ul ul {
	left: -200px;
	top: 0px;
	float:left;
}
/* clearfix */
/* .main{
margin:0px auto;
width:1000px;
}
.page{
margin:0px auto;
width:1000px;
} */
.menu-part{
float:right;
width:230px;
height:30px;
padding:0px 0px 0px 0px;
font-size: 13px;
font-weight: bold;
font-family: Arial, verdena, sans-serif;
}

</style>
</head>

<body>
<div class="menu-part" id="mainMenuId" >
    <ul id="nav">
        <!-- <li class="current"><a href="#">Home</a></li> -->
         <li>
               <img src="<%=contextPath%>/images/menu-btn.png"  width="149" height="35" border="0" style="vertical-align: bottom; cursor: pointer;"/>
               <ul>
                   <li id="masterMenuId"  style="display: none; text-align: left;"><a href="#">Master Screen</a>
                       <ul>
                           <li style="text-align: left;"><s:a href="listProjectOwnerMapping" >Project&nbsp;owner&nbsp;mapping</s:a></li>
                       </ul>
                   </li>
                   <li id="reportMenuId"  style="display: none; text-align: left;">
                   	<s:a href="openReport">Reports</s:a>
                   </li>
                   <li id="empEffortReportMenuId"  style="display: none; text-align: left;">
                   	<s:a href="openEmpEffortReport">Employee&nbsp;Effort&nbsp;MIS&nbsp;Reports</s:a>
                   </li>
                   <li id="empWorkDetailReportMenuId"  style="display: none; text-align: left;display: none;">
                   	<s:a href="openEmpWorkDetailReport">Employee&nbsp;Work&nbsp;Detail&nbsp;Reports</s:a>
                   </li>
                   <li id="taskSheetMenuId"  style=" text-align: left;">
                   	<s:a href="openTask">Task Sheet</s:a>
                   </li>
                   <li id="bulkTaskMenuId" style="display: none; text-align: left;">
                   	<s:a href="openActualRecursiveTask">Bulk Task</s:a>
                   </li>
                   <li id="resourceAllocationId"  style="display: none;text-align: left;">
                   	<s:a href="openResourceAllocation">Resource Allocation</s:a>
                   </li>
                   <li id="leaveFormMenuId"  style="text-align: left;display: none;">
                   	<s:a href="listLeave">Leave Form</s:a>
                   </li>
                </ul>
          </li>
     </ul>      
</div>
<script>
	var desgId = ${sessionScope.user.desgId};
	if(desgId == 25){
		document.getElementById("masterMenuId").style.display ='';
		document.getElementById("reportMenuId").style.display ='';
		document.getElementById("empEffortReportMenuId").style.display ='';
		//document.getElementById("empWorkDetailReportMenuId").style.display ='';
		document.getElementById("bulkTaskMenuId").style.display ='';
		//document.getElementById("taskSheetMenuId").style.display ='';
	}
//	var reportDesgIdList = new Array(<fmt:bundle basename="task"><fmt:message key="privilege.report.designation.id"/></fmt:bundle>);
	<c:forEach items="${projectManagerList}" var="entry" varStatus="row"><c:if test="${entry.idValue eq sessionScope.user.employeeId}">
		document.getElementById("reportMenuId").style.display ='';
		document.getElementById("empEffortReportMenuId").style.display ='';
		//document.getElementById("empWorkDetailReportMenuId").style.display ='';
		//document.getElementById("resourceAllocationId").style.display ='';
		document.getElementById("bulkTaskMenuId").style.display ='';
		//document.getElementById("taskSheetMenuId").style.display ='';
	</c:if></c:forEach>
	<c:if test="${sessionScope.user.guest eq true}">
		document.getElementById("masterMenuId").style.display ='';
		document.getElementById("reportMenuId").style.display ='';
		document.getElementById("empEffortReportMenuId").style.display ='';
		//document.getElementById("empWorkDetailReportMenuId").style.display ='';
	</c:if>
</script>
</body>

</html>
