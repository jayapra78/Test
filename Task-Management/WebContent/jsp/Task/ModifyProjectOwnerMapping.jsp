<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
			response.setLocale(java.util.Locale.ENGLISH);
%>
<%
    /* Getting Context Path */
			String contextPath = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }
</script>
<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Project Owner Mapping-SRM Technologies</title>
	<sj:head compressed="false" />
	<sx:head debug="false" />
	<s:head theme="simple" />
	<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/shirai.css" />
	<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
	<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />

<style type="text/css">
	.button_style {
		background-color: black;
		color: white;
		cursor: pointer;
	}

	.logout_style {
		background: transparent;
		color: white;
		cursor: pointer;
	}
	<style type="text/css">
	.button_style {
		background-color: black;
		color: white;
		cursor: pointer;
	}

	.logout_style {
		background: transparent;
		color: white;
		cursor: pointer;
	}
	.footer-part{
		position:absolute;
		top:600px;
		margin-left: auto;
        margin-right: auto;
		width:1000px;
		height:30px;
		background-color:#2d4a59;
		line-height:30px;
		font-family:Arial, verdena, sans-serif;
		font-size:11px;
		color:#fff;
		text-align:center;
		float:left;
	}

	fieldset { border:1px solid #66CCFF }

	legend {
	  padding: 0.2em 0.5em;
	  text-align:left;
	  font-weight: bold;
	  font-family: sans-serif;
	  font-size: 14px;
	  color: #0000FF;
	  }
</style>
</style>

<script type="text/javascript"
	src="<%=contextPath%>/js/iepngfix_tilebg.js"></script>

<script type="text/javascript">
	var projectAry = new Array();
    <s:iterator value="projectList" status="IteratorStatus">
    	projectAry["${key}"] = "${idValue}:${refId}:${name}";
    </s:iterator>

    var mappedProjectAry = new Array();
    <s:iterator value="mappedProject" status="IteratorStatus">
    	if(!mappedProjectAry["${refId}"]){
    		mappedProjectAry["${refId}"] = new Array();
    	}
    	mappedProjectAry["${refId}"]["${name}"] = "${commonName}";
    </s:iterator>

    var currentDate = "${currentDate}";
</script>

</head>

<body style="background-color:#DDE4E8;">
	<div class="main">
		<div class="page">
			<div class="top-header">
				<div class="title-heading">
					<div class="plan-lead-logo">
						<img src="<%=contextPath%>/images/plan-lead-logo.png" width="210"
							height="65" border="0" alt="Logo" title="Plan-Lead" />
					</div>
					<div class="tech-trends-logo">
						<img src="<%=contextPath%>/images/tech-trends-logo.png"
							width="195" height="65" border="0" alt="Logo" title="Tech-Trend" />
					</div>
					<div class="logout">
						<a href="logOut">Logout</a>
					</div>

				</div>
			</div>
			<div class="emp-details-part">
			<s:form name="updateProjectOwnerMapping" id="updateProjectOwnerMapping"
					action="updateProjectOwnerMapping" validate="true" method="POST" theme="simple">
				<s:hidden name="token" id="token"/>
				<div class="main_heading">
					<table cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td width="33%">&nbsp;</td>
							<td width="34%">Modify Project</td>
							<td width="33%"> <s:include value="/jsp/menu/MainMenu.jsp"></s:include></td>
						</tr>
					</table>
				</div>
				<table width="75%" border="0" cellspacing="0" cellpadding="0">
							<tr style="display: hidden;" id = "errorRow">
								<td width="20%" nowrap="nowrap" align="left" style="padding:0 0 0 250px;">
									<div id="errorMessageDiv">
										<s:if test="hasActionErrors()">
											<s:actionerror cssClass="error" />
										</s:if>
									</div>
								</td>
							</tr>
				</table>

				<s:hidden name="userSecureId" id="userSecureId"> </s:hidden>
				<s:hidden name="projectOwnerMappingActionVo.projectOwnerMappingId" value="%{projectOwnerMappingActionVo.projectOwnerMappingId}" > </s:hidden>
					<table width="100%" border="0" cellspacing="5" cellpadding="0" style="background-color:#DDE4E8;">

						<tr><td height="20%">&nbsp;</td></tr>

						<tr id="ProjectType">
							<td width="50%" class="project-mapping-label" align="right">
									Project Type  *&nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:select name="projectOwnerMappingActionVo.projectTypeId" list="projectTypeVoList" listKey="projectTypeId" listValue="projectType" id="projectTypeId"
	                    			disabled="true" cssClass="list-box" style="width:212px" tabindex="1"/>
							</td>
						</tr>
						<s:hidden name="projectOwnerMappingActionVo.projectId" id="projectId"/>
						<tr id="ProjectCode"
						<s:if test="%{projectOwnerMappingActionVo.projectTypeId != 1}">style="display:none"</s:if>
						>
							<td width="50%" class="project-mapping-label" align="right">
								Project Code  *&nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
                  				<s:textfield name="projectOwnerMappingActionVo.projectCode" value="%{projectOwnerMappingActionVo.projectCode}" id="projectCodeId" size="25" tabindex="2" maxlength="50" onchange="toUpper(this);setProjectName();" style="width:208px;vertical-align: middle;"/>
		                    	<a id="projectPopup" href="javascript:openSearchProjectPopup()" tabindex="2.1">
	                        	<img src="<%=contextPath%>/images/select-icon.png" width="20" height="20" border="0" style="cursor: pointer; vertical-align: middle;" title="Select Project"/>
	                        </a>
							</td>
						</tr>

						<tr id="ProjectName">
							<td width="50%" class="project-mapping-label" align="right">
									Project Name  *&nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:textfield name="projectOwnerMappingActionVo.projectName" id="projectName" tabindex="3" maxlength="50"  value="%{projectOwnerMappingActionVo.projectName}" size="30"  style="width:208px"/>
							</td>
						</tr>

						<tr id="ProjectStatus">
							<td width="50%" class="project-mapping-label" align="right">
									Project Status  *&nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
							    <s:hidden name="prStatusId" id="prStatusId" />
								<s:select list="projectStatusList" listKey="idValue" listValue="name" headerKey="0" headerValue="Select"
									cssClass="list-box" name="projectOwnerMappingActionVo.projectStatusActionVo.statusId" id="projectStatusId"
									style="width:212px; vertical-align: middle;"
									tabindex="4"/>
							</td>
						</tr>

						<tr id="ProjectManagerName">
							<td width="50%" class="project-mapping-label" align="right">
									Project Manager  *&nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:select headerKey="" headerValue="select" name="projectOwnerMappingActionVo.pmId"  list="employeeList" listKey="key" listValue="name"
	                    			onchange="changeEmail(this,'pmEmailId');" cssClass="list-box" style="width:212px" tabindex="4"/>
							</td>
						</tr>

						<tr id="ProjectManagerEmail">
							<td width="50%" class="project-mapping-label" align="right">
									Project Manager Email  *&nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:textfield name="projectOwnerMappingActionVo.pmEmailId" value="%{projectOwnerMappingActionVo.pmEmailId}" tabindex="5" id="pmEmailId" size="30" maxlength="60"  style="width:208px"/>
							</td>
						</tr>

						<tr id="ProjectLeaderName">
							<td width="50%" class="project-mapping-label" align="right">
									Project Leader  &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:select headerKey="" headerValue="select" name="projectOwnerMappingActionVo.plId" list="employeeList" listKey="key" listValue="name"
	                    			onchange="changeEmail(this,'plEmailId');" cssClass="list-box" style="width:212px" tabindex="6"/>
							</td>
						</tr>

						<tr id="ProjectLeaderEmail">
							<td width="50%" class="project-mapping-label" align="right">
									Project Leader Email  &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:textfield name="projectOwnerMappingActionVo.plEmailId" value="%{projectOwnerMappingActionVo.plEmailId}" tabindex="7" maxlength="60" id="plEmailId" size="30"  style="width:208px"/>
							</td>
						</tr>
						<s:if test="%{projectOwnerMappingActionVo.projectTypeId != 1}">
							<tr>
								<td width="50%" class="project-mapping-label" align="right">
									Associated Project &nbsp;:&nbsp;
								</td>
								<td class="project-mapping-controls">
									<s:select name="projectOwnerMappingActionVo.associatedProjectId" id="associatedProjectId" list="associatedProjectList"
									listKey="idValue" listValue="name" headerKey="0" headerValue="Select" style="width:212px" tabindex="8"/>
								</td>
							</tr>

							<tr>
								<td width="50%" class="project-mapping-label" align="right">
									Associated From  &nbsp;:&nbsp;
								</td>
								<td>
									<sj:datepicker displayFormat="dd/mm/yy" tabindex="8" maxlength="10"
										id="associatedFromStr" cssStyle="width:120px;" onChangeTopics="onChangeDate"
										buttonImageOnly="true" name="projectOwnerMappingActionVo.associatedFromStr" readonly="false"
										onpaste="return false;" />
								</td>
							</tr>
						</s:if>

						<%-- <tr>
							<td colspan="2" style="width: 600px;" align="center">
								<fieldset style="width: 510px;">
									<legend>Customer Detail:</legend>
									<table width="100%" align="center" border="0" cellpadding=0 cellspacing=0>
										<tr>
											<td colspan="2" style="width: 50%; text-align: right;"> Customer Name&nbsp;:&nbsp;</td>
											<td colspan="2" style="width: 50%;"> <s:textfield  tabindex="9" id="customerName" name="projectOwnerMappingActionVo.customerName" size="30" maxlength="200"  style="width:300px" placeholder="If more than one please enter coma seperated"/></td>
										</tr>
										<tr><td colspan="4">&nbsp;</td></tr>
										<tr>
											<td colspan="2" style="width: 50%; text-align: right;"> Customer Email&nbsp;:&nbsp;</td>
											<td colspan="2" style="width: 50%;"> <s:textfield  tabindex="10" id="customerEmail" name="projectOwnerMappingActionVo.customerEmailId" size="30" maxlength="200"  style="width:300px" placeholder="If more than one please enter coma seperated"/></td>
										</tr>
										<tr><td colspan="4">&nbsp;</td></tr>
										<tr>
											<td style="width: 42%; text-align: right;">Status Mail&nbsp;:&nbsp;</td>
											<td style="width: 8%; text-align: center;"><s:checkbox tabindex="11" id="statusMail" name="projectOwnerMappingActionVo.statusMail"/> </td>
											<td style="width: 25%; text-align: right;">Consolidated Mail&nbsp;:&nbsp;</td>
											<td style="width: 25%;">&nbsp;&nbsp;<s:checkbox tabindex="12" id="consolidatedMail" name="projectOwnerMappingActionVo.consolidatedMail" /> </td>
										</tr>
									</table>
								</fieldset>
							</td>
						</tr> --%>

						<tr><td height="20%">&nbsp;</td></tr>
					</table>

					<table style="background-color:#DDE4E8;" width="100%" border="0" cellspacing="0" cellpadding="0">
	          			<tr>
				            <td style="width:40%;"></td>
				            <td class="save-btn">
				            	<a href="#" tabindex="8" title="Update" name="saveId" id="saveId" onclick="return validation()">
			                      	<img src="<%=contextPath%>/images/update.png" align="middle" border="none" alt="Update"/>
			                    </a>
							</td>

							<td class="save-btn">
								<a href="#" tabindex="9" title="Cancel" onclick="closeWindow()">
			                      	<img src="<%=contextPath%>/images/cancel-btn-new.png" align="middle" border="none" alt="Cancel"/>
			                    </a>
				            </td>
				            <td style="width:38%;"></td>
	          			</tr>
	        		</table>
	        		<div class="footer-part">Copyright © SRM Technologies Private Limited </div>
				</s:form>
			</div>
		</div>
	</div>
<script>
	var conPath = "<%=contextPath%>";
	function closeWindow(){
		document.updateProjectOwnerMapping.action = conPath + "/app/listProjectOwnerMapping";
		document.updateProjectOwnerMapping.submit();
	}
	function openSearchProjectPopup(){
		var projectTypeId = document.getElementById("projectTypeId").value;
		if(projectTypeId=="0"){
			alert("Please select project type");
			return;
		}
		var selectedProjectCode = document.getElementById("projectCodeId").value;
		newwindow=window.open("projectList?projectListId=projectCodeId&projectCodeId=projectId&projectType="+projectTypeId+"&selectedProjectCode="+selectedProjectCode,'newwindow','scrollbars=yes,menubar=0,height=410,width=510,left=200,top=200');
	}
	var alreadySubmitted = false;
	function validation(){
		if(alreadySubmitted){
			return false;
		}
		try{
			var errMsg = "";
			var isInvalid = false;

			var projectTypeObj   = document.getElementById("projectTypeId");
			var projectCodeObj   = document.getElementById("projectCodeId");
			var projectNameObj   = document.getElementById("projectName");
			var projectStatusObj = document.getElementById("projectStatusId");
			var pmIdObj          = document.getElementById("updateProjectOwnerMapping_projectOwnerMappingActionVo_pmId");
			var pmEmailIdObj     = document.getElementById("pmEmailId");
			var plIdObj          = document.getElementById("updateProjectOwnerMapping_projectOwnerMappingActionVo_plId");
			var plEmailIdObj     = document.getElementById("plEmailId");
			var associatedFromObj= document.getElementById("associatedFromStr");
			var associatedProjectObj= document.getElementById("associatedProjectId");

			if(projectTypeObj.value=="0"){
				errMsg += parseErrorMsg("Project Type is required");
				isInvalid = true;
			}
			if(projectCodeObj.value.replace(/ /g,'')=="" && (projectTypeObj.value=="1" || projectTypeObj.value=="0")){
				errMsg += parseErrorMsg("Project Code is required");
				isInvalid = true;
			}
			var projectTypeIdObj = document.getElementById("projectTypeId");
			var projectCode      = document.getElementById("projectCodeId").value.replace(/ /g,'');
			var projectId        = document.getElementById("projectId");
			if(projectTypeIdObj.value == "1" && projectAry[projectCode]){
				projectId.value = projectAry[projectCode].split(":")[0];
			}
			if(projectTypeIdObj.value == "1" && !projectAry[projectCode] && projectCode!=""){
				errMsg += parseErrorMsg("Project Code is invalid");
				isInvalid = true;
			}
			else if(projectTypeIdObj.value == "1" && mappedProjectAry["1"][projectCode] && projectCode!="" && projectCode!="${projectOwnerMappingActionVo.projectCode}"){
				errMsg += parseErrorMsg("Project is already exist");
				isInvalid = true;
			}
			else if(projectTypeIdObj.value != "1" && projectTypeIdObj.value!="0"){
				if(projectNameObj.value!=""){
					for(key1 in mappedProjectAry){
						if(key1!="1"){
							for(key2 in mappedProjectAry[key1]){
								if(mappedProjectAry[key1][key2].toUpperCase()==projectNameObj.value.toUpperCase() && projectNameObj.value!="${projectOwnerMappingActionVo.projectName}"){
									errMsg += parseErrorMsg("Project is already exist");
									isInvalid = true;
									break;
									break;
								}
							}
						}
					}
				}
			}
			if(projectNameObj.value.replace(/ /g,'')==""){
				errMsg += parseErrorMsg("Project Name is required");
				isInvalid = true;
			}
			if(projectStatusObj.value=="0"){
				errMsg += parseErrorMsg("Project Status is required");
				isInvalid = true;
			}
			if(pmIdObj.value.replace(/ /g,'')==""){
				errMsg += parseErrorMsg("Project Manager is required");
				isInvalid = true;
			}
			if(pmEmailIdObj.value.replace(/ /g,'')==""){
				errMsg += parseErrorMsg("Project Manager Email is required");
				isInvalid = true;
			}
			if(pmEmailIdObj.value.replace(/ /g,'')!="" && !isValidEmail(pmEmailIdObj.value)){
				errMsg += parseErrorMsg("Invalid Project Manager Email Id");
				isInvalid = true;
			}
			if(plEmailIdObj.value.replace(/ /g,'')!="" && !isValidEmail(plEmailIdObj.value)){
				errMsg += parseErrorMsg("Invalid Project Leader Email Id");
				isInvalid = true;
			}
			if(associatedProjectObj && associatedProjectObj.value!="0" &&  projectTypeObj.value=="2" && associatedFromObj.value==""){
				errMsg += parseErrorMsg("Associated From is required");
				isInvalid = true;
			}
			else if(projectTypeObj.value=="2" && associatedFromObj.value!="" && !isValidDate(associatedFromObj)){
				errMsg += parseErrorMsg("Associated From is invalid. (Format - dd/mm/yyyy)");
				isInvalid = true;
			}
			else if(projectTypeObj.value=="2" && associatedFromObj.value!="" && !dateCompare({"value":currentDate},associatedFromObj)){
				errMsg += parseErrorMsg("Associated From date should should be greater than or equal to current date");
				isInvalid = true;
			}
			if(isInvalid){
				showErrorMsg(errMsg);
				return false;
			}
		}
		catch(e){alert(e); return false;}
		document.getElementById("projectTypeId").disabled = false;
		alreadySubmitted = true;

		<c:if test="${sessionScope.user.guest eq false}">
			document.getElementById("updateProjectOwnerMapping").submit();
		</c:if>

		<c:if test="${sessionScope.user.guest eq true}">
			alreadySubmitted = false;
		</c:if>
		hideErrorMsg();
		return true;
	}
	function setProjectName(){
		var projectTypeIdObj = document.getElementById("projectTypeId");
		var projectCode      = document.getElementById("projectCodeId").value;
		var projectId        = document.getElementById("projectId");
		var projectName      = document.getElementById("projectName");
		var projectAryValues = projectAry[projectCode].split(":");
		if(projectTypeIdObj.value == "1" && projectAry[projectCode]){
			projectId.value   = projectAryValues[0];
			if(projectAryValues.length==4){
				projectName.value = projectAryValues[3].trim();
			}
		}
		else if(projectTypeIdObj.value == "1" && !projectAry[projectCode]){
			projectId.value ="";
			projectName.value = "";
		}
	}
	function resetSearchProject(){
		document.getElementById("projectCodeId").value="";
		document.getElementById("projectName").value="";
	}

	function removeErrorField(objValue){
		var newDiv = document.getElementById(objValue);
		newDiv.innerHTML = "";
	}
	function toUpper(obj){
		obj.value=obj.value.toUpperCase();
	}
	function parseErrorMsg(msg){
		return "<li><span>" + msg + "</span></li>";
	}
	function hideErrorMsg(){
		document.getElementById("errorMessageDiv").style.display = "none";
        document.getElementById("successMessageDiv").style.display = "none";
	}
	function showErrorMsg(err){
		var msg = "<ul id=\"addTask_\" class=\"error\">";
        msg += err;
        msg += "</ul>";
        document.getElementById("errorMessageDiv").style.display = "";
        document.getElementById("errorMessageDiv").innerHTML = msg;
	}
	function changeEmail(obj,mailId){
		var emailId = obj.value.split("-")[2];
		if(emailId){
			document.getElementById(mailId).value=emailId;
		}
		else{
			document.getElementById(mailId).value="";
		}
	}
	function isValidEmail(email) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        return reg.test(email);
	}
	window.onload=function(){
		var pmObj = document.getElementById("updateProjectOwnerMapping_projectOwnerMappingActionVo_pmId");
		var plObj = document.getElementById("updateProjectOwnerMapping_projectOwnerMappingActionVo_plId");
		setLeaderId(pmObj,"${projectOwnerMappingActionVo.pmId}");
		setLeaderId(plObj,"${projectOwnerMappingActionVo.plId}");
	}
	function setLeaderId(obj,value){
		value = value.substring(0,value.indexOf("-")+1)
		for(var i=0;i<obj.options.length;i++){
			if(obj.options[i].value.indexOf(value)==0){
				obj.options[i].selected = true;
				return;
			}
		}
	}
	function isValidDate(date){
		var matches = /^(\d{2}|\d{1})[\/\/](\d{2}|\d{1})[\/\/](\d{4})$/.exec(date.value);
	    if (matches == null) return false;
	    var d = matches[1];
	    var m = matches[2] - 1;
	    var y = matches[3];
	    var composedDate = new Date(y, m, d);
	    var result = composedDate.getDate() == d &&
	           composedDate.getMonth() == m &&
	           composedDate.getFullYear() == y &&
	           (parseInt(y)>=2000);
	    if(result){
	    	if(d.length==1) d="0"+d;
	    	if(m<10) m="0"+(m+1);
	    	else m=(m+1);
	    	date.value = d+"/"+m+"/"+y;
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}
	function dateCompare(startDate, endDate){
		startDate = startDate.value;
		startDate = startDate.split("/");
		var startDate1 = new Date();
		startDate1.setFullYear(parseInt(startDate[2]));
		startDate1.setMonth(parseInt(startDate[1]));
		startDate1.setDate(parseInt(startDate[0]));

		endDate = endDate.value;
		endDate = endDate.split("/");
		var endDate1 = new Date();
		endDate1.setFullYear(parseInt(endDate[2]));
		endDate1.setMonth(parseInt(endDate[1]));
		endDate1.setDate(parseInt(endDate[0]));

		if (startDate1 > endDate1) {
			return false;
		}
		else{
			return true;
		}
	}
</script>
</body>
</html>
