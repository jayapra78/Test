<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Project Selection Screen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT language = "javascript">
	function setProjectCode(lstProject){
		
		var intIndex = lstProject.selectedIndex ;
		if (intIndex < 0 ){
			alert("Select any Project") ;
			lstProject.focus();
			return false;
		}
		
		var doc = window.opener.document;
		var row = "${projectCodeId}".substring(12);
		
		var objPrjCode = doc.getElementById("${projectCodeId}");
		var objPrjId = doc.getElementById("${projectListId}");
		var objManager = doc.getElementById("addTask_task_"+row+"__reportempId");
		var projectNameObj = doc.getElementById("projectName");
		
		if(objPrjCode)	   objPrjCode.value="";
		if(objPrjId)	   objPrjId.value="";
		if(objManager)	   objManager.value="";
		if(projectNameObj) projectNameObj.value="";
		
		var options = lstProject.options;
		for(var index=0;index<options.length;index++){
			if(options[index].selected=="selected" || options[index].selected==true){
				var strVal = options[index].value ;
				var strName = lstProject.options[index].innerHTML;
				var arrVal = strVal.split(",");
				if(objPrjId.value != ""){
					objPrjId.value += ",";
					objPrjCode.value += ",";
				}
				objPrjId.value += arrVal[0];
				objPrjCode.value += arrVal[1];
				
				if(projectNameObj){
					var sName = strName.split(":");
					if(sName.length>0){
						projectNameObj.value += sName[1].trim();
					}
				}
				try{
					if(arrVal[2]!=""){
						objManager.value = arrVal[2];
						if(objManager.value==""){
							objManager.options[0].selected=true;
						}
					}
				}
				catch(e){}
			}
		}
		window.close();
	}
	function isEnterKey(event){
		var keyCode = event.charCode ? event.charCode : event.keyCode;
		if (keyCode == 13) {
			return true;
		}
		return false;
	}
</SCRIPT>
<body class=lbody scroll="no" style="overflow-x:hidden;overflow-y:hidden;" onKeyPress="setEvent(this,'1')">
<FORM name = "frmProject">
	<TABLE border = 0 width = 540 cellspacing="0" cellspading="0">
		<TR>
			<TD>
				<table border = 0 width = 540 cellspacing="0" cellspading="0">
					<tr>
						<c:forEach items="${deptProjectList}" var="entry" varStatus="row">
							<td>
							    <input type="radio" name="department" id="department_${row.index}" onclick="showProjects(${row.index},${fn:length(deptProjectList)})"> ${entry.key}
							</td>
						</c:forEach>
					</tr>
				</table>
			</TD>
		</TR>
		<TR>
			<TD>
				<c:set var="defaultRow" value="0"/>
				<c:set var="selectedRow" value=""/>
				<c:forEach items="${deptProjectList}" var="entry" varStatus="row">
					<SELECT name="lstProject_${row.index}" id="lstProject_${row.index}" size=21 width=520 
					 style="HEIGHT: 400px; WIDTH: 500px;display:none;" onkeypress="return (isEnterKey(event) && setProjectCode(this)) || true;" 
					 ondblClick="setProjectCode(this);" class=selactctrls multiple="multiple"
					 >
						<c:forEach items="${entry.value}" var="project">
    						<option value='${project.idValue},${project.key},${project.refId}'
    						<c:if test="${param.selectedProjectCode eq project.key || fn:indexOf(param.selectedProjectCode, project.key) ne '-1'}"> 
					 			<c:set var="selectedRow" value="${row.index}"/>
					 			selected="selected"
					 		</c:if>
    						>${project.name}</option>
    						<c:if test="${sessionScope.user.deptId eq project.idLongValue}"> 
					 			<c:set var="defaultRow" value="${row.index}"/>
					 		</c:if>
    					</c:forEach>
					</SELECT>
				</c:forEach>
				<c:if test="${selectedRow ne ''}"> 
		 			<c:set var="defaultRow" value="${selectedRow}"/>
		 		</c:if>
			</TD>
		</TR>
	</TABLE>
</FORM>
<script>
window.onload=function(){
	document.getElementById("lstProject_${defaultRow}").style.display="block";
	document.getElementById("department_${defaultRow}").checked="true";
	document.getElementById("lstProject_${defaultRow}").focus();
	if("${projectType}" != "" && "${projectType}" != "0"){
		var doc = window.opener.document;
		var objPrjCode = doc.getElementById("${projectCodeId}");
		var objPrjId = doc.getElementById("${projectListId}");
		if(doc.projectAry["${projectType}"] && doc.projectAry["${projectType}"][objPrjCode.value]){
			var value = doc.projectAry["${projectType}"][objPrjCode.value];
		}
	}
}
function showProjects(currentRow,length){
	for(var r=0;r<length;r++){
		document.getElementById("lstProject_"+r).style.display="none";
	}
	document.getElementById("lstProject_"+currentRow).style.display="block";
}
</script>
</body>
</html>  