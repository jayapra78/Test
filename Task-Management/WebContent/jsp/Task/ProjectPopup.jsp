<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Project Selection Screen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT language = "javascript">
	function setProjectCode(intSelIndex){
		var strVal ;
		var intIndex = document.frmProject.lstProject.selectedIndex ;
	
		if (intIndex < 0 ){
			alert("Select any Project") ;
			document.frmProject.lstProject.focus();
			return false;
		}
		
		strVal = document.frmProject.lstProject.options[intIndex].value ;
		var strName = document.frmProject.lstProject.options[intIndex].innerHTML;
	
		var doc = window.opener.document;
		
		var row = "${projectListId}".substring(12);
		
		var objPrjCode = doc.getElementById("${projectListId}");
		var objPrjId = doc.getElementById("${projectCodeId}");
		var objManager = doc.getElementById("addTask_task_"+row+"__reportempId");
		
		var arrVal = strVal.split(",");
		var projectNameObj = doc.getElementById("projectName");
		objPrjId.value = arrVal[0];
		objPrjCode.value = arrVal[1];
		
		if(projectNameObj){
			var sName = strName.split(":");
			if(sName.length>0){
				projectNameObj.value = sName[1].trim();
			}
		}
		
		try{
			if(arrVal[2]!=""){
				objManager.value = arrVal[2];
				objManager.disabled=true;
			}
			else{
				objManager.disabled=false;
			}
		}
		catch(e){}
		window.close();
	}
	function isEnterKey(event){
		var keyCode = event.charCode ? event.charCode : event.keyCode;
		if (keyCode == 13) {
			return true;
		}
		return false;
	}
</SCRIPT>
<body class=lbody scroll="no" style="overflow-x:hidden;overflow-y:hidden;" onKeyPress="setEvent(this,'1')" onLoad="JavaScript:frmProject.lstProject.focus();">
<FORM name = "frmProject">
	<TABLE border = 0 width = 540 cellspacing="0" cellspading="0">
		<TR>
			<TD>
				<SELECT name="lstProject" size=21 width=520 style="HEIGHT: 400px; WIDTH: 500px;" onkeypress="return (isEnterKey(event) && setProjectCode(1)) || true;" ondblClick="setProjectCode(1);" class=selactctrls>
					
					<s:iterator value="projectList" status="IteratorStatus">
    					<option value='${idValue},${key},${refId}'
    					<c:if test="${param.selectedProjectCode eq key}"> 
				 			selected="selected"
				 		</c:if> 
    					>${name}</option>
    				</s:iterator>
				
				</SELECT>
			</TD>
		</TR>
		
	</TABLE>
</FORM>
</body>
</html>