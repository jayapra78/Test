<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
			response.setLocale(java.util.Locale.ENGLISH);
%>
<%
    /* Getting Context Path */
			String contextPath = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }
</script>
<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Report-SRM Technologies</title>
<sj:head compressed="false" />
<sx:head debug="false" />
<s:head theme="simple" />

<link href="<%=contextPath%>/css/jquery.alerts.css" rel="stylesheet" type="text/css"/>
<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/home.css" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/scrolling.css" /> 
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/shirai.css" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/demo_table.css" />


<script type='text/javascript' src="<%=contextPath%>/js/DataTables.js"></script>
<style type="text/css">
.dataTable{
	height: 10px;
}
.dataTable thead th {
    height: 20px;
}
table.display thead th {
    cursor: pointer;
    font-weight: bold;
    padding: 3px 18px 3px 10px;
    background-color: #C9D1D5;
    border: 1px solid #A1A2A2;
    }
.button_style {
	background-color: black;
	color: white;
	cursor: pointer;
}

.logout_style {
	background: transparent;
	color: white;
	cursor: pointer;
}

.msgcontainer {
	width: 120px;
}

.wrapword {
	white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */
	white-space: -pre-wrap; /* Opera 4-6 */
	white-space: -o-pre-wrap; /* Opera 7 */
	white-space: pre-wrap; /* css-3 */
	white-space: normal;
	word-wrap: break-word; /* Internet Explorer 5.5+ */
}

.footer-part{
	width : 100%;
	height:30px;
	background-color:#2d4a59;
	line-height:30px;
	font-family:Arial, verdena, sans-serif;
	font-size:11px;
	color:#fff;
	text-align:center;
	float:left;
}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#resultTable').dataTable( {
		"sScrollY"			: 170,
		"sScrollX"			: "100%",
		"sPaginationType"	: "full_numbers",
		"bSort"				: false
	});
} );
</script>

<script>
	
	var projectAry = new Array();
	<c:forEach items="${allProjectList}" var="entry" varStatus="row">
		projectAry["${entry.key}"] = new Array();
		<c:forEach items="${entry.value}" var="project">
			projectAry["${entry.key}"]["${project.key}"] = "${project.idValue}:${project.refId}";
		</c:forEach>
	</c:forEach>
	
	function openSearchProjectPopup(){
		var projectTypeId = document.getElementById("projectTypeId").value;
		if(projectTypeId=="0"){
			alert("Please select project type");
			return;
		}
		var selectedProjectCode = document.getElementById("projectCodeId").value;
		newwindow=window.open("mappedProjectList?projectListId=projectId&projectCodeId=projectCodeId&projectType="+projectTypeId+"&selectedProjectCode="+selectedProjectCode+"&statusId=0",'newwindow','scrollbars=no,location=no,toolbar=no,resizable=no,menubar=0,height=430,width=517,left=200,top=200');
	}

	function resetSearchProject(){
		document.getElementById("projectCodeId").value="";
		document.getElementById("projectId").value="";
	}

	function getReportType() {
		var selectedReport = document.getElementById('reportTypeId').value;
		document.getElementById("errorRow").style.display = 'none';
		if (selectedReport == "0") {
			hideProjectType();
			hideprojectManager();
			hideEmp();
			document.getElementById("dateTable").style.display = 'none';
		}
		else if (selectedReport == "1") {
			document.getElementById("Project").style.display = '';
			hideprojectManager();
			hideEmp();
		}
		else if (selectedReport == "2") {
			document.getElementById("ProjectManager").style.display = '';
			hideProjectType();
			hideEmp();
		}
		else if (selectedReport == "3") {
			document.getElementById("Employee").style.display = '';
			hideProjectType();
			hideprojectManager();
		}
	}
	
	function hideProjectType() {
		document.getElementById("dateTable").style.display = '';
		document.getElementById("Project").style.display = 'none';
		document.getElementById("projectTypeId").options[0].selected = true;
		document.getElementById("projectCodeId").value = "";
	}
	
	function hideprojectManager() {
		document.getElementById("dateTable").style.display = '';
		document.getElementById("ProjectManager").style.display = 'none';
		document.getElementById("pmId").options[0].selected = true;
	}
	
	function hideEmp() {
		document.getElementById("dateTable").style.display = '';
		document.getElementById("Employee").style.display = 'none';
		document.getElementById("empId").options[0].selected = true;
	} 

	function searchClick() {
			var searchObj = document.getElementById('searchBtn');
			var formObj = document.getElementById('generateReport');
			validate(searchObj, formObj);
	}

	
	function validate(searchObj , formObj) {
		var selectedReport = document.getElementById('reportTypeId').value;
		if (selectedReport == "0") {
			alert("Please select the report type");
			return false;
		}
		if (selectedReport == "1") {
			if (document.getElementById("projectTypeId").value == 0 ) {
				alert("Please select the Project Type");
				return false;
			}
			else if (document.getElementById("projectCodeId").value == "") {
				alert("Please select the Project Code");
				return false;
			}
			
			var projectTypeId = document.getElementById("projectTypeId").value;
			var projectCode = document.getElementById("projectCodeId").value.replace(/ /g,'');
			var projectId = document.getElementById("projectId").value;

			if(projectCode!=""){
				if(!projectAry[projectTypeId] || !projectAry[projectTypeId][projectCode]){
					alert("Project code is invalid");
					document.getElementById("projectId").value ="";
					return false;
				}
				else{
					document.getElementById("projectId").value = projectAry[projectTypeId][projectCode].split(":")[0];
				}
			}
			
			if(projectCode=="0"){
				document.getElementById("projectId").value ="";
			}
		}
		else if (selectedReport == "2" && document.getElementById("pmId").value == 0) {
			alert("Please select the project manager name");
			return false;
		}
		else if (selectedReport == "3" && document.getElementById("empId").value == 0) {
			alert("Please select the employee name");
			return false;
		}
		return dateValidate(searchObj , formObj);
	}

	function dateValidate(searchObj , formObj){
		var today = new Date();
		var startDate = document.getElementById("startDateStrId");
		var endDate = document.getElementById("endDateStrId");

		if(startDate.value!="" && !isValidDate(startDate)){
			alert("Start Date is invalid. (Format - dd/mm/yyyy)");
			return false;
		}
		if(endDate.value!="" && !isValidDate(endDate)){
			alert("End Date is invalid. (Format - dd/mm/yyyy)");
			return false;
		}
		
		startDate = startDate.value;
		startDate = startDate.split("/");
		startDate = new Date(startDate[2]+"-"+startDate[1]+"-"+startDate[0]);
		
		endDate = endDate.value;
		endDate = endDate.split("/");
		endDate = new Date(endDate[2]+"-"+endDate[1]+"-"+endDate[0]);
		
		if (startDate > endDate){
			alert("Start date should be less than or equal to end date");
			return false;
		}

		if (searchObj != null)
		{
			formObj.action = "searchReport";
			formObj.submit();
		}
		return true;
	}
	
	function isValidDate(date){
		var matches = /^(\d{2}|\d{1})[\/\/](\d{2}|\d{1})[\/\/](\d{4})$/.exec(date.value);
	    if (matches == null) return false;
	    var d = matches[1];
	    var m = matches[2] - 1;
	    var y = matches[3];
	    var composedDate = new Date(y, m, d);
	    var result = composedDate.getDate() == d &&
	           composedDate.getMonth() == m &&
	           composedDate.getFullYear() == y &&
	           (parseInt(y)>=2000);
	    if(result){
	    	if(d.length==1) d="0"+d;
	    	if(m<10) m="0"+(m+1);
	    	else m=(m+1);
	    	date.value = d+"/"+m+"/"+y;
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}
	function toUpper(obj){
		obj.value=obj.value.toUpperCase();
	}
</script>


</head>
<body>
	<div class="main">
		<div class="page">
			<div class="top-header">
				<div class="title-heading">
					<div class="plan-lead-logo">
						<img src="<%=contextPath%>/images/plan-lead-logo.png" width="210"
							height="65" border="0" alt="Logo" title="Plan-Lead" />
					</div>
					<div class="tech-trends-logo">
						<img src="<%=contextPath%>/images/tech-trends-logo.png"
							width="195" height="65" border="0" alt="Logo" title="Tech-Trend" />
					</div>
					<div class="logout">
						<a href="logOut">Logout</a>
					</div>

				</div>
			</div>
			<div class="emp-details-part">
				<s:form name="generateReport" id="generateReport" action="generateReport"
					method="post" theme="simple">
					<div class="main_heading">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="33%">&nbsp;</td>
								<td width="34%">Report</td>
								<td width="33%"> <s:include value="/jsp/menu/MainMenu.jsp"></s:include></td>
							</tr>
						</table>
					</div>
					<table width="75%" border="0" cellspacing="0" cellpadding="0">
						<tr style="display: hidden;" id = "errorRow">
							<td width="20%" nowrap="nowrap" align="left" style="padding:0 0 0 250px;">
								<div id="errorMessageDiv">
									<s:if test="hasActionErrors()">
										<s:actionerror cssClass="error" />
									</s:if>
								</div>
							</td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="5" cellpadding="0">
					
						<tr>
							<td width="5%" class="report-labels" valign="middle" align="right">
								Report Type * &nbsp;:&nbsp;</td>
							<td width="25%" class="report-controls" align="left" valign="middle">
								<s:select cssClass="list-box" tabindex="1" 
								id="reportTypeId" list="#{'0':'Select', '1':'Project', '2':'Project Manager', '3':'Employee'}" 
								name="reportType" onchange="getReportType();" style="width:145px; vertical-align: middle;"/>
							</td>
						</tr>
					</table>
					<div style="background-color:#DDE4E8;">
						<table width="100%" border="0" cellspacing="5" cellpadding="0">
							<tr>
								<td>
									<table id="Project" width="100%" border="0" cellspacing="5" 
											cellpadding="0" style="display: none;">
										<tr>
											<td width="13%" class="report-labels" align="right">Project Type *&nbsp;:&nbsp;</td>
											<td class="report-controls" width="13%">
					                        	<s:select name="projectTypeId" id="projectTypeId" list="projectTypeVoList" listKey="projectTypeId" 
					                        		listValue="projectType" tabindex="2" cssClass="list-box" 
					                        		style="width:145px" onchange="resetSearchProject();"/>
	                        				</td>
											<td width="11%" class="report-labels" align="right">Project Code *&nbsp;:&nbsp;</td>
												
											<td width="40%" class="report-controls" align="left" nowrap="nowrap" valign="middle">
												<s:hidden name="projectId" id="projectId"/>
	                    						<s:textfield name="projectCodeId" id="projectCodeId" style="width:120px; height : 13px;" tabindex="3" 
	                    									 onchange="toUpper(this)" />
															 
												<a id="projectPopup" href="javascript:openSearchProjectPopup()" tabindex="3.1">
	                       							<img src="<%=contextPath%>/images/select-icon.png" width="18" height="18" border="0"
	                       								style="cursor: pointer; padding 0 0 0 0; vertical-align: middle;" title="Select Project"/>
	                      	 					</a>
													 
											</td>
										</tr>
									</table>
									<table align="center"  id="ProjectManager" width="100%" border="0" cellspacing="5" 
											cellpadding="0" style="display: none;">
										<tr>
											<td width="13%" class="report-labels" align="right">Project Manager *&nbsp;:&nbsp;</td>
												
											<td width="13%" class="report-controls" align="left" nowrap="nowrap" valign="middle">
												
												<s:select list="projectManagerList" listKey="idValue" listValue="name" headerKey="0" headerValue="Select" 
													cssClass="list-box" name="pmId" id="pmId" style="width:145px; vertical-align: middle;" 
													tabindex="2"/>
											</td>
											<td width="12%">&nbsp;</td>
											<td width="40%">&nbsp;</td>
										</tr>
									</table>
									<table id="Employee" width="100%" border="0" cellspacing="5" 
											cellpadding="0" style="display: none;">
										<tr>
											<td width="13%" class="report-labels" align="right">Employee Name *&nbsp;:&nbsp;</td>
												
											<td width="13%" class="report-controls" align="left" nowrap="nowrap" valign="middle">
												<s:hidden name="employeeId" id="employeeId" />
												
												<s:select list="employeeList" listKey="idValue" listValue="name" headerKey="0" headerValue="Select" 
													cssClass="list-box" name="empId" id="empId" style="width:145px; vertical-align: middle;" 
													tabindex="2"/>
											</td>
											<td width="12%">&nbsp;</td>
											<td width="40%">&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					<table width="100%" border="0" cellspacing="5" cellpadding="0" style="background-color:#DDE4E8;display: none;" id="dateTable">
						<tr>
							<td width="17%" class="report-labels" align="right">Start Date &nbsp;:&nbsp;</td>
								
							<td width="16%" class="report-controls" align="left" nowrap="nowrap">
								<sj:datepicker displayFormat="dd/mm/yy" tabindex="4" maxlength="10"
									id="startDateStrId" cssStyle="width:120px;" onChangeTopics="onChangeDate"
									buttonImageOnly="true" name="taskStartDate" readonly="false"
									onpaste="return false;" />
									
							</td>
							
							<td width="15%" class="report-labels" align="right">End Date 
								&nbsp;:&nbsp;</td>
								
							<td width="25%" class="report-controls" align="left" nowrap="nowrap">
								<sj:datepicker displayFormat="dd/mm/yy" tabindex="5" maxlength="10"
									id="endDateStrId" cssStyle="width:120px;" cssClass="txt"
									buttonImageOnly="true" onChangeTopics="onChangeDate"
									name="taskEndDate" readonly="false" onpaste="return false;" />
							</td>
							<td width="25%">&nbsp;</td>
						</tr>
					</table>
					<table style="background-color:#DDE4E8;" width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="35%">&nbsp;</td>
							<td width="25%" class="save-btn" align="center">
								<a name="search"  id="searchBtn" tabindex="6" onclick="return searchClick();">
									<img title="Search" src="<%=contextPath%>/images/search-btn-new.png"
										 border="none" style="cursor: pointer;"/>
								</a>
							</td>
									
							<td width="25%" class="save-btn" align="center">
								<input type="image" name="saveId" id="saveId"
										tabindex="8" title="Save" src="<%=contextPath%>/images/pdf-report-btn.png" 
										onclick="return validate();"/>
							</td>
							<td width="35%">&nbsp;</td>
										
						</tr>
					</table>
				</s:form>
				<!-- <div style="background-color:#DDE4E8;"> -->
				
				<c:if test="${fn:length(reportList) gt 0}">
					<table width="100%" cellspacing="0" cellpadding="0">

						<tr>
							<td style="line-height: 10px; background-color: vertical-align : middle;
								background-color: #2D4A58;">
								&nbsp;
							</td>
						</tr>

					</table>

					<table width="100%" cellspacing="0" cellpadding="0" class="display" id="resultTable">
						<c:if test="${(reportType =='1') || (reportType =='2')}">
							<thead>
								<tr>
									<th rowspan="2" align="left" style="width:1%;">Sl.No.</th>
									<c:if test="${reportType =='1'}">
										<th rowspan="2" align="left" style="width:5%;">Manager&nbsp;Name</th>
									</c:if>
									<c:if test="${reportType =='2'}">
										<th rowspan="2" align="left" style="width:5%;">Project&nbsp;Name</th>
									</c:if>
									<th rowspan="2" align="left" style="width:5%;">Employee&nbsp;Name</th>
									<th rowspan="2" align="left" style="width: 8%;">Task&nbsp;Date</th>
									<th colspan="3">Plan</th>
									<th colspan="3">Actual</th>
									<th rowspan="2" align="left" style="width: 8%;">Activity</th>
									<th rowspan="2" align="left" style="width: 8%;">Work&nbsp;Item</th>
								</tr>
								<tr>
									<th align="left" style="width: 14%;">Task&nbsp;Description</th>
									<th align="left" style="width: 8%;">Start&nbsp;Time</th>
									<th align="left" style="width: 8%;">End&nbsp;Time</th>
									<th align="left" style="width: 15%;">Task&nbsp;Description</th>
									<th align="left" style="width: 10%;">Start&nbsp;Time</th>
									<th align="left" style="width: 10%;">End&nbsp;Time</th>
								</tr>
							</thead>
						</c:if>
						<c:if test="${reportType =='3'}">
							<thead>
								<tr>
									<th rowspan="2" align="left" style="width:1%;">Sl.No.</th>
									<th rowspan="2" align="left" style="width: 10%;">Task&nbsp;Date</th>
									<th colspan="3">Plan</th>
									<th colspan="3">Actual</th>
									<th rowspan="2" align="left" style="width: 10%;">Activity</th>
									<th rowspan="2" align="left" style="width: 10%;">Work&nbsp;Item</th>
								</tr>
								<tr>
									<th align="left" style="width: 14%;">Task&nbsp;Description</th>
									<th align="left" style="width: 10%;">Start&nbsp;Time</th>
									<th align="left" style="width: 10%;">End&nbsp;Time</th>
									<th align="left" style="width: 15%;">Task&nbsp;Description</th>
									<th align="left" style="width: 10%;">Start&nbsp;Time</th>
									<th align="left" style="width: 10%;">End&nbsp;Time</th>
								</tr>
							</thead>	
						</c:if>	
						<tbody>			
							<s:iterator value="reportList"	status="IteratorStatus">
								<tr>
									<td>
										<s:property value="(#IteratorStatus.index + 1)"/>
									</td>
									
									<c:if test="${reportType =='1'}">
										<td>
											<s:property value="MANAGER"/>
										</td>
									
										<td>
											<s:property value="Name"/>
										</td>
									</c:if>
									
									<c:if test="${reportType =='2'}">
										<td>
											<s:property value="PROJECT_NAME"/>
										</td>
									
										<td>
											<s:property value="Name"/>
										</td>
									</c:if>
									
									<td>
										<s:property value="taskDate"/>
									</td>
											
									<td>
										<div align="left" class="msgcontainer wrapword">
											<s:property value="PLAN_TASK_DESC"/>
										</div>
									</td>
										
									<td>
										<s:property value="PLAN_START_TIME"/>
									</td>
											
									<td>
										<s:property value="PLAN_END_TIME"/>
									</td>
											
									<td>
										<div align="left" class="msgcontainer wrapword">
											<s:property value="ACTUAL_TASK_DESC"/>
										</div>
									</td>
											
									<td>
										<s:property value="ACTUAL_START_TIME"/>
									</td>
									
									<td>
										<s:property value="ACTUAL_END_TIME"/>
									</td>
									
									<td>
										<s:property value="ACTIVITY"/>
									</td>
									
									<td>
										<s:property value="WORK_ITEM"/>
									</td>
								</tr>
						</s:iterator>
						</tbody>
					</table>
				</c:if>
				<!-- </div> -->
				<c:if test="${fn:length(reportList) eq 0}">
					<div style="background-color:#DDE4E8; height: 370px;">
					
						<table width="100%" cellspacing="0" cellpadding="0">
							<c:forEach var="i" begin="1" end="25">
								<tr>
									<td colspan="10">&nbsp;</td>
								</tr>
							</c:forEach>
						</table>
					
					</div>
				</c:if>
				<div class ="footer-part">Copyright © SRM Technologies Private Limited </div>
			</div>
		</div>
	</div>

	<script>
		function openProjectPopup(projectId, projectCodeId, rowNo) {

			var projectType = "1";
			var projectTypeId = document.getElementById("addTask_task_" + rowNo
					+ "__projectTypeActionVo_projectTypeId");
			if (projectTypeId) {
				if (projectTypeId.value == "0") {
					alert("Please select project type");
					return false;
				}
				projectType = document.getElementById("addTask_task_" + rowNo
						+ "__projectTypeActionVo_projectTypeId").value;
			}

			newwindow = window.open("projectList?projectListId=" + projectId
									+ "&projectCodeId=" + projectCodeId
									+ "&projectType=" + projectType, 'newwindow',
									'scrollbars=yes,menubar=0,height=300,width=500,left=200,top=200');
		}
		window.onload=function(){
			getReportType();
			document.getElementById("errorRow").style.display = '';
			document.getElementById("startDateStrId").value="${taskStartDate}";
			document.getElementById("endDateStrId").value="${taskEndDate}";
		}
	</script>
</body>
</html>
