<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
			response.setLocale(java.util.Locale.ENGLISH);
%>
<%
    /* Getting Context Path */
			String contextPath = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Resource Allocation-SRM Technologies</title>
<sj:head compressed="false" />
<sx:head debug="false" />
<s:head theme="simple" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/shirai.css" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<%=contextPath%>/js/iepngfix_tilebg.js"></script>
<script type="text/javascript">
	window.history.forward();
	function noBack() { 
		window.history.forward(); 
	}

</script>
<script type="text/javascript">

var roleAry = new Array();
<s:iterator value="roleList" status="IteratorStatus">
	roleAry["${idValue}"] = "${name}";
</s:iterator>

var modeAry = new Array();
<s:iterator value="modeList" status="IteratorStatus">
	modeAry["${idValue}"] = "${name}";
</s:iterator>

var sharedResourceAry = new Array();
<s:iterator value="availResourceList" status="userStatus" ><c:if test="${mode eq '1'}">
		sharedResourceAry["${employeeCode}"]="${employeeName}";
</c:if></s:iterator>

function searchResource() {
	if(document.getElementById("projectId").value=="0"){
		alert("Please select Project Name");
		return false;
	}
	document.fom.action	="searchResourceAllocation.action";
   	document.fom.method = "POST";
   	document.fom.submit();
}

function isValidDate(date){
	var matches = /^(\d{2}|\d{1})[\/\/](\d{2}|\d{1})[\/\/](\d{4})$/.exec(date.value);
    if (matches == null) return false;
    var d = matches[1];
    var m = matches[2] - 1;
    var y = matches[3];
    var composedDate = new Date(y, m, d);
    var result = composedDate.getDate() == d &&
           composedDate.getMonth() == m &&
           composedDate.getFullYear() == y &&
           (parseInt(y)>=2000);
    if(result){
    	if(d.length==1) d="0"+d;
    	if(m<9) m="0"+(m+1);
    	else m=(m+1);
    	date.value = d+"/"+m+"/"+y;
    	return true;
    }
    else{
    	return false;
    }
}
function dateCompare(startDate, endDate){
	startDate = startDate.value;
	startDate = startDate.split("/");
	var startDate1 = new Date();
	startDate1.setFullYear(parseInt(startDate[2]));
	startDate1.setMonth(parseInt(startDate[1])-1);
	startDate1.setDate(parseInt(startDate[0]));
	
	endDate = endDate.value;
	endDate = endDate.split("/");
	var endDate1 = new Date();
	endDate1.setFullYear(parseInt(endDate[2]));
	endDate1.setMonth(parseInt(endDate[1])-1);
	endDate1.setDate(parseInt(endDate[0]));
	if (startDate1 > endDate1) {
		return false;
	}
	else{
		return true;
	}
}
function updateResources(){
	var secondRowCounter = document.getElementById("secondRowCounter").value;
	if(secondRowCounter!=""){
		secondRowCounter = parseInt(secondRowCounter);
		for(var row = 0; row<secondRowCounter;row++){
			var prjStartDate = document.getElementById("startDateId");
			var prjEndDate = document.getElementById("endDateId");
			var EmpCode   = document.getElementById("secondEmpCode_"+row);
			var startDate = document.getElementById("startDateStr_"+row);
			var endDate   = document.getElementById("endDateStr_"+row);
			var role      = document.getElementById("secondEmpRole_"+row);
			var mode      = document.getElementById("secondEmpMode_"+row);
			
			if(startDate && startDate.value==""){
				alert("Start date is required for Employee - "+EmpCode.value);
				return false;
			}
			else if(startDate && startDate.value!="" && !isValidDate(startDate)){
				alert("Start date is invalid for Employee - "+EmpCode.value);
				return false;
			}
			else if(startDate && startDate.value!="" && !dateCompare(prjStartDate,startDate)){
				alert("Start date should should be greater than project start date for Employee - "+EmpCode.value);
				return false;
			}
			if(endDate && endDate.value==""){
				alert("End date is required for Employee - "+EmpCode.value);
				return false;
			}
			else if(endDate && endDate.value!="" && !isValidDate(endDate)){
				alert("End date is invalid for Employee - "+EmpCode.value);
				return false;
			}
			else if(endDate && endDate.value!="" && !dateCompare(endDate,prjEndDate)){
				alert("End date should should be less than project end date for Employee - "+EmpCode.value);
				return false;
			}
			else if(startDate && startDate.value!="" && endDate && endDate.value!="" && !dateCompare(startDate,endDate)){
				alert("Start date should should be less than End date for Employee - "+EmpCode.value);
				return false;
			}
			if(role && role.value=="0"){
				alert("Role is required for Employee - "+EmpCode.value);
				return false;
			}
			if(mode && mode.value=="0"){
				alert("Mode is required for Employee - "+EmpCode.value);
				return false;
			}
		}
	}
 	document.fom.action	="saveResourceAllocation.action";
   	document.fom.method = "POST";
   	document.fom.submit();
}
  
  
 function addRightTableRow(obj) {
		
     var firstTable = document.getElementById("first");
     var count = firstTable.rows.length;
     var secondRowCounter = document.getElementById("secondRowCounter").value;
     var isRecord = false;
     var countSelected = 0;
     for (var i=count-1; i>=0; i--) {
	    var row = firstTable.rows[i];
 		var checked = true;
 		if(obj == "false") { 
 		   checked = findCheckbox(row)[0].checked; 
 		} 
 		    
        if(checked){	        	
          var checkBox = findCheckbox(row)[0];
  	      var rowNo = parseInt(checkBox.id.substring(11));	   
  	       	      
          var table = document.getElementById("second");
          var rowCount = table.rows.length;
		  if(obj == "false") {
				for(var n=rowCount-1; n>=0; n--) {
				  var secondRow = table.rows[n];
				  var secondcheckBox = findCheckbox(secondRow)[0];
  	              var secondRowNo = parseInt(secondcheckBox.id.substring(12));
					if(document.getElementById("firstEmployeeId_"+rowNo).value == document.getElementById("secondEmpId_"+secondRowNo).value){
						 alert(document.getElementById("firstEmpName_"+rowNo).value+" Emp ID already exists");
						 isRecord = true;
				   }
				}
		  }
          if(!isRecord) {
              var allocRow = table.insertRow(rowCount);
				//row.id = "secondRow_"+(i+1);
              var cell1 = allocRow.insertCell(0);
              cell1.className = "table-cont-checkbox";
              cell1.style.width = "50px";
              var element1 = document.createElement("input");
              element1.type = "checkbox";
              element1.id = "secondCheck_"+secondRowCounter;
              element1.className = "check-box";
              cell1.appendChild(element1);
              
              var cell2 = allocRow.insertCell(1);
              cell2.className = "table-cont";
              cell2.style.width = "120px";
              var element2 = document.createElement("input");
              element2.type = "text";
              element2.id = "secondEmpCode_"+secondRowCounter;
              element2.readOnly = "readonly";
              element2.name = "allocResourceList["+secondRowCounter+"].employeeCode";
              element2.value = document.getElementById("firstEmpCode_"+rowNo).value;
              element2.className = "emp-id";
              cell2.appendChild(element2);
			  
              var cell3 = allocRow.insertCell(2);
              cell3.className = "table-cont";
              cell3.style.width = "80px";
              var element3 = document.createElement("input");
              element3.type = "text";
              element3.readonly = "readonly";
              element3.className = "shedule-date";
              element3.id="startDateStr_"+secondRowCounter;
              element3.setAttribute("name", "allocResourceList["+secondRowCounter+"].startDateStr");
              cell3.appendChild(element3);
              try{
                  $('#startDateStr_'+secondRowCounter).datepicker({
                      dateFormat: 'dd/mm/yy'
                  });
              }
              catch(e){alert(e)}
              
              var cell4 = allocRow.insertCell(3);
              cell4.className = "table-cont";
              cell4.style.width = "80px";
              var element4 = document.createElement("input");
              element4.type = "text";
              element4.readonly = "readonly";
              element4.className = "shedule-date";
              element4.id="endDateStr_"+secondRowCounter;
              element4.setAttribute("name", "allocResourceList["+secondRowCounter+"].endDateStr");
              cell4.appendChild(element4);
              try{
                  $('#endDateStr_'+secondRowCounter).datepicker({
                      dateFormat: 'dd/mm/yy'
                  });
             }
             catch(e){alert(e)}
                            
             var cell5 = allocRow.insertCell(4); 
             cell5.className="table-cont";
             var element5 = document.createElement("select");
             element5.className = "role";
             cell5.style.width = "230px";
             element5.id = "secondEmpRole_"+secondRowCounter;
             element5.setAttribute("name", "allocResourceList["+secondRowCounter+"].role");
             var option;
             
			   // create options elements 
			  option = document.createElement("option");
			  option.setAttribute("value", 0);
			  option.innerHTML = "Select";
			  element5.appendChild(option);
			  
			  for(var key in roleAry){
				  option = document.createElement("option");
				  option.setAttribute("value", key);
				  option.innerHTML = roleAry[key];
				  element5.appendChild(option);
			  }
			  
			  element5.value = document.getElementById("firstEmpRoleHidden_"+rowNo).value;	  
			  cell5.appendChild(element5);
			   
			  var cell6 = allocRow.insertCell(5); 
			  cell6.className="role";
			  cell6.style.width = "150px";
	          var element6 = document.createElement("select");
	          element6.className = "mode";  
	          element6.id = "secondEmpMode_"+secondRowCounter;
	          element6.setAttribute("name", "allocResourceList["+secondRowCounter+"].mode");
			  // create options elements 
			  
			  if(sharedResourceAry[element2.value]){
			  	  for(var key in modeAry){
					 if(key=="1"){
						  option = document.createElement("option");
						  option.setAttribute("value", key);
						  option.innerHTML = modeAry[key];
						  element6.appendChild(option);
					 }
			  	  }
		       }
			   else{
				   option = document.createElement("option");
				   option.setAttribute("value", 0);
				   option.innerHTML = "Select";
				   element6.appendChild(option);
				   for(var key in modeAry){
					  option = document.createElement("option");
					  option.setAttribute("value", key);
					  option.innerHTML = modeAry[key];
					  element6.appendChild(option);
				   }
			  }
			  element6.value = document.getElementById("firstEmpModeHidden_"+rowNo).value;	  
			  cell6.appendChild(element6);
			   //delete
			   
			  var element7 = document.createElement("input");
              element7.type = "hidden";
              element7.id = "secondEmpName_"+secondRowCounter;
              element7.name = "allocResourceList["+secondRowCounter+"].empName";
              element7.value = document.getElementById("firstEmpName_"+rowNo).value;
              cell6.appendChild(element7);
              
              var element9   = document.createElement("input");
              element9.type  = "hidden";
              element9.id    = "secondEmpId_"+secondRowCounter;
              element9.name  = "allocResourceList["+secondRowCounter+"].employeeId";
              element9.value = document.getElementById("firstEmployeeId_"+rowNo).value;
              cell6.appendChild(element9);
              
              secondRowCounter++;
              document.getElementById("secondRowCounter").value =  secondRowCounter;
              countSelected++;
           }
         }
                               		
     }
              
    if(countSelected == 0 && !isRecord) {
         alert("Select atleast one row");
    }
} 

function addLeftTableRow(obj) {
    var allocTable = document.getElementById("second");
    var allocTableRowCount = allocTable.rows.length;
    var firstRowCounter = document.getElementById("firstRowCounter").value;
    var selectPointer = false; 
    var countSelected = 0;
    var checked = true;
    for (var i=allocTableRowCount-1; i>=0; i--) {
	   var allocRow = allocTable.rows[i];
	   var allocChecked = true;
	   if(obj == "false") { 
		   allocChecked = findCheckbox(allocRow)[0].checked; 
	   } 
       if(allocChecked) {
          	var allocCheckBox = findCheckbox(allocRow)[0];
          	var allocRowNo = parseInt(allocCheckBox.id.substring(12));
          	var availTable = document.getElementById("first");
          	var availRowCount = availTable.rows.length;
          	selectPointer = true;
      		if(document.getElementById("secondEmpMode_"+allocRowNo).value != "1") { 
	     	     var availRow = availTable.insertRow(availRowCount);              
	             var cell1 = availRow.insertCell(0);
	             cell1.className = "table-cont-checkbox";
	             cell1.style.width = "50px";
	             var element1 = document.createElement("input");
	             element1.type = "checkbox";
	             element1.id = "firstCheck_"+firstRowCounter;
	             element1.className = "check-box";
	             element1.value = parseInt(firstRowCounter)+parseInt("1");
	             cell1.appendChild(element1);
	             
	             var cell2 = availRow.insertCell(1);
	             cell2.className = "table-cont";
	             cell2.style.width = "50px";
	             var element2 = document.createElement("input");
	             element2.type = "text";
	             
	             element2.readonly = "readonly";
	             element2.value = parseInt(firstRowCounter)+parseInt("1");
	             element2.className = "serial-no";
	             cell2.appendChild(element2);
	             
	             var cell3 = availRow.insertCell(2);
	             cell3.className = "table-cont";
	             cell3.style.width = "80px";
	             var element3 = document.createElement("input");
	             element3.type = "text";
	             element3.readonly = "readonly";
	             element3.id = "firstEmpName_"+firstRowCounter;
	             element3.name="availableResource["+firstRowCounter+"].employeeName";
	             element3.value = document.getElementById("secondEmpName_"+allocRowNo).value;
	             element3.className = "name-list";
	             cell3.appendChild(element3);
	             
	             var cell4 = availRow.insertCell(3);
	             cell4.className = "table-cont";
	             cell4.style.width = "120px";
	             var element4 = document.createElement("input");
	             element4.type = "text";
	             element4.readonly = "readonly";
	             element4.id = "firstEmpCode_"+firstRowCounter;
	             element4.name="availableResource["+firstRowCounter+"].empId"
	             element4.value = document.getElementById("secondEmpCode_"+allocRowNo).value;
	             element4.className = "emp-id";
	             cell4.appendChild(element4);
	             
	             var element5 = document.createElement("input");
	             element5.type = "hidden";
	             element5.id = "firstEmpRoleHidden_"+firstRowCounter;
	             element5.name = "availableResource["+firstRowCounter+"].empRole";
	             element5.value = document.getElementById("secondEmpRole_"+allocRowNo).value;
	             cell3.appendChild(element5); 
	             
	             var element6 = document.createElement("input");
	             element6.type = "hidden";
	             element6.id = "firstEmpModeHidden_"+firstRowCounter;
	             element6.name = "availableResource["+firstRowCounter+"].empMode";
	             element6.value = document.getElementById("secondEmpMode_"+allocRowNo).value;
	             cell3.appendChild(element6); 
	             
	             var element7 = document.createElement("input");
	             element7.type = "hidden";
	             element7.id = "firstEmployeeId_"+firstRowCounter;
	             element7.name = "availableResource["+firstRowCounter+"].employeeId";
	             element7.value = document.getElementById("secondEmpId_"+allocRowNo).value;
	             cell3.appendChild(element7); 
	             
	             var element8 = document.createElement("input");
	             element8.type = "hidden";
	             element8.id = "firstEmpNameHidden_"+firstRowCounter;
	             //element8.name = "availableResource["+firstRowCounter+"].employeeId";
	             element8.value = document.getElementById("secondEmpName_"+allocRowNo).value;
	             cell3.appendChild(element8); 
	             
              	 firstRowCounter++;
              	 document.getElementById("firstRowCounter").value =  firstRowCounter;
              	 countSelected++;            
        	}
        }
    }
    if((countSelected == 0) && (!selectPointer)) {
         alert("Select atleast one row");
    }
}

function deleteLeftTableRow(obj , tableId){
	 var firstTable = document.getElementById(tableId);
	 var count = firstTable.rows.length;
	 for (var i=count-1; i>=0; i--) {
	 		var row = firstTable.rows[i];
	       var checked = true;
	 		if(obj == "false") { 
	 		   checked = findCheckbox(row)[0].checked; 
	 		} 
	  		if(checked) {
			   	var checkBox = findCheckbox(row)[0];
			   	var rowNo = parseInt(checkBox.id.substring(11)); 
			   	document.getElementById("firstCheck_"+rowNo).checked = false;
			   	if(document.getElementById(tableId+"EmpModeHidden_"+rowNo).value != "1") {  
			    	firstTable.deleteRow(i);
			    }  
	   		}
	  }
}

function deleteRightTableRow(obj , tableId){
	 var firstTable = document.getElementById(tableId);
	 var count = firstTable.rows.length;
	 for (var i=count-1; i>=0; i--) {
	 		var row = firstTable.rows[i];
	       var checked = true;
	 		   if(obj == "false") { 
	 		   		var checked = findCheckbox(row)[0].checked; 
	 		    } 
	  		   if(checked) {
				    	firstTable.deleteRow(i);
		    	}
	 }
}
function findCheckbox(row) {
	var inputs = row.getElementsByTagName("input");
	var chk = new Array();
	for(var i=0;i<inputs.length;i++) {
		if(inputs[i].type=="checkbox") {
		 	chk[chk.length] = inputs[i];
		 	
		}
	}
	return chk;
}

</script>

<script type="text/javascript">
//<![CDATA[
function selfTest() {
   if (!/MSIE (5\.5|6)/.test(navigator.userAgent)) {
       return alert('Please try this in IE6 :)');
   }
   var failed = 'MISSING! Check your .HTC pathname is correct ' +
       'and that the server is returning the correct MIME type.';
   var demoImg = document.getElementById('demoImg');
   alert(
       'IEPNGFix Core Behavior: ' +
       (IEPNGFix.process ? 'OK' : failed) +
       '\n\nBackground repeat/position extension: ' +
       (IEPNGFix.tileBG ? 'OK' : 'MISSING! Check your SCRIPT SRC for the extension') +
       '\n\nFilter activation: ' +
       (demoImg.filters['DXImageTransform.Microsoft.AlphaImageLoader'] ? 'OK' : failed) +
       '\n\nBLANK image pathname: ' + demoImg.src
   );
   if (location.protocol.indexOf('http') == -1) {
       return alert('Please run this from your server for MIME testing.');
   }
   alert('The script will now request "iepngfix.htc" in the current folder...');
   var xmlhttp;
   try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
   } catch (e) {
       try {
           xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (e) { }
   }
   // Change this to test if your pathname is right.
   xmlhttp.open('GET', '<%=contextPath%>/iepngfix.htc', true);
   xmlhttp.onreadystatechange = function() {
       if (xmlhttp.readyState == 4) {
           alert(
               'Server response: File ' +
               (xmlhttp.status == 404 ? 'NOT FOUND!' : 'found.') +
               '\n\nMIME type (should be "text/x-component"): ' +
               xmlhttp.getResponseHeader('Content-Type')
           );
       }
   };
   xmlhttp.send(null);
};
//]]>
function MM_swapImgRestore() { //v3.0
 var i,x,a=document.MM_sr; 
 for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
 var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
   d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
 if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
 for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
 if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
 var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
  if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<style type="text/css">
img, div {
behavior: url(<%=contextPath%>/iepngfix.htc);
}
</style> 

</head>

<body onload="MM_preloadImages('images/right-move-hover.png','images/total-right-move-hover.png','images/left-move-hover.png','images/total-left-move-hover.png','images/update-hover.png','images/cancel-hover.png')">

<s:form name="fom" theme="simple">
<s:hidden name="selectedProjectId" value="%{projectId}"/>
<div class="main">
	<div class="page">
		<div class="top-header">
			<div class="title-heading">
				<div class="plan-lead-logo">
					<img src="<%=contextPath%>/images/plan-lead-logo.png" width="210"
						height="65" border="0" alt="Logo" title="Plan-Lead" />
				</div>
				<div class="tech-trends-logo">
					<img src="<%=contextPath%>/images/tech-trends-logo.png"
						width="195" height="65" border="0" alt="Logo" title="Tech-Trend" />
				</div>
				<div class="logout">
					<a href="logOut">Logout</a>
				</div>

			</div>
		</div>
		<div class="pro-details-part" style="height: 120px;">
			<table class="main_heading" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="33%">&nbsp;</td>
					<td width="34%">Resource Allocation</td>
					<td width="33%"><s:include value="/jsp/menu/MainMenu.jsp"></s:include></td>
				</tr>
			</table>
			<table width="80%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td style="width:20px;">&nbsp;</td>
					<td style="width:100px;">
						<s:label value="Project Name :"/>
					</td>
					<td style="width:220px;">
						<s:select name="projectId" id="projectId" cssStyle="width : 220px;" headerKey="0" headerValue="Select"  list ="projectNameList" listKey="key" listValue="name"/>
					</td>
					<td style="width:70px;">
						<s:label value="PM Name :"/>
					</td>
					<td style="width:120px;">
						<s:textfield name="pmName" id="pmName" cssStyle="width:120px;" readonly="true"/>
					</td>
					<td style="width:70px;">
						<s:label value="PL Name :"/>
					</td>
					<td style="width:120px;">
						<s:textfield name="plName" id="plName" cssStyle="width:120px;" readonly="true"/>
					</td>
				</tr>
				<tr>
					<td style="width:20px;">&nbsp;</td>
					<td>
						<s:label value="Start Date :"/>
					</td>
					<td>
						<s:textfield id="startDateId" name="startDateStr" tabindex="4" Style="width:120px;" maxlength="10" readonly="true"/>
						<%-- <sj:datepicker displayFormat="dd/mm/yy" tabindex="4" maxlength="10"
						id="startDateId" cssStyle="width:120px;" onChangeTopics="onChangeDate"
						buttonImageOnly="true" name="startDateStr" readonly="true" value="%{startDateStr}"
						onpaste="return false;" showOn="none"/> --%>
					</td>
					<td>
						<s:label value="End Date :"/>
					</td>
					<td>
						<s:textfield id="endDateId" name="endDateStr" tabindex="6" Style="width:120px;" maxlength="10" readonly="true"/>
						<sj:datepicker displayFormat="dd/mm/yy" tabindex="5" maxlength="10"
						id="test" cssStyle="width:120px;display:none;" onChangeTopics="onChangeDate"
						buttonImageOnly="true" name="test" readonly="true" value="%{endDateStr}"
						onpaste="return false;" showOn="none"/>
						<%-- <sj:datepicker displayFormat="dd/mm/yy" tabindex="5" maxlength="10"
						id="endDateId" cssStyle="width:120px;" onChangeTopics="onChangeDate"
						buttonImageOnly="true" name="endDateStr" readonly="true" value="%{endDateStr}"
						onpaste="return false;" showOn="none"/> --%>
					</td>	
					<td width="25%" class="save-btn" align="center" style="vertical-align: top;" colspan="2">
						<a name="search"  id="searchBtn" tabindex="6">
							<img title="Search" src="<%=contextPath%>/images/search-btn-new.png"
								 border="none" style="cursor: pointer;" onclick="searchResource();"/>
						</a>
					</td>
				</tr>
			</table>
		</div>
		<div class="error-msg">&nbsp;
			<table width="1148px" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td nowrap="nowrap" align="center">
						<div id="successMessageDiv">
							<s:if test="hasActionMessages()">
								<s:actionmessage cssClass="success" />
							</s:if>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="main-table-part">
			<div class="freeze-header-part">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr id="firstRow">
						<td style="width:450px;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="table-title" style="width:50px;"></td>
									<td class="table-title" style="width:50px;">S.No</td>
									<td class="table-title" style="width:230px;">Emp Name</td>
									<td class="table-title" style="width:120px;">Emp Id</td>
								 </tr>
							</table>
						</td>
						<td style="width:60px; background-color:#c3c9cc;">&nbsp;</td>
						<td style="width:730px;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr id="secondRow">
									<td class="table-title" style="width:50px;"></td>
									<td class="table-title" style="width:110px;">Emp Id</td>
									<td class="table-title" style="width:78px;">Start Date</td>
									<td class="table-title" style="width:78px;">End Date</td>
									<td class="table-title" style="width:230px;">Role</td>
									<td class="table-title" style="width:150px;">Mode</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div class="freeze-cont-part" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="width:450px;" valign="top">
							<div class="left-part-freeze">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" id="first">
									<% int idx =  0; %>
									<s:iterator value="availResourceList" status="userStatus" >
									<tr id="firstRow_<%=idx%>">
										<td class="table-cont-checkbox" style="width:50px; /* border-bottom:#c3c9cc solid 1px;  */background-color:#FFFFFF;">
										<input  type="checkbox" id="firstCheck_<%=idx%>" value="<%=idx%>" class="check-box" /></td>
										<td class="table-cont" style="width:50px;"><s:textfield readonly="true" value="%{#userStatus.index + 1}" cssClass="serial-no" /></td>
										<td class="table-cont" style="width:50px;"><s:textfield readonly="true" name="availableResource[%{#userStatus.index}].employeeName" id="firstEmpName_%{#userStatus.index}" value="%{employeeName}" cssClass="name-list" />
											<input type="hidden" id="firstEmpNameHidden_<%=idx%>" value='<s:property value="employeeName"/>' />
											<input type="hidden" id="firstEmpCodeHidden_<%=idx%>" value='<s:property value="employeeCode"/>' />
											<input type="hidden" id="firstEmpRoleHidden_<%=idx%>" name="availableResource[<%=idx%>].role"  value='<s:property value="role"/>' />
											<input type="hidden" id="firstEmpModeHidden_<%=idx%>" name="availableResource[<%=idx%>].mode"  value='<s:property value="mode"/>' />
										</td>
										<td class="table-cont" style="width:50px;">
											<s:textfield readonly="true" name="availableResource[%{#userStatus.index}].employeeCode" id="firstEmpCode_%{#userStatus.index}" value="%{employeeCode}" cssClass="emp-id"/>
											<s:hidden name="availableResource[%{#userStatus.index}].employeeId" id="firstEmployeeId_%{#userStatus.index}" value="%{employeeId}"/>
										</td>
									</tr>
									<% idx++; %>
									</s:iterator>
									<input type="hidden" name="firstRowCounter" id="firstRowCounter" value="<%=idx%>" /> 
								</table>
							</div>
						</td>
						<td style="width:60px;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td height="40px">&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
										<a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image7','','<%=contextPath%>/images/right-move-hover.png',1)">
											<img src="<%=contextPath%>/images/right-move.png" name="Image7" width="30" height="30" border="0" id="Image7" onclick="javascript:addRightTableRow('false');deleteLeftTableRow('false' , 'first')"/>
										</a>
									</td>
								</tr>
								<tr>
									<td height="20px">&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
										<a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image8','','<%=contextPath%>/images/total-right-move-hover.png',1)">
											<img src="<%=contextPath%>/images/total-right-move.png" name="Image8" width="30" height="30" border="0" id="Image8" onclick="javascript:addRightTableRow('true');deleteLeftTableRow('true' , 'first');" />
										</a>
									</td>
								</tr>
								<tr>
									<td height="20px">&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
										<a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image9','','<%=contextPath%>/images/left-move-hover.png',1)">
											<img src="<%=contextPath%>/images/left-move.png" name="Image9" width="30" height="30" border="0" id="Image9" onclick="javascript:addLeftTableRow('false');deleteRightTableRow('false' , 'second');"/>
										</a>
									</td>
								</tr>
								<tr>
									<td height="20px">&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
										<a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image10','','<%=contextPath%>/images/total-left-move-hover.png',1)">
											<img src="<%=contextPath%>/images/total-left-move.png" name="Image10" width="30" height="30" border="0" id="Image10" onclick="javascript:addLeftTableRow('true');deleteRightTableRow('true' , 'second');"/>
										</a>
									</td>
								</tr>
								<tr>
									<td height="40px">&nbsp;</td>
								</tr>
							</table>
						</td>
						<td style="width:730px;" valign="top">
						<div class="right-part-freeze">
						
							<table id="second" width="100%" border="0" cellspacing="0" cellpadding="0" >
								<% int iex =  0; %>
								<s:iterator value="allocResourceList" status="userStatus" >
									<tr>
										<td class="table-cont-checkbox" style="width:50px;">
											<input id="secondCheck_<%=iex%>" type="checkbox" value="<%=iex%>" class="check-box" />
										</td>
										<td class="table-cont" style="width:120px;" >
											<s:textfield name="allocResourceList[%{#userStatus.index}].employeeCode" readonly="true"  id="secondEmpCode_%{#userStatus.index}" value="%{employeeCode}" cssClass="emp-id"/>
											<input type="hidden" name="allocResourceList[<%=iex%>].employeeName" id="secondEmpName_<%=iex%>" value='<s:property value="employeeName"/>' /> 
											<input type="hidden" name="allocResourceList[<%=iex%>].employeeId" id="secondEmpId_<%=iex%>" value='<s:property value="employeeId"/>' /> 
										</td>
										<td class="table-cont" style="width:80px;">								  
											<sj:datepicker displayFormat="dd/mm/yy" maxlength="10" 
												id="startDateStr_%{#userStatus.index}" onChangeTopics="onChangeDate"
												buttonImageOnly="true" name="allocResourceList[%{#userStatus.index}].startDateStr"
												showOn="focus" cssClass="shedule-date"/>									
										</td>
										<td class="table-cont" style="width:80px;">	
										
											<sj:datepicker displayFormat="dd/mm/yy" maxlength="10"
												id="endDateStr_%{#userStatus.index}" onChangeTopics="onChangeDate"
												buttonImageOnly="true" name="allocResourceList[%{#userStatus.index}].endDateStr"
												showOn="focus" cssClass="shedule-date"/>
										</td>
										<td class="table-cont" style="width:230px;">
											<s:select name="allocResourceList[%{#userStatus.index}].role" id="secondEmpRole_%{#userStatus.index}" cssClass="role" list="roleList" listKey="idValue" listValue="name" headerKey="0"
											headerValue="Select"  value="%{role}" />															  
										</td>
										<td class="table-cont" style="width:150px;">
											<s:select name="allocResourceList[%{#userStatus.index}].mode" id="secondEmpMode_%{#userStatus.index}"  cssClass="mode" list="modeList" listKey="idValue" listValue="name" headerKey="0"
											headerValue="Select" value="%{mode}" />
										</td>
									</tr>
								 <% iex++; %>
								 </s:iterator> 
								 <input type="hidden" name="secondRowCounter" id="secondRowCounter" value="<%=iex%>" /> 
							</table>							
						</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="save-btns-part">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td style="width:39%;"></td>
					<td class="save-btn" ><a onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image11','','<%=contextPath%>/images/update-hover.png',1)"><img src="<%=contextPath%>/images/update.png" onclick="updateResources();" name="Image11" width="86" height="36" border="0" id="Image11" /></a></td>
					<td style="width:2%;"></td>
					<td >&nbsp</td>
					<td style="width:39%;"></td>
				</tr>
			</table>
		</div>
		<div class="footer-part">Copyright @ 2012 SRM Technologies Private Limited
		</div>
	</div>
</div>
</s:form>
<script>
	window.onload=function(){
		<s:iterator value="allocResourceList" status="userStatus" >
			removeDedicatedOption(document.getElementById("secondEmpMode_${userStatus.index}"));
		</s:iterator>
	}
	function removeDedicatedOption(obj){
		if(obj && obj.value=="1"){
			for(var i=obj.options.length-1;i>=0;i--){
				if(obj.options[i].value!="1"){
					obj.remove(i);
				}
			}
		}
	}
</script>
</body>
</html>
