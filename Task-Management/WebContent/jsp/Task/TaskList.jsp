<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<html>
<head>
<sj:head  compressed="false"/>
<sx:head debug="false" />
<style>
	table{
		width:100%; 
		border-spacing:0;
		border-collapse: collapse;
	}
	th, td {
 		padding: 0;
	}
	.txt{
		width:90%;
	}
	
	/* Word Wrap */
.msgcontainer {
	width: 120px;
	height: auto;

}

.wrapword {
	white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */
	white-space: -pre-wrap; /* Opera 4-6 */
	white-space: -o-pre-wrap; /* Opera 7 */
	white-space: pre-wrap; /* css-3 */
	white-space: normal;
	word-wrap: break-word; /* Internet Explorer 5.5+ */
	
}
</style>
</head>
<body>
	<table style="width:100%">
		<tr>
			<td width="100%">TIME ANALYSIS SYSTEM</td>
		</tr>
	</table>
	<s:form name="searchTask" id="searchTask" action="searchTask" method="post">
	<table style="width:100%">
		<tr>
			<td width="20%">Employee Name</td>
			<td width="20%">Project</td>
			<td width="20%">Start Date</td>
			<td width="20%">End Date</td>
			<td width="20%">&nbsp;</td>
		</tr>
		<tr>
			<td width="20%">Sengottaiyan</td>
			<td width="20%"><table>
			 
			<%-- <sj:autocompleter
    				id="projectId"
    				name="echo"
    				list="%{projectList}"
    				listValue="name"
    				listKey="idValue"
    				indicator="idValue"
    				selectBox="true"
    				maxlength="20"
    				loadMinimumCount="2"
    			    selectBoxIcon="true"
    			   
    			/>  --%>
    			<sx:autocompleter showDownArrow="false" 
    			 searchType="startstring"
    			  list="projectList"   name="pickupSearchVo.companyNameKanji"   maxlength="40" 
																			 cssStyle=" height:24px;width:200px;outline:none;" 
                         													 dropdownHeight="400" 
                         													 resultsLimit="-1"
                         													 listValue="name"
    																		 listKey="idValue"
                         													 id="pickupSearchAuto" 
                         													 loadMinimumCount="25"
                         													  
				>  </sx:autocompleter>
    			
    			</table></td>
			<td width="20%"><table>
			<sj:datepicker displayFormat="yy/mm/dd" tabindex="2" id="startDateStrId"  
			 cssStyle="width:140px;height:25px" onChangeTopics="onChangeDate"  maxlength="	20"
			 name="startDate" readonly="false" onpaste="return false;" />
					</table></td>
																			  
			<td width="20%"><table>
				<sj:datepicker displayFormat="yy/mm/dd" tabindex="4"
					id="endDateStrId" cssStyle="width:140px;height:25px"
					onChangeTopics="onChangeDate" maxlength="20"
					name="endDate" readonly="false"
					onpaste="return false;" />
					</table></td>
					
				<td width="20%">
				<table>
				<sx:submit name="submit" type="button" key="search" formId="searchTask" 
				executeScripts="true" targets="content" showLoadingText="false" 
				errorText="Error"
				id="searchId"
				parseContent="false"
				separateScripts="false"
				validate="true"
				indicator="loadImage"
				/>
				</table>
			</td>
		</tr>
	</table>
	</s:form>
	
	<img id="loadImage" src="../images/loading.gif" style="display:none" alt="loading.."/>
	
	<s:url id="serachUrl" action="searchTask" >
	    <s:param name="projectId" value="projectId" />
	    <s:param name="startDate" value="startDate" />
	    <s:param name="endDate" value="endDate" />
	</s:url>
	
	<sx:div id="content" href="%{#serachUrl}"
	formId="searchTask" 
	preload="true" 
	executeScripts="true"
	parseContent="false"
	separateScripts="false"
	indicator="loadImage"
	showLoadingText="false"
	errorText="Error"
	>Results
	</sx:div>
</body>
</html>