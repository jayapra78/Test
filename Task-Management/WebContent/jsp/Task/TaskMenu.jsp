<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<sj:head compressed="false" />
<s:head theme="simple" />
<%
	/* Getting Context Path */
	String contextPath = request.getContextPath();
String taskDisplayMsg = (String)request.getAttribute("taskDisplayMsg")!=null?(String)request.getAttribute("taskDisplayMsg"):"";
%>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Task Sheet-SRM Technologies</title>

<!--pop-up start Here Created by K.K.Gopinath -->
 <link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/login-page.css" />
 <link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
<!--pop-up End Here -->
<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
    var currentDate = "${currentDateStr}";
	var currentTime = "${currentTime}";
	var sysTime     = parseFloat(currentTime.replace(/:/g,''));
</script>
<style type="text/css">
	body{
		background-color:transparent;
		width:100%;
		margin:0px auto;
	}
	.page {
		margin: 10px;
		width: 600px;
	}
	.main{
		width:640px;
		margin:0px;
		text-align:left;
		float:left;
		background-image:none;
	}
	.login-field-part {
		width: 600px;
		float: left;
		padding: 10px;
		background-color: #fcfcfc;
		border: #C0C0C0 solid 5px;
		margin: 0px 0px 0px 0px;
	}
	#popover {
		height: 324px;
		margin: 0px;
		z-index: 9999;
		width: 600px;
	}
</style> 
</head>

<body>
	<s:form name="saveRecursiveTask" id="saveRecursiveTask" action="saveRecursiveTask"
		method="post" theme="simple">
<div class="main">
    <div class="page">
         <div id="popover">
           <div class="pop-main">
                    <div class="login-field-part">
                            <div class="plan-details-part">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                            	<td class="plan-header">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="plan-header-title">Recursive Task</td>
                                    <td>&nbsp;</td>
                                    <td class="close-btn">
                                    	<a href="#" onclick="parent.modalWindow.close();" class="close-btn" tabindex="9">Close</a>
                                    </td>
                                  </tr>
                                </table>
                                </td>
                            </tr>
                            <tr>
                            	<td>
		                            <div class="error-msg-popup" id="errorMessageDiv">&nbsp;
										<s:if test="hasActionErrors()">
											<s:actionerror cssClass="error" id="errorId"/>
										</s:if>
									</div>
								</td>
							</tr>
					
					
                              <tr>
                                <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td style="width:5px;">&nbsp;</td>
                                    <td class="plan-field-name" style="width:80px;" valign="top">Start Date* :</td>
                                    <td  style="width:140px;" nowrap="nowrap">
                                    	<sj:datepicker onchange="isValidDate(this);"
										displayFormat="dd/mm/yy" tabindex="1" id="startDateStrId"
										cssStyle="width:100px;" onChangeTopics="onChangeDate"
										maxlength="20" buttonImageOnly="true"
										name="recursiveTaskActionVo.taskStartDateStr" readonly="false" value="%{recursiveTaskActionVo.taskStartDateStr}"
										onpaste="return false;"/>
									</td>
                                    <td class="select-icon" style="width:20px;">&nbsp;</td>
                                    <td style="width:20px;">&nbsp;</td>
                                    <td class="plan-field-name" style="width:120px;" valign="top">End Date* :</td>
                                    <td  style="width:140px" nowrap="nowrap">
                                    	<sj:datepicker onchange="isValidDate(this);"
										displayFormat="dd/mm/yy" tabindex="2" id="endDateStrId"
										cssStyle="width:100px;" cssClass="txt" buttonImageOnly="true"
										onChangeTopics="onChangeDate" maxlength="20"
										name="recursiveTaskActionVo.taskEndDateStr" readonly="false" value="%{recursiveTaskActionVo.taskEndDateStr}"
										onpaste="return true;"/>
									</td>
                                    <td class="select-icon" style="width:20px;">&nbsp;</td>
                                    <td style="width:20px;">&nbsp;</td>
                                  </tr>
                                </table>
                                </td>
                              </tr>
                              <tr>
                                <td style="background-color: #DDE4E8;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td style="width:5px;">&nbsp;</td>
                                    <td class="plan-field-name" style="width:80px;" valign="top">Start Time* :</td>
                                    <td style="width:140px;">
                                    	<s:select tabindex="3"
										name="recursiveTaskActionVo.planStartTime" list="timeList"
										listKey="key" listValue="name" value="%{recursiveTaskActionVo.planStartTime}"
										cssStyle="width:105px; " />
									</td>
                                    <td class="select-icon" style="width:20px;">&nbsp;</td>
                                    <td style="width:20px;">&nbsp;</td>
                                    <td class="plan-field-name" style="width:120px;" valign="top">End Time* :</td>
                                    <td style="width:140px;">
                                    	<s:select
										name="recursiveTaskActionVo.planEndTime" list="timeList" tabindex="4"
										listKey="key" listValue="name" value="%{recursiveTaskActionVo.planEndTime}"
										cssStyle="width:105px;" />
									</td>
									<td class="select-icon" style="width:20px;">&nbsp;</td>
                                    <td style="width:20px;">&nbsp;</td>
                                  </tr>
                                </table>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td style="width:5px;">&nbsp;</td>
                                    <td class="plan-field-name" style="width:80px;" valign="top">Day* :</td>
                                    <td style="width:140px;">
                                        <s:select multiple="true" name="recursiveTaskActionVo.taskDay" list="dayList" listKey="key" tabindex="5"
										listValue="name" value="%{recursiveTaskActionVo.taskDay}" cssClass="list-box"  cssStyle="width:105px;" />
                                        </td>
                                    <td class="select-icon" style="width:20px;">&nbsp;</td>
                                    <td style="width:20px;">&nbsp;</td>
                                    <td class="plan-field-name" style="width:120px;" nowrap="nowrap" valign="top">Task Description* :</td>
                                    <td style="width:140px;">
                                    	<s:textarea rows="2" cols="10" tabindex="6"
										name="recursiveTaskActionVo.planDesc"
										id="planDesc"
										style="width: 120px; height: 33px; resize: none;  overflow: auto;"></s:textarea></td>
                                    <td class="select-icon" style="width:20px;">&nbsp;</td>
                                    <td style="width:20px;">&nbsp;</td>
                                  </tr>
                                </table>
                                </td>
                              </tr>
                            </table>
                            </div>
                            <div class="plan-btns-part">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td style="width:39%;"></td>
                                <td class="save-btn" style="width:10%;height:35px">
	                                <a href="#" title="Plan" tabindex="7">
				                      	<img src="<%=contextPath%>/images/plan-btn.png" align="middle" border="none" alt="Save" style="height:35px;width:55px"
				                      	id="submitButton" onclick="return funFormSubmit();"
				                      	/>
				                    </a>
                                	<%-- <input onclick="return funFormSubmit();" type="image" src="<%=contextPath%>/images/plan-btn.png" style="height:35px" title="Plan" alt="Button" tabindex="7" id="submitButton"/> --%>
                                </td>
                                <td style="width:3%;"></td>
                                <td class="save-btn" style="width:10%;height:35px">
                                	<input onclick="javascript:parent.modalWindow.close();" type="image" src="<%=contextPath%>/images/cancel-btn-new.png" style="height:35px" title="Cancel" alt="Cancel" tabindex="8"/>
                                </td>
                                <td style="width:38%;"></td>
                              </tr>
                            </table>
                            </div>
                    </div>
            </div>        
    </div>
</div>
</div>
</s:form>
<script>

	window.onload = function(){
		document.getElementById("startDateStrId").maxLength = "10";
		document.getElementById("endDateStrId").maxLength = "10";
		var len = document.getElementById("errorMessageDiv").getElementsByTagName("li").length;
		if(len==1){
			document.getElementById('popover').style.height= 345+"px";
		}
		document.getElementById("startDateStrId").value = '${recursiveTaskActionVo.taskStartDateStr}';
		document.getElementById("endDateStrId").value = '${recursiveTaskActionVo.taskEndDateStr}';
	}
	
	function updatePopupHeight(){
		var len = document.getElementById("errorMessageDiv").getElementsByTagName("li").length;
		document.getElementById('popover').style.height= (324+len*5+(len>1?(len-1)*11:0))+"px";
	}
	
	var isMultiSubmit = false;
	var jsDisplayMsg = '<%=taskDisplayMsg%>';
	 
	
	 
 	if (jsDisplayMsg != null && jsDisplayMsg != '') {
 		parent.document.getElementById("taskDisplayMsg").value =jsDisplayMsg;
 		
 		parent.funRefreshParent();
 		parent.modalWindow.close();
	}
	 
 	function dateValidation(){
		
 		var curDate     = currentDate.split("/");
		curDate = new Date(curDate[2]+"-"+curDate[1]+"-"+curDate[0]);
		
		var startDate = document.getElementById("startDateStrId");
		var endDate = document.getElementById("endDateStrId");
		
		if(startDate.value=="" || endDate.value==""){
			return true;
		}
		else if(!isValidDate(startDate)){
			alert("Start Date is invalid. (Format - dd/mm/yyyy)");
			return false;
		}
		else if(!isValidDate(endDate)){
			alert("End Date is invalid. (Format - dd/mm/yyyy)");
			return false;
		}
		
		var oneYearDate = currentDate.split("/");
		oneYearDate = new Date((parseInt(oneYearDate[2])+1)+"-"+oneYearDate[1]+"-"+oneYearDate[0]);
		
		startDate = startDate.value;
		startDate = startDate.split("/");
		startDate = new Date(startDate[2]+"-"+startDate[1]+"-"+startDate[0]);
		
		endDate = endDate.value;
		endDate = endDate.split("/");
		endDate = new Date(endDate[2]+"-"+endDate[1]+"-"+endDate[0]);
		
		if(curDate.getTime()>=startDate.getTime()){
			alert("Start Date should be greater than the Current Date");
			return false;
		}
		else if (startDate.getTime() > endDate.getTime()) {
			alert("End Date should be greater or equal to the Start Date");
			return false;
		}
		else if (oneYearDate.getTime() < endDate.getTime()) {
			alert("End Date should be less than the one year");
			return false;
		}
		
		var startTime = parseFloat(document.getElementById("saveRecursiveTask_recursiveTaskActionVo_planStartTime").value.replace(/:/,''));
		var endTime   = parseFloat(document.getElementById("saveRecursiveTask_recursiveTaskActionVo_planEndTime").value.replace(/:/,''));
		if(startDate.getTime() != endDate.getTime() && (startTime==0 || endTime==0)){
			return true;
		}
		else if(startTime>=endTime){
			alert("End time should be greater than start time");
			return false;
		}
		return true;
	}
 	function fieldValidation(){
 		var errMsg = "";
 		var isInvalid = false;
 		if(document.getElementById("startDateStrId").value==""){
 			errMsg += parseErrorMsg("Start date is required");
 			isInvalid = true;
 		}
		if(document.getElementById("endDateStrId").value==""){
			errMsg += parseErrorMsg("End date is required");
 			isInvalid = true;
		}
		if(document.getElementById("saveRecursiveTask_recursiveTaskActionVo_planStartTime").value==""){
			errMsg += parseErrorMsg("Start time is required");
 			isInvalid = true;
		}
		if(document.getElementById("saveRecursiveTask_recursiveTaskActionVo_planEndTime").value==""){
			errMsg += parseErrorMsg("End time is required");
 			isInvalid = true;
		}
		
		if(document.getElementById("saveRecursiveTask_recursiveTaskActionVo_taskDay").selectedIndex==-1){
			errMsg += parseErrorMsg("TaskDay is required");
 			isInvalid = true;
		}
		if(document.getElementById("planDesc").value.replace(/ /g,'')==""){
			errMsg += parseErrorMsg("Task Description is required");
 			isInvalid = true;
		}
 		
		if(isInvalid){
			showErrorMsg(errMsg);
			return false;
		}
		else{
			return true;
		}
 	}
 	var submitButtonDisabled = false; 
	 function funFormSubmit(){
		 <c:if test="${sessionScope.user.guest eq false}">
			 if(!submitButtonDisabled){
				 submitButtonDisabled = true;
				 if(dateValidation() && fieldValidation()){
					 document.getElementById("saveRecursiveTask").submit();
					 return true;
				 }
				 else{
					 submitButtonDisabled = false;
					 return false;
				 }
			 }
			 return false;
	 	</c:if>
	 	
	 }
	 var millisecondsToWait = 2000;
		function timeOutSet(){
			  setTimeout(function() {
				 isMultiSubmit = false;
			 }, millisecondsToWait); }
			 
		
	function targetopener(mylink, closeme, closeonly){
		if (! (window.focus && window.opener))return true;
		window.opener.focus();
		if (! closeonly)window.opener.location.href=mylink.href;
		if (closeme)window.close();
		return false;
	}
	function parseErrorMsg(msg){
		return "<li><span>" + msg + "</span></li>";
	}
	function showErrorMsg(err){
		var msg = "<ul id=\"addTask_\" class=\"error\">";
        msg += err;
        msg += "</ul>";
        document.getElementById("errorMessageDiv").innerHTML = msg;
        updatePopupHeight();
	}
	function isValidDate(date){
		var matches = /^(\d{2}|\d{1})[\/\/](\d{2}|\d{1})[\/\/](\d{4})$/.exec(date.value);
	    if (matches == null) return false;
	    var d = matches[1];
	    var m = matches[2] - 1;
	    var y = matches[3];
	    var composedDate = new Date(y, m, d);
	    var result = composedDate.getDate() == d &&
	           composedDate.getMonth() == m &&
	           composedDate.getFullYear() == y &&
	           (parseInt(y)>=2000);
	    if(result){
	    	if(d.length==1) d="0"+d;
	    	if(m<10) m="0"+(m+1);
	    	else m=(m+1);
	    	date.value = d+"/"+m+"/"+y;
	    	return true;
	    }
	    else{
	    	return false;
	    }
	}
	</script> 
</body>
</html>
