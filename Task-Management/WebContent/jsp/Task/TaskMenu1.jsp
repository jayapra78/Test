<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	/* Getting Context Path */
	String contextPath = request.getContextPath();
%>

<script type="text/javascript" src="<%=contextPath%>/js/TAS.js"></script>


<script type="text/javascript" src="<%=contextPath%>/js/Shirai.js"></script>
<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Task Sheet-SRM Technologies</title>

<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/error.css" />
<link href="<%=contextPath%>/css/style.css" type="text/css"
	rel="stylesheet" />
<style type="text/css">
.button_style {
	background-color: black;
	color: white;
	cursor: pointer;
}

.logout_style {
	background: transparent;
	color: white;
	cursor: pointer;
}
</style>


<style>
.title {
	background-color: #81BEF7;
	width: 100%;
	text-align: center;
	font-size: 16px;
	font-weight: bold;
}

.tr_heading {
	background-color: #CEE3F6;
}

.td_content {
	text-align: center;
}

.txt {
	BORDER-RIGHT: #66ccff 1px solid;
	BORDER-TOP: #66ccff 1px solid;
	BORDER-LEFT: #66ccff 1px solid;
	BORDER-BOTTOM: #66ccff 1px solid;
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;
	FONT-WEIGHT: normal;
	FONT-SIZE: 11px;
	BACKGROUND-COLOR: #ffffff;
	COLOR: #000000;
}

.dropdown {
	BORDER-RIGHT: #66ccff 1px solid;
	BORDER-TOP: #66ccff 1px solid;
	BORDER-LEFT: #66ccff 1px solid;
	BORDER-BOTTOM: #66ccff 1px solid;
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;
	FONT-WEIGHT: normal;
	FONT-SIZE: 11px;
	BACKGROUND-COLOR: #ffffff;
	COLOR: #000000;
}

.tdLabel {
	padding: 0;
	width: 0;
}

.labe1 {
	BORDER-RIGHT: #66ccff 1px solid;
	BORDER-TOP: #66ccff 1px solid;
	BORDER-LEFT: #66ccff 1px solid;
	BORDER-BOTTOM: #66ccff 1px solid;
	/*FONT-SIZE: 11px;
	    FONT-STYLE: normal;
	    FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;
	    TEXT-ALIGN: justify;
	    COLOR: #66ccff;*/
}

.table {
	white-space: normal;
	line-height: normal;
	font-weight: normal;
	font-size: medium;
	font-variant: normal;
	font-style: normal;
	color: -webkit-text;
	text-align: start;
	border-spacing: 2px;
	border-color: gray;
	border-collapse: separate;
}

p {
	display: block;
	-webkit-margin-before: 1em;
	-webkit-margin-after: 1em;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 0px;
}

datepick {
	BORDER-RIGHT: #66ccff 1px solid;
	BORDER-TOP: #66ccff 1px solid;
	BORDER-LEFT: #66ccff 1px solid;
	BORDER-BOTTOM: #66ccff 1px solid;
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;
	FONT-WEIGHT: normal;
	FONT-SIZE: 11px;
	BACKGROUND-COLOR: #ffffff;
	COLOR: #000000;
}
</style>

<head>
<sj:head compressed="false" />
<sx:head debug="false" />
<s:head theme="simple" />
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/error.css" />
</head>
<body class=lbody>
	<table style="width: 100%">
		<tr>
			<td class="title">RECURSIVE TASK</td>
		</tr>
	</table>

	<!-- Search Start -->

	<script>
	
	var projectAry = new Array();
    <s:iterator value="projectList" status="IteratorStatus">
    	projectAry["${key}"] = "${idValue}";
    </s:iterator>
    
    var activityAry = new Array();
    <s:iterator value="activityList" status="IteratorStatus">
    	activityAry["${key}"] = "${idValue}";
    </s:iterator>
	
	function openProjectPopup(projectId,projectCodeId){
		newwindow=window.open("projectList?projectListId="+projectId+"&projectCodeId="+projectCodeId,'newwindow','scrollbars=yes,menubar=0,height=300,width=500,left=200,top=200');
	}
	
	function openActivityPopup(projectId,projectCodeId){
		newwindow=window.open("activityList?projectListId="+projectId+"&projectCodeId="+projectCodeId,'newwindow','scrollbars=yes,menubar=0,height=300,width=500,left=200,top=200');
	}
	
	function searchValidation(){
		var projectId = document.getElementById("projectId").value;
		if(projectAry[projectId]){
			document.getElementById("projectCodeId").value = projectAry[projectId];
		}
		return true;
	}
	
	function validateProjectAndActivity(){
		var rows = parseInt(document.getElementById("rows").value);
		var isInvalid = false;
		var errMsg = "";
		
		/* if(document.getElementById("taskDate").value == ""){
			errMsg += parseErrorMsg("Date is required");
			isInvalid = true;
		} */
		
		for(var row=0;row<rows;row++){
			if(document.getElementById("projectCode_"+row)){
				var projectCode = document.getElementById("projectCode_"+row).value;
				var projectId   = document.getElementById("project_"+row);
				projectId.value = "";
				if(projectAry[projectCode]){
					projectId.value = projectAry[projectCode];
				}
				else if(projectCode!=""){
					errMsg += parseErrorMsg("Project code is invalid for row "+(row+1));
					isInvalid = true;
				}
				
				/*var planStartTime = parseInt(document.getElementById("addTask_task_"+row+"__planStartTime").value.replace(\:\g,''));
				var planEndTime   = parseInt(document.getElementById("addTask_task_"+row+"__planEndTime").value.replace(\:\g,''));
				
				if(planStartTime>planEndTime){
					
				}*/
				
				var activityCode = document.getElementById("activityCode_"+row).value;
				var activityId   = document.getElementById("activityId_"+row);
				activityId.value="";
				if(activityAry[activityCode]){
					activityId.value = activityAry[activityCode];
				}
				else if(activityCode!=""){
					errMsg += parseErrorMsg("Activity code is invalid for row "+(row+1));
					isInvalid = true;
				}
			}
		}
		if(isInvalid){
			showErrorMsg(errMsg);
			return false;
		}
		else{
			return true;
		}
		
	}
	
	function parseErrorMsg(msg){
		return "<li><span>" + msg + "</span></li>";
	}
	function showErrorMsg(err){
		var msg = "<ul id=\"addTask_\" class=\"error\">";
        msg += err;
        msg += "</ul>";
        document.getElementById("errorMessageDiv").innerHTML = msg;
	}
	
</script>

	<s:form name="searchTask" id="searchTask" action="searchTask"
		method="post" theme="simple">
		
		<div class="error-msg">&nbsp;
		        <table width="100%">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								align="left">
								<tr>
									<td nowrap="nowrap" align="left">
										<div id="errorMessageDiv">
											<s:if test="hasActionErrors()">
												<s:actionerror cssClass="error" />
											</s:if>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									<td nowrap="nowrap" align="center">
										<div id="successMessageDiv">
											<s:if test="hasActionMessages()">
												<s:actionmessage cssClass="success" />
											</s:if>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		<table width="325px" border="0" cellspacing="2" cellpadding="0">
			<tr>
				<td class="emp-details-field-name">Start Date :</td>
				<td colspan="2" nowrap="nowrap"><sj:datepicker
						displayFormat="dd/mm/yy" tabindex="4" id="startDateStrId" value=""
						cssStyle="width:100px;" onChangeTopics="onChangeDate"
						maxlength="20" buttonImageOnly="true"
						name="recursiveTaskActionVo.taskStartDateStr" readonly="false"
						onpaste="return false;" /></td>
						<td class="emp-details-field-name">End Date :</td>
				<td colspan="2" nowrap="nowrap"><sj:datepicker
						displayFormat="dd/mm/yy" tabindex="5" id="endDateStrId"
						cssStyle="width:100px;" cssClass="txt" buttonImageOnly="true"
						onChangeTopics="onChangeDate" maxlength="20"
						name="recursiveTaskActionVo.taskEndDateStr" readonly="false" value="%{recursiveTaskActionVo.taskEndDateStr}"
						onpaste="return false;" /></td>
				
			</tr>
			<tr>
			<td class="emp-details-field-name">Start Time :</td>
				<td colspan="2" nowrap="nowrap"><s:select
						name="recursiveTaskActionVo.planStartTime" list="timeList"
						listKey="key" listValue="name" value="planStartTime"
						cssClass="list-box"
						cssStyle="width:60px; " /></td>
				
				<td class="emp-details-field-name">End Time :</td>
				<td colspan="2" nowrap="nowrap"><s:select
						name="recursiveTaskActionVo.planEndTime" list="timeList"
						listKey="key" listValue="name" value="planStartTime"
						cssClass="list-box"
						cssStyle="width:60px;" /></td>
			</tr>
			<tr>
				<td class="emp-details-field-name">Week Days</td>
				<td colspan="2" nowrap="nowrap"><s:select
						name="recursiveTaskActionVo.taskDay" list="dayList" listKey="key"
						listValue="name" value="planStartTime" cssClass="list-box"
						  /></td>

				<td class="emp-details-field-name">Plan Description</td>
				<td ><textarea rows="2" cols="10"
						name="recursiveTaskActionVo.planDesc"
						id="recursiveTaskActionVo.planDesc"
						style="width: 120px; height: 33px; resize: none;  overflow: auto;"></textarea>
				</td>
			</tr>

		</table>
			<table width="325px" border="0" cellspacing="0" cellpadding="0" align="center">
			
			<tr>
			
				<td class="save-btn" >
						<input type="image" name="saveId" id="saveId" tabindex="1002" title="Save"
							   src="<%=contextPath%>/images/save-btn-new.png"
							   onclick="funFormSubmit(document.forms[0],'saveRecursiveTask');"/>
	            </td>
	            
				<td colspan="2" nowrap="nowrap"></td>

				
				
			</tr>
			</table>

		
	</s:form>
	
	<script>
	 var isMultiSubmit = false;
	 function funFormSubmit(obj,action)
	   { 
		 timeOutSet();
		   if(isMultiSubmit==false){
	   		 isMultiSubmit = true;
	   		 funSubmitForm(obj,action);
	   		}
	   }
	 var millisecondsToWait = 2000;
		function timeOutSet(){
			  setTimeout(function() {
				 isMultiSubmit = false;
			 }, millisecondsToWait); }
			 
		 </script> 
	<%-- 
	
		 function validatedate(smessage, emessage,title,button,dateMatch, startDateMsg, endDateMsg, startDateReq, endDateReq) {
		    	
		    	var startDate = document.getElementById("startDateStrId").value;
		    	var endDate = document.getElementById("endDateStrId").value;
		    	if(startDate!="" && endDate!=""){
		    	if(dateValidation(smessage,startDateMsg, title,button, startDate)  && dateValidation(emessage,endDateMsg,title,button, endDate) && checkMonth(startDateMsg,title,button, startDate) && checkMonth(endDateMsg,title,button, endDate)){
		    		
		    		var startdateStr = startDate.split("/");
		    		var enddateStr = endDate.split("/");
		    		
		    		var stdate=new Date();
		    		stdate.setFullYear(startdateStr[0],(startdateStr[1]-1),startdateStr[2]);
		    		
		    		var endate=new Date();
		    		endate.setFullYear(enddateStr[0],(enddateStr[1]-1),enddateStr[2]);
		    		
		    		if (endate >= stdate)
		    		  {
		    			funSubmitForm(document.forms[0],'saveInformation');
		    		  }
		    		else
		    		  {
		    			jAlert(dateMatch,title,'',button);
		    		  }
			        }
		    	} else if(startDate==""){
		    		jAlert(startDateReq,title,'',button);
		    	}else if(endDate==""){
		    		jAlert(endDateReq,title,'',button);
		    	}
		    }
		    
		    function dateValidation(message, selectMsg, title,button, date){
		    	if(date!=""){
		    		var startdateStr = date.split("/");
		        	var pattern = /^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.]([0-9][0-9])$/;   //defining pattern for date
		       		var datePattern = 	/^(0[1-9]|[12][0-9]|3[01])$/; 
		        if (pattern.test(date)) {
		        	if (datePattern.test(startdateStr[2])) {
			            return true;
			        }else{
			        	jAlert(selectMsg,title,'',button);
			        	return false;
			        }
		        } 
		        else
		        	{
		        	jAlert(message,title,'',button);
		        	return false;
		        	}
		        }
		    	
		    }
		    
		    
		    function checkleapyear(datea)
		    {
		           datea = parseInt(datea);
		            if(datea%4 == 0)
		            {
		                    if(datea%100 != 0)
		                    {
		                            return true;
		                    }
		                    else
		                    {
		                            if(datea%400 == 0)
		                                    return true;
		                            else
		                                    return false;
		                    }
		            }
		    return false;
		    }
		    
		    function checkMonth(message,title,button, date){
		    	var startdateStr = date.split("/");
		    	if(((startdateStr[1]-1)==1)) {
					if (checkleapyear(startdateStr[0])){
		    			if(startdateStr[2]>29){
		    				jAlert(message,title,'',button);
		    				return false;
		    			}
		    		}else{
						if(startdateStr[2]>28){
							jAlert(message,title,'',button);
							return false;
		    			}
		    		}
	    		}else if(((startdateStr[1]-1)==0) || ((startdateStr[1]-1)==2) || ((startdateStr[1]-1)==4) || ((startdateStr[1]-1)==6) || ((startdateStr[1]-1)==7) || ((startdateStr[1]-9)==0) || ((startdateStr[1]-1)==11)){
	    			if(startdateStr[2]>31){
	    				jAlert(message,title,'',button);
	    				return false;
	    			}
	    		}else{
	    			if(startdateStr[2]>30){
	    				jAlert(message,title,'',button);
	    				return false;
	    			}
	    		}
		    	 return true;
		    }
		    </script>	 --%>	    

</body>
