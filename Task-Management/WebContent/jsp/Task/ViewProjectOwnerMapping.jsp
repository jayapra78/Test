<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
			response.setLocale(java.util.Locale.ENGLISH);
%>
<%
    /* Getting Context Path */
			String contextPath = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
window.history.forward();
function noBack() { window.history.forward(); }
var conPath = "<%=contextPath%>";
/*function closeWindow(){
	document.saveProjectOwnerMapping.action = conPath + "/app/listProjectOwnerMapping";
	document.saveProjectOwnerMapping.submit();
}*/

function closeWindow(){
	document.updateProjectOwnerMapping.action = conPath + "/app/listProjectOwnerMapping";
	return true;
}
</script>
<link rel="shortcut icon" href="<%=contextPath%>/images/favicon1.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Project Owner Mapping-SRM Technologies</title>
	<sj:head compressed="false" />
	<sx:head debug="false" />
	<s:head theme="simple" />
	<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/shirai.css" />
	<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/error.css" />
	<link href="<%=contextPath%>/css/style.css" type="text/css" rel="stylesheet" />
<style type="text/css">
	.button_style {
		background-color: black;
		color: white;
		cursor: pointer;
	}

	.logout_style {
		background: transparent;
		color: white;
		cursor: pointer;
	}
	.footer-part{
		position:absolute;
		top:600px;
		margin-left: auto;
        margin-right: auto;
		width:1000px;
		height:30px;
		background-color:#2d4a59;
		line-height:30px;
		font-family:Arial, verdena, sans-serif;
		font-size:11px;
		color:#fff;
		text-align:center;
		float:left;
	}

	fieldset { border:1px solid #66CCFF }

	legend {
	  padding: 0.2em 0.5em;
	  text-align:left;
	  font-weight: bold;
	  font-family: sans-serif;
	  font-size: 14px;
	  color: #0000FF;
	  }

</style>

</head>

<body style="background-color:#DDE4E8;">
	<div class="main">
		<div class="page">
			<div class="top-header">
				<div class="title-heading">
					<div class="plan-lead-logo">
						<img src="<%=contextPath%>/images/plan-lead-logo.png" width="210"
							height="65" border="0" alt="Logo" title="Plan-Lead" />
					</div>
					<div class="tech-trends-logo">
						<img src="<%=contextPath%>/images/tech-trends-logo.png"
							width="195" height="65" border="0" alt="Logo" title="Tech-Trend" />
					</div>
					<div class="logout">
						<a href="logOut">Logout</a>
					</div>

				</div>
			</div>
			<div class="emp-details-part">
			<s:form name="saveProjectOwnerMapping" id="saveProjectOwnerMapping" action="saveProjectOwnerMapping" validate="true" method="POST" theme="simple">
			<div class="main_heading">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="33%">&nbsp;</td>
						<td width="34%">View Project</td>
						<td width="33%"> <s:include value="/jsp/menu/MainMenu.jsp"></s:include></td>
					</tr>
				</table>
			</div>

					<table width="100%" border="0" cellspacing="5" cellpadding="0" style="background-color:#DDE4E8;">

						<tr><td height="20%">&nbsp;</td></tr>

						<tr id="ProjectType">
							<td width="50%" class="project-mapping-label" align="right">
									Project Type &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:select name="projectOwnerMappingActionVo.projectTypeId" list="projectTypeVoList" listKey="projectTypeId" listValue="projectType"
	                    					 cssClass="list-box" style="width:212px" onchange="resetSearchProject();" disabled="true"/>
							</td>
						</tr>

						<tr id="ProjectCode">
							<td width="50%" class="project-mapping-label" align="right">
									Project Code &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:hidden name="projectOwnerSearchVo.projectId" id="projectId"/>
	                    		<s:textfield name="projectOwnerMappingActionVo.projectCode" value="%{projectOwnerMappingActionVo.projectCode}" id="projectCodeId" size="30" style="width:208px" tabindex="2" disabled="true"/>
							</td>
						</tr>

						<tr id="ProjectName">
							<td width="50%" class="project-mapping-label" align="right">
									Project Name &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:textfield name="projectOwnerMappingActionVo.projectName" tabindex="3" id="PNameTextBoxID" value="%{projectOwnerMappingActionVo.projectName}" size="30" style="width:208px" disabled="true"/>
							</td>
						</tr>
						<tr id="ProjectStatusName">
							<td width="50%" class="project-mapping-label" align="right">
									Project Status &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:textfield name="projectOwnerMappingActionVo.projectStatusActionVo.statusName" size="30" style="width:208px" disabled="true"/>
							</td>
						</tr>
						<tr id="ProjectManagerName">
							<td width="50%" class="project-mapping-label" align="right">
									Project Manager &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:select headerKey="-1" headerValue="select" name="projectOwnerMappingActionVo.pmId" list="pmNameList" listKey="idValue" listValue="name"
	                    					 cssClass="list-box" style="width:212px" disabled="true"/>
							</td>
						</tr>

						<tr id="ProjectManagerEmail">
							<td width="50%" class="project-mapping-label" align="right">
									Project Manager Email &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:textfield name="projectOwnerMappingActionVo.pmEmailId" value="%{projectOwnerMappingActionVo.pmEmailId}" tabindex="5" id="PMEmailTextID" size="30" style="width:208px" disabled="true"/>
							</td>
						</tr>

						<tr id="ProjectLeaderName">
							<td width="50%" class="project-mapping-label" align="right">
									Project Leader &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:select headerKey="-1" headerValue="select" name="projectOwnerMappingActionVo.plId" list="plNameList" listKey="idValue" listValue="name"
	                    					 cssClass="list-box" style="width:212px" disabled="true"/>
							</td>
						</tr>

						<tr id="ProjectLeaderEmail">
							<td width="50%" class="project-mapping-label" align="right">
									Project Leader Email &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:textfield name="projectOwnerMappingActionVo.plEmailId" value="%{projectOwnerMappingActionVo.plEmailId}" tabindex="7" id="PLEmailTextID" size="30" style="width:208px" disabled="true"/>
							</td>
						</tr>

						<tr id="ProjectLeaderEmail">
							<td width="50%" class="project-mapping-label" align="right">
									Associated Project &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:select name="projectOwnerMappingActionVo.associatedProjectId" id="associatedProjectId" list="associatedProjectList" disabled="true"
								listKey="idValue" listValue="name" headerKey="0" headerValue="Select" style="width:212px" tabindex="8"/>
								<%-- <s:textfield name="projectOwnerMappingActionVo.plEmailId" value="%{projectOwnerMappingActionVo.plEmailId}" tabindex="7" id="PLEmailTextID" size="30" style="width:208px" disabled="true"/> --%>
							</td>
						</tr>

						<tr id="ProjectLeaderEmail">
							<td width="50%" class="project-mapping-label" align="right">
									Associated From &nbsp;:&nbsp;
							</td>
							<td class="project-mapping-controls">
								<s:textfield name="projectOwnerMappingActionVo.associatedFromStr" value="%{projectOwnerMappingActionVo.associatedFromStr}" tabindex="7" id="PLEmailTextID" size="30" style="width:208px" disabled="true"/>
							</td>
						</tr>

						<%-- <tr>
							<td colspan="2" style="width: 600px;" align="center">
								<fieldset style="width: 510px;">
									<legend>Customer Detail:</legend>
									<table width="100%" align="center" border="0" cellpadding=0 cellspacing=0>
										<tr>
											<td colspan="2" style="width: 50%; text-align: right;"> Customer Name&nbsp;:&nbsp;</td>
											<td colspan="2" style="width: 50%;"> <s:textfield disabled="true" tabindex="9" id="customerName" name="projectOwnerMappingActionVo.customerName" size="30" maxlength="200"  style="width:300px" placeholder="If more than one please enter coma seperated"/></td>
										</tr>
										<tr><td colspan="4">&nbsp;</td></tr>
										<tr>
											<td colspan="2" style="width: 50%; text-align: right;"> Customer Email&nbsp;:&nbsp;</td>
											<td colspan="2" style="width: 50%;"> <s:textfield disabled="true" tabindex="10" id="customerEmail" name="projectOwnerMappingActionVo.customerEmailId" size="30" maxlength="200"  style="width:300px" placeholder="If more than one please enter coma seperated"/></td>
										</tr>
										<tr><td colspan="4">&nbsp;</td></tr>
										<tr>
											<td style="width: 42%; text-align: right;">Status Mail&nbsp;:&nbsp;</td>
											<td style="width: 8%; text-align: center;"><s:checkbox disabled="true" tabindex="11" id="statusMail" name="projectOwnerMappingActionVo.statusMail"/> </td>
											<td style="width: 25%; text-align: right;">Consolidated Mail&nbsp;:&nbsp;</td>
											<td style="width: 25%;">&nbsp;&nbsp;<s:checkbox disabled="true" tabindex="12" id="consolidatedMail" name="projectOwnerMappingActionVo.consolidatedMail" /> </td>
										</tr>
									</table>
								</fieldset>
							</td>
						</tr> --%>

						<tr><td height="20%">&nbsp;</td></tr>
					</table>

					<table style="background-color:#DDE4E8;" width="100%" border="0" cellspacing="0" cellpadding="0">
	          			<tr>
				            <td style="width:45%;"></td>

							<td class="save-btn" style="width:10%;" align="center">
								<a tabindex="9" href="<%=contextPath%>/app/listProjectOwnerMapping">
									<img src="<%=contextPath%>/images/cancel-btn-new.png" border="none" style="cursor: pointer;"/>
								</a>
				            </td>
				            <td style="width:45%;"></td>
	          			</tr>
	        		</table>
	        		<div class="footer-part">Copyright © SRM Technologies Private Limited </div>
				</s:form>
			</div>
		</div>
	</div>
</body>
</html>
