<%@page import="java.io.File" %>
<%!
	public String getHomeDirectory(){
    	String userDir = System.getProperty("user.dir");
    	userDir = userDir.replaceAll(File.separator+File.separator+"bin", "");
    	return userDir;
    }
	public String getReportDirectory(){
		return getHomeDirectory()+File.separator+"webapps"+File.separator+"Task"+File.separator+"report";
	}
%>
User directory = <%=System.getProperty("user.dir")%>
<br>
Home directory = <%=getHomeDirectory()%>
<br>
ReportDirectory = <%=getReportDirectory()+":"+new File(getReportDirectory()).exists()%>
<br>
Separator = <%=File.separator%>
<br>
