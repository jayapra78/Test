<%--
 -Copyright(C) 2012 Shirai Group, Tokyo, Japan All rights reserved.
 --%>
<!--  CLEARING THE CACHE -->
<%
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader("Expires", 0);
%>
<!--  CLEARING THE CACHE -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<html>
<head>
<%
	/* Getting Context Path */
	String contextPath = null;
	contextPath = request.getContextPath();
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/css/home.css" />
<script language='javascript' type='text/javascript' src="<%=contextPath%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=contextPath%>/js/curvy.corners.trunk.js"></script>
<script type="text/javascript" src="<%=contextPath%>/js/animatedcollapse.js"></script>
<title><s:text name="SHIRAI" /></title>
<script type="text/javascript">

animatedcollapse.addDiv('cat', 'fade=1,speed=400,group=pets,persist=1,hide=1')
animatedcollapse.addDiv('dog', 'fade=0,speed=400,group=pets,persist=1,hide=1')
animatedcollapse.addDiv('rabbit', 'fade=1,speed=400,group=pets,persist=1,hide=1')

animatedcollapse.ontoggle=function($, divobj, state){ //fires each time a DIV is expanded/contracted
	//$: Access to jQuery
	//divobj: DOM reference to DIV being expanded/ collapsed. Use "divobj.id" to get its ID
	//state: "block" or "none", depending on state
}

animatedcollapse.init()

</script>

</head>
<body>
<s:form name="Error" id="Error" validate="true" theme="simple">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td class="main-bg">
        <table width="60%" align="center" border="0" cellspacing="0" cellpadding="0">
          <tr><td height="10"></td></tr>
          <tr><td height="10"></td></tr>
          <tr>
            <td align="center">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="14"><img src="<%=contextPath%>/images/heading-bg-left.png" alt="" title="" width="14" height="34" /></td>
                    <td class="heading-bg"><s:text name="ERROROCCURED"></s:text></td>
                    <td width="14"><img src="<%=contextPath%>/images/heading-bg-right.png" alt="" title="" width="14" height="34" /></td>
                  </tr>
                </table>            
            </td>
          </tr>
            <td class="spacer"></td>
          <tr>
            <td align="center">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="14"><img src="<%=contextPath%>/images/login-left-top-home.png" alt="" title="" width="14" height="14" /></td>
                    <td class="login-top-home"></td>
                    <td width="14"><img src="<%=contextPath%>/images/login-right-top-home.png" alt="" title="" width="14" height="14" /></td>
                  </tr>
                  <tr>
                    <td class="login-left-home">&nbsp;</td>
                    <td class="error-bg">
                        <table width="100%" border="0" cellspacing="2" cellpadding="0">
                          <tr>
                            <td class="error-title" width="25%"><label><s:text name="ERRORPAGETITLE"></s:text></label>
                           &nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp; 
								<s:property value="errorMsg"/> 
							</td>
                          </tr>
                        </table>
                    </td>
                    <td class="login-right-home">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><img src="<%=contextPath%>/images/login-left-bot-home.png" alt="" title="" width="14" height="14" /></td>
                    <td class="login-bot-home"></td>
                    <td><img src="<%=contextPath%>/images/login-right-bot-home.png" alt="" title="" width="14" height="14" /></td>
                  </tr>
                </table>            
                </td>
          </tr>
          <tr>
            <td class="spacer"></td>
          </tr>
          
          <tr>
          	<td>
            <table width="30%" align="right" border="0" cellspacing="0" cellpadding="0">
              <tr>
            <td>
                <div style="width:100px; float:right;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="9"><img src="<%=contextPath%>/images/btn-left.png" alt="" title="" width="9" height="28" /></td>
                        <td class="btn-bg-home"><a href="login.action"><s:text name="RELOGIN"></s:text></a></td>
                        <td width="14"><img src="<%=contextPath%>/images/btn-right.png" alt="" title="" width="14" height="28" /></td>
                      </tr>
                    </table>
                </div>
            </td>
                <td>
                <s:if test="%{errorDetailMessage !=null}">
                <div style="width:100px; float:right;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><a href="#" rel="toggle[cat]" data-openimage="<%=contextPath%>/images/details-up-btn.png" data-closedimage="<%=contextPath%>/images/details-down-btn.png"><img src="<%=contextPath%>/images/details-down-btn.png" border="0" /></a></td>
                      </tr>
                    </table>
                </div>
                </s:if>
                <s:else>
                 <div style="width:100px; float:right;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><a href="#"><img src="<%=contextPath%>/images/details-down-btn.png" border="0" /></a></td>
                      </tr>
                    </table>
                </div>
                </s:else>
                </td>
              </tr>
            </table>
            
            </td>
          </tr>
          <tr>
          <td height="130px">
          	<div id="cat">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="14"><img src="<%=contextPath%>/images/login-left-top-home.png" alt="" title="" width="14" height="14" /></td>
                <td class="login-top-home"></td>
                <td width="14"><img src="<%=contextPath%>/images/login-right-top-home.png" alt="" title="" width="14" height="14" /></td>
              </tr>
              <tr>
                <td class="login-left-home">&nbsp;</td>
                <td class="error-bg">
			        <div class="error-details">
                    	<s:property value="errorDetailMessage"/> 
                    </div>
                </td>
                <td class="login-right-home">&nbsp;</td>
              </tr>
              <tr>
                <td><img src="<%=contextPath%>/images/login-left-bot-home.png" alt="" title="" width="14" height="14" /></td>
                <td class="login-bot-home"></td>
                <td><img src="<%=contextPath%>/images/login-right-bot-home.png" alt="" title="" width="14" height="14" /></td>
              </tr>
            </table>
            </div>
          </td>
          </tr>
          <s:hidden name="errorMsg" id="errorMsg"></s:hidden>
          <tr>
            <td height="10"></td>
          </tr>
        </table>
    </td>
  </tr>
  </table>
  <script type="text/javascript">
	
	function loginPage(){
		document.getElementById("Error").action="login.action";
		document.getElementById("Error").method="POST";
		document.getElementById("Error").submit();	
	}
  </script>
  </s:form>
</body>
</html>