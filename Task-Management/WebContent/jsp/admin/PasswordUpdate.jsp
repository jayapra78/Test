<%--
 -Copyright(C) 2012 Shirai Group, Tokyo, Japan All rights reserved.
 --%>
 <!--  CLEARING THE CACHE -->
<%
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader("Expires", 0);
%>
<!--  CLEARING THE CACHE -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>
<html>
<head>

<%
	/* Getting Context Path */
	String contextPath = null;
	contextPath = request.getContextPath();
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- Including Struts 2 Jquery Head -->
<title><s:text name="PasswordUpdate"/></title>

<!-- Including Shirai script Page -->
<script type="text/javascript" src="<%=contextPath%>/js/Shirai.js"></script>
<script language='javascript' type='text/javascript'
	src="<%=contextPath%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=contextPath%>/js/Popup.js"></script>
<!-- Including all Css pages -->
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/shirai.css">
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/home.css" />
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/scrolling.css" />
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/dropdown-menus.css" />
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/popup.css" />
	<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/error.css" />
</head>

<body>
	<s:form name="Password" id="Password" validate="true" theme="simple">
		<!-- HiddenValues -->
		<s:hidden name="userSecureId"  id="userSecureId"> </s:hidden>
        <s:hidden name="deleteValue" id="deleteValue"></s:hidden>

		<!-- New Template -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="main-bg">
					<table width="78%" align="center" border="0" cellspacing="0"
						cellpadding="0">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="user-name"><s:text name="USERNAME"/><s:property value="userNameSession"/></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="14"><img
											src="<%=contextPath%>/images/heading-bg-left.png" alt=""
											title="" width="14" height="34" /></td>
										<td class="heading-bg"><s:text
												name="PasswordUpdate"></s:text></td>
										<td width="14"><img
											src="<%=contextPath%>/images/heading-bg-right.png" alt=""
											title="" width="14" height="34" /></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<s:if test="hasActionMessages()">
								<td>
									<table width="50%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td nowrap="nowrap" align="center">
												   <div>
												      <s:actionmessage cssClass="success"/>
												   </div>
											</td>
										</tr>
									</table>
								</td>
							</s:if>
							<s:if test="hasActionErrors()">
								<td>
									<table width="50%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td nowrap="nowrap" align="center">
												   <div>
												      <s:actionerror cssClass="error"/>
												   </div>
											</td>
										</tr>
									</table>
								</td>
							</s:if>
							<s:else>
							<td class="spacer"></td>
							</s:else>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr>
										<td width="14"><img
											src="<%=contextPath%>/images/login-left-top-home.png" alt=""
											title="" width="14" height="14" /></td>
										<td class="login-top-home"></td>
										<td width="14"><img
											src="<%=contextPath%>/images/login-right-top-home.png" alt=""
											title="" width="14" height="14" /></td>
									</tr>
									<tr>
										<td class="login-left-home">&nbsp;</td>
										<td class="login-bg">
											<table width="100%" align="center" border="0" cellspacing="8"
												cellpadding="0">
												<tr>
													<td width="40%">
														<table width="100%" align="left" border="0" cellspacing="0"
															cellpadding="0">
															
															<tr>
																<td width="50%" class="search_text_list_page" nowrap="nowrap"><s:text name="LoginUser"/>&nbsp;:&nbsp;</td>
																<td width="50%">
																	<table border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<th height="28"><div align="left"><s:textfield
																					name="loginName" size="30" theme="simple"
																					maxlength="30" disabled="true" />
																					</div></th></tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												
												<tr>
													<td width="40%">
														<table width="100%" align="left" border="0" cellspacing="0"
															cellpadding="0">
															<tr><td></td><td height="5px" ><div id="OldPassword">
			                          	    					<s:fielderror cssClass="error" fieldName="passwordValue.loginPassword"  theme="simple"/>
			                          						</div></td></tr>
															<tr>
																<td width="50%" class="search_text_list_page" nowrap="nowrap"><s:text name="OldPassword"/><font color="red">*</font>&nbsp;:&nbsp;</td>
																<td width="50%">
																	<table border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<th height="28"><div align="left"><s:password 
																					onchange="removeErrorField('OldPassword')"
																			        tabindex="1" id="loginPass" 
																					name="passwordValue.loginPassword"
																					theme="simple" maxlength="20" size="30"
																					onpaste="return false;" />
																					</div></th>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td width="40%">
														<table width="100%" align="left" border="0" cellspacing="0"
															cellpadding="0">
															<tr><td></td><td height="5px" ><div id="NewPassword">
			                         	    					<s:fielderror cssClass="error" fieldName="passwordValue.newPassword"  theme="simple"/>
			                         						</div></td></tr>
															<tr>
																<td width="50%" class="search_text_list_page" nowrap="nowrap"><s:text name="NewPassword"/><font color="red">*</font>&nbsp;:&nbsp;</td>
																<td width="50%">
																	<table border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<th height="28"><div align="left"><s:password
																					
																					onchange="removeErrorField('NewPassword')"
																			        tabindex="2"
																			        id ="newPassword"
																					name="passwordValue.newPassword"
																					theme="simple" maxlength="20" size="30"
																					onpaste="return false;" />
																					</div></th>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td width="40%">
														<table width="100%" align="left" border="0" cellspacing="0"
															cellpadding="0">
															<tr><td></td><td height="5px" ><div id="ConfirmPassword">
		                    	    							<s:fielderror cssClass="error" fieldName="passwordValue.confirmPassword"  theme="simple"/>
		                    								</div></td></tr>
															<tr>
																<td width="50%" class="search_text_list_page" nowrap="nowrap"><s:text name="ConfirmPassword"/><font color="red">*</font>&nbsp;:&nbsp;</td>
																<td width="50%">
																	<table border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<th height="28"><div align="left"><s:password
																					
																					onchange="removeErrorField('ConfirmPassword')"
																			        tabindex="3"
																			        id="confirmPassword"
																					name="passwordValue.confirmPassword"
																					theme="simple" maxlength="20" size="30"
																					onpaste="return false;" />
																					</div></th>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>

												
											</table>

										</td>
										<td class="login-right-home">&nbsp;</td>
									</tr>
									<tr>
										<td><img src="<%=contextPath%>/images/login-left-bot-home.png"
											alt="" title="" width="14" height="14" /></td>
										<td class="login-bot-home"></td>
										<td><img
											src="<%=contextPath%>/images/login-right-bot-home.png" alt=""
											title="" width="14" height="14" /></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="spacer"></td>
						</tr>

						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td>
								<table width="100px" align="center" border="0" cellspacing="0"
									cellpadding="0">
									<tr>
										  <s:if test="%{authorizationMap.get('PasswordModify') == 1}">
										<td><a tabindex="4" onmouseover="changeButtonFontList('updateId')"
												 onmouseout="javascript:resetClassList('updateId');"
										onblur="javascript:resetClassList('updateId')"
											onfocus="changeButtonFontList('updateId')"
														onclick="javascript:funFormSubmit(document.forms[0],'updatePassword');" 
														onkeypress="if(event.keyCode == 13){funFormSubmit(document.forms[0],'updatePassword');}" 
														style="cursor: pointer; text-decoration: none; outline: none;">
											<table align="center" width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td width="9"><img
														src="<%=contextPath%>/images/btn-left.png" alt="" title=""
														width="9" height="28" /></td>
													<td class="btn-bg-home" id="updateId"> <s:text
																name="Update" /></td>
													<td width="14"><img
														src="<%=contextPath%>/images/btn-right.png" alt=""
														title="" width="14" height="28" /></td>
												</tr>
											</table></a>
											<a onkeyup="setTabIndex();" tabindex="5"/>
										</td>
										</s:if>
										
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="180">
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<script>
		var isMultiSubmit = false;
			function removeErrorField(objValue) {
				var newDiv = document.getElementById(objValue);
				newDiv.innerHTML = "";
			}

			//document.getElementById("loginPass").focus();
			
			window.onload=setTabIndex;
			 function setTabIndex(){
			 	document.getElementById('loginPass').focus();
			 }
			 
			 function funFormSubmit(obj,action)
			   { 
				   if(isMultiSubmit==false){
			   		 isMultiSubmit = true;
			   		 funSubmitForm(obj,action);
			   		}
			   }
			 
			 function changeButtonFontList(controlId){
					document.getElementById(controlId).className="btn-bg-home_focus";
				}

				function resetClassList(controlId){
					document.getElementById(controlId).className="btn-bg-home";
				}	
		</script>

	</s:form>
</body>
</html>
