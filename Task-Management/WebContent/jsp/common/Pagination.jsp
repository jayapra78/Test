<%--
 -Copyright(C) 2012 Shirai Group, Tokyo, Japan All rights reserved.
 --%>
<%@taglib uri="/struts-tags" prefix="s"%>

<style>
.demo{    
		
		margin:10px auto;
		color:#000;
}
.demo a{
	    
		padding:2px 4px 2px 4px;
		margin:10px auto;
		border: 1px solid #fff;
		background-color:#053246;
		color:#fff;
		text-decoration:none;
}
.demo a:hover{
	    
		padding:2px 4px 2px 4px;
		margin:10px auto;
		border: 1px solid #333333;
		background-color:#cccccc;
		color:#000;
		text-decoration:none;
}
.demo1{
	    
		padding:2px 4px 2px 4px;
		margin:10px auto;
		border: 1px solid #fff;
		background-color:#cccccc;
		color:#fff;
		text-decoration:none;
}
</style>
<table  class="main_table">
	<tr align="right">
							<td colspan="2">
								<div align="right">
									<table class="pagination_table">
										<tr class="list_text" align="right">
										<td align="right" nowrap="nowrap">
										
											<s:iterator id="pgRecpCtrl"
												value="%{responseVo.pagingVo.pageIdxArr}" status="recp_pg">
												<s:if
													test="%{(responseVo.pagingVo.startPageNumber+1) != responseVo.pagingVo.endPageNumber || responseVo.pagingVo.endPageNumber != 1}">

													<s:if
														test="%{((#recp_pg.index) + responseVo.pagingVo.startPageNumber) == responseVo.pagingVo.startPageNumber}">
														<s:if test="%{(responseVo.pagingVo.startPageNumber) > 1}">
															<s:url id="nextPage"
																value="%{(responseVo.pagingVo.actionName)}">
																<s:param name="Page">
																	<s:property value="1" />
																</s:param>
																
															</s:url>
															<span style="text-decoration: none;" class="demo"><s:a
																	href="javascript:funPagination('%{nextPage}');">
															<s:text name="FIRST"/>
															</s:a></span>&nbsp;
														</s:if>
														<s:else>
															<span style="text-decoration: none;" class="demo1"><s:text name="FIRST"/></span>&nbsp;
														</s:else>
													</s:if>

													<s:if
														test="%{((#recp_pg.index) + responseVo.pagingVo.startPageNumber) == responseVo.pagingVo.startPageNumber}">
														<s:if test="%{(responseVo.pagingVo.startPageNumber) > 1}">
															<s:url id="nextPage"
																value="%{(responseVo.pagingVo.actionName)}">
																<s:param name="Page">
																	<s:property
																		value="(#recp_pg.index) + responseVo.pagingVo.startPageNumber" />
																</s:param>
																
																
															</s:url>
															<span style="text-decoration: none;" class="demo"><s:a
																	href="javascript:funPagination('%{nextPage}');">
															<s:text name="PREVIOUS"/>
															</s:a></span>&nbsp;
														</s:if>
														<s:else>
															<span style="text-decoration: none;" class="demo1"><s:text name="PREVIOUS"/></span>&nbsp;
														</s:else>
													</s:if>
													


													<s:url id="nextPage"
														value="%{(responseVo.pagingVo.actionName)}">
														<s:param name="Page">
															<s:property
																value="(#recp_pg.index) + responseVo.pagingVo.startPageNumber + 1" />
														</s:param>
														
													</s:url>

													<!-- Code modifided starts -->

													<s:if
														test="%{((#recp_pg.index) + responseVo.pagingVo.startPageNumber+1) != responseVo.pagingVo.currentPageNumber}">

														<span style="text-decoration: none;" class="demo">
															<s:a href="javascript:funPagination('%{nextPage}');">
																<s:property />
															</s:a>
														</span>&nbsp;
													</s:if>

													<s:else>

														<span style="text-decoration: none;" class="demo1">
															<s:a href="#">
																<s:property />
															</s:a>
														</span>&nbsp;

													</s:else>

													<!-- Code modifided ends -->

													<s:if
														test="%{((#recp_pg.index) + responseVo.pagingVo.startPageNumber+1) == responseVo.pagingVo.endPageNumber}">
														<s:if
															test="%{(responseVo.pagingVo.totalNumberOfPages) > responseVo.pagingVo.endPageNumber}">
															<s:url id="nextPage"
																value="%{(responseVo.pagingVo.actionName)}">
																<s:param name="Page">
																	<s:property
																		value="(#recp_pg.index) + responseVo.pagingVo.startPageNumber + 2" />
																</s:param>
																
															</s:url>
															<span style="text-decoration: none;" class="demo">
																<s:a href="javascript:funPagination('%{nextPage}');">
															<s:text name="NEXT"/>
															</s:a>
															</span>
														</s:if>
														<s:else>
															<span style="text-decoration: none;" class="demo1"><s:text name="NEXT"/></span>&nbsp;
														</s:else>
													</s:if>

													<s:if
														test="%{((#recp_pg.index) + responseVo.pagingVo.startPageNumber+1) == responseVo.pagingVo.endPageNumber}">
														<s:if
															test="%{(responseVo.pagingVo.totalNumberOfPages) > responseVo.pagingVo.endPageNumber}">
															<s:url id="nextPage"
																value="%{(responseVo.pagingVo.actionName)}">
																<s:param name="Page">
																	<s:property
																		value="responseVo.pagingVo.totalNumberOfPages" />
																</s:param>
																
															</s:url>
															<span style="text-decoration: none;" class="demo">
																<s:a href="javascript:funPagination('%{nextPage}');">
																&nbsp;<s:text name="LAST"/>
																</s:a>
															</span>
														</s:if>
														<s:else>
															<span style="text-decoration: none;" class="demo1"><s:text name="LAST"/></span>&nbsp;
														</s:else>
													</s:if>

												</s:if>
												<s:else>
													<span style="text-decoration: none;" class="demo1"><s:text name="FIRST"/></span>&nbsp;
													<span style="text-decoration: none;" class="demo1"><s:text name="PREVIOUS"/></span>&nbsp;
													<span style="text-decoration: none;" class="demo1"><s:text name="NEXT"/></span>&nbsp;
													<span style="text-decoration: none;" class="demo1"><s:text name="LAST"/></span>
												</s:else>
											</s:iterator>
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
	<tr>
		<td height="19" colspan="2">&nbsp;</td>
	</tr>
</table>
<script>
var isMultiSubmit = false;
function funPagination(url){
	if(isMultiSubmit == false){
		timeOutSet();
		isMultiSubmit = true;
	window.location.href = url+'&userSecureId='+document.getElementById("userSecureId").value;
	}
}

var millisecondsToWait = 3000;
function timeOutSet(){
	  setTimeout(function() {
		 isMultiSubmit = false;
	 }, millisecondsToWait); }
</script>