<%--
 -Copyright(C) 2012 Shirai Group, Tokyo, Japan All rights reserved.
 --%>
<tr>
    <td class="main-bg">
        <table width="78%" align="center" border="0" cellspacing="0" cellpadding="0">
          <tr><td height="10"></td></tr>
          <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="user-name">User Name</td>
                    <td class="company-name">Associated Company Name</td>
                  </tr>
                </table>           
            </td>
          </tr>
          <tr><td height="10"></td></tr>
          <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="14"><img src="images/heading-bg-left.png" alt="" title="" width="14" height="34" /></td>
                    <td class="heading-bg">COMPANY MASTER</td>
                    <td width="14"><img src="images/heading-bg-right.png" alt="" title="" width="14" height="34" /></td>
                  </tr>
                </table>            
            </td>
          </tr>
            <td class="spacer"></td>
          <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="14"><img src="images/login-left-top.png" alt="" title="" width="14" height="14" /></td>
                    <td class="login-top"></td>
                    <td width="14"><img src="images/login-right-top.png" alt="" title="" width="14" height="14" /></td>
                  </tr>
                  <tr>
                    <td class="login-left">&nbsp;</td>
                    <td class="login-bg">
                        <table width="80%" align="center" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="40%">
                                <table width="95%" align="left" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="login-text">User Name</td>
                                    <td>
                                        <table border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td width="11"><img src="images/input-left.png" alt="" title="" width="11" height="24" /></td>
                                            <td class="input-bg"><input name="" type="text" style="width:200px; height:24px;" /></td>
                                            <td width="11"><img src="images/input-right.png" alt="" title="" width="11" height="24" /></td>
                                          </tr>
                                        </table>                            
                                    </td>
                                    <td>&nbsp;</td>
                                  </tr>
                                </table>
                            </td>
                            <td width="40%">
                                <table width="95%" align="left" border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                    <td class="login-text">Contact Number</td>
                                    <td>
                                        <table border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td width="11"><img src="images/input-left.png" alt="" title="" width="11" height="24" /></td>
                                            <td class="input-bg"><input name="" type="text" style="width:200px; height:24px;" /></td>
                                            <td width="11"><img src="images/input-right.png" alt="" title="" width="11" height="24" /></td>
                                          </tr>
                                        </table>                            
                                    </td>
                                  </tr>
                                </table>
                            </td>
                            <td width="10%">
                                <table width="95%" align="center" border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td width="9"><img src="images/btn-left.png" alt="" title="" width="9" height="28" /></td>
                                            <td class="btn-bg"><a href="#">SEARCH</a></td>
                                            <td width="14"><img src="images/btn-right.png" alt="" title="" width="14" height="28" /></td>
                                          </tr>
                                        </table>
                                    </td>
                                  </tr>
                                </table>
                            </td>
                          </tr>
                        </table>
                    
                                            
                    </td>
                    <td class="login-right">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><img src="images/login-left-bot.png" alt="" title="" width="14" height="14" /></td>
                    <td class="login-bot"></td>
                    <td><img src="images/login-right-bot.png" alt="" title="" width="14" height="14" /></td>
                  </tr>
                </table>            
                </td>
          </tr>
          <tr>
            <td class="spacer"></td>
          </tr>
          
          <tr>
          	<td>
            
<div>
        <table border="0" cellpadding="0" cellspacing="0" class='main'>
            <tr>
                <td class='tablefrozencolumn'>
                    <div id='divroot' class='root'>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class='root'>
                            <tr>
                                <td class='inner frozencol colwidth head' width="13"><img src="images/titles-left.jpg" alt="" title="" width="11" height="28" /></td>
                                <td class='inner frozencol colwidth head' width="25" align="center">&nbsp;</td>
                                <td class='inner frozencol colwidth head'>Sl.No</td>
                            </tr>
                        </table>
                    </div>
                    <div id='divfrozen' class='frozen'>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class='frozen'>
                            <tr>
                                <td class='inner frozencol1' id="freeze-lines1" width="13">&nbsp;</td>
                                <td class='inner frozencol1' width="25" align="center"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class='inner frozencol1'>Col1Row2</td>
                            </tr>
                            <tr>
                                <td class='inner frozencol2' id="freeze-lines2">&nbsp;</td>
                                <td class='inner frozencol2' width="25" align="center"><img src="images/checked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class='inner frozencol2'>Col1Row2</td>
                            </tr>
                            <tr>
                                <td class='inner frozencol1' id="freeze-lines3">&nbsp;</td>
                                <td class='inner frozencol1' width="25" align="center"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class='inner frozencol1'>Col1Row2</td>
                            </tr>
                            <tr>
                                <td class='inner frozencol2' id="freeze-lines4">&nbsp;</td>
                                <td class='inner frozencol2' width="25" align="center"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class='inner frozencol2'>Col1Row2</td>
                            </tr>
                            <tr>
                                <td class='inner frozencol1' id="freeze-lines5">&nbsp;</td>
                                <td class='inner frozencol1' width="25" align="center"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class='inner frozencol1'>Col1Row2</td>
                            </tr>
                            <tr>
                                <td class='inner frozencol2' id="freeze-lines6">&nbsp;</td>
                                <td class='inner frozencol2' width="25" align="center"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class='inner frozencol2'>Col1Row2</td>
                            </tr>
                            <tr>
                                <td class='inner frozencol1' id="freeze-lines7">&nbsp;</td>
                                <td class='inner frozencol1' width="25" align="center"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class='inner frozencol1'>Col1Row2</td>
                            </tr>
                            <tr>
                                <td class='inner frozencol2' id="freeze-lines8">&nbsp;</td>
                                <td class='inner frozencol2' width="25" align="center"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class='inner frozencol2'>Col1Row2</td>
                            </tr>
                            <tr>
                                <td class='inner frozencol1' id="freeze-lines9">&nbsp;</td>
                                <td class='inner frozencol1' width="25" align="center"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class='inner frozencol1'>Col1Row2</td>
                            </tr>
                            <tr>
                                <td class='inner frozencol2' id="freeze-lines10">&nbsp;</td>
                                <td class='inner frozencol2' width="25" align="center"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class='inner frozencol2'>Col1Row2</td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td class='tablecontent'>
                    <div id='headscroll' class='divhead'>
                        <table border="0" cellpadding="0" cellspacing="0" class='head'>
                            <tr>
                                <td class='inner col1 head'>Sl.No</td>
                                <td class='inner col2 head'>Company Name</td>
                                <td class='inner col3 head'>Contact Person</td>
                                <td class='inner col4 head'>Contact Number</td>
                                <td class='inner col5 head'>Contact Fax Number</td>
                                <td class='inner col6 head'>Date</td>
                                <td class='inner col7 head'>Status</td>
                                <td class='inner col8 head'>Head 8</td>
                                <td class='inner col9 head'>Head 9</td>
                                <td class='inner col10 head'>&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    <div id='contentscroll' class='content' onscroll='reposHead(this);'>
                        <table border="0" cellpadding="0" cellspacing="0" class='content' id='innercontent'>
                            <tr>
                                <td class='inner col1'>Col1Row2</td>
                                <td class='inner col2'>Col2Row2</td>
                                <td class='inner col3'>Col3Row2</td>
                                <td class='inner col4'>Col4Row2</td>
                                <td class='inner col5'>Col5Row2</td>
                                <td class='inner col6'>Col6Row2</td>
                                <td class='inner col7'>Col7Row2</td>
                                <td class='inner col8'>Col8Row2</td>
                                <td class='inner col9'>Col9Row2</td>
                                <td class='inner col10'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class='inner1 col11'>
                                    Col1Row3
                                </td>
                                <td class='inner1'>
                                    Col2Row3
                                </td>
                                <td class='inner1'>
                                    Col3Row3
                                </td>
                                <td class='inner1'>
                                    Col4Row3
                                </td>
                                <td class='inner1'>
                                    Col5Row3
                                </td>
                                <td class='inner1'>
                                    Col6Row3
                                </td>
                                <td class='inner1'>
                                    Col7Row3
                                </td>
                                <td class='inner1'>
                                    Col8Row3
                                </td>
                                <td class='inner1'>
                                    Col9Row3
                                </td>
                                <td class='inner1'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class='inner col1'>
                                    Col1Row4
                                </td>
                                <td class='inner' id="inner-lines">
                                    Col2Row4
                                </td>
                                <td class='inner' id="inner-lines">
                                    Col3Row4
                                </td>
                                <td class='inner' id="inner-lines">
                                    Col4Row4
                                </td>
                                <td class='inner' id="inner-lines">
                                    Col5Row4
                                </td>
                                <td class='inner' id="inner-lines">
                                    Col6Row4
                                </td>
                                <td class='inner' id="inner-lines">
                                    Col7Row4
                                </td>
                                <td class='inner' id="inner-lines">
                                    Col8Row4
                                </td>
                                <td class='inner' id="inner-lines">
                                    Col9Row4
                                </td>
                                <td class='inner' id="inner-lines">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class='inner1 col11'>
                                    Col1Row5
                                </td>
                                <td class='inner1'>
                                    Col2Row5
                                </td>
                                <td class='inner1'>
                                    Col3Row5
                                </td>
                                <td class='inner1'>
                                    Col4Row5
                                </td>
                                <td class='inner1'>
                                    Col5Row5
                                </td>
                                <td class='inner1'>
                                    Col6Row5
                                </td>
                                <td class='inner1'>
                                    Col7Row5
                                </td>
                                <td class='inner1'>
                                    Col8Row5
                                </td>
                                <td class='inner1'>
                                    Col9Row5
                                </td>
                                <td class='inner1'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class='inner col1'>
                                    Col1Row6
                                </td>
                                <td class='inner'>
                                    Col2Row6
                                </td>
                                <td class='inner'>
                                    Col3Row6
                                </td>
                                <td class='inner'>
                                    Col4Row6
                                </td>
                                <td class='inner'>
                                    Col5Row6
                                </td>
                                <td class='inner'>
                                    Col6Row6
                                </td>
                                <td class='inner'>
                                    Col7Row6
                                </td>
                                <td class='inner'>
                                    Col8Row6
                                </td>
                                <td class='inner'>
                                    Col9Row6
                                </td>
                                <td class='inner'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class='inner1 col11'>
                                    Col1Row7
                                </td>
                                <td class='inner1'>
                                    Col2Row7
                                </td>
                                <td class='inner1'>
                                    Col3Row7
                                </td>
                                <td class='inner1'>
                                    Col4Row7
                                </td>
                                <td class='inner1'>
                                    Col5Row7
                                </td>
                                <td class='inner1'>
                                    Col6Row7
                                </td>
                                <td class='inner1'>
                                    Col7Row7
                                </td>
                                <td class='inner1'>
                                    Col8Row7
                                </td>
                                <td class='inner1'>
                                    Col9Row7
                                </td>
                                <td class='inner1'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class='inner col1'>
                                    Col1Row8
                                </td>
                                <td class='inner'>
                                    Col2Row8
                                </td>
                                <td class='inner'>
                                    Col3Row8
                                </td>
                                <td class='inner'>
                                    Col4Row8
                                </td>
                                <td class='inner'>
                                    Col5Row8
                                </td>
                                <td class='inner'>
                                    Col6Row8
                                </td>
                                <td class='inner'>
                                    Col7Row8
                                </td>
                                <td class='inner'>
                                    Col8Row8
                                </td>
                                <td class='inner'>
                                    Col9Row8
                                </td>
                                <td class='inner'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class='inner1 col11'>
                                    Col1Row8
                                </td>
                                <td class='inner1'>
                                    Col2Row8
                                </td>
                                <td class='inner1'>
                                    Col3Row8
                                </td>
                                <td class='inner1'>
                                    Col4Row8
                                </td>
                                <td class='inner1'>
                                    Col5Row8
                                </td>
                                <td class='inner1'>
                                    Col6Row8
                                </td>
                                <td class='inner1'>
                                    Col7Row8
                                </td>
                                <td class='inner1'>
                                    Col8Row8
                                </td>
                                <td class='inner1'>
                                    Col9Row8
                                </td>
                                <td class='inner1'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class='inner col1'>
                                    Col1Row8
                                </td>
                                <td class='inner'>
                                    Col2Row8
                                </td>
                                <td class='inner'>
                                    Col3Row8
                                </td>
                                <td class='inner'>
                                    Col4Row8
                                </td>
                                <td class='inner'>
                                    Col5Row8
                                </td>
                                <td class='inner'>
                                    Col6Row8
                                </td>
                                <td class='inner'>
                                    Col7Row8
                                </td>
                                <td class='inner'>
                                    Col8Row8
                                </td>
                                <td class='inner'>
                                    Col9Row8
                                </td>
                                <td class='inner'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class='inner1 col11'>
                                    Col1Row8
                                </td>
                                <td class='inner1'>
                                    Col2Row8
                                </td>
                                <td class='inner1'>
                                    Col3Row8
                                </td>
                                <td class='inner1'>
                                    Col4Row8
                                </td>
                                <td class='inner1'>
                                    Col5Row8
                                </td>
                                <td class='inner1'>
                                    Col6Row8
                                </td>
                                <td class='inner1'>
                                    Col7Row8
                                </td>
                                <td class='inner1'>
                                    Col8Row8
                                </td>
                                <td class='inner1'>
                                    Col9Row8
                                </td>
                                <td class='inner1'>&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td class='tableverticalscroll' rowspan="2">
                    <div class='vertical-scroll' onscroll='reposVertical(this);'>
                        <div>
                        </div>
                    </div>
                    <div class='ff-fill'>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class='horizontal-scroll' onscroll='reposHorizontal(this);'>
                        <div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>			
			<!--
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td style=" background-image:url(images/titles-bg.png); background-repeat:repeat-x; background-position:top;">
                        <table id="data" class="dataTable" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                        <thead>
                            <tr>
                                <th class="titles-left"><img src="images/titles-left.jpg" alt="" title="" width="13" height="32" /></th>
                                <th class="ch-box">&nbsp;</th>
                                <th class="titles-div"><img src="images/titles-div.png" alt="" title="" width="4" height="32" /></th>
                                <th class="sl-no">Sl.No</th>
                                <th class="titles-div"><img src="images/titles-div.png" alt="" title="" width="4" height="32" /></th>
                                <th class="com-name">Company Name</th>
                                <th class="titles-div"><img src="images/titles-div.png" alt="" title="" width="4" height="32" /></th>
                                <th class="con-person">Contact Person</th>
                                <th class="titles-div"><img src="images/titles-div.png" alt="" title="" width="4" height="32" /></th>
                                <th class="con-num">Contact Number</th>
                                <th class="titles-div"><img src="images/titles-div.png" alt="" title="" width="4" height="32" /></th>
                                <th class="con-fax-num">Contact Fax Number</th>
                                <th class="titles-div"><img src="images/titles-div.png" alt="" title="" width="4" height="32" /></th>
                                <th class="date">Date</th>
                                <th class="titles-div"><img src="images/titles-div.png" alt="" title="" width="4" height="32" /></th>
                                <th class="status">Status</th>
                                <th class="titles-right"><img src="images/titles-right.jpg" alt="" title="" width="13" height="32" /></th>
                            </tr>
                        </thead>
                                    
                        <tbody>
                            <tr>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-ch"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-ch"><img src="images/checked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-ch"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-ch"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-ch"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-ch"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-ch"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1-div">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                                <td class="details1">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-ch"><img src="images/unchecked-img.jpg" alt="" title="" width="13" height="13" /></td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2-div">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                                <td class="details2">&nbsp;</td>
                            </tr>
                        </tbody>
                        
                        </table>
                    </td>
                  </tr>
                </table>
                -->
            </td>
          </tr>
          
          <tr>
            <td height="10"></td>
          </tr>
          <tr>
            <td>
                <table bgcolor="#CCCCCC" align="right" width="30%" border="0" cellspacing="1" cellpadding="0">
                  <tr>
                    <td bgcolor="#ffffff">Pagination</td>
                  </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td height="10"></td>
          </tr>
          <tr>
            <td>
                <table width="30%" align="center" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="9"><img src="images/btn-left.png" alt="" title="" width="9" height="28" /></td>
                            <td class="btn-bg"><a href="#">ADD</a></td>
                            <td width="14"><img src="images/btn-right.png" alt="" title="" width="14" height="28" /></td>
                          </tr>
                        </table>
                    </td>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="9"><img src="images/btn-left.png" alt="" title="" width="9" height="28" /></td>
                            <td class="btn-bg"><a href="#">MODIFY</a></td>
                            <td width="14"><img src="images/btn-right.png" alt="" title="" width="14" height="28" /></td>
                          </tr>
                        </table>
                    </td>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="9"><img src="images/btn-left.png" alt="" title="" width="9" height="28" /></td>
                            <td class="btn-bg"><a href="#">DELETE</a></td>
                            <td width="14"><img src="images/btn-right.png" alt="" title="" width="14" height="28" /></td>
                          </tr>
                        </table>
                    </td>
                  </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td height="10"></td>
          </tr>
        </table>
    </td>
  </tr>