<%--
 -Copyright(C) 2012 Shirai Group, Tokyo, Japan All rights reserved.
 --%>

<!--  CLEARING THE CACHE -->
<%
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader("Expires", 0);
%>
<!--  CLEARING THE CACHE -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s"%>

<html>
<head>

<%
	/* Getting Context Path */
	String contextPath = request.getContextPath();
	
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- Including Struts 2 Jquery Head -->

<title><s:text name="ParameterMaster"/></title>

<!-- Including Shirai script Page -->
<script type="text/javascript" src="<%=contextPath%>/js/Shirai.js"></script>
<script type="text/javascript" src="<%=contextPath%>/js/TextArea-Valid.js"></script>
<script language='javascript' type='text/javascript'
	src="<%=contextPath%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=contextPath%>/js/Popup.js"></script>
<!-- Including all Css pages -->
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/shirai.css">
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/home.css" />
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/dropdown-menus.css" />
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/popup.css" />
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/css/error.css" />

</head>

<body>
	<s:form name="Parameter" id="Parameter" validate="true" theme="simple">
		<!-- HiddenValues -->
		<s:hidden name="userSecureId"  id="userSecureId"> </s:hidden>
        <s:hidden name="deleteValue" id="deleteValue"></s:hidden>
        <s:hidden name="parameterValue.parameterId" value="%{parameterValue.parameterId}"> </s:hidden>
        <s:hidden name="parameterValue.createdDate" value="%{parameterValue.createdDate}"> </s:hidden>
        <s:hidden name="parameterValue.companyActionVo.companyId" value="%{parameterValue.companyActionVo.companyId}"> </s:hidden>
        <s:hidden name="parameterValue.createdBy" value="%{parameterValue.createdBy}"> </s:hidden>
        

		<!-- New Template -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="main-bg">
					<table width="78%" align="center" border="0" cellspacing="0"
						cellpadding="0">
						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="user-name"><s:text name="USERNAME"/><s:property value="userNameSession"/></td>
										<s:if test="%{associateCompanySession == null || associateCompanySession == ''  }"> 
										<td nowrap="nowrap" class="success">
										 <s:text name="SelectAssociateCompanyParam"></s:text>
										</td>
										</s:if>
										<s:if test="%{userTypeSession > 2}">
										<td class="company-name"><s:text name="ASSOCIATECOMAPNY"/><s:property value="associateCompanySession"/></td>
										</s:if>
										<s:else>
											<td class="company-name"><s:url
													var="changeAssociateCompany"
													action="changeAssociateCompanys">
												</s:url> <s:a
													href="javascript:funCallData('%{changeAssociateCompany}','changeAssociateCompany','300','650',200,'')"
													tabindex="16">
													<s:text name="ASSOCIATECOMAPNY" />
												</s:a> <s:property value="associateCompanySession" /></td>
										</s:else>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="10"></td>
						</tr>
						
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="14"><img
											src="<%=contextPath%>/images/heading-bg-left.png" alt=""
											title="" width="14" height="34" /></td>
										<td class="heading-bg"><s:text
												name="ParameterMaster"></s:text></td>
										<td width="14"><img
											src="<%=contextPath%>/images/heading-bg-right.png" alt=""
											title="" width="14" height="34" /></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<s:if test="hasActionMessages()">
								<td>
									<table width="50%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td nowrap="nowrap" align="center">
												   <div>
												      <s:actionmessage cssClass="success"/>
												   </div>
											</td>
										</tr>
									</table>
								</td>
							</s:if>
							<s:if test="hasActionErrors()">
								<td>
									<table width="50%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td nowrap="nowrap" align="center">
												   <div>
												      <s:actionerror cssClass="error"/>
												   </div>
											</td>
										</tr>
									</table>
								</td>
							</s:if>
							<s:else>
							<td class="spacer"></td>
							</s:else>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr>
										<td width="14"><img
											src="<%=contextPath%>/images/login-left-top-home.png" alt=""
											title="" width="14" height="14" /></td>
										<td class="login-top-home"></td>
										<td width="14"><img
											src="<%=contextPath%>/images/login-right-top-home.png" alt=""
											title="" width="14" height="14" /></td>
									</tr>
									<tr>
										<td class="login-left-home">&nbsp;</td>
										<td class="login-bg"> <div style="height: 400px; overflow: auto;margin-left:auto;margin-right:auto;">
											<table align="center" border="0" cellpadding="0" cellspacing="2" width="640">
                      <tbody>
														<tr>
															<td height="5px"></td><td colspan="2"><div id="GpsInterval">
																	<s:fielderror cssClass="error"
																		fieldName="parameterValue.gpsInterval" theme="simple" />
																</div></td>
														</tr>
														<tr>
															<th class="form-text-middle" align="right"><s:text
																	name="GpsInterval" /><font color="red">*</font>&nbsp;:&nbsp;</th>
															<th height="28"><div align="left">
																	<s:textfield onkeypress="return numbersonly(event)"
																		onchange="removeErrorField('GpsInterval')"
																		tabindex="1" id="gpsInter"
																		name="parameterValue.gpsInterval" theme="simple"
																		maxlength="4" size="38" onpaste="return false;"
																		cssStyle="ime-mode:disabled;" />
																</div></th>
															<th nowrap="nowrap">
                        	                                <div class="example-tool" style="float:left;height:28;"><s:text name="Minutes"/></div>
                                                            </th>
														</tr>
														<tr>
															<td height="5px"></td><td colspan="2"><div id="GpsModeDelay">
                          	    							<s:fielderror cssClass="error" fieldName="parameterValue.gpsModeDelay"  theme="simple"/>
                         									</div></td>
														</tr>
														<tr>
															<th class="form-text-middle" align="right"><s:text
																	name="GpsModeDelay" /><font color="red">*</font>&nbsp;:&nbsp;</th>
															<th height="28"><div align="left">
																	<s:textfield onkeypress="return numbersonly(event)"
																		onchange="removeErrorField('GpsModeDelay')"
																		tabindex="2" id="gpsModeDelay"
																		name="parameterValue.gpsModeDelay" theme="simple"
																		maxlength="4" size="38" onpaste="return false;"
																		cssStyle="ime-mode:disabled;" />
																</div></th>
															<th nowrap="nowrap">
                        	                                <div class="example-tool" style="float:left;height:28;"><s:text name="Minutes"/></div>
                                                            </th>
														</tr>
														<tr>
															<td height="5px"></td><td colspan="2"><div id="GpsStopCountLimit">
                          	    							<s:fielderror cssClass="error" fieldName="parameterValue.gpsStopCount"  theme="simple"/>
                         									</div></td>
														</tr>
														<tr>
															<th class="form-text-middle" align="right"><s:text name="GpsStopCountLimit"/><font color="red">*</font>&nbsp;:&nbsp;</th>
															<th height="28"><div align="left">
																	<s:textfield onkeypress="return numbersonly(event)"
																		onchange="removeErrorField('GpsStopCountLimit')"
																		tabindex="3" id="gpsStopCount"
																		name="parameterValue.gpsStopCount" theme="simple"
																		maxlength="4" size="38" onpaste="return false;"
																		cssStyle="ime-mode:disabled;" />
																</div></th>
															<th nowrap="nowrap"></th>
														</tr>
														<tr>
															<td height="5px"></td><td colspan="2"><div id="GpsStopDistance">
                          	    							<s:fielderror cssClass="error" fieldName="parameterValue.gpsStopDistance"  theme="simple"/>
                         									</div></td>
														</tr>
														<tr>
															<th class="form-text-middle" align="right"><s:text
																	name="GpsStopDistance" /><font color="red">*</font>&nbsp;:&nbsp;</th>
															<th height="28"><div align="left">
																	<s:textfield onkeypress="return numbersonly(event)"
																		onchange="removeErrorField('GpsStopDistance')"
																		tabindex="4" id="gpsStopDistance"
																		name="parameterValue.gpsStopDistance" theme="simple"
																		maxlength="4" size="38" onpaste="return false;"
																		cssStyle="ime-mode:disabled;" />
																</div></th>
															<th nowrap="nowrap">
                        	                                <div class="example-tool" style="float:left;height:28;"><s:text name="Meter"/></div>
                                                            </th>
														</tr>
														<tr>
															<td height="5px"></td><td colspan="2"><div id="PhotoMailDelayLimit">
                          	    							<s:fielderror cssClass="error" fieldName="parameterValue.photoMailDelay"  theme="simple"/>
			                         						</div></td>
														</tr>
														<tr>
															<th class="form-text-middle" align="right"><s:text
																	name="PhotoMailDelayLimit" /><font color="red">*</font>&nbsp;:&nbsp;</th>
															<th height="28"><div align="left">
																	<s:textfield onkeypress="return numbersonly(event)"
																		onchange="removeErrorField('PhotoMailDelayLimit')"
																		tabindex="5" id="photoMailDelay"
																		name="parameterValue.photoMailDelay" theme="simple"
																		maxlength="4" size="38" onpaste="return false;"
																		cssStyle="ime-mode:disabled;" />
																</div></th>
															<th nowrap="nowrap">
                        	                                <div class="example-tool" style="float:left;height:28;"><s:text name="Minutes"/></div>
                                                            </th>
														</tr>
														<tr>
															<td height="5px"></td><td colspan="2"><div id="GpsModeDelayMessage">
                          	    							<s:fielderror cssClass="error" fieldName="parameterValue.gpsModeDelayMessage"  theme="simple"/>
                         									</div></td>
														</tr>
														<tr>
															<th class="form-text" align="right"><s:text
																name="GpsModeDelayMessage"/><font color="red">*</font>&nbsp;:&nbsp;
															</th>
															<th height="28"><div align="left">
																	<s:textarea
																		onchange="removeErrorField('GpsModeDelayMessage')"
																		tabindex="6" name="parameterValue.gpsModeDelayMessage"
																		theme="simple" maxlength="100" rows="4" cols="23"
																		style="resize:none;"
																		id="gpsModeDelayMessage">
																	</s:textarea>
																</div></th>
															<th nowrap="nowrap"></th>
														</tr>
														<tr>
															<td height="5px"></td><td colspan="2"><div id="GpsStopMessage">
                          	    							<s:fielderror cssClass="error" fieldName="parameterValue.gpsStopMessage"  theme="simple"/>
                         									</div></td>
														</tr>
														<tr>
															<th class="form-text" align="right"><s:text
																	name="GpsStopMessage" /><font color="red">*</font>&nbsp;:&nbsp;
															</th>
															<th height="28"><div align="left">
																	<s:textarea
																		onchange="removeErrorField('GpsStopMessage')"
																		tabindex="7" name="parameterValue.gpsStopMessage"
																		theme="simple" maxlength="100" rows="4" cols="23"
																		style="resize:none;" id="gpsStopMessage">
																	</s:textarea>
																</div></th>
															<th nowrap="nowrap"></th>
														</tr>
														<tr>
															<td height="5px"></td><td colspan="2"><div id="PhotoMailDelayMessage">
                          	    							<s:fielderror cssClass="error" fieldName="parameterValue.photoMailDelayMessage"  theme="simple"/>
                         									</div></td>
														</tr>
														<tr>
															<th class="form-text" align="right"><s:text
																	name="PhotoMailDelayMessage" /><font color="red">*</font>&nbsp;:&nbsp;
															</th>
															<th height="28"><div align="left">
																	<s:textarea
																		onchange="removeErrorField('PhotoMailDelayMessage')"
																		tabindex="8"
																		name="parameterValue.photoMailDelayMessage"
																		theme="simple" maxlength="100" rows="4" cols="23"
																		style="resize:none;"
																		id="photoMailDelayMessage">
																	</s:textarea>
																</div></th>
															<th nowrap="nowrap"></th>
														</tr>
														<tr>
															<td height="5px"></td><td colspan="2"><div id="PhotoMailAckMessage">
			                          	    				<s:fielderror cssClass="error" fieldName="parameterValue.photoMailAckMessage"  theme="simple"/>
						             						</div></td>
														</tr>
														<tr>
															<th class="form-text" align="right"><s:text
																	name="PhotoMailAckMessage" /><font color="red">*</font>&nbsp;:&nbsp;
															</th>
															<th height="28"><div align="left">
																	<s:textarea
																		onchange="removeErrorField('PhotoMailAckMessage')"
																		tabindex="9" name="parameterValue.photoMailAckMessage"
																		theme="simple" maxlength="100" rows="4" cols="23"
																		style="resize:none;"
																		id="photoMailAckMessage"></s:textarea>
																</div></th>
															<th nowrap="nowrap"></th>
														</tr>
														<tr>
															<td height="5px"></td><td colspan="2"><div id="MailServer">
                          	    							<s:fielderror cssClass="error" fieldName="parameterValue.mailServer"  theme="simple"/>
                         									</div></td>
														</tr>
														<tr>
															<th class="form-text-middle" align="right"><s:text
																	name="MailServer" /><font color="red">*</font>&nbsp;:&nbsp;
															</th>
															<th height="28"><div align="left">
																	<s:textfield onchange="removeErrorField('MailServer')"
																		tabindex="10" name="parameterValue.mailServer"
																		theme="simple" maxlength="50" size="38"
																		id="mailServer"
																		cssStyle="ime-mode:disabled;" />
																</div></th>
															<th nowrap="nowrap"></th>
														</tr>
														<tr>
															<td height="5px"></td><td colspan="2"><div id="SystemMailAddress">
			                          	    				<s:fielderror cssClass="error" fieldName="parameterValue.systemMailAddress"  theme="simple"/>
						             						</div></td>
														</tr>
														<tr>
															<th class="form-text-middle" align="right"><s:text
																	name="SystemMailAddress" /><font color="red">*</font>&nbsp;:&nbsp;
															</th>
															<th height="28"><div align="left">
																	<s:textfield
																		onchange="removeErrorField('SystemMailAddress')"
																		tabindex="11" name="parameterValue.systemMailAddress"
																		theme="simple" maxlength="100" size="38"
																		id="systemMailAddress"
																		cssStyle="ime-mode:disabled;" />
																</div></th>
															<th nowrap="nowrap">
																<div class="example-tool"
																	style="float: left; height: 28;">
																	<s:text name="EmailEg" />
																</div>
															</th>
														</tr>
														
														<tr>
															<td height="3px"></td>
														</tr>
														<tr>
															<th class="form-text-middle" align="right"><s:text
																	name="ServiceName" />&nbsp;:&nbsp;
															</th>
															<th height="28"><div align="left">
																	<s:textfield
																		tabindex="12" name="parameterValue.serviceName"
																		theme="simple" maxlength="100" size="38"/>
																</div></th>
															<th nowrap="nowrap">
																&nbsp;
															</th>
														</tr>
													</tbody>
                    </table></div> </td>

										<td class="login-right-home">&nbsp;</td>
									</tr>
									<tr>
										<td><img src="<%=contextPath%>/images/login-left-bot-home.png"
											alt="" title="" width="14" height="14" /></td>
										<td class="login-bot-home"></td>
										<td><img
											src="<%=contextPath%>/images/login-right-bot-home.png" alt=""
											title="" width="14" height="14" /></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="spacer"></td>
						</tr>

						<tr>
							<td height="10"></td>
						</tr>
						<tr>
							<td>
								<table width="200px" align="center" border="0" cellspacing="0"
									cellpadding="0">
									<tr>
										
										<s:if test="%{associateCompanySession != null && associateCompanySession != ''  }">    
										<s:if test="%{authorizationMap.get('ParameterModify') == 1}"> 
										<td width="100px"><a tabindex="13" onmouseover="changeButtonFontList('updateId')"
												 onmouseout="javascript:resetClassList('updateId');"
										onblur="javascript:resetClassList('updateId')"
											onfocus="changeButtonFontList('updateId')"
														onclick="javascript:funFormSubmit(document.forms[0],'updateParameter');"
														onkeypress="if(event.keyCode == 13){funFormSubmit(document.forms[0],'updateParameter');}"
														style="cursor: pointer; text-decoration: none;outline: none;">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td width="9"><img
														src="<%=contextPath%>/images/btn-left.png" alt="" title=""
														width="9" height="28" /></td>
													<td class="btn-bg-home" id="updateId"> <s:text
																name="Update" /></td>
													<td width="14"><img
														src="<%=contextPath%>/images/btn-right.png" alt=""
														title="" width="14" height="28" /></td>
												</tr>
											</table></a>
										</td>
										 </s:if>
										<s:if test="%{authorizationMap.get('ParameterModify') == 1}">    
										<td width="100px"><a tabindex="14" onmouseover="changeButtonFontList('resetId')"
												 onmouseout="javascript:resetClassList('resetId');"
										onblur="javascript:resetClassList('resetId')"
											onfocus="changeButtonFontList('resetId')"
														onclick="javascript:funFormSubmit(document.forms[0],'resetParameter');"
														onkeypress="if(event.keyCode == 13){funFormSubmit(document.forms[0],'resetParameter');}"
														style="cursor: pointer; text-decoration: none;outline: none;">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td width="9"><img
														src="<%=contextPath%>/images/btn-left.png" alt="" title=""
														width="9" height="28" /></td>
													<td class="btn-bg-home" id="resetId"><s:text
																name="Reset" /></td>
													<td width="14"><img
														src="<%=contextPath%>/images/btn-right.png" alt=""
														title="" width="14" height="28" /></td>
												</tr>
											</table></a>
											<a onkeyup="setTabIndex();" tabindex="15" style="outline: none;"></a>
										</td>
										</s:if>
										</s:if>
										
									</tr>
								</table>
							</td>
						</tr>						
					</table>
				</td>
			</tr>
		</table>

		<script>
		var isMultiSubmit = false;
		
			function removeErrorField(objValue){
			var newDiv = document.getElementById(objValue);
			newDiv.innerHTML = "";
			}
			
			function funRefreshParent(obj,actionName)
			{
			
			 document.getElementById("Parameter").action="viewParameter.action";
			 document.getElementById("Parameter").method="POST";
			 document.getElementById("Parameter").submit();	
			}
			
						
			window.onload=setTabIndex;
			 function setTabIndex(){
			 	document.getElementById('gpsInter').focus();
			 }
			
			 function funFormSubmit(obj,action)
			   { 
				   if(isMultiSubmit==false){
			   		 isMultiSubmit = true;
			   		 funSubmitForm(obj,action);
			   		}
			   }
			 function funCallData(url, actionName, height, width, marginHeight, view){
					var source = null;
					if(view=="view"){
					source = url+"&userSecureId="+document.getElementById('userSecureId').value;
					}else{
					source = url+"?userSecureId="+document.getElementById('userSecureId').value;
					}
					funViewData(source, actionName, height, width, marginHeight);
				}
			 
			 function changeButtonFontList(controlId){
					document.getElementById(controlId).className="btn-bg-home_focus";
				}

				function resetClassList(controlId){
					document.getElementById(controlId).className="btn-bg-home";
				}	
			
		</script>

	</s:form>
</body>
</html>
