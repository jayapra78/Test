<%--
 -Copyright(C) 2012 Shirai Group, Tokyo, Japan All rights reserved.
 --%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:text name="SHIRAI" /></title>
<%String contextPath = request.getContextPath();%>
</head>

<body>
	<div id="msg" style="font-size:largest;">
		<!-- you can set whatever style you want on this -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="main-bg" align="center" valign="middle" height="500px">
				<img src="<%=contextPath%>/images/traceability-logo-anim.gif" width="200" height="100" alt=""/>		
			</td>
		</tr>
		</table>
	</div>
	<div id="body" style="display:none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td ><tiles:insertAttribute name="body" /></td>
		</tr>
	</table>
	</div>
	<script type="text/javascript">
	$(document).ready(function() {
	    $('#body').show();
	    $('#msg').hide();
	});
	</script>
</body>
</html>