import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sun.mail.util.MailSSLSocketFactory;
import com.task.common.MailVo;

/**
 * Creater / sengottaiyansSRM copyright information to Shirai 2013
 * 
 */

/**
 * @author sengottaiyans
 *
 */
public class TestMail {

	public static void main(String[] args) {
		MailVo mailVo = new MailVo();
        mailVo.setSubject("SMTP WORKING FROM OUTSIDE");
        mailVo.setMailTo(new String[] {"balajir@srmtech.com"});
        mailVo.setMailCc(new String[] {"sumantapani@srmtech.com","stg@srmtech.com"});
        //mailVo.setMailCc(new String[] {""});
        
        //mailVo.setMailTo(new String[] {"Janakiramank@srmtech.com"});
        //mailVo.setMailCc(new String[] {"sengottaiyans@srmtech.com","balamurugank@srmtech.com","dhulasiramanp@srmtech.com","rajasureshg@srmtech.com","srinuvasaraon@srmtech.com","manimekalaim@srmtech.com","umamaheswarik@srmtech.com","umedsahu@srmtech.com","samanthacarolynet@srmtech.com","karuanasaranm@srmtech.com","valsalakumarj@srmtech.com","kirthikam@srmtech.com","karthikks@srmtech.com","sankarasubramanis@srmtech.com","maheswarip@srmtech.com","archanas@srmtech.com","sugandhic@srmtech.com","senthilkumarg@srmtech.com","vvnagabuveeranki@srmtech.com","aruljothia@srmtech.com","subhav@srmtech.com","rajamanip@srmtech.com","kamalnathanj@srmtech.com","durgeshkumarjw@srmtech.com","neethimohana@srmtech.com","santhoshkumarm@srmtech.com","sivakumarrajamani@srmtech.com","naveenrahuld@srmtech.com","selvakannanr@srmtech.com","nagoorhaneefas@srmtech.com","peterpaulraj@srmtech.com","parthasarathykannanm@srmtech.com","karthickr@srmtech.com","krishnavenit@srmtech.com","karthikkumarr@srmtech.com","vivekv@srmtech.com","swanapreethig@srmtech.com","subham@srmtech.com","lathad@srmtech.com","karthikeyank@srmtech.com","madhanp@srmtech.com","gopinathkk@srmtech.com","kamalakannankk@srmtech.com","thomaskm@srmtech.com","sureshbabukr@srmtech.com","rinimujeeb@srmtech.com","puvanajas@srmtech.com","madhumathip@srmtech.com","somnathguha@srmtech.com","somasundaramsp@srmtech.com","geethapriyaa@srmtech.com","aarthyv@srmtech.com","ashwinib@srmtech.com","davidprincec@srmtech.com","dharmambals@srmtech.com","elakkiyap@srmtech.com","indumathyc@srmtech.com","jamesprabhaharv@srmtech.com","kalaivanim@srmtech.com","koteswararaogutti@srmtech.com","preetigv@srmtech.com","rajasekharareddyp@srmtech.com","ramyav@srmtech.com","sarikab@srmtech.com","tamilmanik@srmtech.com"});
        
        mailVo.setMessage("SMTP WORKING");
        try{
        	sendMail(mailVo);
        }
        catch(Exception e){
        	System.out.println("Exception :" +e);
        }
	}
	
	public static boolean sendMail(MailVo mailVo) throws Exception{
		
		Properties objProps = new Properties();
		
		StringBuilder logMessage = new StringBuilder();
		
		ResourceBundle bundle1 = ResourceBundle.getBundle("Mail_Config");
		
		//final String userName="srmtchn\\neethimohana";
		final String userName="shiraijob";
		final String password="srm12345";
		String fromAddress = "shiraijob@mail.srmtech.net";
		
		objProps.put("mail.smtp.host", "mail.srmtech.net");
		objProps.put("mail.smtp.port", "587");
		objProps.put("mail.smtp.auth", "true");
		objProps.put("mail.smtp.starttls.enable", "true");
		
		// Added for SSL Verification other than Google mail
		MailSSLSocketFactory sf = new MailSSLSocketFactory();
	    sf.setTrustAllHosts(true);
	    objProps.put("mail.smtp.ssl.socketFactory", sf);
		
		Session session = Session.getInstance(objProps,
				  new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(userName,password );
					}
				  });
		
		MimeMessage msg = new MimeMessage(session);
		
		try {
			
			InternetAddress address = new InternetAddress();
			try {
				address.setAddress(fromAddress);
				address.setPersonal(mailVo.getPersonal());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			msg.setFrom(address);
			
			InternetAddress[] addressTo = new InternetAddress[mailVo.getMailTo().length];        
			
			for (int i = 0; i < mailVo.getMailTo().length; i++) {                        
				addressTo[i] = new InternetAddress(mailVo.getMailTo()[i]);   
				logMessage.append("To : " + addressTo[i] + "\n");
			}                
			msg.setRecipients(Message.RecipientType.TO, addressTo);       
			
			InternetAddress[] addressCc = null;
			if(mailVo.getMailCc() !=null && mailVo.getMailCc().length > 0){
				addressCc = new InternetAddress[mailVo.getMailCc().length];                
				for (int i = 0; i < mailVo.getMailCc().length; i++) {                        
					addressCc[i] = new InternetAddress(mailVo.getMailCc()[i]);   
					logMessage.append("Cc : " + addressCc[i] + "\n");
				} 
			}
			if(addressCc != null){
				msg.setRecipients(Message.RecipientType.CC, addressCc);
			}
			msg.setSubject(mailVo.getSubject());
			
			// Attachment Start
			Multipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setContent(mailVo.getMessage(),"text/html; charset=UTF-8");
	        //logMessage.append("Message : " +  mailVo.getMessage() + "\n");
	        multipart.addBodyPart(messageBodyPart);
			if(mailVo.getAttachment() != null && mailVo.getAttachment().trim().length() > 0){
				addAtachments(new String[]{mailVo.getAttachment()}, multipart);
			}
			// Attachment End
			
			msg.setContent(multipart);
			msg.saveChanges();
			Transport.send(msg);
			System.out.println("msg = "+logMessage.toString());
		} catch (AddressException addressExcep) {
			System.out.println(addressExcep);
		} catch (Exception exception) {
			System.out.println(exception);
		}
		return false;
	}
	/**
	 * @Method : 
	 * @Description : 
	 * @param attachments
	 * @param multipart
	 * @throws MessagingException
	 * @throws AddressException
	 */
	protected static void addAtachments(String[] attachments, Multipart multipart)
	
	            throws MessagingException, AddressException {
	
	        for (int i = 0; i <= attachments.length - 1; i++) {
	
	            String filename = attachments[i];
	
	            MimeBodyPart attachmentBodyPart = new MimeBodyPart();
	
	            //use a JAF FileDataSource as it does MIME type detection
	
	            DataSource source = new FileDataSource(filename);
	
	            attachmentBodyPart.setDataHandler(new DataHandler(source));
	
	            attachmentBodyPart.setFileName(filename.substring(filename.lastIndexOf(File.separator)+1, filename.length()));

	            //add the attachment
	
	            multipart.addBodyPart(attachmentBodyPart);
	
	        }
	    }
}
