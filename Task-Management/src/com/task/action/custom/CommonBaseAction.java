/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.action.custom;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionSupport;
import com.task.common.CommonVo;
import com.task.common.LanguageVo;
import com.task.common.UserVo;
import com.task.common.constant.ConstantEnum;
import com.task.common.constant.ResultConstant;
import com.task.util.ApplicationPagingVo;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.util.SessionHandler;
import com.task.vo.custom.action.UserLoginVo;

/**
 * 
 * @author Rajnarayan Saraf
 * 
 */
@SuppressWarnings("serial")
public class CommonBaseAction extends ActionSupport implements
		ServletRequestAware, ServletResponseAware {
	private static Logger log = LoggingAspectInterceptor
			.getLog(CommonBaseAction.class);
	protected HttpServletRequest servletRequest;
	protected HttpServletResponse servletResponse;
	protected ApplicationPagingVo pageValue;
	protected Map<String, String> authorizationMap;
	protected String userIdentity;
	protected String userNameSession;
	protected Integer userTypeSession;
	protected String associateCompanySession;
	protected UserVo userVoSession;
	protected Locale userLocaleSession;
	protected String errorMsg;
	protected String errorDetailMessage;
	protected String userSecureId;
	protected String resType;
	
	/** loginValue instance */
	private UserLoginVo loginValue;
	
	/** languageList instance to get form language */
	private List<LanguageVo> languageList;

	/**
	 * @Method : error 
	 * @Description : to display the error message
	 * for add new policy
	 * 
	 * @return String
	 * @throws Exception
	 */
	public String error() {
		if (getServletRequest().getSession().getAttribute(
				getText("ERROR_SESSION")) != null) {
			errorDetailMessage = (String) getServletRequest()
					.getSession().getAttribute(getText("ERROR_SESSION"));
		}
		getServletRequest().getSession().removeAttribute(
				getText("ERROR_SESSION"));
		return ResultConstant.RESULT_ERROR;
	}

	/**
	 * @Method : getPaggingParamValue
	 * 
	 * @Description : current page number setting
	 * 
	 * @return ApplicationPagingVo
	 */
	public ApplicationPagingVo getPaggingParamValue() {
		pageValue = new ApplicationPagingVo();
		if (servletRequest.getParameter("Page") != null) {
			pageValue.setCurrentPageNumber(Integer.valueOf(servletRequest
					.getParameter("Page")));

		}
		return pageValue;
	}

	/**
	 * @Method : getIntegerParamValue
	 * 
	 * @Description : get Integer value of parameter
	 * 
	 * @param inParam
	 * @return Integer
	 */
	public Integer getIntegerParamValue(String inParam) {
		if (null != getServletRequest().getParameter(inParam)
				&& !getServletRequest().getParameter(inParam).equals("")) {
			return Integer.parseInt(getServletRequest().getParameter(inParam));
		}
		return null;
	}

	/**
	 * Method Name : getLongParamValue Method Description : get Long value of
	 * parameter
	 * 
	 * @param inParam
	 * @return Long
	 */
	public Long getLongParamValue(String inParam) {
		if (null != getServletRequest().getParameter(inParam)
				&& !getServletRequest().getParameter(inParam).equals("")) {
			return Long.parseLong(getServletRequest().getParameter(inParam));
		}
		return null;
	}
	
	public Boolean getBooleanParamValue(String inParam) {
		if (null != getServletRequest().getParameter(inParam)
				&& !getServletRequest().getParameter(inParam).equals("")) {
			return Boolean.valueOf(getServletRequest().getParameter(inParam));
		}
		return null;
	}
	public String setToken(){
		UserVo userVo = (UserVo)getServletRequest().getSession().getAttribute("user");
		userVo.setToken(String.valueOf(new Date().getTime()));
		return userVo.getToken();
	}
	/**
	 * @Method : getStringParamValue 
	 * @Description : get String value of parameter
	 * 
	 * @param inParam
	 * @return String
	 */
	public String getStringParamValue(String inParam) {
		if (null != getServletRequest().getParameter(inParam)
				&& !getServletRequest().getParameter(inParam).equals("")) {
			return getServletRequest().getParameter(inParam);
		}
		return null;
	}

	/**
	 * Getter method for authorizationMap
	 * 
	 * @return the authorizationMap (Map<String, String>)
	 */
	public Map<String, String> getAuthorizationMap() {
		return SessionHandler.getSessionVo(getServletRequest())
				.getAuthorizationMap();
	}

	/**
	 * Getter method for SearchMap
	 * 
	 * @return the SearchMap (Map<String, CommonVo>)
	 */
	public Map<String, CommonVo> getSearchMap() {
		return SessionHandler.getSessionVo(getServletRequest())
				.getSearchMap();
	}

	/**
	 * Setter method for SearchMap
	 * 
	 * @param SearchMap the SearchMap to set
	 */
	public void setSearchMap( Map<String, CommonVo> searchMap) {
		SessionHandler.setSearchMap(getServletRequest(), searchMap);
	}


	/**
	 * Getter method for userIdentity
	 * 
	 * @return the userIdentity (String)
	 */
	public String getUserIdentity() {
		return userIdentity;
	}

	/**
	 * Setter method for userIdentity
	 * 
	 * @param userIdentity
	 *            the userIdentity to set
	 */
	public void setUserIdentity(String userIdentity) {
		this.userIdentity = userIdentity;
	}

	/**
	 * Getter method for userNameSession
	 * 
	 * @return String
	 */
	public String getUserNameSession() {
		return SessionHandler.getUserName(getServletRequest());
	}

	/**
	 * Getter method for associateCompanySession
	 * 
	 * @return String
	 */
	public String getAssociateCompanySession() {
		return SessionHandler.getAssociateCompany(getServletRequest());
	}

	/**
	 * Getter method for userTypeSession
	 * 
	 * @return String
	 */
	public Integer getUserTypeSession() {
		return SessionHandler.getUserType(getServletRequest());
	}

	/**
	 * Getter method for userSessionVo
	 * 
	 * @return the userSessionVo (UserVo)
	 */
	public UserVo getUserVoSession() {
		return SessionHandler.getUserDetails(getServletRequest());
	}

	/**
	 * Getter method for userSessionLocale
	 * 
	 * @return the userSessionLocale (Locale)
	 */
	public Locale getUserLocaleSession() {
		return SessionHandler.getSessionLocale(getServletRequest());
	}

	/**
	 * Getter method for servletRequest
	 * 
	 * @return the servletRequest (HttpServletRequest)
	 */
	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	/**
	 * Setter method for servletRequest
	 * 
	 * @param servletRequest
	 *            the servletRequest to set
	 */
	public void setServletRequest(HttpServletRequest servletRequest) {
		this.servletRequest = servletRequest;
	}

	/**
	 * Getter method for pageValue
	 * 
	 * @return the pageValue (ApplicationPagingVo)
	 */
	public ApplicationPagingVo getPageValue() {
		return pageValue;
	}

	/**
	 * Setter method for pageValue
	 * 
	 * @param pageValue
	 *            the pageValue to set
	 */
	public void setPageValue(ApplicationPagingVo pageValue) {
		this.pageValue = pageValue;
	}
	
	/**
	 * @Method : getLogMessage
	 * @Description : to get format for log msg
	 * @return String
	 */
	public String getLogMessage(){
		String msg = "";
		String companyId = "";
		UserVo userVo = getUserVoSession();
		if(userVo != null && userVo.getUserType() != null && userVo.getUserType() > 2){
			companyId = String.valueOf(userVo.getCompanyId());
			msg = FrameworkConstant.getLogMessage("ACTOR_DETAIL", getUserLocaleSession(), userVo.getUserName(),companyId);
		}
		return msg;
	}

	/**
	 * Getter method for servletResponse
	 * 
	 * @return the servletResponse (HttpServletResponse)
	 */
	public HttpServletResponse getServletResponse() {
		return this.servletResponse;
	}

	/**
	 * Setter method for servletResponse
	 *
	 * @param servletResponse the servletResponse to set
	 */
	public void setServletResponse(HttpServletResponse servletResponse) {
		this.servletResponse = servletResponse;
	}
	
	public String getLanguageKey() {
		Locale language = SessionHandler.getSessionLocale(getServletRequest());
		if(language != null)
			return language.getLanguage();
		else
			return ConstantEnum.JAPANESECODE.getStringValue();
	}

	/**
	 * 
	 * @Method : getLogoPath
	 * @Description : This Method is used to get Logo path.
	 * @return String
	 */
	public String getLogoPath() {
		UserVo userVo = getUserVoSession();
		if (null != userVo && null != userVo.getLogoPath()) {
			return userVo.getLogoPath();
		} else {
			return ConstantEnum.C_EMPTY.getStringValue();
		}
	}
	
	
	/**
	 * 
	 * @Method : getCompanyMsgColor
	 * @Description : This Method is used to get Company Msg Color
	 * @return String
	 */
	public String getCompanyMsgColor() {
		UserVo userVo = getUserVoSession();
		if (null != userVo && null != userVo.getCompanyMsgColor()) {
			return userVo.getCompanyMsgColor();
		} else {
			return ConstantEnum.C_EMPTY.getStringValue();
		}
	}
	
	/**
	 * 
	 * @Method : getSystemMsgColor
	 * @Description : This Method is used to System Msg Color
	 * @return String
	 */
	public String getSystemMsgColor() {
		UserVo userVo = getUserVoSession();
		if (null != userVo && null != userVo.getSystemMsgColor()) {
			return userVo.getSystemMsgColor();
		} else {
			return ConstantEnum.C_EMPTY.getStringValue();
		}
	}
	
	
	/**
	 * 
	 * @Method : getLoginUserName
	 * @Description : This Method is used to get LoginName.
	 * @return
	 */
	public String getLoginName() {
		UserVo userVo = getUserVoSession();

		if (null != userVo && null != userVo.getLoginName()) {
			return userVo.getLoginName();
		} else {
			return ConstantEnum.C_EMPTY.getStringValue();
		}
	}
	

	/**
	 * Getter method for errorMsg
	 * 
	 * @return the errorMsg (Locale)
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * Setter method for errorMsg
	 *
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	/**
	 * Getter method for errorDetailMessage
	 * 
	 * @return the errorDetailMessage (String)
	 */
	public String getErrorDetailMessage() {
		return errorDetailMessage;
	}

	/**
	 * Setter method for errorDetailMessage
	 *
	 * @param errorDetailMessage the errorDetailMessage to set
	 */
	public void setErrorDetailMessage(String errorDetailMessage) {
		this.errorDetailMessage = errorDetailMessage;
	}
	
	/**
	 * @Method : breakStringValue
	 * @Description :
	 * @param strValue
	 * @return String
	 */
	public String breakStringValue(String strValue,int maxValue) {
		String finalString = "";
		
		if(strValue.length() > maxValue){
			int noOfLines = strValue.length()/maxValue;
			
			if ((strValue.length() % maxValue) != 0){
				noOfLines++;
			}
			int strCount = 0;
			int strLastCount = maxValue;
			for(int index=0; index < noOfLines; index++){
				if(index+1 == noOfLines){
					finalString = finalString+strValue.substring(strCount, strValue.length());
					return finalString;
				}else{
					if(strLastCount > strValue.length()){
						finalString = finalString+strValue.substring(strCount, strValue.length());
						return finalString;
					}else{
						int indexPosition = strValue.substring(strCount, strLastCount).indexOf(" ");
						if(indexPosition != -1){
							if(indexPosition != strLastCount){
								finalString = finalString+strValue.substring(strCount, strLastCount)+"\n";
							}else{
								finalString = finalString + strValue.substring(strCount, 
										strCount+indexPosition)+"\n";
								strCount = strCount + indexPosition;
								strLastCount = strLastCount + indexPosition;
								if(strLastCount > strValue.length()){
									finalString = finalString+strValue.substring(strCount, strValue.length());
									return finalString;
								}else{
									finalString = finalString+strValue.substring(strCount, strLastCount)+"\n";
								}
							}
						}else{
							finalString = finalString+strValue.substring(strCount, strLastCount)+"\n";
						}
						strCount = strLastCount;
						strLastCount = strLastCount + maxValue;
					}
				}
			}
			
		}else{
			finalString = strValue;
			return finalString;
		}
		
		return finalString;
	}

	/**
	 * Getter method for userSecureId
	 * 
	 * @return the userSecureId (String)
	 */
	public String getUserSecureId() {
		return userSecureId;
	}

	/**
	 * Setter method for userSecureId
	 *
	 * @param userSecureId the userSecureId to set
	 */
	public void setUserSecureId(String userSecureId) {
		this.userSecureId = userSecureId;
	}

	/**
	 * Getter method for resType
	 * 
	 * @return the resType (String)
	 */
	public String getResType() {
		return resType;
	}

	/**
	 * Setter method for resType
	 *
	 * @param resType the resType to set
	 */
	public void setResType(String resType) {
		this.resType = resType;
	}
	
	
	/**
	 * @Method : getLanguageList
	 * 
	 * @Description : get language list from properties file
	 * 
	 * @return List<LanguageVo>
	 */
	public List<LanguageVo> getLanguageList() {
		
		List<LanguageVo> languages = new ArrayList<LanguageVo>();
		this.languageList = null;

		try {
			LanguageVo englishVo = new LanguageVo();
			LanguageVo japaneseVo = new LanguageVo();
			if (getLocale().getLanguage().equals(ConstantEnum.ENGLISHCODE.getStringValue())) {

				/** call ShiraiLabel_en_US properties */
				ResourceBundle bundle = ResourceBundle.getBundle("ShiraiLabel_en_US");
				englishVo.setLanguageCode(ConstantEnum.ENGLISHCODE.getStringValue());
				englishVo.setLanguageDisplay(bundle.getString("English"));
				japaneseVo.setLanguageCode(ConstantEnum.JAPANESECODE.getStringValue());
				japaneseVo.setLanguageDisplay(bundle.getString("Japanese"));
			} else {

				/** call ShiraiLabel_ja_JP properties */
				ResourceBundle bundle = ResourceBundle.getBundle("ShiraiLabel_ja_JP");
				englishVo.setLanguageCode(ConstantEnum.ENGLISHCODE.getStringValue());
				englishVo.setLanguageDisplay(bundle.getString("English"));
				japaneseVo.setLanguageCode(ConstantEnum.JAPANESECODE.getStringValue());
				japaneseVo.setLanguageDisplay(bundle.getString("Japanese"));
			}
			languages.add(englishVo);
			languages.add(japaneseVo);
		} catch (Exception e) {
			log.error(getLogMessage() + e.getLocalizedMessage());
		}
		this.languageList = languages;
		return languageList;
	}
	
	/**
	 * @Method : getSystemLaunguage
	 * 
	 * @Description : get system default language
	 */
	private void getSystemLaunguage() {
		try {
			Locale locale = getServletRequest().getLocale();
			loginValue = new UserLoginVo();
			if (locale.getLanguage().equals(ConstantEnum.JAPANESECODE.getStringValue())) {
				loginValue.setLanguage(locale.getLanguage());
			} else if (locale.getLanguage().equals(ConstantEnum.ENGLISHCODE.getStringValue())) {
				loginValue.setLanguage(locale.getLanguage());
			} else {
				loginValue.setLanguage(ConstantEnum.ENGLISHCODE.getStringValue());
			}
		} catch (Exception e) {
			log.error(getLogMessage() + e.getLocalizedMessage());
		}
	}

	/**
	 * Getter method for loginValue
	 * 
	 * @return the loginValue (UserLoginVo)
	 */
	public UserLoginVo getLoginValue() {
		return loginValue;
	}

	/**
	 * Setter method for loginValue
	 *
	 * @param loginValue the loginValue to set
	 */
	public void setLoginValue(UserLoginVo loginValue) {
		this.loginValue = loginValue;
	}
	
	/*public String getAwsURL(){
		String awsURL="";
		AwsConfiguration awsConfig = AwsConfiguration.getInstance();
		String awsEnable = awsConfig.getProperty("aws.enable.flag");
		if(awsEnable.equals("true")){
			awsURL = awsConfig.getProperty("aws.href.path");
		}
		return awsURL;
	}*/
	
	/*public String getAwsURLPhoto(){
		String awsURLPhoto="";
		AwsConfiguration awsConfig = AwsConfiguration.getInstance();
		String awsEnable = awsConfig.getProperty("aws.enable.flag");
		if(awsEnable.equals("true")){
			awsURLPhoto = awsConfig.getProperty("aws.href.path");
		}else{
			awsURLPhoto = awsConfig.getProperty("aws.href.photo.path");
		}
		return awsURLPhoto;
	}*/
	
}
