/**
 * Creater / durgeshkumarjhaSRM copyright information to Shirai 2013
 * 
 */
package com.task.action.custom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Preparable;
import com.task.common.ComboVo;
import com.task.common.CommonVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.UserVo;
import com.task.common.constant.AttributeConstant;
import com.task.common.constant.ConstantEnum;
import com.task.common.constant.ResultConstant;
import com.task.service.custom.LeaveService;
import com.task.service.custom.ProjectOwnerMappingService;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.vo.custom.action.LeaveActionVo;
import com.task.vo.custom.action.LeaveSearchVo;

/**
 * @author durgeshkumarjha
 *
 */
public class LeaveAction extends CommonBaseAction implements Preparable {

	/**
     * long serialVersionUID :
     */
    private static final long serialVersionUID = 1L;

    /** log instance for logging */
    private static Logger log = LoggingAspectInterceptor
	    .getLog(TaskAction.class);

    /** responseVo instance to get data from service layer */
    private ResponseVo responseVo;

	/** LeaveActionVo instance to get form data of leave */
	private LeaveActionVo leaveActionVo ;
	
	/** LeaveSearchVo instance to get form data of leave search condition */
	private LeaveSearchVo leaveSearchVo;
	
	/** displayMsg instance to display message */
	private String leaveMsg ;
    
    /** requestVo instance to send data to service layer */
    private RequestVo requestVo;
    
    private String token;
    private List<ComboVo> kindOfLeaveList 		= new ArrayList<ComboVo>();
    private List<ComboVo> leaveStatusList 		= new ArrayList<ComboVo>();
    private List<ComboVo> employeeList			= new ArrayList<ComboVo>();
    private List<ComboVo> pmLeaveCountList 		= new ArrayList<ComboVo>();
    private List<ComboVo> projectManagerList 	= new ArrayList<ComboVo>();
    private List<ComboVo> approvedLeaveList 	= new ArrayList<ComboVo>();
    
    /**
     * 
     * @Method : searchLeave
     * @Description : This method is used for searching leave based on given search condition.
     * @return
     */
    public String searchLeave() {
		try {
			log.debug(getText("METHOD_LOG_START") + getText("searchLeave"));
			/* Set the SearchVo values in Session */
			token = setToken();
			if (null != leaveSearchVo) {
				if ((null != leaveSearchVo.getFromDateStr() && !leaveSearchVo.getFromDateStr().trim().equals(""))
						|| (null != leaveSearchVo.getToDateStr() && !leaveSearchVo.getToDateStr().trim().equals("")) ) {
					Map<String, CommonVo> searchMap = new HashMap<String, CommonVo>();
					searchMap.put(AttributeConstant.LEAVE_SEARCH_VO, leaveSearchVo);
					setSearchMap(searchMap);
				} else {
					Map<String, CommonVo> searchsession = getSearchMap();
					if (null != searchsession && !searchsession.isEmpty()) {
						searchsession.remove(AttributeConstant.LEAVE_SEARCH_VO);
					}
				}
			}
			/* After search calling the listLeave() */
			leaveMsg = null;
			log.debug(getText("METHOD_LOG_END") + getText("searchLeave"));
			return listLeave();
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			return ResultConstant.RESULT_ERROR;
		}
	}
    
   /**
    * 
    * @Method : applyLeave
    * @Description : This method is used for open apply leave page.
    * @return
    */
	public String applyLeave() {
		token = setToken();
		log.debug(getText("METHOD_LOG_START") + getText("applyLeave"));
		log.debug(getText("METHOD_LOG_END") + getText("applyLeave"));
		if(employeeList!=null && employeeList.size()==1){
			if(leaveActionVo==null){
				leaveActionVo = new LeaveActionVo();
			}
			leaveActionVo.setReportingManager(employeeList.get(0).getKey());
	    }
		return "applyLeave";
	
	}
	
	/**
	 * 
	 * @Method : saveLeave
	 * @Description : This method is used for apply leave.
	 * @return
	 */
	public String saveLeave() {
		try {
			log.debug(getText("METHOD_LOG_START") + getText("saveLeave"));
			UserVo userVo = requestVo.getUserVo();
//			if(userVo.getToken().equals(token)){
				
				leaveActionVo.setCreatedBy(userVo.getEmployeeId());
				leaveActionVo.setCreatedOn(CommonUtils.getCurrentDate());

				leaveActionVo.setEmployeeId(Integer.parseInt(userVo.getEmployeeId()));
				String []pmArray = leaveActionVo.getReportingManager().split("-");
				leaveActionVo.setReportingManager(pmArray[0]);
				
				requestVo.setCommonVo(leaveActionVo);
	
				/* creating instance for service class bean */
				LeaveService leaveService =
						(LeaveService) ContextLoader.getBean(FrameworkConstant
								.getBeanId("LEAVE_SERVICE"));
	
				/* Calling service class saveCompany method */
				responseVo = leaveService.saveLeave(requestVo);
				
				if (null != responseVo.getTransactionMessage()){
					leaveMsg = responseVo.getTransactionMessage();
					addActionMessage(leaveMsg);
					log.info(getLogMessage() + leaveMsg);
				}
				log.debug(getText("METHOD_LOG_END") + getText("saveLeave"));
				comboLoading();
				
//			}
			return listLeave();
//			else{
//				return "saveLeave";
//			}
			
		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"),
					ce.getLogMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			return "saveLeave";
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), e.getMessage());
			return "saveLeave";
		}
	}
    
	/**
	 * 
	 * @Method : approveLeave
	 * @Description : This method is used for get leave detail for approval.
	 * @return
	 */
	public String approveLeave() {
		try {
			token = setToken();
			log.debug(getText("METHOD_LOG_START") + getText("approveLeave"));
			leaveActionVo = new LeaveActionVo();
			leaveActionVo.setEmpLeaveDetailId(super.getIntegerParamValue("hiddenEmpLeaveDetailId").intValue());

			requestVo.setCommonVo(leaveActionVo);

			 /* creating instance for service class bean in getCompany method */
			responseVo = getLeaveDetail(requestVo);

			leaveActionVo = (LeaveActionVo) responseVo.getCommonVo();

			log.debug(getText("METHOD_LOG_END") + getText("approveLeave"));
		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"),
					ce.getLogMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			return "approveLeave";
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), e.getMessage());
			return "approveLeave";
		}
		return "approveLeave";
	}
	
	/**
	 * 
	 * @Method : getLeaveDetail
	 * @Description : This method is used for get leave detail for approval.
	 * @param requestVo2
	 * @return
	 * @throws Exception
	 */
	private ResponseVo getLeaveDetail(RequestVo requestVo2) throws Exception{
		/* creating instance for service class bean */
		LeaveService leaveService =
				(LeaveService) ContextLoader.getBean(FrameworkConstant
						.getBeanId("LEAVE_SERVICE"));
		return leaveService.getLeaveDetail(requestVo);
	}
	
	/**
	 * 
	 * @Method : This method is used for approve applied leave.
	 * @Description : 
	 * @return
	 */
	public String updateLeave() {
		try {
			log.debug(getText("METHOD_LOG_START") + getText("updateLeave"));

			requestVo.setCommonVo(leaveActionVo);

			/* creating instance for service class bean */
			LeaveService leaveService =
					(LeaveService) ContextLoader.getBean(FrameworkConstant
							.getBeanId("LEAVE_SERVICE"));

			/* Calling service class updateCompany method */
			responseVo = leaveService.updateLeave(requestVo);

			if (null != responseVo.getTransactionMessage()){
				leaveMsg = responseVo.getTransactionMessage();
				addActionMessage(leaveMsg);
				log.info(getLogMessage() + leaveMsg);
			}

			log.debug(getText("METHOD_LOG_END") + getText("updateLeave"));
		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"),
					ce.getLogMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), e.getMessage());
		}
//		return "updateLeave";
		return listLeave();
	}
	
	/**
	 * 
	 * @Method : listLeave
	 * @Description : This method is used for getting all applied leave list.
	 * @return
	 */
	public String listLeave() {
		try {
			log.debug(getText("METHOD_LOG_START") + getText("listLeave"));
			
			if (null == leaveSearchVo) {
				leaveSearchVo = new LeaveSearchVo();
			}	

			Map<String, CommonVo> searchsession = getSearchMap();

			if (null != searchsession && !searchsession.isEmpty() && searchsession.get(AttributeConstant.LEAVE_SEARCH_VO) != null) {
				leaveSearchVo = (LeaveSearchVo) searchsession.get(AttributeConstant.LEAVE_SEARCH_VO);
			} 

			/* Pagination */
			getPaggingParamValue();
			//requestVo = new RequestVo();
			requestVo.setPagingVo(super.pageValue);
			requestVo.setSeacrhVo(leaveSearchVo);
			

			// creating instance for leaveService class bean
			LeaveService leaveService =
					(LeaveService) ContextLoader.getBean(FrameworkConstant
							.getBeanId("LEAVE_SERVICE"));

			// calling the leaveService class
			responseVo = (ResponseVo) leaveService.listLeave(requestVo);

			/*if (leaveMsg != null) {
				addActionMessage(leaveMsg);
			}*/
			if (responseVo.getErrorMessage() != null
					&& !ConstantEnum.C_EMPTY.getStringValue().equals(
							responseVo.getErrorMessage().trim())) {
				leaveMsg = null;
				addActionError(responseVo.getErrorMessage());
			}
			log.debug(getText("METHOD_LOG_END") + getText("listLeave"));

			return "listLeave";
		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"),
					ce.getLogMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			setErrorDetailMessage(ce.getLogMessage());
			return ResultConstant.RESULT_ERROR;

		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			setErrorDetailMessage(e.getMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), e.getMessage());
			return ResultConstant.RESULT_ERROR;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.Preparable#prepare()
	 */
	@Override
	public void prepare() throws Exception {
    	if (requestVo == null) {
			requestVo = new RequestVo();
			UserVo userVo = (UserVo)getServletRequest().getSession().getAttribute("user");
			requestVo.setUserVo(userVo);
		}
	    comboLoading();
    }
	
	/**
	 * 
	 * @Method : comboLoading
	 * @Description : This method is used for load combo.
	 * @throws CustomApplicationException
	 */
	private void comboLoading() throws CustomApplicationException{
    	
		LeaveService leaveService =
				(LeaveService) ContextLoader.getBean(FrameworkConstant.getBeanId("LEAVE_SERVICE"));
		
		ProjectOwnerMappingService mappingService =
				(ProjectOwnerMappingService) ContextLoader.getBean(FrameworkConstant
						.getBeanId("PROJECT_OWNER_MAPPING_SERVICE"));
		
		if (requestVo == null) {
			requestVo = new RequestVo();
			UserVo userVo = (UserVo)getServletRequest().getSession().getAttribute("user");
			requestVo.setUserVo(userVo);
		}
		
    	requestVo.setComboType("LeaveStatus");
		responseVo = leaveService.comboLoading(requestVo);
		leaveStatusList = (List<ComboVo>) responseVo.getComboValueList();
		
		requestVo.setComboType("KindOfLeave");
		responseVo = leaveService.comboLoading(requestVo);
		kindOfLeaveList = (List<ComboVo>) responseVo.getComboValueList();
	
		requestVo.setComboType("PMListForLeave");
	    responseVo = mappingService.comboLoading(requestVo);
	    employeeList = (List<ComboVo>) responseVo.getComboValueList();
	    
	    requestVo.setComboType("ProjectManagerList");
		responseVo = leaveService.comboLoading(requestVo);
		projectManagerList = (List<ComboVo>) responseVo.getComboValueList();
	    
		requestVo.setComboType("approvedLeaveList");
		responseVo = leaveService.comboLoading(requestVo);
		approvedLeaveList = (List<ComboVo>) responseVo.getComboValueList();
		
    }

	/** 
	 * Getter method for responseVo
	 * 
	 * @return the responseVo (ResponseVo)
	 */
	public ResponseVo getResponseVo() {
		return responseVo;
	}

	/**
	 * Setter method for responseVo
	 *
	 * @param responseVo the responseVo to set
	 */
	public void setResponseVo(ResponseVo responseVo) {
		this.responseVo = responseVo;
	}

	/**
	 * Getter method for leaveActionVo
	 * 
	 * @return the leaveActionVo (LeaveActionVo)
	 */
	public LeaveActionVo getLeaveActionVo() {
		return leaveActionVo;
	}

	/**
	 * Setter method for leaveActionVo
	 *
	 * @param leaveActionVo the leaveActionVo to set
	 */
	public void setLeaveActionVo(LeaveActionVo leaveActionVo) {
		this.leaveActionVo = leaveActionVo;
	}

	/**
	 * Getter method for leaveMsg
	 * 
	 * @return the leaveMsg (String)
	 */
	public String getLeaveMsg() {
		return leaveMsg;
	}

	/**
	 * Setter method for leaveMsg
	 *
	 * @param leaveMsg the leaveMsg to set
	 */
	public void setLeaveMsg(String leaveMsg) {
		this.leaveMsg = leaveMsg;
	}

	/**
	 * Getter method for requestVo
	 * 
	 * @return the requestVo (RequestVo)
	 */
	public RequestVo getRequestVo() {
		return requestVo;
	}

	/**
	 * Setter method for requestVo
	 *
	 * @param requestVo the requestVo to set
	 */
	public void setRequestVo(RequestVo requestVo) {
		this.requestVo = requestVo;
	}

	/**
	 * Getter method for token
	 * 
	 * @return the token (String)
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Setter method for token
	 *
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * Getter method for kindOfLeaveList
	 * 
	 * @return the kindOfLeaveList (List<ComboVo>)
	 */
	public List<ComboVo> getKindOfLeaveList() {
		return kindOfLeaveList;
	}

	/**
	 * Setter method for kindOfLeaveList
	 *
	 * @param kindOfLeaveList the kindOfLeaveList to set
	 */
	public void setKindOfLeaveList(List<ComboVo> kindOfLeaveList) {
		this.kindOfLeaveList = kindOfLeaveList;
	}

	/**
	 * Getter method for employeeList
	 * 
	 * @return the employeeList (List<ComboVo>)
	 */
	public List<ComboVo> getEmployeeList() {
		return employeeList;
	}

	/**
	 * Setter method for employeeList
	 *
	 * @param employeeList the employeeList to set
	 */
	public void setEmployeeList(List<ComboVo> employeeList) {
		this.employeeList = employeeList;
	}

	/**
	 * Getter method for leaveSearchVo
	 * 
	 * @return the leaveSearchVo (LeaveSearchVo)
	 */
	public LeaveSearchVo getLeaveSearchVo() {
		return leaveSearchVo;
	}

	/**
	 * Setter method for leaveSearchVo
	 *
	 * @param leaveSearchVo the leaveSearchVo to set
	 */
	public void setLeaveSearchVo(LeaveSearchVo leaveSearchVo) {
		this.leaveSearchVo = leaveSearchVo;
	}

	/**
	 * Getter method for leaveStatusList
	 * 
	 * @return the leaveStatusList (List<ComboVo>)
	 */
	public List<ComboVo> getLeaveStatusList() {
		return leaveStatusList;
	}

	/**
	 * Setter method for leaveStatusList
	 *
	 * @param leaveStatusList the leaveStatusList to set
	 */
	public void setLeaveStatusList(List<ComboVo> leaveStatusList) {
		this.leaveStatusList = leaveStatusList;
	}

	/**
	 * Getter method for pmLeaveCountList
	 * 
	 * @return the pmLeaveCountList (List<ComboVo>)
	 */
	public List<ComboVo> getPmLeaveCountList() {
		return pmLeaveCountList;
	}

	/**
	 * Setter method for pmLeaveCountList
	 *
	 * @param pmLeaveCountList the pmLeaveCountList to set
	 */
	public void setPmLeaveCountList(List<ComboVo> pmLeaveCountList) {
		this.pmLeaveCountList = pmLeaveCountList;
	}

	/**
	 * Getter method for projectManagerList
	 * 
	 * @return the projectManagerList (List<ComboVo>)
	 */
	public List<ComboVo> getProjectManagerList() {
		return projectManagerList;
	}

	/**
	 * Setter method for projectManagerList
	 *
	 * @param projectManagerList the projectManagerList to set
	 */
	public void setProjectManagerList(List<ComboVo> projectManagerList) {
		this.projectManagerList = projectManagerList;
	}

	/**
	 * Getter method for approvedLeaveList
	 * 
	 * @return the approvedLeaveList (List<ComboVo>)
	 */
	public List<ComboVo> getApprovedLeaveList() {
		return approvedLeaveList;
	}

	/**
	 * Setter method for approvedLeaveList
	 *
	 * @param approvedLeaveList the approvedLeaveList to set
	 */
	public void setApprovedLeaveList(List<ComboVo> approvedLeaveList) {
		this.approvedLeaveList = approvedLeaveList;
	}

	
}
