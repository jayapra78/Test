/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.action.custom;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.constant.AttributeConstant;
import com.task.common.constant.ConstantEnum;
import com.task.common.constant.ResultConstant;
import com.task.service.custom.LoginService;
import com.task.util.ContextLoader;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.vo.custom.action.UserLoginVo;

/**
 * Action class used to validate login user
 * 
 * @author Rajnarayan Saraf
 */
@Validations
public class LoginAction extends CommonBaseAction implements Preparable {

	/** serialVersionUID instance */
	private static final long serialVersionUID = 1L;

	/** log instance for logging */
	private static Logger log = LoggingAspectInterceptor.getLog(LoginAction.class);

	/** loginValue instance */
	private UserLoginVo loginValue;

	/** responseVo instance to get data from service layer */
	private ResponseVo responseVo;

	/** requestVo instance to send data to service layer */
	private RequestVo requestVo;

	/** loginValue instance to get locale */

	/**
	 * @Method : login
	 * 
	 * @Description : to show login screen
	 * 
	 * @return String
	 */
	@SkipValidation
	public String login() {
		return ResultConstant.RESULT_LOGIN_US;
	}

	
	/**
	 * @Method : checkUser
	 * @Description : check the user is exist or not
	 * @return String
	 */
	public String checkUser() {
	    try {
		if(null == loginValue || "".equals(loginValue.getLoginName()) || "".equals(loginValue.getLoginName().trim())){
		    addActionError("User name is required");
		    return ERROR;
		}
		else if(null == loginValue || "".equals(loginValue.getPassword()) || "".equals(loginValue.getPassword().trim())){
		    addActionError("Password is required");
		    return ERROR;
		}
		else{
		    UserLoginVo userLoginVo = new UserLoginVo();
		    userLoginVo.setLoginName(loginValue.getLoginName());
		    userLoginVo.setPassword(loginValue.getPassword());
		    requestVo.setCommonVo(userLoginVo);
		    return authenticateUser();
		}
		
	    }catch (Exception ce) {
		log.error(getLogMessage() + ce.getLocalizedMessage());
		return ERROR;
	    }
	}

	/**
	 * @Method : checkUser
	 * 
	 * @Description : check the user is exist or not
	 * 
	 * @return String
	 */
//	public String checkUser() {
//		String returnPages = null;
//		try {
//			/** check Validation */
//			if (null != loginValue ) {
//				
//				if (loginValue.getLoginName().trim().equals("")) {
//					if (loginValue.getPassword().trim().equals("")) {
//
//						/** Validate a Login Name & Password is empty or not */
//						returnPages = checkUserValidation(loginValue);
//					} else {
//
//						/** Validate a Login Name is empty or not */
//						returnPages = checkUserValidation(loginValue);
//					}
//				} else if (loginValue.getPassword().trim().equals("")) {
//
//					/** Validate a Password is empty or not */
//					returnPages = checkUserValidation(loginValue);
//
//				} else if (loginValue.getLoginName().trim().length() < 5
//						|| loginValue.getPassword().trim().length() < 4) {
//					if (loginValue.getPassword().trim().length() < 4) {
//
//						/** Validate a Login Name & Password is length more than 4 */
//						returnPages = checkUserLengthValidation(loginValue);
//					} else {
//
//						/** Validate a Login Name & Password is length more than 4 */
//						returnPages = checkUserLengthValidation(loginValue);
//					}
//				} else {
//
//					/** call authenticateUser of LoginService */
//					requestVo.setCommonVo(loginValue);
//					returnPages = authenticateUser();
//				}
//				
//			}
//		} catch (Exception ce) {
//			log.error(getLogMessage() + ce.getLocalizedMessage());
//			getServletRequest().getSession()
//					.setAttribute(getText("ERROR_SESSION"), ce.getMessage());
//			setErrorMsg(ResultConstant.ADMIN_ERROR);
//			setErrorDetailMessage(ce.getMessage());
//			return ResultConstant.RESULT_LOGIN_ERRORS;
//		}
//		return returnPages;
//	}
	
	/**
	 * @Method : authenticateUser
	 * 
	 * @Description : this method is for authenticate login user
	 *  
	 * @return
	 */
	public String authenticateUser() {
		try {
			/** creating instance for service class bean */
			LoginService loginservice = (LoginService) ContextLoader.getBean(FrameworkConstant
							.getBeanId("SERVICE_LOGIN"));
			
			responseVo = (ResponseVo) loginservice.authenticateUser(requestVo);
			if (responseVo == null || !responseVo.isStatusFlag()) {
			    addActionError("Invalid User Name or Password");
			    return ERROR;
			} else{
				getServletRequest().getSession().setAttribute("user",responseVo.getSessionVo().getUserVo());
			    getServletRequest().getSession().setAttribute(AttributeConstant.SECURE_ACCESS_ID,AttributeConstant.TMS);
			    getServletRequest().getSession().setAttribute(AttributeConstant.TMS, responseVo.getSessionVo());
			    return SUCCESS;
			}

		} catch (Exception e) {
		    addActionError("Invalid user name and password");
		    return ERROR;
		}
	}


	/**
	 * @Method : logOut
	 * 
	 * @Description : log out the user
	 * 
	 * @return String
	 */
	public String logOut() {
		return ResultConstant.RESULT_GOLOGIN;
	}
	
	
	/**
	 * @Method : checkUserValidation
	 * 
	 * @Description : This Method to validate a login id.
	 * 
	 * @param loginValue
	 * 
	 * @return
	 */
	public String checkUserValidation(UserLoginVo loginValue) {
		String returnPages;
			ResourceBundle bundle = ResourceBundle.getBundle("package_en_US");
			if (loginValue.getLoginName().trim().equals(""))
				addFieldError("loginValue.loginName", bundle.getString("Login.error.message"));
			if (loginValue.getPassword().trim().equals(""))
				addFieldError("loginValue.password", bundle.getString("Password.error.message"));
			returnPages = ERROR;
		return returnPages;
	}

	/**
	 * @Method : checkUserLengthValidation
	 * 
	 * @Description : this Method to validate a Login Name length & Password length.
	 * 
	 * @param loginValue
	 * 
	 * @return
	 */
	public String checkUserLengthValidation(UserLoginVo loginValue) {
		String returnPages;
		if (loginValue.getLanguage().equals(ConstantEnum.ENGLISHCODE.getStringValue())) {
			ResourceBundle bundle = ResourceBundle.getBundle("package_en_US");
			if (loginValue.getLoginName().trim().length() < 5)
				addFieldError("loginValue.loginName", bundle.getString("LoginSize.error.message"));

			if (loginValue.getPassword().trim().length() < 4)
				addFieldError("loginValue.password", bundle.getString("PasswordSize.error.message"));
			returnPages = ResultConstant.RESULT_LOGIN_US;
		} else {
			ResourceBundle bundle = ResourceBundle.getBundle("package_ja_JP");

			if (loginValue.getLoginName().trim().length() < 5)
				addFieldError("loginValue.loginName", bundle.getString("LoginSize.error.message"));

			if (loginValue.getPassword().trim().length() < 4)
				addFieldError("loginValue.password", bundle.getString("PasswordSize.error.message"));
			returnPages = ResultConstant.RESULT_LOGIN_JP;
		}
		return returnPages;
	}
	
	/**
	 * The Method to get loginValue
	 * 
	 * @return loginValue
	 */
	public UserLoginVo getLoginValue() {
		return loginValue;
	}

	/**
	 * The Method to set loginValue
	 * 
	 * @param loginValue
	 *            loginValue
	 */
	public void setLoginValue(UserLoginVo loginValue) {
		this.loginValue = loginValue;
	}

	

	/**
	 * Getter method for responseVo
	 * 
	 * @return the responseVo (ResponseVo)
	 */
	public ResponseVo getResponseVo() {
		return responseVo;
	}

	/**
	 * Setter method for responseVo
	 * 
	 * @param responseVo
	 *            the responseVo to set
	 */
	public void setResponseVo(ResponseVo responseVo) {
		this.responseVo = responseVo;
	}

	/**
	 * Getter method for requestVo
	 * 
	 * @return the requestVo (RequestVo)
	 */
	public RequestVo getRequestVo() {
		return requestVo;
	}

	/**
	 * Setter method for requestVo
	 * 
	 * @param requestVo
	 *            the requestVo to set
	 */
	public void setRequestVo(RequestVo requestVo) {
		this.requestVo = requestVo;
	}

	/**
	 * @Method : prepare
	 * @Description : default values
	 * 
	 */
	@Override
	public void prepare() {
		try {
			if (requestVo == null) {
				requestVo = new RequestVo();
			}
		} catch (Exception e) {
			log.error(getLogMessage() + e.getLocalizedMessage());
		}
	}
}
