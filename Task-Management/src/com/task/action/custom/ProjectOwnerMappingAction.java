/**
 * Creater / DurgeshKumarJha[SRM] copyright information to SRM Technologies 2013
 *
 */
package com.task.action.custom;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.opensymphony.xwork2.Preparable;
import com.task.common.ComboVo;
import com.task.common.CommonVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.UserVo;
import com.task.common.constant.AttributeConstant;
import com.task.common.constant.ResultConstant;
import com.task.service.custom.ProjectOwnerMappingService;
import com.task.util.CommonConstants;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.vo.custom.action.ProjectOwnerMappingActionVo;
import com.task.vo.custom.action.ProjectOwnerSearchVo;
import com.task.vo.custom.action.ProjectTypeActionVo;

/**
 * This Action class is used to Add, Update, View, Search and Delete Project
 * Owner Mapping.
 *
 * @author DurgeshKumarJha[SRM]
 * @since 22/02/2013
 *
 */
public class ProjectOwnerMappingAction extends CommonBaseAction implements Preparable {

	/**
	 * long serialVersionUID :
	 */
	private static final long serialVersionUID = 1L;

	/** log instance for logging */
	private static Logger log = LoggingAspectInterceptor.getLog(TaskAction.class);

	/** responseVo instance to get data from service layer */
	private ResponseVo responseVo;

	/** companySearchVo instance to get form data of company search condition */
	private ProjectOwnerSearchVo projectOwnerSearchVo = null;

	/** companyActionVo instance to get form data of company */
	private ProjectOwnerMappingActionVo projectOwnerMappingActionVo;

	/** displayMsg instance to display message */
	private String projectOwnerMappingMsg;

	/** requestVo instance to send data to service layer */
	private RequestVo requestVo;

	private String projectCodeId;

	private List<ProjectOwnerMappingActionVo> projectMappingList;

	private List<ProjectTypeActionVo> projectTypeVoList = new ArrayList<ProjectTypeActionVo>();;
	private List<ComboVo> projectList = new ArrayList<ComboVo>();
	private List<ComboVo> associatedProjectList = new ArrayList<ComboVo>();
	private List<ComboVo> pmNameList = new ArrayList<ComboVo>();
	private List<ComboVo> plNameList = new ArrayList<ComboVo>();
	private List<ComboVo> mappedProject = new ArrayList<ComboVo>();
	private List<ComboVo> projectStatusList = new ArrayList<ComboVo>();
	private List<ComboVo> employeeList = new ArrayList<ComboVo>();
	private String projectId;
	private String token;
	private Date associatedFrom;
	private String currentDate;

	/**
	 * @Method : searchProjectOwnerMapping
	 * @Description : Project Owner Mapping search based on conditions
	 * @return String
	 * @throws Exception
	 */
	public String searchProjectOwnerMapping() {
		try {
			log.debug(getText("METHOD_LOG_START") + getText("searchProjectOwnerMapping"));
			/* Set the SearchVo values in Session */
			token = setToken();
			if (null != projectOwnerSearchVo) {
				if ((null != projectOwnerSearchVo.getProjectTypeId()) || (null != projectOwnerSearchVo.getProjectCode())
						|| (null != projectOwnerSearchVo.getPlId() && !projectOwnerSearchVo.getPlId().trim().equals(""))
						|| (null != projectOwnerSearchVo.getPlName()
								&& !projectOwnerSearchVo.getPlName().trim().equals(""))
						|| (null != projectOwnerSearchVo.getPmId() && !projectOwnerSearchVo.getPmId().trim().equals(""))
						|| (null != projectOwnerSearchVo.getPmName()
								&& !projectOwnerSearchVo.getPmName().trim().equals(""))) {
					Map<String, CommonVo> searchMap = new HashMap<String, CommonVo>();
					searchMap.put(AttributeConstant.PROJECT_OWNER_MAPPING_SEARCH_VO, projectOwnerSearchVo);
					setSearchMap(searchMap);
				} else {
					Map<String, CommonVo> searchsession = getSearchMap();
					if (null != searchsession && !searchsession.isEmpty()) {
						searchsession.remove(AttributeConstant.PROJECT_OWNER_MAPPING_SEARCH_VO);
					}
				}
			}
			/* After search calling the listCompany() */
			projectOwnerMappingMsg = null;
			log.debug(getText("METHOD_LOG_END") + getText("searchProjectOwnerMapping"));
			return listProjectOwnerMapping();
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			return ResultConstant.RESULT_ERROR;
		}
	}

	/**
	 *
	 * @Method : listProjectOwnerMapping
	 * @Description : Method is used for displaying list.
	 * @return
	 */
	public String listProjectOwnerMapping() {

		try {
			token = setToken();
			log.debug(getText("METHOD_LOG_START") + getText("listProjectOwnerMapping"));

			if (null == projectOwnerSearchVo) {
				projectOwnerSearchVo = new ProjectOwnerSearchVo();
			}

			Map<String, CommonVo> searchsession = getSearchMap();

			if (null != searchsession && !searchsession.isEmpty()
					&& searchsession.get(AttributeConstant.PROJECT_OWNER_MAPPING_SEARCH_VO) != null) {
				projectOwnerSearchVo = (ProjectOwnerSearchVo) searchsession
						.get(AttributeConstant.PROJECT_OWNER_MAPPING_SEARCH_VO);
			}

			/* Pagination */
			getPaggingParamValue();
			// requestVo = new RequestVo();
			requestVo.setPagingVo(super.pageValue);
			requestVo.setSeacrhVo(projectOwnerSearchVo);

			/* creating instance for service class bean */
			ProjectOwnerMappingService projectOwnerMappingService = (ProjectOwnerMappingService) ContextLoader
					.getBean(FrameworkConstant.getBeanId("PROJECT_OWNER_MAPPING_SERVICE"));

			/* return the List of Company Record in responseVo */
			responseVo = (ResponseVo) projectOwnerMappingService.listProjectOwnerMapping(requestVo);

			if (null != projectOwnerMappingMsg && !projectOwnerMappingMsg.equals(""))
				addActionMessage(projectOwnerMappingMsg);

			if (null != responseVo.getErrorMessage() && !responseVo.getErrorMessage().equals(""))
				addActionError(responseVo.getErrorMessage());

			log.debug(getText("METHOD_LOG_END") + getText("listProjectOwnerMapping"));
			return "listProjectOwnerMapping";

		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			setErrorDetailMessage(ce.getLogMessage());
			return ResultConstant.RESULT_ERROR;

		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			setErrorDetailMessage(e.getMessage());
			return ResultConstant.RESULT_ERROR;
		}

	}

	/**
	 *
	 * @Method : addProjectOwnerMapping
	 * @Description : This method is used for open project mapping add form
	 * @return
	 */
	public String addProjectOwnerMapping() {
		token = setToken();
		log.debug(getText("METHOD_LOG_START") + getText("addProjectOwnerMapping"));
		log.debug(getText("METHOD_LOG_END") + getText("addProjectOwnerMapping"));
		currentDate = CommonUtils.convertdateTostring(CommonUtils.getCurrentDateWithoutTime(), "dd/MM/yyyy");
		if (projectOwnerMappingActionVo == null) {
			projectOwnerMappingActionVo = new ProjectOwnerMappingActionVo();
		}
		if (projectOwnerMappingActionVo.getAssociatedFromStr() == null) {
			projectOwnerMappingActionVo.setAssociatedFromStr(currentDate);
		}

		return "addProjectOwnerMapping";
	}

	/**
	 *
	 * @Method : saveProjectOwnerMapping
	 * @Description : This method is used to add new project mapping.
	 * @return
	 */
	public String saveProjectOwnerMapping() {
		try {
			currentDate = CommonUtils.convertdateTostring(CommonUtils.getCurrentDateWithoutTime(), "dd/MM/yyyy");
			log.debug(getText("METHOD_LOG_START") + getText("saveProjectOwnerMapping"));
			UserVo userVo = requestVo.getUserVo();
			if (userVo.getToken().equals(token)) {

				projectOwnerMappingActionVo.setCreatedBy(getUserNameSession());
				projectOwnerMappingActionVo.setCreatedOn(CommonUtils.getCurrentDate());

				String[] pmArray = projectOwnerMappingActionVo.getPmId().split("-");
				projectOwnerMappingActionVo.setPmId(pmArray[0]);
				projectOwnerMappingActionVo.setPmName(pmArray[1]);

				if (!projectOwnerMappingActionVo.getPlId().isEmpty()) {
					String[] plArray = projectOwnerMappingActionVo.getPlId().split("-");
					projectOwnerMappingActionVo.setPlId(plArray[0]);
					projectOwnerMappingActionVo.setPlName(plArray[1]);
				}
				projectOwnerMappingActionVo.setDeptId(userVo.getDeptId());

				// Customer mail flag
				if (null != projectOwnerMappingActionVo.getStatusMail()
						&& projectOwnerMappingActionVo.getStatusMail() == 't') {
					projectOwnerMappingActionVo.setStatusMail(CommonConstants.STATUS_MAIL_ON);
				}
				if (null != projectOwnerMappingActionVo.getStatusMail()
						&& projectOwnerMappingActionVo.getStatusMail() == 'f') {
					projectOwnerMappingActionVo.setStatusMail(CommonConstants.STATUS_MAIL_OFF);
				}
				if (null != projectOwnerMappingActionVo.getConsolidatedMail()
						&& projectOwnerMappingActionVo.getConsolidatedMail() == 't') {
					projectOwnerMappingActionVo.setConsolidatedMail(CommonConstants.CONSOLIDATE_MAIL_ON);
				}
				if (null != projectOwnerMappingActionVo.getConsolidatedMail()
						&& projectOwnerMappingActionVo.getConsolidatedMail() == 'f') {
					projectOwnerMappingActionVo.setConsolidatedMail(CommonConstants.CONSOLIDATE_MAIL_OFF);
				}

				requestVo.setCommonVo(projectOwnerMappingActionVo);

				/* creating instance for service class bean */
				ProjectOwnerMappingService projectOwnerMappingService = (ProjectOwnerMappingService) ContextLoader
						.getBean(FrameworkConstant.getBeanId("PROJECT_OWNER_MAPPING_SERVICE"));

				/* Calling service class saveCompany method */
				responseVo = projectOwnerMappingService.saveProjectOwnerMapping(requestVo);
				if (null != responseVo.getTransactionMessage()) {
					projectOwnerMappingMsg = responseVo.getTransactionMessage();
					log.info(getLogMessage() + projectOwnerMappingMsg);
				}
				log.debug(getText("METHOD_LOG_END") + getText("saveProjectOwnerMapping"));
				comboLoading();
				return listProjectOwnerMapping();
			} else {
				return listProjectOwnerMapping();
			}
		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), ce.getLogMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			return "addProjectOwnerMapping";
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), e.getMessage());
			return "addProjectOwnerMapping";
		}
	}

	/**
	 * @Method : modifyProjectOwnerMapping
	 * @Description : This method is used to open modify form with selected
	 *              record data.
	 * @return
	 */
	public String modifyProjectOwnerMapping() {
		try {
			currentDate = CommonUtils.convertdateTostring(CommonUtils.getCurrentDateWithoutTime(), "dd/MM/yyyy");
			token = setToken();
			log.debug(getText("METHOD_LOG_START") + getText("modifyProjectOwnerMapping"));
			projectOwnerMappingActionVo = new ProjectOwnerMappingActionVo();
			projectOwnerMappingActionVo
					.setProjectOwnerMappingId(super.getIntegerParamValue("hiddenProjectOwnerMappingId").intValue());

			requestVo.setCommonVo(projectOwnerMappingActionVo);

			/* creating instance for service class bean in getCompany method */
			responseVo = getProjectOwnerMapping(requestVo);

			projectOwnerMappingActionVo = (ProjectOwnerMappingActionVo) responseVo.getCommonVo();
			String pmId = projectOwnerMappingActionVo.getPmId();
			String pmName = projectOwnerMappingActionVo.getPmName();
			String pmEmail = projectOwnerMappingActionVo.getPmEmailId();

			projectOwnerMappingActionVo.setPmId(pmId + "-" + pmName + "-" + pmEmail);
			String plId = projectOwnerMappingActionVo.getPlId();
			String plName = projectOwnerMappingActionVo.getPlName();
			String plEmail = projectOwnerMappingActionVo.getPlEmailId();
			if (!(plId.isEmpty() && plName.isEmpty() && plEmail.isEmpty())) {
				projectOwnerMappingActionVo.setPlId(plId + "-" + plName + "-" + plEmail);
			}

			log.debug(getText("METHOD_LOG_END") + getText("modifyProjectOwnerMapping"));
		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), ce.getLogMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			return "modifyProjectOwnerMapping";
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), e.getMessage());
			return "modifyProjectOwnerMapping";
		}
		return "modifyProjectOwnerMapping";
	}

	/**
	 * @Method : getProjectOwnerMapping
	 * @Description : This method is used for getting project owner mapping data
	 *              for view.
	 * @param requestVo2
	 * @return
	 */
	private ResponseVo getProjectOwnerMapping(RequestVo requestVo2) throws Exception {
		/* creating instance for service class bean */
		ProjectOwnerMappingService projectOwnerMappingService = (ProjectOwnerMappingService) ContextLoader
				.getBean(FrameworkConstant.getBeanId("PROJECT_OWNER_MAPPING_SERVICE"));
		return projectOwnerMappingService.getProjectOwnerMapping(requestVo);
	}

	/**
	 *
	 * @Method : updateProjectOwnerMapping
	 * @Description : This method is used for updating existing project owner
	 *              maping.
	 * @return
	 */
	public String updateProjectOwnerMapping() {
		try {
			currentDate = CommonUtils.convertdateTostring(CommonUtils.getCurrentDateWithoutTime(), "dd/MM/yyyy");
			log.debug(getText("METHOD_LOG_START") + getText("updateProjectOwnerMapping"));
			UserVo userVo = requestVo.getUserVo();
			if (userVo.getToken().equals(token)) {

				projectOwnerMappingActionVo.setCreatedBy(getUserNameSession());
				projectOwnerMappingActionVo.setCreatedOn(CommonUtils.getCurrentDate());

				String[] pmArray = projectOwnerMappingActionVo.getPmId().split("-");
				projectOwnerMappingActionVo.setPmId(pmArray[0]);
				projectOwnerMappingActionVo.setPmName(pmArray[1]);

				if (!projectOwnerMappingActionVo.getPlId().isEmpty()) {
					String[] plArray = projectOwnerMappingActionVo.getPlId().split("-");
					projectOwnerMappingActionVo.setPlId(plArray[0]);
					projectOwnerMappingActionVo.setPlName(plArray[1]);
				}
				projectOwnerMappingActionVo.setDeptId(userVo.getDeptId());

				if (projectOwnerMappingActionVo.getAssociatedFromStr() != null) {
					projectOwnerMappingActionVo.setAssociatedFrom(CommonUtils
							.convertStringToDate(projectOwnerMappingActionVo.getAssociatedFromStr(), "dd/MM/yy"));
				}

				// Customer mail flag
				if (null != projectOwnerMappingActionVo.getStatusMail()
						&& projectOwnerMappingActionVo.getStatusMail() == 't') {
					projectOwnerMappingActionVo.setStatusMail(CommonConstants.STATUS_MAIL_ON);
				}
				if (null != projectOwnerMappingActionVo.getStatusMail()
						&& projectOwnerMappingActionVo.getStatusMail() == 'f') {
					projectOwnerMappingActionVo.setStatusMail(CommonConstants.STATUS_MAIL_OFF);
				}
				if (null != projectOwnerMappingActionVo.getConsolidatedMail()
						&& projectOwnerMappingActionVo.getConsolidatedMail() == 't') {
					projectOwnerMappingActionVo.setConsolidatedMail(CommonConstants.CONSOLIDATE_MAIL_ON);
				}
				if (null != projectOwnerMappingActionVo.getConsolidatedMail()
						&& projectOwnerMappingActionVo.getConsolidatedMail() == 'f') {
					projectOwnerMappingActionVo.setConsolidatedMail(CommonConstants.CONSOLIDATE_MAIL_OFF);
				}

				requestVo.setCommonVo(projectOwnerMappingActionVo);

				/* creating instance for service class bean */
				ProjectOwnerMappingService projectOwnerMappingService = (ProjectOwnerMappingService) ContextLoader
						.getBean(FrameworkConstant.getBeanId("PROJECT_OWNER_MAPPING_SERVICE"));

				/* Calling service class updateCompany method */
				responseVo = projectOwnerMappingService.updateProjectOwnerMapping(requestVo);
				if (null != responseVo.getTransactionMessage()) {
					projectOwnerMappingMsg = responseVo.getTransactionMessage();
					log.info(getLogMessage() + projectOwnerMappingMsg);
				}
				comboLoading();
				listProjectOwnerMapping();
			} else {
				return listProjectOwnerMapping();
			}
			log.debug(getText("METHOD_LOG_END") + getText("updateProjectOwnerMapping"));
		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), ce.getLogMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), e.getMessage());
		}
		return "updateProjectOwnerMapping";
	}

	/**
	 *
	 * @Method : viewProjectOwnerMapping
	 * @Description : This method is used for show Project Owner Mapping.
	 * @return
	 */
	@SkipValidation
	public String viewProjectOwnerMapping() {
		try {
			log.debug(getText("METHOD_LOG_START") + getText("viewProjectOwnerMapping"));
			projectOwnerMappingActionVo = new ProjectOwnerMappingActionVo();
			projectOwnerMappingActionVo
					.setProjectOwnerMappingId(super.getIntegerParamValue("ProjectOwnerMappingId").intValue());
			requestVo.setCommonVo(projectOwnerMappingActionVo);

			/* creating instance for service class bean in getCompany method */

			ProjectOwnerMappingService projectOwnerMappingService = (ProjectOwnerMappingService) ContextLoader
					.getBean(FrameworkConstant.getBeanId("PROJECT_OWNER_MAPPING_SERVICE"));
			responseVo = projectOwnerMappingService.viewProjectOwnerMapping(requestVo);

			projectOwnerMappingActionVo = (ProjectOwnerMappingActionVo) responseVo.getCommonVo();

			log.debug(getText("METHOD_LOG_END") + getText("viewProjectOwnerMapping"));
		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), ce.getLogMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), e.getMessage());
		}
		return "viewProjectOwnerMapping";
	}

	/**
	 *
	 * @Method : deleteProjectOwnerMapping
	 * @Description : This method is for deleting Project Owner Mapping.
	 * @return
	 */
	@SkipValidation
	public String deleteProjectOwnerMapping() {
		try {
			log.debug(getText("METHOD_LOG_START") + getText("deleteProjectOwnerMapping"));

			String deleteValue = super.getStringParamValue("deleteValue");
			requestVo.setParamIds(deleteValue);

			/* creating instance for service class bean */
			ProjectOwnerMappingService projectOwnerMappingService = (ProjectOwnerMappingService) ContextLoader
					.getBean(FrameworkConstant.getBeanId("PROJECT_OWNER_MAPPING_SERVICE"));

			/* Calling service class deleteCompany method */
			responseVo = projectOwnerMappingService.deleteProjectOwnerMapping(requestVo);

			if (null != responseVo.getTransactionMessage()) {
				projectOwnerMappingMsg = responseVo.getTransactionMessage();
				log.info(getLogMessage() + projectOwnerMappingMsg);
			}
			comboLoading();
			log.debug(getText("METHOD_LOG_END") + getText("deleteProjectOwnerMapping"));
		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), ce.getLogMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), e.getMessage());
		}
		return listProjectOwnerMapping();
	}

	/**
	 * @Method : prepare
	 *
	 * @Description : default values
	 */
	@Override
	public void prepare() throws Exception {
		associatedFrom = new Date();
		if (requestVo == null) {
			requestVo = new RequestVo();
			UserVo userVo = (UserVo) getServletRequest().getSession().getAttribute("user");
			requestVo.setUserVo(userVo);
		}
		comboLoading();
	}

	/**
	 * @throws CustomApplicationException
	 * @Method : comboLoading
	 * @Description : To load the Combo's in the Page
	 */
	@SuppressWarnings("unchecked")
	private void comboLoading() throws CustomApplicationException {

		ProjectOwnerMappingService mappingService = (ProjectOwnerMappingService) ContextLoader
				.getBean(FrameworkConstant.getBeanId("PROJECT_OWNER_MAPPING_SERVICE"));

		requestVo.setComboType("projectType");
		responseVo = mappingService.comboLoading(requestVo);
		projectTypeVoList = (List<ProjectTypeActionVo>) responseVo.getResponseList();

		requestVo.setComboType("associatedProjectList");
		responseVo = mappingService.comboLoading(requestVo);
		associatedProjectList = (List<ComboVo>) responseVo.getResponseList();

		requestVo.setComboType("pmName");
		responseVo = mappingService.comboLoading(requestVo);
		pmNameList = (List<ComboVo>) responseVo.getComboValueList();

		requestVo.setComboType("plName");
		responseVo = mappingService.comboLoading(requestVo);
		plNameList = (List<ComboVo>) responseVo.getComboValueList();

		requestVo.setComboType("mappedProject");
		responseVo = mappingService.comboLoading(requestVo);
		mappedProject = (List<ComboVo>) responseVo.getComboValueList();

		requestVo.setComboType("departmentProject");
		requestVo.setParamIds("1");
		responseVo = mappingService.comboLoading(requestVo);
		projectList = (List<ComboVo>) responseVo.getComboValueList();

		requestVo.setComboType("ProjectStatus");
		responseVo = mappingService.comboLoading(requestVo);
		projectStatusList = (List<ComboVo>) responseVo.getComboValueList();

		requestVo.setComboType("employee");
		responseVo = mappingService.comboLoading(requestVo);
		employeeList = (List<ComboVo>) responseVo.getComboValueList();
	}

	/**
	 * Getter method for responseVo
	 *
	 * @return the responseVo (ResponseVo)
	 */
	public ResponseVo getResponseVo() {
		return responseVo;
	}

	/**
	 * Setter method for responseVo
	 *
	 * @param responseVo
	 *            the responseVo to set
	 */
	public void setResponseVo(ResponseVo responseVo) {
		this.responseVo = responseVo;
	}

	/**
	 * Getter method for requestVo
	 *
	 * @return the requestVo (RequestVo)
	 */
	public RequestVo getRequestVo() {
		return requestVo;
	}

	/**
	 * Setter method for requestVo
	 *
	 * @param requestVo
	 *            the requestVo to set
	 */
	public void setRequestVo(RequestVo requestVo) {
		this.requestVo = requestVo;
	}

	/**
	 * Getter method for projectOwnerMappingActionVo
	 *
	 * @return the projectOwnerMappingActionVo (ProjectOwnerMappingActionVo)
	 */
	public ProjectOwnerMappingActionVo getProjectOwnerMappingActionVo() {
		return projectOwnerMappingActionVo;
	}

	/**
	 * Setter method for projectOwnerMappingActionVo
	 *
	 * @param projectOwnerMappingActionVo
	 *            the projectOwnerMappingActionVo to set
	 */
	public void setProjectOwnerMappingActionVo(ProjectOwnerMappingActionVo projectOwnerMappingActionVo) {
		this.projectOwnerMappingActionVo = projectOwnerMappingActionVo;
	}

	/**
	 * Getter method for pmNameList
	 *
	 * @return the pmNameList (List<ComboVo>)
	 */
	public List<ComboVo> getPmNameList() {
		return pmNameList;
	}

	/**
	 * Setter method for pmNameList
	 *
	 * @param pmNameList
	 *            the pmNameList to set
	 */
	public void setPmNameList(List<ComboVo> pmNameList) {
		this.pmNameList = pmNameList;
	}

	/**
	 * Getter method for plNameList
	 *
	 * @return the plNameList (List<ComboVo>)
	 */
	public List<ComboVo> getPlNameList() {
		return plNameList;
	}

	/**
	 * Setter method for plNameList
	 *
	 * @param plNameList
	 *            the plNameList to set
	 */
	public void setPlNameList(List<ComboVo> plNameList) {
		this.plNameList = plNameList;
	}

	/**
	 * Getter method for projectMappingList
	 *
	 * @return the projectMappingList (List<ProjectOwnerMappingActionVo>)
	 */
	public List<ProjectOwnerMappingActionVo> getProjectMappingList() {
		return projectMappingList;
	}

	/**
	 * Setter method for projectMappingList
	 *
	 * @param projectMappingList
	 *            the projectMappingList to set
	 */
	public void setProjectMappingList(List<ProjectOwnerMappingActionVo> projectMappingList) {
		this.projectMappingList = projectMappingList;
	}

	/**
	 * Getter method for projectOwnerSearchVo
	 *
	 * @return the projectOwnerSearchVo (ProjectOwnerSearchVo)
	 */
	public ProjectOwnerSearchVo getProjectOwnerSearchVo() {
		return projectOwnerSearchVo;
	}

	/**
	 * Setter method for projectOwnerSearchVo
	 *
	 * @param projectOwnerSearchVo
	 *            the projectOwnerSearchVo to set
	 */
	public void setProjectOwnerSearchVo(ProjectOwnerSearchVo projectOwnerSearchVo) {
		this.projectOwnerSearchVo = projectOwnerSearchVo;
	}

	/**
	 * Getter method for projectOwnerMappingMsg
	 *
	 * @return the projectOwnerMappingMsg (String)
	 */
	public String getProjectOwnerMappingMsg() {
		return projectOwnerMappingMsg;
	}

	/**
	 * Setter method for projectOwnerMappingMsg
	 *
	 * @param projectOwnerMappingMsg
	 *            the projectOwnerMappingMsg to set
	 */
	public void setProjectOwnerMappingMsg(String projectOwnerMappingMsg) {
		this.projectOwnerMappingMsg = projectOwnerMappingMsg;
	}

	/**
	 * Getter method for projectTypeVoList
	 *
	 * @return the projectTypeVoList (List<ProjectTypeActionVo>)
	 */
	public List<ProjectTypeActionVo> getProjectTypeVoList() {
		return projectTypeVoList;
	}

	/**
	 * Setter method for projectTypeVoList
	 *
	 * @param projectTypeVoList
	 *            the projectTypeVoList to set
	 */
	public void setProjectTypeVoList(List<ProjectTypeActionVo> projectTypeVoList) {
		this.projectTypeVoList = projectTypeVoList;
	}

	/**
	 * Getter method for projectList
	 *
	 * @return the projectList (List<ComboVo>)
	 */
	public List<ComboVo> getProjectList() {
		return projectList;
	}

	/**
	 * Setter method for projectList
	 *
	 * @param projectList
	 *            the projectList to set
	 */
	public void setProjectList(List<ComboVo> projectList) {
		this.projectList = projectList;
	}

	/**
	 * Getter method for projectCodeId
	 *
	 * @return the projectCodeId (String)
	 */
	public String getProjectCodeId() {
		return projectCodeId;
	}

	/**
	 * Setter method for projectCodeId
	 *
	 * @param projectCodeId
	 *            the projectCodeId to set
	 */
	public void setProjectCodeId(String projectCodeId) {
		this.projectCodeId = projectCodeId;
	}

	/**
	 * Getter method for projectId
	 *
	 * @return the projectId (String)
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * Setter method for projectId
	 *
	 * @param projectId
	 *            the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public List<ComboVo> getMappedProject() {
		return mappedProject;
	}

	public void setMappedProject(List<ComboVo> mappedProject) {
		this.mappedProject = mappedProject;
	}

	/**
	 * Getter method for projectStatusList
	 *
	 * @return the projectStatusList (List<ComboVo>)
	 */
	public List<ComboVo> getProjectStatusList() {
		return projectStatusList;
	}

	/**
	 * Setter method for projectStatusList
	 *
	 * @param projectStatusList
	 *            the projectStatusList to set
	 */
	public void setProjectStatusList(List<ComboVo> projectStatusList) {
		this.projectStatusList = projectStatusList;
	}

	public List<ComboVo> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<ComboVo> employeeList) {
		this.employeeList = employeeList;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the associatedProjectList
	 */
	public List<ComboVo> getAssociatedProjectList() {
		return associatedProjectList;
	}

	/**
	 * @param associatedProjectList
	 *            the associatedProjectList to set
	 */
	public void setAssociatedProjectList(List<ComboVo> associatedProjectList) {
		this.associatedProjectList = associatedProjectList;
	}

	/**
	 * @return the associatedFrom
	 */
	public Date getAssociatedFrom() {
		return associatedFrom;
	}

	/**
	 * @param associatedFrom
	 *            the associatedFrom to set
	 */
	public void setAssociatedFrom(Date associatedFrom) {
		this.associatedFrom = associatedFrom;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

}
