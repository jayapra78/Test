/**
 * Creater / durgeshkumarjhaSRM copyright information to Shirai 2013
 * 
 */
package com.task.action.custom;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.JasperCompileManager;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.struts2.StrutsStatics;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;
import com.task.common.ComboVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.UserVo;
import com.task.common.excel.ExcelGenerator;
import com.task.service.custom.ReportService;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.vo.custom.action.ProjectTypeActionVo;
import com.task.vo.custom.action.ReportActionVo;
import com.task.vo.custom.action.TaskActionVo;

/**
 * @author balamurugank
 *
 */
public class ReportAction extends CommonBaseAction implements
	Preparable {

    /** serialVersionUID instance */
    private static final long serialVersionUID = 1L;

    /** log instance for logging */
    private static Logger log = LoggingAspectInterceptor.getLog(ReportAction.class);

    /** responseVo instance to get data from service layer */
    private ResponseVo responseVo;

    /** requestVo instance to send data to service layer */

    private ReportActionVo reportActionVo;

    private String projectCodeId;
    
    private String projectId;

    private String empId;
    
    private String pmId;

    private String reportType;
    
    private String reportName;

    private String taskStartDate;

    private String taskEndDate;
    
    private String endDate;
    
    private String projectName;
    
    private int reportFormat;
    
    private boolean overall;
    
    private Integer projectTypeId;
    
    private ByteArrayInputStream excelStream;

    private List<ReportActionVo> reportList;
    
    private List<TaskActionVo> workDetailList;
    
    private List<ProjectTypeActionVo> projectTypeVoList;
    
    private List<ComboVo> projectManagerList = new ArrayList<ComboVo>();
    
    private List<ComboVo> employeeList = new ArrayList<ComboVo>();
    
    private List<ComboVo> projectList = new ArrayList<ComboVo>();
    
    private List<ComboVo> practiceTeamList = new ArrayList<ComboVo>();
    
    private HashMap<String, String> reportParams = new HashMap<String, String>();
    
    private Map<String,List<ComboVo>> allProjectList;
    
    private final String PROJECT_REPORT = "ProjectReport";
    private final String PM_REPORT = "ProjectManager";
    private final String EMP_REPORT = "EmployeeReport";

    public String openReport() {
	log.debug(getText("METHOD_LOG_END") + getText("openReport"));
	Date currentDate = CommonUtils.getCurrentDateWithoutTime();
	String currentDateStr = CommonUtils.convertdateTostring(currentDate, "dd/MM/yyyy");
	
	taskStartDate = currentDateStr;
	taskEndDate   = currentDateStr;
	return "openReport";
    }
    
    @SuppressWarnings("unchecked")
    public String searchReport() throws CustomApplicationException {
	
	/* creating instance for service class bean */
	ReportService reportService = (ReportService) ContextLoader.getBean(FrameworkConstant.getBeanId("REPORT_SERVICE"));

	reportActionVo = new ReportActionVo();
	
	if ("0".equals(reportType)) {
	    addActionError("Please select report type");
	    return "openReport"; 
	} else if ("1".equals(reportType)) {
	    if (null != projectTypeId && projectTypeId > 0){
		reportActionVo.setPROJECT_TYPE_ID(projectTypeId);
	    }
	    if (null != projectId && !projectId.isEmpty()) {
		reportActionVo.setPROJECT_ID(projectId);
	    }
	} else if ("2".equals(reportType)) {
	    reportActionVo.setREPORTEMP_ID(pmId);
	} else if ("3".equals(reportType)) {
	    reportActionVo.setCode(empId);
	}
	
	RequestVo requestVo = new RequestVo();
	UserVo userVo = (UserVo)getServletRequest().getSession().getAttribute("user");
	requestVo.setUserVo(userVo);
	
	if (null != taskStartDate){
	    reportActionVo.setTaskStartDate(CommonUtils.convertDateFormat(taskStartDate, "dd/MM/yyyy", "yyyy/MM/dd"));
	}
	if (null != taskEndDate){
	    reportActionVo.setTaskEndDate(CommonUtils.convertDateFormat(taskEndDate, "dd/MM/yyyy", "yyyy/MM/dd"));
	}
	
	requestVo.setSeacrhVo(reportActionVo);

	responseVo = (ResponseVo) reportService.reportList(requestVo);

	List<ReportActionVo> responseList = (List<ReportActionVo>) responseVo.getResponseList();
	
	
	if (null != responseList && responseList.size() > 0) {
	    reportList = new ArrayList<ReportActionVo>();
	    for (ReportActionVo reportVo : responseList) {
		reportVo.setTaskDate(CommonUtils.convertdateTostring(reportVo.getTASK_DATE(), "dd/MM/yyyy"));
		reportList.add(reportVo);
	    }
	    return "openReport";
	}else {
	    addActionError("No records found");
	}
	return "errorReport";
    }
    

    /**
     * @Method : generateReport
     * @Description : Method to generate the Report
     * @return String
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public String generateReport() throws Exception {
		log.debug(getText("METHOD_LOG_START") + getText("generateReport"));
		
		ActionContext ac = ActionContext.getContext();
		ServletContext sc = (ServletContext) ac.get(StrutsStatics.SERVLET_CONTEXT);
		
		/* creating instance for service class bean */
		ReportService reportService = (ReportService) ContextLoader.getBean(FrameworkConstant.getBeanId("REPORT_SERVICE"));
	
		reportActionVo = new ReportActionVo();
		
		if ("0".equals(reportType)) {
		    addActionError("Please select report type");
		    return "openReport"; 
		} else if ("1".equals(reportType)) {
		    reportName = PROJECT_REPORT;
		    if (null != projectTypeId && projectTypeId > 0){
			reportActionVo.setPROJECT_TYPE_ID(projectTypeId);
		    }
		    if (null != projectId && !projectId.isEmpty()) {
			reportActionVo.setPROJECT_ID(projectId);
		    }
		} else if ("2".equals(reportType)) {
		    reportName = PM_REPORT;
		    reportActionVo.setREPORTEMP_ID(pmId);
		} else if ("3".equals(reportType)) {
		    reportName = EMP_REPORT;
		    reportActionVo.setCode(empId);
		}
		
		RequestVo requestVo = new RequestVo();
		UserVo userVo = (UserVo)getServletRequest().getSession().getAttribute("user");
		requestVo.setUserVo(userVo);
		
		if (null != taskStartDate){
		    reportActionVo.setTaskStartDate(CommonUtils.convertDateFormat(taskStartDate, "dd/MM/yyyy", "yyyy/MM/dd"));
		}
		if (null != taskEndDate){
		    reportActionVo.setTaskEndDate(CommonUtils.convertDateFormat(taskEndDate, "dd/MM/yyyy", "yyyy/MM/dd"));
		}
		
		requestVo.setSeacrhVo(reportActionVo);
	
		responseVo = (ResponseVo) reportService.reportList(requestVo);
	
		List<ReportActionVo> responseList = (List<ReportActionVo>) responseVo.getResponseList();
		
		
		if (null != responseList && responseList.size() > 0) {
		    reportParams.put("realPath", sc.getRealPath("/images/pdfHeader.jpg"));
		    reportList = new ArrayList<ReportActionVo>();
		    for (ReportActionVo reportVo : responseList) {
		    	reportVo.setTaskDate(CommonUtils.convertdateTostring(reportVo.getTASK_DATE(), "dd/MM/yyyy"));
		    	reportList.add(reportVo);
		    }
		   
		  JasperCompileManager.compileReportToFile(sc.getRealPath("report") + File.separator 
				+ reportName + ".jrxml",sc.getRealPath("report")+ File.separator + "Report.jasper");
		  return "generateReport";
		}else {
		    addActionError("No records found");
		}
	
		if (null != responseVo.getErrorMessage()
			&& !responseVo.getErrorMessage().equals("")) {
		    addActionError(responseVo.getErrorMessage());
		}
		 
		log.debug(getText("METHOD_LOG_END") + getText("generateReport"));
		
		return "errorReport";
    }
    public String openEmpEffortReport(){
    	taskStartDate = "01/04/2013";
    	taskEndDate   = CommonUtils.convertdateTostring(CommonUtils.getCurrentDateWithoutTime(), "dd/MM/yyyy");;
    	return "openEmpEffortReport";
    }
    public String empEffortReport(){
    	try{
    		ReportService reportService = (ReportService) ContextLoader.getBean(FrameworkConstant.getBeanId("REPORT_SERVICE"));
    		RequestVo requestVo = new RequestVo();
    	    UserVo userVo = (UserVo)getServletRequest().getSession().getAttribute("user");
    	    requestVo.setUserVo(userVo);
    	    reportActionVo = new ReportActionVo();
    	    reportActionVo.setTaskStartDate(taskStartDate);
    	    reportActionVo.setTaskEndDate(taskEndDate);
    	    reportActionVo.setCode(empId);
    	    reportActionVo.setPROJECT_TYPE_ID(projectTypeId);
    	    reportActionVo.setPROJECT_CODE(projectCodeId);
    	    reportActionVo.setOverall(overall);
    	    requestVo.setSeacrhVo(reportActionVo);
    	    responseVo = (ResponseVo) reportService.empEffortReport(requestVo);
    	    
    	    Map<String,List<String>> headerMap = responseVo.getMapvalue();
    	    
    	    ExcelGenerator eg = new ExcelGenerator();
    	    eg.generateExcelJobTemplate("Employee effort report",headerMap , responseVo.getResultList(), getServletResponse(),responseVo.getReturnValue());
    	}
    	catch(Exception e){
    		log.error("generateExcel:"+e);
    	}
    	return "openEmpEffortReport";
    }

    /******** Added by Durgesh On Feb 2014 ********/
    /**
     * 
     * @Method : openEmpWorkDetailReport
     * @Description : This method is used to open employee work detail report screen.
     * @return
     */
    public String openEmpWorkDetailReport(){
    	taskStartDate = "01/04/2013";
    	taskEndDate   = CommonUtils.convertdateTostring(CommonUtils.getCurrentDateWithoutTime(), "dd/MM/yyyy");;
    	return "openEmpWorkDetailReport";
    }
    
    /**
     * 
     * @Method : empWorkDetailReport
     * @Description : This method is used to generate employee work detail report.
     * @return
     */
    public String empWorkDetailReport(){
    	
    	ActionContext ac = ActionContext.getContext();
		ServletContext sc = (ServletContext) ac.get(StrutsStatics.SERVLET_CONTEXT);
    	
    	try{
    		ReportService reportService = (ReportService) ContextLoader.getBean(FrameworkConstant.getBeanId("REPORT_SERVICE"));
    		RequestVo requestVo = new RequestVo();
    	    UserVo userVo = (UserVo)getServletRequest().getSession().getAttribute("user");
    	    requestVo.setUserVo(userVo);
    	    reportActionVo = new ReportActionVo();
    	    reportActionVo.setTaskStartDate(taskStartDate);
    	    reportActionVo.setTaskEndDate(taskEndDate);
    	    reportActionVo.setCode(empId);
    	    reportActionVo.setPROJECT_TYPE_ID(projectTypeId);
    	    reportActionVo.setPROJECT_CODE(projectCodeId);
    	    reportActionVo.setPROJECT_ID(projectId);
    	    reportActionVo.setOverall(overall);
    	    requestVo.setSeacrhVo(reportActionVo);
    	    
    	    // Invoking ReportService class empWorkDetailReport method.
    	    responseVo = (ResponseVo) reportService.empWorkDetailReport(requestVo);
    	    
    	    List<TaskActionVo> responseList = (List<TaskActionVo>) responseVo.getResponseList();
    		
    	    if (reportFormat == 1) { // Report format 2 is for PDF report
    	    	if (null != responseList && responseList.size() > 0) {
        		    reportParams.put("realPath", sc.getRealPath("/images/srm_logo.png"));
        		    reportParams.put("projectName", responseList.get(0).getProjectName());
        		    reportParams.put("employeeName", responseList.get(0).getEmpName());
        		    reportParams.put("dateFrom", taskStartDate);
        		    reportParams.put("dateTo", taskEndDate);
        		    reportParams.put("totalTime", String.valueOf(responseVo.getTotalTime()));
        		    
        		    workDetailList = new ArrayList<TaskActionVo>();
        		    for (TaskActionVo reportVo : responseList) {
        		    	String taskDate = CommonUtils.convertdateTostring(reportVo.getTaskDate(), "dd/MM/yyyy");
        		    	reportVo.setTaskDateStr(taskDate);
        		    	workDetailList.add(reportVo);
        		    }
        		   
        		  JasperCompileManager.compileReportToFile(sc.getRealPath("report") + File.separator 
        				+"EmpWorkDetail.jrxml",sc.getRealPath("report")+ File.separator + "EmpWorkDetail.jasper");
        		  return "workDetailReport";
        		}
    	    } else if (reportFormat == 2) { // Report format 2 is for excell report
    	    	try {
    				Workbook hwb = new HSSFWorkbook();
    				CreationHelper createHelper = hwb.getCreationHelper();

    				if (null != responseList && responseList.size() > 0) {
    					int rowNum = 0;

    					Sheet sheet = hwb.createSheet("EmployeeType");
    					Row row1 = sheet.createRow(rowNum);

    					// add picture data to this workbook.
    					Drawing drawing = sheet.createDrawingPatriarch();
    					ClientAnchor anchor = createHelper.createClientAnchor();
    					InputStream is =
    							new FileInputStream(sc.getRealPath("images") + File.separator
    									+ "srm_logo.png");
    					byte[] bytes = IOUtils.toByteArray(is);
    					int pictureIdx = hwb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
    					is.close();
    					anchor.setCol1(0);
    					anchor.setRow1(1);
    					Picture pict = drawing.createPicture(anchor, pictureIdx);
    					pict.resize();

    					CellStyle styleBoldWOBorder = hwb.createCellStyle();
    					Font fontApp = hwb.createFont();
    					fontApp.setBoldweight(Font.BOLDWEIGHT_BOLD);
    					styleBoldWOBorder.setFont(fontApp);

    					CellStyle styleTopHead = hwb.createCellStyle();
    					Font font = hwb.createFont();
    					font.setBoldweight(Font.BOLDWEIGHT_BOLD);
    					font.setFontHeightInPoints((short) 20);
    					styleTopHead.setAlignment(CellStyle.ALIGN_CENTER);
    					styleTopHead.setFont(font);

    					// Header cell style with bold
    					CellStyle styleHead = hwb.createCellStyle();
    					styleHead.setBorderLeft(HSSFCellStyle.BORDER_THIN);
    					styleHead.setBorderBottom(HSSFCellStyle.BORDER_THIN);
    					styleHead.setBorderRight(HSSFCellStyle.BORDER_THIN);
    					styleHead.setBorderTop(HSSFCellStyle.BORDER_THIN);
    					Font fontHead = hwb.createFont();
    					fontHead.setBoldweight(Font.BOLDWEIGHT_BOLD);
    					styleHead.setFont(fontHead);

    					Cell cellHead = row1.createCell(0);
    					cellHead.setCellValue("Employee Work Detail Report");
    					cellHead.setCellStyle(styleTopHead);

    					Row rowBranch = sheet.createRow(1);
    					Cell cellbranch = rowBranch.createCell(4);
    					cellbranch.setCellValue("Employee Name :");
    					cellbranch.setCellStyle(styleBoldWOBorder);
    					Cell cellbranchData = rowBranch.createCell(5);
    					cellbranchData.setCellValue(responseList.get(0).getEmpName());

    					Row rowCenter = sheet.createRow(2);
    					Cell cellCenter = rowCenter.createCell(4);
    					cellCenter.setCellValue("Duration :");
    					cellCenter.setCellStyle(styleBoldWOBorder);
    					Cell cellCenterData = rowCenter.createCell(5);
    					cellCenterData.setCellValue(taskStartDate+" To "+taskEndDate);

    					Row rowCCenter = sheet.createRow(3);
    					Cell cellCCenter = rowCCenter.createCell(4);
    					cellCCenter.setCellValue("Project :");
    					cellCCenter.setCellStyle(styleBoldWOBorder);
    					Cell cellCCenterData = rowCCenter.createCell(5);
    					cellCCenterData.setCellValue(responseList.get(0).getProjectName());

    					// Date cell
    					Row rowDate = sheet.createRow(4);
    					Cell cellDate = rowDate.createCell(4);
    					cellDate.setCellValue("Date :");
    					cellDate.setCellStyle(styleBoldWOBorder);
    					Cell cellDateValue = rowDate.createCell(5);
    					cellDateValue.setCellValue(new Date());
    					CellStyle cellStyle = hwb.createCellStyle();
    					cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
    					cellDateValue.setCellStyle(cellStyle);

    					rowNum = 6;

    					Row row = sheet.createRow(rowNum);

    					Cell cell0 = row.createCell(0);
    					cell0.setCellValue("Sl.No.");
    					Cell cell1 = row.createCell(1);
    					cell1.setCellValue("Date");
    					Cell cell2 = row.createCell(2);
    					cell2.setCellValue("Start Time");
    					Cell cell3 = row.createCell(3);
    					cell3.setCellValue("End Time");
    					Cell cell4 = row.createCell(4);
    					cell4.setCellValue("Activity");
    					Cell cell5 = row.createCell(5);
    					cell5.setCellValue("Detail");

    					CellStyle style = hwb.createCellStyle();
    					style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
    					style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
    					style.setBorderRight(HSSFCellStyle.BORDER_THIN);
    					style.setBorderTop(HSSFCellStyle.BORDER_THIN);
    					
    					CellStyle dateStyle = hwb.createCellStyle();
    					dateStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
    					dateStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
    					dateStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
    					dateStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
    					dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));

    					cell0.setCellStyle(styleHead);
    					cell1.setCellStyle(styleHead);
    					cell2.setCellStyle(styleHead);
    					cell3.setCellStyle(styleHead);
    					cell4.setCellStyle(styleHead);
    					cell5.setCellStyle(styleHead);

    					 // Merging cell in main header
    			        sheet.addMergedRegion(new CellRangeAddress(
    			                0, //first row (0-based)
    			                0, //last row  (0-based)
    			                0, //first column (0-based)
    			                5  //last column  (0-based)
    			        ));
    			        
    					rowNum++;

    					for (int i = 0; i < responseList.size(); i++) {

    						Row rows = sheet.createRow(rowNum);

    						Cell cellb = rows.createCell(0);
    						cellb.setCellValue(i + 1);
    						cellb.setCellStyle(style);

    						Cell cellc = rows.createCell(1);
    						cellc.setCellValue(responseList.get(i).getTaskDate());
    						cellc.setCellStyle(dateStyle);

    						Cell celld = rows.createCell(2);
    						celld.setCellValue(responseList.get(i).getActualStartTime());
    						celld.setCellStyle(style);

    						Cell celle = rows.createCell(3);
    						celle.setCellValue(responseList.get(i).getActualEndTime());
    						celle.setCellStyle(style);

    						Cell cellf = rows.createCell(4);
    						cellf.setCellValue(responseList.get(i).getActivity());
    						cellf.setCellStyle(style);
    						
    						Cell cellg = rows.createCell(5);
    						cellg.setCellValue(responseList.get(i).getWorkItem());
    						cellg.setCellStyle(style);

    						rowNum++;
    					}

    					// Creating row for total data.
    					Row rowTotal = sheet.createRow( responseList.size() + 7);
    					Cell cellTotal = rowTotal.createCell(4);
    					cellTotal.setCellValue("Total Hour");
    					cellTotal.setCellStyle(styleHead);

    					Cell cellbasicTotal = rowTotal.createCell(5);
    					cellbasicTotal.setCellValue(String.valueOf(responseVo.getTotalTime()));
    					cellbasicTotal.setCellStyle(style);
    					
    					// Signature section
    					Row rowSign = sheet.createRow( responseList.size() + 15);
    					Cell cellPM = rowSign.createCell(0);
    					cellPM.setCellValue("Project Manager");
    					cellPM.setCellStyle(styleBoldWOBorder);
    					
    					Cell cellHR = rowSign.createCell(5);
    					cellHR.setCellValue("HR");
    					cellHR.setCellStyle(styleBoldWOBorder);

    					// For auto size column based on data.
    					for (int i = 0; i < 6; i++) {
    						sheet.autoSizeColumn(i);
    					}
    				}
    				addActionError("");
    				ByteArrayOutputStream baos = new ByteArrayOutputStream();
    				hwb.write(baos);
    				excelStream = new ByteArrayInputStream(baos.toByteArray());

    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    			return "XLSReport";
    	    }
    		else {
    		    addActionError("No records found");
    		}
    		 
    		log.debug(getText("METHOD_LOG_END") + getText("empWorkDetailReport"));
    	    
    	}
    	catch(Exception e){
    		log.error("generateExcel:"+e);
    	}
    	return "openEmpWorkDetailReport";
    }
    /******** Added by Durgesh On Feb 2014 ********/
    
    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {
    	comboLoading();
    }
    
    /**
     * @Method : comboLoading
     * @Description : Method to load the drop down's
     * @throws CustomApplicationException
     */
    @SuppressWarnings("unchecked")
    private void comboLoading() throws CustomApplicationException {
	try {
	    /* creating instance for service class bean */
	    ReportService reportService = (ReportService) ContextLoader.getBean(FrameworkConstant.getBeanId("REPORT_SERVICE"));
	    
	    
	    RequestVo requestVo = new RequestVo();
	    UserVo userVo = (UserVo)getServletRequest().getSession().getAttribute("user");
	    requestVo.setUserVo(userVo);
	    
	    requestVo.setComboType("allProject");
	    responseVo = reportService.comboLoading(requestVo);
	    allProjectList = (Map<String,List<ComboVo>>) responseVo.getComboValueMap();

	    requestVo.setComboType("projectType");
	    responseVo = reportService.comboLoading(requestVo);
	    projectTypeVoList = (List<ProjectTypeActionVo>) responseVo.getResponseList();
	    
	    requestVo.setComboType("projectManager");
	    responseVo = reportService.comboLoading(requestVo);
	    projectManagerList = (List<ComboVo>) responseVo.getComboValueList();
	    
	    requestVo.setComboType("employee");
	    responseVo = reportService.comboLoading(requestVo);
	    employeeList = (List<ComboVo>) responseVo.getComboValueList();
	    
	} catch (CustomApplicationException e) {
	    throw new CustomApplicationException(e);
	}
    }
    
    /**
     * Getter method for projectCodeId
     * 
     * @return the projectCodeId (String)
     */
    public String getProjectCodeId() {
	return projectCodeId;
    }

    /**
     * Setter method for projectCodeId
     * 
     * @param projectCodeId
     *            the projectCodeId to set
     */
    public void setProjectCodeId(String projectCodeId) {
	this.projectCodeId = projectCodeId;
    }

    /**
     * Getter method for endDate
     * 
     * @return the endDate (String)
     */
    public String getEndDate() {
	return endDate;
    }

    /**
     * Setter method for endDate
     * 
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(String endDate) {
	this.endDate = endDate;
    }

    /**
     * Getter method for reportType
     * 
     * @return the reportType (String)
     */
    public String getReportType() {
        return reportType;
    }

    /**
     * Setter method for reportType
     *
     * @param reportType the reportType to set
     */
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    /**
     * Getter method for pmId
     * 
     * @return the pmId (String)
     */
    public String getPmId() {
        return pmId;
    }

    /**
     * Setter method for pmId
     *
     * @param pmId the pmId to set
     */
    public void setPmId(String pmId) {
        this.pmId = pmId;
    }

    /**
     * Getter method for empId
     * 
     * @return the empId (String)
     */
    public String getEmpId() {
        return empId;
    }

    /**
     * Setter method for empId
     *
     * @param empId the empId to set
     */
    public void setEmpId(String empId) {
        this.empId = empId;
    }

    /**
     * Getter method for employeeList
     * 
     * @return the employeeList (List<ComboVo>)
     */
    public List<ComboVo> getEmployeeList() {
        return employeeList;
    }

    /**
     * Setter method for employeeList
     *
     * @param employeeList the employeeList to set
     */
    public void setEmployeeList(List<ComboVo> employeeList) {
        this.employeeList = employeeList;
    }

    /**
     * Getter method for projectManagerList
     * 
     * @return the projectManagerList (List<ComboVo>)
     */
    public List<ComboVo> getProjectManagerList() {
        return projectManagerList;
    }

    /**
     * Setter method for projectManagerList
     *
     * @param projectManagerList the projectManagerList to set
     */
    public void setProjectManagerList(List<ComboVo> projectManagerList) {
        this.projectManagerList = projectManagerList;
    }

    /**
     * Getter method for reportName
     * 
     * @return the reportName (String)
     */
    public String getReportName() {
        return reportName;
    }

    /**
     * Setter method for reportName
     *
     * @param reportName the reportName to set
     */
    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    /**
     * Getter method for reportActionVo
     * 
     * @return the reportActionVo (ReportActionVo)
     */
    public ReportActionVo getReportActionVo() {
        return reportActionVo;
    }

    /**
     * Setter method for reportActionVo
     *
     * @param reportActionVo the reportActionVo to set
     */
    public void setReportActionVo(ReportActionVo reportActionVo) {
        this.reportActionVo = reportActionVo;
    }

    /**
     * Getter method for reportList
     * 
     * @return the reportList (List<ReportActionVo>)
     */
    public List<ReportActionVo> getReportList() {
        return reportList;
    }

    /**
     * Setter method for reportList
     *
     * @param reportList the reportList to set
     */
    public void setReportList(List<ReportActionVo> reportList) {
        this.reportList = reportList;
    }

    /**
     * Getter method for reportParams
     * 
     * @return the reportParams (HashMap)
     */
    public HashMap<String, String> getReportParams() {
        return reportParams;
    }

    /**
     * Setter method for reportParams
     *
     * @param reportParams the reportParams to set
     */
    public void setReportParams(HashMap<String, String> reportParams) {
        this.reportParams = reportParams;
    }

    /**
     * Getter method for projectList
     * 
     * @return the projectList (List<ComboVo>)
     */
    public List<ComboVo> getProjectList() {
        return projectList;
    }

    /**
     * Setter method for projectList
     *
     * @param projectList the projectList to set
     */
    public void setProjectList(List<ComboVo> projectList) {
        this.projectList = projectList;
    }

    /**
     * Getter method for projectTypeId
     * 
     * @return the projectTypeId (Integer)
     */
    public Integer getProjectTypeId() {
        return projectTypeId;
    }

    /**
     * Setter method for projectTypeId
     *
     * @param projectTypeId the projectTypeId to set
     */
    public void setProjectTypeId(Integer projectTypeId) {
        this.projectTypeId = projectTypeId;
    }

    /**
     * Getter method for taskStartDate
     * 
     * @return the taskStartDate (String)
     */
    public String getTaskStartDate() {
        return taskStartDate;
    }

    /**
     * Setter method for taskStartDate
     *
     * @param taskStartDate the taskStartDate to set
     */
    public void setTaskStartDate(String taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    /**
     * Getter method for taskEndDate
     * 
     * @return the taskEndDate (String)
     */
    public String getTaskEndDate() {
        return taskEndDate;
    }

    /**
     * Setter method for taskEndDate
     *
     * @param taskEndDate the taskEndDate to set
     */
    public void setTaskEndDate(String taskEndDate) {
        this.taskEndDate = taskEndDate;
    }

    /**
     * Getter method for projectId
     * 
     * @return the projectId (String)
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * Setter method for projectId
     *
     * @param projectId the projectId to set
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * Getter method for projectTypeVoList
     * 
     * @return the projectTypeVoList (List<ProjectTypeActionVo>)
     */
    public List<ProjectTypeActionVo> getProjectTypeVoList() {
        return projectTypeVoList;
    }

    /**
     * Setter method for projectTypeVoList
     *
     * @param projectTypeVoList the projectTypeVoList to set
     */
    public void setProjectTypeVoList(List<ProjectTypeActionVo> projectTypeVoList) {
        this.projectTypeVoList = projectTypeVoList;
    }

	public List<ComboVo> getPracticeTeamList() {
		return practiceTeamList;
	}

	public void setPracticeTeamList(List<ComboVo> practiceTeamList) {
		this.practiceTeamList = practiceTeamList;
	}

	/**
	 * Getter method for allProjectList
	 * 
	 * @return the allProjectList (Map<String,List<ComboVo>>)
	 */
	public Map<String, List<ComboVo>> getAllProjectList() {
	    return allProjectList;
	}

	/**
	 * Setter method for allProjectList
	 *
	 * @param allProjectList the allProjectList to set
	 */
	public void setAllProjectList(Map<String, List<ComboVo>> allProjectList) {
	    this.allProjectList = allProjectList;
	}

	public boolean isOverall() {
		return overall;
	}

	public void setOverall(boolean overall) {
		this.overall = overall;
	}

	/**
	 * Getter method for workDetailList
	 * 
	 * @return the workDetailList (List<TaskActionVo>)
	 */
	public List<TaskActionVo> getWorkDetailList() {
		return workDetailList;
	}

	/**
	 * Setter method for workDetailList
	 *
	 * @param workDetailList the workDetailList to set
	 */
	public void setWorkDetailList(List<TaskActionVo> workDetailList) {
		this.workDetailList = workDetailList;
	}

	/**
	 * Getter method for projectName
	 * 
	 * @return the projectName (String)
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * Setter method for projectName
	 *
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * Setter method for reportFormat
	 *
	 * @param reportFormat the reportFormat to set
	 */
	public void setReportFormat(int reportFormat) {
		this.reportFormat = reportFormat;
	}

	/**
	 * Getter method for excelStream
	 * 
	 * @return the excelStream (ByteArrayInputStream)
	 */
	public ByteArrayInputStream getExcelStream() {
		return excelStream;
	}

	/**
	 * Setter method for excelStream
	 *
	 * @param excelStream the excelStream to set
	 */
	public void setExcelStream(ByteArrayInputStream excelStream) {
		this.excelStream = excelStream;
	}

}
