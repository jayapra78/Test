/**
 * Creater / balamurugankSRM copyright information to Shirai 2013
 *
 */
package com.task.action.custom;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Preparable;
import com.task.common.ComboVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.UserVo;
import com.task.service.custom.ReportService;
import com.task.service.custom.ResourceAllocationService;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.vo.custom.action.AvailableResourceVo;
import com.task.vo.custom.action.ProjectOwnerMappingActionVo;

/**
 * @author balamurugank
 *
 */
/**
 * @author sengottaiyans
 *
 */
public class ResourceAllocationAction extends CommonBaseAction implements Preparable {

	/**
	 * long serialVersionUID :
	 */

	private static final long serialVersionUID = 1L;

	private static Logger log = LoggingAspectInterceptor.getLog(ResourceAllocationAction.class);

	/** responseVo instance to get data from service layer */
	private ResponseVo responseVo;

	/** requestVo instance to send data to service layer */
	private RequestVo requestVo;

	private List<ComboVo> projectNameList = new ArrayList<ComboVo>(); // List
																		// Contains
																		// the
																		// values
																		// for
																		// Project
																		// Combo

	private List<ComboVo> plList = new ArrayList<ComboVo>(); // List Contains
																// the values
																// for PL Combo

	private List<ComboVo> roleList = new ArrayList<ComboVo>(); // List Contains
																// the values
																// for Role
																// Combo

	private List<ComboVo> modeList = new ArrayList<ComboVo>(); // List contains
																// the values
																// for Mode
																// Combo

	private List<AvailableResourceVo> availResourceList = new ArrayList<AvailableResourceVo>();

	private List<AvailableResourceVo> allocResourceList = new ArrayList<AvailableResourceVo>();

	private List<ComboVo> projectManagerList = new ArrayList<ComboVo>();

	private String projectId;

	private String selectedProjectId;

	private String newProjectId;

	private String projectTypeId;

	private String pmName;

	private String plName;

	private Date startDate;

	private Date endDate;

	private String startDateStr;

	private String endDateStr;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opensymphony.xwork2.Preparable#prepare()
	 */
	@Override
	public void prepare() throws Exception {
		comboLoading();
	}

	public String openResourceAllocation() {
		return "openResourceAllocation";
	}

	@SuppressWarnings("unchecked")
	public String searchResourceAllocation() {
		try {
			setProjectTypeID(projectId);

			Object[] paramValue = new Object[] { newProjectId, projectTypeId };

			ResourceAllocationService resourceService = (ResourceAllocationService) ContextLoader
					.getBean(FrameworkConstant.getBeanId("RESOURCE_ALLOCATION_SERVICE"));
			RequestVo requestVo = new RequestVo();
			UserVo userVo = (UserVo) getServletRequest().getSession().getAttribute("user");
			requestVo.setUserVo(userVo);
			requestVo.setComboType("ProjectDetails");
			requestVo.setValues(paramValue);
			ResponseVo responseVo = resourceService.getProjectDetails(requestVo);

			ProjectOwnerMappingActionVo projectVo = (ProjectOwnerMappingActionVo) responseVo.getCommonVo();
			pmName = projectVo.getPmName();
			plName = projectVo.getPlName();
			startDateStr = CommonUtils.convertdateTostring(projectVo.getStartDate(), "dd/MM/yyyy");
			endDateStr = CommonUtils.convertdateTostring(projectVo.getEndDate(), "dd/MM/yyyy");

			availResourceList = (List<AvailableResourceVo>) responseVo.getResultList();

			allocResourceList = (List<AvailableResourceVo>) responseVo.getResponseList();
			if (allocResourceList != null) {
				for (AvailableResourceVo allocResource : allocResourceList) {
					allocResource.setStartDateStr(
							CommonUtils.convertdateTostring(allocResource.getStartDate(), "dd/MM/yyyy"));
					allocResource
							.setEndDateStr(CommonUtils.convertdateTostring(allocResource.getEndDate(), "dd/MM/yyyy"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return "searchResourceAllocation";
	}

	public String saveResourceAllocation() {
		try {
			setProjectTypeID(selectedProjectId);
			String paramsName[] = new String[] { newProjectId, projectTypeId };
			if (allocResourceList != null) {
				for (int i = allocResourceList.size() - 1; i >= 0; i--) {
					AvailableResourceVo resourceVo = allocResourceList.get(i);
					if (resourceVo == null || resourceVo.getEmployeeCode() == null) {
						allocResourceList.remove(i);
					}
				}
				/* creating instance for service class bean */
				ResourceAllocationService resAllocService = (ResourceAllocationService) ContextLoader
						.getBean(FrameworkConstant.getBeanId("RESOURCE_ALLOCATION_SERVICE"));
				// setProjectTypeID();
				projectId = selectedProjectId;
				requestVo.setRequestList(allocResourceList);
				requestVo.setParamNames(paramsName);
				responseVo = (ResponseVo) resAllocService.saveResource(requestVo);
				searchResourceAllocation();
				addActionMessage("Resource(s) allocated successfully");
			}
		} catch (Exception e) {
			log.error(e);
			List<AvailableResourceVo> allocResourceList1 = allocResourceList;
			searchResourceAllocation();
			allocResourceList = allocResourceList1;
			addActionMessage("Error during save, Please contact the Administator");
		}
		return "searchResourceAllocation";
	}

	private void comboLoading() throws CustomApplicationException {

		ResourceAllocationService resourceService = (ResourceAllocationService) ContextLoader
				.getBean(FrameworkConstant.getBeanId("RESOURCE_ALLOCATION_SERVICE"));

		ReportService reportService = (ReportService) ContextLoader
				.getBean(FrameworkConstant.getBeanId("REPORT_SERVICE"));

		requestVo = new RequestVo();
		UserVo userVo = (UserVo) getServletRequest().getSession().getAttribute("user");
		requestVo.setUserVo(userVo);

		requestVo.setComboType("ProjectNameCombo");
		responseVo = resourceService.comboLoading(requestVo);
		projectNameList = (List<ComboVo>) responseVo.getComboValueList();

		requestVo.setComboType("PLName");
		responseVo = resourceService.comboLoading(requestVo);
		plList = (List<ComboVo>) responseVo.getComboValueList();

		requestVo.setComboType("RoleCombo");
		responseVo = resourceService.comboLoading(requestVo);
		roleList = (List<ComboVo>) responseVo.getComboValueList();

		requestVo.setComboType("ModeCombo");
		responseVo = resourceService.comboLoading(requestVo);
		modeList = (List<ComboVo>) responseVo.getComboValueList();

		requestVo.setComboType("projectManager");
		responseVo = reportService.comboLoading(requestVo);
		projectManagerList = (List<ComboVo>) responseVo.getComboValueList();
	}

	private void setProjectTypeID(String projectId) {
		projectTypeId = projectId.substring(projectId.lastIndexOf(':') + 1, projectId.length());
		newProjectId = projectId.substring(0, projectId.lastIndexOf(':'));
	}

	/**
	 * Getter method for responseVo
	 *
	 * @return the responseVo (ResponseVo)
	 */
	public ResponseVo getResponseVo() {
		return responseVo;
	}

	/**
	 * Setter method for responseVo
	 *
	 * @param responseVo
	 *            the responseVo to set
	 */
	public void setResponseVo(ResponseVo responseVo) {
		this.responseVo = responseVo;
	}

	/**
	 * Getter method for requestVo
	 *
	 * @return the requestVo (RequestVo)
	 */
	public RequestVo getRequestVo() {
		return requestVo;
	}

	/**
	 * Setter method for requestVo
	 *
	 * @param requestVo
	 *            the requestVo to set
	 */
	public void setRequestVo(RequestVo requestVo) {
		this.requestVo = requestVo;
	}

	/**
	 * Getter method for projectNameList
	 *
	 * @return the projectNameList (List<ComboVo>)
	 */
	public List<ComboVo> getProjectNameList() {
		return projectNameList;
	}

	/**
	 * Setter method for projectNameList
	 *
	 * @param projectNameList
	 *            the projectNameList to set
	 */
	public void setProjectNameList(List<ComboVo> projectNameList) {
		this.projectNameList = projectNameList;
	}

	/**
	 * Getter method for plList
	 *
	 * @return the plList (List<ComboVo>)
	 */
	public List<ComboVo> getPlList() {
		return plList;
	}

	/**
	 * Setter method for plList
	 *
	 * @param plList
	 *            the plList to set
	 */
	public void setPlList(List<ComboVo> plList) {
		this.plList = plList;
	}

	/**
	 * Getter method for roleList
	 *
	 * @return the roleList (List<ComboVo>)
	 */
	public List<ComboVo> getRoleList() {
		return roleList;
	}

	/**
	 * Setter method for roleList
	 *
	 * @param roleList
	 *            the roleList to set
	 */
	public void setRoleList(List<ComboVo> roleList) {
		this.roleList = roleList;
	}

	/**
	 * Getter method for modeList
	 *
	 * @return the modeList (List<ComboVo>)
	 */
	public List<ComboVo> getModeList() {
		return modeList;
	}

	/**
	 * Setter method for modeList
	 *
	 * @param modeList
	 *            the modeList to set
	 */
	public void setModeList(List<ComboVo> modeList) {
		this.modeList = modeList;
	}

	/**
	 * Getter method for projectId
	 *
	 * @return the projectId (String)
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * Setter method for projectId
	 *
	 * @param projectId
	 *            the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Getter method for pmName
	 *
	 * @return the pmName (String)
	 */
	public String getPmName() {
		return pmName;
	}

	/**
	 * Setter method for pmName
	 *
	 * @param pmName
	 *            the pmName to set
	 */
	public void setPmName(String pmName) {
		this.pmName = pmName;
	}

	/**
	 * Getter method for plName
	 *
	 * @return the plName (String)
	 */
	public String getPlName() {
		return plName;
	}

	/**
	 * Setter method for plName
	 *
	 * @param plName
	 *            the plName to set
	 */
	public void setPlName(String plName) {
		this.plName = plName;
	}

	/**
	 * Getter method for startDate
	 *
	 * @return the startDate (Date)
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Setter method for startDate
	 *
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Getter method for endDate
	 *
	 * @return the endDate (Date)
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Setter method for endDate
	 *
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Getter method for availResourceList
	 *
	 * @return the availResourceList (List<AvailableResourceVo>)
	 */
	public List<AvailableResourceVo> getAvailResourceList() {
		return availResourceList;
	}

	/**
	 * Setter method for availResourceList
	 *
	 * @param availResourceList
	 *            the availResourceList to set
	 */
	public void setAvailResourceList(List<AvailableResourceVo> availResourceList) {
		this.availResourceList = availResourceList;
	}

	/**
	 * Getter method for allocResourceList
	 *
	 * @return the allocResourceList (List<AvailableResourceVo>)
	 */
	public List<AvailableResourceVo> getAllocResourceList() {
		return allocResourceList;
	}

	/**
	 * Setter method for allocResourceList
	 *
	 * @param allocResourceList
	 *            the allocResourceList to set
	 */
	public void setAllocResourceList(List<AvailableResourceVo> allocResourceList) {
		this.allocResourceList = allocResourceList;
	}

	/**
	 * @return the projectTypeId
	 */
	public String getProjectTypeId() {
		return projectTypeId;
	}

	/**
	 * @param projectTypeId
	 *            the projectTypeId to set
	 */
	public void setProjectTypeId(String projectTypeId) {
		this.projectTypeId = projectTypeId;
	}

	/**
	 * @return the newProjectId
	 */
	public String getNewProjectId() {
		return newProjectId;
	}

	/**
	 * @param newProjectId
	 *            the newProjectId to set
	 */
	public void setNewProjectId(String newProjectId) {
		this.newProjectId = newProjectId;
	}

	public String getStartDateStr() {
		return startDateStr;
	}

	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public String getSelectedProjectId() {
		return selectedProjectId;
	}

	public void setSelectedProjectId(String selectedProjectId) {
		this.selectedProjectId = selectedProjectId;
	}

	public List<ComboVo> getProjectManagerList() {
		return projectManagerList;
	}

	public void setProjectManagerList(List<ComboVo> projectManagerList) {
		this.projectManagerList = projectManagerList;
	}

}
