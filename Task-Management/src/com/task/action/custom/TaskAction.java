/**
 * Copyright 2012 by Shirai, All rights reserved.
 *
 * 2012/06/20 <Base Line Version 1.0>
 */
package com.task.action.custom;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.task.common.ComboVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.UserVo;
import com.task.common.constant.ConstantEnum;
import com.task.common.constant.ResultConstant;
import com.task.service.custom.TaskService;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.vo.custom.action.ProjectActionVo;
import com.task.vo.custom.action.ProjectTypeActionVo;
import com.task.vo.custom.action.RecursiveTaskActionVo;
import com.task.vo.custom.action.TaskActionVo;

/**
 * Action class used to validate login user
 *
 * @author Neethimohan A
 */
@Validations
public class TaskAction extends CommonBaseAction implements Preparable {

	/** serialVersionUID instance */
	private static final long serialVersionUID = 1L;

	/** log instance for logging */
	private static Logger log = LoggingAspectInterceptor.getLog(TaskAction.class);

	/** responseVo instance to get data from service layer */
	private ResponseVo responseVo;

	/** requestVo instance to send data to service layer */
	private RequestVo requestVo;

	private TaskActionVo taskActionVo;

	private List<TaskActionVo> task;

	private ProjectActionVo projectTypeActionVo;

	private List<ProjectTypeActionVo> projectTypeVoList;

	private String taskStartDate;

	private String startDate;

	private String endDate;

	private String projectTypeId;

	private String projectDate;

	private List<ComboVo> projectList = new ArrayList<ComboVo>();

	private Map<String, List<ComboVo>> deptProjectList = new HashMap<String, List<ComboVo>>();

	private List<ComboVo> practiceTeamList = new ArrayList<ComboVo>();

	private List<ComboVo> activityList = new ArrayList<ComboVo>();

	private List<ComboVo> projectManagerList = new ArrayList<ComboVo>();

	private List<ComboVo> unitList = new ArrayList<ComboVo>();

	private List<ComboVo> timeList = new ArrayList<ComboVo>();

	private List<ComboVo> dayList = new ArrayList<ComboVo>();

	private List<ComboVo> ccList = new ArrayList<ComboVo>();

	private String projectListId;

	private String projectCodeId;

	private String projectId;

	private Date currentDate;

	private String currentTime;

	private String currentDateStr;

	private boolean saveAndSend;

	private String empId;

	private String empCode;

	private int availableRecords;

	private RecursiveTaskActionVo recursiveTaskActionVo;

	private String taskDisplayMsg;

	private String token;

	private Map<String, List<ComboVo>> allProjectList;

	private String cc;

	private boolean skipToken;

	/**
	 *
	 * @Method :
	 * @Description :
	 * @return
	 */
	@SkipValidation
	public String openTask() {

		currentDate = CommonUtils.getCurrentDateWithoutTime();
		currentDateStr = CommonUtils.convertdateTostring(currentDate, "dd/MM/yyyy");

		taskStartDate = currentDateStr;
		endDate = currentDateStr;

		/*
		 * List<TaskActionVo> taskActionVos = new ArrayList<TaskActionVo>();
		 * TaskActionVo c = new TaskActionVo(); taskActionVos.add(c); responseVo
		 * = new ResponseVo(); responseVo.setResponseList(taskActionVos);
		 */
		searchTask();
		token = setToken();
		return "task";
	}

	@SkipValidation
	public String openActualRecursiveTask() {
		currentDate = CommonUtils.getCurrentDateWithoutTime();
		currentDateStr = CommonUtils.convertdateTostring(currentDate, "dd/MM/yyyy");

		taskStartDate = currentDateStr;
		endDate = currentDateStr;

		log.debug(getText("METHOD_LOG_START") + "openActualRecursiveTask");
		currentDate = CommonUtils.getCurrentDateWithoutTime();
		currentDateStr = CommonUtils.convertdateTostring(currentDate, "dd/MM/yyyy");

		currentTime = CommonUtils.getCurrentTime();

		UserVo userVo = (UserVo) getServletRequest().getSession().getAttribute("user");

		if (empId == null)
			empId = userVo.getEmployeeId();
		if (empCode == null)
			empCode = userVo.getEmployeeCode();
		if (projectTypeId == null)
			projectTypeId = "0";

		/* creating instance for service class bean */
		TaskService taskService = (TaskService) ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_SERVICE"));

		availableRecords = ConstantEnum.CONTENT_TYPE_1.getIntValue();

		if (responseVo == null) {
			responseVo = new ResponseVo();
		}
		if (responseVo.getResponseList() == null || responseVo.getResponseList().size() == 0) {
			List<TaskActionVo> taskActionVos = new ArrayList<TaskActionVo>();
			TaskActionVo c = new TaskActionVo();
			c.setTaskDate(CommonUtils.convertStartTime(CommonUtils.getCurrentDate()));
			c.setPlanStartTime("");
			c.setPlanEndTime("");
			taskActionVos.add(c);
			responseVo.setResponseList(taskActionVos);
		}

		log.debug(getText("METHOD_LOG_END") + "openActualRecursiveTask");
		if (skipToken == false) {
			token = setToken();
		}
		return "openActualRecursiveTask";
	}

	public String addBulkTask() {

		try {
			UserVo userVo = requestVo.getUserVo();
			if (userVo.getToken().equals(token)) {
				currentDate = CommonUtils.getCurrentDateWithoutTime();
				currentDateStr = CommonUtils.convertdateTostring(currentDate, "dd/MM/yyyy");
				currentTime = CommonUtils.getCurrentTime();

				if (empId == null)
					empId = userVo.getEmployeeId();
				if (empCode == null)
					empCode = userVo.getEmployeeCode();

				responseVo = new ResponseVo();
				responseVo.setResponseList(task);

				for (TaskActionVo taskVo : task) {
					taskVo.setCreatedOn(CommonUtils.getCurrentDate());
					taskVo.setBulkStartDateStr(getTaskStartDate());
					taskVo.setBulkEndDateStr(getEndDate());
					taskVo.setEmployeeId(empId);
					taskVo.setEmployeeCode(empCode);
				}

				if (!validation()) {
					return "openActualRecursiveTask";
				}

				/* creating instance for service class bean */
				TaskService taskService = (TaskService) ContextLoader
						.getBean(FrameworkConstant.getBeanId("TASK_SERVICE"));

				requestVo = new RequestVo();
				requestVo.setUserVo(userVo);

				requestVo.setRequestList(task);
				ResponseVo responseVo = (ResponseVo) taskService.saveBulkTaskList(requestVo);
				if (responseVo != null && responseVo.getErrorMessage() != null
						&& !("".equals(responseVo.getErrorMessage()))) {
					addActionError(responseVo.getErrorMessage());
				} else {
					this.responseVo.setResponseList(null);
					openActualRecursiveTask();
					addActionMessage("Task saved successfully");
				}
			} else {
				skipToken = true;
				openActualRecursiveTask();
				skipToken = false;
			}
		} catch (CustomApplicationException e) {
			addActionError("Error Occurred while saving addBulkTask");
			log.error(e);
		}

		return "openActualRecursiveTask";
	}

	/**
	 *
	 * @Method :
	 * @Description :
	 * @return
	 */
	@SkipValidation
	public String recursiveTask() {
		currentDate = CommonUtils.getCurrentDateWithoutTime();
		currentDateStr = CommonUtils.convertdateTostring(currentDate, "dd/MM/yyyy");

		taskStartDate = currentDateStr;
		endDate = currentDateStr;

		currentTime = CommonUtils.getCurrentTime();

		return "openRecursiveTask";
	}

	/**
	 *
	 * @Method :
	 * @Description :
	 * @return
	 */
	@Validations
	public String saveRecursiveTask() {

		log.debug("saveRecursiveTask");
		try {
			currentDate = CommonUtils.getCurrentDateWithoutTime();
			currentDateStr = CommonUtils.convertdateTostring(currentDate, "dd/MM/yyyy");

			taskStartDate = currentDateStr;
			endDate = currentDateStr;

			currentTime = CommonUtils.getCurrentTime();
			/* creating instance for service class bean */
			TaskService taskService = (TaskService) ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_SERVICE"));

			if (!recursiveValidation()) {
				addActionError("Invalid Days Selection");
				return "openRecursiveTask";
			}
			requestVo.setCommonVo(recursiveTaskActionVo);

			/* Calling taskService class saveRecursiveTask method */
			responseVo = (ResponseVo) taskService.saveRecursiveTask(requestVo);

			/* Getting Transaction message */
			if ((responseVo != null) && responseVo.getTransactionMessage() != null
					&& responseVo.getErrorMessage() == null) {
				taskDisplayMsg = responseVo.getTransactionMessage();
				addActionMessage(responseVo.getTransactionMessage());
			} else {
				addActionError(responseVo.getErrorMessage());
			}

		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), ce.getLogMessage());
			addActionError("Invalid Operation Done");
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			getServletRequest().getSession().setAttribute(getText("ERROR_SESSION"), e.getMessage());
			addActionError("Invalid Operation Done");
		}

		return "openRecursiveTask";
	}

	/**
	 * @Method : login
	 *
	 * @Description : to show login screen
	 *
	 * @return String
	 */
	@SkipValidation
	public String searchTask() {
		try {
			log.debug(getText("METHOD_LOG_START") + "searchTask");
			if (taskStartDate != null) {
				token = setToken();
			}
			currentDate = CommonUtils.getCurrentDateWithoutTime();
			currentDateStr = CommonUtils.convertdateTostring(currentDate, "dd/MM/yyyy");

			currentTime = CommonUtils.getCurrentTime();

			UserVo userVo = (UserVo) getServletRequest().getSession().getAttribute("user");
			
			if (empId == null)
				empId = userVo.getEmployeeId();
			if (empCode == null)
				empCode = userVo.getEmployeeCode();
			if (projectTypeId == null)
				projectTypeId = "0";

			/* creating instance for service class bean */
			TaskService taskService = (TaskService) ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_SERVICE"));

			taskActionVo = new TaskActionVo();
			taskActionVo.setTaskStartDate(CommonUtils.convertStringToDate(taskStartDate, "dd/MM/yy"));
			taskActionVo.setTaskEndDate(CommonUtils.convertStringToDate(endDate, "dd/MM/yy"));
			taskActionVo.setProjectCode(projectCodeId);
			taskActionVo.setEmployeeId(empId);
			taskActionVo.setEmployeeCode(empCode);
			ProjectTypeActionVo projectTypeActionVo = new ProjectTypeActionVo();
			projectTypeActionVo.setProjectTypeId(Integer.parseInt(projectTypeId));
			taskActionVo.setProjectTypeActionVo(projectTypeActionVo);
			requestVo.setSeacrhVo(taskActionVo);

			// Calling taskList method in taskService class.
			responseVo = (ResponseVo) taskService.taskList(requestVo);

			List<TaskActionVo> taskActionVoList = (List<TaskActionVo>) responseVo.getResponseList();
			if (null != taskActionVoList && taskActionVoList.size() > 0) {
				for (TaskActionVo taskVo : taskActionVoList) {
					taskVo.setTaskDate(CommonUtils.convertStartTime(taskVo.getTaskDate()));
					taskVo.setTaskDateStr(CommonUtils.convertdateTostring(taskVo.getTaskDate(), "dd/MM/yyyy"));
					/*
					 * if(taskVo.getUnitId()==0 || taskVo.getUnitId()==null){
					 * if(new BigDecimal("0.0").compareTo(taskVo.getSize())==0){
					 * taskVo.setSize(null); } }
					 */
				}
			}

			if (null != taskDisplayMsg && !taskDisplayMsg.equals("")) {
				addActionMessage(taskDisplayMsg);
				taskDisplayMsg = "";
			}

			if (null != taskActionVoList && taskActionVoList.size() > 0) {
				availableRecords = responseVo.getResponseList().size();
				setDefaultRows();
			} else {
				availableRecords = ConstantEnum.CONTENT_TYPE_1.getIntValue();
				setDefaultRows();
			}
			log.debug(getText("METHOD_LOG_END") + "searchTask");
			return "open_task";
		} catch (CustomApplicationException ce) {
			log.error(getLogMessage() + ce.getLogMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			setErrorDetailMessage(ce.getLogMessage());
			setDefaultRows();
			return "open_task";
		} catch (Exception e) {
			log.error(getLogMessage() + e.getMessage());
			setErrorMsg(ResultConstant.ADMIN_ERROR);
			setErrorDetailMessage(e.getMessage());
			setDefaultRows();
			return "open_task";
		}
	}

	public void setDefaultRows() {
		if (responseVo == null) {
			responseVo = new ResponseVo();
		}
		if (responseVo.getResponseList() == null || responseVo.getResponseList().size() == 0) {
			addActionError("No records found");
			List<TaskActionVo> taskActionVos = new ArrayList<TaskActionVo>();
			TaskActionVo c = new TaskActionVo();
			c.setTaskDate(CommonUtils.convertStartTime(CommonUtils.getCurrentDate()));
			c.setTaskDateStr(CommonUtils.convertdateTostring(c.getTaskDate(), "dd/MM/yyyy"));
			c.setPlanStartTime(CommonUtils.getCurrentTaskStartTime());
			c.setPlanEndTime(CommonUtils.getCurrentTaskEndTime());
			taskActionVos.add(c);
			responseVo.setResponseList(taskActionVos);
		}
	}

	@SkipValidation
	public String addTask() {
		try {
			UserVo userVo = requestVo.getUserVo();
			if (userVo.getToken().equals(token)) {
				currentDate = CommonUtils.getCurrentDateWithoutTime();
				currentDateStr = CommonUtils.convertdateTostring(currentDate, "dd/MM/yyyy");
				currentTime = CommonUtils.getCurrentTime();

				if (empId == null)
					empId = userVo.getEmployeeId();
				if (empCode == null)
					empCode = userVo.getEmployeeCode();

				responseVo = new ResponseVo();
				responseVo.setResponseList(task);

				for (TaskActionVo taskVo : task) {
					taskVo.setCreatedOn(CommonUtils.getCurrentDate());
					taskVo.setTaskDate(CommonUtils.convertStringToDate(taskVo.getTaskDateStr(), "dd/MM/yy"));
					taskVo.setEmployeeId(empId);
					taskVo.setEmployeeCode(empCode);
				}

				if (!validation()) {
					return "open_task";
				}

				/* creating instance for service class bean */
				TaskService taskService = (TaskService) ContextLoader
						.getBean(FrameworkConstant.getBeanId("TASK_SERVICE"));

				requestVo = new RequestVo();
				requestVo.setUserVo(userVo);
				requestVo.setSaveAndSend(saveAndSend);
				requestVo.setCc(cc);

				requestVo.setRequestList(task);
				ResponseVo responseVo = (ResponseVo) taskService.saveTaskList(requestVo);
				if (responseVo != null && responseVo.getErrorMessage() != null
						&& !("".equals(responseVo.getErrorMessage()))) {
					addActionError(responseVo.getErrorMessage());
				} else {
					searchTask();
					if (isSaveAndSend()) {
						addActionMessage("Task saved and mail sent successfully");
					} else {
						addActionMessage("Task saved successfully");
					}
				}
			} else {
				searchTask();
			}
		} catch (CustomApplicationException e) {
			addActionError("Error Occurred while saving task");
			log.error(e);
		}

		return "task";
	}

	private boolean recursiveValidation() {
		boolean isValid = true;
		if (recursiveTaskActionVo.getTaskDay().length() > 1) {
			if (recursiveTaskActionVo.getTaskDay().contains("9") || recursiveTaskActionVo.getTaskDay().contains("8")) {
				isValid = false;
			}
		}
		return isValid;
	}

	private boolean validation() {

		boolean validation = true;
		List<TaskActionVo> emptyRowList = new ArrayList<TaskActionVo>();

		for (int i = 0, size = task.size(); i < size; i++) {
			TaskActionVo taskVo = task.get(i);
			String row = String.valueOf(i + 1);
			boolean rowExist = false;
			if (!CommonUtils.isBlank(taskVo.getActualTaskDesc()) || !CommonUtils.isBlank(taskVo.getActualStartTime())
					|| !CommonUtils.isBlank(taskVo.getActualEndTime()) || !CommonUtils.isBlank(taskVo.getActivity())
					|| !CommonUtils.isBlank(taskVo.getWorkItem())) {

				if (CommonUtils.isBlank(taskVo.getActualTaskDesc())) {
					setRequiredErrorMessage(getText("label.ActualTaskDesc"), row);
					validation = false;
				} else {
					rowExist = true;
				}

				if (CommonUtils.isBlank(taskVo.getActualStartTime())) {
					setRequiredErrorMessage(getText("label.ActualStartTime"), row);
					validation = false;
				} else {
					rowExist = true;
				}

				if (CommonUtils.isBlank(taskVo.getActualEndTime())) {
					setRequiredErrorMessage(getText("label.ActualEndTime"), row);
					validation = false;
				} else {
					rowExist = true;
				}

				if (CommonUtils.isBlank(taskVo.getActivityId())) {
					setRequiredErrorMessage(getText("label.Activity"), row);
					validation = false;
				} else {
					rowExist = true;
				}

				if (CommonUtils.isBlank(taskVo.getWorkItem())) {
					setRequiredErrorMessage(getText("WorkItem"), row);
					validation = false;
				} else {
					rowExist = true;
				}

			} else {
				if (taskVo.getProjectTypeActionVo().getProjectTypeId() != 0) {
					rowExist = true;
				}
				if (!CommonUtils.isBlank(taskVo.getProjectId())) {
					rowExist = true;
				}
				if (taskVo.getProjectTypeActionVo().getProjectTypeId() == 0
						&& !CommonUtils.isBlank(taskVo.getProjectId())) {
					setRequiredErrorMessage(getText("label.ProjectType"), row);
					validation = false;
				}
				/*
				 * if(taskVo.getProjectTypeActionVo().getProjectTypeId() != 0 &&
				 * CommonUtils.isBlank(taskVo.getProjectId())){
				 * setRequiredErrorMessage(getText("label.ProjectId"),row);
				 * validation = false; }
				 */
				if (!CommonUtils.isBlank(taskVo.getPlanTaskDesc())) {
					rowExist = true;
				}
				if (!CommonUtils.isBlank(taskVo.getPlanStartTime())) {
					rowExist = true;
				}
				if (!CommonUtils.isBlank(taskVo.getPlanEndTime())) {
					rowExist = true;
				}
			}
			if (!rowExist) {
				emptyRowList.add(taskVo);
			}
		}

		if (validation && emptyRowList.size() > 0) {
			task.removeAll(emptyRowList);
		}

		return validation;
	}

	/**
	 * @Method : prepare
	 * @Description : default values
	 *
	 */
	private void setRequiredErrorMessage(String arg0) {
		setRequiredErrorMessage(arg0, "");
	}

	private void setRequiredErrorMessage(String arg0, String arg1) {
		setRequiredErrorMessage(arg0, arg1, "");
	}

	private void setRequiredErrorMessage(String arg0, String arg1, String arg2) {
		setRequiredErrorMessage(arg0, arg1, arg2, "");
	}

	private void setRequiredErrorMessage(String arg0, String arg1, String arg2, String arg3) {
		String requiredMsg = getText("error.row.required");
		requiredMsg = requiredMsg.replace("{0}", arg0);
		requiredMsg = requiredMsg.replace("{1}", arg1);
		requiredMsg = requiredMsg.replace("{2}", arg2);
		requiredMsg = requiredMsg.replace("{3}", arg3);
		addActionError(requiredMsg);
	}

	@SkipValidation
	public String mappedProjectList() throws CustomApplicationException {
		try {
			/* creating instance for service class bean */
			TaskService taskService = (TaskService) ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_SERVICE"));

			UserVo userVo = (UserVo) getServletRequest().getSession().getAttribute("user");
			requestVo.setUserVo(userVo);
			requestVo.setComboType("allDepartmentProject");
			requestVo.setParamIds(servletRequest.getParameter("projectType"));

			requestVo.setParamNames(new String[] { "statusId" });
			requestVo.setValues(new String[] { servletRequest.getParameter("statusId") });

			responseVo = taskService.comboLoading(requestVo);
			deptProjectList = (Map<String, List<ComboVo>>) responseVo.getComboValueMap();

		} catch (CustomApplicationException e) {
			throw new CustomApplicationException(e);
		}
		return "project";
	}

	public String ccList() throws CustomApplicationException {
		try {
			/* creating instance for service class bean */
			TaskService taskService = (TaskService) ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_SERVICE"));
			requestVo.setComboType("ccList");
			responseVo = taskService.comboLoading(requestVo);
			ccList = (List<ComboVo>) responseVo.getComboValueList();

		} catch (CustomApplicationException e) {
			throw new CustomApplicationException(e);
		}
		return "cc";
	}

	public String projectList() throws CustomApplicationException {
		try {
			/* creating instance for service class bean */
			TaskService taskService = (TaskService) ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_SERVICE"));

			UserVo userVo = (UserVo) getServletRequest().getSession().getAttribute("user");
			requestVo.setUserVo(userVo);
			requestVo.setComboType("departmentProject");
			requestVo.setParamIds(servletRequest.getParameter("projectType"));
			responseVo = taskService.comboLoading(requestVo);
			projectList = (List<ComboVo>) responseVo.getComboValueList();

		} catch (CustomApplicationException e) {
			throw new CustomApplicationException(e);
		}
		return "project";
	}

	@SkipValidation
	public String activityList() throws CustomApplicationException {
		try {
			/* creating instance for service class bean */
			TaskService taskService = (TaskService) ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_SERVICE"));

			requestVo = new RequestVo();
			requestVo.setComboType("activity");
			responseVo = taskService.comboLoading(requestVo);
			activityList = (List<ComboVo>) responseVo.getComboValueList();

		} catch (CustomApplicationException e) {
			throw new CustomApplicationException(e);
		}
		return "project";
	}

	/**
	 * @Method : prepare
	 * @Description : default values
	 *
	 */
	@Override
	public void prepare() {

		try {
			if (requestVo == null) {
				requestVo = new RequestVo();
				UserVo userVo = (UserVo) getServletRequest().getSession().getAttribute("user");
				requestVo.setUserVo(userVo);
			}

			timeList = new ArrayList<ComboVo>();
			setTimeCombo("", "Select");
			setTimeCombo("00:00:00", "00:00");
			setTimeCombo("00:15:00", "00:15");
			setTimeCombo("00:30:00", "00:30");
			setTimeCombo("00:45:00", "00:45");
			setTimeCombo("01:00:00", "01:00");
			setTimeCombo("01:15:00", "01:15");
			setTimeCombo("01:30:00", "01:30");
			setTimeCombo("01:45:00", "01:45");
			setTimeCombo("02:00:00", "02:00");
			setTimeCombo("02:15:00", "02:15");
			setTimeCombo("02:30:00", "02:30");
			setTimeCombo("02:45:00", "02:45");
			setTimeCombo("03:00:00", "03:00");
			setTimeCombo("03:15:00", "03:15");
			setTimeCombo("03:30:00", "03:30");
			setTimeCombo("03:45:00", "03:45");
			setTimeCombo("04:00:00", "04:00");
			setTimeCombo("04:15:00", "04:15");
			setTimeCombo("04:30:00", "04:30");
			setTimeCombo("04:45:00", "04:45");
			setTimeCombo("05:00:00", "05:00");
			setTimeCombo("05:15:00", "05:15");
			setTimeCombo("05:30:00", "05:30");
			setTimeCombo("05:45:00", "05:45");
			setTimeCombo("06:00:00", "06:00");
			setTimeCombo("06:15:00", "06:15");
			setTimeCombo("06:30:00", "06:30");
			setTimeCombo("06:45:00", "06:45");
			setTimeCombo("07:00:00", "07:00");
			setTimeCombo("07:15:00", "07:15");
			setTimeCombo("07:30:00", "07:30");
			setTimeCombo("07:45:00", "07:45");
			setTimeCombo("08:00:00", "08:00");
			setTimeCombo("08:15:00", "08:15");
			setTimeCombo("08:30:00", "08:30");
			setTimeCombo("08:45:00", "08:45");
			setTimeCombo("09:00:00", "09:00");
			setTimeCombo("09:15:00", "09:15");
			setTimeCombo("09:30:00", "09:30");
			setTimeCombo("09:45:00", "09:45");
			setTimeCombo("10:00:00", "10:00");
			setTimeCombo("10:15:00", "10:15");
			setTimeCombo("10:30:00", "10:30");
			setTimeCombo("10:45:00", "10:45");
			setTimeCombo("11:00:00", "11:00");
			setTimeCombo("11:15:00", "11:15");
			setTimeCombo("11:30:00", "11:30");
			setTimeCombo("11:45:00", "11:45");
			setTimeCombo("12:00:00", "12:00");
			setTimeCombo("12:15:00", "12:15");
			setTimeCombo("12:30:00", "12:30");
			setTimeCombo("12:45:00", "12:45");
			setTimeCombo("13:00:00", "13:00");
			setTimeCombo("13:15:00", "13:15");
			setTimeCombo("13:30:00", "13:30");
			setTimeCombo("13:45:00", "13:45");
			setTimeCombo("14:00:00", "14:00");
			setTimeCombo("14:15:00", "14:15");
			setTimeCombo("14:30:00", "14:30");
			setTimeCombo("14:45:00", "14:45");
			setTimeCombo("15:00:00", "15:00");
			setTimeCombo("15:15:00", "15:15");
			setTimeCombo("15:30:00", "15:30");
			setTimeCombo("15:45:00", "15:45");
			setTimeCombo("16:00:00", "16:00");
			setTimeCombo("16:15:00", "16:15");
			setTimeCombo("16:30:00", "16:30");
			setTimeCombo("16:45:00", "16:45");
			setTimeCombo("17:00:00", "17:00");
			setTimeCombo("17:15:00", "17:15");
			setTimeCombo("17:30:00", "17:30");
			setTimeCombo("17:45:00", "17:45");
			setTimeCombo("18:00:00", "18:00");
			setTimeCombo("18:15:00", "18:15");
			setTimeCombo("18:30:00", "18:30");
			setTimeCombo("18:45:00", "18:45");
			setTimeCombo("19:00:00", "19:00");
			setTimeCombo("19:15:00", "19:15");
			setTimeCombo("19:30:00", "19:30");
			setTimeCombo("19:45:00", "19:45");
			setTimeCombo("20:00:00", "20:00");
			setTimeCombo("20:15:00", "20:15");
			setTimeCombo("20:30:00", "20:30");
			setTimeCombo("20:45:00", "20:45");
			setTimeCombo("21:00:00", "21:00");
			setTimeCombo("21:15:00", "21:15");
			setTimeCombo("21:30:00", "21:30");
			setTimeCombo("21:45:00", "21:45");
			setTimeCombo("22:00:00", "22:00");
			setTimeCombo("22:15:00", "22:15");
			setTimeCombo("22:30:00", "22:30");
			setTimeCombo("22:45:00", "22:45");
			setTimeCombo("23:00:00", "23:00");
			setTimeCombo("23:15:00", "23:15");
			setTimeCombo("23:30:00", "23:30");
			setTimeCombo("23:45:00", "23:45");

			dayList = new ArrayList<ComboVo>();
			setDayCombo("1", "Sunday");
			setDayCombo("2", "Monday");
			setDayCombo("3", "Tuesday");
			setDayCombo("4", "Wednesday");
			setDayCombo("5", "Thursday");
			setDayCombo("6", "Friday");
			setDayCombo("7", "Saturday");
			setDayCombo("8", "Week days");
			setDayCombo("9", "All days");

			comboLoading();

		} catch (Exception e) {
			log.error(getLogMessage() + e.getLocalizedMessage());
		}
	}

	private void setTimeCombo(String key, String value) {
		ComboVo comboVo = new ComboVo();
		comboVo.setKey(key);
		comboVo.setName(value);
		timeList.add(comboVo);
	}

	private void setDayCombo(String key, String value) {
		ComboVo comboVo = new ComboVo();
		comboVo.setKey(key);
		comboVo.setName(value);
		dayList.add(comboVo);
	}

	/**
	 * @throws CustomApplicationException
	 * @Method :
	 * @Description :
	 */
	private void comboLoading() throws CustomApplicationException {
		try {
			/* creating instance for service class bean */
			TaskService taskService = (TaskService) ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_SERVICE"));

			requestVo.setComboType("allProject");
			responseVo = taskService.comboLoading(requestVo);
			allProjectList = (Map<String, List<ComboVo>>) responseVo.getComboValueMap();

			requestVo.setComboType("projectType");
			responseVo = taskService.comboLoading(requestVo);
			projectTypeVoList = (List<ProjectTypeActionVo>) responseVo.getResponseList();

			requestVo.setComboType("activity");
			responseVo = taskService.comboLoading(requestVo);
			activityList = (List<ComboVo>) responseVo.getComboValueList();

			requestVo.setComboType("projectManager");
			responseVo = taskService.comboLoading(requestVo);
			projectManagerList = (List<ComboVo>) responseVo.getComboValueList();

			requestVo.setComboType("unit");
			responseVo = taskService.comboLoading(requestVo);
			unitList = (List<ComboVo>) responseVo.getComboValueList();

		} catch (CustomApplicationException e) {
			throw new CustomApplicationException(e);
		}

	}

	/**
	 * Getter method for taskActionVo
	 *
	 * @return the taskActionVo (TaskActionVo)
	 */
	public TaskActionVo getTaskActionVo() {
		return taskActionVo;
	}

	/**
	 * Setter method for taskActionVo
	 *
	 * @param taskActionVo
	 *            the taskActionVo to set
	 */
	public void setTaskActionVo(TaskActionVo taskActionVo) {
		this.taskActionVo = taskActionVo;
	}

	/**
	 * Getter method for projectTypeActionVo
	 *
	 * @return the projectTypeActionVo (ProjectTypeActionVo)
	 */
	public ProjectActionVo getProjectTypeActionVo() {
		return projectTypeActionVo;
	}

	/**
	 * Setter method for projectTypeActionVo
	 *
	 * @param projectTypeActionVo
	 *            the projectTypeActionVo to set
	 */
	public void setProjectTypeActionVo(ProjectActionVo projectTypeActionVo) {
		this.projectTypeActionVo = projectTypeActionVo;
	}

	/**
	 * Getter method for responseVo
	 *
	 * @return the responseVo (ResponseVo)
	 */
	public ResponseVo getResponseVo() {
		return responseVo;
	}

	/**
	 * Setter method for responseVo
	 *
	 * @param responseVo
	 *            the responseVo to set
	 */
	public void setResponseVo(ResponseVo responseVo) {
		this.responseVo = responseVo;
	}

	/**
	 * Getter method for requestVo
	 *
	 * @return the requestVo (RequestVo)
	 */
	public RequestVo getRequestVo() {
		return requestVo;
	}

	/**
	 * Setter method for requestVo
	 *
	 * @param requestVo
	 *            the requestVo to set
	 */
	public void setRequestVo(RequestVo requestVo) {
		this.requestVo = requestVo;
	}

	/**
	 * Getter method for startDate
	 *
	 * @return the startDate (String)
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * Setter method for startDate
	 *
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * Getter method for endDate
	 *
	 * @return the endDate (String)
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * Setter method for endDate
	 *
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * Getter method for projectList
	 *
	 * @return the projectList (List)
	 */
	public List<ComboVo> getProjectList() {
		return projectList;
	}

	/**
	 * Setter method for projectList
	 *
	 * @param projectList
	 *            the projectList to set
	 */
	public void setProjectList(List<ComboVo> projectList) {
		this.projectList = projectList;
	}

	/**
	 * Getter method for projectTypeVoList
	 *
	 * @return the projectTypeVoList (List<ProjectTypeActionVo>)
	 */
	public List<ProjectTypeActionVo> getProjectTypeVoList() {
		return projectTypeVoList;
	}

	/**
	 * Setter method for projectTypeVoList
	 *
	 * @param projectTypeVoList
	 *            the projectTypeVoList to set
	 */
	public void setProjectTypeVoList(List<ProjectTypeActionVo> projectTypeVoList) {
		this.projectTypeVoList = projectTypeVoList;
	}

	/**
	 * Getter method for projectDate
	 *
	 * @return the projectDate (String)
	 */
	public String getProjectDate() {
		return projectDate;
	}

	/**
	 * Setter method for projectDate
	 *
	 * @param projectDate
	 *            the projectDate to set
	 */
	public void setProjectDate(String projectDate) {
		this.projectDate = projectDate;
	}

	/**
	 * Getter method for task
	 *
	 * @return the task (List<TaskActionVo>)
	 */
	public List<TaskActionVo> getTask() {
		return task;
	}

	/**
	 * Setter method for task
	 *
	 * @param task
	 *            the task to set
	 */
	public void setTask(List<TaskActionVo> task) {
		this.task = task;
	}

	/**
	 * Getter method for activityList
	 *
	 * @return the activityList (List<ComboVo>)
	 */
	public List<ComboVo> getActivityList() {
		return activityList;
	}

	/**
	 * Setter method for activityList
	 *
	 * @param activityList
	 *            the activityList to set
	 */
	public void setActivityList(List<ComboVo> activityList) {
		this.activityList = activityList;
	}

	public String getProjectListId() {
		return projectListId;
	}

	public void setProjectListId(String projectListId) {
		this.projectListId = projectListId;
	}

	public String getProjectCodeId() {
		return projectCodeId;
	}

	public void setProjectCodeId(String projectCodeId) {
		this.projectCodeId = projectCodeId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public List<ComboVo> getProjectManagerList() {
		return projectManagerList;
	}

	public void setProjectManagerList(List<ComboVo> projectManagerList) {
		this.projectManagerList = projectManagerList;
	}

	public List<ComboVo> getTimeList() {
		return timeList;
	}

	public void setTimeList(List<ComboVo> timeList) {
		this.timeList = timeList;
	}

	public String getTaskStartDate() {
		return taskStartDate;
	}

	public void setTaskStartDate(String taskStartDate) {
		this.taskStartDate = taskStartDate;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public String getCurrentDateStr() {
		return currentDateStr;
	}

	public void setCurrentDateStr(String currentDateStr) {
		this.currentDateStr = currentDateStr;
	}

	public boolean isSaveAndSend() {
		return saveAndSend;
	}

	public void setSaveAndSend(boolean saveAndSend) {
		this.saveAndSend = saveAndSend;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public List<ComboVo> getPracticeTeamList() {
		return practiceTeamList;
	}

	public void setPracticeTeamList(List<ComboVo> practiceTeamList) {
		this.practiceTeamList = practiceTeamList;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public int getAvailableRecords() {
		return availableRecords;
	}

	public void setAvailableRecords(int availableRecords) {
		this.availableRecords = availableRecords;
	}

	public String getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}

	public String getProjectTypeId() {
		return projectTypeId;
	}

	public void setProjectTypeId(String projectTypeId) {
		this.projectTypeId = projectTypeId;
	}

	public String getTaskDisplayMsg() {
		return taskDisplayMsg;
	}

	public void setTaskDisplayMsg(String taskDisplayMsg) {
		this.taskDisplayMsg = taskDisplayMsg;
	}

	public List<ComboVo> getDayList() {
		return dayList;
	}

	public void setDayList(List<ComboVo> dayList) {
		this.dayList = dayList;
	}

	public RecursiveTaskActionVo getRecursiveTaskActionVo() {
		return recursiveTaskActionVo;
	}

	public void setRecursiveTaskActionVo(RecursiveTaskActionVo recursiveTaskActionVo) {
		this.recursiveTaskActionVo = recursiveTaskActionVo;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<ComboVo> getUnitList() {
		return unitList;
	}

	public void setUnitList(List<ComboVo> unitList) {
		this.unitList = unitList;
	}

	public Map<String, List<ComboVo>> getDeptProjectList() {
		return deptProjectList;
	}

	public void setDeptProjectList(Map<String, List<ComboVo>> deptProjectList) {
		this.deptProjectList = deptProjectList;
	}

	public Map<String, List<ComboVo>> getAllProjectList() {
		return allProjectList;
	}

	public void setAllProjectList(Map<String, List<ComboVo>> allProjectList) {
		this.allProjectList = allProjectList;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public List<ComboVo> getCcList() {
		return ccList;
	}

	public void setCcList(List<ComboVo> ccList) {
		this.ccList = ccList;
	}

}
