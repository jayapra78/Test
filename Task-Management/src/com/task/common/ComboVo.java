package com.task.common;



public class ComboVo extends CommonVo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idValue;
	private Long idLongValue;
	private String name;
	private String key;
	private String refId;
	private String refId2;
	private String commonName;
	
	
	/**
	 * @return the idLongValue
	 */
	public Long getIdLongValue() {
		return idLongValue;
	}
	/**
	 * @param idLongValue the idLongValue to set
	 */
	public void setIdLongValue(Long idLongValue) {
		this.idLongValue = idLongValue;
	}
	/**
	 * @return the idValue
	 */
	public Integer getIdValue() {
		return idValue;
	}
	/**
	 * @param idValue the idValue to set
	 */
	public void setIdValue(Integer idValue) {
		this.idValue = idValue;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Getter method for key
	 * 
	 * @return the key (String)
	 */
	public String getKey() {
		return key;
	}
	/**
	 * Setter method for key
	 *
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * Getter method for commonName
	 * 
	 * @return the commonName (String)
	 */
	public String getCommonName() {
		return commonName;
	}
	/**
	 * Setter method for commonName
	 *
	 * @param commonName the commonName to set
	 */
	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public String getRefId2() {
		return refId2;
	}
	public void setRefId2(String refId2) {
		this.refId2 = refId2;
	}
}
