package com.task.common;

import java.io.Serializable;

public class CommonComboVo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String comboId;
	
	private String comboName;
	
	private String comboCondition;
	
	private String valueObject;

	public String getComboId() {
		return comboId;
	}

	public void setComboId(String comboId) {
		this.comboId = comboId;
	}

	public String getComboName() {
		return comboName;
	}

	public void setComboName(String comboName) {
		this.comboName = comboName;
	}

	public String getComboCondition() {
		return comboCondition;
	}

	public void setComboCondition(String comboCondition) {
		this.comboCondition = comboCondition;
	}

	public String getValueObject() {
		return valueObject;
	}

	public void setValueObject(String valueObject) {
		this.valueObject = valueObject;
	}
	
}
