/**
 * 
 */
package com.task.common;

import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;

/**
 * @author neethimohana
 *
 */
public class CommonDaoConjunctionVo extends CommonVo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Conjunction conjuction = Restrictions.conjunction();

	/**
	 * Getter method for conjuction
	 * 
	 * @return the conjuction (Conjunction)
	 */
	public Conjunction getConjuction() {
		return conjuction;
	}

	/**
	 * Setter method for conjuction
	 *
	 * @param conjuction the conjuction to set
	 */
	public void setConjuction(Conjunction conjuction) {
		this.conjuction = conjuction;
	}

	
}
