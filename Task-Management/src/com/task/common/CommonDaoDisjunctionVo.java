/**
 * 
 */
package com.task.common;

import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

/**
 * @author neethimohana
 *
 */
public class CommonDaoDisjunctionVo extends CommonVo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Disjunction disjunction = Restrictions.disjunction();

	public Disjunction getDisjunction() {
		return disjunction;
	}

	public void setDisjunction(Disjunction disjunction) {
		this.disjunction = disjunction;
	}
	
}
