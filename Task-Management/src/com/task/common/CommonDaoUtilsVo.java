/**
 * 
 */
package com.task.common;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.task.util.ApplicationPagingVo;

/**
 * @author Neethimohan A
 *
 */
public class CommonDaoUtilsVo extends CommonVo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private DetachedCriteria criteria;
	
	private Restrictions restrictions;
	
	private ApplicationPagingVo pagingVo;
	
	private CommonVo commonVo;	
	
	/**
	 * The Method to get commonVo
	 *
	 * @return commonVo
	 */
	public CommonVo getCommonVo() {
		return commonVo;
	}
	/** 
	 * The Method to set commonVo
	 *
	 * @param commonVo  commonVo 
	 */
	public void setCommonVo(CommonVo commonVo) {
		this.commonVo = commonVo;
	}
	/**
	 * The Method to get pagingVo
	 *
	 * @return pagingVo
	 */
	public ApplicationPagingVo getPagingVo() {
		return pagingVo;
	}
	/** 
	 * The Method to set pagingVo
	 *
	 * @param pagingVo  pagingVo 
	 */
	public void setPagingVo(ApplicationPagingVo pagingVo) {
		this.pagingVo = pagingVo;
	}
	/**
	 * The Method to get criteria
	 *
	 * @return criteria
	 */
	public DetachedCriteria getCriteria() {
		return criteria;
	}
	/** 
	 * The Method to set criteria
	 *
	 * @param criteria  criteria 
	 */
	public void setCriteria(DetachedCriteria criteria) {
		this.criteria = criteria;
	}
	/**
	 * @return the restrictions
	 */
	public Restrictions getRestrictions() {
		return restrictions;
	}
	/**
	 * @param restrictions the restrictions to set
	 */
	public void setRestrictions(Restrictions restrictions) {
		this.restrictions = restrictions;
	}
	
}
