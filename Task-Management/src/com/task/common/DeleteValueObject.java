package com.task.common;

import java.io.Serializable;

public interface DeleteValueObject extends Serializable{
	public abstract void setDeleteFlag(Integer deleteFlag);
}
