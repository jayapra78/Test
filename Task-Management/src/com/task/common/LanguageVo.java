package com.task.common;


public class LanguageVo extends CommonVo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String languageCode;
	private String languageDisplay;

	/**
	 * @return the languageCode
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * @param languageCode
	 *            the languageCode to set
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @return the languageDisplay
	 */
	public String getLanguageDisplay() {
		return languageDisplay;
	}

	/**
	 * @param languageDisplay
	 *            the languageDisplay to set
	 */
	public void setLanguageDisplay(String languageDisplay) {
		this.languageDisplay = languageDisplay;
	}

}
