package com.task.common;


public class MailVo extends CommonVo {

	private static final long serialVersionUID = 1L;

	private String mailTo[];
	
	private String mailCc[];

	private String mailFrom;

	private String subject;

	private String message;

	private String personal;
    
	private String attachment;
	
	private String header;

	public String[] getMailTo() {
		return mailTo;
	}

	public void setMailTo(String[] mailTo) {
		this.mailTo = mailTo;
	}

	public String getMailFrom() {
		return mailFrom;
	}

	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPersonal() {
		return personal;
	}

	public void setPersonal(String personal) {
		this.personal = personal;
	}


	/**
	 * Getter method for mailCc
	 * 
	 * @return the mailCc (String[])
	 */
	public String[] getMailCc() {
		return mailCc;
	}

	/**
	 * Setter method for mailCc
	 *
	 * @param mailCc the mailCc to set
	 */
	public void setMailCc(String[] mailCc) {
		this.mailCc = mailCc;
	}

	/**
	 * Getter method for header
	 * 
	 * @return the header (String)
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * Setter method for header
	 *
	 * @param header the header to set
	 */
	public void setHeader(String header) {
		this.header = header;
	}

	/**
	 * Getter method for attachment
	 * 
	 * @return the attachment (String)
	 */
	public String getAttachment() {
		return attachment;
	}

	/**
	 * Setter method for attachment
	 *
	 * @param attachment the attachment to set
	 */
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
}
