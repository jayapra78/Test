package com.task.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class ReportVo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<HashMap<Integer, Integer>> lineChartMap;
	
	private HashMap<Integer, Integer> barChartMap;
	
	private HashMap<Integer,String> color;
	
	private HashMap<Integer,String> label;
	
	private List<ReportVo> reportList;
	
	private List<String> barLabel;
	
	private List<String> xaxisTick;
	
	private Integer width;
	
	private Integer height;
	
	private Integer xaxisMin;
	
	private Integer yaxisMin;
	
	private Integer xaxisMax;
	
	private Integer yaxisMax;
	
	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public List<HashMap<Integer, Integer>> getLineChartMap() {
		return lineChartMap;
	}

	public void setLineChartMap(List<HashMap<Integer, Integer>> lineChartMap) {
		this.lineChartMap = lineChartMap;
	}

	public HashMap<Integer, Integer> getBarChartMap() {
		return barChartMap;
	}

	public void setBarChartMap(HashMap<Integer, Integer> barChartMap) {
		this.barChartMap = barChartMap;
	}

	public HashMap<Integer, String> getColor() {
		return color;
	}

	public void setColor(HashMap<Integer, String> color) {
		this.color = color;
	}

	public HashMap<Integer, String> getLabel() {
		return label;
	}

	public void setLabel(HashMap<Integer, String> label) {
		this.label = label;
	}

	public List<ReportVo> getReportList() {
		return reportList;
	}

	public void setReportList(List<ReportVo> reportList) {
		this.reportList = reportList;
	}

	public List<String> getBarLabel() {
		return barLabel;
	}

	public void setBarLabel(List<String> barLabel) {
		this.barLabel = barLabel;
	}

	public List<String> getXaxisTick() {
		return xaxisTick;
	}

	public void setXaxisTick(List<String> xaxisTick) {
		this.xaxisTick = xaxisTick;
	}

	public Integer getXaxisMin() {
		return xaxisMin;
	}

	public void setXaxisMin(Integer xaxisMin) {
		this.xaxisMin = xaxisMin;
	}

	public Integer getYaxisMin() {
		return yaxisMin;
	}

	public void setYaxisMin(Integer yaxisMin) {
		this.yaxisMin = yaxisMin;
	}

	public Integer getXaxisMax() {
		return xaxisMax;
	}

	public void setXaxisMax(Integer xaxisMax) {
		this.xaxisMax = xaxisMax;
	}

	public Integer getYaxisMax() {
		return yaxisMax;
	}

	public void setYaxisMax(Integer yaxisMax) {
		this.yaxisMax = yaxisMax;
	}
	
}
