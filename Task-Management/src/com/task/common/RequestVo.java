package com.task.common;

import java.util.List;
import java.util.Locale;

import com.task.util.ApplicationPagingVo;

public class RequestVo extends CommonVo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ApplicationPagingVo pagingVo;
	private CommonVo seacrhVo;
	private CommonVo commonVo;
	private String paramIds = null;
	private List<? extends CommonVo> requestList;
	private String comboType;
	private UserVo userVo;
	private Locale userLocale;
	private Long actionType;
	private Long jobId;
	private List<String> errorList;
	private Character deleteFlag;
	private String[] paramNames;
	private Object[] values;
	private boolean saveAndSend;
	private String cc;
	/**
	 * Getter method for pagingVo
	 * 
	 * @return the pagingVo (ApplicationPagingVo)
	 */
	public ApplicationPagingVo getPagingVo() {
		return pagingVo;
	}
	/**
	 * Setter method for pagingVo
	 *
	 * @param pagingVo the pagingVo to set
	 */
	public void setPagingVo(ApplicationPagingVo pagingVo) {
		this.pagingVo = pagingVo;
	}
	/**
	 * Getter method for seacrhVo
	 * 
	 * @return the seacrhVo (CommonVo)
	 */
	public CommonVo getSeacrhVo() {
		return seacrhVo;
	}
	/**
	 * Setter method for seacrhVo
	 *
	 * @param seacrhVo the seacrhVo to set
	 */
	public void setSeacrhVo(CommonVo seacrhVo) {
		this.seacrhVo = seacrhVo;
	}
	/**
	 * Getter method for commonVo
	 * 
	 * @return the commonVo (CommonVo)
	 */
	public CommonVo getCommonVo() {
		return commonVo;
	}
	/**
	 * Setter method for commonVo
	 *
	 * @param commonVo the commonVo to set
	 */
	public void setCommonVo(CommonVo commonVo) {
		this.commonVo = commonVo;
	}
	/**
	 * Getter method for paramIds
	 * 
	 * @return the paramIds (String)
	 */
	public String getParamIds() {
		return paramIds;
	}
	/**
	 * Setter method for paramIds
	 *
	 * @param paramIds the paramIds to set
	 */
	public void setParamIds(String paramIds) {
		this.paramIds = paramIds;
	}
	/**
	 * Getter method for requestList
	 * 
	 * @return the requestList (List<? extends CommonVo>)
	 */
	public List<? extends CommonVo> getRequestList() {
		return requestList;
	}
	/**
	 * Setter method for requestList
	 *
	 * @param requestList the requestList to set
	 */
	public void setRequestList(List<? extends CommonVo> requestList) {
		this.requestList = requestList;
	}
	/**
	 * Getter method for comboType
	 * 
	 * @return the comboType (Integer)
	 */
	public String getComboType() {
		return comboType;
	}
	/**
	 * Setter method for comboType
	 *
	 * @param comboType the comboType to set
	 */
	public void setComboType(String comboType) {
		this.comboType = comboType;
	}
	/**
	 * Getter method for userVo
	 * 
	 * @return the userVo (UserVo)
	 */
	public UserVo getUserVo() {
		return userVo;
	}
	/**
	 * Setter method for userVo
	 *
	 * @param userVo the userVo to set
	 */
	public void setUserVo(UserVo userVo) {
		this.userVo = userVo;
	}
	/**
	 * Getter method for userLocale
	 * 
	 * @return the userLocale (Locale)
	 */
	public Locale getUserLocale() {
		return userLocale;
	}
	/**
	 * Setter method for userLocale
	 *
	 * @param userLocale the userLocale to set
	 */
	public void setUserLocale(Locale userLocale) {
		this.userLocale = userLocale;
	}
	/**
	 * Getter method for jobId
	 * 
	 * @return the jobId (Long)
	 */
	public Long getJobId() {
		return this.jobId;
	}
	/**
	 * Setter method for jobId
	 *
	 * @param jobId the jobId to set
	 */
	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}
	/**
	 * Getter method for actionType
	 * 
	 * @return the actionType (Long)
	 */
	public Long getActionType() {
		return actionType;
	}
	/**
	 * Setter method for actionType
	 *
	 * @param actionType the actionType to set
	 */
	public void setActionType(Long actionType) {
		this.actionType = actionType;
	}
	
	/**
	 * Getter method for errorList
	 * 
	 * @return the errorList (List<String>)
	 */
	public List<String> getErrorList() {
		return errorList;
	}
	/**
	 * Setter method for errorList
	 *
	 * @param errorList the errorList to set
	 */
	public void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}
	/**
	 * Getter method for deleteFlag
	 * 
	 * @return the deleteFlag (Character)
	 */
	public Character getDeleteFlag() {
		return deleteFlag;
	}
	/**
	 * Setter method for deleteFlag
	 *
	 * @param deleteFlag the deleteFlag to set
	 */
	public void setDeleteFlag(Character deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	/**
	 * Getter method for paramNames
	 * 
	 * @return the paramNames (String[])
	 */
	public String[] getParamNames() {
		return paramNames;
	}
	/**
	 * Setter method for paramNames
	 *
	 * @param paramNames the paramNames to set
	 */
	public void setParamNames(String[] paramNames) {
		this.paramNames = paramNames;
	}
	/**
	 * Getter method for values
	 * 
	 * @return the values (Object[])
	 */
	public Object[] getValues() {
		return values;
	}
	/**
	 * Setter method for values
	 *
	 * @param values the values to set
	 */
	public void setValues(Object[] values) {
		this.values = values;
	}
	public boolean isSaveAndSend() {
		return saveAndSend;
	}
	public void setSaveAndSend(boolean saveAndSend) {
		this.saveAndSend = saveAndSend;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}

}
