package com.task.common;

import java.util.List;
import java.util.Map;

import com.task.util.ApplicationPagingVo;

public class ResponseVo extends CommonVo {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9211806162998654945L;
	
	private List<? extends CommonVo> responseList;
	private ApplicationPagingVo pagingVo;
	private CommonVo commonVo;
	private CommonVo commonSearchVo;
	private String transactionMessage;
	private String transactionMessageCode;
	private String errorMessage;
//	String userIdentity;
	private Integer count;
	private List<ComboVo> comboValueList;
	@SuppressWarnings("unchecked")
	private Map comboValueMap;
	private SessionVo sessionVo;
	private String sequenceCode;
	private Integer sequenceKey;
	private List<String> errorList;
	private List<String> transactionMessageList;
	private Map mapvalue;
	private Long primaryKey;
	private List resultList;
	private String errorMessageCode;
	private String returnValue;
	private boolean statusFlag;
	private double totalTime;
	
	/**
	 * @return the comboValueList
	 */
	public List<ComboVo> getComboValueList() {
		return comboValueList;
	}

	/**
	 * @param comboValueList the comboValueList to set
	 */
	public void setComboValueList(List<ComboVo> comboValueList) {
		this.comboValueList = comboValueList;
	}

	/**
	 * The method to get responseList
	 *
	 * @return responseList
	 */
	public List<? extends CommonVo> getResponseList() {
		return responseList;
	}
	
	/**
	 * The method to set responseList
	 *
	 * @param responseList responseList 
	 */
	public void setResponseList(List<? extends CommonVo> responseList) {
		this.responseList = responseList;
	}
	
	/**
	 * The method to get pagingVo
	 *
	 * @return pagingVo
	 */
	public ApplicationPagingVo getPagingVo() {
		return pagingVo;
	}
	
	/**
	 * The method to set pagingVo
	 *
	 * @param pagingVo pagingVo 
	 */
	public void setPagingVo(ApplicationPagingVo pagingVo) {
		this.pagingVo = pagingVo;
	}
	
	/**
	 * The method to get commonVo
	 *
	 * @return commonVo
	 */
	public CommonVo getCommonVo() {
		return commonVo;
	}
	
	/**
	 * The method to set commonVo
	 *
	 * @param commonVo commonVo 
	 */
	public void setCommonVo(CommonVo commonVo) {
		this.commonVo = commonVo;
	}
	
	/**
	 * The method to get commonSearchVo
	 *
	 * @return commonSearchVo
	 */
	public CommonVo getCommonSearchVo() {
		return commonSearchVo;
	}
	
	/**
	 * The method to set commonSearchVo
	 *
	 * @param commonSearchVo commonSearchVo 
	 */
	public void setCommonSearchVo(CommonVo commonSearchVo) {
		this.commonSearchVo = commonSearchVo;
	}
	
	/**
	 * The method to get transactionMessage
	 *
	 * @return transactionMessage
	 */
	public String getTransactionMessage() {
		return transactionMessage;
	}
	
	/**
	 * The method to set transactionMessage
	 *
	 * @param transactionMessage transactionMessage 
	 */
	public void setTransactionMessage(String transactionMessage) {
		this.transactionMessage = transactionMessage;
	}
	
	/**
	 * The method to get errorMessage
	 *
	 * @return errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	
	/**
	 * The method to set errorMessage
	 *
	 * @param errorMessage errorMessage 
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the sessionVo
	 */
	public SessionVo getSessionVo() {
		return sessionVo;
	}

	/**
	 * @param sessionVo the sessionVo to set
	 */
	public void setSessionVo(SessionVo sessionVo) {
		this.sessionVo = sessionVo;
	}


	/**
	 * Getter method for sequenceCode
	 * 
	 * @return the sequenceCode (String)
	 */
	public String getSequenceCode() {
		return sequenceCode;
	}

	/**
	 * Setter method for sequenceCode
	 *
	 * @param sequenceCode the sequenceCode to set
	 */
	public void setSequenceCode(String sequenceCode) {
		this.sequenceCode = sequenceCode;
	}
	
	/**
	 * Getter method for sequenceKey
	 * 
	 * @return the sequenceKey (Integer)
	 */
	public Integer getSequenceKey() {
		return sequenceKey;
	}

	/**
	 * Setter method for sequenceKey
	 *
	 * @param sequenceKey the sequenceKey to set
	 */
	public void setSequenceKey(Integer sequenceKey) {
		this.sequenceKey = sequenceKey;
	}
	
	/**
	 * Getter method for comboValueMap
	 * 
	 * @return the comboValueMap (Map)
	 */
	public Map getComboValueMap() {
		return comboValueMap;
	}

	/**
	 * Setter method for comboValueMap
	 *
	 * @param comboValueMap the comboValueMap to set
	 */
	public void setComboValueMap(Map comboValueMap) {
		this.comboValueMap = comboValueMap;
	}
	/**
	 * Getter method for TransactionMessageList
	 * 
	 * @return the transactionMessageList (List)
	 */
	public List<String> getTransactionMessageList() {
		return transactionMessageList;
	}

	/**
	 * Setter method for TransactionMessageList
	 *
	 * @param transactionMessageList the transactionMessageList to set
	 */
	public void setTransactionMessageList(List<String> transactionMessageList) {
		this.transactionMessageList = transactionMessageList;
	}

	/**
	 * Getter method for errorList
	 * 
	 * @return the errorList (List<String>)
	 */
	public List<String> getErrorList() {
		return errorList;
	}

	/**
	 * Setter method for errorList
	 *
	 * @param errorList the errorList to set
	 */
	public void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}
	/**
	 * Getter method for mapvalue
	 * 
	 * @return the mapvalue (Map)
	 */
	public Map getMapvalue() {
		return mapvalue;
	}

	/**
	 * Setter method for mapvalue
	 *
	 * @param mapvalue the mapvalue to set
	 */
	public void setMapvalue(Map mapvalue) {
		this.mapvalue = mapvalue;
	}

	/**
	 * Getter method for primaryKey
	 * 
	 * @return the primaryKey (Long)
	 */
	public Long getPrimaryKey() {
		return primaryKey;
	}

	/**
	 * Setter method for primaryKey
	 *
	 * @param primaryKey the primaryKey to set
	 */
	public void setPrimaryKey(Long primaryKey) {
		this.primaryKey = primaryKey;
	}


	/**
	 * Getter method for resultList
	 * 
	 * @return the resultList (List)
	 */
	public List getResultList() {
		return resultList;
	}

	/**
	 * Setter method for resultList
	 *
	 * @param resultList the resultList to set
	 */
	public void setResultList(List resultList) {
		this.resultList = resultList;
	}


	/**
	 * Getter method for count
	 * 
	 * @return the count (Integer)
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 * Setter method for count
	 *
	 * @param count the count to set
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

	/**
	 * Getter method for transactionMessageCode
	 * 
	 * @return the transactionMessageCode (String)
	 */
	public String getTransactionMessageCode() {
		return transactionMessageCode;
	}

	/**
	 * Setter method for transactionMessageCode
	 *
	 * @param transactionMessageCode the transactionMessageCode to set
	 */
	public void setTransactionMessageCode(String transactionMessageCode) {
		this.transactionMessageCode = transactionMessageCode;
	}

	/**
	 * Getter method for errorMessageCode
	 * 
	 * @return the errorMessageCode (String)
	 */
	public String getErrorMessageCode() {
		return errorMessageCode;
	}

	/**
	 * Setter method for errorMessageCode
	 *
	 * @param errorMessageCode the errorMessageCode to set
	 */
	public void setErrorMessageCode(String errorMessageCode) {
		this.errorMessageCode = errorMessageCode;
	}

	/**
	 * Getter method for statusFlag
	 * 
	 * @return the statusFlag (boolean)
	 */
	public boolean isStatusFlag() {
		return statusFlag;
	}

	/**
	 * Setter method for statusFlag
	 *
	 * @param statusFlag the statusFlag to set
	 */
	public void setStatusFlag(boolean statusFlag) {
		this.statusFlag = statusFlag;
	}

	public String getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(String returnValue) {
		this.returnValue = returnValue;
	}

	/**
	 * Getter method for totalTime
	 * 
	 * @return the totalTime (double)
	 */
	public double getTotalTime() {
		return totalTime;
	}

	/**
	 * Setter method for totalTime
	 *
	 * @param totalTime the totalTime to set
	 */
	public void setTotalTime(double totalTime) {
		this.totalTime = totalTime;
	}

	
	
}
