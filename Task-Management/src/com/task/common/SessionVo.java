/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.common;

import java.util.Map;

/**
 * @author balajir
 * 
 */
public class SessionVo extends CommonVo {
	private static final long serialVersionUID = -1238686922880954349L;

	private UserVo userVo;
	private Map<String, String> authorizationMap;
	private CustomSessionVo customSessionVo;
	private Map<String, CommonVo> searchMap;
	/**
	 * Getter method for userVo
	 * 
	 * @return the userVo (UserVo)
	 */
	public UserVo getUserVo() {
		return userVo;
	}
	/**
	 * Setter method for userVo
	 *
	 * @param userVo the userVo to set
	 */
	public void setUserVo(UserVo userVo) {
		this.userVo = userVo;
	}
	/**
	 * Getter method for authorizationMap
	 * 
	 * @return the authorizationMap (Map<String,String>)
	 */
	public Map<String, String> getAuthorizationMap() {
		return authorizationMap;
	}
	/**
	 * Setter method for authorizationMap
	 *
	 * @param authorizationMap the authorizationMap to set
	 */
	public void setAuthorizationMap(Map<String, String> authorizationMap) {
		this.authorizationMap = authorizationMap;
	}
	/**
	 * Getter method for customSessionVo
	 * 
	 * @return the customSessionVo (CustomSessionVo)
	 */
	public CustomSessionVo getCustomSessionVo() {
		return customSessionVo;
	}
	/**
	 * Setter method for customSessionVo
	 *
	 * @param customSessionVo the customSessionVo to set
	 */
	public void setCustomSessionVo(CustomSessionVo customSessionVo) {
		this.customSessionVo = customSessionVo;
	}

	/**
	 * Getter method for searchMap
	 * 
	 * @return the searchMap
	 */
	public Map<String, CommonVo> getSearchMap() {
		return searchMap;
	}

	/**
	 * Setter method for searchMap
	 * 
	 * @param searchMap the searchMap to set
	 */
	public void setSearchMap(Map<String, CommonVo> searchMap) {
		this.searchMap = searchMap;
	}

}
