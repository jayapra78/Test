/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.common;


/**
 * @author balajir
 *
 */
public class UserVo extends CommonVo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long userId;
	private Integer userType;
	private String userName; // name
	private String loginName;
	private String employeeId; // id
	private String employeeCode; // code
	private String sysAdminId;
	private Long customerId;
	private Long userGroupId;
	private Long companyId;
	private String mailAddress;
	private String logoPath;
	private String companyMsgColor;
	private String systemMsgColor;
	private String deptId;
	private String desgId;
	private String deptName;
	private String desgName;
	private String emailId;
	private String token;
	private boolean guest;
	/**
	 * Getter method for mailAddress
	 * 
	 * @return the mailAddress (String)
	 */
	public String getMailAddress() {
		return mailAddress;
	}
	/**
	 * Setter method for mailAddress
	 *
	 * @param mailAddress the mailAddress to set
	 */
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	private String assocaiteCompany;
	private Long policyId;
	/**
	 * Getter method for userId
	 * 
	 * @return the userId (Long)
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * Setter method for userId
	 *
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * Getter method for userType
	 * 
	 * @return the userType (Integer)
	 */
	public Integer getUserType() {
		return userType;
	}
	/**
	 * Setter method for userType
	 *
	 * @param userType the userType to set
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	/**
	 * Getter method for userName
	 * 
	 * @return the userName (String)
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * Setter method for userName
	 *
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * Getter method for loginName
	 * 
	 * @return the loginName (String)
	 */
	public String getLoginName() {
		return loginName;
	}
	/**
	 * Setter method for loginName
	 *
	 * @param loginName the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	/**
	 * Getter method for employeeId
	 * 
	 * @return the employeeId (String)
	 */
	public String getEmployeeId() {
		return employeeId;
	}
	/**
	 * Setter method for employeeId
	 *
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	/**
	 * Getter method for sysAdminId
	 * 
	 * @return the sysAdminId (Long)
	 */
	public String getSysAdminId() {
		return sysAdminId;
	}
	/**
	 * Setter method for sysAdminId
	 *
	 * @param sysAdminId the sysAdminId to set
	 */
	public void setSysAdminId(String sysAdminId) {
		this.sysAdminId = sysAdminId;
	}
	/**
	 * Getter method for userGroupId
	 * 
	 * @return the userGroupId (Long)
	 */
	public Long getUserGroupId() {
		return userGroupId;
	}
	/**
	 * Setter method for userGroupId
	 *
	 * @param userGroupId the userGroupId to set
	 */
	public void setUserGroupId(Long userGroupId) {
		this.userGroupId = userGroupId;
	}
	/**
	 * Getter method for companyId
	 * 
	 * @return the companyId (Long)
	 */
	public Long getCompanyId() {
		return companyId;
	}
	/**
	 * Setter method for companyId
	 *
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	/**
	 * Getter method for assocaiteCompany
	 * 
	 * @return the assocaiteCompany (String)
	 */
	public String getAssocaiteCompany() {
		return assocaiteCompany;
	}
	/**
	 * Setter method for assocaiteCompany
	 *
	 * @param assocaiteCompany the assocaiteCompany to set
	 */
	public void setAssocaiteCompany(String assocaiteCompany) {
		this.assocaiteCompany = assocaiteCompany;
	}
	/**
	 * Getter method for policyId
	 * 
	 * @return the policyId (Long)
	 */
	public Long getPolicyId() {
		return policyId;
	}
	/**
	 * Setter method for policyId
	 *
	 * @param policyId the policyId to set
	 */
	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}
	/**
	 * Getter method for logoPath
	 * 
	 * @return the logoPath (String)
	 */
	public String getLogoPath() {
		return logoPath;
	}
	/**
	 * Setter method for logoPath
	 *
	 * @param logoPath the logoPath to set
	 */
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}
	/**
	 * Getter method for systemMsgColor
	 * 
	 * @return the systemMsgColor (String)
	 */
	public String getSystemMsgColor() {
		return systemMsgColor;
	}
	/**
	 * Setter method for systemMsgColor
	 *
	 * @param systemMsgColor the systemMsgColor to set
	 */
	public void setSystemMsgColor(String systemMsgColor) {
		this.systemMsgColor = systemMsgColor;
	}
	/**
	 * Getter method for companyMsgColor
	 * 
	 * @return the companyMsgColor (String)
	 */
	public String getCompanyMsgColor() {
		return companyMsgColor;
	}
	/**
	 * Setter method for companyMsgColor
	 *
	 * @param companyMsgColor the companyMsgColor to set
	 */
	public void setCompanyMsgColor(String companyMsgColor) {
		this.companyMsgColor = companyMsgColor;
	}
	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	/**
	 * Getter method for employeeCode
	 * 
	 * @return the employeeCode (String)
	 */
	public String getEmployeeCode() {
	    return employeeCode;
	}
	/**
	 * Setter method for employeeCode
	 *
	 * @param employeeCode the employeeCode to set
	 */
	public void setEmployeeCode(String employeeCode) {
	    this.employeeCode = employeeCode;
	}
	public String getDesgId() {
		return desgId;
	}
	public void setDesgId(String desgId) {
		this.desgId = desgId;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * Getter method for deptName
	 * 
	 * @return the deptName (String)
	 */
	public String getDeptName() {
		return deptName;
	}
	/**
	 * Setter method for deptName
	 *
	 * @param deptName the deptName to set
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	/**
	 * Getter method for desgName
	 * 
	 * @return the desgName (String)
	 */
	public String getDesgName() {
		return desgName;
	}
	/**
	 * Setter method for desgName
	 *
	 * @param desgName the desgName to set
	 */
	public void setDesgName(String desgName) {
		this.desgName = desgName;
	}
	public boolean isGuest() {
		return guest;
	}
	public void setGuest(boolean guest) {
		this.guest = guest;
	}
	

}
