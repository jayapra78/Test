package com.task.common;




import java.util.LinkedList;
import java.util.List;

import javax.transaction.SystemException;

/**
 * Class to store list of ValueObject.
 * 
 * @param <E
 *            extends ValueObject>
 */
public class ValueObjectList<E extends CommonVo> extends CommonVo {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** ValueObject List. */
	private List<E> list = new LinkedList<E>();

	/**
	 * Get current ValueObject list. 
	 * It clones the existing list and returns the cloned list.
	 * 
	 * @return List<E> clone of list contains ValueObjects
	 */
	public List<E> getCurrentList() {
		// clone
		return new LinkedList<E>(list);
	}
	
	/**
	 * Get ValueObject list.
	 * 
	 * @return List<E> List contains ValueObjects
	 */
	public List<E> getList() {
		return list;
	}

	/**
	 * Method to set new list of ValueObjects.
	 * 
	 * @param list
	 *            List
	 * @throws SystemException
	 *             If list is null ({@link com.hoyavc.nxg.core.CoreMessageCodeEnum#fjfe00004 fjfe00004})
	 */
	public void setNewList(final List<E> list) {

		// validating parameter
		if (list == null) {
//			String[] params = new String[] { List.class.getName(), null };
//			Message message = fjfe00004.createMessage(params);
//
//			// NXG Logger is not Serializable
//			NXGLogger logger = DefaultLogger.getInstance(this.getClass());
//			logger.error(message, new NullPointerException());
//			throw new SystemException(message, new NullPointerException());
		}

		this.list = new LinkedList<E>(list);
	}

	/**
	 * Adding a ValueObject to the existing list.
	 * 
	 * @param e
	 *            ValueObject of type<E>. (null is allowed)
	 * @return true (as per the general contract of Collection.add).
	 */
	public boolean add(final E e) {

		return list.add(e);
	}

}
