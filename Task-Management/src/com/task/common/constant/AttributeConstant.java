/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.common.constant;


public class AttributeConstant {
	/** Session Attribute String Representation  */
	
	/** attribute string for language */
	public static final String DEFAULTLOCALESESSION = "WW_TRANS_I18N_LOCALE";
	/** attribute string for secure id */
	public static final String SECURE_ACCESS_ID = "secureAccessId";
	
	/** attribute string for company search vo */
	public static final String COMPANY_SEARCH_VO = "Company_Search_Vo";
	/** attribute string for company recovery search vo */
	public static final String COMPANY_RECOVERY_VO = "Company_recovery_Vo";
	
	/** attribute string for mobile search vo */
	public static final String MOBILE_SEARCH_VO = "Mobile_Search_Vo";
	/** attribute string for mobile recovery search vo */
	public static final String MOBILE_RECOVERY_SEARCH_VO = "Mobile_Search_Recovery_Vo";
	
	/** attribute string for delivery place search vo */
	public static final String DELIVERYPLACE_SEARCH_VO = "deliveryPlaceSearchVo";
	/** attribute string for delivery place recovery search vo */
	public static final String DELIVERYPLACE_RECOVERY_SEARCH_VO = "deliveryPlaceRecoverySearchVo";
	
	/** attribute string for customer search vo */
	public static final String CUSTOMER_SEARCH_VO = "Customer_Search_Vo";
	/** attribute string for customer recovery search vo */
	public static final String CUSTOMER_RECOVERY_VO = "Customer_recovery_Vo";
	
	/** attribute string for policy definition search vo */
	public static final String POLICYDEFINITION_SEARCH_VO = "PolicyDefinition_Search_Vo";
	/** attribute string for policy definition search vo */
	public static final String POLICYDEFINITION_RECOVERY_VO = "PolicyDefinition_recovery_Vo";
	
	/** attribute string for photo search vo */
	public static final String PHOTO_SEARCH_VO = "photoCompanySearchVo";
	/** attribute string for photo recovery search vo */
	public static final String PHOTO_RECOVERY_SEARCH_VO = "photoRecoverySearchVo";
	
	/** attribute string for Sysadmin search vo */
	public static final String SYSTEMADMIN_SEARCH_VO = "systemAdminSearchVo";
	/** attribute string for Sysadmin recovery search vo */
	public static final String SYSTEMADMIN_RECOVERY_SEARCH_VO = "systemAdminRecoverySearchVo";
	
	/** attribute string for employee search vo */
	public static final String EMPLOYEE_SEARCH_VO = "employeeSearchVo";
	/** attribute string for employee recovery search vo */
	public static final String EMPLOYEE_RECOVERY_SEARCH_VO = "employeeRecoverySearchVo";
	
	/** attribute string for pickup search vo */
	public static final String PICKUP_COMPANY_SEARCH_VO = "pickupCompanySearchVo";
	/** attribute string for pickup recovery search vo */
	public static final String PICKUP_COMPANY_RECOVERY_SEARCH_VO = "pickupCompanyRecoverySearchVo";
	
	/** attribute string for information search vo */
	public static final String INFORMATION_SEARCH_VO = "informationSearchVo";
	/** attribute string for information recovery search vo */
	public static final String INFORMATION_RECOVERY_SEARCH_VO = "informationRecoverySearchVo";
	
	/** attribute string for tracking search vo */
	public static final String TRAKING_MANAGER_SEARCH_VO = "TrackingManager_Search_Vo";
	
	/** attribute string for job search vo */
	public static final String JOB_SEARCH_VO = "jobSearchVo";
	/** attribute string for job recovery search vo */
	public static final String JOB_RECOVERY_SEARCH_VO = "jobRecoverySearchVo";
	
	/** attribute string for job search vo */
	public static final String JOB_LIVE_STATUS_SEARCH_VO = "jobLiveStatusSearchVo";
	
	/** attribute string for Configuration search vo */
	public static final String CONFIGURATION_SEARCH_VO = "Configuration_Search_Vo";
	
	/** attribute string for vehicle search vo */
	public static final String VEHICLE_SEARCH_VO = "vehicleSearchVo";
	/** attribute string for vehicle recovery search vo */
	public static final String VEHICLE_RECOVERY_SEARCH_VO = "vehicleRecoverySearchVo";
	
	/** attribute string for usergroup search vo */
	public static final String USERGROUP_SEARCH_VO = "usergroupSearchVo";
	/** attribute string for usergroup recovery search vo */
	public static final String USERGROUP_RECOVERY_SEARCH_VO = "usergroupRecoverySearchVo";
	
	/** attribute string for screen search vo */
	public static final String SCREEN_SEARCH_VO = "screenSearchVo";
	/** attribute string for screen recovery search vo */
	public static final String SCREEN_RECOVERY_SEARCH_VO = "screenRecoverySearchVo";
	
	/** attribute string for policy search vo */
	public static final String POLICY_SEARCH_VO = "policySearchVo";
	
	/** attribute string for policy recovery search vo */
	public static final String POLICY_RECOVERY_SEARCH_VO = "policyRecoverySearchVo";
	
	/** attribute string for project Owner SearchVo  */
	public static final String PROJECT_OWNER_MAPPING_SEARCH_VO = "projectOwnerSearchVo";
	
	/** attribute string for project Owner SearchVo  */
	public static final String LEAVE_SEARCH_VO = "leaveSearchVo";
	
	/** attribute string for secure id */
	public static final String TMS = "plan2Lead";
}
