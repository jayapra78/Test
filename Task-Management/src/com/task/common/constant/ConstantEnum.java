/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.common.constant;

/**
 * Enum Class.
 * 
 * Constant For Module related Constant
 * 
 * @Author Rajnarayan Saraf 16/02/2012 
 * 
 */
public enum ConstantEnum {

	/* Screen Name */
	MENU("Menu"),
	LOGIN("Login"),
	SSL_HTTPS("https"),
	/* Module Name -  */
	VIEW("view"),
	AUTH("auth"),
	ADD("add"),
	MODIFY("modify"),
	SEARCH("search"),
	LIST("list"),
	SAVE("save"),
	UPDATE("update"),
	UPDATEAUTH("updateAuth"),
	DELETE("delete"),
	RECOVERYLIST("recoveryList"),
	RECOVERY("recovery"),
	RECOVERYSEARCH("searchRecovery"),
	PERMANENTDELETE("permanentDelete"),
	CSVIMPORT("csvImport"),
	CSVEXPORT("csvExport"),
	REGISTER("register"),
	ACCESSTRUE("1"),
	ACTIONCLASS("Action"),
	
	// Delete Flag
	DELETE_FLAG_OFF('0'),
	DELETE_FLAG_ON('1'),
	
	// Customer Mail detail for plan2lead
	STATUS_MAIL_OFF('0'),
	STATUS_MAIL_ON('1'),
	CONSOLIDATED_MAIL_OFF('0'),
	CONSOLIDATED_MAIL_ON('1'),
	
	// Driver Flag
	DRIVER_FLAG_OFF('0'),
	DRIVER_FLAG_ON('1'),
	
	// ComboBox Keys ( States What is the name of the Combobox)
	CORPORATION_TYPE("corporationtype"),
	TRADE_TYPE("tradetype"),
	USER_TYPE("usertype"),
	CONTENT_TYPE("content"),
	REPORT_TEMPLATE_TYPE("reporttemplate"),
	JOB_TYPE("jobtype"),
	WEIGHTINPUT_TYPE("weightinput"),
	STATUS_TYPE("jobstatus"),
	INFORMATION_TYPE("informationtype"),
	VEHICLE_CLASSIFICATION("vehicleclassificationtype"),
	SCREEN_TYPE("screentype"),
	CSV_TEMPLATE_TYPE("csvtemplateType"),
	MASTER_TYPE("masterType"),
	ACTION_TYPE("actionType"),
	MODULE_TYPE("moduleType"),
	
	// Sequence Name
	EMPLOYEE_SEQUENCE("employee_seq"),
	CONTENT_TYPE_SEQUENCE("content_type_seq"),
	SYSADMIN_SEQUENCE("sysadmin_seq"),
	VEHICLE_SEQUENCE("vehicle_seq"),
	CORPORATION_TYPE_SEQUENCE("corporation_type"),
	SCREEN_SEQUENCE("screen_seq"),
		
	// Languages
	ENGLISHCODE("en"),
	JAPANESECODE("ja"),
	
	USER_TYPE_1(1),
	USER_TYPE_2(2),
	USER_TYPE_3(3),
	USER_TYPE_4(4),
	USER_TYPE_5(5),
	
	ACTION_TYPE_SEARCH(1L),
	ACTION_TYPE_VIEW(2L),
	ACTION_TYPE_ADD(3L),
	ACTION_TYPE_MODIFY(4L),
	ACTION_TYPE_DELETE(5L),
	ACTION_TYPE_PERMANENT_DELETE(6L),
	ACTION_TYPE_RECOVERY(7L),
	ACTION_TYPE_LIST(8L),
	
	COMPANY(1L),
	PICKUP(2L),
	DELIVERY_PLACE(3L),
	POLICY_DEFINITION(4L),
	INFO_ALERT(5L),
	PARAMETER(6L),
	CUSTOMER(7L),
	INFORMATION_LANDING(8L),
	POLICY(9L),
	USER_GROUP(10L),
	PASSWORD(11L),
	PHOTO(12L),
	EMPLOYEE(13L),
	VEHICLE(14L),
	MOBILE(15L),
	EXPORT(16L),
	INFORMATION(17L),
	IMPORT(18L),
	TRACK(19L),
	JOB(20L),
	JOBSTATUSLIVE(21L),
	CONFIGURATION(22L),
	COMPANY_CONFIGURATION(25L),
	ASKULJOBIMPORT(28L),
	SYS_ADMIN(29L),
	
	SCREEN_COMPANY("Company"),
	SCREEN_PICKUP("Pickup"),
	SCREEN_DELIVERY_PLACE("DeliveryPlace"),
	SCREEN_POLICY_DEFINITION("PolicyDefinition"),
	SCREEN_PARAMETER("Parameter"),
	SCREEN_CUSTOMER("Customer"),
	SCREEN_POLICY("Policy"),
	SCREEN_USER_GROUP("UserGroup"),
	SCREEN_PASSWORD("Password"),
	SCREEN_PHOTO("Photo"),
	SCREEN_EMPLOYEE("Employee"),
	SCREEN_VEHICLE("Vehicle"),
	SCREEN_MOBILE("Mobile"),
	SCREEN_EXPORT("Export"),
	SCREEN_INFORMATION("Information"),
	SCREEN_IMPORT("Import"),
	SCREEN_TRACK("Track"),
	SCREEN_CONFIGURATION("Configuration"),
	SCREEN_SYS_ADMIN("SysAdmin"),
	SCREEN_COMPANY_CONFIGURATION("CompanyConfiguration"),
	SCREEN_JOB("Job"),
	SCREEN_JOBSTATUSLIVE("JobStatusLive"),
	SCREEN_ASKULJOBIMPORT("AskulJobImport"),
	
	// Job Status
	JOB_STATUS_1(1),
	JOB_STATUS_2(2),
	JOB_STATUS_3(3),
	JOB_STATUS_4(4),
	
	CONTENT_TYPE_1(1),
	CONTENT_TYPE_2(2),
	CONTENT_TYPE_3(3),
	
	REPORT_TEMPLATE_1(1),
	REPORT_TEMPLATE_2(2),
	REPORT_TEMPLATE_3(3),

	// Flag on / off
	FLAG_ON('1'),
	FLAG_OFF('0'),
	
	C_COMMA(","),
    C_EMPTY(""),
    C_SPACE(" "),
    C_DOUBLE_QUOTE("\""),
    C_GPS_INTERVAL_FIVE(5),
    C_MINUS_ONE(-1L),
    
    // Type of Work
    TYPE_OF_WORK_DELIVERY(1),
    TYPE_OF_WORK_COLLECTION(0),
    
    //propertyName
  	DELETE_FLAG("deleteFlag"),
    
	// trade_type
	TRADE_TYPE_SUFFIX(1), 
	TRADE_TYPE_PREFIX(2), 
	JOB_ENTITY_LIST_INDEX(2),
	JOB_STATUS_ENTITY_LIST_INDEX(3),
	CSV_MINUS_ONE(-1),
	TRACKING_ENTITY_LIST_INDEX(1),
	CUSTOMER_ID(0L), 

	NO_RECORD_FOUND("NO_RECORD_FOUND"),
	LOGIN_LOGO("loginLogo"),
	COMPANY_LOGO("companyLogo"),
	COMPANY_MSG_COLOR("companyMsgColor"),
	SYSTEM_MSG_COLOR("systemMsgColor"),
    RESULT_TYPE("restype"),
	
	// Configuarion
	CONF_COMPANY(1),
	CONF_SCREEN(2),
	CONF_SYSTEM(3),
	
	// Flag as String
	FLAG_1("1"),
	FLAG_0("0"),
	JPEG_EXTENSION(".jpeg"),
	JPG_EXTENSION(".jpg"),
	SYSTEM_LOGO_NAME("SystemLogo"),
	
	SCREEN_TYPE_COMPANY(1),
	SCREEN_TYPE_POLICY(2),
	SCREEN_TYPE_SYSTEM(3),
	FORWARD_SLASH("/"),
	FILE_EXTENSION_JPG("jpg"),
	FILE_EXTENSION_JPEG("jpeg"),
	FILE_PNG_EXTENSION("png"),
	PNG_EXTENSION(".png"),
	DOT("."),
	UNDERSCORE("_"),
	PLUS("+"),
	MINUS("-"),
	MINUS_ONE("-1"),
	;
	
	
	
	/** DataType value. */
	private String value;
	private Character charValue;
	private Integer charIntValue;
	private Long charLongValue;
	
	/**
	 * Constructor.
	 * 
	 * @param value
	 *            Value of the Constant.
	 */
	private ConstantEnum(String value) {
		this.value = value;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param value
	 *            Value of the Constant.
	 */
	private ConstantEnum(Character charValue) {
		this.charValue = charValue;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param value
	 *            Value of the Constant.
	 */
	private ConstantEnum(Integer charIntValue) {
		this.charIntValue = charIntValue;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param value
	 *            Value of the Constant.
	 */
	private ConstantEnum(Long charLongValue) {
		this.charLongValue = charLongValue;
	}
	
	
	/**
	 * Getter method for Constant String value.
	 * 
	 * @return Value of the Constant.
	 */
	public String getStringValue() {
		return this.value.toString();
	}
	
	/**
	 * Getter method for Constant Character value.
	 * 
	 * @return Value of the Constant.
	 */
	public Character getCharValue() {
		return this.charValue;
	}
	
	/**
	 * Getter method for Constant Integer value.
	 * 
	 * @return Value of the Constant.
	 */
	public Integer getIntValue() {
		return this.charIntValue;
	}
	
	/**
	 * Getter method for Constant Long value.
	 * 
	 * @return Value of the Constant.
	 */
	public Long getLongValue() {
		return this.charLongValue;
	}
	
	
}
