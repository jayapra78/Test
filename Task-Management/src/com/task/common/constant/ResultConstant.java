/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.common.constant;

public class ResultConstant {
	/** result string for login */
	public static final String RESULT_LOGIN_US = "login_us";
	public static final String RESULT_LOGIN_JP = "login_jp";
	
	public static final String RESULT_LOGIN_ERRORS = "loginErrors";
	/** result string for valid user */
	public static final String RESULT_VALID_LOGIN = "validlogin";
	public static final String RESULT_ERROR = "errorexception";
	public static final String RESULT_INPUT ="input";
	public static final String RESULT_INPUT_MODIFY ="inputModify";
	public static final String RESULT_GOLOGIN = "goLogin";
	public static final String RESULT_NOTAUTHORIZED = "notAuthorized";

	/** landing page */
	public static final String RESULT_INFORMATION_LANDING_LIST = "listInformationLanding";

	/** Company */
	public static final String RESULT_COMPANY_LISTURL = "listUrlCompany";
	public static final String RESULT_COMPANY_LIST = "listCompany";
	public static final String RESULT_COMPANY_ADD = "addCompany";
	public static final String RESULT_COMPANY_MODIFY = "modifyCompany";
	public static final String RESULT_COMPANY_VIEW = "viewCompany";
	public static final String RESULT_COMPANY_RECOVERY = "recoveryCompany";

	/** Pickup Company */
	public static final String PICKUP_LIST = "listPickup";
	public static final String PICKUP_ADD_LIST = "addPickup";
	public static final String PICKUP_SHOW = "showPickup";
	public static final String PICKUP_UPDATE = "updatePickup";
	public static final String PICKUP_VIEW = "viewPickup";
	public static final String PICKUP_EDIT = "editPickup";
	public static final String PICKUP_RECOVERY = "recoveryPickup";
	public static final String PICKUP_MODIFY = "modifyPickup";
	public static final String PICKUP_MODIFY_ERROR = "modifyError";
	public static final String PICKUP_MODIFY_INPUT = "modifyInput";
	public static final String PICKUP_ADD_ERROR = "addError";
	public static final String PICKUP_ADD_INPUT = "input";

	/** Information Master */
	public static final String INFORMATION_LIST = "listInformation";
	public static final String INFORMATION_ADD_LIST = "addInformation";
	public static final String INFORMATION_SHOW = "showInformation";
	public static final String INFORMATION_UPDATE = "updateInformation";
	public static final String INFORMATION_VIEW = "viewInformation";
	public static final String INFORMATION_EDIT = "editInformation";
	public static final String INFORMATION_RECOVERY = "recoveryInformation";
	public static final String INFORMATION_MODIFY_ERROR = "modifyError";
	public static final String INFORMATION_MODIFY_INPUT = "modifyInput";
	public static final String INFORMATION_ADD_ERROR = "addError";
	public static final String INFORMATION_ADD_INPUT = "addInput";

	/** Delivery Place */
	public static final String RESULT_DELIVERYPLACE_LIST = "listDeliveryPlace";
	public static final String RESULT_DELIVERYPLACE_ADD = "addDeliveryPlace";
	public static final String RESULT_DELIVERYPLACE_MODIFY = "modifyDeliveryPlace";
	public static final String RESULT_DELIVERYPLACE_VIEW = "viewDeliveryPlace";
	public static final String RESULT_DELIVERYPLACE_RECOVERY = "recoveryDeliveryPlace";
	public static final String RESULT_DELIVERYPLACE_RECOVERY_LIST = "recoveryListDeliveryPlace";

	/** Photo */
	public static final String RESULT_PHOTO_LIST = "listPhoto";
	public static final String RESULT_PHOTO_ADD = "addPhoto";
	public static final String RESULT_PHOTO_MODIFY = "modifyPhoto";
	public static final String RESULT_PHOTO_VIEW = "viewPhoto";
	public static final String RESULT_PHOTO_RECOVERY = "recoveryPhoto";
	public static final String RESULT_PHOTO_MODIFY_INPUT = "modifyInput";

	/** Policy */
	public static final String RESULT_POLICY_LIST = "listPolicy";
	public static final String RESULT_POLICY_SEARCH = "searchPolicy";
	public static final String RESULT_POLICY_ADD = "addPolicy";
	public static final String RESULT_POLICY_SAVE = "savePolicy";
	public static final String RESULT_POLICY_MODIFY = "modifyPolicy";
	public static final String RESULT_POLICY_UPDATE = "updatePolicy";
	public static final String RESULT_POLICY_VIEW = "viewPolicy";
	public static final String RESULT_POLICY_RECOVERY = "recoveryPolicy";
	public static final String RESULT_POLICY_VIEW_AUTH = "viewAuthentication";
	public static final String RESULT_POLICY_UPDATE_AUTH = "updateAuthentication";

	/** Policy Definition */
	public static final String RESULT_POLICYDEFINITION_LIST = "listPolicyDefinition";
	public static final String RESULT_POLICYDEFINITION_SEARCH = "searchPolicyDefinition";
	public static final String RESULT_POLICYDEFINITION_ADD = "addPolicyDefinition";
	public static final String RESULT_POLICYDEFINITION_SAVE = "savePolicyDefinition";
	public static final String RESULT_POLICYDEFINITION_MODIFY = "modifyPolicyDefinition";
	public static final String RESULT_POLICYDEFINITION_UPDATE = "updatePolicyDefinition";
	public static final String RESULT_POLICYDEFINITION_VIEW = "viewPolicyDefinition";
	public static final String RESULT_POLICYDEFINITION_RECOVERY = "recoveryPolicyDefinition";

	/** User Group */
	public static final String RESULT_USERGROUP_LIST = "listUserGroup";
	public static final String RESULT_USERGROUP_SEARCH = "searchUserGroup";
	public static final String RESULT_USERGROUP_ADD = "addUserGroup";
	public static final String RESULT_USERGROUP_SAVE = "saveUserGroup";
	public static final String RESULT_USERGROUP_MODIFY = "modifyUserGroup";
	public static final String RESULT_USERGROUP_UPDATE = "updateUserGroup";
	public static final String RESULT_USERGROUP_VIEW = "viewUserGroup";
	public static final String RESULT_USERGROUP_RECOVERY = "recoveryUserGroup";
	public static final String RESULT_USERGROUP_VIEW_AUTH = "viewAuthentication";
	public static final String RESULT_USERGROUP_UPDATE_AUTH = "updateAuthentication";
	
	/** Mobile */
	public static final String RESULT_MOBILE_LIST = "listMobile";
	public static final String RESULT_MOBILE_ADD = "addMobile";
	public static final String RESULT_MOBILE_MODIFY = "modifyMobile";
	public static final String RESULT_MOBILE_VIEW = "viewMobile";
	public static final String RESULT_MOBILE_RECOVERY = "recoveryListMobile";
	
	/** Parameter Master */
	public static final String RESULT_VIEW_PARAMETER = "viewParameter";
	public static final String RESULT_UPDATE_PARAMETER = "updateParameter";
	
	/** Password update */
	public static final String RESULT_PASSWORD_UPDATE = "updatePassword";
	
	/** Customer */
	public static final String RESULT_CUSTOMER_LIST = "listCustomer";
	public static final String RESULT_CUSTOMER_ADD = "addCustomer";
	public static final String RESULT_CUSTOMER_MODIFY = "modifyCustomer";
	public static final String RESULT_CUSTOMER_VIEW = "viewCustomer";
	public static final String RESULT_CUSTOMER_RECOVERY = "recoveryCustomer";
	
	/** Employee */
	public static final String RESULT_EMPLOYEE_LIST = "listEmployee";
	public static final String RESULT_EMPLOYEE_ADD = "addEmployee";
	public static final String RESULT_EMPLOYEE_MODIFY = "modifyEmployee";
	public static final String RESULT_EMPLOYEE_VIEW = "viewEmployee";
	public static final String RESULT_EMPLOYEE_RECOVERY = "recoveryEmployee";
	
	/** Vehicle Master */
	public static final String RESULT_VEHICLE_LIST = "listVehicle";
	public static final String RESULT_VEHICLE_ADD = "addVehicle";
	public static final String RESULT_VEHICLE_EDIT = "editVehicle";
	public static final String RESULT_VEHICLE_MODIFY = "modifyVehicle";
	public static final String RESULT_VEHICLE_VIEW = "viewVehicle";
	public static final String RESULT_VEHICLE_RECOVERY = "recoveryVehicle";
	public static final String RESULT_VEHICLE_RECOVERY_LIST = "recoveryListVehicle";
	
	/**System Admin */
	public static final String RESULT_SYSTEMADMIN_LIST = "listSystemAdmin";
	public static final String RESULT_SYSTEMADMIN_ADD = "addSystemAdmin";
	public static final String RESULT_SYSTEMADMIN_MODIFY = "modifySystemAdmin";
	public static final String RESULT_SYSTEMADMIN_VIEW = "viewSystemAdmin";
	public static final String RESULT_SYSTEMADMIN_RECOVERY = "recoverySystemAdmin";
	
	/** Job  */
	public static final String RESULT_JOB_ADD = "addJob";
	public static final String RESULT_JOB_SAVE = "saveJob";
	public static final String RESULT_JOB_LIST = "listJob";
	public static final String RESULT_JOB_MODIFY = "modifyJob";
	public static final String RESULT_JOB_UPDATE = "updateJob";
	public static final String RESULT_JOB_VIEW = "viewJob";
	public static final String RESULT_JOB_RECOVERYLIST = "recoveryListJob";
	public static final String RESULT_JOB_WEIGHT_REPORT = "reportWeightJob";
	public static final String RESULT_JOB_REPORT = "reportJob";
	public static final String RESULT_JOB_STATIC_ONE = "jobTemplateStaticOne";
	public static final String RESULT_JOB_STATIC_TWO = "jobTemplateStaticTwo";
	public static final String RESULT_JOB_STATIC_THREE = "jobTemplateStaticThree";
	public static final String RESULT_JOB_STATIC_FOUR = "jobTemplateStaticFour";
	public static final String RESULT_JOB_STATIC_FIVE = "jobTemplateStaticFive";
	public static final String RESULT_JOB_STATIC_SIX = "jobTemplateStaticSix";
	public static final String RESULT_JOB_STATIC_SEVEN = "jobTemplateStaticSeven";
	public static final String RESULT_JOB_STATIC_EIGHT = "jobTemplateStaticEight";
	public static final String RESULT_JOB_STATIC_NINE = "jobTemplateStaticNine";
	public static final String RESULT_JOB_STATIC_TEN = "jobTemplateStaticTen";
	public static final String RESULT_JOB_STATIC_ELEVEN = "jobTemplateStaticEleven";
	public static final String RESULT_JOB_STATIC_TWELVE = "jobTemplateStaticTwelve";
	
	/** Job Status Live */
	public static final String RESULT_JOB_STATUS_LIVE_LIST = "listJobStatusLive";
	public static final String RESULT_JOB_STATUS_LIVE_VIEW = "viewJobStatusLive";
	public static final String RESULT_JOB_STATUS_LIVE_VIEW_DRIVER = "viewDriverJobStatusLive";
	public static final String RESULT_JOB_STATUS_LIVE_SEND_MAIL = "sendMailJobStatusLive";
	public static final String RESULT_JOB_STATUS_LIVE_DRIVERLIST_MAP = "driverListMapJobStatusLive";
	public static final String RESULT_JOB_STATUS_LIVE_DRIVER_MAP = "driverMapJobStatusLive";
	public static final String RESULT_JOB_STATUS_REPORT = "jobStatusReport";
	public static final String RESULT_JOB_STATUS_REPORT_CUSTOMER = "jobStatusReportCustomer";
	public static final String RESULT_JOB_STATUS_NOT_SEND_MAIL = "notSendMailJobStatusLive";
	
	/**Csv Export */
	public static final String EXPORT_CSV = "csvExport";
	
	public static final String ADMIN_ERROR = "Please contact the Administator";
	
	/** Import Csv */
	public static final String RESULT_IMPORT_CSV_LIST = "CsvImport";
	public static final String CompanyEntityVo = "setCompanyEntityVo";
	public static final String CustomerEntityVo = "setCustomerEntityVo";
	public static final String UserGroupEntityVo = "setUserGroupEntityVo";
	public static final String MobileEntityVo = "setMobileEntityVo";
	public static final String LoginEntityVo = "setLoginEntityVo";
	public static final String ComCompanyEntityVo = "com.shirai.vo.custom.entity.CompanyEntityVo";
	public static final String ComCustomerEntityVo = "com.shirai.vo.custom.entity.CustomerEntityVo";
	public static final String ComUserGroupEntityVo = "com.shirai.vo.custom.entity.UserGroupEntityVo";
	public static final String ComMobileEntityVo = "com.shirai.vo.custom.entity.MobileEntityVo";
	public static final String ComLoginEntityVo = "com.shirai.vo.custom.entity.LoginEntityVo";
	
	/**Askul Job Import */
	public static final String SHOW_ASKUL_IMPORT = "showAskulImport";
	public static final String ASKUL_JOB_LIST = "askulJobList";
	
	/**MIS Report */
	public static final String SEARCH_MIS_REPORT = "searchMISReport";
	public static final String CLIENT_SUMMARY_REPORT = "clientSummary";
	public static final String MONTH_SUMMARY_REPORT = "monthSummary";
	public static final String CLIENT_MONTH_REPORT = "clientMonthSummary";
	
	/** Associated Company */
	public static final String CHANGE_COMPANY = "changeCompany";
	
	/** Tracking Manger */
	public static final String TRACKINGMANAGER_LIST ="listTrackingManager";

	public static final String RESULT_PROJECT_OWNER_LIST = "listProjectOwnerMapping";
	
	public static final String RESULT_LEAVE_LIST = "listLeave";
}
