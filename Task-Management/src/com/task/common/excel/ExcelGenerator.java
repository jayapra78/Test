/**
 * Creater / sengottaiyansSRM copyright information to Shirai 2013
 * 
 */
package com.task.common.excel;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import jxl.CellView;
import jxl.SheetSettings;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.log4j.Logger;

import com.task.common.constant.ConstantEnum;
import com.task.util.CommonConstants;
import com.task.util.CommonUtils;
import com.task.util.LoggingAspectInterceptor;

/**
 * @author sengottaiyans
 *
 */
public class ExcelGenerator {

	private static Logger log = LoggingAspectInterceptor.getLog(ExcelGenerator.class);
	
	/**
     * 
     * @Method : generateCsvJobTemplate
     * @Description : This Method is used to generate csv JobTemplate.
     * @param companyResponseVo
     * @param objectList
     * @return
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")

    public void generateExcelJobTemplate(String downloadFileName,Map<String,List<String>> headerMap,
    		List dataList,HttpServletResponse response,String heading) throws Exception {
    	
    	int col = 1;
    	int row = 0;

    	File file = new File(templateFileName(downloadFileName));

    	WorkbookSettings workbookSettings = new WorkbookSettings();
    	workbookSettings.setEncoding(CommonConstants.FILE_ENCODING);
    	WritableWorkbook workbook = Workbook.createWorkbook(file,workbookSettings);
		workbook.createSheet("Report", 0);
		WritableSheet sheet = workbook.getSheet(0);
		CellView cv = new CellView();
		sheet.setColumnView(0, cv);
		
		SheetSettings settings = sheet.getSettings();
		settings.setVerticalFreeze(6);
		settings.setHorizontalFreeze(7);
		//settings.setPrintGridLines(false);
		//settings.setProtected(false);
		row = header(sheet,row,headerMap,heading);
		//****** Data *********
		int sNo = 1;
		if (null != dataList) {
			
		    Iterator it = dataList.iterator();
		    while (it.hasNext()) {
		    	Object[] value = (Object[]) it.next();
				if (value != null) {
				    col = 1;
				    setExcelCellData(sheet, col++, row, sNo++);
				    for (Object object : value) {
				    	setExcelCellData(sheet, col++, row, object);
				    }
				}
				row++;
		    }
		}
		workbook.write();
		workbook.close();
		
		ServletOutputStream output = response.getOutputStream();
		response.setContentType("application/binary; charset=UTF-8");
		response.setCharacterEncoding(CommonConstants.FILE_ENCODING);
		response.setHeader("Content-Disposition",("attachment;filename=" + downloadFileName + CommonConstants.FILE_EXT_EXCEL));
	    
		FileInputStream in = new FileInputStream(file);
		int fileSize = in.available();
		byte[] outputByte = new byte[fileSize];
		while (in.read(outputByte, 0, fileSize) != -1) {
		    output.write(outputByte, 0, fileSize);
		}
		in.close();
	    output.flush();
	    output.close();
	    log.debug("METHOD_LOG_END : EXPORT_CSV_WRITE_METHOD");
	    file.delete();
    }
    public int header(WritableSheet sheet, int row, Map<String,List<String>> headerMap,String heading) 
    		throws RowsExceededException, WriteException{
    	
    	sheet.setColumnView(0, 2);
    	sheet.setColumnView(1, 8);
    	sheet.setColumnView(2, 27);
    	sheet.setColumnView(3, 13);
    	sheet.setColumnView(4, 10);
    	sheet.setColumnView(5, 10);
    	sheet.setColumnView(6, 22);
    	
    	String logo = CommonUtils.getImageDirectory()+File.separator+"srmlogo.png";
    	File file = new File(logo);
    	WritableImage wrImg = new WritableImage(0, 1, 3, 2, file);
    	sheet.addImage(wrImg);
    	
    	// Create cell font and format
        WritableFont cellFont = new WritableFont(WritableFont.ARIAL, 10,WritableFont.BOLD);
        cellFont.setColour(Colour.BLACK);
    	
        //****** Title *********
    	sheet.mergeCells(3, 1, 12, 2);
    	WritableCellFormat titleCellFormat = new WritableCellFormat(cellFont);
    	titleCellFormat.setBackground(Colour.ROSE);
    	titleCellFormat.setAlignment(Alignment.CENTRE);
    	titleCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
    	titleCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
    	Label titleLable = new Label(3, 1, heading, titleCellFormat);
    	sheet.addCell(titleLable);
    	
    	//****** Header 1 *********
    	//Work Days
    	sheet.mergeCells(4, 4, 5, 4);
    	WritableCellFormat h1WorkDaysCellFormat = new WritableCellFormat(cellFont);
    	h1WorkDaysCellFormat.setBackground(Colour.ICE_BLUE);
    	h1WorkDaysCellFormat.setAlignment(Alignment.CENTRE);
    	h1WorkDaysCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
    	h1WorkDaysCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
    	Label workdaysLable = new Label(4, 4, "Workdays", h1WorkDaysCellFormat);
    	sheet.addCell(workdaysLable);
    	
    	//Effort Spent
    	sheet.mergeCells(6, 4, 6, 5);
    	WritableCellFormat h1EffortSpentCellFormat = new WritableCellFormat(cellFont);
    	h1EffortSpentCellFormat.setBackground(Colour.ICE_BLUE);
    	h1EffortSpentCellFormat.setAlignment(Alignment.CENTRE);
    	h1EffortSpentCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
    	h1EffortSpentCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
    	Label effortSpentLable = new Label(6, 4, "Total Effort Spent", h1EffortSpentCellFormat);
    	sheet.addCell(effortSpentLable);
    	
    	//Project, Practice Team, Others
    	int col = 7;
    	if (null != headerMap) {
    		Colour h1Colors[] = {Colour.LIGHT_GREEN , Colour.CORAL, Colour.YELLOW};
    		int colorIndex = 0;
			Set<String> keys = headerMap.keySet();
			for(String headerKey : keys){
				List<String> projectNames = headerMap.get(headerKey);
		    	sheet.mergeCells(col, 4, col+projectNames.size()-1, 4);
		    	WritableCellFormat h1ProjectTypeCellFormat = new WritableCellFormat(cellFont);
		    	h1ProjectTypeCellFormat.setBackground(h1Colors[colorIndex++]);
		    	h1ProjectTypeCellFormat.setAlignment(Alignment.CENTRE);
		    	h1ProjectTypeCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		    	h1ProjectTypeCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		    	Label projectTypeLable = new Label(col, 4, headerKey, h1ProjectTypeCellFormat);
		    	sheet.addCell(projectTypeLable);
		    	col = col + projectNames.size();
			}
		}
    	
    	//****** Header 2 *********
    	//SNO,Employee Name,Emp ID,Total,Actual,Proj Name1..,Proj Name2..,Others
    	setHeader(sheet,1,5,"S.NO");
    	setHeader(sheet,2,5,"Employee Name");
    	setHeader(sheet,3,5,"Emp ID");
    	setHeader(sheet,4,5,"Total");
    	setHeader(sheet,5,5,"Actual");
    	col = 7;
    	if (null != headerMap) {
			Set<String> keys = headerMap.keySet();
			for(String headerKey : keys){
				for (String headerValue : headerMap.get(headerKey)) {
					int len = 10;
					if(headerValue!=null){
						len= headerValue.length()+headerValue.length()*30/100;
					}
					sheet.setColumnView(col, len);
					setHeader(sheet, col++, 5, headerValue);
			    }				
			}
		}
    	return 6;
    }
    private void setHeader(WritableSheet sheet, int column, int row, Object data) 
    		throws RowsExceededException, WriteException{
    	
    	WritableFont cellFont = new WritableFont(WritableFont.ARIAL, 10,WritableFont.BOLD);
    	cellFont.setColour(Colour.BLACK);
    	
		WritableCellFormat cellFormat = new WritableCellFormat(cellFont);
		cellFormat.setBackground(Colour.ICE_BLUE);
		cellFormat.setAlignment(Alignment.CENTRE);
		cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		
		Label label;
		if (null != data) {
			label = new Label(column, row, data.toString(), cellFormat);
		} else {
			label = new Label(column, row, ConstantEnum.C_EMPTY.getStringValue(), cellFormat);
		}
		sheet.addCell(label);
    }
    /**
     * @Method : setExcelCellData
     * @Description : To set the Excel Cell date in normal Style
     * @param sheet
     * @param column
     * @param row
     * @param data
     * @throws RowsExceededException
     * @throws WriteException
     */
    private void setExcelCellData(WritableSheet sheet, int column, int row,
	    Object data) throws RowsExceededException, WriteException {

    	WritableFont cellFont = new WritableFont(WritableFont.ARIAL, 10,WritableFont.BOLD);
    	cellFont.setColour(Colour.BLACK);
    	WritableCellFormat cellFormat = new WritableCellFormat(cellFont);
    	cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
    	
    	WritableCell cell;
    	if(null == data){
    		cell = new Label(column, row, ConstantEnum.C_EMPTY.getStringValue(),cellFormat);
    	}
    	else if(data instanceof Double){
    		cell = new jxl.write.Number(column, row, (Double)data,cellFormat);
    	}
    	else if(data instanceof Integer){
    		cell = new jxl.write.Number(column, row, (Integer)data,cellFormat); 
    	}
    	else if(data instanceof Date){
    		cell = new jxl.write.DateTime(column, row, (Date)data,cellFormat); 
    	}
    	else{
		    cell = new Label(column, row, data.toString(),cellFormat);
		}
    	
		sheet.addCell(cell);
    }
    
    /**
     * @Method : templateFileName
     * @Description : To generate the template File Name
     * @param templateName
     * @return
     */
    private String templateFileName(String downloadFileName) {
    	SimpleDateFormat sDtFrmt = new SimpleDateFormat("yyMMddHHmmss");
    	return  CommonUtils.getExcelReportDirectory() + File.separator 
    			+ sDtFrmt.format(new Date()) + downloadFileName + CommonConstants.FILE_EXT_EXCEL;
    }
}
