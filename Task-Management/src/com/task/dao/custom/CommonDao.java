/**
 * Copyright 2012 by Shirai, All rights reserved.
 *
 * 2012/06/20 <Base Line Version 1.0>
 */
package com.task.dao.custom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.task.common.CommonDaoUtilsVo;
import com.task.common.CommonVo;
import com.task.common.ResponseVo;
import com.task.common.ValueObjectList;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
/**
 * This is an Common Base Dao for all dao's
 *
 * @author Neethimohan A [SRM]
 * @author Raj Narayan Saraf [SRM]
 */
public class CommonDao {

	/**
	 * @Method :  populateCriteria
	 *
	 * @Description : Method to Create DetachedCriteria
	 *
	 * @param className className
	 *
	 * @return CommonDaoUtilsVo
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings("rawtypes")
	public CommonDaoUtilsVo populateCriteria(Class className) throws CustomApplicationException{
		try{
		if (className != null) {
			DetachedCriteria criteria = DetachedCriteria.forClass(className);

			CommonDaoUtilsVo  commonDaoUtilsVo = new CommonDaoUtilsVo();
			commonDaoUtilsVo.setCriteria(criteria);
			return commonDaoUtilsVo;
		}
	}catch(DataIntegrityViolationException e){
		throw new CustomApplicationException("populateCriteria",  e);
	}catch(CannotAcquireLockException e){
		throw new CustomApplicationException("Some one in transaction please try after some time", e);
	}catch (HibernateException e) {
		throw new CustomApplicationException("populateCriteria",  e);
	}catch (Exception e) {
		throw new CustomApplicationException("populateCriteria",  e);
	}
		return null;
	}

	/**
	 * @Method :  populateCriteria
	 *
	 * @Description : Method to Create DetachedCriteria
	 *
	 * @param className className
	 *
	 * @return CommonDaoUtilsVo
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings("rawtypes")
	public CommonDaoUtilsVo populateCriteria(Class className, String alias) throws CustomApplicationException{
		try{
		if (className != null) {
			DetachedCriteria criteria = DetachedCriteria.forClass(className,alias);

			CommonDaoUtilsVo  commonDaoUtilsVo = new CommonDaoUtilsVo();
			commonDaoUtilsVo.setCriteria(criteria);
			return commonDaoUtilsVo;
		}
	}catch(DataIntegrityViolationException e){
		throw new CustomApplicationException("populateCriteria",  e);
	}catch(CannotAcquireLockException e){
		throw new CustomApplicationException("Some one in transaction please try after some time", e);
	}catch (HibernateException e) {
		throw new CustomApplicationException("populateCriteria",  e);
	}catch (Exception e) {
		throw new CustomApplicationException("populateCriteria",  e);
	}
		return null;
	}

	/**
	 * @Method : delete
	 *
	 * @Description : Method to Delete the entity
	 *
	 * @param queryString String
	 *
	 * @throws CustomApplicationException
	 */
	protected void delete(final String queryString) throws CustomApplicationException {
		HibernateTemplate hibernateTemplate = null;
		try {
		hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
			hibernateTemplate.bulkUpdate(queryString);

		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method : delete
	 *
	 * @Description : Method to Delete the entity
	 *
	 * @param queryString String
	 *
	 * @throws CustomApplicationException
	 */
	protected HibernateTemplate deleteNestedTransaction(final String queryString) throws CustomApplicationException {
		HibernateTemplate hibernateTemplate = null;
		try {
			hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
			hibernateTemplate.clear();
			hibernateTemplate.bulkUpdate(queryString);

		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
		return hibernateTemplate;
	}

	/**
	 * @Method :  saveNestedTransaction
	 * @Description :  Method to Insert entity
	 * @param inVo CommonVo
	 * @throws CustomApplicationException
	 */
	protected void deleteNestedTransaction(final String queryString,
			HibernateTemplate hibernateTemplate) throws CustomApplicationException {
		try {
			if (hibernateTemplate != null)
				hibernateTemplate.bulkUpdate(queryString);

		} catch (DataIntegrityViolationException e) {
			throw new CustomApplicationException("populateCriteria", e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		} catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria", e);
		} catch (Exception e) {
			throw new CustomApplicationException("populateCriteria", e);
		}

	}
	/**
	 * @Method : recovery
	 *
	 * @Description : Method to Recovery the entity
	 *
	 * @param queryString String
	 *
	 * @throws CustomApplicationException
	 */
	protected void recovery(final String queryString) throws CustomApplicationException {
		HibernateTemplate hibernateTemplate =  null;
		try {
		 hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
			hibernateTemplate.bulkUpdate(queryString);
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method : recoveryNestedTransaction
	 * @Description : Method to recovery list of entity
	 * @param queryString String
	 * @throws CustomApplicationException
	 */
	protected HibernateTemplate recoveryNestedTransaction(final String queryString) throws CustomApplicationException {
		HibernateTemplate hibernateTemplate = null;
		try {
			hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
			hibernateTemplate.clear();
			hibernateTemplate.bulkUpdate(queryString);

		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
		return hibernateTemplate;
	}

	/**
	 * @Method :  recoveryNestedTransaction
	 * @Description :  Method to recovery list of entity
	 * @param inVo CommonVo
	 * @throws CustomApplicationException
	 */
	protected void recoveryNestedTransaction(final String queryString,
			HibernateTemplate hibernateTemplate) throws CustomApplicationException {
		try {
			if (hibernateTemplate != null)
				hibernateTemplate.bulkUpdate(queryString);

		} catch (DataIntegrityViolationException e) {
			throw new CustomApplicationException("populateCriteria", e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		} catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria", e);
		} catch (Exception e) {
			throw new CustomApplicationException("populateCriteria", e);
		}

	}
	/**
	 * @Method : deleteAll
	 *
	 * @Description : Method to Delete list of entity
	 *
	 * @param voList ValueObjectList;&ltCommonVo;&gt
	 *
	 * @throws CustomApplicationException
	 */
	protected void deleteAll(final ValueObjectList<CommonVo> voList)
			throws CustomApplicationException{
		HibernateTemplate hibernateTemplate= null;
		try{
		hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
			hibernateTemplate.saveOrUpdateAll(voList.getList());
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method : findValue
	 *
	 * @Description : find all without pagination without criteria
	 *
	 * @param exampleEntity CommonVo
	 *
	 * @param valueId  long
	 *
	 * @return CommonVo
	 *
	 * @throws CustomApplicationException
	 */
	protected CommonVo findValue(CommonVo exampleEntity, long valueId) throws CustomApplicationException{
		HibernateTemplate hibernateTemplate = null;
		try{
		hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		CommonVo valueObject = hibernateTemplate.get(exampleEntity.getClass(),
				valueId);

		return valueObject;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method : findValue
	 *
	 * @Description : find all without pagination without criteria
	 *
	 * @param exampleEntity CommonVo
	 *
	 * @param valueId String
	 *
	 * @return CommonVo
	 *
	 * @throws CustomApplicationException
	 */
	protected CommonVo findValue(CommonVo exampleEntity, String valueId) throws CustomApplicationException{
		HibernateTemplate hibernateTemplate = null;
		try{
		 hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		CommonVo valueObject = hibernateTemplate.get(exampleEntity.getClass(),
				valueId);

		return valueObject;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method :  saveNestedTransaction
	 * @Description :  Method to Insert entity
	 * @param inVo CommonVo
	 * @throws CustomApplicationException
	 */
	protected HibernateTemplate saveNestedTransaction(final CommonVo inVo) throws CustomApplicationException {
		HibernateTemplate hibernateTemplate = null ;
		try{
		hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
			hibernateTemplate.save(inVo);
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
		return hibernateTemplate;
	}

	/**
	 * @Method :  updateNestedTransaction
	 * @Description :  Method to Insert entity
	 * @param inVo CommonVo
	 * @throws CustomApplicationException
	 */
	protected HibernateTemplate updateNestedTransaction(final CommonVo inVo) throws CustomApplicationException {
		HibernateTemplate hibernateTemplate = null ;
		try{
		hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
			hibernateTemplate.update(inVo);
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
		return hibernateTemplate;
	}

	/**
	 * @Method :  saveNestedTransaction
	 * @Description :  Method to Insert entity
	 * @param inVo CommonVo
	 * @throws CustomApplicationException
	 */
	protected void saveNestedTransaction(final CommonVo inVo, HibernateTemplate hibernateTemplate)
			throws CustomApplicationException {
		try {
			if (hibernateTemplate != null)
				hibernateTemplate.save(inVo);

		} catch (DataIntegrityViolationException e) {
			throw new CustomApplicationException("populateCriteria", e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		} catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria", e);
		} catch (Exception e) {
			throw new CustomApplicationException("populateCriteria", e);
		}

	}

	/**
	 * @Method :  saveNestedTransaction
	 * @Description :  Method to Insert entity
	 * @param inVo CommonVo
	 * @throws CustomApplicationException
	 */
	protected void updateNestedTransaction(final CommonVo inVo, HibernateTemplate hibernateTemplate)
			throws CustomApplicationException {
		try {
			if (hibernateTemplate != null)
				hibernateTemplate.update(inVo);

		} catch (DataIntegrityViolationException e) {
			throw new CustomApplicationException("populateCriteria", e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		} catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria", e);
		} catch (Exception e) {
			throw new CustomApplicationException("populateCriteria", e);
		}

	}

	/**
	 * @Method :  save
	 *
	 * @Description :  Method to Insert entity
	 *
	 * @param inVo CommonVo
	 *
	 * @throws CustomApplicationException
	 */
	protected void save(final CommonVo inVo) throws CustomApplicationException {
		HibernateTemplate hibernateTemplate = null;
		try{
		 hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
//		hibernateTemplate.clear();
			hibernateTemplate.save(inVo);
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}

	}

	/**
	 * @Method : update
	 *
	 * @Description : Method to Delete the entity temporarily
	 *
	 * @param vo CommonVo
	 *
	 * @throws CustomApplicationException
	 */
	protected void update(final CommonVo vo) throws CustomApplicationException {
		HibernateTemplate hibernateTemplate = null;
		try{
		hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		//hibernateTemplate.clear();
		hibernateTemplate.update(vo);
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException(e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException(e);
		}catch (Exception e) {
			throw new CustomApplicationException(e);
		}
	}

	/**
	 * @Method : update
	 * @Description : update some fields by query
	 * @param queryString
	 * @throws CustomApplicationException
	 */
	protected void update(final String queryString) throws CustomApplicationException {
		HibernateTemplate hibernateTemplate = null;
		try {
		 hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
//		hibernateTemplate.clear();
			hibernateTemplate.bulkUpdate(queryString);
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException(e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);

		}catch (HibernateException e) {
			throw new CustomApplicationException(e);
		}catch (Exception e) {
			throw new CustomApplicationException(e);
		}
	}

	/**
	 * @Method : totalRecordsWithCriteria
	 *
	 * @Description : Total number record with Criteria
	 *
	 * @param criteria DetachedCriteria
	 *
	 * @return int
	 *
	 * @throws CustomApplicationException
	 */
	private int totalRecordsWithCriteria(DetachedCriteria criteria) throws CustomApplicationException {
		HibernateTemplate hibernateTemplate = null;
		try{
		 hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		criteria.setProjection(Projections.rowCount());
		int totalCount = ((Integer) hibernateTemplate.findByCriteria(criteria)
				.get(0)).intValue();
		criteria.setProjection(null);
		return totalCount;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method : findWithPaginationCriteria
	 *
	 * @Description :Get List of Vo's with pagination with criteria
	 *
	 * @param daoUtilsVo CommonDaoUtilsVo
	 *
	 * @return ResponseVo
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected ResponseVo findWithPaginationCriteria(CommonDaoUtilsVo daoUtilsVo) throws CustomApplicationException {
		try{
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		int totalRecord = totalRecordsWithCriteria(daoUtilsVo.getCriteria());
		List valueList = hibernateTemplate.findByCriteria(daoUtilsVo.getCriteria(),
				daoUtilsVo.getPagingVo().getStartRow(), daoUtilsVo.getPagingVo().getMaxResult());

		ResponseVo responseVo = new ResponseVo();

		responseVo.setResponseList(valueList);
		daoUtilsVo.getPagingVo().setTotalNumberOfRows(totalRecord);
		responseVo.setPagingVo(daoUtilsVo.getPagingVo());
		return responseVo;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method :  findWithoutPaginationCriteria
	 *
	 * @Description : Get List of Vo's without pagination with criteria
	 *
	 * @param commonDaoUtilsVo
	 *
	 * @return ValueObjectList
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected ValueObjectList findWithoutPaginationCriteria(
			CommonDaoUtilsVo commonDaoUtilsVo) throws CustomApplicationException{
		try {
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");

		List valueList = null;

			valueList = hibernateTemplate.findByCriteria(commonDaoUtilsVo.getCriteria());

		ValueObjectList list = new ValueObjectList();
		list.setNewList(valueList);
		return list;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}


	/**
	 * @Method : findWithCriteriaLimit1
	 *
	 * @Description :Get List contain one record
	 *
	 * @param daoUtilsVo CommonDaoUtilsVo
	 *
	 * @return ValueObjectList
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected ValueObjectList findWithCriteriaLimit1(CommonDaoUtilsVo daoUtilsVo) throws CustomApplicationException {
		try{
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		List valueList = hibernateTemplate.findByCriteria(daoUtilsVo.getCriteria(),0,1);
		ValueObjectList list = new ValueObjectList();
		list.setNewList(valueList);
		return list;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method : permanentDelete
	 *
	 * @Description :  Method to Permanent Delete list of entity
	 *
	 * @param voList ValueObjectList;&ltCommonVo;&gt
	 *
	 * @throws CustomApplicationException
	 */
	protected void permanentDelete(final ValueObjectList<CommonVo> voList)
			throws CustomApplicationException{
		try{
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
			hibernateTemplate.delete(voList.getList());
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method : recovery
	 *
	 * @Description : Method to Recovery list of entity
	 *
	 * @param voList ValueObjectList;&ltCommonVo;&gt
	 *
	 * @throws CustomApplicationException
	 */
	protected void recovery(final ValueObjectList<CommonVo> voList)
			throws CustomApplicationException{
		try{
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
			hibernateTemplate.update(voList.getList());
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method : findComboValues
	 *
	 * @Description : Return the list of ComboVo
	 *
	 * @param queryName
	 *
	 * @return List
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings("rawtypes")
	protected List findComboValues(String queryName) throws CustomApplicationException{
		try {
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");

		List valueList = null;
			valueList = hibernateTemplate.findByNamedQuery(queryName);
		return valueList;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * pagination with criteria
	 *
	 * @param criteria DetachedCriteria
	 *
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	protected List findValues(String queryName, Object value) throws CustomApplicationException{
		try {
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");

		List valueList = null;
			valueList = hibernateTemplate.findByNamedQuery(queryName,value);
		return valueList;
		}catch(HibernateException e){
			throw new CustomApplicationException("findWithoutPaginationCriteria", e);
		}
	}

	/**
	 * @Method : findComboValuesByParam
	 * @Description : find values by given parameter
	 * @param queryName
	 * @param paramNames
	 * @param values
	 * @return List
	 * @throws CustomApplicationException
	 */
	protected List findComboValuesByParam(String queryName, String[] paramNames, Object[] values) throws CustomApplicationException{
		try {
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");

		List valueList = null;
			valueList = hibernateTemplate.findByNamedQueryAndNamedParam(queryName, paramNames, values);
		return valueList;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}


	/**
	 * @Method : find
	 *
	 * @Description : Return the list
	 *
	 * @param queryName
	 *
	 * @return List
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings("unchecked")
	protected List find(String queryString) throws CustomApplicationException {
		try {
			HibernateTemplate hibernateTemplate =
					(HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");

			List valueList = null;
			valueList = hibernateTemplate.find(queryString);
			return valueList;
		} catch (DataIntegrityViolationException e) {
			throw new CustomApplicationException("populateCriteria", e);
		} catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria", e);
		} catch (Exception e) {
			throw new CustomApplicationException("populateCriteria", e);
		}
	}
	/**
	 * @Method : find
	 *
	 * @Description : Return the list
	 *
	 * @param queryName
	 *
	 * @return List
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings("unchecked")
	protected List find(String queryString, Object[] values) throws CustomApplicationException {
		try {
			HibernateTemplate hibernateTemplate =
					(HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");

			List valueList = null;
			valueList = hibernateTemplate.find(queryString, values);
			return valueList;
		} catch (DataIntegrityViolationException e) {
			throw new CustomApplicationException("populateCriteria", e);
		} catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria", e);
		} catch (Exception e) {
			throw new CustomApplicationException("populateCriteria", e);
		}
	}

	/**
	 * The method to get the Column Name of the VO's
	 *
	 * @param exampleEntity CommonVo
	 *
	 * @return List
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected List getColumnNames(CommonVo exampleEntity) throws CustomApplicationException {
		try {
			HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
			int noOfColumns = ((AbstractEntityPersister) hibernateTemplate.getSessionFactory().getClassMetadata(
					exampleEntity.getClass())).countSubclassProperties();
			List columnList = new ArrayList();
			for (int i = 0; i < noOfColumns; i++) {
				String[] columnArray = ((AbstractEntityPersister) hibernateTemplate.getSessionFactory()
						.getClassMetadata(exampleEntity.getClass())).getPropertyColumnNames(i);
				if (columnArray.length > 0) {
					columnList.add(columnArray[0]);
				}
			}

			if (columnList.size() > 0) {
				ClassMetadata classMetadata = hibernateTemplate.getSessionFactory().getClassMetadata(
						exampleEntity.getClass());
				if (null == classMetadata) {
					System.out.println("classMetadata is null");
				} else {
					if (classMetadata instanceof AbstractEntityPersister) {
						AbstractEntityPersister persister = (AbstractEntityPersister) classMetadata;
						String[] columnNames = persister.getKeyColumnNames();
						if (columnNames.length > 0) {
							columnList.add(columnNames[0]);
						}
					}
				}
//				Collections.sort(columnList);
			}
			return columnList;
		} catch (HibernateException e) {
			throw new CustomApplicationException("findValue", e);
		}
	}

	protected Map getTableName() throws CustomApplicationException {
		try {
			HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
			List columnList = new ArrayList();
			Map m = hibernateTemplate.getSessionFactory().getAllClassMetadata();
			for (Iterator i = m.values().iterator(); i.hasNext(); ) {
				EntityPersister persister = (EntityPersister) i.next();
				String className = persister.getClassMetadata().getEntityName();
				m.put(i, className);
			}
			return m;
		} catch (HibernateException e) {
			throw new CustomApplicationException("findValue", e);
		}
	}



		/**
	 * @Method :  saveOrUpdateAll
	 *
	 * @Description :  Method to Insert entity list
	 *
	 * @param inVo CommonVo
	 *
	 * @throws CustomApplicationException
	 */
	protected void saveOrUpdateAll(final List inVoList) throws CustomApplicationException {
		try{
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		hibernateTemplate.clear();
			hibernateTemplate.saveOrUpdateAll(inVoList);

		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}

	}

	/**
	 * @Method :  saveOrUpdateAll
	 *
	 * @Description :  Method to Insert entity list
	 *
	 * @param inVo CommonVo
	 *
	 * @throws CustomApplicationException
	 */
	protected void saveOrUpdate(final CommonVo inVo) throws CustomApplicationException {
		try{
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		hibernateTemplate.clear();
			hibernateTemplate.saveOrUpdate(inVo);
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}

	}

	/**
	 * @Method : findValuesByParam
	 * @Description : find values by given parameter
	 * @param queryName
	 * @param paramNames
	 * @param values
	 * @return List
	 * @throws CustomApplicationException
	 */
	protected List findValuesByParam(String queryName, String[] paramNames, Object[] values) throws CustomApplicationException{
		try {
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");

		List valueList = null;
			valueList = hibernateTemplate.findByNamedQueryAndNamedParam(queryName, paramNames, values);
		return valueList;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method : update
	 * @Description : update based on query and param
	 * @param queryString
	 * @param values
	 * @return Integer
	 * @throws CustomApplicationException
	 */
	protected Integer update(final String queryString, Object[] values) throws CustomApplicationException {
		try {
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
//		hibernateTemplate.clear();
			return hibernateTemplate.bulkUpdate(queryString, values);
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException(e);
		}catch (HibernateException e) {
			throw new CustomApplicationException(e);
		}catch (Exception e) {
			throw new CustomApplicationException(e);
		}
	}



	/**
	 * @Method : findWithPaginationCriteria
	 *
	 * @Description :Get List of Vo's with pagination with criteria
	 *
	 * @param daoUtilsVo CommonDaoUtilsVo
	 *
	 * @return ResponseVo
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "rawtypes" })
	protected ResponseVo findWithPaginationCriteriaAlias(CommonDaoUtilsVo daoUtilsVo) throws CustomApplicationException {
		try{
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		int totalRecord = totalRecordsWithCriteria(daoUtilsVo.getCriteria());
		List valueList = hibernateTemplate.findByCriteria(daoUtilsVo.getCriteria(),
				daoUtilsVo.getPagingVo().getStartRow(), daoUtilsVo.getPagingVo().getMaxResult());

		ResponseVo responseVo = new ResponseVo();

		responseVo.setResultList(valueList);
		daoUtilsVo.getPagingVo().setTotalNumberOfRows(totalRecord);
		responseVo.setPagingVo(daoUtilsVo.getPagingVo());
		return responseVo;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method : findWithPaginationCriteria
	 *
	 * @Description :Get List of Vo's with pagination with criteria
	 *
	 * @param daoUtilsVo CommonDaoUtilsVo
	 *
	 * @return ResponseVo
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "rawtypes" })
	protected ResponseVo findWithoutPaginationCriteriaAlias(CommonDaoUtilsVo daoUtilsVo) throws CustomApplicationException {
		try{
		HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		List valueList = hibernateTemplate.findByCriteria(daoUtilsVo.getCriteria());
		ResponseVo responseVo = new ResponseVo();
		responseVo.setResultList(valueList);
		return responseVo;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	/**
	 * @Method :
	 * @Description :
	 * @param exampleEntity
	 * @param valueId
	 * @return
	 * @throws CustomApplicationException
	 */
	protected CommonVo findValueInt(CommonVo exampleEntity, Integer valueId) throws CustomApplicationException{

		HibernateTemplate hibernateTemplate = null;
		try{
		hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		CommonVo valueObject = hibernateTemplate.get(exampleEntity.getClass(),
				valueId);

		return valueObject;
		}catch(DataIntegrityViolationException e){
			throw new CustomApplicationException("populateCriteria",  e);
		}catch(CannotAcquireLockException e){
		    hibernateTemplate.flush();
		    hibernateTemplate.clear();
			throw new CustomApplicationException("Some one in transaction please try after some time", e);
		}catch (HibernateException e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}catch (Exception e) {
			throw new CustomApplicationException("populateCriteria",  e);
		}
	}

	@SuppressWarnings("unchecked")
	   protected List findSql(String queryString) throws CustomApplicationException {
	      HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
	      SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
	      Session session =sessionFactory.openSession();
	      SQLQuery sqlQuery1 = session.createSQLQuery(queryString);
	      List list = sqlQuery1.list();
	      session.close();
	      return list;
	   }

}
