/**
 * Creater / durgeshkumarjhaSRM copyright information to Shirai 2013
 * 
 */
package com.task.dao.custom;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.task.common.ComboVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.UserVo;
import com.task.util.ApplicationPagingVo;
import com.task.util.BoCopyManager;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.LoggingAspectInterceptor;
import com.task.vo.custom.action.ApprovedLeaveResultVo;
import com.task.vo.custom.action.LeaveActionVo;
import com.task.vo.custom.action.LeaveSearchVo;
import com.task.vo.custom.action.LeaveStatusActionVo;
import com.task.vo.custom.action.LeaveTypeActionVo;
import com.task.vo.custom.entity.LeaveEntityVo;
import com.task.vo.custom.entity.LeaveStatusEntityVo;
import com.task.vo.custom.entity.LeaveTypeEntityVo;

/**
 * @author durgeshkumarjha
 * 
 */
public class LeaveDao extends CommonDao {

    private static Logger log = LoggingAspectInterceptor.getLog(LeaveDao.class);

    /**
     * 
     * @Method :
     * @Description :
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo saveLeave(RequestVo requestVo)
	    throws CustomApplicationException {
	ResponseVo responseVo = new ResponseVo();
	LeaveEntityVo leaveEntityVo = new LeaveEntityVo();
	try {
	    /* copy properties */
	    BoCopyManager
		    .copyProperties(requestVo.getCommonVo(), leaveEntityVo);

	    LeaveStatusEntityVo leaveStatusEntityVo = new LeaveStatusEntityVo();
	    leaveStatusEntityVo.setLeaveStatusId(1);
	    leaveEntityVo.setLeaveStatus(leaveStatusEntityVo);

	    LeaveActionVo leaveActionVo = (LeaveActionVo) requestVo
		    .getCommonVo();
	    LeaveTypeEntityVo leaveTypeEntityVo = new LeaveTypeEntityVo();
	    BoCopyManager.copyProperties(leaveActionVo.getLeaveTypeActionVo(),
		    leaveTypeEntityVo);
	    leaveEntityVo.setLeaveType(leaveTypeEntityVo);

	    /* invoke a Common Dao */
	    super.save(leaveEntityVo);
	    return responseVo;
	} catch (CustomApplicationException e) {
	    throw e;
	} catch (Exception ex) {
	    throw new CustomApplicationException(ex);
	}
    }

    /**
     * 
     * @Method :
     * @Description :
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo getLeaveDetail(RequestVo requestVo)
	    throws CustomApplicationException {
	ResponseVo outVo = null;
	try {

	    LeaveActionVo leaveActionVo = (LeaveActionVo) requestVo.getCommonVo();
	    LeaveEntityVo leaveEntityVo = new LeaveEntityVo();
	    /* invoke a Common Dao */
	    leaveEntityVo = (LeaveEntityVo) super.findValueInt(leaveEntityVo,leaveActionVo.getEmpLeaveDetailId());

	    outVo = new ResponseVo();

	    /* copy properties */
	    LeaveActionVo actionVo = new LeaveActionVo();
	    BoCopyManager.copyProperties(leaveEntityVo, actionVo);

	    LeaveTypeActionVo leaveTypeActionVo = new LeaveTypeActionVo();
	    LeaveTypeEntityVo leaveTypeEntityVo = leaveEntityVo.getLeaveType();
	    leaveTypeActionVo.setLeaveTypeId(leaveTypeEntityVo.getLeaveTypeId());
	    
	    LeaveStatusActionVo leaveStatusActionVo = new LeaveStatusActionVo();
	    LeaveStatusEntityVo leaveStatusEntityVo = leaveEntityVo.getLeaveStatus();
	    leaveStatusActionVo.setLeaveStatusId(leaveStatusEntityVo.getLeaveStatusId());

	    actionVo.setLeaveTypeActionVo(leaveTypeActionVo);
	    actionVo.setLeaveStatusActionVo(leaveStatusActionVo);
	    outVo.setCommonVo(actionVo);

	    return outVo;
	} catch (CustomApplicationException e) {
	    throw e;
	} catch (Exception ex) {
	    throw new CustomApplicationException(ex);
	}
    }

    /**
     * 
     * @Method :
     * @Description :
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo updateLeave(RequestVo requestVo)
	    throws CustomApplicationException {
	UserVo userVo = requestVo.getUserVo();
	ResponseVo responseVo = new ResponseVo();
	LeaveEntityVo leaveEntityVo = new LeaveEntityVo();
	try {
	    /* Copy Properties */
	    BoCopyManager.copyProperties(requestVo.getCommonVo(), leaveEntityVo);

	    LeaveActionVo leaveActionVo = (LeaveActionVo) requestVo.getCommonVo();

	    LeaveTypeEntityVo leaveTypeEntityVo = new LeaveTypeEntityVo();
	    BoCopyManager.copyProperties(leaveActionVo.getLeaveTypeActionVo(),leaveTypeEntityVo);
	    leaveEntityVo.setLeaveType(leaveTypeEntityVo);

	    LeaveStatusEntityVo leaveStatusEntityVo = new LeaveStatusEntityVo();
	    BoCopyManager.copyProperties(leaveActionVo.getLeaveStatusActionVo(),leaveStatusEntityVo);
	    leaveEntityVo.setLeaveStatus(leaveStatusEntityVo);

	    /* invoke a Common Dao */
	    super.update(leaveEntityVo);
	    if (leaveEntityVo.getLeaveStatus().getLeaveStatusId() == 4 && userVo.getDesgId().equals("25")) {
			Object[] paramValues = new Object[] { leaveEntityVo.getEmpLeaveDetailId() };
			ApprovedLeaveResultVo approvedLeaveResultVo = getApprovedLeaveList(paramValues);
			responseVo.setCommonVo(approvedLeaveResultVo);
	    }
	    
	    return responseVo;
	} catch (CustomApplicationException e) {
	    throw e;
	} catch (Exception ex) {
	    throw new CustomApplicationException(ex);
	}
    }

    /**
     * 
     * @Method : listLeave
     * @Description : This method is used for getting all applied leave list.
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo listLeave(RequestVo requestVo)
	    throws CustomApplicationException {
	ResponseVo reponseVo = new ResponseVo();
	Session session = null;

	try {

	    
	    ApplicationPagingVo pagingVo = requestVo.getPagingVo();
	    UserVo userVo = requestVo.getUserVo();

	    String countColumn = " SELECT COUNT(*) as cnt";

	    String selectColumns = " SELECT le.employeeId, le.fromDate, le.toDate , le.noOfDays, le.leaveStatus.leaveStatusId,le.leaveStatus.leaveStatus, e.name, le.reasonForLeave, le.empLeaveDetailId, le.supervisorComments";

	    StringBuilder queryBuilder = new StringBuilder();

	    queryBuilder.append(" FROM com.task.vo.custom.entity.LeaveEntityVo le, com.task.vo.custom.entity.EmployeeEntityVo e");
	    queryBuilder.append(" WHERE le.employeeId = e.id ");

	    if (userVo.getDesgId().equalsIgnoreCase("25")) {
	    	queryBuilder.append(" AND ((le.leaveStatus.leaveStatusId in (1,2,3) and le.employeeId in (select pom.pmId From ProjectOwnerMappingEntityVo pom))");
	    	queryBuilder.append(" OR le.leaveStatus.leaveStatusId in (1,2,3))");
	    } else if ((userVo.getDesgId().equalsIgnoreCase("6"))
		    || (userVo.getDesgId().equalsIgnoreCase("24"))
		    || (userVo.getDesgId().equalsIgnoreCase("63"))) {
			queryBuilder.append(" AND (le.reportingManager='"+userVo.getEmployeeId()+"' OR le.employeeId='"+userVo.getEmployeeId()+"')");
			queryBuilder.append(" AND le.leaveStatus.leaveStatusId <> '4'");
	    } else {
	    	queryBuilder.append(" AND le.employeeId='"+userVo.getEmployeeId()+"'");
	    }

	    LeaveSearchVo leaveSearchVo = (LeaveSearchVo) requestVo
		    .getSeacrhVo();

	    if (null != leaveSearchVo.getFromDateStr()
		    && !leaveSearchVo.getFromDateStr().equals("")) {
		queryBuilder.append(" AND le.fromDate >=").append("'")
			.append(CommonUtils.convertDateFormat(leaveSearchVo.getFromDateStr(), "dd/MM/yyyy", "yyyy/MM/dd")).append("'");
	    }

	    if (null != leaveSearchVo.getToDateStr()
		    && !leaveSearchVo.getToDateStr().equals("")) {
		queryBuilder.append(" AND le.toDate <=").append("'")
			.append(CommonUtils.convertDateFormat(leaveSearchVo.getToDateStr(), "dd/MM/yyyy", "yyyy/MM/dd")).append("'");
	    }

	    String query = countColumn + queryBuilder.toString();

	    List result = super.find(query);
	    if (result != null && result.size() > 0) {
		pagingVo.setTotalNumberOfRows(((Long) result.get(0)).intValue());
	    }

	    queryBuilder.append(" ORDER BY le.fromDate, e.name ");

	    HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		session = sessionFactory.openSession();
		    
	    query = selectColumns + queryBuilder.toString();
	    Query sqlQuery = session.createQuery(query);
	    sqlQuery.setFirstResult(pagingVo.getStartRow());
	    sqlQuery.setMaxResults(pagingVo.getMaxResult());

	    List<LeaveActionVo> serviceList = new ArrayList<LeaveActionVo>();
	    result = sqlQuery.list();
	    if (result != null && result.size() > 0) {
		for (int index = 0, size = result.size(); index < size; index++) {
		    Object[] row = (Object[]) result.get(index);

		    LeaveActionVo leaveActionVo = new LeaveActionVo();
		    LeaveStatusActionVo leaveStatusActionVo = new LeaveStatusActionVo();

		    leaveActionVo.setEmployeeId((Integer) row[0]);
		    leaveActionVo.setFromDate((Date) row[1]);
		    leaveActionVo.setToDate((Date) row[2]);
		    leaveStatusActionVo.setLeaveStatusId((Integer) row[4]);
		    leaveStatusActionVo.setLeaveStatus((String) row[5]);
		    
		    leaveActionVo.setLeaveStatusActionVo(leaveStatusActionVo);
		    leaveActionVo.setEmployeeName((String) row[6]);
		    leaveActionVo.setReasonForLeave((String) row[7]);
		    leaveActionVo.setEmpLeaveDetailId((Integer) row[8]);
		    leaveActionVo.setSupervisorComments((String) row[9]);
		    serviceList.add(leaveActionVo);
		}
	    } else {
		reponseVo.setErrorMessage("No record found");
	    }
	    session.close();
	    reponseVo.setPagingVo(pagingVo);
	    reponseVo.setResponseList(serviceList);

	    return reponseVo;

	} catch (Exception ex) {
	    if (session != null) {
		session.close();
	    }
	    throw new CustomApplicationException(ex);
	}
    }

    /**
     * 
     * @Method : comboLoading
     * @Description : This method is used for load combo.
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo comboLoading(RequestVo requestVo)
	    throws CustomApplicationException {
	ResponseVo responseVo = new ResponseVo();

	if ("KindOfLeave".equals(requestVo.getComboType())) {
	    responseVo = getKindOfLeaveCombo("KindOfLeave");
	} else if ("LeaveStatus".equals(requestVo.getComboType())) {
	    responseVo = getLeaveStatusCombo(requestVo,"LeaveStatus");
	} else if ("PMLeaveCount".equals(requestVo.getComboType())) {
	    responseVo = getPmLeaveCountCombo("PMLeaveCount");
	} else if (requestVo.getComboType().equals("ProjectManagerList")) {
	    responseVo = getProjectManagerCombo(requestVo);
	}

	return responseVo;
    }

    /**
     * 
     * @Method :
     * @Description :
     * @param comboName
     * @return
     * @throws CustomApplicationException
     */
    private ResponseVo getKindOfLeaveCombo(String comboName)
	    throws CustomApplicationException {
	List<ComboVo> leaveList = new ArrayList<ComboVo>();
	List comboValueList = super.findComboValues(comboName);

	ResponseVo responseVo = new ResponseVo();

	if (comboValueList != null) {
	    for (Object object : comboValueList) {
		ComboVo comboVo = new ComboVo();
		comboVo.setIdValue((Integer) (((Object[]) object)[0]));
		comboVo.setName((String) ((Object[]) object)[1]);
		comboVo.setKey(((Integer) ((Object[]) object)[0]).toString());
		leaveList.add(comboVo);
	    }
	}
	responseVo.setComboValueList(leaveList);
	return responseVo;
    }

    /**
     * 
     * @Method :
     * @Description :
     * @param comboName
     * @return
     * @throws CustomApplicationException
     */
    private ResponseVo getLeaveStatusCombo(RequestVo requestVo,String comboName)
	    throws CustomApplicationException {
    	
    UserVo userVo = requestVo.getUserVo();
    Integer statusId = 4;
    if("25".equals(userVo.getDesgId())){
    	statusId = 0;
    }
    String paramNames[] = {"statusId"};
    Object paramValues[] = {statusId};
    
	List<ComboVo> statusList = new ArrayList<ComboVo>();
	List comboValueList = super.findValuesByParam(comboName,paramNames,paramValues);

	ResponseVo responseVo = new ResponseVo();

	if (comboValueList != null) {
	    for (Object object : comboValueList) {
		ComboVo comboVo = new ComboVo();
		comboVo.setIdValue((Integer) (((Object[]) object)[0]));
		comboVo.setName((String) ((Object[]) object)[1]);
		comboVo.setKey(((Integer) ((Object[]) object)[0]).toString());
		statusList.add(comboVo);
	    }
	}
	responseVo.setComboValueList(statusList);
	return responseVo;
    }

    /**
     * 
     * @Method : getProjectManagerCombo
     * @Description : This method is used for getting project manager list
     * @param requestVo
     * @return
     */
    private ResponseVo getProjectManagerCombo(RequestVo requestVo) {
	List comboValueList = null;
	ResponseVo responseVo = new ResponseVo();
	try {
	    comboValueList = super.findComboValues("ProjectManagerList");

	    ComboVo comboVo;
	    List<ComboVo> comboValuelst = new ArrayList<ComboVo>();
	    for (Object object : comboValueList) {
		comboVo = new ComboVo();
		comboVo.setIdValue((Integer) ((Object[]) object)[0]);
		comboVo.setName((String) ((Object[]) object)[1]);
		comboVo.setKey((String) ((Object[]) object)[2]);
		comboVo.setRefId((Integer) ((Object[]) object)[0] + ":" + (String) ((Object[]) object)[1]);
		comboVo.setCommonName((String) ((Object[]) object)[2] + ":" + ((Object[]) object)[1]);
		comboValuelst.add(comboVo);
	    }
	    responseVo.setComboValueList(comboValuelst);
	} catch (CustomApplicationException e) {
	    log.error(e);
	}
	return responseVo;
    }

    /**
     * 
     * @Method : getApprovedLeaveList
     * @Description : This method is used for getting all approved leave .
     * @return
     * @throws CustomApplicationException 
     */
    private ApprovedLeaveResultVo getApprovedLeaveList(Object[] paramValues) throws CustomApplicationException {
	List resultValueList = null;
	try {
	    
	    String[] paramNames = new String[]{"empLeaveDetailId"};
//	    comboValueList = super.findComboValues("ApprovedLeave");
	    resultValueList = super.findValuesByParam("ApprovedLeave", paramNames, paramValues);
	    ApprovedLeaveResultVo approvedLeaveVo = new ApprovedLeaveResultVo();;

	    for (Object object : resultValueList) {
		approvedLeaveVo.setEmpName((((Object[]) object)[0]).toString());
		approvedLeaveVo.setEmpCode((((Object[]) object)[1]).toString());
		approvedLeaveVo.setDesignation((((Object[]) object)[2]).toString());
		approvedLeaveVo.setDeptName((((Object[]) object)[3]).toString());
		approvedLeaveVo.setLeaveType((((Object[]) object)[4]).toString());
		approvedLeaveVo.setFromDate((formatDatetoString((Date) ((Object[]) object)[5])));
		approvedLeaveVo.setToDate((formatDatetoString((Date) ((Object[]) object)[6])));

		// To check and show whole number if '0' comes after decimal
		String noOfDay = (((Object[]) object)[7]).toString();
		int position = noOfDay.indexOf('.');
		if (noOfDay.charAt(position + 1) == '0') {
		    noOfDay = noOfDay.substring(0, position);
		}
		approvedLeaveVo.setNoOfDays(noOfDay);

		approvedLeaveVo.setRejoinDate((formatDatetoString((Date) ((Object[]) object)[8])));
		approvedLeaveVo.setRemarks((((Object[]) object)[9]).toString());
		approvedLeaveVo.setReason((((Object[]) object)[10]).toString());
		approvedLeaveVo.setAddress((((Object[]) object)[11]).toString());
		approvedLeaveVo.setContactNo((((Object[]) object)[12]).toString());
		approvedLeaveVo.setEmail((((Object[]) object)[13]).toString());
		approvedLeaveVo.setLeaveStatus((((Object[]) object)[14]).toString());
		approvedLeaveVo.setPmCode((((Object[]) object)[15]).toString());
		
	    }
	    return approvedLeaveVo;
	} catch (Exception e) {
	    log.error(e);
	    throw new CustomApplicationException(e);
	}
    }
    
    /**
     * @Method : formatDatetoString
     * @Description : Format date(dd-MM-yyyy) to string
     * @param date
     * @return String
     */
    private String formatDatetoString(Date date) {
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	String dateString = sdf.format(date);
	return dateString;
    }

    /**
     * 
     * @Method : getPmLeaveCountCombo
     * @Description :
     * @param comboName
     * @return
     * @throws CustomApplicationException
     */
    private ResponseVo getPmLeaveCountCombo(String comboName)
	    throws CustomApplicationException {
	List<ComboVo> statusList = new ArrayList<ComboVo>();
	List comboValueList = super.findComboValues(comboName);

	ResponseVo responseVo = new ResponseVo();

	if (comboValueList != null) {
	    for (Object object : comboValueList) {
		ComboVo comboVo = new ComboVo();
		// comboVo.setIdValue((Integer) (((Object[]) object)[0]));
		// comboVo.setName((String) ((Object[]) object)[1]);
		// comboVo.setKey(((Integer) ((Object[])
		// object)[0]).toString());
		statusList.add(comboVo);
	    }
	}
	responseVo.setComboValueList(statusList);
	return responseVo;
    }

}
