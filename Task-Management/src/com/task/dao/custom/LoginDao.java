/**
 * Copyright 2012 by Shirai, All rights reserved.
 *
 * 2012/06/20 <Base Line Version 1.0>
 */
package com.task.dao.custom;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.SessionVo;
import com.task.common.UserVo;
import com.task.common.constant.ConstantEnum;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.vo.custom.action.UserLoginVo;

/**
 * This Dao class is used to login.
 *
 * @author Rajnarayan Saraf [SRM]
 */
public class LoginDao extends CommonDao {

    public ResponseVo authenticateUser(RequestVo requestVo) throws CustomApplicationException {
	SessionVo sessionVo = new SessionVo();
	UserVo userVo = new UserVo();
	sessionVo.setUserVo(userVo);
	ResponseVo responseVo = new ResponseVo();
	responseVo.setSessionVo(sessionVo);
	responseVo.setStatusFlag(false);
	try {
	    	UserLoginVo userloginVo = (UserLoginVo) requestVo.getCommonVo();
	    	String userName = userloginVo.getLoginName();
	    	String password = userloginVo.getPassword();
	    	String query    = " SELECT ID,CODE,NAME,DEPT_ID,DESG_ID,EMAIL,(SELECT Desg_Name FROM desg_master d where d.Desg_ID=e.Desg_Id) as DESG_NAME,"+
	    					  " (SELECT DEPT_NAME FROM dept_master d where d.DEPT_ID=e.Dept_Id) as DEPT_NAME "+
	    			          " FROM employee e WHERE USER_ID='"+userName+"' AND PASSWORD = ENCODE('"+password+"','13')";
	    	HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
	        SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
	        Session session =sessionFactory.openSession();
	        SQLQuery query1 = session.createSQLQuery(query);
	        List result = query1.list();
	        if(result!=null && result.size()>0){
	        	Object[] row = (Object[])result.get(0);
	            userVo.setEmployeeId(String.valueOf((Integer)row[0]));
	            userVo.setEmployeeCode(String.valueOf((String)row[1]));
	            userVo.setUserName(String.valueOf((String)row[2]));
	            userVo.setDeptId(String.valueOf((String)row[3]));
	            userVo.setDesgId(String.valueOf((String)row[4]));
	            userVo.setEmailId(String.valueOf((String)row[5]));
	            userVo.setDesgName(String.valueOf((String)row[6]));
	            userVo.setDeptName(String.valueOf((String)row[7]));
	            if(userVo.getEmployeeCode().equalsIgnoreCase("guest")){
	            	userVo.setGuest(true);
	            }
	            responseVo.setStatusFlag(true);
	        }
	        session.close();
	    	return responseVo;
	    } catch (Exception ex) {
	    	throw new CustomApplicationException(ex);
	    }
    }


	/**
	 *
	 * @Method : getCompanyConfigLogo
	 * @Description : This Method is used to get CompanyLogo
	 * @param requestVo
	 * @return ResponseVo
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseVo getCompanyConfigLogo(RequestVo requestVo) throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		try {
			String[] paramNames = new String[] { "companyId", "type" };
			Object[] values = new Object[] { requestVo.getUserVo().getCompanyId(),
					ConstantEnum.COMPANY_LOGO.getStringValue() };
			List logolist = super.findValuesByParam("ValueLogo", paramNames, values);
			responseVo.setResponseList(logolist);

		} catch (CustomApplicationException ce) {
			throw ce;
		} catch (Exception ex) {
			throw new CustomApplicationException(ex);
		}
		return responseVo;
	}

	/**
	 *
	 * @Method : getSystemConfigLogo
	 * @Description : This method is used to get Company logo for superadmin and sysadmin.
	 * @param requestVo
	 * @return ResponseVo
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseVo getSystemConfigLogo() throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		try {
			String[] paramNames = new String[] { "type" };
			Object[] values = new Object[] { ConstantEnum.LOGIN_LOGO.getStringValue() };
			List logolist = super.findValuesByParam("ScreenTypeValue", paramNames, values);
			responseVo.setResponseList(logolist);

		} catch (CustomApplicationException ce) {
			throw ce;
		} catch (Exception ex) {
			throw new CustomApplicationException(ex);
		}
		return responseVo;
	}


	/**
	 *
	 * @Method : getSystemConfigLogo
	 * @Description : This method is used to get Company logo for superadmin and sysadmin.
	 * @param requestVo
	 * @return ResponseVo
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseVo getCompanyMsgColor() throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		try {
			String[] paramNames = new String[] { "type" };
			Object[] values = new Object[] { ConstantEnum.COMPANY_MSG_COLOR.getStringValue() };
			List logolist = super.findValuesByParam("ScreenTypeValue", paramNames, values);
			responseVo.setResponseList(logolist);

		} catch (CustomApplicationException ce) {
			throw ce;
		} catch (Exception ex) {
			throw new CustomApplicationException(ex);
		}
		return responseVo;
	}

	/**
	 *
	 * @Method : getSystemConfigLogo
	 * @Description : This method is used to get Company logo for superadmin and sysadmin.
	 * @param requestVo
	 * @return ResponseVo
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseVo getSystemMsgColor() throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		try {
			String[] paramNames = new String[] { "type" };
			Object[] values = new Object[] { ConstantEnum.SYSTEM_MSG_COLOR.getStringValue() };
			List logolist = super.findValuesByParam("ScreenTypeValue", paramNames, values);
			responseVo.setResponseList(logolist);

		} catch (CustomApplicationException ce) {
			throw ce;
		} catch (Exception ex) {
			throw new CustomApplicationException(ex);
		}
		return responseVo;
	}

}
