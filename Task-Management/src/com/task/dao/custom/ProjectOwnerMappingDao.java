/**
 * Creater / DurgeshKumarJha[SRM] copyright information to SRM Technologies 2013
 * 
 */
package com.task.dao.custom;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.task.common.ComboVo;
import com.task.common.CommonDaoUtilsVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.UserVo;
import com.task.common.ValueObjectList;
import com.task.common.constant.ConstantEnum;
import com.task.util.ApplicationPagingVo;
import com.task.util.BoCopyManager;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.vo.custom.action.ProjectOwnerMappingActionVo;
import com.task.vo.custom.action.ProjectOwnerSearchVo;
import com.task.vo.custom.action.ProjectStatusActionVo;
import com.task.vo.custom.action.ProjectTypeActionVo;
import com.task.vo.custom.entity.ProjectOwnerMappingEntityVo;
import com.task.vo.custom.entity.ProjectStatusEntityVo;
import com.task.vo.custom.entity.TaskEntityVo;

/**
 * This DAO class is used to Add, Update, View, Search and Delete Project Owner Mapping.
 * 
 * @author DurgeshKumarJha[SRM]
 * @since 22/02/2013
 * 
 */
public class ProjectOwnerMappingDao extends CommonDao {
    private static Logger log = LoggingAspectInterceptor.getLog(ProjectOwnerMappingDao.class);
    /**
     * @Method : saveProjectOwnerMapping
     * @Description :  To save the Project Owner Mapping details in [project_owner_mapping] table.
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo saveProjectOwnerMapping(RequestVo requestVo)
	    throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		ProjectOwnerMappingEntityVo projectOwnerMappingEntityVo = new ProjectOwnerMappingEntityVo();
		try {
			/* copy properties */
			BoCopyManager.copyProperties(requestVo.getCommonVo(), projectOwnerMappingEntityVo);
			
			if (projectOwnerMappingEntityVo.getProjectTypeId() == 2 ) {
				int projectId = getMaxPracticeTeamId(projectOwnerMappingEntityVo.getProjectTypeId());
				projectOwnerMappingEntityVo.setProjectId(String.valueOf((projectId+1)));
				projectOwnerMappingEntityVo.setProjectCode(String.valueOf(projectId+1));
			}
			else{
				projectOwnerMappingEntityVo.setAssociatedProjectId(null);
				projectOwnerMappingEntityVo.setAssociatedFrom(null);
			}
			ProjectStatusEntityVo projectStatus = new ProjectStatusEntityVo();
			projectStatus.setStatusId(1);
			projectOwnerMappingEntityVo.setProjectStatus(projectStatus);
			
			/* invoke a Common Dao */
			super.save(projectOwnerMappingEntityVo);
			return responseVo;
		} catch (CustomApplicationException e) {
			throw e;
		} catch (Exception ex) {
			throw new CustomApplicationException(ex);
		}
	}

    /**
     * @Method : showProjectOwnerMapping
     * @Description : To show the Project Owner mapping's
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    @SuppressWarnings("unchecked")
    public ResponseVo showProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException {
	ResponseVo responseVo = null;
	try {

	    /* To Populate Criteria */
	    CommonDaoUtilsVo commonDaoUtilsVo = super.populateCriteria(TaskEntityVo.class);


	    ValueObjectList<ProjectOwnerMappingEntityVo> valueList = (ValueObjectList<ProjectOwnerMappingEntityVo>) super.findWithoutPaginationCriteria(commonDaoUtilsVo);
	    List<ProjectOwnerMappingActionVo> mappingActionVos = new ArrayList<ProjectOwnerMappingActionVo>();
	    if ((valueList != null) && (valueList.getList().size() > 0)) {

		for (ProjectOwnerMappingEntityVo entityVo : valueList.getList()) {
		    ProjectOwnerMappingActionVo actionVo = new ProjectOwnerMappingActionVo();
		    mappingActionVos.add(actionVo);
		}

		responseVo = new ResponseVo();
		responseVo.setResponseList(mappingActionVos);
	    } else {
		responseVo = new ResponseVo();
		responseVo.setErrorMessage(FrameworkConstant
			.getError("NO_RECORD_FOUND"));
	    }
	    return responseVo;
	} catch (CustomApplicationException e) {
	    throw e;
	} catch (Exception e) {
	    throw new CustomApplicationException(e);
	}

    }
    
    /**
     * @Method : comboLoading
     * @Description : Method to load the Combo values
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo comboLoading(RequestVo requestVo) throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		if(requestVo.getComboType().equals("projectType")){
			responseVo = getProjectTypes(requestVo);
		}
		else if(requestVo.getComboType().equals("project")){
			responseVo = getProjectCombo(requestVo);	
		}
		else if(requestVo.getComboType().equals("departmentProject")){
			responseVo = getDepartmentProjectCombo(requestVo);	
		}
		else if ("pmName".equals(requestVo.getComboType())) {
			responseVo.setComboValueList(comboLoadingPmPL("PrjMList",requestVo));
		}
		else if ("plName".equals(requestVo.getComboType())) {
			responseVo.setComboValueList(comboLoadingPmPL("PrjLList",requestVo));
		}	
		else if ("mappedProject".equals(requestVo.getComboType())) {
			responseVo.setComboValueList(comboLoadingMappedProject());
		}
		else if ("ProjectStatus".equals(requestVo.getComboType())){
			responseVo = getProjectStatusCombo("ProjectStatus");	
		}
		else if ("employee".equals(requestVo.getComboType())) {
		    responseVo = getEmployeeNameList(requestVo);
		}
		else if ("PMListForLeave".equals(requestVo.getComboType())) {
		    responseVo = getPMListForLeave(requestVo);
		}
		else if ("associatedProjectList".equals(requestVo.getComboType())) {
		    responseVo = getAllProject(requestVo);
		}
		return responseVo;
    }
    private int getMaxPracticeTeamId(int projectTypeId) throws CustomApplicationException {
    	int returnValue=0;
    	List<String> paramNames = new ArrayList<String>();
   	 	List<Object> paramValues = new ArrayList<Object>();
        paramNames.add("projectTypeId");
        paramValues.add(projectTypeId);
        List comboValueList = super.findComboValuesByParam("MaxPracticeTeamProjectId", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());
		if (comboValueList != null && comboValueList.size()>0) {
			for (Object object : comboValueList) {
				if(object!=null){
					returnValue = Integer.parseInt((String)object);
				}
			}
		}
		return returnValue;
    }
    /**
     * @Method : comboLoadingPmPL
     * @Description : Method to load Project manager in PM combobox.
     * @param comboName
     * @return
     * @throws CustomApplicationException
     */
    private List<ComboVo> comboLoadingMappedProject() throws CustomApplicationException {
		List<ComboVo> projectList = new ArrayList<ComboVo>();
		List comboValueList = super.findComboValues("ExistingMappedProjects");
		if (comboValueList != null) {
			for (Object object : comboValueList) {
			    ComboVo comboVo = new ComboVo();
			    comboVo.setKey((String)(((Object[]) object)[0]));
			    comboVo.setName((String) ((Object[]) object)[1]);
			    comboVo.setCommonName((String) ((Object[]) object)[2]);
			    comboVo.setRefId(((Integer) ((Object[]) object)[3]).toString());
			    projectList.add(comboVo);
			}
		}
		return projectList;
    }
    
    /**
     * @Method : comboLoadingPmPL
     * @Description : Method to load Project manager in PM combobox.
     * @param comboName
     * @return
     * @throws CustomApplicationException
     */
    private List<ComboVo> comboLoadingPmPL(String comboName,RequestVo requestVo) throws CustomApplicationException {
		List<ComboVo> projectList = new ArrayList<ComboVo>();
		UserVo userVo = requestVo.getUserVo();
		List<String> paramNames = new ArrayList<String>();
   	 	List<Object> paramValues = new ArrayList<Object>();
        paramNames.add("deptId");
        paramValues.add(userVo.getDeptId());
        List comboValueList = super.findComboValuesByParam(comboName, (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());
		
		if (comboValueList != null) {
			for (Object object : comboValueList) {
			    ComboVo comboVo = new ComboVo();
			    comboVo.setIdValue((Integer)(((Object[]) object)[0]));
			    comboVo.setKey((Integer) ((Object[]) object)[0]+"-"+(String) ((Object[]) object)[2]+"-"+(String) ((Object[]) object)[3]);
			    comboVo.setName((String) ((Object[]) object)[2]);
			    projectList.add(comboVo);
			}
		}
		return projectList;
    }

	/**
	 * @Method : listProjectOwnerMapping
	 * @Description : Method is used for displaying project owner mapping list.
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException 
	 */
	public ResponseVo listProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException {
		ResponseVo reponseVo = new ResponseVo();
		Session session = null;
		try {	
				HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		        SessionFactory sessionFactory = hibernateTemplate.getSessionFactory(); 
		        session =sessionFactory.openSession();
		        ApplicationPagingVo pagingVo = requestVo.getPagingVo();
		        UserVo userVo = requestVo.getUserVo();

		        String countColumn =  " SELECT COUNT(*) cnt";
		        String selectColumns = " SELECT pom.PROJECT_OWNER_MAPPING_ID, pt.PROJECT_TYPE, pom.PROJECT_ID, pom.PROJECT_NAME, " +
		        		"pom.PROJECT_CODE, pom.PM_NAME, pom.PL_NAME, pom.PM_EMAIL_ID, pom.PL_EMAIL_ID, ps.STATUS_NAME, pom.STATUS_ID ";
		        
		        StringBuilder queryBuilder = new StringBuilder();
		        
		        queryBuilder.append(" FROM project_owner_mapping pom, project_type pt, project_status AS ps");
		        queryBuilder.append(" WHERE pom.PROJECT_TYPE_ID=pt.PROJECT_TYPE_ID");
		        queryBuilder.append(" AND pom.STATUS_ID=ps.STATUS_ID ");
		        queryBuilder.append(" AND pom.DEPT_ID="+userVo.getDeptId());
		        
		        ProjectOwnerSearchVo searchVo = (ProjectOwnerSearchVo) requestVo.getSeacrhVo();
		        
		        if (null!=searchVo.getProjectTypeId() && searchVo.getProjectTypeId() > 0) {
		        	queryBuilder.append(" AND pom.PROJECT_TYPE_ID=").append(searchVo.getProjectTypeId());
				}
				
				if (null!=searchVo.getProjectId() && !searchVo.getProjectId().equals("")) {
					queryBuilder.append(" AND pom.PROJECT_ID=").append(searchVo.getProjectId());
				}
				
				if (null!=searchVo.getProjectCode() && !searchVo.getProjectCode().equals("")) {
					queryBuilder.append(" AND pom.PROJECT_CODE=").append("'").append(searchVo.getProjectCode()).append("'");
				}
				
				if (null!=searchVo.getPmId() && !(searchVo.getPmId().equals(ConstantEnum.MINUS_ONE.getStringValue()))) {
					queryBuilder.append(" AND pom.PM_ID=").append(searchVo.getPmId());
				}
				
				if (null!=searchVo.getPlId() && !(searchVo.getPlId().equals(ConstantEnum.MINUS_ONE.getStringValue()))) {
					queryBuilder.append(" AND pom.PL_ID=").append(searchVo.getPlId());
				}
				
		        String query = countColumn + queryBuilder.toString();
		        SQLQuery sqlQuery = session.createSQLQuery(query);
		        
		        List result = sqlQuery.list();
		        if(result!=null && result.size()>0){
		        	pagingVo.setTotalNumberOfRows(((BigInteger)result.get(0)).intValue());
		        }
		        
		        queryBuilder.append(" ORDER BY pt.PROJECT_TYPE DESC, pom.PROJECT_NAME ASC");
				queryBuilder.append(" LIMIT ").append(pagingVo.getStartRow()).append(",").append(pagingVo.getMaxResult());
		        
				query = selectColumns + queryBuilder.toString();
		        sqlQuery = session.createSQLQuery(query);
				
		        List<ProjectOwnerMappingActionVo> serviceList = new ArrayList<ProjectOwnerMappingActionVo>();
		        result = sqlQuery.list();
		        if(result!=null && result.size()>0){
		        	for(int index = 0,size=result.size();index<size;index++){
			        	Object[] row = (Object[])result.get(index);
			        	
			        	ProjectOwnerMappingActionVo pom = new ProjectOwnerMappingActionVo();
			        	ProjectStatusActionVo statusActionVo = new ProjectStatusActionVo();
			        	
			        	pom.setProjectOwnerMappingId((Integer)row[0]);
			        	pom.setProjectType((String)row[1]);
			        	pom.setProjectId(String.valueOf((Integer)row[2]));
			        	pom.setProjectName((String)row[3]);
			        	pom.setProjectCode((String)row[4]);
			        	pom.setPmName((String)row[5]);
			        	pom.setPlName((String)row[6]);
			        	pom.setPmEmailId((String)row[7]);
			        	pom.setPlEmailId((String)row[8]);
			        	statusActionVo.setStatusName((String)row[9]);
			        	statusActionVo.setStatusId((Integer)row[10]);
			        	pom.setProjectStatusActionVo(statusActionVo);
			            serviceList.add(pom);
		        	}
		        }
		        else {
					reponseVo.setErrorMessage("No record found");
				}
		        session.close();
		        reponseVo.setPagingVo(pagingVo);
		        reponseVo.setResponseList(serviceList);
			
			return reponseVo;
		}  catch (Exception ex) {
			if(session!=null){
				session.close();
			}
			throw new CustomApplicationException(ex);
		}
	}
	
	

	/**
	 * 
	 * @Method : deleteProjectOwnerMapping
	 * @Description : This method is for deleting Project Owner Mapping from [project_owner_mapping] table.
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo deleteProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		try {
			/*Multiple record delete*/
			StringBuilder deleteQuery = new StringBuilder();
			deleteQuery.append("Delete ProjectOwnerMappingEntityVo where projectOwnerMappingId = ");
			String deleteParam[] = requestVo.getParamIds().split(",");
			int i = 0;
			HibernateTemplate hibTemplate = null;
			for (String id : deleteParam) {
				StringBuilder queryString = new StringBuilder();
				queryString.append(deleteQuery.toString());
				queryString.append(id);

				if (i == 0) {
					hibTemplate = super.deleteNestedTransaction(queryString.toString());
				} else {
					super.deleteNestedTransaction(queryString.toString(), hibTemplate);
				}
				i++;
			}
			
				return responseVo;
				
		} catch (CustomApplicationException e) {
			throw e;
		} catch (Exception ex) {
			throw new CustomApplicationException(ex);
		}
	}

	/**
	 * 
	 * @Method : getProjectOwnerMapping
	 * @Description : This method is used for getting project owner mapping detail for view.
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo getProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException{
		ResponseVo outVo = null;
		try {
			ProjectOwnerMappingActionVo projectOwnerMappingActionVo = (ProjectOwnerMappingActionVo) requestVo.getCommonVo();
			ProjectOwnerMappingEntityVo projectOwnerMappingEntityVo = new ProjectOwnerMappingEntityVo();
			/* invoke a Common Dao */
			projectOwnerMappingEntityVo = (ProjectOwnerMappingEntityVo) super.findValueInt(projectOwnerMappingEntityVo,
					projectOwnerMappingActionVo.getProjectOwnerMappingId());
			/* copy properties */
			outVo = new ResponseVo();
			ProjectOwnerMappingActionVo mappingActionVo = new ProjectOwnerMappingActionVo();
			BoCopyManager.copyProperties(projectOwnerMappingEntityVo,mappingActionVo);
			if(projectOwnerMappingEntityVo.getAssociatedFrom()!=null){
				mappingActionVo.setAssociatedFromStr(CommonUtils.convertdateTostring(projectOwnerMappingEntityVo.getAssociatedFrom(), "dd/MM/yyyy"));
			}
			ProjectStatusActionVo projectStatusActionVo = new ProjectStatusActionVo();
			ProjectStatusEntityVo projectStatusEntityVo = projectOwnerMappingEntityVo.getProjectStatus();
			projectStatusActionVo.setStatusId(projectStatusEntityVo.getStatusId());
			
			// Customer mail flag
			if (null != mappingActionVo.getStatusMail() && mappingActionVo.getStatusMail() == '1') {
				mappingActionVo.setStatusMail('t');
			}
			if (null != mappingActionVo.getStatusMail() && mappingActionVo.getStatusMail() == '0') {
				mappingActionVo.setStatusMail(null);
			}
			if (null != mappingActionVo.getConsolidatedMail() && mappingActionVo.getConsolidatedMail() == '1') {
				mappingActionVo.setConsolidatedMail('t');
			}
			if (null != mappingActionVo.getConsolidatedMail() && mappingActionVo.getConsolidatedMail() == '0') {
				mappingActionVo.setConsolidatedMail(null);
			}
			
			mappingActionVo.setProjectStatusActionVo(projectStatusActionVo);
					   
			outVo.setCommonVo(mappingActionVo);
		    	
			return outVo;
		} catch (CustomApplicationException e) {
			throw e;
		} catch (Exception ex) {
			throw new CustomApplicationException(ex);
		}
	}

	public ResponseVo viewProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException{
		List comboValueList = null;
		ResponseVo responseVo = new ResponseVo();
		try {
			ProjectOwnerMappingActionVo projectOwnerMappingActionVo = (ProjectOwnerMappingActionVo)requestVo.getCommonVo();
		    List<String> paramNames = new ArrayList<String>();
		    List<Object> paramValues = new ArrayList<Object>();
		    
		    paramNames.add("projectOwnerMappingId");
		    paramValues.add(projectOwnerMappingActionVo.getProjectOwnerMappingId());
		    
		    comboValueList = super.findComboValuesByParam("ViewProjectOwnerMapping",
			    (String[]) (String[]) paramNames.toArray(new String[0]),
			    paramValues.toArray());
	
		    for (Object object : comboValueList) {
		    	
		    	//pom.projectOwnerMappingId,pom.projectStatus.statusId,pom.projectStatus.statusName,pom.projectTypeId,
		    	//pt.projectType,pom.projectId,pom.projectCode,
		    	//pom.projectName,pom.pmId,pom.pmName,pom.pmEmailId,pom.plId,pom.plName,pom.plEmailId,pom.deptId
		    	projectOwnerMappingActionVo.setProjectOwnerMappingId((Integer)(((Object[]) object)[0]));
		    	ProjectStatusActionVo projectStatusActionVo = new ProjectStatusActionVo();
		    	projectStatusActionVo.setStatusId((Integer)(((Object[]) object)[1]));
		    	projectStatusActionVo.setStatusName((String)(((Object[]) object)[2]));
		    	projectOwnerMappingActionVo.setProjectStatusActionVo(projectStatusActionVo);
		    	projectOwnerMappingActionVo.setProjectTypeId((Integer)(((Object[]) object)[3]));
		    	
		    	projectOwnerMappingActionVo.setProjectType((String)(((Object[]) object)[4]));
		    	projectOwnerMappingActionVo.setProjectId((String)(((Object[]) object)[5]));
		    	projectOwnerMappingActionVo.setProjectCode((String)(((Object[]) object)[6]));
		    	
		    	projectOwnerMappingActionVo.setProjectName((String)(((Object[]) object)[7]));
		    	projectOwnerMappingActionVo.setPmId((String)(((Object[]) object)[8]));
		    	projectOwnerMappingActionVo.setPmName((String)(((Object[]) object)[9]));
		    	projectOwnerMappingActionVo.setPmEmailId((String)(((Object[]) object)[10]));
		    	projectOwnerMappingActionVo.setPlId((String)(((Object[]) object)[11]));
		    	projectOwnerMappingActionVo.setPlName((String)(((Object[]) object)[12]));
		    	projectOwnerMappingActionVo.setPlEmailId((String)(((Object[]) object)[13]));
		    	projectOwnerMappingActionVo.setDeptId((String)(((Object[]) object)[14]));
		    	projectOwnerMappingActionVo.setAssociatedProjectId((String)(((Object[]) object)[15]));
		    	
		    	// Added Customer Detail
		    	projectOwnerMappingActionVo.setCustomerName((String)(((Object[]) object)[16]));
		    	projectOwnerMappingActionVo.setCustomerEmailId((String)(((Object[]) object)[17]));
		    	projectOwnerMappingActionVo.setStatusMail((Character)(((Object[]) object)[18]));
		    	projectOwnerMappingActionVo.setConsolidatedMail((Character)(((Object[]) object)[19]));
		    	
		    	if (null != projectOwnerMappingActionVo.getStatusMail() && projectOwnerMappingActionVo.getStatusMail() == '1') {
					projectOwnerMappingActionVo.setStatusMail('t');
				} else {
					projectOwnerMappingActionVo.setStatusMail(null);
				}
		    	if (null != projectOwnerMappingActionVo.getConsolidatedMail() && projectOwnerMappingActionVo.getConsolidatedMail() == '1') {
					projectOwnerMappingActionVo.setConsolidatedMail('1');
				} else {
					projectOwnerMappingActionVo.setConsolidatedMail(null);
				}
		    	// Added Customer Detail
		    	
		    	projectOwnerMappingActionVo.setAssociatedProject((String)(((Object[]) object)[20]));
		    	projectOwnerMappingActionVo.setAssociatedFrom((Date)(((Object[]) object)[21]));
		    	if(projectOwnerMappingActionVo.getAssociatedFrom()!=null){
		    		projectOwnerMappingActionVo.setAssociatedFromStr(CommonUtils.convertdateTostring(projectOwnerMappingActionVo.getAssociatedFrom(), "dd/MM/yyyy"));
		    	}
		    }
		    
		    responseVo.setCommonVo(projectOwnerMappingActionVo);
		} catch (CustomApplicationException e) {
		    e.printStackTrace();
		}
		return responseVo;
	}
	
	/**
	 * 
	 * @Method : updateProjectOwnerMapping
	 * @Description :  This method is used for updating existing project maping.
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo updateProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException{
		ResponseVo responseVo = new ResponseVo();
		ProjectOwnerMappingEntityVo projectOwnerMappingEntityVo = new ProjectOwnerMappingEntityVo();
		try {
		    
			/* Copy Properties */
		    ProjectOwnerMappingActionVo ownerActionVo = (ProjectOwnerMappingActionVo) requestVo.getCommonVo();
		    
		    ProjectStatusActionVo statusActionVo =  ownerActionVo.getProjectStatusActionVo();
		    ProjectStatusEntityVo statusEntityVo = new ProjectStatusEntityVo();
		    
		    BoCopyManager.copyProperties(statusActionVo, statusEntityVo);
		    
			BoCopyManager.copyProperties(ownerActionVo, projectOwnerMappingEntityVo);
			projectOwnerMappingEntityVo.setProjectStatus(statusEntityVo);
			
			if(projectOwnerMappingEntityVo.getProjectTypeId()==2){
				projectOwnerMappingEntityVo.setProjectCode(projectOwnerMappingEntityVo.getProjectId());
			}
			else{
				projectOwnerMappingEntityVo.setAssociatedProjectId(null);
				projectOwnerMappingEntityVo.setAssociatedFrom(null);
			}
			/* invoke a Common Dao */
			super.update(projectOwnerMappingEntityVo);
			return responseVo;
		} catch (CustomApplicationException e) {
			throw e;
		} catch (Exception ex) {
			throw new CustomApplicationException(ex);
		}
	}
	
	/**
	 * 
	 * @Method : getProjectCombo
	 * @Description : Method to load the Project
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo getProjectCombo(RequestVo requestVo) throws CustomApplicationException{
		ResponseVo responseVo = new ResponseVo();
		List comboValueList = null;
		List<ComboVo> projectList = new ArrayList<ComboVo>();
		
		if("1".equals(requestVo.getParamIds())){
			comboValueList = super.findComboValues("ProjectIdCombo");
			if(comboValueList!=null){
			   for(Object object:comboValueList){
				    ComboVo comboVo = new ComboVo();
					comboVo.setIdValue((Integer) ((Object[]) object)[0]);
					comboVo.setName((String) ((Object[]) object)[1] +"  :  "+(String) ((Object[]) object)[2]);
					comboVo.setKey((String) ((Object[]) object)[1]);
					comboVo.setRefId((String) ((Object[]) object)[3]);
					projectList.add(comboVo);
				}
			}
		}
		else{
			List<String> paramNames = new ArrayList<String>();
       	 	List<Object> paramValues = new ArrayList<Object>();
            paramNames.add("projectTypeId");
            paramValues.add(Integer.parseInt(requestVo.getParamIds()));
            comboValueList = super.findComboValuesByParam("PracticeTeamCombo", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());
            
    		if(comboValueList!=null){
    		   for(Object object:comboValueList){
    			   
    			   String code = (String) ((Object[]) object)[1];
   			    ComboVo comboVo = new ComboVo();
   				comboVo.setIdValue(Integer.parseInt(((Object[]) object)[0].toString()));
   				comboVo.setName(code +"  :  "+(String) ((Object[]) object)[2]);
   				comboVo.setKey(code);
   				comboVo.setRefId((String) ((Object[]) object)[3]);
   				projectList.add(comboVo);
   				
    			}
    		}
		}
		responseVo.setComboValueList(projectList);
		return responseVo;
	}

	
	
	
	/**
	 * 
	 * @Method : getDepartmentProjectCombo
	 * @Description : Method to load the Project Combo
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo getDepartmentProjectCombo(RequestVo requestVo) throws CustomApplicationException{
		ResponseVo responseVo = new ResponseVo();
		List comboValueList = null;
		List<ComboVo> projectList = new ArrayList<ComboVo>();
		UserVo userVo = requestVo.getUserVo();
		if("1".equals(requestVo.getParamIds())){
			
			List<String> paramNames = new ArrayList<String>();
       	 	List<Object> paramValues = new ArrayList<Object>();
            paramNames.add("empId");
            paramValues.add(Integer.parseInt(userVo.getEmployeeId()));
            comboValueList = super.findComboValuesByParam("DepartmentProjectCombo", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());
            if(comboValueList!=null){
			   for(Object object:comboValueList){
				    ComboVo comboVo = new ComboVo();
					comboVo.setIdValue((Integer) ((Object[]) object)[0]);
					comboVo.setName((String) ((Object[]) object)[1] +"  :  "+(String) ((Object[]) object)[2]);
					comboVo.setKey((String) ((Object[]) object)[1]);
					comboVo.setRefId((String) ((Object[]) object)[3]);
					projectList.add(comboVo);
				}
			}
		}
		else{
			List<String> paramNames = new ArrayList<String>();
       	 	List<Object> paramValues = new ArrayList<Object>();
            paramNames.add("projectTypeId");
            paramValues.add(Integer.parseInt(requestVo.getParamIds()));
            comboValueList = super.findComboValuesByParam("PracticeTeamCombo", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());
            
    		if(comboValueList!=null){
    		   for(Object object:comboValueList){
    			    /*String code = (String) ((Object[]) object)[1];
    			    if(code==null){
    			    	code = String.valueOf((Integer) ((Object[]) object)[2]);
    			    }*/
    			    String code = (String) ((Object[]) object)[1];
    			    ComboVo comboVo = new ComboVo();
    				comboVo.setIdValue(Integer.parseInt(((Object[]) object)[0].toString()));
    				comboVo.setName( code +"  :  "+(String) ((Object[]) object)[2]);
    				comboVo.setKey(code);
    				comboVo.setRefId((String) ((Object[]) object)[3]);
    				projectList.add(comboVo);
    			}
    		}
		}
		responseVo.setComboValueList(projectList);
		return responseVo;
	}
	
    /**
     * @Method : getProjectStatusCombo
     * @Description : Method to load the Project Status Combo
     * @param comboName
     * @return
     * @throws CustomApplicationException
     */
    private ResponseVo getProjectStatusCombo(String comboName) throws CustomApplicationException {
	List<ComboVo> projectList = new ArrayList<ComboVo>();
	List comboValueList = super.findComboValues(comboName);
	
	ResponseVo responseVo = new ResponseVo();
	
	if (comboValueList != null) {
	    for (Object object : comboValueList) {
		ComboVo comboVo = new ComboVo();
		comboVo.setIdValue((Integer) (((Object[]) object)[0]));
		comboVo.setName((String) ((Object[]) object)[1]);
		comboVo.setKey(((Integer) ((Object[]) object)[0]).toString());
		projectList.add(comboVo);
	    }
	}
	responseVo.setComboValueList(projectList);
	return responseVo;
    }
    /**
     * @Method : getEmployeeNameList
     * @Description : Method to load the Employee Name Combo Values
     * @param requestVo
     * @return ResponseVo
     */
    @SuppressWarnings("rawtypes")
    private ResponseVo getPMListForLeave(RequestVo requestVo) {
		List comboValueList = null;
		ResponseVo responseVo = new ResponseVo();
		try {
		    UserVo userVo = requestVo.getUserVo();
		    String empId = userVo.getEmployeeId();
	
		    List<String> paramNames = new ArrayList<String>();
		    List<Object> paramValues = new ArrayList<Object>();
		    
		    paramNames.add("empId");
		    paramValues.add(Integer.parseInt(userVo.getEmployeeId()));
		    comboValueList = super.findComboValuesByParam("PMListForLeave",
			    paramNames.toArray(new String[0]),
			    paramValues.toArray());
	
		    ComboVo comboVo;
		    List<ComboVo> comboValuelst = new ArrayList<ComboVo>();
		    
		    for (Object object : comboValueList) {
				comboVo = new ComboVo();
				comboVo.setIdValue((Integer)(((Object[]) object)[0]));
			    comboVo.setKey((Integer) ((Object[]) object)[0]+"-"+(String) ((Object[]) object)[1]+"-"+(String) ((Object[]) object)[3]);
			    comboVo.setName((String) ((Object[]) object)[1]);
				comboValuelst.add(comboVo);
		    }
		    
		    comboValueList = super.findComboValuesByParam("SBUListForLeave", new String[]{"userId"}, new Object[]{empId.toString()});
		    for (Object object : comboValueList) {
				comboVo = new ComboVo();
				comboVo.setIdValue((Integer)(((Object[]) object)[0]));
			    comboVo.setKey((Integer) ((Object[]) object)[0]+"-"+(String) ((Object[]) object)[1]+"-"+(String) ((Object[]) object)[3]);
			    comboVo.setName((String) ((Object[]) object)[1]);
				comboValuelst.add(comboVo);
		    }
		    responseVo.setComboValueList(comboValuelst);
		} catch (CustomApplicationException e) {
		    e.printStackTrace();
		}
		return responseVo;
    }
    
    /**
     * @Method : getEmployeeNameList
     * @Description : Method to load the Employee Name Combo Values
     * @param requestVo
     * @return ResponseVo
     */
    @SuppressWarnings("rawtypes")
    private ResponseVo getEmployeeNameList(RequestVo requestVo) {
		List comboValueList = null;
		ResponseVo responseVo = new ResponseVo();
		try {
		    UserVo userVo = requestVo.getUserVo();
	
		    List<String> paramNames = new ArrayList<String>();
		    List<Object> paramValues = new ArrayList<Object>();
		    
		    paramNames.add("deptId");
		    paramValues.add(userVo.getDeptId());
		    comboValueList = super.findComboValuesByParam("EmployeeCombo",
			    (String[]) (String[]) paramNames.toArray(new String[0]),
			    paramValues.toArray());
	
		    ComboVo comboVo;
		    List<ComboVo> comboValuelst = new ArrayList<ComboVo>();
		    
		    for (Object object : comboValueList) {
				comboVo = new ComboVo();
				
				comboVo.setIdValue((Integer)(((Object[]) object)[0]));
			    comboVo.setKey((Integer) ((Object[]) object)[0]+"-"+(String) ((Object[]) object)[1]+"-"+(String) ((Object[]) object)[3]);
			    comboVo.setName((String) ((Object[]) object)[1]);
				
				comboValuelst.add(comboVo);
		    }
		    
		    responseVo.setComboValueList(comboValuelst);
		} catch (CustomApplicationException e) {
		    e.printStackTrace();
		}
		return responseVo;
    }
    
    /**
     * @Method : getProjectTypes
     * @Description : Method to load the Project Type Combo
     * @param requestVo
     * @return
     */
    private ResponseVo getProjectTypes(RequestVo requestVo){
	List comboValueList = null;
	ResponseVo responseVo = new ResponseVo();
	try {
		comboValueList = super.findComboValues("ProjectType");
		ProjectTypeActionVo projectTypeActionVo;
		List<ProjectTypeActionVo> comboValuelst = new ArrayList<ProjectTypeActionVo>();
		for (Object object : comboValueList) {
			projectTypeActionVo = new ProjectTypeActionVo();
			projectTypeActionVo.setProjectTypeId((Integer) ((Object[]) object)[0]);
			projectTypeActionVo.setProjectType((String) ((Object[]) object)[1]);
			comboValuelst.add(projectTypeActionVo);
		}
		responseVo.setResponseList(comboValuelst);
		
	} catch (CustomApplicationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return responseVo;
}
    /**
     * @Method : getAllProject
     * @Description : Method to load all the Project to associate with Practice team
     * @param requestVo
     * @return ResponseVo
     * @throws CustomApplicationException
     */
    @SuppressWarnings("rawtypes")
    private ResponseVo getAllProject(RequestVo requestVo) throws CustomApplicationException{
	
	try {
	    UserVo userVo = requestVo.getUserVo();

	    List<String> paramNames = new ArrayList<String>();
	    List<Object> paramValues = new ArrayList<Object>();

	    paramNames.add("deptId");
	    paramValues.add(userVo.getDeptId());

	    ResponseVo responseVo = new ResponseVo();
	    List<ComboVo> projectList = new ArrayList<ComboVo>();
	    List comboValueList = super.findComboValuesByParam("projectAssociate",(String[]) (String[]) paramNames.toArray(new String[0]), paramValues.toArray());
	    if (comboValueList != null) {
		for (Object object : comboValueList) {
		    ComboVo comboVo = new ComboVo();
		    comboVo.setIdValue(Integer.parseInt(((Object[]) object)[0]
			    .toString()));
		    comboVo.setKey((String) ((Object[]) object)[1]);
		    comboVo.setName((String) ((Object[]) object)[2]);
		    comboVo.setRefId((String) ((Object[]) object)[3]);
		    comboVo.setCommonName(((Object[]) object)[4].toString());
		    projectList.add(comboVo);
		}
	    }
	    responseVo.setResponseList(projectList);
	    return responseVo;
	} catch (Exception e) {
	    log.error(e);
	    throw new CustomApplicationException(e);
	}

    }


}
