/**
/**
 * Creater / balamurugankSRM copyright information to Shirai 2013
 * 
 */
package com.task.dao.custom;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.task.common.ComboVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.UserVo;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.LoggingAspectInterceptor;
import com.task.vo.custom.action.ProjectTypeActionVo;
import com.task.vo.custom.action.ReportActionVo;
import com.task.vo.custom.action.TaskActionVo;

/**
 * @author balamurugank
 * 
 */
public class ReportDao extends CommonDao {

    private static Logger log = LoggingAspectInterceptor.getLog(ReportDao.class);
    
    @SuppressWarnings("unchecked")
    public ResponseVo reportList(RequestVo requestVo)
	    throws CustomApplicationException {
	StringBuffer queryBuilder = new StringBuffer();  
	queryBuilder.append("SELECT t.TASK_ID, e.Code, e.Name, t.TASK_DATE, " +
	    " IF(pt.PROJECT_TYPE_ID=0,'',PROJECT_TYPE) AS PROJECT_TYPE, " +	   
	    " IFNULL((SELECT distinct pom.PROJECT_CODE FROM project_owner_mapping pom WHERE pom.PROJECT_ID = t.PROJECT_ID AND pom.PROJECT_TYPE_ID=t.PROJECT_TYPE_ID),'') AS PROJECT_CODE,"+
	    " IFNULL((SELECT distinct pom.PROJECT_NAME FROM project_owner_mapping pom WHERE pom.PROJECT_ID = t.PROJECT_ID AND pom.PROJECT_TYPE_ID=t.PROJECT_TYPE_ID),'') AS PROJECT_NAME,"+
	    " IFNULL((SELECT NAME FROM employee e WHERE e.Id = t.REPORTEMP_ID),'') AS MANAGER, " +
	    " IFNULL(PLAN_TASK_DESC,'') AS PLAN_TASK_DESC," +
	    " IFNULL(PLAN_START_TIME,'') AS PLAN_START_TIME, " +
	    " IFNULL(PLAN_END_TIME,'') AS PLAN_END_TIME, " +
	    " IFNULL(ACTUAL_TASK_DESC,'') AS ACTUAL_TASK_DESC, " +
	    " IFNULL(ACTUAL_START_TIME,'') AS ACTUAL_START_TIME, " +
	    " IFNULL(ACTUAL_END_TIME,'') AS ACTUAL_END_TIME, " +
	    " IFNULL(ACTIVITY,'') AS ACTIVITY, " +
	    " IFNULL(WORK_ITEM,'') AS WORK_ITEM " +
	    " FROM task t,employee e,project_type pt " +
	    " WHERE t.EMPLOYEE_ID = e.id " +
	    " AND (IFNULL(t.PROJECT_TYPE_ID,0) = pt.PROJECT_TYPE_ID) ");
	
	
	HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
	SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
	Session session = sessionFactory.openSession();
	ReportActionVo reportActionVo = (ReportActionVo) requestVo.getSeacrhVo();
	
	UserVo userVo = requestVo.getUserVo();
	
	List<ReportActionVo> reportDataList = new ArrayList<ReportActionVo>();

	ResponseVo responseVo = null;
	try {
	    
	    queryBuilder.append(" AND e.Dept_Id = '" + userVo.getDeptId() +"' ");
	    
	    if (null != reportActionVo.getCode()) {
		queryBuilder.append(" AND t.EMPLOYEE_ID = '"+ reportActionVo.getCode() + "'");
	    }
	    if (null != reportActionVo.getREPORTEMP_ID()) {
		queryBuilder.append(" AND t.REPORTEMP_ID = '" + reportActionVo.getREPORTEMP_ID() + "'");
	    }
	    if (null != reportActionVo.getPROJECT_ID()) {
		queryBuilder.append(" AND t.PROJECT_TYPE_ID = '" + reportActionVo.getPROJECT_TYPE_ID() + "'");
		queryBuilder.append(" AND t.PROJECT_ID = '" + reportActionVo.getPROJECT_ID() + "'");
	    }
	    if (null!=reportActionVo.getTaskStartDate() && reportActionVo.getTaskStartDate().length()>0) {
		queryBuilder.append(" AND t.TASK_DATE >= '" + reportActionVo.getTaskStartDate().replaceAll("/", "-") + "'");
	    }
	    if (null != reportActionVo.getTaskEndDate() && reportActionVo.getTaskEndDate().length()>0) {
		queryBuilder.append(" AND t.TASK_DATE <= '" + reportActionVo.getTaskEndDate().replaceAll("/", "-") + "'");
	    }

	    queryBuilder.append(" ORDER BY t.TASK_DATE, ACTUAL_START_TIME, e.Name");
	    
	    Query sqlQuery = session.createSQLQuery(queryBuilder.toString())
		    .setResultTransformer(Transformers.aliasToBean(ReportActionVo.class));
	    reportDataList = sqlQuery.list();
	    responseVo = new ResponseVo();
	    responseVo.setResponseList(reportDataList);
	    return responseVo;
	} catch (Exception e) {
	    log.error("Exception " + e);
	    throw new CustomApplicationException(e);
	} finally {
	    session.close();
	}

    }
    
    public ResponseVo empEffortReport(RequestVo requestVo) throws CustomApplicationException {
    	try {
    		ResponseVo responseVo = new ResponseVo();
    		ReportActionVo reportActionVo = (ReportActionVo)requestVo.getSeacrhVo();
    		String empid           = reportActionVo.getCode();
        	String taskStartDate   = reportActionVo.getTaskStartDate();
    	    String taskEndDate     = reportActionVo.getTaskEndDate();
    	    Integer projectTypeId1 = reportActionVo.getPROJECT_TYPE_ID();
    	    String projectCode1    = reportActionVo.getPROJECT_CODE();
    	    boolean overall        = reportActionVo.isOverall();
    	    
    	    if(taskStartDate!=null){
    	    	taskStartDate = CommonUtils.convertDateFormat(taskStartDate, "dd/MM/yy", "yyyy-MM-dd");
    	    }
    	    if(taskEndDate!=null){
    	    	taskEndDate = CommonUtils.convertDateFormat(taskEndDate, "dd/MM/yy", "yyyy-MM-dd");
    	    }
    		String whereAnd = " where ";
	    	StringBuilder qryBuilder = new StringBuilder();
	    	qryBuilder.append(" Select distinct (task.projectType.projectTypeId || '-' || task.projectId) as prj,");
	    	qryBuilder.append(" task.projectType.projectTypeId, task.projectType.projectType, task.projectId,");
	    	qryBuilder.append(" (Select pom.projectCode from ProjectOwnerMappingEntityVo pom where task.projectType.projectTypeId=pom.projectTypeId and task.projectId=pom.projectId) as projectCode,");
	    	qryBuilder.append(" (Select pom.projectName from ProjectOwnerMappingEntityVo pom where task.projectType.projectTypeId=pom.projectTypeId and task.projectId=pom.projectId) as projectName");
	    	qryBuilder.append(" from TaskEntityVo task");
	    	qryBuilder.append(setEmpEffortReportCondition(requestVo,whereAnd));
	    	qryBuilder.append(" order by task.projectType.projectTypeId");
	    	String prjQuery = qryBuilder.toString();
	    	List result = super.find(prjQuery);
	    	Map<String,List<String>> projectList = new LinkedHashMap<String,List<String>>();

	    	if(result!=null && result.size()>0){
	    		
	    		int totalWorkingDays = getTotalWorkingDays(taskStartDate,taskEndDate);
	    		
	    		StringBuilder queryBuilder = new StringBuilder();
	    		queryBuilder.append("Select emp.name, emp.code, round('"+totalWorkingDays+"',0) as twd");
	    		whereAnd = " and ";
	    		queryBuilder.append(",(select round(count(distinct task.taskDate),0) from TaskEntityVo task where emp.id=task.employeeId "+setEmpEffortReportCondition(requestVo,whereAnd)+") as awd");
	    		queryBuilder.append(",(select round(sum(timediffmin(task.actualEndTime,task.actualStartTime))/60,2) from TaskEntityVo task where emp.id=task.employeeId "+setEmpEffortReportCondition(requestVo,whereAnd)+") as te");
	    		String othersQuery = "";
	    		boolean othersExist = false;
	    		for(int index = 0,size=result.size();index<size;index++){
	    		  Object[] row = (Object[])result.get(index);
	    		  int projectTypeId     = (Integer)row[1];
	    		  String projectType    = (String)row[2];
	    		  String projectId      = (String)row[3];
	    		  String projectName    = (String)row[5];
	    		  if(projectTypeId!=0 && projectId!=null && projectId.length()>0){
	    			  List<String> prjList = projectList.get(projectType);
		    		  if(prjList==null){
		    			  prjList = new ArrayList<String>();
		    			  projectList.put(projectType, prjList);
		    		  }
		    		  prjList.add(projectName);
	    		  }
	    		  else{
	    			  othersExist = true;
	    		  }
	    		  if(projectTypeId!=0 && projectId!=null && projectId.length()>0){
	    			  queryBuilder.append(",(select round(sum(timediffmin(task.actualEndTime,task.actualStartTime))/60,2) from TaskEntityVo task");
	    			  queryBuilder.append(" where emp.id=task.employeeId and task.projectType.projectTypeId='"+projectTypeId+"' and task.projectId="+projectId);
	    			  queryBuilder.append(setEmpEffortReportCondition(requestVo,whereAnd)+") as te_"+projectId);
	    		  }
	    		  else{
	    			  othersQuery =  ",(select round(sum(timediffmin(task.actualEndTime,task.actualStartTime))/60,2) from TaskEntityVo task";
	    			  othersQuery += " where emp.id=task.employeeId and (task.projectId is null or task.projectId='') "+setEmpEffortReportCondition(requestVo,whereAnd)+") as te_others";
	    		  }
	    	  }
	    	  if(othersExist){
	    		  List<String> prjList = projectList.get("Others");
	    		  if(prjList==null){
	    			  prjList = new ArrayList<String>();
	    			  projectList.put("Others", prjList);
	    		  }
	    		  prjList.add("Others");
	    	  }
	    	  queryBuilder.append(othersQuery); 
	    	  queryBuilder.append(" from com.task.vo.custom.entity.EmployeeEntityVo emp where emp.deptId in (100,108,112) and emp.status<>'3'");
	    	  if(empid!=null && !"0".equals(empid) && empid.length()>0){
	    		  queryBuilder.append(" and emp.id = '"+empid+"'");
	  	      }
	    	  else if(!overall && projectTypeId1!=null && projectTypeId1!=0 && projectCode1!=null && projectCode1.length()>0){
	    		  String empIds = getEmployeeList(requestVo);
	  	    	  queryBuilder.append(whereAnd+" emp.id in ("+empIds+")");
	  	    	  whereAnd = " and ";
	    	  }
	    	  else if(!overall){
	    		  String empIds = getEmployeeList(requestVo);
	  	    	  queryBuilder.append(whereAnd+" emp.id in ("+empIds+")");
	  	    	  whereAnd = " and ";
	  	      }
	    	  
	    	  queryBuilder.append(" order by emp.name");
	    	  String empEffortQry = queryBuilder.toString();
	    	  List effResult = super.find(empEffortQry);
	    	  responseVo.setResultList(effResult);
	    	  responseVo.setMapvalue(projectList);
	    	  responseVo.setReturnValue(getHeaderName(requestVo));
	    	}
	    	return responseVo;
    	} catch (CustomApplicationException e) {
        	log.error(e);
            throw e;
        } catch (Exception e) {
        	log.error(e);
            throw new CustomApplicationException(e);
        }
    }
    public String getHeaderName(RequestVo requestVo) throws CustomApplicationException{
    	
    	ReportActionVo reportActionVo = (ReportActionVo)requestVo.getSeacrhVo();
    	String taskStartDate   = reportActionVo.getTaskStartDate();
	    String taskEndDate     = reportActionVo.getTaskEndDate();
	    String empId           = reportActionVo.getCode();
	    Integer projectTypeId  = reportActionVo.getPROJECT_TYPE_ID();
	    String projectCode     = reportActionVo.getPROJECT_CODE();
	    boolean overall        = reportActionVo.isOverall();
	    String dateHeader = "";
	    if(taskStartDate!=null && taskStartDate.length()==10 && taskEndDate!=null && taskEndDate.length()==10){
	    	dateHeader = "["+taskStartDate+" - "+taskEndDate+"]";
	    }
    	String headerMsg = "";
    	String overAllHeader = "";
    	if(overall){
    		overAllHeader = "(OVERALL)";
    	}
	    if(empId!=null && !"0".equals(empId)){
	    	String empName = getEmployeeName(empId);
	    	headerMsg = "EMPLOYEE("+empName.toUpperCase()+") EFFORT MIS"+overAllHeader+" REPORT"+dateHeader;
	    }
	    else if(projectCode!=null && projectCode.length()>0){
	    	String projectNames = getProjectNames(projectCode);
	    	if(projectTypeId==1){
	    		headerMsg = "PROJECT("+projectNames.toUpperCase()+") EFFORT MIS"+overAllHeader+" REPORT"+dateHeader;
	    	}
	    	if(projectTypeId==2){
	    		headerMsg = "PRACTICE TEAM("+projectNames.toUpperCase()+") EFFORT MIS"+overAllHeader+" REPORT"+dateHeader;
	    	}
	    }
	    else if(projectTypeId==1){
	    	headerMsg = "PROJECT EFFORT MIS"+overAllHeader+" REPORT"+dateHeader;
	    }
	    else if(projectTypeId==2){
	    	headerMsg = "PRACTICE TEAM EFFORT MIS"+overAllHeader+" REPORT"+dateHeader;
	    }
	    else if(taskStartDate!=null && taskStartDate.length()==10 && taskEndDate!=null && taskEndDate.length()==10){
	    	headerMsg = "DURATION"+dateHeader+" - EFFORT MIS"+overAllHeader+" REPORT";
	    }
	    return headerMsg;
    }
    
    /**
     * Employee project wise effort report
     * @Method : empWorkDetailReport
     * @Description : This method is used to generate employee work detail report.
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo empWorkDetailReport(RequestVo requestVo) throws CustomApplicationException {
    	ResponseVo responseVo = new ResponseVo();
    	try {
    		ReportActionVo reportActionVo = (ReportActionVo)requestVo.getSeacrhVo();
    		String empid           = reportActionVo.getCode();
        	String taskStartDate   = reportActionVo.getTaskStartDate();
    	    String taskEndDate     = reportActionVo.getTaskEndDate();
    	    String projectId	   = reportActionVo.getPROJECT_ID();	
    	    
    	    if(taskStartDate!=null){
    	    	taskStartDate = CommonUtils.convertDateFormat(taskStartDate, "dd/MM/yy", "yyyy-MM-dd");
    	    }
    	    if(taskEndDate!=null){
    	    	taskEndDate = CommonUtils.convertDateFormat(taskEndDate, "dd/MM/yy", "yyyy-MM-dd");
    	    }
    	    
    	    //int totalWorkinghour = getTotalWorkingHour(taskStartDate,taskEndDate,empid,projectId);
    	    
	    	StringBuilder qryBuilder = new StringBuilder();
	    	qryBuilder.append(" SELECT ");
	    	qryBuilder.append(" DATE(t.startTime) AS task_date, ");
	    	qryBuilder.append(" TIME(t.startTime) AS start_time, ");
	    	qryBuilder.append(" TIME(t.endTime) AS end_time, ");
	    	qryBuilder.append(" CONCAT(mja.description,'-',mia.description) AS activity, ");
	    	qryBuilder.append(" t.detail, ");
	    	qryBuilder.append(" t.prjId, ");
	    	qryBuilder.append(" e.name, ");
	    	qryBuilder.append(" p.name ");
	    	
	    	qryBuilder.append(" FROM ");
	    	qryBuilder.append(" TimesheetEntityVo t, ActivityEntityVo a, MajoractivityEntityVo mja, MinoractivityEntityVo mia, EmployeeEntityVo e, ProjectEntityVo p ");
	    	
	    	qryBuilder.append(" WHERE ");
	    	qryBuilder.append("  a.id=t.actId ");
	    	qryBuilder.append(" AND a.majorId=mja.id  ");
	    	qryBuilder.append(" AND a.minorId=mia.id  ");
	    	qryBuilder.append(" AND e.id=t.empId  ");
	    	qryBuilder.append(" AND p.id=t.prjId  ");
	    	qryBuilder.append(" AND t.empId = '"+empid+"' ");
	    	qryBuilder.append(" AND t.prjId = '"+projectId+"' ");
	    	qryBuilder.append(" AND DATE(t.startTime)>='"+taskStartDate+"' " );
	    	qryBuilder.append(" AND DATE(t.endTime)<='"+taskEndDate+"' " );
	    	qryBuilder.append(" ORDER BY t.startTime ");
	    	
	    	String reportQuery = qryBuilder.toString();
	    	double  totalTime = 0;
	    	List result = super.find(reportQuery);
	    	
	    	List<TaskActionVo> taskActionVoList = new ArrayList<TaskActionVo>();
	    	for(Object object:result){
	    		
	    		TaskActionVo taskActionVo = new TaskActionVo();
	    		taskActionVo.setTaskDate((Date)((Object[])(Object[])object)[0]);
	    		taskActionVo.setActualStartTime(((Object[])(Object[])object)[1].toString());
	    		taskActionVo.setActualEndTime(((Object[])(Object[])object)[2].toString());
	    		taskActionVo.setActivity((String)((Object[])(Object[])object)[3]);
	    		taskActionVo.setWorkItem((String)((Object[])(Object[])object)[4]);
	    		taskActionVo.setEmpName(((Object[])(Object[])object)[6].toString());
	    		taskActionVo.setProjectName(((Object[])(Object[])object)[7].toString());
	    		
	    		totalTime = totalTime+(CommonUtils.convertStringToTimestamp(taskActionVo.getActualEndTime(), "HH:mm:ss") - CommonUtils.convertStringToTimestamp(taskActionVo.getActualStartTime(), "HH:mm:ss"));
	    		
	    		 taskActionVoList.add(taskActionVo);
	    	}
	    	
	    	responseVo.setTotalTime(totalTime/(3600.00*1000.00));
	    	responseVo.setResponseList(taskActionVoList);
    	} catch (CustomApplicationException e) {
        	log.error(e);
            throw e;
        } catch (Exception e) {
        	log.error(e);
            throw new CustomApplicationException(e);
        }
    	return responseVo;
    }
    
    /**
	 * @Method : getTotalWorkingHour
	 * @Description : This method is to get total working hour within given time range. 
	 * @param taskStartDate
	 * @param taskEndDate
	 * @param empid
	 * @param projectId
	 * @return
	 */
	private int getTotalWorkingHour(String taskStartDate, String taskEndDate, String empid,String projectId)throws CustomApplicationException {
		
		StringBuilder qryBuilder = new StringBuilder();
    	qryBuilder.append(" SELECT ");
    	qryBuilder.append(" TIMESTAMPDIFF(SECOND, t.startTime, t.endTime) AS total_second ");
    	qryBuilder.append(" FROM ");
    	qryBuilder.append(" TimesheetEntityVo t ");
    	qryBuilder.append(" WHERE ");
    	qryBuilder.append(" t.empId = '"+empid+"' ");
    	qryBuilder.append(" AND t.prjId = '"+projectId+"' ");
    	qryBuilder.append(" AND DATE(t.startTime)>='"+taskStartDate+"' " );
    	qryBuilder.append(" AND DATE(t.endTime)<='"+taskEndDate+"' " );
    	
    	String reportQuery = qryBuilder.toString();
    	
    	List result = super.find(reportQuery);
    	
		if (result != null && result.size() > 0) {
			for (int index = 0, size = result.size(); index < size; index++) {
				Object[] row = (Object[]) result.get(index);
				return (Integer) row[0];
			}
		}
		return 0;
	}

	public String getEmployeeList(RequestVo requestVo) throws CustomApplicationException{
    	
    	ReportActionVo reportActionVo = (ReportActionVo)requestVo.getSeacrhVo();
	    Integer projectTypeId = reportActionVo.getPROJECT_TYPE_ID();
	    String projectCode    = reportActionVo.getPROJECT_CODE();
	    boolean overall        = reportActionVo.isOverall();
	    String whereAnd = "where";
    	StringBuilder qryBuilder = new StringBuilder(); 
    	if(projectTypeId!=null && projectTypeId!=0 && projectCode!=null && projectCode.length()>0){
	    	String projectCode1 = projectCode.replace(",", "','");
	    	qryBuilder.append(whereAnd+" task.projectId in (Select pom.projectId from ProjectOwnerMappingEntityVo pom where pom.projectTypeId="+projectTypeId+" and pom.projectCode in ('"+projectCode1+"'))");
	    	whereAnd = " and ";
	    }
	    else if(projectTypeId!=null && projectTypeId!=0){
	    	qryBuilder.append(whereAnd+" (task.projectType.projectTypeId="+projectTypeId+")");
	    	whereAnd = " and ";
	    }
    	String query = qryBuilder.toString();
    	List result = super.find("select distinct task.employeeId from TaskEntityVo task "+query+setEmpEffortReportCondition(requestVo,whereAnd));
    	String empList = "";
    	if(result!=null && result.size()>0){
    		for(int index = 0,size=result.size();index<size;index++){
	    		String emp = (String)result.get(index);
	    		if(index==0){
	    			empList = "'"+emp+"'";
	    		}
	    		else{
	    			empList += "," +"'"+emp+"'";
	    		}
    		}
    	}
    	return empList;
    }
    public String getEmployeeName(String empId) throws CustomApplicationException{
    	String employeeNames = "";
    	List result = super.find("Select name from com.task.vo.custom.entity.EmployeeEntityVo e where id in ('"+empId.replace(",", "','")+"')");
    	if(result!=null && result.size()>0){
    		for(int index = 0,size=result.size();index<size;index++){
	    		if(index != 0){
	    			employeeNames +=",";
	    		}
	    		employeeNames += (String)result.get(index);
    		}
    	}
    	return employeeNames;
    }
    public String getProjectNames(String projectCodes) throws CustomApplicationException{
    	String projectNames = "";
    	List result = super.find("Select pom.projectName from ProjectOwnerMappingEntityVo pom where pom.projectCode in ('"+projectCodes.replace(",", "','")+"')");
    	if(result!=null && result.size()>0){
    		for(int index = 0,size=result.size();index<size;index++){
	    		if(index != 0){
	    			projectNames +=",";
	    		}
	    		projectNames += (String)result.get(index);
    		}
    	}
    	return projectNames;
    }
    public int getTotalWorkingDays(String taskStartDate,String taskEndDate) throws CustomApplicationException{
    	List result = super.find("Select workingdays('"+taskStartDate+"','"+taskEndDate+"') as a,count(holidayId)as cnt from HolidayEntityVo where holidayId=1");
    	if(result!=null && result.size()>0){
    		for(int index = 0,size=result.size();index<size;index++){
	    		Object[] row = (Object[])result.get(index);
	    		return (Integer)row[0];
    		}
    	}
    	return 0;
    }
    public String setEmpEffortReportCondition(RequestVo requestVo,String whereAnd){
    	StringBuilder qryBuilder = new StringBuilder();
    	ReportActionVo reportActionVo = (ReportActionVo)requestVo.getSeacrhVo();
    	String taskStartDate  = reportActionVo.getTaskStartDate();
	    String taskEndDate    = reportActionVo.getTaskEndDate();
	    String code           = reportActionVo.getCode();
	    Integer projectTypeId = reportActionVo.getPROJECT_TYPE_ID();
	    String projectCode    = reportActionVo.getPROJECT_CODE();
	    if(taskStartDate!=null){
	    	taskStartDate = CommonUtils.convertDateFormat(taskStartDate, "dd/MM/yy", "yyyy-MM-dd");
	    }
	    if(taskEndDate!=null){
	    	taskEndDate = CommonUtils.convertDateFormat(taskEndDate, "dd/MM/yy", "yyyy-MM-dd");
	    }
	    if(taskStartDate!=null && taskStartDate.length()==10){
	    	qryBuilder.append(whereAnd+" task.taskDate>='"+taskStartDate+"'");
	    	whereAnd = " and ";
	    }
	    if(taskStartDate!=null && taskStartDate.length()==10){
	    	qryBuilder.append(whereAnd+" task.taskDate<='"+taskEndDate+"'");
	    	whereAnd = " and ";
	    }
	    if(code!=null && !"0".equals(code) && code.length()>0){
	    	qryBuilder.append(whereAnd+" task.employeeId = '"+code+"'");
	    	whereAnd = " and ";
	    }
	    if(projectTypeId!=null && projectTypeId!=0 && projectCode!=null && projectCode.length()>0){
	    	String projectCode1 = projectCode.replace(",", "','");
	    	qryBuilder.append(" and (task.projectId in (Select pom.projectId from ProjectOwnerMappingEntityVo pom");
	    	qryBuilder.append(" where pom.projectTypeId="+projectTypeId+" and pom.projectCode in ('"+projectCode1+"')) or task.projectId='' or task.projectId is null)");
	    	whereAnd = " and ";
	    	
	    }
	    else if(projectTypeId!=null && projectTypeId!=0){
	    	qryBuilder.append(whereAnd+" (task.projectType.projectTypeId="+projectTypeId+" or task.projectType.projectTypeId is null or task.projectType.projectTypeId='')");
	    	whereAnd = " and ";
	    }
	    
	    qryBuilder.append(whereAnd+" task.actualStartTime is not null and task.actualStartTime<>'' and task.actualEndTime is not null and task.actualEndTime<>'' ");
	    whereAnd = " and ";
	    
	    return qryBuilder.toString();
    }
    public ResponseVo comboLoading(RequestVo requestVo) throws CustomApplicationException {
	ResponseVo responseVo = new ResponseVo();

	if (requestVo.getComboType().equals("allProject")) {
	    responseVo = getAllProject(requestVo);
	}
	if (requestVo.getComboType().equals("project")) {
	    responseVo = getProjectCombo(requestVo);
	} else if (requestVo.getComboType().equals("projectType")) {
	    responseVo = getProjectTypes(requestVo);
	} else if (requestVo.getComboType().equals("projectManager")) {
	    responseVo = getProjectManager(requestVo);
	} else if (requestVo.getComboType().equals("employee")) {
	    responseVo = getEmployeeNameList(requestVo);
	}
	
	return responseVo;
    }
    
    /**
     * @Method : getAllProject
     * @Description : Method to load all the Project
     * @param requestVo
     * @return ResponseVo
     * @throws CustomApplicationException
     */
    @SuppressWarnings("rawtypes")
    private ResponseVo getAllProject(RequestVo requestVo) throws CustomApplicationException{
	
	ResponseVo responseVo = new ResponseVo();
	List<ComboVo> projectList = new ArrayList<ComboVo>();
	List comboValueList = super.findComboValues("AllProject");
	if(comboValueList!=null){
	   for(Object object:comboValueList){
		    ComboVo comboVo = new ComboVo();
			comboVo.setIdValue(Integer.parseInt(((Object[]) object)[0].toString()));
			comboVo.setKey((String) ((Object[]) object)[1]);
			comboVo.setName((String) ((Object[]) object)[2]);
			comboVo.setRefId((String) ((Object[]) object)[3]);
			comboVo.setCommonName(((Object[]) object)[4].toString());
			projectList.add(comboVo);
		}
	}
	Map<String,List<ComboVo>> projects = orderProjectTypeWise(projectList);
	responseVo.setComboValueMap(projects);
	return responseVo;
    }
    
    

    /**
     * @Method : getProjectCombo
     * @Description : Method to load the Project Name Combo Values
     * @param requestVo
     * @return ResponseVo
     * @throws CustomApplicationException
     */
    @SuppressWarnings("rawtypes")
    private ResponseVo getProjectCombo(RequestVo requestVo)
	    throws CustomApplicationException {
	ResponseVo responseVo = new ResponseVo();
	List comboValueList = null;
	List<ComboVo> projectList = new ArrayList<ComboVo>();

	if ("1".equals(requestVo.getParamIds())) {
	    comboValueList = super.findComboValues("ProjectIdCombo");
	    if (comboValueList != null) {
		for (Object object : comboValueList) {
		    ComboVo comboVo = new ComboVo();
		    comboVo.setIdValue((Integer) ((Object[]) object)[0]);
		    comboVo.setName((String) ((Object[]) object)[1] + "  :  " + (String) ((Object[]) object)[2]);
		    comboVo.setKey((String) ((Object[]) object)[1]);
		    comboVo.setRefId((String) ((Object[]) object)[3]);
		    projectList.add(comboVo);
		}
	    }
	} else {
	    List<String> paramNames = new ArrayList<String>();
	    List<Object> paramValues = new ArrayList<Object>();
	    paramNames.add("projectTypeId");
	    paramValues.add(Integer.parseInt(requestVo.getParamIds()));
	    comboValueList = super.findComboValuesByParam("PracticeTeamCombo",
		    (String[]) (String[]) paramNames.toArray(new String[0]),
		    paramValues.toArray());

	    if (comboValueList != null) {
		for (Object object : comboValueList) {
		    String code = String.valueOf((Integer) ((Object[]) object)[2]);
		    ComboVo comboVo = new ComboVo();
		    comboVo.setIdValue((Integer) ((Object[]) object)[0]);
		    comboVo.setName(code + "  :  " + (String) ((Object[]) object)[3]);
		    comboVo.setKey(code);
		    comboVo.setRefId((String) ((Object[]) object)[4]);
		    projectList.add(comboVo);
		}
	    }
	}
	responseVo.setComboValueList(projectList);
	return responseVo;
    }
    
    /**
     * @Method : getProjectTypes
     * @Description : Method to load the Project Type Combo Values
     * @param requestVo
     * @return ResponseVo
     */
    @SuppressWarnings("rawtypes")
    private ResponseVo getProjectTypes(RequestVo requestVo){
	List comboValueList = null;
	ResponseVo responseVo = new ResponseVo();
	try {
		comboValueList = super.findComboValues("ProjectType");
		ProjectTypeActionVo projectTypeActionVo;
		List<ProjectTypeActionVo> comboValuelst = new ArrayList<ProjectTypeActionVo>();
		for (Object object : comboValueList) {
			projectTypeActionVo = new ProjectTypeActionVo();
			projectTypeActionVo.setProjectTypeId((Integer) ((Object[]) object)[0]);
			projectTypeActionVo.setProjectType((String) ((Object[]) object)[1]);
			comboValuelst.add(projectTypeActionVo);
		}
		responseVo.setResponseList(comboValuelst);
		
	} catch (CustomApplicationException e) {
		e.printStackTrace();
	}
	return responseVo;
}

    /**
     * @Method : getProjectManager
     * @Description : Method to load the Project Manager Combo Values
     * @param requestVo
     * @return ResponseVo
     */
    @SuppressWarnings("rawtypes")
    private ResponseVo getProjectManager(RequestVo requestVo) {
	List comboValueList = null;
	ResponseVo responseVo = new ResponseVo();
	try {
	    UserVo userVo = requestVo.getUserVo();

	    List<String> paramNames = new ArrayList<String>();
	    List<Object> paramValues = new ArrayList<Object>();
	    
	    paramNames.add("deptId");
	    paramValues.add(userVo.getDeptId());
	    
	    comboValueList = super.findComboValuesByParam(
		    "AllProjectManagerReport",
		    (String[]) (String[]) paramNames.toArray(new String[0]),
		    paramValues.toArray());

	    ComboVo comboVo;
	    List<ComboVo> comboValuelst = new ArrayList<ComboVo>();
	    
	    for (Object object : comboValueList) {
		comboVo = new ComboVo();
		comboVo.setIdValue((Integer) ((Object[]) object)[0]);
		comboVo.setName((String) ((Object[]) object)[1]);
		comboVo.setKey((String) ((Object[]) object)[2]);
		comboVo.setRefId((Integer) ((Object[]) object)[0] + ":"	+ (String) ((Object[]) object)[1]);
		comboVo.setCommonName((String) ((Object[]) object)[2] + ":" + ((Object[]) object)[1]);
		comboValuelst.add(comboVo);
	    }
	    
	    responseVo.setComboValueList(comboValuelst);
	} catch (CustomApplicationException e) {
	    log.error("Exception " + e);
	}
	return responseVo;
    }

    /**
     * @Method : getEmployeeNameList
     * @Description : Method to load the Employee Name Combo Values
     * @param requestVo
     * @return ResponseVo
     */
    @SuppressWarnings("rawtypes")
    private ResponseVo getEmployeeNameList(RequestVo requestVo) {
	List comboValueList = null;
	ResponseVo responseVo = new ResponseVo();
	try {
	    UserVo userVo = requestVo.getUserVo();

	    List<String> paramNames = new ArrayList<String>();
	    List<Object> paramValues = new ArrayList<Object>();
	    
	    paramNames.add("deptId");
	    paramValues.add(userVo.getDeptId());
	    
	    comboValueList = super.findComboValuesByParam("AllEmployeeReport",
		    (String[]) (String[]) paramNames.toArray(new String[0]),
		    paramValues.toArray());

	    ComboVo comboVo;
	    List<ComboVo> comboValuelst = new ArrayList<ComboVo>();
	    
	    for (Object object : comboValueList) {
		comboVo = new ComboVo();
		comboVo.setIdValue((Integer) ((Object[]) object)[0]);
		comboVo.setName((String) ((Object[]) object)[1]);
		comboVo.setKey((String) ((Object[]) object)[2]);
		comboVo.setRefId((Integer) ((Object[]) object)[0] + ":" + (String) ((Object[]) object)[1]);
		comboVo.setCommonName((String) ((Object[]) object)[3]);
		
		comboValuelst.add(comboVo);
	    }
	    
	    responseVo.setComboValueList(comboValuelst);
	} catch (CustomApplicationException e) {
	    log.error("Exception " + e);
	}
	return responseVo;
    }
    
    /**
     * @Method : orderProjectTypeWise
     * @Description : Method to ignore the Order the Project List
     * @param projectList
     * @return Map
     */
    private Map<String,List<ComboVo>> orderProjectTypeWise(List<ComboVo> projectList){
	Map<String,List<ComboVo>> projects = new HashMap<String,List<ComboVo>>();
	for (ComboVo comboVo : projectList) {
		String projectTypeId = comboVo.getCommonName();
		List<ComboVo> list = projects.get(projectTypeId);
		if(list == null){
			list = new ArrayList<ComboVo>();
			projects.put(projectTypeId, list);
		}
		list.add(comboVo);
	}
	return projects;
}

}
