/**
 * Creater / balamurugankSRM copyright information to Shirai 2013
 * 
 */
package com.task.dao.custom;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.task.common.ComboVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.UserVo;
import com.task.util.CommonUtils;
import com.task.util.CustomApplicationException;
import com.task.util.LoggingAspectInterceptor;
import com.task.vo.custom.action.AvailableResourceVo;
import com.task.vo.custom.action.ProjectOwnerMappingActionVo;
import com.task.vo.custom.entity.ModeEntityVo;
import com.task.vo.custom.entity.ResourceAllocationEntityVo;
import com.task.vo.custom.entity.RoleEntityVo;

/**
 * @author balamurugank
 * 
 */
public class ResourceAllocationDao extends CommonDao {

    private static Logger log = LoggingAspectInterceptor.getLog(ResourceAllocationDao.class);
    private UserVo userVo;
    
    /**
     * @Method : comboLoading
     * @Description : Method to load the Combo Values
     * @param requestVo
     * @return ResponseVo
     * @throws CustomApplicationException
     */
    public ResponseVo comboLoading(RequestVo requestVo)
	    throws CustomApplicationException {
	ResponseVo responseVo = new ResponseVo();
	userVo = requestVo.getUserVo();
	if (requestVo.getComboType().equals("RoleCombo")) {
	    responseVo = getComboValues("RoleCombo");
	}else if (requestVo.getComboType().equals("ModeCombo")) {
	    responseVo = getComboValues("ModeCombo");
	}else if (requestVo.getComboType().equals("ProjectNameCombo")) {
	    String[] params = new String[] {"employeeId"};
	    Object[] values = new Object[] {userVo.getEmployeeId()};
	    responseVo = getProjectCombo("ProjectNameCombo", params, values);
	}
	return responseVo;
    }

    
    /**
     * @Method : getProjectDetails
     * @Description : Method to get the Project Owner Details
     * @param queryName
     * @param paramValue
     * @return ResponseVo
     * @throws CustomApplicationException
     */
    public ResponseVo getProjectDetails(RequestVo requestVo) throws CustomApplicationException {
	   	ResponseVo responseVo = new ResponseVo();
	   	String[] params = new String[] {"projectId", "projectTypeId"};
	   	Object val[] = requestVo.getValues();
	   	Object[] values = new Object[]{val[1],Integer.parseInt((String)val[0])};
	   	ProjectOwnerMappingActionVo projectDetailsVo = new ProjectOwnerMappingActionVo();
	   	List queryValueList = super.findValuesByParam(requestVo.getComboType(), params, values);
	   	if (queryValueList != null) {
	   	    for (Object object : queryValueList) {
	   		
	   		projectDetailsVo.setPmId(((Object[]) object)[0] == null ?  "" : ((Object[]) object)[0].toString()); // null check before cast
	   		projectDetailsVo.setPmName(((Object[]) object)[1] == null ?  "" : ((Object[]) object)[1].toString());// null check before cast
	   		projectDetailsVo.setPlId(((Object[]) object)[2] == null ?  "" : ((Object[]) object)[2].toString());// null check before cast
	   		projectDetailsVo.setPlName(((Object[]) object)[3] == null ?  "" : ((Object[]) object)[3].toString());// null check before cast
	   		projectDetailsVo.setStartDate(((Object[]) object)[4] == null ? null : (Date)((Object[]) object)[4]);// null check before cast
	   		projectDetailsVo.setEndDate(((Object[]) object)[5] == null ? null : (Date)((Object[]) object)[5]);// null check before cast
	   	    }
	   	}
	   	getAvaliableResource(responseVo, requestVo);
	   	getAllocatedResource(responseVo, requestVo);
	   	responseVo.setCommonVo(projectDetailsVo);
	   	return responseVo;
    }
    
    /**
     * @Method : getAvaliableResource
     * @Description : Method to get the Available Resource
     * @param responseVo
     * @param requestVo
     * @throws CustomApplicationException
     */
    private void getAvaliableResource(ResponseVo responseVo, RequestVo requestVo) {
		try {
		    userVo = requestVo.getUserVo();
		    List<AvailableResourceVo> availableResourceList = new ArrayList<AvailableResourceVo>();
		    List resultList = super.findComboValues("AvailableResource");
		    if (resultList != null) {
				for (Object object : resultList) {
				    AvailableResourceVo resourceVo = new AvailableResourceVo();
				    resourceVo.setEmployeeId(Integer.parseInt(((Object[]) object)[0].toString()));
				    resourceVo.setEmployeeCode(((Object[]) object)[1].toString());
				    resourceVo.setEmployeeName(((Object[]) object)[2].toString());
				    if(((Object[]) object)[3]!=null && !"0".equals(((Object[]) object)[3].toString())){
				    	resourceVo.setMode(1);
				    }
				    availableResourceList.add(resourceVo);
				}
		    }
		    responseVo.setResultList(availableResourceList);
		} catch (Exception e) {
		    log.error(e);
		}
    }
    
    /**
     * @Method : getAllocatedResource
     * @Description : Method to get the Allocated resource for the Project
     * @param responseVo
     * @param requestVo
     * @throws CustomApplicationException
     */
    private void getAllocatedResource(ResponseVo responseVo, RequestVo requestVo) throws CustomApplicationException {
		try {
		    userVo = requestVo.getUserVo();
	
		    List<AvailableResourceVo> availableResourceList = new ArrayList<AvailableResourceVo>();
		    String[] params = new String[] { "projectId" };
		    Integer projectId = Integer.parseInt(requestVo.getValues()[1].toString());
		    Object[] values = new Object[] {projectId};
		    List resultList = super.findValuesByParam("AllocatedResource", params, values);
		    if (resultList != null) {
				for (Object object : resultList) {
				    AvailableResourceVo resourceVo = new AvailableResourceVo();
				    resourceVo.setEmployeeCode(((Object[]) object)[0].toString());
				    resourceVo.setEmployeeId(Integer.parseInt(((Object[]) object)[1].toString()));
				    resourceVo.setRole(Integer.parseInt(((Object[]) object)[2].toString()));
				    resourceVo.setMode(Integer.parseInt(((Object[]) object)[3].toString()));
				    resourceVo.setStartDate((Date) ((Object[]) object)[4]);
				    resourceVo.setEndDate((Date) ((Object[]) object)[5]);
				    resourceVo.setEmployeeName((String) ((Object[]) object)[6]);
				    availableResourceList.add(resourceVo);
				}
		    }
		    responseVo.setResponseList(availableResourceList);
		} catch (Exception e) {
		    log.error(e);
		}
    }
    

    /**
     * @Method : getComboValues
     * @Description : Method to load the Role & Mode Combo Values for the Employee
     * @return ResponseVo
     * @throws CustomApplicationException
     */
    private ResponseVo getComboValues(String comboName)
	    throws CustomApplicationException {

	ResponseVo responseVo = new ResponseVo();
	List<ComboVo> roleList = new ArrayList<ComboVo>();
	List comboValueList = super.findComboValues(comboName);
	if (comboValueList != null) {
	    for (Object object : comboValueList) {
		ComboVo comboVo = new ComboVo();
		comboVo.setIdValue(Integer.parseInt(((Object[]) object)[0].toString()));
		comboVo.setKey(((Object[]) object)[0].toString());
		comboVo.setName((String) ((Object[]) object)[1]);
		roleList.add(comboVo);
	    }
	}
	responseVo.setComboValueList(roleList);
	return responseVo;
    }
    
    /**
     * @Method : getProjectCombo
     * @Description : Method to load the Project Combo values based on given Parameters and Values
     * @param queryName
     * @param params
     * @param values
     * @return
     * @throws CustomApplicationException
     */
    @SuppressWarnings("rawtypes")
    private ResponseVo getProjectCombo(String queryName, String[] params, Object[] values) throws CustomApplicationException {
	try {
	ResponseVo responseVo = new ResponseVo();
	List<ComboVo> projectList = new ArrayList<ComboVo>();
	
	List queryValueList = super.findValuesByParam(queryName, params, values);
	if (queryValueList != null) {
	    for (Object object : queryValueList) {
		ComboVo comboVo = new ComboVo();
		comboVo.setIdValue(Integer.parseInt(((Object[]) object)[0].toString()));
		comboVo.setKey(((Object[]) object)[0].toString() +":"+ ((Object[]) object)[1].toString());
		comboVo.setName((String) ((Object[]) object)[2]);
		projectList.add(comboVo);
	    }
	}
	responseVo.setComboValueList(projectList);
	return responseVo;
	}catch(Exception e) {
	    log.error(e);
	    throw new CustomApplicationException(e);
	}
    }
    
    public ResponseVo saveResource(RequestVo requestVo) throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		try {
			UserVo userVo = (UserVo) requestVo.getUserVo();
			List<AvailableResourceVo> resourceList = (List<AvailableResourceVo>)requestVo.getRequestList();
			
			String paramsName[] = requestVo.getParamNames();
			String projectTypeId = paramsName[0];
			String projectId = paramsName[1];
			updateReleasedResource(resourceList,projectId, projectTypeId);
			setExistingResource(resourceList,projectId, projectTypeId);
			
			List<ResourceAllocationEntityVo> entityVoList = new ArrayList<ResourceAllocationEntityVo>();
			for (AvailableResourceVo resource : resourceList) {
				
			    RoleEntityVo role = new RoleEntityVo();
				role.setRoleId(resource.getRole());
			    
				ModeEntityVo mode = new ModeEntityVo();
				mode.setModeId(resource.getMode());
				
				ResourceAllocationEntityVo entityVo = new ResourceAllocationEntityVo();
				entityVo.setResourceAllocationId(resource.getResourceAllocationId());
				entityVo.setRole(role);
				entityVo.setMode(mode);
				entityVo.setProjectId(Integer.parseInt(projectId));
				entityVo.setProjectTypeId(Integer.parseInt(projectTypeId));
				entityVo.setEmployeeId(resource.getEmployeeId());
				entityVo.setStartDate(CommonUtils.convertStringToDate(resource.getStartDateStr(), "dd/MM/yyyy"));
				entityVo.setEndDate(CommonUtils.convertStringToDate(resource.getEndDateStr(), "dd/MM/yyyy"));
				if(resource.getCreatedOn()==null){
					entityVo.setCreatedOn(new Date());
					entityVo.setCreatedBy(Integer.parseInt(userVo.getEmployeeId()));
				}
				else{
					entityVo.setCreatedOn(resource.getCreatedOn());
					entityVo.setCreatedBy(resource.getCreatedBy());
				}
				entityVo.setResourceReleased(false);
				entityVoList.add(entityVo);
			}
			if(entityVoList!=null && entityVoList.size()>0){
				super.saveOrUpdateAll(entityVoList);
			}
		    return responseVo;
		}
		catch (CustomApplicationException e) {
		    log.error(e);
		    throw e;
		}
		catch (Exception e) {
		    log.error(e);
		    throw new CustomApplicationException(e);
		}
    }
    private void setExistingResource(List<AvailableResourceVo> resourceList,String projectId, String projectTypeId) throws CustomApplicationException{
    	StringBuilder qryBuilder = new StringBuilder("");
	    qryBuilder.append(" Select ra.resourceAllocationId,(select e.code from EmployeeEntityVo e where e.id=ra.employeeId) as code, createdOn, createdBy " +
	    			"from ResourceAllocationEntityVo ra where ra.projectId="+projectId+" and ra.resourceReleased='0' and ra.projectTypeId=" +projectTypeId);
	    String empCode = "";
		for (AvailableResourceVo resource : resourceList) {
			empCode += ",'"+ resource.getEmployeeCode()+"'";
		}
		if(empCode.length()>0){
			empCode = empCode.substring(1);
			
			qryBuilder.append(" and ra.employeeId in (select id from EmployeeEntityVo where code in ("+empCode+"))");
			List result = super.find(qryBuilder.toString());
			if (result != null) {
			    for (Object object : result) {
					int resourceAllocationId = Integer.parseInt(((Object[]) object)[0].toString());
					String employeeCode 	 = (String) ((Object[]) object)[1];
					Date createdOn 	         = (Date) ((Object[]) object)[2];
					int createdBy 	         = Integer.parseInt(((Object[]) object)[3].toString());
					for (AvailableResourceVo resource : resourceList) {
						if(employeeCode!=null && employeeCode.equals(resource.getEmployeeCode())){
							resource.setResourceAllocationId(resourceAllocationId);
							resource.setCreatedOn(createdOn);
							resource.setCreatedBy(createdBy);
						}
					}
			    }
			}
		}
    }
    private void updateReleasedResource(List<AvailableResourceVo> resourceList,String projectId, String projectTypeId) throws CustomApplicationException{
    	try{
	    	StringBuilder updateQuery = new StringBuilder();
	    	updateQuery.append("Update ResourceAllocationEntityVo set resourceReleased='1' where projectId="+projectId + " and projectTypeId=" + projectTypeId);
			
			String empCode = "";
			for (AvailableResourceVo resource : resourceList) {
				empCode += ",'"+ resource.getEmployeeCode()+"'";
			}
			if(empCode.length()>0){
				empCode = empCode.substring(1);
				updateQuery.append(" and employeeId not in (select id from EmployeeEntityVo where code in ("+empCode+"))");
			}
			super.delete(updateQuery.toString());
    	}
    	catch(Exception e){
    		throw new CustomApplicationException(e);
    	}
    }
    /**
     * Getter method for userVo
     * 
     * @return the userVo (UserVo)
     */
    public UserVo getUserVo() {
        return userVo;
    }

    /**
     * Setter method for userVo
     *
     * @param userVo the userVo to set
     */
    public void setUserVo(UserVo userVo) {
        this.userVo = userVo;
    }
}
