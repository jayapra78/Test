/**
 * Copyright 2012 by Shirai, All rights reserved.
 *
 * 2012/06/20 <Base Line Version 1.0>
 */

package com.task.dao.custom;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.task.common.ComboVo;
import com.task.common.CommonDaoUtilsVo;
import com.task.common.CommonVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.UserVo;
import com.task.common.constant.ConstantEnum;
import com.task.util.BoCopyManager;
import com.task.util.CommonDaoUtils;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.vo.custom.action.ProjectTypeActionVo;
import com.task.vo.custom.action.RecursiveTaskActionVo;
import com.task.vo.custom.action.TaskActionVo;
import com.task.vo.custom.entity.ProjectType;
import com.task.vo.custom.entity.TaskEntityVo;
import com.task.vo.custom.entity.TimesheetEntityVo;

/**
 *
 * @author NeethiMohan A [SRM]
 */

public class TaskDao extends CommonDao {

	@SuppressWarnings("unused")
	private static Logger log = LoggingAspectInterceptor.getLog(TaskDao.class);

	/**
	 * Method Name : listPickup
	 *
	 * Method Description : This method is used to get PickupCompany master details
	 *
	 * @param requestVo
	 *
	 * @return ResponseVo
	 *
	 * @throws CustomApplicationException
	 */
	public ResponseVo taskList(RequestVo requestVo) throws CustomApplicationException {
        ResponseVo responseVo = null;
        try {
        	 StringBuilder qryBuilder = new StringBuilder();
        	 qryBuilder.append("Select taskId, projectType.projectTypeId, reportempId, employeeId");
        	 qryBuilder.append(",projectId, taskDate, planTaskDesc, planStartTime, planEndTime");
        	 qryBuilder.append(",actualTaskDesc, actualStartTime, actualEndTime, activity");
        	 qryBuilder.append(",workItem, createdOn, projectCode, activityId, unitId, size, module");
        	 qryBuilder.append(",(Select id from TimesheetEntityVo ts where task.employeeId=ts.empId and ts.startTime=(task.taskDate ||' '|| task.actualStartTime) and ts.endTime=(task.taskDate ||' '|| task.actualEndTime))");

        	 qryBuilder.append(" from TaskEntityVo task");

        	 TaskActionVo searchVo = (TaskActionVo) requestVo.getSeacrhVo();
        	 String whereAnd = " where ";
    	     if ( searchVo.getProjectCode() != null && !(searchVo.getProjectCode().equals("")) ) {
    	    	qryBuilder.append(whereAnd+" projectCode = '"+searchVo.getProjectCode()+"'");
    	    	whereAnd = " and ";
    	     }
    	     if (searchVo.getTaskStartDate() != null && searchVo.getTaskEndDate() != null) {
    	    	 qryBuilder.append(whereAnd+" taskDate >= '"+CommonUtils.convertdateTostring(searchVo.getTaskStartDate(),"yyyy-MM-dd")+"'");
    	    	 whereAnd = " and ";
    	    	 qryBuilder.append(whereAnd+" taskDate <= '"+CommonUtils.convertdateTostring(searchVo.getTaskEndDate(),"yyyy-MM-dd")+"'");
    	     } else if (searchVo.getTaskStartDate() != null) {
    	    	 qryBuilder.append(whereAnd+" taskDate >= '"+CommonUtils.convertdateTostring(searchVo.getTaskStartDate(),"yyyy-MM-dd")+"'");
    	    	 whereAnd = " and ";
    	     } else if (searchVo.getTaskEndDate() != null) {
    	    	 qryBuilder.append(whereAnd+" taskDate <= '"+CommonUtils.convertdateTostring(searchVo.getTaskEndDate(),"yyyy-MM-dd")+"'");
    	    	 whereAnd = " and ";
    	     }
    	     if (searchVo.getEmployeeId() != null) {
    	    	 qryBuilder.append(whereAnd+" employeeId = '"+searchVo.getEmployeeId()+"'");
    	    	 whereAnd = " and ";
    	     }
    	     if (searchVo.getProjectTypeActionVo().getProjectTypeId()!=0) {
    	    	 qryBuilder.append(whereAnd+" projectType.projectTypeId = "+searchVo.getProjectTypeActionVo().getProjectTypeId());
    	    	 whereAnd = " and ";
    	     }
    	     qryBuilder.append(" order by taskDate, orderdatetime(ACTUAL_START_TIME,PLAN_START_TIME)");
    	     //    	     		"if((ifnull(ACTUAL_START_TIME,PLAN_START_TIME)<ifnull(PLAN_START_TIME, ACTUAL_START_TIME)),ifnull(ACTUAL_START_TIME,PLAN_START_TIME),ifnull(PLAN_START_TIME, ACTUAL_START_TIME))");

        	  String query = qryBuilder.toString();
		      List result = super.find(query);
		      List<TaskActionVo> taskActionVos = new ArrayList<TaskActionVo>();
		      if(result!=null && result.size()>0){
		    	  for(int index = 0,size=result.size();index<size;index++){
		    		  Object[] row = (Object[])result.get(index);
		    		  TaskActionVo actionVo = new TaskActionVo();
		    		  ProjectTypeActionVo projectTypeActionVo = new ProjectTypeActionVo();
		    		  actionVo.setTaskId((Integer)row[0]);
		    		  if(row[1]!=null){
		    			  projectTypeActionVo.setProjectTypeId((Integer)row[1]);
		    		  }
		    		  actionVo.setProjectTypeActionVo(projectTypeActionVo);
		    		  actionVo.setReportempId((String)row[2]);
		    		  actionVo.setEmployeeId((String)row[3]);
		    		  actionVo.setProjectId((String)row[4]);
		    		  actionVo.setTaskDate((Date)row[5]);
		    		  actionVo.setPlanTaskDesc((String)row[6]);
		    		  actionVo.setPlanStartTime((String)row[7]);
		    		  actionVo.setPlanEndTime((String)row[8]);
		    		  actionVo.setActualTaskDesc((String)row[9]);
		    		  actionVo.setActualStartTime((String)row[10]);
		    		  actionVo.setActualEndTime((String)row[11]);
		    		  actionVo.setActivity((String)row[12]);
		    		  actionVo.setWorkItem((String)row[13]);
		    		  actionVo.setCreatedOn((Date)row[14]);
		    		  actionVo.setProjectCode((String)row[15]);
		    		  actionVo.setActivityId((String)row[16]);
		    		  actionVo.setUnitId((Integer)row[17]);
		    		  actionVo.setSize((BigDecimal)row[18]);
		    		  actionVo.setModule((String)row[19]);
		    		  actionVo.setTimesheetId((Integer)row[20]);
		    		  taskActionVos.add(actionVo);
		    	  }
		    	  responseVo = new ResponseVo();
                  responseVo.setResponseList(taskActionVos);
		      }
		      else {
                  responseVo = new ResponseVo();
                  responseVo.setErrorMessage(FrameworkConstant.getError("NO_RECORD_FOUND"));
              }
              return responseVo;
        } catch (CustomApplicationException e) {
        	log.error(e);
            throw e;
        } catch (Exception e) {
        	log.error(e);
            throw new CustomApplicationException(e);
        }

 }


	/**
	 * @Method :
	 * @Description :
	 * @param entityVo
	 * @param jobActionVo
	 * @throws CustomApplicationException
	 */
	private void convertEntityToAction(TaskEntityVo entityVo, TaskActionVo actionVo) throws CustomApplicationException {

		BoCopyManager.copyProperties(entityVo, actionVo);
		ProjectTypeActionVo projectTypeActionVo = new ProjectTypeActionVo();
		BoCopyManager.copyProperties(entityVo.getProjectType(), projectTypeActionVo);
		actionVo.setProjectTypeActionVo(projectTypeActionVo);

	}

	/**
	 * Method Name : setSearchVo
	 *
	 * Method Description : The method to set the ValueObjects in Criteria
	 *
	 * @param requestVo
	 *
	 * @param commonDaoUtilsVo
	 *
	 * @return CommonDaoUtilsVo
	 */
	private CommonDaoUtilsVo setSearchVo(RequestVo requestVo, CommonDaoUtilsVo commonDaoUtilsVo)
            throws Exception {

     TaskActionVo searchVo = (TaskActionVo) requestVo.getSeacrhVo();
     if ( searchVo.getProjectCode() != null && !(searchVo.getProjectCode().equals("")) ) {
            CommonDaoUtils.eqOp("projectCode", searchVo.getProjectCode(),
                         commonDaoUtilsVo);
     }
     if (searchVo.getTaskStartDate() != null && searchVo.getTaskEndDate() != null) {
            CommonDaoUtils.geOp("taskDate", searchVo.getTaskStartDate(), commonDaoUtilsVo);
            CommonDaoUtils.leOp("taskDate", searchVo.getTaskEndDate(), commonDaoUtilsVo);
     } else if (searchVo.getTaskStartDate() != null) {
            CommonDaoUtils.geOp("taskDate", searchVo.getTaskStartDate(), commonDaoUtilsVo);
     } else if (searchVo.getTaskEndDate() != null) {
            CommonDaoUtils.leOp("taskDate", searchVo.getTaskEndDate(), commonDaoUtilsVo);
     }
     if (searchVo.getEmployeeId() != null) {
            CommonDaoUtils.eqOp("employeeId", searchVo.getEmployeeId(), commonDaoUtilsVo);
     }
     if (searchVo.getProjectTypeActionVo().getProjectTypeId()!=0) {
         CommonDaoUtils.eqOp("projectType.projectTypeId", searchVo.getProjectTypeActionVo().getProjectTypeId(), commonDaoUtilsVo);
     }
     CommonDaoUtils.orderByAsc("taskDate", commonDaoUtilsVo);
     CommonDaoUtils.orderByAsc("planStartTime", commonDaoUtilsVo);
     CommonDaoUtils.orderByAsc("actualStartTime", commonDaoUtilsVo);
     return commonDaoUtilsVo;
}



	/**
 	 * @Method :
 	 * @Description :
 	 * @param requestVo
 	 * @return
 	 * @throws CustomApplicationException
 	 */
 	public ResponseVo saveBulkTask(RequestVo requestVo) throws CustomApplicationException {

 		List comboValueList = null;
 		ResponseVo responseVo = new ResponseVo();
 		responseVo.setStatusFlag(false);
 		try {
 			List<TaskActionVo> taskActionVos = (List<TaskActionVo>) requestVo.getRequestList();
			List<TaskEntityVo> taskEntityVos = new ArrayList<TaskEntityVo>();
 			for(TaskActionVo taskActionVo : taskActionVos){

 				String empId  	   = requestVo.getUserVo().getEmployeeId();
 				int projectTypeId  = taskActionVo.getProjectTypeActionVo().getProjectTypeId();
 				String projectId   = "0";
 				if(taskActionVo.getProjectTypeActionVo()!=null && taskActionVo.getProjectTypeActionVo().getProjectTypeId()==1){
 					projectId = taskActionVo.getProjectId();
				} else if(taskActionVo.getProjectTypeActionVo()!=null && taskActionVo.getProjectTypeActionVo().getProjectTypeId()==2
					&& taskActionVo.getAssociatedProjectId()!=null && taskActionVo.getAssociatedProjectId().length()!=0){
					projectId = taskActionVo.getAssociatedProjectId();
				}
 				String projectCode = taskActionVo.getProjectCode();
 				String reportEmpId = taskActionVo.getReportempId();
 				String plnTaskDesc = taskActionVo.getPlanTaskDesc();

 				String stDate 	   = CommonUtils.convertDateFormat(taskActionVo.getBulkStartDateStr(), "dd/MM/yy","yyyy-MM-dd");
 				String edDate 	   = CommonUtils.convertDateFormat(taskActionVo.getBulkEndDateStr(), "dd/MM/yy","yyyy-MM-dd");
 				String plnStTime   = taskActionVo.getPlanStartTime();
 				String plnEdTime   = taskActionVo.getPlanEndTime();
 				String aclStTime   = taskActionVo.getActualStartTime();
 				String aclEdTime   = taskActionVo.getActualEndTime();

 				String aclTaskDesc = taskActionVo.getActualTaskDesc();
 				String activity    = taskActionVo.getActivity();
 				String activityId  = taskActionVo.getActivityId();
 				String workItem	   = taskActionVo.getWorkItem();
 				BigDecimal size	   = taskActionVo.getSize();
 				int unit		   = taskActionVo.getUnitId();

 				String crDate = CommonUtils.convertdateTostring(CommonUtils.getCurrentDate(),"yyyy-MM-dd HH:mm:ss");

 				List<String> paramNames = new ArrayList<String>();
 	        	List<Object> paramValues = new ArrayList<Object>();

 	            HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		        SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		        Session session =sessionFactory.openSession();
		        StringBuilder qryBuilder1 = new StringBuilder("");
		        StringBuilder qryBuilder = new StringBuilder("");
 	            // Overlapping validation
 	            try{
			    	qryBuilder1.append(" SELECT * FROM task ");
			    	qryBuilder1.append(" WHERE EMPLOYEE_ID='"+empId+"' AND TASK_DATE>='"+stDate+"' AND TASK_DATE<='"+edDate+"'");
			    	if(plnStTime!=null && !"".equals(plnStTime) && plnEdTime!=null && !"".equals(plnEdTime) && aclStTime!=null && !"".equals(aclStTime) && aclEdTime!=null && !"".equals(aclEdTime)){
			    		qryBuilder1.append(" AND (((PLAN_START_TIME='"+plnStTime+"' and '"+plnEdTime+"'=PLAN_END_TIME) or (PLAN_START_TIME<'"+plnStTime+"' and '"+plnStTime+"'<PLAN_END_TIME) or (PLAN_START_TIME<'"+plnEdTime+"' and '"+plnEdTime+"'<PLAN_END_TIME) or ('"+plnStTime+"'<PLAN_START_TIME and PLAN_START_TIME<'"+plnEdTime+"') or ('"+plnStTime+"'<PLAN_END_TIME and PLAN_END_TIME<'"+plnEdTime+"'))");
			    		qryBuilder1.append("   OR ((ACTUAL_START_TIME='"+aclStTime+"' and '"+aclEdTime+"'=ACTUAL_END_TIME) or (ACTUAL_START_TIME<'"+aclStTime+"' and '"+aclStTime+"'<ACTUAL_END_TIME) or (ACTUAL_START_TIME<'"+aclEdTime+"' and '"+aclEdTime+"'<ACTUAL_END_TIME) or ('"+aclStTime+"'<ACTUAL_START_TIME and ACTUAL_START_TIME<'"+aclEdTime+"') or ('"+aclStTime+"'<ACTUAL_END_TIME and ACTUAL_END_TIME<'"+aclEdTime+"')))");
			    	} else{
				    	if(plnStTime!=null && !"".equals(plnStTime) && plnEdTime!=null && !"".equals(plnEdTime)){
				    		qryBuilder1.append(" AND ((PLAN_START_TIME='"+plnStTime+"' and '"+plnEdTime+"'=PLAN_END_TIME) or (PLAN_START_TIME<'"+plnStTime+"' and '"+plnStTime+"'<PLAN_END_TIME) or (PLAN_START_TIME<'"+plnEdTime+"' and '"+plnEdTime+"'<PLAN_END_TIME) or ('"+plnStTime+"'<PLAN_START_TIME and PLAN_START_TIME<'"+plnEdTime+"') or ('"+plnStTime+"'<PLAN_END_TIME and PLAN_END_TIME<'"+plnEdTime+"'))");
				    	}
				    	if(aclStTime!=null && !"".equals(aclStTime) && aclEdTime!=null && !"".equals(aclEdTime)){
				    		qryBuilder1.append(" AND ((ACTUAL_START_TIME='"+aclStTime+"' and '"+aclEdTime+"'=ACTUAL_END_TIME) or (ACTUAL_START_TIME<'"+aclStTime+"' and '"+aclStTime+"'<ACTUAL_END_TIME) or (ACTUAL_START_TIME<'"+aclEdTime+"' and '"+aclEdTime+"'<ACTUAL_END_TIME) or ('"+aclStTime+"'<ACTUAL_START_TIME and ACTUAL_START_TIME<'"+aclEdTime+"') or ('"+aclStTime+"'<ACTUAL_END_TIME and ACTUAL_END_TIME<'"+aclEdTime+"'))");
				    	}
			    	}
			    	qryBuilder1.append(" AND DAYOFWEEK(TASK_DATE) in(2,3,4,5,6)");
			    	String query1 = qryBuilder1.toString();

			    	log.info("qryBuilder1="+qryBuilder1.toString());

			    	SQLQuery sqlQuery1 = session.createSQLQuery(query1);
				    List result = sqlQuery1.list();
			        if(result!=null && result.size()>0){
			        	responseVo.setErrorMessage("Task date and time is overlapping with existing record");
			        }
	 	            else{
	 			    	qryBuilder.append(" INSERT INTO task ");
	 			    	qryBuilder.append(" (EMPLOYEE_ID,TASK_DATE,PROJECT_TYPE_ID,PROJECT_ID,PROJECT_CODE,REPORTEMP_ID,PLAN_TASK_DESC,PLAN_START_TIME,PLAN_END_TIME,ACTUAL_TASK_DESC,ACTUAL_START_TIME,ACTUAL_END_TIME,ACTIVITY,ACTIVITY_ID,WORK_ITEM,SIZE,UNIT_ID,CREATED_ON)");
	 			    	qryBuilder.append(" (SELECT '"+empId+"',selected_date,"+projectTypeId+",'"+projectId+"','"+projectCode+"','"+reportEmpId+"','"+plnTaskDesc+"','"+plnStTime+"','"+plnEdTime+"','"+aclTaskDesc+"','"+aclStTime+"','"+aclEdTime+"','"+activity+"','"+activityId+"','"+workItem+"','"+size+"','"+unit+"','"+crDate+"' FROM ");
	 			    	qryBuilder.append(" (SELECT ADDDATE('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date FROM");
	 			    	qryBuilder.append(" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,");
	 			    	qryBuilder.append(" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,");
	 			    	qryBuilder.append(" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,");
	 			    	qryBuilder.append(" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,");
	 			    	qryBuilder.append(" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v");
	 			    	qryBuilder.append(" WHERE selected_date BETWEEN '"+stDate+"' AND '"+edDate+"' ");
 			    		qryBuilder.append(" AND DAYOFWEEK(selected_date) in(2,3,4,5,6))");

	 			    	String query = qryBuilder.toString();

	 			    	log.info("qryBuilder="+qryBuilder.toString());

	 			        SQLQuery sqlQuery = session.createSQLQuery(query);
	 			        int resultNo = sqlQuery.executeUpdate();

	 			        if(resultNo==0){
	 			        	responseVo.setErrorMessage("The selected day is not exist between the task start date and end date");
	 			        } else {
    			           removeBulkTimeSheetEntry(empId,stDate,edDate,aclStTime,aclEdTime);
		 			       StringBuilder qryBuilder2 = new StringBuilder("");
		 			       qryBuilder2.append(" INSERT INTO timesheet (PrjId, ActId, EmpId, WorkItem, StartTime, EndTime, Detail, UnitId, Size) ");
		 			       qryBuilder2.append(" (SELECT IF(PROJECT_ID IS NULL OR PROJECT_ID='',0,PROJECT_ID),IF(ACTIVITY_ID='',0,ACTIVITY_ID),EMPLOYEE_ID,WORK_ITEM,CONCAT(TASK_DATE,' ',ACTUAL_START_TIME),CONCAT(TASK_DATE,' ',ACTUAL_END_TIME),ACTUAL_TASK_DESC,UNIT_ID,SIZE FROM task WHERE EMPLOYEE_ID='"+empId+"' and ACTUAL_START_TIME is not null and ACTUAL_START_TIME<>'' and CREATED_ON='"+crDate+"')");
		 			       sqlQuery = session.createSQLQuery(qryBuilder2.toString());
		 			       sqlQuery.executeUpdate();
	 			       }
	 	            }
			        session.close();
 	            }
		        catch(Exception e){
		        	log.info("qryBuilder1="+qryBuilder1.toString());
		        	log.info("qryBuilder="+qryBuilder.toString());
		        	session.close();
		        	throw e;
		        }
 		    	return responseVo;
 			}
 		} catch (Exception ex) {
 		    	throw new CustomApplicationException(ex);
 		}
 		return responseVo;
 	}
	/**
	 * Method Name : savePickup
	 *
	 * Method Description : This method is used to insert PickupCompany master
	 *
	 * @param requestVo
	 *
	 * @return ResponseVo
	 *
	 * @throws CustomApplicationException
	 */
	public ResponseVo saveTaskList(RequestVo requestVo) throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		List<CommonVo> list = new ArrayList<CommonVo>();
		try {
			List<TaskActionVo> taskActionVos = (List<TaskActionVo>) requestVo.getRequestList();

			List<TaskEntityVo> taskEntityVos = new ArrayList<TaskEntityVo>();
			for(TaskActionVo taskActionVo : taskActionVos){
				ProjectType projectEntityVo = new ProjectType();
				BoCopyManager.copyProperties(taskActionVo.getProjectTypeActionVo(), projectEntityVo);
				TaskEntityVo taskEntityVo = new TaskEntityVo();
				BoCopyManager.copyProperties(taskActionVo, taskEntityVo);
				taskEntityVo.setProjectType(projectEntityVo);
				if(taskEntityVo.getTaskId()!=null && taskEntityVo.getTaskId()==0){
					taskEntityVo.setTaskId(null);
				}
				taskEntityVos.add(taskEntityVo);
			}

			if(timeOverLappingValidation(taskEntityVos,requestVo)){
				List<TimesheetEntityVo> timesheetEntityVos = new ArrayList<TimesheetEntityVo>();
				List<TimesheetEntityVo> newTimesheetEntityVos = new ArrayList<TimesheetEntityVo>();
				for(TaskActionVo taskActionVo : taskActionVos){
					if(taskActionVo.getActualStartTime()!=null && !"".equals(taskActionVo.getActualStartTime())){
						TimesheetEntityVo tsEntityVo = new TimesheetEntityVo();
						if("".equals(taskActionVo.getProjectId())){
							tsEntityVo.setPrjId(0);
						}
						else if(taskActionVo.getProjectTypeActionVo()!=null && taskActionVo.getProjectTypeActionVo().getProjectTypeId()==1){
							tsEntityVo.setPrjId(Integer.parseInt(taskActionVo.getProjectId()));
						}
						else if(taskActionVo.getProjectTypeActionVo()!=null && taskActionVo.getProjectTypeActionVo().getProjectTypeId()==2
							&& taskActionVo.getAssociatedProjectId()!=null && taskActionVo.getAssociatedProjectId().length()!=0){
								tsEntityVo.setPrjId(Integer.parseInt(taskActionVo.getAssociatedProjectId()));
						}
						else{
							tsEntityVo.setPrjId(0);
						}
						tsEntityVo.setActId(Integer.parseInt(taskActionVo.getActivityId()));
						tsEntityVo.setEmpId(Integer.parseInt(taskActionVo.getEmployeeId()));
						tsEntityVo.setWorkItem(taskActionVo.getWorkItem());
						tsEntityVo.setStartTime(CommonUtils.convertdateTostring(taskActionVo.getTaskDate(), "yyyy-MM-dd")+" "+taskActionVo.getActualStartTime());
						tsEntityVo.setEndTime(CommonUtils.convertdateTostring(taskActionVo.getTaskDate(), "yyyy-MM-dd")+" "+taskActionVo.getActualEndTime());
						tsEntityVo.setSize(taskActionVo.getSize());
						tsEntityVo.setUnitId(taskActionVo.getUnitId());
						tsEntityVo.setDetail(taskActionVo.getActualTaskDesc());
						tsEntityVo.setId(taskActionVo.getTimesheetId());

						timesheetEntityVos.add(tsEntityVo);
						if(tsEntityVo.getId()==null || tsEntityVo.getId()==0){
							newTimesheetEntityVos.add(tsEntityVo);
						}
					}
				}
				removeTimeSheetEntry(newTimesheetEntityVos);
				list.addAll(timesheetEntityVos);
				list.addAll(taskEntityVos);

				List<Integer> tskList = new ArrayList<Integer>();
				List<Integer> tsList = new ArrayList<Integer>();
				for(int i=0;i<list.size();i++){
					if(list.get(i) instanceof TaskEntityVo){
						tskList.add(((TaskEntityVo)list.get(i)).getTaskId());
					}
					else if(list.get(i) instanceof TimesheetEntityVo){
						Integer tsId = ((TimesheetEntityVo)list.get(i)).getId();
						tsList.add(tsId);
						if(tsId!=null && tsId == 0){
							((TimesheetEntityVo)list.get(i)).setId(null);
						}
					}
				}
				log.info("taskIdList="+tskList+",timesheetIdList="+tsList);
				super.saveOrUpdateAll(list);
			}
			else{
				responseVo.setErrorMessage("Date and time is overlapping with existing records");
			}
		} catch (CustomApplicationException e) {
			log.error(e);
			throw e;
		} catch (Exception e) {
			log.error(e);
			throw new CustomApplicationException(e);
		}
		return responseVo;
	}

	public boolean timeOverLappingValidation(List<TaskEntityVo> taskEntityVos,RequestVo requestVo)  throws CustomApplicationException{
		try{
			StringBuilder skipTaskIds = new StringBuilder("");
			boolean existingRecords = false;
			for(TaskEntityVo taskEntityVo : taskEntityVos){
				if(taskEntityVo.getTaskId()!=null && taskEntityVo.getTaskId()!=0){
					skipTaskIds.append(",");
					skipTaskIds.append(taskEntityVo.getTaskId());
					existingRecords = true;
	    		}
			}
			String skipTaskId = skipTaskIds.toString();
			String taskIdCondition = "";
			if(existingRecords){
				taskIdCondition = " and taskId not in (" + skipTaskId.substring(1)+")";
			}

			boolean validationResult = true;
			UserVo userVo = requestVo.getUserVo();
			if(taskEntityVos!=null && taskEntityVos.size()>0){
				StringBuilder qryBuilder = new StringBuilder("");
			    qryBuilder.append(" Select taskId from TaskEntityVo");
			    qryBuilder.append(" where employeeId = '"+userVo.getEmployeeId()+"' "+taskIdCondition+" and (1=0 ");
			    for(TaskEntityVo taskEntityVo : taskEntityVos){
			    	String taskDate = CommonUtils.convertdateTostring(taskEntityVo.getTaskDate(),"yyyy-MM-dd");
			    	if(taskEntityVo.getPlanStartTime()!=null && (!"".equals(taskEntityVo.getPlanStartTime()))){
			    		String planStartTime = taskEntityVo.getPlanStartTime();
			    		String planEndTime   = taskEntityVo.getPlanEndTime();
			    		qryBuilder.append(" or (taskDate='"+taskDate+"'");
			    		qryBuilder.append(" and ((planStartTime='"+planStartTime+"' and '"+planEndTime+"'=planEndTime) or");
			    		qryBuilder.append(" (planStartTime<'"+planStartTime+"' and '"+planStartTime+"'<planEndTime) or");
			    		qryBuilder.append(" (planStartTime<'"+planEndTime+"' and '"+planEndTime+"'<planEndTime) or");
			    		qryBuilder.append(" ('"+planStartTime+"'<planStartTime and planStartTime<'"+planEndTime+"') or");
			    		qryBuilder.append(" ('"+planStartTime+"'<planEndTime and planEndTime<'"+planEndTime+"')");
			    		qryBuilder.append(" ))");
			    	}
			    	if(taskEntityVo.getActualStartTime()!=null && (!"".equals(taskEntityVo.getActualStartTime()))){
			    		String actualStartTime = taskEntityVo.getActualStartTime();
			    		String actualEndTime   = taskEntityVo.getActualEndTime();
			    		qryBuilder.append(" or (taskDate='"+taskDate+"'");
			    		qryBuilder.append(" and ((actualStartTime='"+actualStartTime+"' and '"+actualEndTime+"'=actualEndTime) or");
			    		qryBuilder.append(" (actualStartTime<'"+actualStartTime+"' and '"+actualStartTime+"'<actualEndTime) or");
			    		qryBuilder.append(" (actualStartTime<'"+actualEndTime+"' and '"+actualEndTime+"'<actualEndTime) or");
			    		qryBuilder.append(" ('"+actualStartTime+"'<actualStartTime and actualStartTime<'"+actualEndTime+"') or");
			    		qryBuilder.append(" ('"+actualStartTime+"'<actualEndTime and actualEndTime<'"+actualEndTime+"')");
			    		qryBuilder.append(" ))");
			    	}
			    }
			    qryBuilder.append(")");
			    String query = qryBuilder.toString();
		        List result = super.find(query);

		        if(result!=null && result.size()>0){
		        	validationResult = false;
		        }
			}
			return validationResult;
		} catch (CustomApplicationException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomApplicationException(e);
		}
	}
	/**
	 * @Method :
	 * @Description :
	 * @param startTime
	 * @param endTime
	 * @throws Exception
	 */
	public void removeBulkTimeSheetEntry(String empId,String startDate, String endDate, String startTime,String endTime) throws Exception{

		String deleteQuery = " Delete From TimesheetEntityVo where empId='"+empId+"' and date(startTime)>='"+startDate+"' and date(endTime)<='"+endDate+"' "
						   + " and ( (time(startTime)=time('"+startTime+"') and time(endTime)=time('"+endTime+"')) "
						      + " or (time(startTime)<time('"+startTime+"') and time('"+startTime+"')<time(endTime)) or (time(startTime)<time('"+endTime+"') and time('"+endTime+"')<time(endTime)) "
						      + " or (time('"+startTime+"')<time(startTime) and time(startTime)<time('"+endTime+"')) or (time('"+startTime+"')<time(endTime) and time(endTime)<time('"+endTime+"'))) ";
		super.delete(deleteQuery);
	}
	/**
	 * @Method :
	 * @Description :
	 * @param startTime
	 * @param endTime
	 * @throws Exception
	 */
	public void removeTimeSheetEntry(List<TimesheetEntityVo> timesheetEntityVos) throws Exception{

		StringBuilder deleteQuery = new StringBuilder();
		deleteQuery.append(" Delete From TimesheetEntityVo where ");

		boolean firstEntry = true;

		for (TimesheetEntityVo timesheetEntityVo : timesheetEntityVos) {
			int empId 		 = timesheetEntityVo.getEmpId();
			String startTime = timesheetEntityVo.getStartTime();
			String endTime 	 = timesheetEntityVo.getEndTime();
			if(firstEntry){
				firstEntry = false;
			}
			else{
				deleteQuery.append(" or ");
			}
			deleteQuery.append(" (empId='"+empId+"' and ((startTime='"+startTime+"' and '"+endTime+"'=endTime) or (startTime<'"+startTime+"' and '"+startTime+"'<endTime) or (startTime<'"+endTime+"' and '"+endTime+"'<endTime) or ('"+startTime+"'<startTime and startTime<'"+endTime+"') or ('"+startTime+"'<endTime and endTime<'"+endTime+"')))");
		}
		if(timesheetEntityVos!=null && timesheetEntityVos.size()>0){
			super.delete(deleteQuery.toString());
		}
	}

	/**
	 * Method Name : tradeCombo
	 *
	 * Method Description : This method to load the Trade ComboList.
	 *
	 * @return ResponseVo
	 *
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseVo tradeCombo() throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		try {

			List comboValueList = super.findComboValues("CustomerCompany");
			responseVo.setComboValueList(comboValueList);

		} catch (CustomApplicationException e) {
			throw e;
		} catch (Exception e) {
			throw new CustomApplicationException(e);
		}
		return responseVo;
	}



	/**
	 * @Method :
	 * @Description :
	 * @return
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "rawtypes" })
	public ResponseVo activityLoading(RequestVo requestVo) throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		try {
			List<String> paramNames = new ArrayList<String>();
			paramNames.add("deleteFlag");
			List<Object> paramValues = new ArrayList<Object>();
			paramValues.add(ConstantEnum.FLAG_OFF.getCharValue());
			if (requestVo != null && requestVo.getUserVo().getCompanyId() != null) {
				paramNames.add("companyIdSession");
				paramValues.add(requestVo.getUserVo().getCompanyId());
			}

			List comboValueList = null;
			if (requestVo != null && requestVo.getUserVo().getCompanyId() != null) {
				comboValueList = super.findComboValuesByParam("PickupNameComboCompany",
						(String[]) paramNames.toArray(new String[] {}), paramValues.toArray());
			} else {
				comboValueList = super.findComboValuesByParam("PickupNameCombo",
						(String[]) paramNames.toArray(new String[] {}), paramValues.toArray());
			}
			Map<String, String> comboValueMap = new HashMap<String, String>();
			for (Object object : comboValueList) {
				comboValueMap.put((String) ((Object[]) object)[0], (String) ((Object[]) object)[1]);
			}
			responseVo.setComboValueMap(comboValueMap);

		} catch (CustomApplicationException e) {
			throw e;
		} catch (Exception e) {
			throw new CustomApplicationException(e);
		}
		return responseVo;
	}



	/**
	 * @Method :
	 * @Description :
	 * @return
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "rawtypes" })
	public ResponseVo projectIdLoading(RequestVo requestVo) throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		try {
			List<String> paramNames = new ArrayList<String>();
			paramNames.add("deleteFlag");
			List<Object> paramValues = new ArrayList<Object>();
			paramValues.add(ConstantEnum.FLAG_OFF.getCharValue());
			if (requestVo != null && requestVo.getUserVo().getCompanyId() != null) {
				paramNames.add("companyIdSession");
				paramValues.add(requestVo.getUserVo().getCompanyId());
			}
			List comboValueList = null;
			if (requestVo != null && requestVo.getUserVo().getCompanyId() != null) {
				comboValueList = super.findComboValuesByParam("CustomerNameCompany",
						(String[]) paramNames.toArray(new String[] {}), paramValues.toArray());
			} else {
				comboValueList = super.findComboValuesByParam("CustomerName",
						(String[]) paramNames.toArray(new String[] {}), paramValues.toArray());
			}
			Map<Long, String> comboValueMap = new HashMap<Long, String>();
			for (Object object : comboValueList) {
				comboValueMap.put((Long) ((Object[]) object)[0], (String) ((Object[]) object)[1]
						+ " - " + (String) ((Object[]) object)[2]);
			}
			responseVo.setComboValueMap(comboValueMap);

		} catch (CustomApplicationException e) {
			throw e;
		} catch (Exception e) {
			throw new CustomApplicationException(e);
		}
		return responseVo;
	}

	/**
	 * @Method :
	 * @Description :
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo comboLoading(RequestVo requestVo) throws CustomApplicationException {
		ResponseVo responseVo = new ResponseVo();
		if(requestVo.getComboType().equals("allProject")){
			responseVo = getAllProject(requestVo);
		}
		else if(requestVo.getComboType().equals("allDepartmentProject")){
			responseVo = getAllDepartmentProject(requestVo);
		}
		else if(requestVo.getComboType().equals("project")){
			responseVo = getProjectCombo(requestVo);
		}
		else if(requestVo.getComboType().equals("departmentProject")){
			responseVo = getDepartmentProjectCombo(requestVo);
		}
		else if(requestVo.getComboType().equals("activity")){
			responseVo = getActivityCombo(requestVo);
		}
		else if(requestVo.getComboType().equals("projectManager")){
            responseVo = getProjectManager(requestVo);
		}
		else if(requestVo.getComboType().equals("projectType")){
			responseVo = getProjectTypes(requestVo);
		}
		else if(requestVo.getComboType().equals("unit")){
			responseVo = getUnitCombo(requestVo);
		}
		else if(requestVo.getComboType().equals("ccList")){
			responseVo = getCcList(requestVo);
		}
		return responseVo;
	}

	private ResponseVo getProjectTypes(RequestVo requestVo){
		List comboValueList = null;
		ResponseVo responseVo = new ResponseVo();
		try {
			comboValueList = super.findComboValues("ProjectType");
			ProjectTypeActionVo projectTypeActionVo;
			List<ProjectTypeActionVo> comboValuelst = new ArrayList<ProjectTypeActionVo>();
			for (Object object : comboValueList) {
				projectTypeActionVo = new ProjectTypeActionVo();
				projectTypeActionVo.setProjectTypeId((Integer) ((Object[]) object)[0]);
				projectTypeActionVo.setProjectType((String) ((Object[]) object)[1]);
				comboValuelst.add(projectTypeActionVo);
			}
			responseVo.setResponseList(comboValuelst);

		} catch (CustomApplicationException e) {
			// TODO Auto-generated catch block
			log.error(e);
		}
		return responseVo;
	}
	private ResponseVo getCcList(RequestVo requestVo){
		List comboValueList = null;
		ResponseVo responseVo = new ResponseVo();
		try {

			List<String> paramNames = new ArrayList<String>();
       	 	List<Object> paramValues = new ArrayList<Object>();
            paramNames.add("deptId");
            paramValues.add(requestVo.getUserVo().getDeptId());
            comboValueList = super.findComboValuesByParam("CCList", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());

			List<ComboVo> comboValuelst = new ArrayList<ComboVo>();
			for (Object object : comboValueList) {
				ComboVo comboVo = new ComboVo();
				comboVo.setName((String) ((Object[]) object)[0]);
				comboVo.setCommonName((String) ((Object[]) object)[1]);
				comboValuelst.add(comboVo);
			}
			responseVo.setComboValueList(comboValuelst);

		} catch (CustomApplicationException e) {
			// TODO Auto-generated catch block
			log.error(e);
		}
		return responseVo;
	}
	private ResponseVo getUnitCombo(RequestVo requestVo){
		List comboValueList = null;
		ResponseVo responseVo = new ResponseVo();
		try {
			comboValueList = super.findComboValues("UnitCombo");

			List<ComboVo> comboValuelst = new ArrayList<ComboVo>();
			for (Object object : comboValueList) {
				ComboVo comboVo = new ComboVo();
				comboVo.setIdValue((Integer) ((Object[]) object)[0]);
				comboVo.setName((String) ((Object[]) object)[1]);
				comboValuelst.add(comboVo);
			}
			responseVo.setComboValueList(comboValuelst);

		} catch (CustomApplicationException e) {
			// TODO Auto-generated catch block
			log.error(e);
		}
		return responseVo;
	}

	/**
	 * @Method :
	 * @Description :
	 * @param requestVo
	 * @return
	 */
	private ResponseVo getActivityCombo(RequestVo requestVo) {
		List comboValueList = null;
		ResponseVo responseVo = new ResponseVo();
		try {
			comboValueList = super.findComboValues("ActivityDetails");

			ComboVo comboVo;
			List<ComboVo> comboValuelst = new ArrayList<ComboVo>();
			for (Object object : comboValueList) {
				comboVo = new ComboVo();
				comboVo.setIdValue((Integer) ((Object[]) object)[0]);
				comboVo.setKey((String) ((Object[]) object)[1]+(String) ((Object[]) object)[3]);
				comboVo.setName((String) ((Object[]) object)[1] + " : "
						+ (String) ((Object[]) object)[2] + "     "
						+ (String) ((Object[]) object)[3] + " : "
						+ (String) ((Object[]) object)[4]);
				comboValuelst.add(comboVo);

			}
			responseVo.setComboValueList(comboValuelst);

		} catch (CustomApplicationException e) {
			// TODO Auto-generated catch block
			log.error(e);
		}
		return responseVo;
	}
	/**
	 *
	 * @Method :
	 * @Description :
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo getDepartmentProjectCombo(RequestVo requestVo) throws CustomApplicationException{
		ResponseVo responseVo = new ResponseVo();
		List comboValueList = null;
		List<ComboVo> projectList = new ArrayList<ComboVo>();
		UserVo userVo = requestVo.getUserVo();
		if("1".equals(requestVo.getParamIds())){

			List<String> paramNames = new ArrayList<String>();
       	 	List<Object> paramValues = new ArrayList<Object>();
            paramNames.add("empId");
            paramValues.add(Integer.parseInt(userVo.getEmployeeId()));
            comboValueList = super.findComboValuesByParam("DepartmentProjectCombo", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());
            if(comboValueList!=null){
			   for(Object object:comboValueList){
				    ComboVo comboVo = new ComboVo();
					comboVo.setIdValue((Integer) ((Object[]) object)[0]);
					comboVo.setName((String) ((Object[]) object)[1] +"  :  "+(String) ((Object[]) object)[2]);
					comboVo.setKey((String) ((Object[]) object)[1]);
					comboVo.setRefId((String) ((Object[]) object)[3]);
					projectList.add(comboVo);
				}
			}
		}
		else{
			List<String> paramNames = new ArrayList<String>();
       	 	List<Object> paramValues = new ArrayList<Object>();
            paramNames.add("projectTypeId");
            paramValues.add(Integer.parseInt(requestVo.getParamIds()));
            paramNames.add("deptId");
            paramValues.add(userVo.getDeptId());
            comboValueList = super.findComboValuesByParam("PracticeTeamCombo", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());

    		if(comboValueList!=null){
    		   for(Object object:comboValueList){
    			    /*String code = (String) ((Object[]) object)[1];
    			    if(code==null){
    			    	code = String.valueOf((Integer) ((Object[]) object)[2]);
    			    }*/
    			    String code = (String) ((Object[]) object)[1];
    			    ComboVo comboVo = new ComboVo();
    				comboVo.setIdValue(Integer.parseInt(((Object[]) object)[0].toString()));
    				comboVo.setName( code +"  :  "+(String) ((Object[]) object)[2]);
    				comboVo.setKey(code);
    				comboVo.setRefId((String) ((Object[]) object)[3]);
    				projectList.add(comboVo);
    			}
    		}
		}
		responseVo.setComboValueList(projectList);
		return responseVo;
	}
	public ResponseVo getAllProject(RequestVo requestVo) throws CustomApplicationException{

		ResponseVo responseVo = new ResponseVo();
		List<ComboVo> projectList = new ArrayList<ComboVo>();
		List comboValueList = super.findComboValues("AllProject");
		if(comboValueList!=null){
		   for(Object object:comboValueList){
			    ComboVo comboVo = new ComboVo();
				comboVo.setIdValue(Integer.parseInt(((Object[]) object)[0].toString()));
				comboVo.setKey((String) ((Object[]) object)[1]);
				comboVo.setName((String) ((Object[]) object)[2]);
				comboVo.setRefId((String) ((Object[]) object)[3]);
				comboVo.setCommonName(((Object[]) object)[4].toString());
				comboVo.setRefId2((String) ((Object[]) object)[5]);
				projectList.add(comboVo);
			}
		}
		Map<String,List<ComboVo>> projects = orderProjectTypeWise(projectList);
		responseVo.setComboValueMap(projects);
		return responseVo;
	}
	public ResponseVo getAllDepartmentProject(RequestVo requestVo) throws CustomApplicationException{

		ResponseVo responseVo = new ResponseVo();
		List<ComboVo> projectList = new ArrayList<ComboVo>();

		String statusName = requestVo.getParamNames()[0];
		String statusValue = (String)requestVo.getValues()[0];

		List<String> paramNames = new ArrayList<String>();
   	 	List<Object> paramValues = new ArrayList<Object>();
        paramNames.add("projectTypeId");
        paramValues.add(Integer.parseInt(requestVo.getParamIds()));
        paramNames.add(statusName);
        paramValues.add(Integer.parseInt(statusValue));

        List comboValueList = super.findComboValuesByParam("AllDepartmentProject", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());

		if(comboValueList!=null){

		   for(Object object:comboValueList){

			    String code = (String) ((Object[]) object)[1];
			    ComboVo comboVo = new ComboVo();
				comboVo.setIdValue(Integer.parseInt(((Object[]) object)[0].toString()));
				comboVo.setName(code +"  :  "+(String) ((Object[]) object)[2]);
				comboVo.setKey(code);
				comboVo.setRefId((String) ((Object[]) object)[3]);
				comboVo.setCommonName((String) ((Object[]) object)[4]);
				comboVo.setIdLongValue(Long.parseLong((String)((Object[]) object)[5]));
				projectList.add(comboVo);

			}
		}
		Map<String,List<ComboVo>> departmentProjects = orderDepartmentWise(projectList);
		responseVo.setComboValueMap(departmentProjects);
		return responseVo;
	}
	private Map<String,List<ComboVo>> orderProjectTypeWise(List<ComboVo> projectList){
		Map<String,List<ComboVo>> projects = new HashMap<String,List<ComboVo>>();
		for (ComboVo comboVo : projectList) {
			String projectTypeId = comboVo.getCommonName();
			List<ComboVo> list = projects.get(projectTypeId);
			if(list == null){
				list = new ArrayList<ComboVo>();
				projects.put(projectTypeId, list);
			}
			list.add(comboVo);
		}
		return projects;
	}
	private Map<String,List<ComboVo>> orderDepartmentWise(List<ComboVo> projectList){
		Map<String,List<ComboVo>> departmentProjects = new HashMap<String,List<ComboVo>>();
		for (ComboVo comboVo : projectList) {
			String department = comboVo.getCommonName();
			List<ComboVo> list = departmentProjects.get(department);
			if(list == null){
				list = new ArrayList<ComboVo>();
				departmentProjects.put(department, list);
			}
			list.add(comboVo);
		}
		return departmentProjects;
	}
	/**
	 *
	 * @Method :
	 * @Description :
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo getProjectCombo(RequestVo requestVo) throws CustomApplicationException{
		ResponseVo responseVo = new ResponseVo();
		List comboValueList = null;
		List<ComboVo> projectList = new ArrayList<ComboVo>();

		if("1".equals(requestVo.getParamIds())){
			comboValueList = super.findComboValues("ProjectIdCombo");
			if(comboValueList!=null){
			   for(Object object:comboValueList){
				    ComboVo comboVo = new ComboVo();
					comboVo.setIdValue((Integer) ((Object[]) object)[0]);
					comboVo.setName((String) ((Object[]) object)[1] +"  :  "+(String) ((Object[]) object)[2]);
					comboVo.setKey((String) ((Object[]) object)[1]);
					comboVo.setRefId((String) ((Object[]) object)[3]);
					projectList.add(comboVo);
				}
			}
		}
		else{
			List<String> paramNames = new ArrayList<String>();
       	 	List<Object> paramValues = new ArrayList<Object>();
            paramNames.add("projectTypeId");
            paramValues.add(Integer.parseInt(requestVo.getParamIds()));
            comboValueList = super.findComboValuesByParam("PracticeTeamCombo", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());

    		if(comboValueList!=null){
    		   for(Object object:comboValueList){
    			    /*String code = (String) ((Object[]) object)[1];
    			    if(code==null){
    			    	code = String.valueOf((Integer) ((Object[]) object)[2]);
    			    }*/

    			   String code = (String) ((Object[]) object)[1];
   			    ComboVo comboVo = new ComboVo();
   				comboVo.setIdValue(Integer.parseInt(((Object[]) object)[0].toString()));
   				comboVo.setName(code +"  :  "+(String) ((Object[]) object)[2]);
   				comboVo.setKey(code);
   				comboVo.setRefId((String) ((Object[]) object)[3]);
   				projectList.add(comboVo);

    			}
    		}
		}
		responseVo.setComboValueList(projectList);
		return responseVo;
	}

	/**
     * @Method :
      * @Description :
      * @param requestVo
     * @return
     */
     private ResponseVo getProjectManager(RequestVo requestVo) {
         List comboValueList = null;
         ResponseVo responseVo = new ResponseVo();
         try {
             comboValueList = super.findComboValues("AllProjectManager");

                 ComboVo comboVo;
                 List<ComboVo> comboValuelst = new ArrayList<ComboVo>();
                 for (Object object : comboValueList) {
                     comboVo = new ComboVo();
                     comboVo.setIdValue((Integer) ((Object[]) object)[0]);
                     comboVo.setName((String) ((Object[]) object)[1]);
                     comboVo.setKey((String) ((Object[]) object)[2] );
                     comboVo.setRefId((Integer) ((Object[]) object)[0]+":"+(String) ((Object[]) object)[1]);
                     comboVo.setCommonName((String) ((Object[]) object)[2] +":"+((Object[]) object)[1]);
                     comboValuelst.add(comboVo);
                 }
                 responseVo.setComboValueList(comboValuelst);
         } catch (CustomApplicationException e) {
        	 log.error(e);
         }
         return responseVo;
     }

     public ResponseVo getIndividualMailDetails(RequestVo requestVo)throws CustomApplicationException{
         ResponseVo responseVo = new ResponseVo();

         try{
        	 UserVo userVo = requestVo.getUserVo();

        	 List<String> paramNames = new ArrayList<String>();
        	 List<Object> paramValues = new ArrayList<Object>();
             paramNames.add("empId");
             paramValues.add(userVo.getEmployeeId());
             
	         String sql  = " SELECT"+
			             " t.TASK_DATE, pt.PROJECT_TYPE, t.PROJECT_CODE, t.PLAN_TASK_DESC, t.PLAN_START_TIME, t.PLAN_END_TIME,"+ 
			             " t.ACTUAL_TASK_DESC, t.ACTUAL_START_TIME, t.ACTUAL_END_TIME, t.ACTIVITY, t.WORK_ITEM, e.Name, e.Code, "+
			             " sbu.Email AS sbuMail,"+
			             " sbu.Name AS sbuName,"+
			             " pom.PM_EMAIL_ID AS pmMail,"+
			             " pm.Email AS pmAltMail,"+
			             " pm.Name AS pmName,"+
			             " pom.PL_EMAIL_ID AS plEmail,"+
			             " IF(pom.STATUS_MAIL IS NOT NULL,pom.CUSTOMER_EMAIL_ID,NULL) AS customerEmail,"+
			             " pt.PROJECT_TYPE_ID,"+
			             " pom.PROJECT_NAME"+
			             " FROM"+
			             " task t"+ 
			             " LEFT OUTER JOIN employee e ON t.EMPLOYEE_ID=e.Id"+
			             " LEFT OUTER JOIN project_type pt ON t.PROJECT_TYPE_ID=pt.PROJECT_TYPE_ID"+
			             " LEFT OUTER JOIN employee sbu ON sbu.Desg_Id = '25' AND sbu.Dept_Id=e.Dept_Id AND sbu.Status <> '3' AND sbu.CODE <> 'A0001'"+
			             " LEFT OUTER JOIN project_owner_mapping pom ON pom.PROJECT_TYPE_ID=t.PROJECT_TYPE_ID AND pom.PROJECT_ID=t.PROJECT_ID"+
			             " LEFT OUTER JOIN employee pm ON t.REPORTEMP_ID=pm.Id"+
			             " WHERE t.TASK_DATE = CURRENT_DATE() AND t.EMPLOYEE_ID='1622'"+
			             " ORDER BY t.PLAN_START_TIME, t.ACTUAL_START_TIME";
             
             
             
             List comboValueList = super.findComboValuesByParam("EmpMailInfo", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());
             List<TaskActionVo> taskActionVoList = new ArrayList<TaskActionVo>();
             for(Object object:comboValueList){
            	 TaskActionVo taskActionVo = new TaskActionVo();
                 taskActionVo.setTaskDate((Date)((Object[])(Object[])object)[0]);

                 ProjectTypeActionVo projectTypeActionVo = new ProjectTypeActionVo();
                 projectTypeActionVo.setProjectType((String)((Object[])(Object[])object)[1]);
                 taskActionVo.setProjectTypeActionVo(projectTypeActionVo);

                 taskActionVo.setProjectCode((String)((Object[])(Object[])object)[2]);
                 taskActionVo.setPlanTaskDesc((String)((Object[])(Object[])object)[3]);
                 taskActionVo.setPlanStartTime((String)((Object[])(Object[])object)[4]);
                 taskActionVo.setPlanEndTime((String)((Object[])(Object[])object)[5]);
                 taskActionVo.setActualTaskDesc((String)((Object[])(Object[])object)[6]);
                 taskActionVo.setActualStartTime((String)((Object[])(Object[])object)[7]);
                 taskActionVo.setActualEndTime((String)((Object[])(Object[])object)[8]);
                 taskActionVo.setActivity((String)((Object[])(Object[])object)[9]);
                 taskActionVo.setWorkItem((String)((Object[])(Object[])object)[10]);
                 taskActionVo.setEmpName((String)((Object[])(Object[])object)[11]);
                 taskActionVo.setEmployeeId((String)((Object[])(Object[])object)[12]);
                 taskActionVo.setSbuEmailId((String)((Object[])(Object[])object)[13]);
                 taskActionVo.setSbuName((String)((Object[])(Object[])object)[14]);
                 taskActionVo.setPmEmailId((String)((Object[])(Object[])object)[15]);
                 if(taskActionVo.getPmEmailId()==null){
                	 taskActionVo.setPmEmailId((String)((Object[])(Object[])object)[16]);
                 }
                 taskActionVo.setPmName((String)((Object[])(Object[])object)[17]);
                 taskActionVo.setPlEmailId((String)((Object[])(Object[])object)[18]);

                 //taskActionVo.setCustomerEmailId((String)((Object[])(Object[])object)[19]);

                 projectTypeActionVo.setProjectTypeId((Integer)((Object[])(Object[])object)[20]);
                 if(projectTypeActionVo.getProjectTypeId()==0){
                	 projectTypeActionVo.setProjectType("");
                 }

                 taskActionVo.setProjectName((String)((Object[])(Object[])object)[21]);

                 if(taskActionVo.getProjectName()==null){
                	 taskActionVo.setProjectName("");
                 }

                 taskActionVoList.add(taskActionVo);
             }
             responseVo.setResponseList(taskActionVoList);
         }
         catch(CustomApplicationException e){
        	 log.error(e);
         }
         return responseVo;
    }

     public ResponseVo getConsolidatedTaskMailDetails(RequestVo requestVo)throws CustomApplicationException{
         ResponseVo responseVo = new ResponseVo();
         try{
        	 List comboValueList = super.findComboValues("ConslidatedTaskMail");
             List<TaskActionVo> taskActionVoList = new ArrayList<TaskActionVo>();
             TaskActionVo taskActionVo;
             for(Object object:comboValueList){
                 taskActionVo = new TaskActionVo();
                 taskActionVo.setTaskDate((Date)((Object[])(Object[])object)[0]);

                 ProjectTypeActionVo projectTypeActionVo = new ProjectTypeActionVo();
                 projectTypeActionVo.setProjectType((String)((Object[])(Object[])object)[1]);
                 taskActionVo.setProjectTypeActionVo(projectTypeActionVo);

                 taskActionVo.setProjectCode((String)((Object[])(Object[])object)[2]);
                 taskActionVo.setPlanTaskDesc(((String)((Object[])(Object[])object)[3]).replaceAll("&", "&amp;"));
                 taskActionVo.setPlanStartTime((String)((Object[])(Object[])object)[4]);
                 taskActionVo.setPlanEndTime((String)((Object[])(Object[])object)[5]);
                 taskActionVo.setActualTaskDesc(((String)((Object[])(Object[])object)[6]).replaceAll("&", "&amp;"));
                 taskActionVo.setActualStartTime((String)((Object[])(Object[])object)[7]);
                 taskActionVo.setActualEndTime((String)((Object[])(Object[])object)[8]);
                 taskActionVo.setActivity((String)((Object[])(Object[])object)[9]);
                 taskActionVo.setWorkItem(((String)((Object[])(Object[])object)[10]).replaceAll("&", "&amp;"));
                 taskActionVo.setEmpName((String)((Object[])(Object[])object)[11]);
                 taskActionVo.setEmployeeId((String)((Object[])(Object[])object)[12]);
                 taskActionVo.setSbuEmailId((String)((Object[])(Object[])object)[13]);
                 taskActionVo.setSbuName((String)((Object[])(Object[])object)[14]);

                 taskActionVo.setPmEmailId((String)((Object[])(Object[])object)[15]);
                 if(taskActionVo.getPmEmailId()==null){
                	 taskActionVo.setPmEmailId((String)((Object[])(Object[])object)[16]);
                 }
                 taskActionVo.setPmName((String)((Object[])(Object[])object)[17]);
                 taskActionVo.setPlEmailId((String)((Object[])(Object[])object)[18]);

                 //taskActionVo.setCustomerEmailId((String)((Object[])(Object[])object)[19]);

                 projectTypeActionVo.setProjectTypeId((Integer)((Object[])(Object[])object)[20]);
                 if(projectTypeActionVo.getProjectTypeId()==0){
                	 projectTypeActionVo.setProjectType("");
                 }
               	 taskActionVo.setProjectName((String)((Object[])(Object[])object)[21]);
                 if(taskActionVo.getProjectName()==null){
                	 taskActionVo.setProjectName("");
                 }
                 taskActionVoList.add(taskActionVo);
             }
             responseVo.setResponseList(taskActionVoList);
         }
         catch(CustomApplicationException e){
             log.error("getConsolidatedTaskMailDetails="+e);
         }
         return responseVo;
     }

     public ResponseVo getNoTaskEmployees(RequestVo requestVo) throws CustomApplicationException{
    	 ResponseVo responseVo = new ResponseVo();
    	 List<ComboVo> comboVoList = new ArrayList<ComboVo>();
    	 int currentDay = CommonUtils.getCurrentDay();
    	 if(currentDay != Calendar.SATURDAY && currentDay != Calendar.SUNDAY){
	         try{
	        	 List comboValueList = super.findComboValues("NoTaskEmployee");
	             ComboVo comboVo;
	             for(Object object:comboValueList){
	            	 comboVo = new ComboVo();
	            	 comboVo.setName((String)((Object[])(Object[])object)[0]); // Employee name
	            	 comboVo.setKey((String)((Object[])(Object[])object)[1]); // Employee Code
	            	 comboVo.setCommonName((String)((Object[])(Object[])object)[2]); // SBU Email
	            	 comboVoList.add(comboVo);
	             }
	         }
	         catch(CustomApplicationException e){
	        	 log.error(e);
	         }
    	 }
    	 responseVo.setResponseList(comboVoList);
         return responseVo;
     }
  /**
   *
   * @Method :
   * @Description :
   * @param requestVo
   * @return
   * @throws CustomApplicationException
   */
     public ResponseVo getPmList(RequestVo requestVo)
         throws CustomApplicationException
     {
    	 List comboValueList = null;
         ResponseVo responseVo = new ResponseVo();
         try
         {
             comboValueList = super.findComboValues("PMList");
             List<ComboVo> comboValuelst = new ArrayList<ComboVo>();
             ComboVo comboVo;
             for(Object object:comboValueList)
             {
                 comboVo = new ComboVo();
                 comboVo.setName((String)((Object[])(Object[])object)[0]);
                 //Email Id of PM
                 comboVo.setKey((String)((Object[])(Object[])object)[1]);
                 //Name  of PM
                 comboVo.setCommonName((String)((Object[])(Object[])object)[2]);
                 comboValuelst.add(comboVo);
             }

             responseVo.setComboValueList(comboValuelst);
         }
         catch(CustomApplicationException e)
         {
        	 log.error(e);
         }
         return responseVo;
     }

     public ResponseVo getEmployeeList(RequestVo requestVo)
     {
    	 List comboValueList = null;
         ResponseVo responseVo = new ResponseVo();
         try
         {
        	 List<String> paramNames = new ArrayList<String>();
        	 List<Object> paramValues = new ArrayList<Object>();
             String empId = ((ComboVo)requestVo.getCommonVo()).getName();
             paramNames.add("reportEmpId");
             paramValues.add(empId);
             paramNames.add("taskDate");
             paramValues.add(CommonUtils.getCurrentDate());
             comboValueList = super.findComboValuesByParam("MailEmployeeId", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());
             List<ComboVo> comboValuelst = new ArrayList<ComboVo>();
             ComboVo comboVo;
             for(Object object:comboValueList)
             {
                 comboVo = new ComboVo();
                 comboVo.setName((String)((Object[])(Object[])object)[0]);
                 comboValuelst.add(comboVo);
             }

             responseVo.setComboValueList(comboValuelst);
         }
         catch(CustomApplicationException e)
         {
        	 log.error(e);
         }
         return responseVo;
     }

     public ResponseVo getTaskList(RequestVo requestVo)
     {
    	 List comboValueList = null;
         ResponseVo responseVo = new ResponseVo();
         try
         {
        	 List<String> paramNames = new ArrayList<String>();
        	 List<Object> paramValues = new ArrayList<Object>();
        	 String empId = ((ComboVo)requestVo.getCommonVo()).getName();
             paramNames.add("EmployeeId");
             paramValues.add(empId);
             paramNames.add("taskDate");
             paramValues.add(CommonUtils.getCurrentDate());
             comboValueList = super.findComboValuesByParam("TaskList", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());
             List<TaskActionVo> taskActionVoList = new ArrayList<TaskActionVo>();
             TaskActionVo taskActionVo;
             for(Object object:comboValueList)
             {
                 taskActionVo = new TaskActionVo();
                 taskActionVo.setTaskDate((Date)((Object[])(Object[])object)[0]);
                 taskActionVo.setProjectCode((String)((Object[])(Object[])object)[1]);
                 taskActionVo.setPlanTaskDesc((String)((Object[])(Object[])object)[2]);
                 taskActionVo.setPlanStartTime((String)((Object[])(Object[])object)[3]);
                 taskActionVo.setPlanEndTime((String)((Object[])(Object[])object)[4]);
                 taskActionVo.setActualTaskDesc((String)((Object[])(Object[])object)[5]);
                 taskActionVo.setActualStartTime((String)((Object[])(Object[])object)[6]);
                 taskActionVo.setActualEndTime((String)((Object[])(Object[])object)[7]);
                 taskActionVo.setActivity((String)((Object[])(Object[])object)[8]);
                 taskActionVo.setWorkItem((String)((Object[])(Object[])object)[9]);
                 taskActionVo.setEmpName((String)((Object[])(Object[])object)[10]);
                 taskActionVo.setEmployeeId((String)((Object[])(Object[])object)[11]);
                 taskActionVoList.add(taskActionVo);

             }

             responseVo.setResponseList(taskActionVoList);
         }
         catch(CustomApplicationException e)
         {
        	 log.error(e);
         }
         return responseVo;
     }

     /**
 	 * @Method :
 	 * @Description :
 	 * @param requestVo
 	 * @return
 	 * @throws CustomApplicationException
 	 */
 	public ResponseVo saveRecursiveTask(RequestVo requestVo) throws CustomApplicationException {

 		List comboValueList = null;
 		ResponseVo responseVo = new ResponseVo();
 		responseVo.setStatusFlag(false);
 		try {
 				RecursiveTaskActionVo actionVo = (RecursiveTaskActionVo) requestVo.getCommonVo();

 				String empId  = requestVo.getUserVo().getEmployeeId();
 				String stDate = CommonUtils.convertDateFormat(actionVo.getTaskStartDateStr(), "dd/MM/yy","yyyy-MM-dd");
 				String edDate = CommonUtils.convertDateFormat(actionVo.getTaskEndDateStr(), "dd/MM/yy","yyyy-MM-dd");
 				String stTime = actionVo.getPlanStartTime();
 				String edTime = actionVo.getPlanEndTime();
 				String desc   = actionVo.getPlanDesc();
 				String day   = actionVo.getTaskDay();
 				String crDate = CommonUtils.convertdateTostring(CommonUtils.getCurrentDate(),"yyyy-MM-dd HH:mm:ss");

 				List<String> paramNames = new ArrayList<String>();
 	        	List<Object> paramValues = new ArrayList<Object>();

 	            paramNames.add("empId");
 	            paramValues.add(empId);
 	            paramNames.add("taskStartDate");
 	            paramValues.add(CommonUtils.convertStringToDate(actionVo.getTaskStartDateStr(),"dd/MM/yy"));
 	            paramNames.add("taskEndDate");
 	            paramValues.add(CommonUtils.convertStringToDate(actionVo.getTaskEndDateStr(),"dd/MM/yy"));
 	            paramNames.add("startTime");
 	            paramValues.add(stTime);
 	            paramNames.add("endTime");
 	            paramValues.add(edTime);

 	            comboValueList = super.findComboValuesByParam("TaskOverlapping", (String[])(String[])paramNames.toArray(new String[0]), paramValues.toArray());

 	            HibernateTemplate hibernateTemplate = (HibernateTemplate) ContextLoader.getBean("myHibernateTemplate");
		        SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		        Session session =sessionFactory.openSession();
		        StringBuilder qryBuilder1 = new StringBuilder("");
		        StringBuilder qryBuilder = new StringBuilder("");
 	            // Overlapping validation
 	            try{
			    	qryBuilder1.append(" SELECT * FROM task ");
			    	qryBuilder1.append(" WHERE EMPLOYEE_ID='"+empId+"' AND TASK_DATE>='"+stDate+"' AND TASK_DATE<='"+edDate+"'");
			    	qryBuilder1.append(" AND ((PLAN_START_TIME='"+stTime+"' and '"+edTime+"'=PLAN_END_TIME) or (PLAN_START_TIME<'"+stTime+"' and '"+stTime+"'<PLAN_END_TIME) or (PLAN_START_TIME<'"+edTime+"' and '"+edTime+"'<PLAN_END_TIME) or ('"+stTime+"'<PLAN_START_TIME and PLAN_START_TIME<'"+edTime+"') or ('"+stTime+"'<PLAN_END_TIME and PLAN_END_TIME<'"+edTime+"'))");

			    	if(null != day && day.contains("9")){
			    		qryBuilder1.append(" AND DAYOFWEEK(TASK_DATE) in(1,2,3,4,5,6,7)");
			    	} else if(null != day && day.contains("8")){
			    		qryBuilder1.append(" AND DAYOFWEEK(TASK_DATE) in(2,3,4,5,6)");
			    	} else{
			    		qryBuilder1.append(" AND DAYOFWEEK(TASK_DATE) in("+day+")");
			    	}
			    	String query1 = qryBuilder1.toString();
			    	SQLQuery sqlQuery1 = session.createSQLQuery(query1);
				    List result = sqlQuery1.list();
			        if(result!=null && result.size()>0){
			        	responseVo.setErrorMessage("Task date and time is overlapping with existing record");
			        }
	 	            else{
	 			    	qryBuilder.append(" INSERT INTO task ");
	 			    	qryBuilder.append(" (EMPLOYEE_ID,TASK_DATE,PLAN_TASK_DESC,PLAN_START_TIME,PLAN_END_TIME,CREATED_ON)");
	 			    	qryBuilder.append(" (SELECT '"+empId+"',selected_date,'"+desc+"','"+stTime+"','"+edTime+"','"+crDate+"' FROM ");
	 			    	qryBuilder.append(" (SELECT ADDDATE('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date FROM");
	 			    	qryBuilder.append(" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,");
	 			    	qryBuilder.append(" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,");
	 			    	qryBuilder.append(" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,");
	 			    	qryBuilder.append(" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,");
	 			    	qryBuilder.append(" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v");
	 			    	qryBuilder.append(" WHERE selected_date BETWEEN '"+stDate+"' AND '"+edDate+"' ");

	 			    	if(null != day && day.contains("9")){
	 			    		qryBuilder.append(" AND DAYOFWEEK(selected_date) in(1,2,3,4,5,6,7))");
	 			    	} else if(null != day && day.contains("8")){
	 			    		qryBuilder.append(" AND DAYOFWEEK(selected_date) in(2,3,4,5,6))");
	 			    	} else{
	 			    		qryBuilder.append(" AND DAYOFWEEK(selected_date) in("+day+"))");
	 			    	}

	 			    	String query = qryBuilder.toString();


	 			        SQLQuery sqlQuery = session.createSQLQuery(query);
	 			        int resultNo = sqlQuery.executeUpdate();
	 			        if(resultNo==0){
	 			        	responseVo.setErrorMessage("The selected day is not exist between the task start date and end date");
	 			        }
	 	            }
			        session.close();
 	            }
		        catch(Exception e){
		        	log.info("qryBuilder1="+qryBuilder1.toString());
		        	log.info("qryBuilder="+qryBuilder.toString());
		        	session.close();
		        	throw e;
		        }
 		    	return responseVo;
 		    } catch (Exception ex) {
 		    	throw new CustomApplicationException(ex);
 		    }
 	    }

}
