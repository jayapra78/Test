/**
 * Creater / sengottaiyansSRM copyright information to Shirai 2013
 * 
 */
package com.task.dialect;

import org.hibernate.Hibernate;
import org.hibernate.dialect.function.SQLFunctionTemplate;

/**
 * @author sengottaiyans
 *
 */
public class MySQLDialect extends org.hibernate.dialect.MySQLDialect{
	@SuppressWarnings("deprecation")
	public MySQLDialect() {
		super();
		registerFunction("timediffmin", new SQLFunctionTemplate(Hibernate.INTEGER, "ROUND(TIME_TO_SEC(TIMEDIFF (?1, ?2))/60)"));
		registerFunction("tochar", new SQLFunctionTemplate(Hibernate.STRING, "CAST(?1 AS CHAR)"));
		registerFunction("round", new SQLFunctionTemplate(Hibernate.DOUBLE, "ROUND(?1,?2)"));
		registerFunction("if", new SQLFunctionTemplate(Hibernate.STRING, "if(?1,?2,?3)"));
		registerFunction("ifnull", new SQLFunctionTemplate(Hibernate.STRING, "ifnull(?1,?2)"));
		//registerFunction("orderdatetime", new SQLFunctionTemplate(Hibernate.STRING, "if((ifnull(task.ACTUAL_START_TIME,task.PLAN_START_TIME)<ifnull(task.PLAN_START_TIME, task.ACTUAL_START_TIME)),ifnull(task.ACTUAL_START_TIME,task.PLAN_START_TIME),ifnull(task.PLAN_START_TIME, task.ACTUAL_START_TIME))"));
		registerFunction("orderdatetime", new SQLFunctionTemplate(Hibernate.STRING, "IF("+
		"IF(?1 IS NULL OR ?1='',?2,?1) <IF(?2 IS NULL OR ?2='',?1,?2),"+
		"IF(?1 IS NULL OR ?1='',?2,?1),IF(?2 IS NULL OR ?2='',?1,?2)"+
		")"));
		registerFunction("workingdays", new SQLFunctionTemplate(Hibernate.INTEGER, 
		"( SELECT COUNT(selected_date) FROM"+
		" (SELECT ADDDATE('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date FROM"+
		" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,"+
		" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,"+
		" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,"+
		" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,"+
		" (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v"+
		" WHERE selected_date BETWEEN ?1 AND ?2 AND selected_date NOT IN (SELECT HOLIDAY_DATE FROM holiday) AND DAYOFWEEK(selected_date) IN(2,3,4,5,6) )"));
		
  }
}
