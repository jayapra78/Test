/**
 * Copyright 2012 by Shirai, All rights reserved.
 *
 * 2012/06/20 <Base Line Version 1.0>
 */
package com.task.service.custom;

import org.apache.log4j.Logger;
import org.hibernate.exception.GenericJDBCException;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.transaction.TransactionDefinition;

import com.task.util.CustomApplicationException;
import com.task.util.LoggingAspectInterceptor;
import com.task.util.TransactionService;

public abstract class CommonService {
	private static Logger log = LoggingAspectInterceptor.getLog(CommonService.class);

	/**
	 * @Method : startTransaction
	 *
	 * @Description : The method to start the transaction
	 *
	 * @return TransactionService
	 *
	 * @throws CustomApplicationException
	 */
	public TransactionService startTransaction()
			throws CustomApplicationException {
		try {
			TransactionService transaction = new TransactionService(
					TransactionDefinition.PROPAGATION_REQUIRED);
			return transaction;
		}catch (GenericJDBCException e) {
			throw new CustomApplicationException(e);
		}catch (CannotCreateTransactionException e) {
			throw new CustomApplicationException(e);
		} catch (Exception e) {
			throw new CustomApplicationException("startTransaction", e);
		}
	}

	/**
	 * @Method : startNewTransaction
	 *
	 * @Description : The method to start the New Transaction
	 *
	 * @return TransactionService
	 *
	 * @throws CustomApplicationException
	 */
	public TransactionService startNewTransaction()
			throws CustomApplicationException {
		try {
			TransactionService transaction = new TransactionService(
					TransactionDefinition.PROPAGATION_REQUIRES_NEW);
			return transaction;
		} catch (Exception e) {
			throw new CustomApplicationException("startNewTransaction", e);
		}
	}

}
