/**
 * Creater / durgeshkumarjhaSRM copyright information to Shirai 2013
 * 
 */
package com.task.service.custom;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.task.common.MailVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.constant.ResultConstant;
import com.task.dao.custom.LeaveDao;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.util.MailSendEngine;
import com.task.util.TransactionService;
import com.task.vo.custom.action.ApprovedLeaveResultVo;
import com.task.vo.custom.action.LeaveActionVo;

/**
 * @author durgeshkumarjha
 * 
 */
public class LeaveService extends CommonService {
    private static Logger log = LoggingAspectInterceptor.getLog(LeaveService.class);

    /**
     * 
     * @Method :
     * @Description :
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo saveLeave(RequestVo requestVo)
	    throws CustomApplicationException {

	/* starting the transaction */
	TransactionService service = null;
	try {
	    service = super.startTransaction();
	    LeaveActionVo leaveActionVo = (LeaveActionVo) requestVo
		    .getCommonVo();
	    requestVo.setUserLocale(Locale.US);

	    /* creating instance for dao class bean */
	    LeaveDao leaveDao = (LeaveDao) ContextLoader
		    .getBean(FrameworkConstant.getBeanId("LEAVE_DAO"));

	    /* Calling leaveDao class saveLeave method */
	    ResponseVo responseVo = leaveDao.saveLeave(requestVo);
	    /* commiting the transaction */
	    service.commitTransaction();

	    responseVo.setTransactionMessage("Leave applied Successfully");

	    return responseVo;
	} catch (CustomApplicationException e) {
	    if (null != service)
		service.rollbackTransaction();
	    throw e;
	} catch (Exception ex) {
	    if (null != service)
		service.rollbackTransaction();
	    throw new CustomApplicationException(ex);
	}
    }

    /**
     * 
     * @Method :
     * @Description :
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo getLeaveDetail(RequestVo requestVo)
	    throws CustomApplicationException {
	try {
	    /* creating instance for dao class bean */
	    LeaveDao leaveDao = (LeaveDao) ContextLoader
		    .getBean(FrameworkConstant.getBeanId("LEAVE_DAO"));
	    /* Calling leaveDao getLeaveDetail method */
	    ResponseVo responseVo = (ResponseVo) leaveDao
		    .getLeaveDetail(requestVo);

	    return responseVo;
	} catch (CustomApplicationException e) {
	    throw e;
	} catch (Exception ex) {
	    throw new CustomApplicationException(ex);
	}
    }

    public ResponseVo updateLeave(RequestVo requestVo) throws CustomApplicationException {
	/* starting the transaction */
	TransactionService service = null;
	try {
	    service = super.startTransaction();

	    /* creating instance for dao class bean */
	    LeaveDao leaveDao = (LeaveDao) ContextLoader.getBean(FrameworkConstant.getBeanId("LEAVE_DAO"));

	    /* Calling leaveDao updateLeave method */
	    ResponseVo responseVo = leaveDao.updateLeave(requestVo);
	    ApprovedLeaveResultVo approvedLeaveVo = (ApprovedLeaveResultVo) responseVo.getCommonVo();
	    if (null != approvedLeaveVo){
		leaveFormBuilder(approvedLeaveVo);
	    }
	    /* Commiting the transaction */
	    service.commitTransaction();
	    responseVo.setTransactionMessage("Leave Status updated successfully");

	    return responseVo;

	} catch (CustomApplicationException e) {
	    if (null != service)
		service.rollbackTransaction();
	    throw e;
	} catch (Exception ex) {
	    if (null != service)
		service.rollbackTransaction();
	    throw new CustomApplicationException(ex);
	}
    }

    /**
     * 
     * @Method : listLeave
     * @Description : This method is used for getting all applied leave list.
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo listLeave(RequestVo requestVo)
	    throws CustomApplicationException {
	TransactionService service = null;
	ResponseVo responseVo = new ResponseVo();
	try {
	    service = super.startTransaction();
	    responseVo.setPagingVo(requestVo.getPagingVo());
	    CommonUtils.setStartRow(requestVo.getPagingVo(),
		    ResultConstant.RESULT_LEAVE_LIST);
	    /* creating instance for dao class bean */
	    LeaveDao leaveDao = (LeaveDao) ContextLoader
		    .getBean(FrameworkConstant.getBeanId("LEAVE_DAO"));

	    /* Calling leaveDao class saveLeave method */
	    responseVo = (ResponseVo) leaveDao.listLeave(requestVo);

	    if (null != responseVo && null != responseVo.getPagingVo())
		CommonUtils.setPagination(responseVo.getPagingVo());

	    service.commitTransaction();
	    return responseVo;
	} catch (CustomApplicationException e) {
	    if (null != service)
		service.rollbackTransaction();
	    throw e;
	} catch (Exception ex) {
	    if (null != service)
		service.rollbackTransaction();
	    throw new CustomApplicationException(ex);
	}
    }

    /**
     * 
     * @Method : comboLoading
     * @Description : This method is used for load combo.
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo comboLoading(RequestVo requestVo)
	    throws CustomApplicationException {
		/* Starting the transaction */
		try {
		    /* Creating DAO class instance */
		    LeaveDao leaveDao = (LeaveDao) ContextLoader
			    .getBean(FrameworkConstant.getBeanId("LEAVE_DAO"));
		    /* Calling DAO class comboLoading method */
		    ResponseVo responseVo = leaveDao.comboLoading(requestVo);
		    /* commiting the transaction */
		    return responseVo;
		} catch (Exception e) {
		    throw new CustomApplicationException(e);
		}
    }
    
    /**
     * @Method : leaveFormBuilder
     * @Description : To generate the Leave form in HTML to generate the PDF Document.
     * @param approvedLeaveVo
     * @return
     */
    private void leaveFormBuilder(ApprovedLeaveResultVo approvedLeaveVo) {
	
	try {
		String currentDate = CommonUtils.convertdateTostring(CommonUtils.getCurrentDateWithoutTime(), "dd-MM-yyyy");
			
	    	String fileName = "leaveForm_" + approvedLeaveVo.getEmpCode();
        	String leaveFormDir = CommonUtils.getLeaveFormDirectory();
        	
            	File dir = new File(leaveFormDir);
            	if (dir.exists()){
            	    FileDeleteStrategy.FORCE.delete(dir);
            	}
            	dir.mkdir();
            	String filePath = dir.getAbsoluteFile() + File.separator + fileName + ".pdf";
            	FileOutputStream fileOutStream = new FileOutputStream(filePath);
            	
        	String pdfContent= new String();
        	
        	StringBuilder leaveForm = new StringBuilder();
        	
        	// Leave form header creation
        	leaveForm.append("<html><head><title>Leave Form</title></head>");
        	
        	// Leave form Body creation
        	leaveForm.append("<body><table align='center' border='0' cellpadding='0' cellspacing='0' style='width: 100%'>");
        	leaveForm.append("<tbody><tr><td width='20%' valign='bottom'>");
        	leaveForm.append("<img src='" + CommonUtils.getLeaveFormLogoPath()+ "'/>");
        	leaveForm.append("</td><td width='30%' align='center' valign='bottom' style='font-size:18px;'>");
        	leaveForm.append("<b>LEAVE FORM</b></td><td width='20%' align='right' valign='bottom'>HR - 02</td></tr></tbody></table>");
        	
        	// Leave details content starting
        	leaveForm.append("<table align='center' border='1' cellpadding='0' cellspacing='0' style='width: 100%'><tbody><tr height='50px'>");
        	
        	// Emp Name 1'st row starts
        	leaveForm.append("<td width='16%'>&nbsp;&nbsp;Emp. Name</td><td width='29%' style='font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getEmpName() + "</td>");
        	// Emp ID and 1'st row end
        	leaveForm.append("<td width='16%'>&nbsp;&nbsp;Emp. id</td><td width='32%' style='font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getEmpCode() + "</td></tr>");
        	
        	// Emp Designation 2'nd row starts
        	leaveForm.append("<tr height='48px'><td width='16%'>&nbsp;&nbsp;Designation</td><td width='29%' style='font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getDesignation() + "</td>");
        	// Emp Department 2'nd row end
        	leaveForm.append("<td width='16%'>&nbsp;&nbsp;Department</td><td width='32%' style='font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getDeptName()+ "</td></tr></tbody></table>");
        	
        	// 3'rd Row heading start
        	leaveForm.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='width: 100%'><tr><td>&nbsp;</td></tr></table>");
        	leaveForm.append("<table align='center' border='1' cellpadding='0' cellspacing='0' style='width: 100%'>");
        	leaveForm.append("<tbody><tr height='30px'><td width='18%' align='left' nowrap='nowrap'>&nbsp;&nbsp;Kind of Leave</td>");
        	leaveForm.append("<td width='16%' align='center' nowrap='nowrap'>From</td>");
        	leaveForm.append("<td width='16%' align='center' nowrap='nowrap'>To</td>");
        	leaveForm.append("<td width='18%' align='left' nowrap='nowrap'>&nbsp;&nbsp;No. of Days</td>");
        	leaveForm.append("<td width='20%' align='center' nowrap='nowrap'>Date of Rejoining</td>");
        	leaveForm.append("<td width='12%' nowrap='nowrap' align='center' nowrap='nowrap'>Remarks</td></tr>");
        	
        	// 4'th Row started
        	leaveForm.append("<tr height='70px' style='text-align: center;'><td style='font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getLeaveType()+ "</td>");
        	leaveForm.append("<td style='text-align: center;font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getFromDate()+ "</td>");
        	leaveForm.append("<td style='text-align: center;font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getToDate()+ "</td>");
        	leaveForm.append("<td style='text-align: center;font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getNoOfDays()+ "</td>");
        	leaveForm.append("<td style='text-align: center;font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getRejoinDate()+ "</td>");
        	leaveForm.append("<td style='text-align: center;font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getRemarks()+ "</td></tr>");
        	
        	// 5'th Row started
        	leaveForm.append("<tr height='70px'><td width='50%' colspan='3' valign='top'>&nbsp;&nbsp;Reason for Leave:&nbsp;");
        	leaveForm.append("<table align='left' border='0' cellpadding='0' cellspacing='0' style='width: 100%'>");
        	leaveForm.append("<tbody><tr><td style='font-family: Arial, verdena, sans-serif;font-size: 11px;'>&nbsp;&nbsp;" + approvedLeaveVo.getReason()+ "</td></tr></tbody></table></td>");
        	leaveForm.append("<td width='50%' colspan='3' valign='bottom'>&nbsp;");
        	leaveForm.append("<table align='left' border='0' cellpadding='0' cellspacing='0' style='width: 100%'>");
//        	leaveForm.append("<tbody><tr><td>" + approvedLeaveVo.getReason()+ "</td></tr></tbody></table>");
        	leaveForm.append("<tbody><tr><td style='font-family: Arial, verdena, sans-serif;font-size: 11px;'>&nbsp;&nbsp;"+approvedLeaveVo.getEmpName()+"["+currentDate+"]"+"</td></tr></tbody></table>");
        	leaveForm.append("<br/>&nbsp;&nbsp;Signature of Employee with date:</td></tr>");
        	
        	// 6'th Row started
        	leaveForm.append("<tr height='40px'><td width='50%' height='50px' colspan='3'><table><tr>");
        	leaveForm.append("<td height='50px' width='100%' nowrap='nowrap' valign='top'>&nbsp;&nbsp;Address during Leave:&nbsp;");
        	leaveForm.append("<table align='left' border='0' cellpadding='0' cellspacing='0' style='width: 100%'>");
        	leaveForm.append("<tbody><tr><td style='font-family: Arial, verdena, sans-serif;font-size: 11px;'>&nbsp;&nbsp;" + approvedLeaveVo.getAddress()+ "</td></tr></tbody></table></td></tr></table>");
        	leaveForm.append("<table width='100%'><tr><td width='13%'>&nbsp;&nbsp;Tel.No.</td>");
        	leaveForm.append("<td width='16%' style='font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getContactNo()+ "</td>");
        	leaveForm.append("<td width='10%' nowrap='nowrap'>E-mail</td>");
        	leaveForm.append("<td width='26%' style='font-family: Arial, verdena, sans-serif;font-size: 11px;'>" + approvedLeaveVo.getEmail()+ "</td></tr></table></td>");
        	leaveForm.append("<td width='50%' height='50px' colspan='3'><table><tr><td height='50px' nowrap='nowrap' valign='top'>");
        	leaveForm.append("&nbsp;&nbsp;Sanctioned&nbsp;<table><tbody><tr><td style='font-family: Arial, verdena, sans-serif;font-size: 11px;'><br/>&nbsp;&nbsp;PM Approved ["+approvedLeaveVo.getPmCode()+"], SBU Head Approved[A0294]["+currentDate+"]</td></tr></tbody></table></td></tr>");
        	leaveForm.append("<tr><td nowrap='nowrap' width='100%'>&nbsp;&nbsp;Signature of Sanctioning Authority with date</td></tr></table></td></tr>");
        	
        	// 7'th Row started
        	leaveForm.append("<tr height='70px'><td width='50%' colspan='6' valign='top'>&nbsp;&nbsp;For Office Use:</td></tr></tbody></table></body></html>");
        	
        	pdfContent = leaveForm.toString();
        	generatePdf(fileOutStream ,pdfContent);
        	
        	ResourceBundle reBundle = ResourceBundle.getBundle("Mail_Config");
        	
        	String mailBody = reBundle.getString("mail.leaveform.message.body")+ approvedLeaveVo.getEmpCode();
        	String mailSubject = reBundle.getString("mail.leaveform.subject")+ approvedLeaveVo.getEmpCode();
        	String mailHeading = reBundle.getString("mail.leaveform.message.heading");
	    	String autoGenerate = reBundle.getString("mail.dontreply.message");
        	
        	MailVo mailVo = new MailVo();
        	String[] mailTo = new String[]{reBundle.getString("leaveform.mail.to.address")};
        	List<String> ccList = new ArrayList<String>();
        	
        	String tempCc = reBundle.getString("leaveform.mail.cc.address1");
        	
        	if (null != tempCc && !tempCc.isEmpty()){
        		ccList.add(tempCc);
        	}
        	
        	tempCc = reBundle.getString("leaveform.mail.cc.address2");
        	if (null != tempCc && !tempCc.isEmpty()){
        		ccList.add(tempCc);
        	}
        	
        	tempCc = reBundle.getString("leaveform.mail.cc.address3");
        	if (null != tempCc && !tempCc.isEmpty()){
        		ccList.add(tempCc);
        	}
        	
	    	mailVo.setSubject(mailSubject);
	    	mailVo.setAttachment(filePath);
	    	mailVo.setMailTo(mailTo);
    	    mailVo.setMailCc(ccList.toArray(new String[ccList.size()]));
	    	
	    	StringBuilder msgBuilder = new StringBuilder();
	    	
	    	msgBuilder.append("<html><head><title></title></head><body>");
	    	msgBuilder.append("<table border='0' cellpadding='0' cellspacing='0' style='width: 100%;'><tbody><tr>");
	    	msgBuilder.append("<td>" + mailHeading + "</td></tr><tr>");
	    	msgBuilder.append("<td style='text-align: left;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
	    	msgBuilder.append(mailBody + "&nbsp;");
	    	msgBuilder.append("</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>");
	    	msgBuilder.append("<tr><td>" + autoGenerate + "</td></tr>");
	    	msgBuilder.append("</tbody></table></body></html>");
	    	
	    	
	    	mailVo.setMessage(msgBuilder.toString());
	    	
	    	sendMail(mailVo);
	
//	return pdfContent;
	}catch (Exception e) {
	    log.error(e);
	}
    }
    
    /**
     * @Method : generatePdf
     * @Description : To generate PDF to attach in mail
     * @param fileName
     * @param pdfContent
     */
    private void generatePdf(FileOutputStream fileName, String pdfContent){
	try {
	    	Document document = new Document(PageSize.A4);
		PdfWriter pdfWriter = PdfWriter.getInstance(document, fileName);
		document.open();
		XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
		worker.parseXHtml(pdfWriter, document, new StringReader(pdfContent));
		document.close();

	} catch (Exception e) {
	    log.error(e);
	}
	
    }
    
    private boolean sendMail(MailVo mailVo) {

	MailSendEngine mailSendEngine = new MailSendEngine();
	try {
	    mailSendEngine.sendmail(mailVo);
	} catch (Exception e) {
	    log.error(e);
	    return false;
	}
	return true;
    }

}
