/**
 * Copyright(C) 2012 Shirai Group, Tokyo, Japan All rights reserved.
 */
package com.task.service.custom;

import java.util.HashMap;
import java.util.Map;

import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.SessionVo;
import com.task.dao.custom.LoginDao;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;

/**
 * LoginService for user authenticate and permission.
 *
 * @author Sengottaiyan [SRM]
 */
public class LoginService extends CommonService {

	/** sessionVo instance to set session */
	private SessionVo sessionVo = new SessionVo();

	/** responseVo instance to get data from dao layer */
	private ResponseVo responseVo = new ResponseVo();

	Map<String, String> policyToCompanyMap = new HashMap<String, String>();

	/**
	 * @Method : authenticateUser
	 * @Description : authenticate the login user
	 *
	 * @param requestVo
	 *
	 * @return ResponseVo
	 *
	 * @throws CustomApplicationException
	 */
	public ResponseVo authenticateUser(RequestVo requestVo) throws CustomApplicationException {
		responseVo = new ResponseVo();
		try {
			responseVo = getUserData(requestVo);
			return responseVo;
		}catch (CustomApplicationException e) {
			throw new CustomApplicationException(e);
		} catch (Exception e) {
			throw new CustomApplicationException(e);
		}
	}

	private ResponseVo getUserData(RequestVo requestVo) throws CustomApplicationException {
	    /** Get LoginDao */
		LoginDao loginDao = (LoginDao) ContextLoader.getBean(FrameworkConstant.getBeanId("DAO_LOGIN"));
		responseVo = (ResponseVo) loginDao.authenticateUser(requestVo);


	    return responseVo;
	}


	/**
	 * Getter method for sessionVo
	 *
	 * @return the sessionVo (SessionVo)
	 */
	public SessionVo getSessionVo() {
		return sessionVo;
	}

	/**
	 * Setter method for sessionVo
	 *
	 * @param sessionVo the sessionVo to set
	 */
	public void setSessionVo(SessionVo sessionVo) {
		this.sessionVo = sessionVo;
	}

	/**
	 * Getter method for responseVo
	 *
	 * @return the responseVo (ResponseVo)
	 */
	public ResponseVo getResponseVo() {
		return responseVo;
	}

	/**
	 * Setter method for responseVo
	 *
	 * @param responseVo the responseVo to set
	 */
	public void setResponseVo(ResponseVo responseVo) {
		this.responseVo = responseVo;
	}

	/**
	 * Getter method for policyToCompanyMap
	 *
	 * @return the policyToCompanyMap (Map<String,String>)
	 */
	public Map<String, String> getPolicyToCompanyMap() {
		return policyToCompanyMap;
	}

	/**
	 * Setter method for policyToCompanyMap
	 *
	 * @param policyToCompanyMap the policyToCompanyMap to set
	 */
	public void setPolicyToCompanyMap(Map<String, String> policyToCompanyMap) {
		this.policyToCompanyMap = policyToCompanyMap;
	}


}
