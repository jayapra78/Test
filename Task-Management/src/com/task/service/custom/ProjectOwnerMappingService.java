/**
 * Creater / DurgeshKumarJha[SRM] copyright information to SRM Technologies 2013
 * 
 */
package com.task.service.custom;

import java.util.Locale;

import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.constant.ResultConstant;
import com.task.dao.custom.ProjectOwnerMappingDao;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;
import com.task.util.TransactionService;
import com.task.vo.custom.action.ProjectOwnerMappingActionVo;

/**
 * This Service class is used to Add, Update, View, Search and Delete Project Owner Mapping.
 * 
 * @author DurgeshKumarJha[SRM]
 * @since 22/02/2013
 * 
 */
public class ProjectOwnerMappingService extends CommonService {

    
    /**
     * @Method : saveProjectOwnerMapping
     * @Description : To save the Project Owner Mapping details
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo saveProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException {
		
    	/* starting the transaction */
		TransactionService service = null;
		try {
			service = super.startTransaction();
			ProjectOwnerMappingActionVo projectOwnerMappingActionVo = (ProjectOwnerMappingActionVo) requestVo.getCommonVo();
			requestVo.setUserLocale(Locale.US);

			/* creating instance for dao class bean */
			ProjectOwnerMappingDao projectOwnerMappingDao =
					(ProjectOwnerMappingDao) ContextLoader.getBean(FrameworkConstant
							.getBeanId("PROJECT_OWNER_MAPPING_DAO"));
			
			/* Calling projectOwnerMappingDao class saveProjectOwnerMapping method */
			ResponseVo responseVo = projectOwnerMappingDao.saveProjectOwnerMapping(requestVo);
			/* commiting the transaction */
			service.commitTransaction();
			
			responseVo.setTransactionMessage(FrameworkConstant.getMessage("Insertedsuccess",
					requestVo.getUserLocale(), projectOwnerMappingActionVo.getProjectName()));
			
			return responseVo;
		} catch (CustomApplicationException e) {
			if (null != service)
				service.rollbackTransaction();
			throw e;
		} catch (Exception ex) {
			if (null != service)
				service.rollbackTransaction();
			throw new CustomApplicationException(ex);
		}
	}
    
    /**
     * @Method : showProjectOwnerMapping
     * @Description : To show the Project Owner mapping's
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo showProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException{
	TransactionService service = null;
	ResponseVo responseVo = new ResponseVo();
	try{
		/* starting the transaction */
		service = super.startTransaction();
		/* creating instance for dao class bean */
		ProjectOwnerMappingDao mappingDao = (ProjectOwnerMappingDao) ContextLoader.getBean(FrameworkConstant.getBeanId("PROJECT_OWNER_MAPPING_DAO"));
		
		/* Calling projectOwnerMappingDao class showProjectOwnerMapping method */
		responseVo = (ResponseVo) mappingDao.showProjectOwnerMapping(requestVo);
		if(responseVo != null && responseVo.getPagingVo() != null){
			CommonUtils.setPagination(responseVo.getPagingVo());
		}
		/* commiting the transaction */
		service.commitTransaction();
		return responseVo;
	} catch (CustomApplicationException e) {
		if (null != service)
			service.rollbackTransaction();
		throw e;
	} catch (Exception ex) {
		if (null != service)
			service.rollbackTransaction();
		throw new CustomApplicationException(ex);
	}
    }
    
    /**
     * @Method : comboLoading
     * @Description : Method to load the Combo Loading
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo comboLoading(RequestVo requestVo) throws CustomApplicationException {
    	/*Starting the transaction*/
	try {
	    /*Creating DAO class instance*/
	    ProjectOwnerMappingDao mappingDao = (ProjectOwnerMappingDao) ContextLoader.getBean(FrameworkConstant.getBeanId("PROJECT_OWNER_MAPPING_DAO"));
	    /*Calling DAO class comboLoading method*/
	    ResponseVo responseVo = mappingDao.comboLoading(requestVo);
	    /* commiting the transaction */
	    return responseVo;
	} catch (Exception e) {
	    throw new CustomApplicationException(e);
	}
    }

	/**
	 * @Method : listProjectOwnerMapping
	 * @Description : Method is used for displaying list.
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException 
	 */
	public ResponseVo listProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException {
		/* starting the transaction */
		TransactionService service = null;
		try {
			service = super.startTransaction();
			ResponseVo responseVo = new ResponseVo();
			responseVo.setPagingVo(requestVo.getPagingVo());
			/* Pagination */
			CommonUtils.setStartRow(requestVo.getPagingVo(), ResultConstant.RESULT_PROJECT_OWNER_LIST);
			
			/* creating instance for dao class bean */
			ProjectOwnerMappingDao projectOwnerMappingDao =
					(ProjectOwnerMappingDao) ContextLoader.getBean(FrameworkConstant
							.getBeanId("PROJECT_OWNER_MAPPING_DAO"));
			/* calling the companyDao getAll method */
			responseVo = (ResponseVo) projectOwnerMappingDao.listProjectOwnerMapping(requestVo);

			if (null != responseVo && null != responseVo.getPagingVo())
				CommonUtils.setPagination(responseVo.getPagingVo());
			/* commiting the transaction */
			service.commitTransaction();
			return responseVo;
		} catch (CustomApplicationException e) {
			if (null != service)
				service.rollbackTransaction();
			throw e;
		} catch (Exception ex) {
			if (null != service)
				service.rollbackTransaction();
			throw new CustomApplicationException(ex);
		}
	}

	/**
	 * 
	 * @Method : deleteProjectOwnerMapping
	 * @Description : This method is for deleting Project Owner Mapping.
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo deleteProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException {
		
		/* starting the transaction */
		TransactionService service = null;
		try {
			service = super.startTransaction();
			/* creating instance for dao class bean */
			ProjectOwnerMappingDao projectOwnerMappingDao =
					(ProjectOwnerMappingDao) ContextLoader.getBean(FrameworkConstant
							.getBeanId("PROJECT_OWNER_MAPPING_DAO"));
			/* Calling CompanyDao deleteCompany method */
			ResponseVo responseVo = projectOwnerMappingDao.deleteProjectOwnerMapping(requestVo);
			/* Commiting the transaction */
			service.commitTransaction();
			String deleteParam[] = requestVo.getParamIds().split(",");
			if (deleteParam.length == 1) {
				responseVo.setTransactionMessage(" Project deleted successfully");
			} else {
				responseVo.setTransactionMessage(" Projects deleted successfully");
			}
			
			return responseVo;
		} catch (CustomApplicationException e) {
			if (null != service)
				service.rollbackTransaction();
			throw e;
		} catch (Exception ex) {
			if (null != service)
				service.rollbackTransaction();
			throw new CustomApplicationException(ex);
		}
	}

	/**
	 * @Method : getProjectOwnerMapping
	 * @Description : This method is used to take project owner mapping detail for view and modify.
	 * @param requestVo
	 * @return
	 */
	public ResponseVo getProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException {
		try {
			/* creating instance for dao class bean */
			ProjectOwnerMappingDao projectOwnerMappingDao =
					(ProjectOwnerMappingDao) ContextLoader.getBean(FrameworkConstant
							.getBeanId("PROJECT_OWNER_MAPPING_DAO"));
			/* Calling projectOwnerMappingDao getProjectOwnerMapping method */
			ResponseVo responseVo = (ResponseVo) projectOwnerMappingDao.getProjectOwnerMapping(requestVo);

			return responseVo;
		} catch (CustomApplicationException e) {
			throw e;
		} catch (Exception ex) {
			throw new CustomApplicationException(ex);
		}
	}

	public ResponseVo viewProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException {
		try {
			/* creating instance for dao class bean */
			ProjectOwnerMappingDao projectOwnerMappingDao =
					(ProjectOwnerMappingDao) ContextLoader.getBean(FrameworkConstant
							.getBeanId("PROJECT_OWNER_MAPPING_DAO"));
			/* Calling projectOwnerMappingDao getProjectOwnerMapping method */
			ResponseVo responseVo = (ResponseVo) projectOwnerMappingDao.viewProjectOwnerMapping(requestVo);

			return responseVo;
		} catch (CustomApplicationException e) {
			throw e;
		} catch (Exception ex) {
			throw new CustomApplicationException(ex);
		}
	}
	
	/**
	 * 
	 * @Method : updateProjectOwnerMapping
	 * @Description :  This method is used for updating existing project maping.
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo updateProjectOwnerMapping(RequestVo requestVo) throws CustomApplicationException{
		/* starting the transaction */
		TransactionService service = null;
		try {
			service = super.startTransaction();
			
			ProjectOwnerMappingActionVo projectOwnerMappingActionVo = (ProjectOwnerMappingActionVo) requestVo.getCommonVo();
			requestVo.setUserLocale(Locale.US);
			
			/* creating instance for dao class bean */
			ProjectOwnerMappingDao projectOwnerMappingDao =
					(ProjectOwnerMappingDao) ContextLoader.getBean(FrameworkConstant
							.getBeanId("PROJECT_OWNER_MAPPING_DAO"));
			/* Calling projectOwnerMappingDao updateProjectOwnerMapping method */
			ResponseVo responseVo = projectOwnerMappingDao.updateProjectOwnerMapping(requestVo);
			/* Commiting the transaction */
			service.commitTransaction();
			
			responseVo.setTransactionMessage(FrameworkConstant.getMessage("Updatedsuccess",
					requestVo.getUserLocale(), projectOwnerMappingActionVo.getProjectName()));
			
			return responseVo;

		} catch (CustomApplicationException e) {
			if (null != service)
				service.rollbackTransaction();
			throw e;
		} catch (Exception ex) {
			if (null != service)
				service.rollbackTransaction();
			throw new CustomApplicationException(ex);
		}
	}

	
}
