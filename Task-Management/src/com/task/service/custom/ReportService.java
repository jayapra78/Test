/**
 * Creater / balamurugankSRM copyright information to Shirai 2013
 * 
 */
package com.task.service.custom;

import org.apache.log4j.Logger;

import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.dao.custom.ReportDao;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.util.TransactionService;

/**
 * @author balamurugank
 * 
 */
public class ReportService extends CommonService {

    /** sessionVo instance to set session */

    private static Logger log = LoggingAspectInterceptor
	    .getLog(ReportService.class);

    /** responseVo instance to get data from dao layer */
    private ResponseVo responseVo = new ResponseVo();
    
    /**
     * @Method : reportList
     * @Description : Method to get the result for Report
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo reportList(RequestVo requestVo) throws CustomApplicationException{
	
	log.info("Report Service Class : reportList Method started");
	TransactionService service = null;
	ResponseVo responseVo = new ResponseVo();
	try{
		ReportDao reportDao = (ReportDao) ContextLoader.getBean(FrameworkConstant.getBeanId("REPORT_DAO"));
		
		responseVo = (ResponseVo) reportDao.reportList(requestVo);
		log.info("ReportService Class : reportList Method ends");
		return responseVo;
	} catch (Exception ex) {
		if (null != service)
			service.rollbackTransaction();
		log.error("Exception in reportList method in ReportService class");
		log.error(ex);
		throw new CustomApplicationException(ex);
	}
	
    }
    
    /**
     * @Method : reportList
     * @Description : Method to get the result for Report
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo empEffortReport(RequestVo requestVo) throws CustomApplicationException{
		ResponseVo responseVo = new ResponseVo();
		try{
			ReportDao reportDao = (ReportDao) ContextLoader.getBean(FrameworkConstant.getBeanId("REPORT_DAO"));
			responseVo = (ResponseVo) reportDao.empEffortReport(requestVo);
			return responseVo;
		} catch (Exception ex) {
			log.error("empEffortReport:"+ex);
			throw new CustomApplicationException(ex);
		}
	
    }
    
    /**
     * 
     * @Method : empWorkDetailReport
     * @Description : This method is used to generate employee work detail report.
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo empWorkDetailReport(RequestVo requestVo) throws CustomApplicationException{
		ResponseVo responseVo = new ResponseVo();
		try{
			ReportDao reportDao = (ReportDao) ContextLoader.getBean(FrameworkConstant.getBeanId("REPORT_DAO"));
			
			// invoking ReportDao class empWorkDetailReport method.
			responseVo = (ResponseVo) reportDao.empWorkDetailReport(requestVo);
			return responseVo;
		} catch (Exception ex) {
			log.error("empEffortReport:"+ex);
			throw new CustomApplicationException(ex);
		}
	
    }
    
    /**
     * @Method : comboLoading
     * @Description : Method to load all the Combo Values
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo comboLoading(RequestVo requestVo) throws CustomApplicationException  {
		try {
			ReportDao reportDao = (ReportDao) ContextLoader.getBean(FrameworkConstant.getBeanId("REPORT_DAO"));
			ResponseVo responseVo = reportDao.comboLoading(requestVo);
			return responseVo;
		} catch (Exception e){
			throw new CustomApplicationException(e);
		}
    }

    /**
     * Getter method for responseVo
     * 
     * @return the responseVo (ResponseVo)
     */
    public ResponseVo getResponseVo() {
        return responseVo;
    }

    /**
     * Setter method for responseVo
     *
     * @param responseVo the responseVo to set
     */
    public void setResponseVo(ResponseVo responseVo) {
        this.responseVo = responseVo;
    }

}
