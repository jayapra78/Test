/**
 * Creater / balamurugankSRM copyright information to Shirai 2013
 * 
 */
package com.task.service.custom;

import org.apache.log4j.Logger;

import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.dao.custom.ResourceAllocationDao;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.util.TransactionService;

/**
 * @author balamurugank
 *
 */
public class ResourceAllocationService extends CommonService {
    
    /** sessionVo instance to set session */

    private static Logger log = LoggingAspectInterceptor.getLog(ResourceAllocationService.class);

    /** responseVo instance to get data from dao layer */
    private ResponseVo responseVo = new ResponseVo();
    
    /**
     * @Method : comboLoading
     * @Description : Method to load all the Combo Values
     * @param requestVo
     * @return
     * @throws CustomApplicationException
     */
    public ResponseVo comboLoading(RequestVo requestVo) throws CustomApplicationException  {
		try {
			ResourceAllocationDao resourceDao = (ResourceAllocationDao) ContextLoader.getBean(FrameworkConstant.getBeanId("RESOURCE_ALLOCATION_DAO"));
			ResponseVo responseVo = resourceDao.comboLoading(requestVo);
			return responseVo;
		} catch (Exception e){
			throw new CustomApplicationException(e);
		}
    }
    
    public ResponseVo saveResource(RequestVo requestVo) throws CustomApplicationException {
		TransactionService service = null;
		ResponseVo responseVo = new ResponseVo();
		try{
			service = super.startNewTransaction();
			ResourceAllocationDao resourceDao = (ResourceAllocationDao) ContextLoader.getBean(FrameworkConstant.getBeanId("RESOURCE_ALLOCATION_DAO"));
			responseVo = resourceDao.saveResource(requestVo);
			service.commitTransaction();
			return responseVo;
		} catch (CustomApplicationException e) {
			if (null != service)
			service.rollbackTransaction();
			throw e;
		} catch (Exception ex) {
			if (null != service)
			service.rollbackTransaction();
			throw new CustomApplicationException(ex);
		}
	}
    
    /**
     * @Method : getProjectDetails
     * @Description : Method to get the Project Details based on Project ID
     * @param paramValue
     * @return ResponseVo
     * @throws CustomApplicationException
     */
    public ResponseVo getProjectDetails(RequestVo requestVo) throws CustomApplicationException{
	TransactionService service = super.startTransaction();
	try {
	    ResourceAllocationDao resourceDao = (ResourceAllocationDao) ContextLoader.getBean(FrameworkConstant.getBeanId("RESOURCE_ALLOCATION_DAO"));
	    ResponseVo responseVo = resourceDao.getProjectDetails(requestVo);
	    return responseVo;
	} catch (Exception e){
	    service.rollbackTransaction();
	    throw new CustomApplicationException(e);
	}
    }

}
