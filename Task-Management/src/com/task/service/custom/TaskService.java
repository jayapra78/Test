/**
 * Copyright(C) 2012 Shirai Group, Tokyo, Japan All rights reserved.
 */
package com.task.service.custom;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.task.action.custom.TaskAction;
import com.task.common.ComboVo;
import com.task.common.MailVo;
import com.task.common.RequestVo;
import com.task.common.ResponseVo;
import com.task.common.UserVo;
import com.task.dao.custom.TaskDao;
import com.task.util.CommonUtils;
import com.task.util.ContextLoader;
import com.task.util.CustomApplicationException;
import com.task.util.EmailMessageBuilder;
import com.task.util.FrameworkConstant;
import com.task.util.LoggingAspectInterceptor;
import com.task.util.MailSendEngine;
import com.task.util.MailSendResponse;
import com.task.util.TransactionService;
import com.task.vo.custom.action.TaskActionVo;

/**
 *
 *
 * @author Neethimohan
 */
public class TaskService extends CommonService {

	/** sessionVo instance to set session */

	private static Logger log = LoggingAspectInterceptor.getLog(TaskAction.class);

	/** responseVo instance to get data from dao layer */
	private ResponseVo responseVo = new ResponseVo();

	Map<String, String> policyToCompanyMap = new HashMap<String, String>();

	/**
	 *
	 * @Method :
	 * @Description :
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo saveBulkTaskList(RequestVo requestVo) throws CustomApplicationException {
		TransactionService service = null;
		ResponseVo responseVo = new ResponseVo();
		try{
			service = super.startNewTransaction();

			TaskDao taskDao = (TaskDao) ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_DAO"));
			responseVo = taskDao.saveBulkTask(requestVo);

			service.commitTransaction();

			return responseVo;

		} catch (CustomApplicationException e) {
			if (null != service)
			service.rollbackTransaction();
			log.error(e);
			throw e;
		} catch (Exception ex) {
			if (null != service)
			service.rollbackTransaction();
			log.error(ex);
			throw new CustomApplicationException(ex);
		}
	}

	/**
	 *
	 * @Method :
	 * @Description :
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo saveTaskList(RequestVo requestVo) throws CustomApplicationException {
		TransactionService service = null;
		ResponseVo responseVo = new ResponseVo();
		try{
			service = super.startNewTransaction();

			TaskDao taskDao = (TaskDao) ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_DAO"));
			responseVo = taskDao.saveTaskList(requestVo);

			service.commitTransaction();

			if(requestVo.isSaveAndSend() && (responseVo.getErrorMessage()==null || "".equals(responseVo.getErrorMessage()))){
				sendIndividualTaskMail(requestVo.getUserVo(),taskDao,requestVo.getCc());
			}
			/*responseVo.setTransactionMessage(FrameworkConstant.getMessage("Insertedsuccess", requestVo.getUserLocale(),actionVo
					.getTaskId()));*/

			return responseVo;

		} catch (CustomApplicationException e) {
			if (null != service)
			service.rollbackTransaction();
			log.error(e);
			throw e;
		} catch (Exception ex) {
			if (null != service)
			service.rollbackTransaction();
			log.error(ex);
			throw new CustomApplicationException(ex);
		}
	}





	/**
	 * @Method :
	 * @Description :
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */

	public ResponseVo taskList(RequestVo requestVo) throws CustomApplicationException{
		ResponseVo responseVo = new ResponseVo();
		try{
			TaskDao taskDao = (TaskDao) ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_DAO"));
			responseVo = (ResponseVo) taskDao.taskList(requestVo);
			if(responseVo != null && responseVo.getPagingVo() != null){
				CommonUtils.setPagination(responseVo.getPagingVo());
			}
			return responseVo;
		} catch (CustomApplicationException e) {
			throw e;
		} catch (Exception ex) {
			throw new CustomApplicationException(ex);
		}
	}

	/**
	 *
	 * @Method	 :
	 * @Description :
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo comboLoading(RequestVo requestVo) throws CustomApplicationException  {
		try {
			// Copy Properties
			TaskDao taskDao = (TaskDao) ContextLoader.getBean(FrameworkConstant
							.getBeanId("TASK_DAO"));

			ResponseVo responseVo = taskDao.comboLoading(requestVo);

			return responseVo;
		} catch (Exception e){
			throw new CustomApplicationException(e);
		}
	}



	/**
	 * Getter method for responseVo
	 *
	 * @return the responseVo (ResponseVo)
	 */
	public ResponseVo getResponseVo() {
		return responseVo;
	}



	/**
	 * Setter method for responseVo
	 *
	 * @param responseVo the responseVo to set
	 */
	public void setResponseVo(ResponseVo responseVo) {
		this.responseVo = responseVo;
	}

	/**
	 * @Method :
	 * @Description :
	 */
	public void getTaskListMail(){
        TransactionService service = null;
        try{
            TaskDao taskDao = (TaskDao)ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_DAO"));

            Map<String,List<ComboVo>> sbuNoTaskEmpMap = new HashMap<String,List<ComboVo>>();

            ResponseVo empVo = taskDao.getNoTaskEmployees(new RequestVo());

            if(empVo!=null && empVo.getResponseList()!=null && empVo.getResponseList().size()>0){

            	List<ComboVo> noTaskEmpList = (List<ComboVo>)empVo.getResponseList();

            	for (ComboVo comboVo : noTaskEmpList) {

            		String sbuMail = comboVo.getCommonName();//SBU Email

            		List<ComboVo> empList = sbuNoTaskEmpMap.get(sbuMail);

                	if(empList==null){
                		empList = new ArrayList<ComboVo>();
                		sbuNoTaskEmpMap.put(sbuMail,empList);
                	}
                	if(!empList.contains(comboVo)){
                		empList.add(comboVo);
                	}
				}
            }

            Map<String,Map<String,List<TaskActionVo>>> sbuMap = new HashMap<String,Map<String,List<TaskActionVo>>>();

            ResponseVo responseVo = taskDao.getConsolidatedTaskMailDetails(new RequestVo());

            if(null != responseVo && null != responseVo.getResponseList() && responseVo.getResponseList().size() > 0){

            	List<TaskActionVo> taskActionVoList = (List<TaskActionVo>) responseVo.getResponseList();

            	// PM Email Start

            	Map<String,Map<String,Map<String,List<TaskActionVo>>>> pmMap = new HashMap<String,Map<String,Map<String,List<TaskActionVo>>>>();

            	for (TaskActionVo taskActionVo : taskActionVoList) {

                	String pmEmail      = taskActionVo.getPmEmailId();
                	String projectType  = taskActionVo.getProjectTypeActionVo().getProjectType();
                	String empId  		= taskActionVo.getEmployeeId();

                	Map<String,Map<String,List<TaskActionVo>>> ptMap = pmMap.get(pmEmail);

                	if(ptMap==null){
                		ptMap = new HashMap<String,Map<String,List<TaskActionVo>>>();
                		pmMap.put(pmEmail,ptMap);
                	}

                	Map<String,List<TaskActionVo>> empMap = ptMap.get(projectType);
                	if(empMap==null){
                		empMap = new HashMap<String,List<TaskActionVo>>();
                		ptMap.put(projectType,empMap);
                	}

                	List<TaskActionVo> empTaskVoList = empMap.get(empId);
                	if(empTaskVoList==null){
                		empTaskVoList = new ArrayList<TaskActionVo>();
                		empMap.put(empId, empTaskVoList);
                	}
                	if(!empTaskVoList.contains(taskActionVo)){
                		empTaskVoList.add(taskActionVo);
                	}
				}

            	sendConsolidatedPmMail(pmMap);

            	// PM Email End

            	// Project wise mail start

            	Map<String,Map<String,List<TaskActionVo>>> projectMap = new HashMap<String,Map<String,List<TaskActionVo>>>();

            	for (TaskActionVo taskActionVo : taskActionVoList) {

                	String projectName  = taskActionVo.getProjectName();
                	String empId  		= taskActionVo.getEmployeeId();

                	Map<String,List<TaskActionVo>> empMap = projectMap.get(projectName);

                	if(empMap==null){
                		empMap = new HashMap<String,List<TaskActionVo>>();
                		projectMap.put(projectName,empMap);
                	}

                	List<TaskActionVo> empTaskVoList = empMap.get(empId);
                	if(empTaskVoList==null){
                		empTaskVoList = new ArrayList<TaskActionVo>();
                		empMap.put(empId, empTaskVoList);
                	}
                	if(!empTaskVoList.contains(taskActionVo)){
                		empTaskVoList.add(taskActionVo);
                	}
				}

            	sendConsolidatedProjectMail(projectMap);

            	// Project wise mail end

            	// SBU Head Mail Settings Start

            	Collections.sort(taskActionVoList);

            	//Map<String,List<TaskActionVo>> empMap = new HashMap<String,List<TaskActionVo>>();

            	for (TaskActionVo taskActionVo : taskActionVoList) {

            		String sbuEmail = taskActionVo.getSbuEmailId();
            		String empId   = taskActionVo.getEmployeeId();

            		Map<String,List<TaskActionVo>> empMap = sbuMap.get(sbuEmail);

            		if(empMap==null){
            			empMap = new HashMap<String,List<TaskActionVo>>();
            			sbuMap.put(sbuEmail,empMap);
            		}

            		List<TaskActionVo> empTaskVoList = empMap.get(empId);
                	if(empTaskVoList==null){
                		empTaskVoList = new ArrayList<TaskActionVo>();
                		empMap.put(empId, empTaskVoList);
                	}
                	if(!empTaskVoList.contains(taskActionVo)){
                		empTaskVoList.add(taskActionVo);
                	}
            	}

            	// SBU Head Mail Settings End
            }

            //Send SBU Mail
            sendConsolidatedSbuMail(sbuMap,sbuNoTaskEmpMap);

        }
        catch(CustomApplicationException e){
        	log.error(e);
        }
        catch(Exception e){
        	log.error(e);
        }
    }

    private boolean sendMail(MailVo mailVo){

        MailSendEngine mailSendEngine = new MailSendEngine();
        try{
            mailSendEngine.sendmail(mailVo);
        }
        catch(Exception e){
            return false;
        }
        return true;
	}

    private MailVo setIndividualTaskMailVo(UserVo userVo,TaskDao taskDao, String cc) throws Exception{
    	try{
	    	RequestVo requestVo  = new RequestVo();
	    	requestVo.setUserVo(userVo);
	    	ResponseVo responseVo = taskDao.getIndividualMailDetails(requestVo);
	    	if(null != responseVo && null != responseVo.getResponseList() && responseVo.getResponseList().size() > 0){
	        	List<TaskActionVo> taskActionVoList = (List<TaskActionVo>) responseVo.getResponseList();
	        	return EmailMessageBuilder.getIndividualTaskMailVo(taskActionVoList,cc);
	    	}
    	} catch (CustomApplicationException e) {
    		log.error(e);
			throw e;
		} catch (Exception e) {
			log.error(e);
			throw new CustomApplicationException(e);
		}
    	return new MailVo();
    }

    private void sendConsolidatedSbuMail(Map<String,Map<String,List<TaskActionVo>>> sbuMap, Map<String,List<ComboVo>> sbuNoTaskEmpMap) throws IOException, DocumentException{
    	Set<String> sbuList = sbuMap.keySet();
    	int reportCount = 0;
    	String reportDir = CommonUtils.getReportDirectory();
    	String pdfHeading = "Consolidated Task Report";
    	for (String sbuEmailId : sbuList) {
    	    File subDir = new File(reportDir+File.separator+"report"+File.separator+"sbu_" + reportCount++);
	    	subDir.mkdir();
	    	String filePath = subDir.getAbsoluteFile() + File.separator + pdfHeading + ".pdf";
	    	FileOutputStream fileName = new FileOutputStream(filePath);
    		Map<String,List<TaskActionVo>> empMap = sbuMap.get(sbuEmailId);
    		List<ComboVo> noTaskEmpList = sbuNoTaskEmpMap.get(sbuEmailId);
    		if(noTaskEmpList==null){
    			noTaskEmpList = new ArrayList<ComboVo>();
    		}
    		MailVo mailVo = EmailMessageBuilder.getConsolidatedSBUMailVo(empMap,noTaskEmpList);
    		String pdfContent = mailVo.getMessage();
    		getAttachPdf(fileName, pdfHeading, pdfContent);
    		mailVo.setAttachment(filePath);

    		sendMail(mailVo);
    	}
    }

    private void sendConsolidatedProjectMail(Map<String,Map<String,List<TaskActionVo>>> projectMap) throws IOException, DocumentException{
    	Set<String> projectList = projectMap.keySet();
    	int reportCount = 0;
    	String reportDir = CommonUtils.getReportDirectory();
    	for (String projectName : projectList) {
    		if(projectName!=null && !"".equals(projectName)){
	    		String pdfHeading = "Task Report for "+projectName;
		    	File subDir = new File(reportDir+File.separator+"report"+File.separator+"project_" + reportCount++);
		    	subDir.mkdir();
		    	String filePath = subDir.getAbsoluteFile() + File.separator + pdfHeading + ".pdf";
		    	FileOutputStream fileName = new FileOutputStream(filePath);
	    		MailVo mailVo = EmailMessageBuilder.getConsolidatedProjectMailVo(projectName,projectMap.get(projectName));
	    		String pdfContent = mailVo.getMessage();
	    		getAttachPdf(fileName, pdfHeading, pdfContent);
	    		mailVo.setAttachment(filePath);
	    		sendMail(mailVo);
	    		fileName.close();
    		}
    	}
    }

    private void sendConsolidatedPmMail(Map<String,Map<String,Map<String,List<TaskActionVo>>>> pmMap) throws IOException, DocumentException{
    	Set<String> pmList = pmMap.keySet();
    	String reportDir = CommonUtils.getReportDirectory();
    	File dir = new File(reportDir+File.separator+"report");
    	if (dir.exists()){
    	    FileDeleteStrategy.FORCE.delete(dir);
    	}
    	dir.mkdir();
    	int reportCount = 0;
    	String pdfHeading = "Consolidated Task Report";
    	for (String pmEmailId : pmList) {
	    	File subDir = new File(reportDir+File.separator+"report"+File.separator+"pm_" + reportCount++);
	    	subDir.mkdir();
	    	String filePath = subDir.getAbsoluteFile() + File.separator + pdfHeading + ".pdf";
	    	FileOutputStream fileName = new FileOutputStream(filePath);

    		MailVo mailVo = EmailMessageBuilder.getConsolidatedPmMailVo(pmMap.get(pmEmailId),pmEmailId);
    		String pdfContent = mailVo.getMessage();
    		getAttachPdf(fileName, pdfHeading, pdfContent);
    		mailVo.setAttachment(filePath);
    		sendMail(mailVo);
    		fileName.close();
    	}
    }

    /**
     * @Method :
     * @Description :
     * @param taskActionVoList
     * @param taskDao
     * @throws CustomApplicationException
     */
	private void sendIndividualTaskMail(UserVo userVo, TaskDao taskDao, String cc) throws CustomApplicationException {
		MailVo mailVo;
		try {
			mailVo = setIndividualTaskMailVo(userVo, taskDao,cc);
			String msg = mailVo.getMessage();
			String msg1 = msg;
			MailSendResponse sendResponse = new MailSendResponse();
			sendResponse.andAction(mailVo);
		} catch (Exception e) {
			log.error(e);
			throw new CustomApplicationException(e);
		}
	}

	/**
	 * @Method :
	 * @Description :
	 * @param requestVo
	 * @return
	 * @throws CustomApplicationException
	 */
	public ResponseVo saveRecursiveTask(RequestVo requestVo) throws CustomApplicationException {
		TransactionService service = null;
		ResponseVo responseVo = new ResponseVo();
		try{
			service = super.startTransaction();
			 TaskDao taskDao = (TaskDao)ContextLoader.getBean(FrameworkConstant.getBeanId("TASK_DAO"));

			responseVo = taskDao.saveRecursiveTask(requestVo);

			service.commitTransaction();
			responseVo.setTransactionMessage("Recursive task saved successfully");

			return responseVo;
		} catch (CustomApplicationException e) {
			log.error(e);
			if (null != service)
			service.rollbackTransaction();
			throw e;
		} catch (Exception e) {
			log.error(e);
			if (null != service)
			service.rollbackTransaction();
			throw new CustomApplicationException(e);
		}
	}

	/**
	 * @Method : getAttachPdf
	 * @Description : Method to generate the PDF to attach with the mail
	 * @param fileName
	 * @param pdfHeading
	 * @param pdfContent
	 * @throws DocumentException
	 * @throws IOException
	 */
	private void getAttachPdf(FileOutputStream fileName, String pdfHeading, String pdfContent) throws DocumentException, IOException{
		Document document = new Document(PageSize.A4.rotate());
		String divHeader = "<div style='padding: 0 0 0 300px; font-family: sans-serif; font-size: 20px; font-weight: bold; pa'>" +
		"<table align='center' style='text-align: center; font-family: sans-serif; font-size: 20px; font-weight: bold;'><tr><td>"
		+ pdfHeading + " [ " + CommonUtils.convertdateTostring(new Date(), "dd/MM/yyyy") + " ] "
					+ "</td></tr></table></div><br/><br/>";
		PdfWriter pdfWriter = PdfWriter.getInstance(document, fileName);
		document.open();

		XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
		pdfContent = pdfContent.replaceAll("975px", "100%");
		pdfContent = pdfContent.replaceAll("<br>", "<br/>");
		pdfContent = pdfContent.replaceAll("Note: This is auto generated mail, please do not reply.", "  ");
		pdfContent = divHeader.concat(pdfContent);
		worker.parseXHtml(pdfWriter, document, new StringReader(pdfContent));
		document.close();

	}
}
