package com.task.util;

import java.util.ArrayList;

public class ApplicationPagingVo {
	// total number of pages
	private int totalNumberOfPages;

	// number of rows
	private int maxResult;

	// number of rows
	private int totalNumberOfRows;

	// number of rows
	private int startRow;

	// current page number
	private int currentPageNumber;

	// current page number
	private int startPageNumber;

	// current page number
	private int endPageNumber;

	// action Name
	private String actionName;
	
	// action Name
	private String orderBy;
	// index of page number
	
	private int serialNumber;
	
	private ArrayList<Integer> pageIdxArr;
	/**
	 * @return the totalNumberOfPages
	 */
	public int getTotalNumberOfPages() {
		return totalNumberOfPages;
	}
	/**
	 * @param totalNumberOfPages the totalNumberOfPages to set
	 */
	public void setTotalNumberOfPages(int totalNumberOfPages) {
		this.totalNumberOfPages = totalNumberOfPages;
	}
	/**
	 * @return the maxResult
	 */
	public int getMaxResult() {
		return maxResult;
	}
	/**
	 * @param maxResult the maxResult to set
	 */
	public void setMaxResult(int maxResult) {
		this.maxResult = maxResult;
	}
	/**
	 * @return the totalNumberOfRows
	 */
	public int getTotalNumberOfRows() {
		return totalNumberOfRows;
	}
	/**
	 * @param totalNumberOfRows the totalNumberOfRows to set
	 */
	public void setTotalNumberOfRows(int totalNumberOfRows) {
		this.totalNumberOfRows = totalNumberOfRows;
	}
	/**
	 * @return the startRow
	 */
	public int getStartRow() {
		return startRow;
	}
	/**
	 * @param startRow the startRow to set
	 */
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	/**
	 * @return the currentPageNumber
	 */
	public int getCurrentPageNumber() {
		return currentPageNumber;
	}
	/**
	 * @param currentPageNumber the currentPageNumber to set
	 */
	public void setCurrentPageNumber(int currentPageNumber) {
		this.currentPageNumber = currentPageNumber;
	}
	/**
	 * @return the startPageNumber
	 */
	public int getStartPageNumber() {
		return startPageNumber;
	}
	/**
	 * @param startPageNumber the startPageNumber to set
	 */
	public void setStartPageNumber(int startPageNumber) {
		this.startPageNumber = startPageNumber;
	}
	/**
	 * @return the endPageNumber
	 */
	public int getEndPageNumber() {
		return endPageNumber;
	}
	/**
	 * @param endPageNumber the endPageNumber to set
	 */
	public void setEndPageNumber(int endPageNumber) {
		this.endPageNumber = endPageNumber;
	}
	/**
	 * @return the actionName
	 */
	public String getActionName() {
		return actionName;
	}
	/**
	 * @param actionName the actionName to set
	 */
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	/**
	 * @return the pageIdxArr
	 */
	public ArrayList<Integer> getPageIdxArr() {
		return pageIdxArr;
	}
	/**
	 * @param pageIdxArr the pageIdxArr to set
	 */
	public void setPageIdxArr(ArrayList<Integer> pageIdxArr) {
		this.pageIdxArr = pageIdxArr;
	}
	/**
	 * @return the orderBy
	 */
	public String getOrderBy() {
		return orderBy;
	}
	/**
	 * @param orderBy the orderBy to set
	 */
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	/**
	 * @return the serialNumber
	 */
	public int getSerialNumber() {
		return serialNumber;
	}
	/**
	 * @param serialNumber the serialNumber to set
	 */
	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

}
