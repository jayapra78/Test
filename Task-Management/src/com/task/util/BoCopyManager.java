/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.task.common.CommonVo;

/**
 * BoCopyManager Class to copy BO value to new BO object
 * 
 * @author rajnarayan saraf
 * 
 */
public class BoCopyManager {

	/**
	 * Method to copy single source Bean object to Destination Bean Object after creating new
	 * Destination Bean using class name
	 * 
	 * @param origVo CommonVo
	 * @param className Class
	 * @return CommonVo
	 * @throws CustomApplicationException
	 */

	public static CommonVo copyPropertiesWithInstance(CommonVo origVo, Class<?> className)
			throws CustomApplicationException {
		/** create instance of sub class of CommonVo */
		CommonVo destVo;
		try {
			destVo = Class.forName(className.getName()).asSubclass(CommonVo.class).newInstance();

			/** copy origVo to destVo */
			if (origVo != null) {
				BeanUtils.copyProperties(origVo, destVo);
				return destVo;
			} 
			return null;
			

		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomApplicationException("copyPropertiesWithInstance", e);
		}
	}

	/**
	 * Method to copy single source Bean object to Destination Bean Object after creating new
	 * Destination Bean using class name
	 * 
	 * @param origVo CommonVo
	 * @param className Class
	 * @return CommonVo
	 * @throws CustomApplicationException
	 */

	public static void copyProperties(CommonVo origVo, CommonVo destVo)
			throws CustomApplicationException {
		/** create instance of sub class of CommonVo */
		try {
			/** copy origVo to destVo */
			if (origVo != null) {
				BeanUtils.copyProperties(origVo, destVo);
			} else {
				destVo = null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomApplicationException("copyProperties", e);
		}
	}

	/**
	 * Method to copy origVoList to destVoList bu creating new instance
	 * 
	 * @param origVoList
	 * @param destVoList
	 * @param destClass
	 * @throws CustomApplicationException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void copyListProperties(List origVoList, List destVoList, Class destClass)
			throws CustomApplicationException {
		try {

			/** Iterator for origVoList */
			Iterator itr = origVoList.iterator();
			CommonVo v;

			/** List to set sub class of CommonVo */
			if (origVoList != null) {
				while (itr.hasNext()) {
					/** create instance of sub class of CommonVo */
					v = Class.forName(destClass.getName()).asSubclass(CommonVo.class).newInstance();
					/** copy origVo to destVo */
					BeanUtils.copyProperties(itr.next(), v);

					/** Add to list */
					destVoList.add(v);
				}
			} 

			/** Set list to destVoList */
			// destVoList.addAll(list.getList());

		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomApplicationException("copyListProperties", e);
		}
	}

	

}
