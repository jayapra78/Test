/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

/**
 * @author neethimohana
 *
 */
@SuppressWarnings("rawtypes")
public class CommonAsynMailThread implements Callable{
	Logger log = Logger.getLogger(CommonAsynMailThread.class);
	private MailbackInterface mailbackInterface;
    private Object valueObject;

    public CommonAsynMailThread() {
    }
    
    /**
     * @Method : call
     * 
     * @Description : The method to call the send mail
     * 
     * @return null   
     */
    public Object call() throws Exception{
    	log.debug("Method Start : "+ "call");
    	mailbackInterface.returnResult(this.valueObject);
    	log.debug("Method End : "+ "call");
        return null;
    }

	/**
	 * Getter method for mailbackInterface
	 * 
	 * @return the mailbackInterface (MailbackInterface)
	 */
	public MailbackInterface getMailbackInterface() {
		return mailbackInterface;
	}

	/**
	 * Setter method for mailbackInterface
	 *
	 * @param mailbackInterface the mailbackInterface to set
	 */
	public void setMailbackInterface(MailbackInterface mailbackInterface) {
		this.mailbackInterface = mailbackInterface;
	}

	/**
	 * Getter method for valueObject
	 * 
	 * @return the valueObject (Object)
	 */
	public Object getValueObject() {
		return valueObject;
	}

	/**
	 * Setter method for valueObject
	 *
	 * @param valueObject the valueObject to set
	 */
	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}

   
    
}
