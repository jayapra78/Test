/**
 * Creater / sengottaiyansSRM copyright information to Shirai 2013
 * 
 */
package com.task.util;

/**
 * @author sengottaiyans
 *
 */
public class CommonConstants {
	
	public static final String FILE_ENCODING = "UTF-8"; // File Encoding
	public static final String FILE_EXT_EXCEL = ".xls"; // File Extension
	
	public static final Character STATUS_MAIL_ON		= '1';
	public static final Character STATUS_MAIL_OFF		= '0';
	public static final Character CONSOLIDATE_MAIL_ON	= '1';
	public static final Character CONSOLIDATE_MAIL_OFF	= '0';
}
