/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.task.common.CommonDaoConjunctionVo;
import com.task.common.CommonDaoDisjunctionVo;
import com.task.common.CommonDaoUtilsVo;
import com.task.common.CommonVo;



/**
 * CommonDaoUtils to get the common operation before DB action
 * 
 * @author Rajnarayan Saraf, Neethimohan A
 */
public class CommonDaoUtils {

	
	/**
	 * Method to create Like Criteria
	 * 
	 * @param fieldName
	 *            String
	 * 
	 * @param fieldValue
	 *            String
	 */
	public static void likeOp(String fieldName, String fieldValue,
			CommonVo daoUtilVo) throws CustomApplicationException{
		try{
			if (daoUtilVo instanceof CommonDaoUtilsVo) {
				((CommonDaoUtilsVo) daoUtilVo).getCriteria().add(
						Restrictions.ilike(fieldName, fieldValue.trim(),
								MatchMode.ANYWHERE));

			}else if (daoUtilVo instanceof CommonDaoDisjunctionVo) {
				((CommonDaoDisjunctionVo) daoUtilVo).getDisjunction().add(
						Restrictions.ilike(fieldName, fieldValue.trim(),
								MatchMode.ANYWHERE));

			}
		} catch (HibernateException e) {
			throw new CustomApplicationException("likeOp", e);
		}
	}

	/**
	 * Method to create Like Equal operation
	 * 
	 * @param fieldName
	 *            String
	 * 
	 * @param fieldValue
	 *            Object
	 */
	public static void eqOp(String fieldName, Object fieldValue,
			CommonVo daoUtilVo) throws CustomApplicationException{
		if(fieldValue instanceof String){
			fieldValue = ((String) fieldValue).trim();
		}
		try{
		if (daoUtilVo instanceof CommonDaoUtilsVo) {
			((CommonDaoUtilsVo) daoUtilVo).getCriteria().add(Restrictions.eq(fieldName, fieldValue));

		}else if (daoUtilVo instanceof CommonDaoDisjunctionVo) {
			((CommonDaoDisjunctionVo) daoUtilVo).getDisjunction().add(Restrictions.eq(fieldName, fieldValue));

		}else if (daoUtilVo instanceof CommonDaoConjunctionVo) {
			((CommonDaoConjunctionVo) daoUtilVo).getConjuction().add(Restrictions.eq(fieldName, fieldValue));

		}
		
		} catch (HibernateException e) {
			throw new CustomApplicationException("eqOp", e);
		}
	}

	/**
	 * Method to create Like Between operation
	 * 
	 * @param fieldName
	 *            String
	 * 
	 * @param fieldValue1
	 *            Object
	 * 
	 * @param fieldValue2
	 *            Object
	 */
	public static void betweenOp(String fieldName, Object fieldValue1,
			Object fieldValue2, CommonDaoUtilsVo daoUtilsVo) throws CustomApplicationException {
		try{
		daoUtilsVo.getCriteria().add(
				Restrictions.between(fieldName, fieldValue1, fieldValue2));
		} catch (HibernateException e) {
			throw new CustomApplicationException("betweenOp", e);
		}
	}

	/**
	 * Method to create Greater than operation
	 * 
	 * @param fieldName
	 *            String
	 * 
	 * @param fieldValue
	 *            Object
	 */
	public static void gtOp(String fieldName, Object fieldValue,
			CommonVo daoUtilsVo) throws CustomApplicationException{
		try{
			if (daoUtilsVo instanceof CommonDaoUtilsVo) {
				((CommonDaoUtilsVo) daoUtilsVo).getCriteria().add(Restrictions.gt(fieldName, fieldValue));

			}else if (daoUtilsVo instanceof CommonDaoDisjunctionVo) {
				((CommonDaoDisjunctionVo) daoUtilsVo).getDisjunction().add(Restrictions.gt(fieldName, fieldValue));

			}else if (daoUtilsVo instanceof CommonDaoConjunctionVo) {
				((CommonDaoConjunctionVo) daoUtilsVo).getConjuction().add(Restrictions.gt(fieldName, fieldValue));

			}
		//daoUtilsVo.getCriteria().add(Restrictions.gt(fieldName, fieldValue));
		} catch (HibernateException e) {
			throw new CustomApplicationException("gtOp", e);
		}
	}

	/**
	 * Method to create Less than operation
	 * 
	 * @param fieldName
	 *            String
	 * 
	 * @param fieldValue
	 *            Object
	 */
	public static void ltOp(String fieldName, Object fieldValue,
			CommonVo daoUtilsVo) throws CustomApplicationException{
		try{
			if (daoUtilsVo instanceof CommonDaoUtilsVo) {
				((CommonDaoUtilsVo) daoUtilsVo).getCriteria().add(Restrictions.lt(fieldName, fieldValue));

			}else if (daoUtilsVo instanceof CommonDaoDisjunctionVo) {
				((CommonDaoDisjunctionVo) daoUtilsVo).getDisjunction().add(Restrictions.lt(fieldName, fieldValue));

			}else if (daoUtilsVo instanceof CommonDaoConjunctionVo) {
				((CommonDaoConjunctionVo) daoUtilsVo).getConjuction().add(Restrictions.lt(fieldName, fieldValue));

			}
		} catch (HibernateException e) {
			throw new CustomApplicationException("ltOp", e);
		}
	}

	/**
	 * Method to create Greater than or Equal operation
	 * 
	 * @param fieldName
	 *            String
	 * 
	 * @param fieldValue
	 *            Object
	 */
	public static void geOp(String fieldName, Object fieldValue,
			CommonVo daoUtilsVo) throws CustomApplicationException{
		try{
			if (daoUtilsVo instanceof CommonDaoUtilsVo) {
				((CommonDaoUtilsVo) daoUtilsVo).getCriteria().add(Restrictions.ge(fieldName, fieldValue));

			}else if (daoUtilsVo instanceof CommonDaoDisjunctionVo) {
				((CommonDaoDisjunctionVo) daoUtilsVo).getDisjunction().add(Restrictions.ge(fieldName, fieldValue));

			}else if (daoUtilsVo instanceof CommonDaoConjunctionVo) {
				((CommonDaoConjunctionVo) daoUtilsVo).getConjuction().add(Restrictions.ge(fieldName, fieldValue));

			}
			
			
		} catch (HibernateException e) {
			throw new CustomApplicationException("geOp", e);
		}
	}

	/**
	 * Method to create Less than or Equal operation
	 * 
	 * @param fieldName
	 *            Object
	 * 
	 * @param fieldValue
	 *            Object
	 */
	public static void leOp(String fieldName, Object fieldValue,
			CommonVo daoUtilsVo) throws CustomApplicationException{
		try{	
			if (daoUtilsVo instanceof CommonDaoUtilsVo) {
				((CommonDaoUtilsVo) daoUtilsVo).getCriteria().add(Restrictions.le(fieldName, fieldValue));

			}else if (daoUtilsVo instanceof CommonDaoDisjunctionVo) {
				((CommonDaoDisjunctionVo) daoUtilsVo).getDisjunction().add(Restrictions.le(fieldName, fieldValue));

			}else if (daoUtilsVo instanceof CommonDaoConjunctionVo) {
				((CommonDaoConjunctionVo) daoUtilsVo).getConjuction().add(Restrictions.le(fieldName, fieldValue));

			}
		} catch (HibernateException e) {
			throw new CustomApplicationException("leOp", e);
		}
	}

	/**
	 * Method to create Between Greater than or Less than operation
	 * 
	 * @param fieldName
	 *            String
	 * 
	 * @param fieldValue1
	 *            Object
	 * 
	 * @param fieldValue2
	 *            Object
	 */
	public static void betweenEqOp(String fieldName, Object fieldValue1,
			Object fieldValue2, CommonDaoUtilsVo daoUtilsVo) throws CustomApplicationException {
		try{
		Conjunction conjunction = Restrictions.conjunction();
		conjunction.add(Restrictions.ge(fieldName, fieldValue1));
		conjunction.add(Restrictions.le(fieldName, fieldValue2));
		daoUtilsVo.getCriteria().add(conjunction);
		} catch (HibernateException e) {
			throw new CustomApplicationException("betweenEqOp", e);
		}
	}

	/**
	 * Method to Order by Ascending
	 * 
	 * @param fieldName
	 *            String
	 */
	public static void orderByAsc(String fieldName, CommonDaoUtilsVo daoUtilsVo) throws CustomApplicationException{
		try{
		daoUtilsVo.getCriteria().addOrder(Order.asc(fieldName));
	} catch (HibernateException e) {
		throw new CustomApplicationException("orderBy", e);
	}
		
	}
	/**
	 * Method to Order by Descending
	 * 
	 * @param fieldName
	 *            String
	 */
	public static void orderByDesc(String fieldName, CommonDaoUtilsVo daoUtilsVo) throws CustomApplicationException{
		try{
		daoUtilsVo.getCriteria().addOrder(Order.desc(fieldName));
	} catch (HibernateException e) {
		throw new CustomApplicationException("orderBy", e);
	}
	}
	
	/**
	 * Method used to fetch parent if child records is available
	 * 
	 * @param childName
	 *            String
	 */
	public static void filterBasedOnChild(String childName,
			CommonDaoUtilsVo daoUtilsVo) throws CustomApplicationException {
		try{
		daoUtilsVo.getCriteria().add(Restrictions.isNotEmpty(childName));
		} catch (HibernateException e) {
			throw new CustomApplicationException("filterBasedOnChild", e);
		}
	}
	/**
	 * @Method : isNull
	 * @Description : null case 
	 * @param fieldName
	 * @param daoUtilsVo
	 * @throws CustomApplicationException
	 */
	public static void isNull(String fieldName, CommonVo daoUtilsVo) throws CustomApplicationException{
		try{
			if (daoUtilsVo instanceof CommonDaoUtilsVo) {
				((CommonDaoUtilsVo) daoUtilsVo).getCriteria().add(Restrictions.isNull(fieldName));

			}
			else if (daoUtilsVo instanceof CommonDaoDisjunctionVo) {
				((CommonDaoDisjunctionVo) daoUtilsVo).getDisjunction().add(Restrictions.isNull(fieldName));

			}
			} catch (HibernateException e) {
				throw new CustomApplicationException("isNull", e);
			}
	}
	
	/**
	 * @Method : isNotNull
	 * @Description : not null case 
	 * @param fieldName
	 * @param daoUtilsVo
	 * @throws CustomApplicationException
	 */
	public static void isNotNull(String fieldName, CommonVo daoUtilsVo) throws CustomApplicationException{
		try{
			if (daoUtilsVo instanceof CommonDaoUtilsVo) {
				((CommonDaoUtilsVo) daoUtilsVo).getCriteria().add(Restrictions.isNotNull(fieldName));

			}
			else if (daoUtilsVo instanceof CommonDaoDisjunctionVo) {
				((CommonDaoDisjunctionVo) daoUtilsVo).getDisjunction().add(Restrictions.isNotNull(fieldName));

			}
			} catch (HibernateException e) {
				throw new CustomApplicationException("isNull", e);
			}
	}
	
	/**
	 * @Method : notEq
	 * @Description : not equal case
	 * @param fieldName
	 * @param fieldValue
	 * @param daoUtilVo
	 * @throws CustomApplicationException
	 */
	public static void notEq(String fieldName, Object fieldValue,
			CommonVo daoUtilVo) throws CustomApplicationException{
		try{
			if (daoUtilVo instanceof CommonDaoUtilsVo) {
				((CommonDaoUtilsVo) daoUtilVo).getCriteria().add(Restrictions.ne(fieldName, fieldValue));

			}
			else if (daoUtilVo instanceof CommonDaoDisjunctionVo) {
				((CommonDaoDisjunctionVo) daoUtilVo).getDisjunction().add(Restrictions.ne(fieldName, fieldValue));

			}
			else if (daoUtilVo instanceof CommonDaoConjunctionVo) {
				((CommonDaoConjunctionVo) daoUtilVo).getConjuction().add(Restrictions.ne(fieldName, fieldValue));

			}
		//daoUtilVo.getCriteria().add(Restrictions.ne(fieldName, fieldValue));
		} catch (HibernateException e) {
			throw new CustomApplicationException("notEq", e);
		}
	}
	
	/**
	 * @Method : createAlias
	 * @Description : The method to Create Alias
	 * @param className String
	 * @param aliasValue String
	 * @param daoUtilVo CommonDaoUtilsVo
	 * @throws CustomApplicationException
	 */
	public static void createAlias(String className, String aliasValue,
			CommonDaoUtilsVo daoUtilVo) throws CustomApplicationException{
		try{
		daoUtilVo.getCriteria().createAlias(className, aliasValue);
		} catch (HibernateException e) {
			throw new CustomApplicationException("notEq", e);
		}
	}
}
