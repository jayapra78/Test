/*
 ***************************************************************************** 
 * Copyright 2014 NSK Ltd. All rights reserved.
 *****************************************************************************
 *  Change Activity:
 *  -------------------------------------------------------------------------- 
 *  Date      		Author      		Description
 *  -------------------------------------------------------------------------- 
 *  Jul 01, 2014    Neethimohana[SRM]   Initial Code drop.
 *****************************************************************************
 */
package com.task.util;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * <H3> AvisDateUtil </H3>
 * 
 * @author Neethimohana[SRM]
 * @since Jul 01, 2014
 */
public class CommonDateUtil {

	/** Format yyMM */
	public static final String FORMAT_YYMM = "yyMM";
	/** Format  yyMMdd */
	public static final String FORMAT_YYMMDD = "yyMMdd";
	/** Format  FORMAT_YY_MM_DD */
	public static final String FORMAT_YY_MM_DD = "yy/MM/dd";
	/** Format yyMMdd HHmm */
	public static final String FORMAT_YYMMDDHHMM = "yyMMdd HHmm";
	/**Format yyyyMM */
	public static final String FORMAT_YYYYMM = "yyyyMM";
	/** Format yyyy/MM */
	public static final String FORMAT_YYYY_MM = "yyyy/MM";
	/** Format yyyyMMdd */
	public static final String FORMAT_YYYYMMDD = "yyyyMMdd";
	/** Format yyyy/MM/dd */
	public static final String FORMAT_YYYY_MM_DD = "yyyy/MM/dd";
	/** Format yyyy/MM/dd HH:mm */
	public static final String FORMAT_YYYY_MM_DD_HH_MM = "yyyy/MM/dd HH:mm";
	/** Format yyyyMMddHHmmss */
	public static final String FORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public static final String FORMAT_YYYYMMDDHH24MISS = "yyyyMMddHH24miss";
	/** Format yyyyMMdd_HHmmss */
	public static final String FORMAT_YYYYMMDD_HHMMSS = "yyyyMMdd_HHmmss";
	/** Format yyyy/MM/dd HH:mm:ss */
	public static final String FORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy/MM/dd HH:mm:ss";
	/** DB Format yyyy/MM/dd HH:mm:ss */
	public static final String FORMAT_YYYY_MM_DD_HH_MI_SS = "yyyy/MM/dd HH24:MI:SS";
	/** Format yy/MM/dd HH:mm:ss */
	public static final String FORMAT_YY_MM_DD_HH_MM_SS = "yy/MM/dd HH:mm:ss";
	/** Format yyyyMMddHHmmssSSS */
	public static final String FORMAT_YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";
	/** Format HH:mm */
	public static final String FORMAT_HH_MM = "HH:mm";
	
	/** Format HH:mm:ss */
	public static final String FORMAT_HH_MM_SS ="HH:mm:ss";
	
	
	public static final String FORMAT_HHMMSS = "HHmmss";
	
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	
	public static final String YY_MM_DD="yy-MM-dd";
	
	/** Format yyMM */
	public static final String FORMAT_YY = "yy";

	public static final String FORMAT_YYYY_MM_DD_HH_MI 		= "yyyy/MM/dd HH24:MI";
	
	public static final String FORMAT_YYYY_MM_DD_HH_MM_SS_S = "yyyy-MM-dd HH:mm:ss.S";
	
	public static final String DD_MMMM_YYYY = "dd-MMMM-yyyy";
	
	public static final String DD_MMM_YYYY = "dd MMM yyyy";

	// ***** constructor *****
	/**
	 * ãƒ‡ãƒ•ã‚©ãƒ«ãƒˆã‚³ãƒ³ã‚¹ãƒˆãƒ©ã‚¯ã‚¿ãƒ¼
	 */
	private CommonDateUtil() {
	}

	// ***** public method *****
	/**
	 * æ—¥ä»˜ãƒ�ã‚§ãƒƒã‚¯
	 * 
	 * @param dateStr æ—¥ä»˜æ–‡å­—åˆ—
	 * @param format ãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆ
	 * @return trueï¼šOKï¼�falseï¼šNG
	 */
	public static boolean isDate(String dateStr, String format) {

		// æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆã‚’å�–å¾—
		Date _date = getDate(dateStr, format);

		// å�–å¾—çµ�æžœã‚’è¿�?å�´
		return (null != _date);
	}

	/**
	 * 2ã�¤ã�®æ—¥ä»˜ã‚’æ¯�?è¼ƒã�—ã�¾ã�™ã€‚æ—¥ä»˜ã�Œä»¥ä¸Šã�®å ´å�ˆã�¯nullã�Œè€ƒæ…®ã�•ã‚Œã�¾ã�™ã€‚
	 * @return 2ã�¤ã�®æ—¥ä»˜ã�§ã�¯ä¸€ç•ªå…ˆæ—¥ã�Œè€ƒæ…®ã�•ã‚Œã�¾ã�™ã€‚
	 */
	public static Date least(Date a, Date b) {
	    return a == null ? b : (b == null ? a : (a.before(b) ? a : b));
	}
	
	/**
	 * æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆå�–å¾—
	 * 
	 * @param value æ—¥ä»˜æ–‡å­—åˆ—
	 * @param format ãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆ
	 * @return æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 */
	public static Date getDate(String value, String format) {

		// æ—¥ä»˜
		Date _date = null;

		// å¯¾è±¡æ–‡å­—åˆ—ã�Œå­˜åœ¨ã�™ã‚‹å ´å�ˆ
		if (!isNullOrBlank(value)) {

			// æ—¥ä»˜æ–‡å­—æ•°ã�Œæ­£å¸¸ã�®å ´å�ˆ
			if (value.length() == format.length()) {

				// æ—¥ä»˜ã‚’å�–å¾—
				try {
					SimpleDateFormat _sdf = new SimpleDateFormat(format);
					_sdf.setLenient(false);
					_date = _sdf.parse(value, new ParsePosition(0));
				}
				catch (Exception e) {
				}
			}
		}

		return _date;
	}
	/**
	 * æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆå�–å¾—
	 * 
	 * @param value æ—¥ä»˜æ–‡å­—åˆ—
	 * @param format ãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆ
	 * @return æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 */
	public static Date getDateNullChk(Date date, String format) {
		// å¯¾è±¡æ–‡å­—åˆ—ã�Œå­˜åœ¨ã�™ã‚‹å ´å�ˆ
		if (date!=null) {
			return getDate(date,format);
		} else {
			return getDate(getCurrentDate(),format);
		}
	}
	/**
	 * å½“æ—¥ã‚’å�–å¾—ã�™ã‚‹ãƒ¡ã‚½ãƒƒãƒ‰ã€‚
	 * <br>
	 * @return Date return on _cal
	 */
	public static Date getCurrentDate() {
		Calendar _cal = Calendar.getInstance();
		return _cal.getTime();
	}	
	/**
	 * æ—¥ä»˜ã‚’å�–å¾—ã�™ã‚‹ãƒ¡ã‚½ãƒƒãƒ‰ã€‚
	 * <br>
	 * @return Date return on _cal
	 */
	public static Date getDate(String year, String month, String date) {
		int y = Integer.parseInt(year);
		int m = Integer.parseInt(month)-1;
		int d = Integer.parseInt(date);
		Calendar cal = Calendar.getInstance();
		cal.set(y,m,d);
		return cal.getTime();
	}	
	public static int monthDifference(Date date1,Date date2){
		Calendar c1 = Calendar.getInstance();
		c1.setTime(date1);
		Calendar c2 = Calendar.getInstance();
		c2.setTime(date2);
		int yearDiff = c1.get(Calendar.YEAR)-c2.get(Calendar.YEAR);
		return ((yearDiff*12)+c1.get(Calendar.MONTH))-c2.get(Calendar.MONTH);
	}
	public static int dateDifference(Date date1,Date date2){
		Calendar c1 = Calendar.getInstance();
		c1.setTime(date1);
		Calendar c2 = Calendar.getInstance();
		c2.setTime(date2);
		return c1.get(Calendar.YEAR)*c1.get(Calendar.MONTH)*c1.get(Calendar.DATE)-
			   c2.get(Calendar.YEAR)*c2.get(Calendar.MONTH)*c2.get(Calendar.DATE);
	}
	/**
	 * æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆå�–å¾—
	 * 
	 * @param date æ—¥æ™‚ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 * @param format ãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆ
	 * @return æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 */
	public static Date getDate(Date date, String format) {

		// æ—¥ä»˜
		Date _date = null;

		// æ—¥ä»˜æ–‡å­—åˆ—ã‚’å�–å¾—
		String _dateStr = getDateStr(date, format);

		// æ—¥ä»˜æ–‡å­—åˆ—ã�Œå­˜åœ¨ã�™ã‚‹å ´å�ˆ
		if (!isNullOrBlank(_dateStr)) {

			// æ—¥ä»˜ã‚’å�–å¾—
			_date = getDate(_dateStr, format);
		}

		return _date;
	}

	/**
	 * æ—¥æ™‚æ–‡å­—åˆ—å�–å¾—
	 * 
	 * @param date æ—¥æ™‚ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 * @param format ãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆ
	 * @return æ—¥æ™‚æ–‡å­—åˆ—
	 */
	public static String getDateStr(Date date, String format) {

		// æ—¥æ™‚æ–‡å­—åˆ—
		String _str = null;

		// å¯¾è±¡æ—¥ä»˜ã�Œå­˜åœ¨ã�™ã‚‹å ´å�ˆ
		if (null != date) {

			// æŒ‡å®šãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆã�®æ—¥æ™‚æ–‡å­—åˆ—ã‚’å�–å¾—
			_str = format(date, format);
		}

		return _str;
	}
	
	public static String getLocaleEnDateStr(String value, String formatFrom, String formatTo) {

		// æ—¥æ™‚æ–‡å­—åˆ—
		String _str = null;

		// å¯¾è±¡æ–‡å­—åˆ—ã�Œå­˜åœ¨ã�™ã‚‹å ´å�ˆ
		if (!isNullOrBlank(value)) {

			// æŒ‡å®šãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆã�®æ—¥æ™‚æ–‡å­—åˆ—ã‚’å�–å¾—
			_str = getDateStrByLocal(getDate(value, formatFrom), formatTo);
		}

		return _str;
	}
	
	public static String getDateStrByLocal(Date date, String format) {

		// æ—¥æ™‚æ–‡å­—åˆ—
		String _str = null;

		// å¯¾è±¡æ—¥ä»˜ã�Œå­˜åœ¨ã�™ã‚‹å ´å�ˆ
		if (null != date) {

			// æŒ‡å®šãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆã�®æ—¥æ™‚æ–‡å­—åˆ—ã‚’å�–å¾—
			_str = new SimpleDateFormat(format,Locale.ENGLISH).format(date);
		}

		return _str;
	}
	
	/**
	 * æ—¥æ™‚æ–‡å­—åˆ—å�–å¾—
	 * 
	 * @param value æ—¥æ™‚æ–‡å­—åˆ—From
	 * @param formatFrom ãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆFrom
	 * @param formatTo ãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆTo
	 * @return æ—¥æ™‚æ–‡å­—åˆ—To
	 */
	public static String getDateStr(String value, String formatFrom, String formatTo) {

		// æ—¥æ™‚æ–‡å­—åˆ—
		String _str = null;

		// å¯¾è±¡æ–‡å­—åˆ—ã�Œå­˜åœ¨ã�™ã‚‹å ´å�ˆ
		if (!isNullOrBlank(value)) {

			// æŒ‡å®šãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆã�®æ—¥æ™‚æ–‡å­—åˆ—ã‚’å�–å¾—
			_str = getDateStr(getDate(value, formatFrom), formatTo);
		}

		return _str;
	}

	/**
	 * æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆå�–å¾—
	 * 
	 * @param date æ—¥æ™‚ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 * @param format ãƒ•ã‚©ãƒ¼ãƒžãƒƒãƒˆ
	 * @return æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 */
	public static Date getFirstDate(Date date) {

		// æ—¥ä»˜ã‚’å�–å¾—
		return getDate(date, FORMAT_YYYYMM);
	}

	/**
	 * æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆå�–å¾—
	 * 
	 * @param date æ—¥æ™‚ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 * @return æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 */
	public static Date getPreviousMonthFirstDate(Date date) {

		// å¯¾è±¡æ—¥ä»˜ã‚’å‰�æœˆæœˆåˆ�æ—¥ä»˜ã�«å¤‰æ�›
		return getFirstDate(addMonthsTo(date, -1));
	}

	/**
	 * æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆå�–å¾—
	 * 
	 * @param date æ—¥æ™‚ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 * @param addDays æ—¥æ•°
	 * @return æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 */
	public static Date addDaysTo(Date date, int addDays) {

		// æ—¥ä»˜
		Date _date = null;

		// å¯¾è±¡æ—¥ä»˜ã�Œå­˜åœ¨ã�™ã‚‹å ´å�ˆ
		if (null != date) {

			// å¯¾è±¡æ—¥ä»˜ã�«æ—¥æ•°ã‚’åŠ ç®—
			_date = addDaysTo1(date, addDays);
		}

		return _date;
	}
	public static int getMonth(String date,String format){
		Calendar cal = Calendar.getInstance();
		cal.setTime(getDateByValue(date, format));
		return cal.get(Calendar.MONTH);
	}
	public static Date setDate(Date fullDate, int date) {
		 Calendar cal = Calendar.getInstance();			
		 cal.setTime(fullDate);
		 cal.set(Calendar.DATE, date);
		return cal.getTime();
	}
	
	/**
	 * æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆå�–å¾—
	 * 
	 * @param date æ—¥æ™‚ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 * @param addMonths æœˆæ•°
	 * @return æ—¥ä»˜ã‚ªãƒ–ã‚¸ã‚§ã‚¯ãƒˆ
	 */
	public static Date addMonthsTo(Date date, int addMonths) {

		// æ—¥ä»˜
		Date _date = null;

		// å¯¾è±¡æ—¥ä»˜ã�Œå­˜åœ¨ã�™ã‚‹å ´å�ˆ
		if (null != date) {

			// å¯¾è±¡æ—¥ä»˜ã�«æœˆæ•°ã‚’åŠ ç®—
			_date = rollMonth(date, addMonths);
		}

		return _date;
	}
	
	/**
	 * Betweenæ—¥ä»˜ã‚’æ¯�?è¼ƒã�™ã‚‹ãƒ¡ã‚½ãƒƒãƒ‰ã€‚
	 * <br>
	 * @param compareDate
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static boolean compareBetweenDate(String compareDate, String startDate, String endDate){
		if(!isNullOrWhitespaces(compareDate) && 
		   !isNullOrWhitespaces(startDate) && 
		   !isNullOrWhitespaces(endDate)){
			compareDate = compareDate.length()>8?compareDate.substring(2):compareDate;
			startDate = startDate.length()>8?startDate.substring(2):startDate;
			endDate = endDate.length()>8?endDate.substring(2):endDate;
			return compareDate.compareTo(startDate)>=0 && endDate.compareTo(compareDate)>=0;
		}
		return false;
	}
	
	/**
	 * 
	 * <br>
	 * @param value
	 * @param format
	 * @return
	 */
	public static Date getDateByValue(String value, String format) {

		// æ—¥ä»˜
		Date _date = null;

		// å¯¾è±¡æ–‡å­—åˆ—ã�Œå­˜åœ¨ã�™ã‚‹å ´å�ˆ
		if (!isNullOrBlank(value)) {

			// æ—¥ä»˜æ–‡å­—æ•°ã�Œæ­£å¸¸ã�®å ´å�ˆ
			if (value.length() == format.length()) {

				// æ—¥ä»˜ã‚’å�–å¾—
				try {
					SimpleDateFormat _sdf = new SimpleDateFormat(format);
					_sdf.setLenient(false);
					_date = _sdf.parse(value, new ParsePosition(0));
				}
				catch (Exception e) {
				}
				
			}else if(value.length()==FORMAT_YYYYMMDD.length()){
				try {
					SimpleDateFormat _sdf = new SimpleDateFormat(FORMAT_YYYYMMDD);
					_sdf.setLenient(false);
					_date = _sdf.parse(value, new ParsePosition(0));
				}
				catch (Exception e) {
				}
			}
		}

		return _date;
	}
	
	/**
	 * 
	 * <br>
	 * @return
	 */
	public static String sessionTime(int time){
		Date _date = new Date();
	      Calendar _cal = Calendar. getInstance();
	      _cal.setTime(_date);
	      _cal. add(Calendar.MINUTE, time);
	      getDateStr(_cal.getTime(), FORMAT_HH_MM);
		return  getDateStr(_cal.getTime(), FORMAT_HH_MM);
	}
	
	/**
	 * isNullOrBlank<br>
	 * 
	 * @param value String
	 * @return true if value is null or blank (length zero string)
	 */
	public static boolean isNullOrBlank(String value) {
	    
		return (null == value) || (0 == value.length());
	}

	

	/**
	 * return date after addDays[day] from the date.<br>
	 * @param date Date
	 * @param addDays int
	 * @return Date
	 */
	public static Date addDaysTo1(Date date, int addDays) {
	    
		Calendar _c = new GregorianCalendar();
		_c.setTime(date);
		_c.add(Calendar.DATE, addDays);
		return _c.getTime(); 
	}
	
	
	
	/**
	 * return formatted Date.<br>
	 * @param pattern date format pattern
	 * @param source original Date
	 * @author Y. Tsuda
	 * @since 2005/02/03
	 */	
	public static String format(final Date source, final String pattern) {
	    
		if (null == source) {
		    return null;
		}
		return new SimpleDateFormat(pattern).format(source);
	}
	// ***** protected method *****
	// ***** private method *****
	// ***** call back method *****
	// ***** getter and setter *****
	
	/**
	 * shift date with input months.
	 * <br>
	 * @param orgDate --- Original Date 
	 * @param month --- shifted months ( input plus --- shift future, minus then shift past)
	 * @return Date --- Shifted Date Object.
	 * @author T.Kodama
	 * @since 2005/04/05
	 */
	public static Date rollMonth(Date orgDate, int month) {
		//obtain calendar and set this date.
		Calendar _tmpCalendar = getCalendar(orgDate);
		//shift searchDate from today with in request condition
		_tmpCalendar.add(Calendar.MONTH, month);
		//obtain shift days and create Date Object.
		long _searchDateTime = (_tmpCalendar.getTime()).getTime();
		Date _date = new Date(_searchDateTime);
		return _date;
	}//end rollMonth()
	
	
	/**
	 * get initialized Calendar instance.<br>
	 * set monday at first day of week.
	 * <br>
	 * @param date Date
	 * @return Calendar instance
	 */
	public static Calendar getCalendar(Date date) {
		GregorianCalendar _cal = new GregorianCalendar();
		_cal.setTime(date);
		_cal.setFirstDayOfWeek(Calendar.MONDAY);
		return _cal;
	}//end getCalendar()
	
	/**
	 * isNullOrWhiteSpaces<br>
	 * 
	 * @param value String
	 * @return true if value is null, blank or whitespaces
	 */
	public static boolean isNullOrWhitespaces(String value) {
	    
		return (null == value) || value.matches("\\s*");
	}
	public static Date convertSlashStringToDate(String dateStr) {
		DateFormat formatter;
		Date date;
		formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		try {
			date = (Date) formatter.parse(dateStr);
		} catch (ParseException e) {
			return null;
		}
		return date;
	}
}
