/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.task.common.ComboVo;
import com.task.common.constant.ConstantEnum;

public class CommonUtils {
	private static final double RADIUS_KM = 6367;

	public static ApplicationContext getContext() {
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				new String[] { "applicationContext.xml" });
		return ctx;
	}

	public static void setPagination(ApplicationPagingVo pagingVo) {
		// pagingVo.setStartRow(startRow);
		// pagingVo.setTotalNumberOfRows(totalRows);
		if (pagingVo.getStartRow() == 0) {
			pagingVo.setCurrentPageNumber(1);
		}

		if (pagingVo.getTotalNumberOfRows() % pagingVo.getMaxResult() == 0) {
			pagingVo.setTotalNumberOfPages(pagingVo.getTotalNumberOfRows()
					/ pagingVo.getMaxResult());
		} else {
			pagingVo.setTotalNumberOfPages(pagingVo.getTotalNumberOfRows()
					/ pagingVo.getMaxResult() + 1);
		}
		int displayPageCount = 0;
		int intIndex = 0;
		if (pagingVo.getTotalNumberOfPages() > 0) {
			ArrayList<Integer> ar = new ArrayList<Integer>();
			displayPageCount = pagingVo.getCurrentPageNumber() / 5;
			if (pagingVo.getCurrentPageNumber() % 5 == 0) {
				displayPageCount = displayPageCount - 1;
			}

			for (int i = 0; i < 5; i++) {
				if (displayPageCount != 0) {
					if (i == 0) {
						intIndex = displayPageCount * 5;
						pagingVo.setStartPageNumber(intIndex);
					}
					if (pagingVo.getTotalNumberOfPages() > intIndex) {
						ar.add(++intIndex);
						pagingVo.setEndPageNumber(intIndex);
					}
				} else {
					if (pagingVo.getTotalNumberOfPages() > intIndex) {
						ar.add(++intIndex);
						pagingVo.setEndPageNumber(intIndex);
					}

				}
			}
			pagingVo.setPageIdxArr(ar);
		}

	}

	public static void setStartRow(ApplicationPagingVo pagingVo,
			String actionName) {
		pagingVo.setMaxResult(10);
		if (pagingVo.getCurrentPageNumber() == 0) {
			pagingVo.setStartRow(0);
		} else {
			pagingVo.setStartRow(((pagingVo.getCurrentPageNumber() - 1) * pagingVo
					.getMaxResult()));
		}

		pagingVo.setActionName(actionName);
	}

	/**
	 * Used to print Exception Stack Trace
	 * 
	 * @param t
	 * @return
	 */

	public static String getUtilStackTrace(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		pw.flush();
		return sw.toString();
	}

	public Date getCurrentDatetime() {
		/*
		 * String curDttm = null; try { Calendar cal = Calendar.getInstance();
		 * DateFormat dtf = new SimpleDateFormat("yyyy-MM-dd"); curDttm =
		 * dtf.format(cal.getTime()); } catch (Exception e) {
		 * e.printStackTrace(); }
		 * 
		 * return curDttm;
		 */
		return (new Date());
	}

	public String getStringCurrentDatetime() {
		String curDttm = null;
		try {
			Calendar cal = Calendar.getInstance();
			DateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
			curDttm = dtf.format(cal.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return curDttm;
	}

	public double roundDoubleValue(double dVal) {
		double cVal = dVal;
		try {
			DecimalFormat df = new DecimalFormat("##.##");
			String amt = df.format(cVal);
			cVal = Double.parseDouble(amt);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return cVal;
	}

	public static String getPhoneFaxPattern1(String phoneFaxumber) {

		if (null != phoneFaxumber && phoneFaxumber.trim().length() > 0) {
			// First 3 Digits STD Code
			String stdCode = phoneFaxumber.substring(0, 2);
			return stdCode;
		}
		return null;
	}

	public static String getPhoneFaxPattern2(final String phoneFaxumber) {
		if (null != phoneFaxumber && phoneFaxumber.trim().length() > 0) {
			// First 4 Digits Code
			String firstFourDigits = phoneFaxumber.substring(2, 6);
			return firstFourDigits;
		}
		return null;
	}

	public static String getPhoneFaxPattern3(final String phoneFaxumber) {
		if (null != phoneFaxumber && phoneFaxumber.trim().length() > 0) {
			String lastFourDigits = phoneFaxumber.substring(6, 10);
			return lastFourDigits;
		}
		return null;
	}

	public static Date getCurrentDate() {
		Calendar cal = Calendar.getInstance();
		return cal.getTime();
	}
	
	public static int getCurrentDay() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	
	public static Date getCurrentDateMinusHr(int hr) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, hr);
		return cal.getTime();
	}

	public static Date getCurrentDateMinusMinute(int minute) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, minute);
		return cal.getTime();
	}

	public static Date getCurrentDateMinusDay(int day) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, day);
		return cal.getTime();
	}
	
	/**
	 * Method Name: formateToDate Method Description: This method is used to
	 * formate a Date
	 * 
	 * @param : String dateValue
	 * @return : Date date
	 * 
	 * @throws Exception
	 */
	public static Date formateToDate(final String dateValue) {
		Date date = null;
		try {
			if (null != dateValue) {
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				date = (Date) formatter.parse(dateValue);
			}
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Method Name: formateSlashToDate Method Description: This method is used to
	 * formate a Date
	 * 
	 * @param : String dateValue
	 * @return : Date date
	 * 
	 * @throws Exception
	 */
	public static Date formateSlashToDate(final String dateValue) {
		Date date = null;
		try {
			if (null != dateValue) {
				DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
				date = (Date) formatter.parse(dateValue);
			}
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	

	/**
	 * Method Name: getMobilePattern1 Method Description: This method is used to
	 * split the Mobile Number
	 * 
	 * @param : String mobileNumber
	 * @return : String mobilePattern1
	 * 
	 * @throws Exception
	 */
	public static String getMobilePattern1(String mobileNumber) {
		if (null != mobileNumber) {
			String mobilePattern1 = mobileNumber.substring(0, 3);
			return mobilePattern1;
		}
		return null;
	}

	/**
	 * Method Name: getMobilePattern2 Method Description: This method is used to
	 * split the Mobile Number
	 * 
	 * @param : String mobileNumber
	 * @return : String mobilePattern2
	 * 
	 * @throws Exception
	 */
	public static String getMobilePattern2(final String mobileNumber) {
		if (null != mobileNumber) {
			String mobilePattern2 = mobileNumber.substring(3, 7);
			return mobilePattern2;
		}
		return null;
	}

	/**
	 * Method Name: getMobilePattern3 Method Description: This method is used to
	 * split the Mobile Number
	 * 
	 * @param : String mobileNumber
	 * @return : String mobilePattern3
	 * 
	 * @throws Exception
	 */
	public static String getMobilePattern3(final String mobileNumber) {
		if (null != mobileNumber) {
			String mobilePattern3 = mobileNumber.substring(7, 11);
			return mobilePattern3;
		}
		return null;
	}

	/**
	 * Method Name: getMobileNumber Method Description: This method is used to
	 * append the Mobile number1,number2 & number3
	 * 
	 * @param : String mobileNumber
	 * @return : String mobilePattern3
	 * 
	 * @throws Exception
	 */
	public static String getMobileNumber(final String number1,
			final String number2, final String number3) {
		StringBuilder mobileNumber = new StringBuilder();
		mobileNumber.append(number1);
		mobileNumber.append(number2);
		mobileNumber.append(number3);
		return mobileNumber.toString();
	}

	/**
	 * Method Name: getPhonePattern1 Method Description: This method is used to
	 * split the Mobile Number
	 * 
	 * @param : String mobileNumber
	 * @return : String mobilePattern1
	 * 
	 * @throws Exception
	 */
	public static String getPhonePattern1(String phoneNumber) {
		if (null != phoneNumber) {
			String phonePattern1 = phoneNumber.substring(0, 2);
			return phonePattern1;
		}
		return null;
	}

	/**
	 * Method Name: getPhonePattern2 Method Description: This method is used to
	 * split the Mobile Number
	 * 
	 * @param : String mobileNumber
	 * @return : String mobilePattern2
	 * 
	 * @throws Exception
	 */
	public static String getPhonePattern2(final String phoneNumber) {
		if (null != phoneNumber) {
			String phonePattern2 = phoneNumber.substring(2, 6);
			return phonePattern2;
		}
		return null;
	}

	/**
	 * Method Name: getPhonePattern3 Method Description: This method is used to
	 * split the Mobile Number
	 * 
	 * @param : String phoneNumber
	 * @return : String phonePattern3
	 * 
	 * @throws Exception
	 */
	public static String getPhonePattern3(final String phoneNumber) {
		if (null != phoneNumber) {
			String phonePattern3 = phoneNumber.substring(6, 10);
			return phonePattern3;
		}
		return null;
	}

	/**
	 * Method Name: getPhoneNumber Method Description: This method is used to
	 * append the Phone number1,number2 & number3
	 * 
	 * @param : String phoneNumber
	 * @return : String phonePattern3
	 * 
	 * @throws Exception
	 */
	public static String getPhoneNumber(final String number1,
			final String number2, final String number3) {
		StringBuilder phoneNumber = new StringBuilder();
		phoneNumber.append(number1);
		phoneNumber.append(number2);
		phoneNumber.append(number3);
		return phoneNumber.toString();
	}

	/**
	 * @Method : formatDatetoString
	 * @Description : Format date(yyyy-MM-dd) to string
	 * @param date
	 * @return String
	 */
	public static String formatDatetoString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = sdf.format(date);
		return dateString;
	}
	
	
	/**
	 * @Method : formatDatetoSlashString
	 * @Description : Format date(yyyy/MM/dd) to string
	 * @param date
	 * @return String
	 */
	public static String formatDatetoSlashString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		String dateString = sdf.format(date);
		return dateString;
	}

	public static Date getDateFromDatetime(Date datetime) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(datetime);

		// Set time fields to zero
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		// Put it back in the Date object
		return cal.getTime();
	}

	public static Date getCurrentDateAsDate() {
		Calendar cal = Calendar.getInstance();

		// Set time fields to zero
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		// Put it back in the Date object
		return cal.getTime();
	}
	public static String getTodayDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd");
		return sdf.format(new Date());
	}
	public static Date getNextDateAsDate() {
		Calendar cal = Calendar.getInstance();

		cal.add(Calendar.DATE, +1);
		
		// Set time fields to zero
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		// Put it back in the Date object
		return cal.getTime();
	}

	/**
	 * @Method : getCurrentDateWithoutTime
	 * @Description :
	 * @return Date
	 */
	public static Date getCurrentDateWithoutTime() {
		Calendar currDtCal = Calendar.getInstance();
		currDtCal.set(Calendar.HOUR_OF_DAY, 0);
		currDtCal.set(Calendar.MINUTE, 0);
		currDtCal.set(Calendar.SECOND, 0);
		currDtCal.set(Calendar.MILLISECOND, 0);
		return currDtCal.getTime();
	}
	
	public static Date convertStartTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	public static Date convertEndTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}

	/**
	 * @Method : convertStringToDate
	 * @Description :
	 * @param dateStr
	 * @return Date
	 */
	public static Date convertStringToDate(String dateStr) {
		DateFormat formatter;
		Date date;
		formatter = new SimpleDateFormat("yyyyMMddhhmmss");
		try {
			date = (Date) formatter.parse(dateStr);
		} catch (ParseException e) {
			return null;
		}
		return date;
	}
	/**
	 * @Method : convertStringToDate
	 * @Description :
	 * @param dateStr
	 * @param formatStr
	 * @return Date
	 */
	public static String convertDateFormat(String dateStr, String fromFormatStr, String toFormatStr) {
		Date dt = convertStringToDate(dateStr,fromFormatStr);
		return convertdateTostring(dt, toFormatStr);
	}
	/**
	 * @Method : convertStringToDate
	 * @Description :
	 * @param dateStr
	 * @param formatStr
	 * @return Date
	 */
	public static Date convertStringToDate(String dateStr, String formatStr) {
		DateFormat formatter;
		Date date;
		formatter = new SimpleDateFormat(formatStr);
		try {
			date = (Date) formatter.parse(dateStr);
		} catch (ParseException e) {
			return null;
		}
		return date;
	}

	/**
	 * @Method : convert2DecimalDeg
	 * @Description : get decimal value of Lat or Lon of format DDDMM.MMMMM
	 * @param lat
	 * @return String
	 */
	public static String convert2DecimalDegLat(String lat) {
		String degstr, minstr;
		int length;
		Double min, deg;
		Double temp1;

		length = lat.length();
		int decimalIndex = lat.lastIndexOf('.');
		if (lat.contains(ConstantEnum.PLUS.getStringValue())
				|| lat.contains(ConstantEnum.MINUS.getStringValue())) {
			return lat;
		} else {
			if (decimalIndex < 4) {
				return lat;
			} else {
				int degEnd = 0;
				if (decimalIndex > 1 && decimalIndex < 5)
					degEnd = 2;
				else if (decimalIndex == 5)
					degEnd = 3;
				else
					return null;
				degstr = lat.substring(0, degEnd);
				minstr = lat.substring(degEnd, length);
				temp1 = new Double(degstr);
				deg = temp1.doubleValue();
				temp1 = new Double(minstr);
				min = temp1.doubleValue();
				min = min / 60;
				deg = deg + min;
				return deg.toString();
			}
		}

	}

	public static String convert2DecimalDegLon(String lon) {
		String degstr, minstr;
		int length;
		Double min, deg;
		Double temp1;

		length = lon.length();
		int decimalIndex = lon.lastIndexOf('.');
		if (lon.contains(ConstantEnum.PLUS.getStringValue())
				|| lon.contains(ConstantEnum.MINUS.getStringValue())) {
			return lon;
		} else {
			if (decimalIndex < 4) {
				return lon;
			} else {
				int degEnd = 0;
				if (decimalIndex > 1 && decimalIndex < 5)
					degEnd = 2;
				else if (decimalIndex == 5)
					degEnd = 3;
				else
					return null;
				degstr = lon.substring(0, degEnd);
				minstr = lon.substring(degEnd, length);
				temp1 = new Double(degstr);
				deg = temp1.doubleValue();
				temp1 = new Double(minstr);
				min = temp1.doubleValue();
				min = min / 60;
				deg = deg + min;
				return deg.toString();
			}
		}
	}

	/**
	 * @Method : convert2DecimalDeg
	 * @Description : get decimal value of Lat or Lon of format DDDMM.MMMMM
	 * @param lat
	 * @return double
	 */
	public static Double convert2DecimalDoubleLat(String lat) {
		String degstr, minstr;
		int length;
		Double min, deg;
		Double temp1;

		length = lat.length();
		int decimalIndex = lat.lastIndexOf('.');
		if (lat.contains(ConstantEnum.PLUS.getStringValue())
				|| lat.contains(ConstantEnum.MINUS.getStringValue())) {
			deg = Double.valueOf(lat);
		} else {
			if (decimalIndex < 4) {
				deg = Double.valueOf(lat);
			} else {
				int degEnd = 0;
				if (decimalIndex > 1 && decimalIndex < 5)
					degEnd = 2;
				else if (decimalIndex == 5)
					degEnd = 3;
				else
					return null;
				degstr = lat.substring(0, degEnd);
				minstr = lat.substring(degEnd, length);
				temp1 = new Double(degstr);
				deg = temp1.doubleValue();
				temp1 = new Double(minstr);
				min = temp1.doubleValue();
				min = min / 60;
				deg = deg + min;
			}
		}
		return deg;
	}

	/**
	 * @Method : convert2DecimalDeg
	 * @Description : get decimal value of Lat or Lon of format DDDMM.MMMMM
	 * @param lat
	 * @return double
	 */
	public static Double convert2DecimalDoubleLon(String lon) {
		String degstr, minstr;
		int length;
		Double min, deg;
		Double temp1;

		length = lon.length();
		int decimalIndex = lon.lastIndexOf('.');
		if (lon.contains(ConstantEnum.PLUS.getStringValue())
				|| lon.contains(ConstantEnum.MINUS.getStringValue())) {
			deg = Double.valueOf(lon);
		} else {
			if (decimalIndex < 4) {
				deg = Double.valueOf(lon);
			} else {
				int degEnd = 0;
				if (decimalIndex > 1 && decimalIndex < 5)
					degEnd = 2;
				else if (decimalIndex == 5)
					degEnd = 3;
				else
					return null;
				degstr = lon.substring(0, degEnd);
				minstr = lon.substring(degEnd, length);
				temp1 = new Double(degstr);
				deg = temp1.doubleValue();
				temp1 = new Double(minstr);
				min = temp1.doubleValue();
				min = min / 60;
				deg = deg + min;
			}
		}
		return deg;
	}

	/**
	 * @Method : distanceInMeters
	 * @Description : to find the distance between two points
	 * @param posLat
	 * @param posLon
	 * @param destLat
	 * @param destLon
	 * @return double
	 */
	public static double distanceInMeters(double posLat, double posLon,
			double destLat, double destLon) {
		double lon1, lat1, lon2, lat2, dlon, dlat, a, b, c, d;

		// convert our values to radians
		lat1 = Math.toRadians(posLat);
		lon1 = Math.toRadians(posLon);
		lat2 = Math.toRadians(destLat);
		lon2 = Math.toRadians(destLon);

		// start the calculations
		dlon = lon2 - lon1;
		dlat = lat2 - lat1;
		a = Math.pow((Math.sin(dlat / 2)), (double) 2);
		b = Math.cos(lat1) * Math.cos(lat2);
		c = Math.pow(Math.sin(dlon / 2), (double) 2);
		a = a + b * c;
		c = 2 * (Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
		d = RADIUS_KM * c * 1000;
		return d;
	}

	/**
	 * @Method : convertdateTostring
	 * @Description : convert date to specific string format
	 * @param date
	 * @param strFormat
	 * @return String
	 */
	public static String convertdateTostring(Date date, String strFormat) {
		String strDate = "";
		DateFormat formatter;
		formatter = new SimpleDateFormat(strFormat);
		if (date != null)
			strDate = formatter.format(date);
		return strDate;
	}

	/**
	 * 
	 * @Method : sortByComparator
	 * @Description : This Method is used to sort Map
	 * @param unsortMap
	 * @return sortedMap
	 */
	@SuppressWarnings("unchecked")
	public static Map<Long, String> sortByComparator(Map<Long, String> unsortMap) {

		List<Object> list = new ArrayList<Object>(unsortMap.entrySet());

		// sort list based on comparator
		Collections.sort(list, new Comparator<Object>() {
			public int compare(Object o1, Object o2) {
				return ((Comparable<String>) ((Map.Entry<Long, String>) (o1))
						.getValue().toLowerCase()).compareTo(((Map.Entry<Long, String>) (o2))
						.getValue().toLowerCase());
			}
		});
		// put sorted list into map again
		Map<Long, String> sortedMap = new LinkedHashMap<Long, String>();
		for (Iterator<Object> it = list.iterator(); it.hasNext();) {
			Map.Entry<Long, String> entry = (Map.Entry<Long, String>) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/**
	 * @Method : checkEan
	 * @Description : to check barcode is correct or not ean13
	 * @param eanCode
	 * @return boolean
	 */
	public static boolean checkEan(String eanCode) {
		// Check if only digits

		String ValidChars = "0123456789";

		for (int i = 0; i < eanCode.length(); i++) {

			Character digit = eanCode.charAt(i);

			if (ValidChars.indexOf(digit) == -1) {

				return false;

			}

		}

		// Add five 0 if the code has only 8 digits

		// if (eanCode.length() < 13) {
		//
		// //eanCode = "00000" + eanCode;
		// return false;
		//
		// }

		// Check for 13 digits otherwise

		if (eanCode.length() != 13) {

			return false;

		}

		// Get the check number

		int originalCheck = Integer.parseInt(eanCode.substring(eanCode.length() - 1));

		eanCode = eanCode.substring(0, eanCode.length() - 1);

		// Add even numbers together

		int even = Integer.parseInt(String.valueOf(eanCode.charAt(1))) +

		Integer.parseInt(String.valueOf(eanCode.charAt(3))) +

		Integer.parseInt(String.valueOf(eanCode.charAt(5))) +

		Integer.parseInt(String.valueOf(eanCode.charAt(7))) +

		Integer.parseInt(String.valueOf(eanCode.charAt(9))) +

		Integer.parseInt(String.valueOf(eanCode.charAt(11)));

		// Multiply this result by 3

		even *= 3;

		// Add odd numbers together

		int odd = Integer.parseInt(String.valueOf(eanCode.charAt(0))) +

				Integer.parseInt(String.valueOf(eanCode.charAt(2))) +

				Integer.parseInt(String.valueOf(eanCode.charAt(4))) +

				Integer.parseInt(String.valueOf(eanCode.charAt(6))) +

				Integer.parseInt(String.valueOf(eanCode.charAt(8))) +

				Integer.parseInt(String.valueOf(eanCode.charAt(10)));

		// Add two totals together

		int total = even + odd;

		// Calculate the checksum

		// Divide total by 10 and store the remainder

		int checksum = total % 10;

		// If result is not 0 then take away 10

		if (checksum != 0) {

			checksum = 10 - checksum;

		}

		// Return the result

		if (checksum != originalCheck) {

			return false;

		}

		return true;
	}
	
	
	/**
	 * 
	 * @Method : sortByComparator 
	 * @Description : This Method is used to sort Map By String Value
	 * @param unsortMap
	 * @return sortedMap
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, String> sortByComparatorString(Map<String, String> unsortMap) {

		List<Object> list = new ArrayList<Object>(unsortMap.entrySet());

		// sort list based on comparator
		Collections.sort(list, new Comparator<Object>() {
			public int compare(Object o1, Object o2) {
				return ((Comparable<String>) ((Map.Entry<String, String>) (o1))
						.getValue().toLowerCase()).compareTo(((Map.Entry<String, String>) (o2))
						.getValue().toLowerCase());
			}
		});
		// put sorted list into map again
		Map<String, String> sortedMap = new LinkedHashMap<String, String>();
		for (Iterator<Object> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	
	/**
	 * 
	 * @Method : sortByComparatorLong 
	 * @Description : This Method is used to sort Map By Long Value
	 * @param unsortMap
	 * @return sortedMap
	 */
	@SuppressWarnings("unchecked")
	public static Map<Long, Long> sortByComparatorLong(Map<Long, Long> unsortMap) {

		List<Object> list = new ArrayList<Object>(unsortMap.entrySet());

		// sort list based on comparator
		Collections.sort(list, new Comparator<Object>() {
			public int compare(Object o1, Object o2) {
				return ((Comparable<Long>) ((Map.Entry<Long, Long>) (o1))
						.getValue()).compareTo(((Map.Entry<Long, Long>) (o2))
						.getValue());
			}
		});
		// put sorted list into map again
		Map<Long, Long> sortedMap = new LinkedHashMap<Long, Long>();
		for (Iterator<Object> it = list.iterator(); it.hasNext();) {
			Map.Entry<Long, Long> entry = (Map.Entry<Long, Long>) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	
	
	/**
	 * 
	 * @Method : sortByComparator 
	 * @Description : This Method is used to sort Map By Integer Value
	 * @param unsortMap
	 * @return sortedMap
	 */
	@SuppressWarnings("unchecked")
	public static Map<Integer, String> sortByComparatorInteger(Map<Integer, String> unsortMap) {

		List<Object> list = new ArrayList<Object>(unsortMap.entrySet());

		// sort list based on comparator
		Collections.sort(list, new Comparator<Object>() {
			public int compare(Object o1, Object o2) {
				return ((Comparable<String>) ((Map.Entry<Integer, String>) (o1))
						.getValue().toLowerCase()).compareTo(((Map.Entry<Integer, String>) (o2))
						.getValue().toLowerCase());
			}
		});
		// put sorted list into map again
		Map<Integer, String> sortedMap = new LinkedHashMap<Integer, String>();
		for (Iterator<Object> it = list.iterator(); it.hasNext();) {
			Map.Entry<Integer, String> entry = (Map.Entry<Integer, String>) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	
	public static Long startJobId(String jobId){
		String zeroFill = "000000000000";
		if(jobId != null && jobId.trim().length() > 0){
			jobId += zeroFill.substring(jobId.length() - 1, 11);
			return Long.valueOf(jobId);
		}else{
			return Long.valueOf(zeroFill);
		}
		
	}
	
	public static Long endJobId(String jobId){
		String zeroFill = "999999999999";
		if(jobId != null && jobId.trim().length() > 0){
			jobId += zeroFill.substring(jobId.length() - 1, 11);
			return Long.valueOf(jobId);
		}else{
			return Long.valueOf(zeroFill);
		}
		
	}
	
//	public static String fromEmailAddress(){
//		ResourceBundle bundle = ResourceBundle.getBundle("send_mail");
//		return bundle.getString("mail.user.name");
//	}
//	
//	public static String smtpServer(){
//		ResourceBundle bundle = ResourceBundle.getBundle("send_mail");
//		return bundle.getString("mail.server.name");
//	}
	
	public static boolean checkInteger(String intString){
		String ValidChars = "0123456789";

		for (int i = 0; i < intString.length(); i++) {

			Character digit = intString.charAt(i);

			if (ValidChars.indexOf(digit) == -1) {

				return false;

			}

		}
		return true;

	}
	public static List<ComboVo> getConfigTypeList(Locale userLocale, Integer userType, String companyView, String screenView, String systemView ){
		List<ComboVo> confTypeList = new ArrayList<ComboVo>();
		if(userType.intValue() == ConstantEnum.USER_TYPE_1.getIntValue()){
			if(userLocale.getLanguage().equals(ConstantEnum.ENGLISHCODE.getStringValue())){
				ResourceBundle bundle = ResourceBundle.getBundle("ShiraiLabel_en_US");
				ComboVo comboVo1 = new ComboVo();
				comboVo1.setIdValue(ConstantEnum.CONF_COMPANY.getIntValue());
				comboVo1.setName(bundle.getString("COMPANYCONFIGURATION"));
				confTypeList.add(comboVo1);
				ComboVo comboVo2 = new ComboVo();
				comboVo2.setIdValue(ConstantEnum.CONF_SCREEN.getIntValue());
				comboVo2.setName(bundle.getString("SCREENCONFIGURATION"));
				confTypeList.add(comboVo2);
				ComboVo comboVo3 = new ComboVo();
				comboVo3.setIdValue(ConstantEnum.CONF_SYSTEM.getIntValue());
				comboVo3.setName(bundle.getString("SYSTEMCONFIGURATION"));
				confTypeList.add(comboVo3);
			}else{
				ResourceBundle bundle = ResourceBundle.getBundle("ShiraiLabel_ja_JP");
				ComboVo comboVo1 = new ComboVo();
				comboVo1.setIdValue(ConstantEnum.CONF_COMPANY.getIntValue());
				comboVo1.setName(bundle.getString("COMPANYCONFIGURATION"));
				confTypeList.add(comboVo1);
				ComboVo comboVo2 = new ComboVo();
				comboVo2.setIdValue(ConstantEnum.CONF_SCREEN.getIntValue());
				comboVo2.setName(bundle.getString("SCREENCONFIGURATION"));
				confTypeList.add(comboVo2);
				ComboVo comboVo3 = new ComboVo();
				comboVo3.setIdValue(ConstantEnum.CONF_SYSTEM.getIntValue());
				comboVo3.setName(bundle.getString("SYSTEMCONFIGURATION"));
				confTypeList.add(comboVo3);
			}
		} else if((userType.intValue() == ConstantEnum.USER_TYPE_3.getIntValue() ||
				userType.intValue() == ConstantEnum.USER_TYPE_4.getIntValue()) && (companyView != null && companyView.equals(ConstantEnum.FLAG_1.getStringValue()))){
			if(userLocale.getLanguage().equals(ConstantEnum.ENGLISHCODE.getStringValue())){
				ResourceBundle bundle = ResourceBundle.getBundle("ShiraiLabel_en_US");
				ComboVo comboVo1 = new ComboVo();
				comboVo1.setIdValue(ConstantEnum.CONF_COMPANY.getIntValue());
				comboVo1.setName(bundle.getString("COMPANYCONFIGURATION"));
				confTypeList.add(comboVo1);
			}else{
				ResourceBundle bundle = ResourceBundle.getBundle("ShiraiLabel_ja_JP");
				ComboVo comboVo1 = new ComboVo();
				comboVo1.setIdValue(ConstantEnum.CONF_COMPANY.getIntValue());
				comboVo1.setName(bundle.getString("COMPANYCONFIGURATION"));
				confTypeList.add(comboVo1);
			}
		} else if(userType.intValue() == ConstantEnum.USER_TYPE_2.getIntValue()){
			if(userLocale.getLanguage().equals(ConstantEnum.ENGLISHCODE.getStringValue())){
				ResourceBundle bundle = ResourceBundle.getBundle("ShiraiLabel_en_US");
				if(companyView != null && companyView.equals(ConstantEnum.FLAG_1.getStringValue())){
				ComboVo comboVo1 = new ComboVo();
				comboVo1.setIdValue(ConstantEnum.CONF_COMPANY.getIntValue());
				comboVo1.setName(bundle.getString("COMPANYCONFIGURATION"));
				confTypeList.add(comboVo1);
				}
				if(screenView != null && screenView.equals(ConstantEnum.FLAG_1.getStringValue())){
				ComboVo comboVo2 = new ComboVo();
				comboVo2.setIdValue(ConstantEnum.CONF_SCREEN.getIntValue());
				comboVo2.setName(bundle.getString("SCREENCONFIGURATION"));
				confTypeList.add(comboVo2);
				}
				if(systemView != null && systemView.equals(ConstantEnum.FLAG_1.getStringValue())){
				ComboVo comboVo3 = new ComboVo();
				comboVo3.setIdValue(ConstantEnum.CONF_SYSTEM.getIntValue());
				comboVo3.setName(bundle.getString("SYSTEMCONFIGURATION"));
				confTypeList.add(comboVo3);
				}
			}else{
				ResourceBundle bundle = ResourceBundle.getBundle("ShiraiLabel_ja_JP");
				if(companyView != null && companyView.equals(ConstantEnum.FLAG_1.getStringValue())){
				ComboVo comboVo1 = new ComboVo();
				comboVo1.setIdValue(ConstantEnum.CONF_COMPANY.getIntValue());
				comboVo1.setName(bundle.getString("COMPANYCONFIGURATION"));
				confTypeList.add(comboVo1);
				}
				if(screenView != null && screenView.equals(ConstantEnum.FLAG_1.getStringValue())){
				ComboVo comboVo2 = new ComboVo();
				comboVo2.setIdValue(ConstantEnum.CONF_SCREEN.getIntValue());
				comboVo2.setName(bundle.getString("SCREENCONFIGURATION"));
				confTypeList.add(comboVo2);
				}
				if(systemView != null && systemView.equals(ConstantEnum.FLAG_1.getStringValue())){
				ComboVo comboVo3 = new ComboVo();
				comboVo3.setIdValue(ConstantEnum.CONF_SYSTEM.getIntValue());
				comboVo3.setName(bundle.getString("SYSTEMCONFIGURATION"));
				confTypeList.add(comboVo3);
				}
			}
		}
		return confTypeList;
	}
	
	public static String getHeaderValue(String language){
		
		if(language.equals(ConstantEnum.ENGLISHCODE.getStringValue())){
			ResourceBundle bundle = ResourceBundle.getBundle("ShiraiLabel_en_US");
			return bundle.getString("SELECTOPTION");
		}else{
			ResourceBundle bundle = ResourceBundle.getBundle("ShiraiLabel_ja_JP");
			return bundle.getString("SELECTOPTION");
		}
	}
	
	
	public static String getPhoneFaxPattern(String phoneFaxumber, Integer start, Integer end) {
		if (null != phoneFaxumber && phoneFaxumber.trim().length() > 0) {
			// First 3 Digits STD Code
			String stdCode = phoneFaxumber.substring(start, end);
			return stdCode;
		}
		return null;
	}
	
	public static byte[] byteArrayPhoto(File originalFile, String extension)
			throws IOException {
		 	byte[] photoData = null;
			BufferedImage bufferedImage = ImageIO.read(originalFile);
			// convert BufferedImage to byte array
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bufferedImage, extension, baos);
			baos.flush();
			photoData = baos.toByteArray();
			baos.close();
			return photoData;
	}

	
	/**
	 * @Method : The method to change the date format
	 * @Description : formatDatetoString
	 * @param date Date
	 * @param format String
	 * @param locale Locale
	 * @return String
	 */
	public static String formatDatetoString(Date date, String format, Locale locale) {
		DateFormat dateFormat = new SimpleDateFormat(format).getDateInstance(DateFormat.LONG,locale);
		String dateString = dateFormat.format(date);
		return dateString;
	
	}
	
	public static boolean isBlank(String str){
		return (str == null || "".equals(str.trim()));
	}
	
	public static boolean isCurrentDate(Date date){
		if(date == null){ 
			return false;
		}
		Date currentDate = getCurrentDateWithoutTime();
		Date userDate    = convertStartTime(date);
		return currentDate.compareTo(userDate) == 0;
	}
	
	public static String getCurrentTime(){
		return new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
	}
	public static String getHomeDirectory(){
    	String userDir = System.getProperty("user.dir");
    	userDir = userDir.replaceAll(File.separator+File.separator+"bin", "");
    	return userDir;
    }
	public static String getReportDirectory(){
		return getHomeDirectory()+File.separator+"webapps"+File.separator+"Task"+File.separator+"report";
	}
	public static String getImageDirectory(){
		return getHomeDirectory()+File.separator+"webapps"+File.separator+"Task"+File.separator+"images";
	}
	public static String getExcelReportDirectory(){
		String reportDir = getHomeDirectory()+File.separator+"webapps"+File.separator+"Task"+File.separator+"ExcelReport";
		File file = new File(reportDir);
		if(!file.exists()){
			file.mkdir();
		}
		return reportDir;
	}
	public static String getLeaveFormDirectory(){
		return getHomeDirectory()+File.separator+"webapps"+File.separator+"Task"+File.separator+"LeaveForm";
	}
	
	public static String getLeaveFormLogoPath() {
	    return getHomeDirectory()+File.separator+"webapps"+File.separator+"Task"+File.separator+"images" + File.separator + "srm_logo.png";
	}
	
	public static String getCurrentTaskStartTime(){
		Calendar currDtCal = Calendar.getInstance();
		int min = currDtCal.get(Calendar.MINUTE);
		if(min==0){
			currDtCal.set(Calendar.MINUTE,0);
		}
		else if(min<=15){
			currDtCal.set(Calendar.MINUTE,15);
		}
		else if(min<=30){
			currDtCal.set(Calendar.MINUTE,30);
		}
		else if(min<=45){
			currDtCal.set(Calendar.MINUTE,45);
		}
		else if(min>45){
			currDtCal.set(Calendar.MINUTE,0);
			currDtCal.add(Calendar.HOUR, 1);
		}
		currDtCal.set(Calendar.SECOND, 0);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(currDtCal.getTime());
	}
	public static String getCurrentTaskEndTime(){
		Calendar currDtCal = Calendar.getInstance();
		currDtCal.add(Calendar.MINUTE, 15);
		int min = currDtCal.get(Calendar.MINUTE);
		if(min==0){
			currDtCal.set(Calendar.MINUTE,0);
		}
		else if(min<=15){
			currDtCal.set(Calendar.MINUTE,15);
		}
		else if(min<=30){
			currDtCal.set(Calendar.MINUTE,30);
		}
		else if(min<=45){
			currDtCal.set(Calendar.MINUTE,45);
		}
		else if(min>45){
			currDtCal.set(Calendar.MINUTE,0);
			currDtCal.add(Calendar.HOUR, 1);
		}
		currDtCal.set(Calendar.SECOND, 0);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(currDtCal.getTime());
	}
	
	public static long convertStringToTimestamp(String time, String format) throws ParseException{
		SimpleDateFormat _dateFmt = new SimpleDateFormat(format);
		long _ctime = _dateFmt.parse(time).getTime();
		Timestamp _ts = new Timestamp(_ctime);
	    return _ts.getTime() ;
		
	}
}

