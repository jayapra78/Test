/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;


import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * The Class ContextLoader.
 */
public class ContextLoader {


	/** The app context. */
	private static ApplicationContext appContext;
	
	/**
	 * Inits the.
	 *
	 * @param path the path
	 */
	public static void init(final String[] path) {
		if (path == null || path.length == 0) {			
			throw new NullPointerException("path is null or empty["+path+"]");
		}
		for (String filePath : path) {
			if (filePath == null || filePath.length() <= 0) {		
				throw new NullPointerException("path is null or empty["+filePath+"]");
			}
		}
		synchronized (ContextLoader.class) {
			try {				
				appContext = new ClassPathXmlApplicationContext(path);
			} catch (BeansException beanExp) {
				throw beanExp;
			}
		}
	}
	
	/**
	 * Gets the bean.
	 *
	 * @param <E> the element type
	 * @param beanId the bean id
	 * @return the bean
	 */
	public static <E> E getBean(final String beanId) {
		
			if (appContext == null) {
				final String[] path = new String[] { "applicationContext.xml" };
				init(path);
			}

			if(beanId == null || beanId.trim().length() <= 0) {
				throw new NullPointerException("beanId should not be null.");
			}
			
			Object object = null;
			try {
				object = appContext.getBean(beanId);
			} catch (BeansException beansExp) {
				beansExp.printStackTrace();
				throw new NullPointerException("Error while loading beanId["+beanId+"]");
			}

			@SuppressWarnings("unchecked")
			E returnObject = (E) object;
			
			return returnObject;

		}

	
}
