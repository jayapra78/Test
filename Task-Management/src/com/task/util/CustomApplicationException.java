/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.hibernate.HibernateException;
import org.hibernate.exception.GenericJDBCException;
import org.springframework.transaction.CannotCreateTransactionException;

import com.task.common.UserVo;

@SuppressWarnings("serial")
public class CustomApplicationException extends Exception {
	
	public String messageText;
	public String traceMessage;
	public String logMessage;
	public String displayMessage;
	public String messageCode;
	
	public CustomApplicationException(Exception e) {
		messageText = e.getLocalizedMessage();
		traceMessage = getUtilStackTrace(e);
		logMessage = messageText + "---" + traceMessage;
	}
	
	public CustomApplicationException(CannotCreateTransactionException e) {
		messageText = e.getLocalizedMessage();
		traceMessage = getUtilStackTrace(e);
		logMessage = messageText + "---" + traceMessage;
		messageCode = "e001";
	}
	
	public CustomApplicationException(String methodName,HibernateException he) {
		messageText = he.getMessage();
		displayMessage = getDisplayMessageByMethod(methodName) + " Please check the log for details.";
		traceMessage = getUtilStackTrace(he);
		logMessage = messageText + "---" + traceMessage;
	}
	
	public CustomApplicationException(GenericJDBCException gjdbc) {
		messageText = gjdbc.getMessage();
		displayMessage = " Please check the log for details.";
		traceMessage = getUtilStackTrace(gjdbc);
		logMessage = messageText + "---" + traceMessage;
		messageCode = "e001";
	}


	public CustomApplicationException(String methodName,Exception ex) {
		super(ex.getMessage());
		messageText = ex.getMessage();
		displayMessage = getDisplayMessageByMethod(methodName) +  "Please check the log for details.";
		traceMessage = getUtilStackTrace(ex);
		logMessage = messageText + "---" + traceMessage;
		if(ex instanceof GenericJDBCException){
			messageCode = "e001";
		}
	}
	
	public CustomApplicationException(String methodName,UserVo userVo, Exception ex) {
		super(ex.getMessage());
		messageText = ex.getMessage();
		displayMessage = getDisplayMessageByMethod(methodName) +  "Please check the log for details.";
		traceMessage = getUtilStackTrace(ex);
		logMessage = messageText + "---" + traceMessage;
	}
	
	private String getDisplayMessageByMethod(String methodName) {
		if (methodName == null || methodName.length() <=0)
			return FrameworkConstant.getError("UTIL_EXCEPTION_MSG");
		
		return FrameworkConstant.getError("UTIL_EXCEPTION");
		
	}
	
	private String getUtilStackTrace(Throwable t) {
    	StringWriter sw = new StringWriter();
    	PrintWriter pw = new PrintWriter(sw);
    	t.printStackTrace(pw);
    	pw.flush();
    	return sw.toString();
   }	 
	
	@SuppressWarnings("unused")
	private String getMessageText() {
		return messageText;
	}

	@SuppressWarnings("unused")
	private void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	@SuppressWarnings("unused")
	private String getTraceMessage() {
		return traceMessage;
	}

	@SuppressWarnings("unused")
	private void setTraceMessage(String traceMessage) {
		this.traceMessage = traceMessage;
	}

	@SuppressWarnings("unused")
	public String getLogMessage() {
		return logMessage;
	}

	public void setLogMessage(String logMessage) {
		this.logMessage = logMessage;
	}

	public String getDisplayMessage() {
		return displayMessage;
	}

	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}

	/**
	 * Getter method for messageCode
	 * 
	 * @return the messageCode (String)
	 */
	public String getMessageCode() {
		return messageCode;
	}

	/**
	 * Setter method for messageCode
	 *
	 * @param messageCode the messageCode to set
	 */
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}


}
