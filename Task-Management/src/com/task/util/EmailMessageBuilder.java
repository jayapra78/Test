/**
 * Creater / sengottaiyansSRM copyright information to Shirai 2013
 * 
 */
package com.task.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.task.common.ComboVo;
import com.task.common.MailVo;
import com.task.vo.custom.action.TaskActionVo;

/**
 * @author sengottaiyans
 *
 */
public class EmailMessageBuilder {
	
	private static final String projectStyle = "style='background-color: #dde4e8;'";
	private static final String planStyle    = "style='background-color:#A9D0F5'";
	private static final String actualStyle  = "style='background-color:#b0eaea'";

	public static MailVo getConsolidatedPmMailVo(Map<String,Map<String,List<TaskActionVo>>> ptMap, String pmEmailId){
		
		String currentDate = CommonUtils.convertdateTostring(CommonUtils.getCurrentDate(), "dd/MM/yyyy");
		
		MailVo mailVo = new MailVo();
		mailVo.setMailTo(new String[] {pmEmailId});
        
        StringBuilder subjectBuilder = new StringBuilder("");
        subjectBuilder.append("Employee consolidated task list on "+currentDate);
        mailVo.setSubject(subjectBuilder.toString());
		
		StringBuilder msgBuilder = new StringBuilder("");
		
		Set<String> ccList = new HashSet<String>();
		
		Set<String> ptList 	 = ptMap.keySet();
		int noOfProjectType  = ptList.size();
		int projectTypeIndex = 1;
		boolean isGlobalHeaderAdded = false;
		
		for (String projectType : ptList) {
			
			boolean isProjectTypeHeaderAdded = false;
			
			Map<String,List<TaskActionVo>> empMap = ptMap.get(projectType);
			Set<String> empList = empMap.keySet();
			int noOfEmp = empList.size();
			int empIndex = 1;
			
			for (String empId : empList) {
				
				boolean empHeaderAdded = false;
				
				List<TaskActionVo> empTaskVoList = empMap.get(empId);
				
				String oddRow  = "<tr style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 12px;line-height: 25px;padding: 0px 0px 0px 3px;background-color: #dde4e8;'>";
				String evenRow = "<tr style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 12px;line-height: 25px;padding: 0px 0px 0px 3px;background-color: #D8D8D8;'>";
				
				boolean isOddRow = true;
				int rowNo = 1;
				int empNoOfTask = empTaskVoList.size();
				
				for (TaskActionVo taskActionVo : empTaskVoList) {
					if(taskActionVo.getPlEmailId()!=null && !"".equals(taskActionVo.getPlEmailId())){
						ccList.add(taskActionVo.getPlEmailId());
					}
					
					if(isGlobalHeaderAdded==false){
						
						msgBuilder.append("<html>");
						msgBuilder.append("<head></head>");
						msgBuilder.append("<body style='background-color: #F8F8F8;'>");
						msgBuilder.append("<b style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>Dear "+taskActionVo.getPmName()+" san,</b>");
						msgBuilder.append("<br><br>");
						msgBuilder.append("<div style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
						msgBuilder.append("The below details are employee consolidated task on "+currentDate+".</div>");
						
						isGlobalHeaderAdded=true;
					}
					
					if(isProjectTypeHeaderAdded==false){
						
						msgBuilder.append("<br>");
						msgBuilder.append("<fieldset style='border-color:#474343;border-style: solid;'>");
						msgBuilder.append("<legend style='border-style:none;background-color: #474343;font-family: Tahoma, Arial, Helvetica;font-weight: bold;font-size: 9.5pt;Color: White;width:20%;padding-left:10px;'>");
						msgBuilder.append("Project Type : "+taskActionVo.getProjectTypeActionVo().getProjectType());
						msgBuilder.append("</legend>");
						
						isProjectTypeHeaderAdded=true;
					}
					
					if(empHeaderAdded==false){
						
						msgBuilder.append("<table border='1' cellpadding='0' cellspacing='0' width='975px' style='background-color:#CED8F6;border-style:solid;border-width:3px;border-color:#BBABAB;'>");
						msgBuilder.append("<tbody>");
						
						msgBuilder.append("<tr style='font: 11px/17px Verdana, Arial, Helvetica, sans-serif;color:#333;font-size:12px;font-weight: bold;'>");
						msgBuilder.append("<td colspan='4'>&nbsp;Employee Name : "+taskActionVo.getEmpName()+"</td>");
						msgBuilder.append("<td colspan='4'>&nbsp;</td>");
						msgBuilder.append("<td colspan='2' align='right'>Employee Id: "+taskActionVo.getEmployeeId()+" &nbsp;</td>");
						msgBuilder.append("</tr>");
						
						msgBuilder.append("<tr>");
						msgBuilder.append("<td colspan='2' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 3px;'>Project Details</td>");
						msgBuilder.append("<td colspan='3' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 3px;'>Plan</td>");
						msgBuilder.append("<td colspan='5' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 0px;'>Actual</td>");
						msgBuilder.append("</tr>");
						msgBuilder.append("<tr style='background-color:#2d4a59;color: #fff;font-family: Arial, verdena, sans-serif;font-size: 12px;text-align: left;line-height: 25px;padding: 0px 0px 0px 3px;'>");
						msgBuilder.append("<td align='center' width='40px'>S.no</td>");
						msgBuilder.append("<td width='90px'>Project</td>");
						msgBuilder.append("<td width='160px'>Description</td>");
						msgBuilder.append("<td width='70px'>Start Time</td>");
						msgBuilder.append("<td width='70px'>End Time</td>");
						msgBuilder.append("<td width='160px'>Description</td>");
						msgBuilder.append("<td width='70px'>Start Time</td>");
						msgBuilder.append("<td width='70px'>End Time</td>");
						msgBuilder.append("<td width='50px'>Activity</td>");
						msgBuilder.append("<td width='80px'>Work Item</td>");
						msgBuilder.append("</tr>");
						
						empHeaderAdded = true;
					}
					msgBuilder.append(isOddRow?oddRow:evenRow);
					isOddRow = !isOddRow;
					
					msgBuilder.append("<td "+projectStyle+" align='center'>"+rowNo+"</td>");
					msgBuilder.append("<td "+projectStyle+" >"+fillBlank(taskActionVo.getProjectName())+"</td>");
					msgBuilder.append("<td "+planStyle+" >"+fillBlank(taskActionVo.getPlanTaskDesc())+"</td>");
					msgBuilder.append("<td "+planStyle+" >"+fillBlank(taskActionVo.getPlanStartTime())+"</td>");
					msgBuilder.append("<td "+planStyle+" >"+fillBlank(taskActionVo.getPlanEndTime())+"</td>");
					msgBuilder.append("<td "+actualStyle+" >"+fillBlank(taskActionVo.getActualTaskDesc())+"</td>");
					msgBuilder.append("<td "+actualStyle+" >"+fillBlank(taskActionVo.getActualStartTime())+"</td>");
					msgBuilder.append("<td "+actualStyle+" >"+fillBlank(taskActionVo.getActualEndTime())+"</td>");
					msgBuilder.append("<td "+actualStyle+" >"+fillBlank(taskActionVo.getActivity())+"</td>");
					msgBuilder.append("<td "+actualStyle+" >"+fillBlank(taskActionVo.getWorkItem())+"</td>");
					msgBuilder.append("</tr>");
					
					//Closing employee table
					if(rowNo==empNoOfTask){
						msgBuilder.append("</tbody>");
						msgBuilder.append("</table>");
						msgBuilder.append("<br>");
					}
					
					rowNo++;
				}
				//Closing project type table
				if(empIndex==noOfEmp){
					msgBuilder.append("</fieldset>");
					msgBuilder.append("<br>");
				}
				empIndex++;
			}
			if(noOfProjectType==projectTypeIndex){
				msgBuilder.append("<div style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>Note: This is auto generated mail, please do not reply.</div>");
				msgBuilder.append("</body>");
				msgBuilder.append("</html>");
			}
			projectTypeIndex++;
		}
		/*if(ccList!=null && ccList.size()>0){
			String cc[] = new String[ccList.size()];
			ccList.toArray(cc);
			mailVo.setMailCc(cc);
		}*/
		mailVo.setMessage(msgBuilder.toString());
		return mailVo;
	}
	
	public static MailVo getConsolidatedProjectMailVo(String projectName,Map<String,List<TaskActionVo>> empMap){
		
		String currentDate = CommonUtils.convertdateTostring(CommonUtils.getCurrentDate(), "dd/MM/yyyy");
		
		MailVo mailVo = new MailVo();
        mailVo.setSubject("Task list for "+projectName+" on "+currentDate);
		StringBuilder msgBuilder = new StringBuilder("");
		
		Set<String> empList = empMap.keySet();
		int noOfEmp = empList.size();
		int empIndex = 1;
		
		boolean isGlobalHeaderAdded = false;
		
		for (String empId : empList) {
			
			boolean empHeaderAdded = false;
				
			List<TaskActionVo> empTaskVoList = empMap.get(empId);
				
			String oddRow  = "<tr style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 12px;line-height: 25px;padding: 0px 0px 0px 3px;background-color: #dde4e8;'>";
			String evenRow = "<tr style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 12px;line-height: 25px;padding: 0px 0px 0px 3px;background-color: #D8D8D8;'>";
			
			boolean isOddRow = true;
			int rowNo = 1;
			int empNoOfTask = empTaskVoList.size();
			
			for (TaskActionVo taskActionVo : empTaskVoList) {
					
				if(isGlobalHeaderAdded==false){
					
					mailVo.setMailTo(new String[] {taskActionVo.getPmEmailId()});
					
					/******* Added customer mail ID*******/
					String plMailId = taskActionVo.getPlEmailId();
					if(plMailId!=null && plMailId.length()>0){
						plMailId = plMailId+",";
					}
					else{
						plMailId = "";
					}
					
					String customerMailId = taskActionVo.getCustomerEmailId();
					if(customerMailId!=null && customerMailId.length()>0){
						customerMailId = customerMailId+",";
					}
					else{
						customerMailId = "";
					}
					
					String ccList[] = (plMailId+customerMailId).split(",");
					mailVo.setMailCc(ccList);
					
					/******* Added customer mail ID*******/
					
					/*if(taskActionVo.getPlEmailId()!=null && taskActionVo.getPlEmailId().length()>0){
						mailVo.setMailCc(new String[] {taskActionVo.getPlEmailId()});
					}*/
					
					msgBuilder.append("<html>");
					msgBuilder.append("<head></head>");
					msgBuilder.append("<body style='background-color: #F8F8F8;'>");
					msgBuilder.append("<b style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>Dear "+taskActionVo.getPmName()+" san,</b>");
					msgBuilder.append("<br><br>");
					msgBuilder.append("<div style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
					msgBuilder.append("The below details are task list for project "+projectName+" on "+currentDate+".</div><br>");
					
					isGlobalHeaderAdded=true;
				}
					
				if(empHeaderAdded==false){
						
					msgBuilder.append("<table border='1' cellpadding='0' cellspacing='0' width='975px' style='background-color:#CED8F6;border-style:solid;border-width:3px;border-color:#BBABAB;'>");
					msgBuilder.append("<tbody>");
					
					msgBuilder.append("<tr style='font: 11px/17px Verdana, Arial, Helvetica, sans-serif;color:#333;font-size:12px;font-weight: bold;'>");
					msgBuilder.append("<td colspan='6'>&nbsp;Employee Name : "+taskActionVo.getEmpName()+"</td>");
					msgBuilder.append("<td colspan='6' align='right'>Employee Id: "+taskActionVo.getEmployeeId()+" &nbsp;</td>");
					msgBuilder.append("</tr>");
					
					msgBuilder.append("<tr>");
					msgBuilder.append("<td colspan='4' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 3px;'>Project Details</td>");
					msgBuilder.append("<td colspan='3' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 3px;'>Plan</td>");
					msgBuilder.append("<td colspan='5' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 0px;'>Actual</td>");
					msgBuilder.append("</tr>");
					msgBuilder.append("<tr style='background-color:#2d4a59;color: #fff;font-family: Arial, verdena, sans-serif;font-size: 12px;text-align: left;line-height: 25px;padding: 0px 0px 0px 3px;'>");
					msgBuilder.append("<td align='center' width='40px'>S.no</td>");
					msgBuilder.append("<td width='82px'>Project Type</td>");
					msgBuilder.append("<td width='110px'>Project</td>");
					msgBuilder.append("<td width='70px'>Manager</td>");
					msgBuilder.append("<td width='160px'>Description</td>");
					msgBuilder.append("<td width='62px'>Start Time</td>");
					msgBuilder.append("<td width='62px'>End Time</td>");
					msgBuilder.append("<td width='160px'>Description</td>");
					msgBuilder.append("<td width='62px'>Start Time</td>");
					msgBuilder.append("<td width='62px'>End Time</td>");
					msgBuilder.append("<td width='50px'>Activity</td>");
					msgBuilder.append("<td width='80px'>Work Item</td>");
					msgBuilder.append("</tr>");
					
					empHeaderAdded = true;
				}
				msgBuilder.append(isOddRow?oddRow:evenRow);
				isOddRow = !isOddRow;
				
				msgBuilder.append("<td "+projectStyle+" align='center'>"+rowNo+"</td>");
				msgBuilder.append("<td "+projectStyle+">"+fillBlank(taskActionVo.getProjectTypeActionVo().getProjectType())+"</td>");
				msgBuilder.append("<td "+projectStyle+">"+fillBlank(taskActionVo.getProjectName())+"</td>");
				msgBuilder.append("<td "+projectStyle+">"+fillBlank(taskActionVo.getPmName())+"</td>");
				msgBuilder.append("<td "+planStyle+">"+fillBlank(taskActionVo.getPlanTaskDesc())+"</td>");
				msgBuilder.append("<td "+planStyle+">"+fillBlank(taskActionVo.getPlanStartTime())+"</td>");
				msgBuilder.append("<td "+planStyle+">"+fillBlank(taskActionVo.getPlanEndTime())+"</td>");
				msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActualTaskDesc())+"</td>");
				msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActualStartTime())+"</td>");
				msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActualEndTime())+"</td>");
				msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActivity())+"</td>");
				msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getWorkItem())+"</td>");
				msgBuilder.append("</tr>");
				
				//Closing employee table
				if(rowNo==empNoOfTask){
					msgBuilder.append("</tbody>");
					msgBuilder.append("</table>");
					msgBuilder.append("<br>");
				}
				
				rowNo++;
			}
			//Closing project type table
			if(empIndex==noOfEmp){
				msgBuilder.append("<div style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>Note: This is auto generated mail, please do not reply.</div>");
				msgBuilder.append("</body>");
				msgBuilder.append("</html>");
			}
			empIndex++;
		}
		
		mailVo.setMessage(msgBuilder.toString());
		return mailVo;
	}
	
	public static MailVo getConsolidatedSBUMailVo(Map<String,List<TaskActionVo>> empMap, List<ComboVo> noTaskEmpList){
		
		String currentDate = CommonUtils.convertdateTostring(CommonUtils.getCurrentDate(), "dd/MM/yyyy");
		
		MailVo mailVo = new MailVo();
        mailVo.setSubject("Employee consolidated task list on "+currentDate);
        mailVo.setMailCc(new String[] {"balajir@srmtech.com"});
		StringBuilder msgBuilder = new StringBuilder("");
		
		Set<String> empList = empMap.keySet();
		int noOfEmp = empList.size();
		int empIndex = 1;
		
		boolean isGlobalHeaderAdded = false;
		
		for (String empId : empList) {
			
			boolean empHeaderAdded = false;
				
			List<TaskActionVo> empTaskVoList = empMap.get(empId);
				
			String oddRow  = "<tr style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 12px;line-height: 25px;padding: 0px 0px 0px 3px;background-color: #dde4e8;'>";
			String evenRow = "<tr style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 12px;line-height: 25px;padding: 0px 0px 0px 3px;background-color: #D8D8D8;'>";
			
			boolean isOddRow = true;
			int rowNo = 1;
			int empNoOfTask = empTaskVoList.size();
			
			for (TaskActionVo taskActionVo : empTaskVoList) {
					
				if(isGlobalHeaderAdded==false){
					
					mailVo.setMailTo(new String[] {taskActionVo.getSbuEmailId()});
					
					msgBuilder.append("<html>");
					msgBuilder.append("<head></head>");
					msgBuilder.append("<body style='background-color: #F8F8F8;'>");
					msgBuilder.append("<b style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>Dear "+taskActionVo.getSbuName()+" san,</b>");
					msgBuilder.append("<br><br>");
					msgBuilder.append("<div style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
					msgBuilder.append("The below details are employee consolidated task on "+currentDate+".</div><br>");
					
					isGlobalHeaderAdded=true;
				}
					
				if(empHeaderAdded==false){
						
					msgBuilder.append("<table border='1' cellpadding='0' cellspacing='0' width='975px' style='background-color:#CED8F6;border-style:solid;border-width:3px;border-color:#BBABAB;'>");
					msgBuilder.append("<tbody>");
					
					msgBuilder.append("<tr style='font: 11px/17px Verdana, Arial, Helvetica, sans-serif;color:#333;font-size:12px;font-weight: bold;'>");
					msgBuilder.append("<td colspan='6'>&nbsp;Employee Name : "+taskActionVo.getEmpName()+"</td>");
					msgBuilder.append("<td colspan='6' align='right'>Employee Id: "+taskActionVo.getEmployeeId()+" &nbsp;</td>");
					msgBuilder.append("</tr>");
					
					msgBuilder.append("<tr>");
					msgBuilder.append("<td colspan='4' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 3px;'>Project Details</td>");
					msgBuilder.append("<td colspan='3' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 3px;'>Plan</td>");
					msgBuilder.append("<td colspan='5' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 0px;'>Actual</td>");
					msgBuilder.append("</tr>");
					msgBuilder.append("<tr style='background-color:#2d4a59;color: #fff;font-family: Arial, verdena, sans-serif;font-size: 12px;text-align: left;line-height: 25px;padding: 0px 0px 0px 3px;'>");
					msgBuilder.append("<td align='center' width='40px'>S.no</td>");
					msgBuilder.append("<td width='82px'>Project Type</td>");
					msgBuilder.append("<td width='110px'>Project</td>");
					msgBuilder.append("<td width='70px'>Manager</td>");
					msgBuilder.append("<td width='160px'>Description</td>");
					msgBuilder.append("<td width='62px'>Start Time</td>");
					msgBuilder.append("<td width='62px'>End Time</td>");
					msgBuilder.append("<td width='160px'>Description</td>");
					msgBuilder.append("<td width='62px'>Start Time</td>");
					msgBuilder.append("<td width='62px'>End Time</td>");
					msgBuilder.append("<td width='50px'>Activity</td>");
					msgBuilder.append("<td width='80px'>Work Item</td>");
					msgBuilder.append("</tr>");
					
					empHeaderAdded = true;
				}
				msgBuilder.append(isOddRow?oddRow:evenRow);
				isOddRow = !isOddRow;
				
				msgBuilder.append("<td "+projectStyle+" align='center'>"+rowNo+"</td>");
				msgBuilder.append("<td "+projectStyle+">"+fillBlank(taskActionVo.getProjectTypeActionVo().getProjectType())+"</td>");
				msgBuilder.append("<td "+projectStyle+">"+fillBlank(taskActionVo.getProjectName())+"</td>");
				msgBuilder.append("<td "+projectStyle+">"+fillBlank(taskActionVo.getPmName())+"</td>");
				msgBuilder.append("<td "+planStyle+">"+fillBlank(taskActionVo.getPlanTaskDesc())+"</td>");
				msgBuilder.append("<td "+planStyle+">"+fillBlank(taskActionVo.getPlanStartTime())+"</td>");
				msgBuilder.append("<td "+planStyle+">"+fillBlank(taskActionVo.getPlanEndTime())+"</td>");
				msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActualTaskDesc())+"</td>");
				msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActualStartTime())+"</td>");
				msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActualEndTime())+"</td>");
				msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActivity())+"</td>");
				msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getWorkItem())+"</td>");
				msgBuilder.append("</tr>");
				
				//Closing employee table
				if(rowNo==empNoOfTask){
					msgBuilder.append("</tbody>");
					msgBuilder.append("</table>");
					msgBuilder.append("<br>");
				}
				
				rowNo++;
			}
			//Closing project type table
			if(empIndex==noOfEmp){
				
				if(noTaskEmpList!=null && noTaskEmpList.size()>0){
					msgBuilder.append("<table border='1' cellpadding='0' cellspacing='0' width='400px' style='background-color:#CED8F6;border-style:solid;border-width:3px;border-color:#BBABAB;'>");
					msgBuilder.append("<tbody>");
					msgBuilder.append("<tr>");
					msgBuilder.append("<td colspan='3' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 3px;'>Following employees are not entered timesheet today</td>");
					msgBuilder.append("</tr>");
					msgBuilder.append("<tr style='background-color:#2d4a59;color: #fff;font-family: Arial, verdena, sans-serif;font-size: 12px;text-align: left;line-height: 25px;padding: 0px 0px 0px 3px;'>");
					msgBuilder.append("<td align='center' width='40px'>S.no</td>");
					msgBuilder.append("<td width='150px'>Employee Name</td>");
					msgBuilder.append("<td width='70px'>Employee Code</td>");
					msgBuilder.append("</tr>");
					
					
				}
				int sNo = 1;
				for (ComboVo emp : noTaskEmpList) {
					String empName = emp.getName();
					String empCode = emp.getKey();
					
					msgBuilder.append(isOddRow?oddRow:evenRow);
					isOddRow = !isOddRow;
					
					msgBuilder.append("<td align='center'>"+sNo+"</td>");
					msgBuilder.append("<td>"+empName+"</td>");
					msgBuilder.append("<td>"+empCode+"</td>");
					msgBuilder.append("</tr>");
					sNo++;
				}
				
				if(noTaskEmpList!=null && noTaskEmpList.size()>0){
					msgBuilder.append("</tbody>");
					msgBuilder.append("</table>");
					msgBuilder.append("<br>");
				}
				
				msgBuilder.append("<div style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>Note: This is auto generated mail, please do not reply.</div>");
				msgBuilder.append("</body>");
				msgBuilder.append("</html>");
			}
			empIndex++;
		}
		
		mailVo.setMessage(msgBuilder.toString());
		return mailVo;
	}
	
	public static MailVo getIndividualTaskMailVo(List<TaskActionVo> taskActionVoList, String cc){
		
		String currentDate = CommonUtils.convertdateTostring(CommonUtils.getCurrentDate(), "dd/MM/yyyy");
		
		MailVo mailVo = new MailVo();
		
		String oddRow  = "<tr style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 12px;line-height: 25px;padding: 0px 0px 0px 3px;background-color: #dde4e8;'>";
		String evenRow = "<tr style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 12px;line-height: 25px;padding: 0px 0px 0px 3px;background-color: #D8D8D8;'>";
		
		boolean isGlobalHeaderAdded = false;
		boolean empHeaderAdded = false;
		
		boolean isOddRow = true;
		int rowNo = 1;
		int empNoOfTask = taskActionVoList.size();
		
		StringBuilder msgBuilder = new StringBuilder("");
		
		List<String> mailToAddress = new ArrayList<String>();
		
		for (TaskActionVo taskActionVo : taskActionVoList) {
			
			if(!mailToAddress.contains(taskActionVo.getPmEmailId())){
				if(taskActionVo.getPmEmailId()!=null && taskActionVo.getPmEmailId().trim().length()>0){
					mailToAddress.add(taskActionVo.getPmEmailId());
				}
			}
			
			if(isGlobalHeaderAdded==false){
				
				mailVo.setMailTo(new String[] {taskActionVo.getPmEmailId()});
				
				String plMailId = taskActionVo.getPlEmailId();
				if(plMailId!=null && plMailId.length()>0){
					plMailId = plMailId+",";
				}
				else{
					plMailId = "";
				}
				
				String customerMailId = taskActionVo.getCustomerEmailId();
				if(customerMailId!=null && customerMailId.length()>0){
					customerMailId = customerMailId+",";
				} else{
					customerMailId = "";
				}
				
				if(cc!=null && cc.length()>0){
					String ccList[] = (plMailId+customerMailId+taskActionVo.getSbuEmailId()+","+cc).split(",");
					mailVo.setMailCc(ccList);
				}
				else{
					String ccList[] = (plMailId+customerMailId+taskActionVo.getSbuEmailId()).split(",");
					mailVo.setMailCc(ccList);
				}
				
				mailVo.setSubject(taskActionVo.getEmpName()+" TaskList ("+currentDate+")");
				msgBuilder.append("<html>");
				msgBuilder.append("<head></head>");
				msgBuilder.append("<body style='background-color: #F8F8F8;'>");
				msgBuilder.append("<b style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>Dear "+taskActionVo.getPmName()+" san,</b>");
				msgBuilder.append("<br><br>");
				msgBuilder.append("<div style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
				msgBuilder.append("The below details are my today("+currentDate+") task list.</div><br>");
				
				isGlobalHeaderAdded=true;
			}
				
			if(empHeaderAdded==false){
					
				msgBuilder.append("<table border='1' cellpadding='0' cellspacing='0' width='975px' style='background-color:#CED8F6;border-style:solid;border-width:3px;border-color:#BBABAB;'>");
				msgBuilder.append("<tbody>");
				
				msgBuilder.append("<tr style='font: 11px/17px Verdana, Arial, Helvetica, sans-serif;color:#333;font-size:12px;font-weight: bold;'>");
				msgBuilder.append("<td colspan='6'>&nbsp;Employee Name : "+taskActionVo.getEmpName()+"</td>");
				msgBuilder.append("<td colspan='6' align='right'>Employee Id: "+taskActionVo.getEmployeeId()+" &nbsp;</td>");
				msgBuilder.append("</tr>");
				
				msgBuilder.append("<tr>");
				msgBuilder.append("<td colspan='4' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 3px;'>Project Details</td>");
				msgBuilder.append("<td colspan='3' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 3px;'>Plan</td>");
				msgBuilder.append("<td colspan='5' style='color: #474343;font-family: Arial, verdena, sans-serif;font-size: 13px;text-align: center;line-height: 25px;font-weight: bold;background-color: #d7e8f1;border-right:#f5efe7 solid 0px;'>Actual</td>");
				msgBuilder.append("</tr>");
				msgBuilder.append("<tr style='background-color:#2d4a59;color: #fff;font-family: Arial, verdena, sans-serif;font-size: 12px;text-align: left;line-height: 25px;padding: 0px 0px 0px 3px;'>");
				msgBuilder.append("<td align='center' width='40px'>S.no</td>");
				msgBuilder.append("<td width='90px'>Project Type</td>");
				msgBuilder.append("<td width='70px'>Project</td>");
				msgBuilder.append("<td width='70px'>Manager</td>");
				msgBuilder.append("<td width='160px'>Description</td>");
				msgBuilder.append("<td width='70px'>Start Time</td>");
				msgBuilder.append("<td width='70px'>End Time</td>");
				msgBuilder.append("<td width='160px'>Description</td>");
				msgBuilder.append("<td width='70px'>Start Time</td>");
				msgBuilder.append("<td width='70px'>End Time</td>");
				msgBuilder.append("<td width='50px'>Activity</td>");
				msgBuilder.append("<td width='80px'>Work Item</td>");
				msgBuilder.append("</tr>");
				
				empHeaderAdded = true;
			}
			msgBuilder.append(isOddRow?oddRow:evenRow);
			isOddRow = !isOddRow;
			
			msgBuilder.append("<td "+projectStyle+" align='center'>"+rowNo+"</td>");
			msgBuilder.append("<td "+projectStyle+">"+fillBlank(taskActionVo.getProjectTypeActionVo().getProjectType())+"</td>");
			msgBuilder.append("<td "+projectStyle+">"+fillBlank(taskActionVo.getProjectName())+"</td>");
			msgBuilder.append("<td "+projectStyle+">"+fillBlank(taskActionVo.getPmName())+"</td>");
			msgBuilder.append("<td "+planStyle+">"+fillBlank(taskActionVo.getPlanTaskDesc())+"</td>");
			msgBuilder.append("<td "+planStyle+">"+fillBlank(taskActionVo.getPlanStartTime())+"</td>");
			msgBuilder.append("<td "+planStyle+">"+fillBlank(taskActionVo.getPlanEndTime())+"</td>");
			msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActualTaskDesc())+"</td>");
			msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActualStartTime())+"</td>");
			msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActualEndTime())+"</td>");
			msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getActivity())+"</td>");
			msgBuilder.append("<td "+actualStyle+">"+fillBlank(taskActionVo.getWorkItem())+"</td>");
			msgBuilder.append("</tr>");
			
			//Closing employee table
			if(rowNo==empNoOfTask){
				msgBuilder.append("</tbody>");
				msgBuilder.append("</table>");
				msgBuilder.append("<br>");
				
				msgBuilder.append("<div style='font-family: Arial, verdena, sans-serif;font-size: 14px;'>Note: This is auto generated mail, please do not reply.</div>");
				msgBuilder.append("</body>");
				msgBuilder.append("</html>");
			}
			
			rowNo++;
		}
		mailVo.setMailTo(mailToAddress.toArray(new String[mailToAddress.size()]));
		mailVo.setMessage(msgBuilder.toString());
		return mailVo;
	}
	
	public static String fillBlank(String str){
		if(str == null || "".equals(str)){
			return "&nbsp;";
		}
		else{
			return str;
		}
	}
}
