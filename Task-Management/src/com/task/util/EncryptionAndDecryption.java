/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;



/**
 * <p>
 * Title: EncryptionAndDecryption
 * </p>
 * <p>Description: This class used to encrypt and decrypt the password supplied.
 * </p>
 * 
 */
public class EncryptionAndDecryption {

	final static int RADIX = 32;
	
	/* Plain text buffer */
	final static int PADSIZE = 40; 
	
	private static SecureRandom sr = null;
	
	private static MessageDigest md = null;

	/**
	 * Constructor: To get the instance of SecureRandom and MessageDigest.
	 */
	public EncryptionAndDecryption() {
		if (sr == null)
			sr = new SecureRandom();
		try {
			md = MessageDigest.getInstance("SHA");
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Yow! NoSuchAlgorithmException."
					+ " Abandon all hope");
		}
	}

	/**
	 * Method name : To encrypt the password.
	 * 
	 * @param strEnc
	 *            Password
	 * @return encrypted password.
	 */
	public String doEncryption(String strEnc) {
		byte[] plain_left, plain_right;
		byte[] ciph_left, ciph_right;
		byte[] digest;

		/* Pad the string */
		byte input[] = convertStringToBytes(strEnc); 
		plain_left = new byte[PADSIZE / 2];
		plain_right = new byte[PADSIZE / 2];

		ciph_left = new byte[PADSIZE / 2];
		ciph_right = new byte[PADSIZE / 2];
		
		/* Temp storage for the hash */
		digest = new byte[PADSIZE / 2]; 
		int cursor = 0;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		out.write(42);

		while (cursor < input.length) {

			for (int i = 0; i < PADSIZE / 2; i++) {
				plain_left[i] = input[cursor + i];
				plain_right[i] = input[cursor + PADSIZE / 2 + i];
			}
			
			/* Start the hash fresh */
			md.reset(); 
			md.update(plain_left);
			
			/* Get out the digest bits */
			digest = md.digest();
			for (int i = 0; i < PADSIZE / 2; i++) {
				ciph_right[i] = (byte) (digest[i] ^ plain_right[i]);
			}
			md.reset();
			md.update(ciph_right);
			digest = md.digest();
			for (int i = 0; i < PADSIZE / 2; i++) {
				ciph_left[i] = (byte) (digest[i] ^ plain_left[i]);
			}
			out.write(ciph_left, 0, PADSIZE / 2);
			out.write(ciph_right, 0, PADSIZE / 2);
			cursor += PADSIZE;
		}

		BigInteger bi_out = new BigInteger(out.toByteArray());
		return (bi_out.toString(RADIX));

	}

	/**
	 * Method name : doDec
	 * 
	 * To decrypt the password.
	 * 
	 * @param strDec
	 *            Encrypted Password
	 * @return decrypted password.
	 */
	public String doDecryption(String strDec) {
		BigInteger bi;
		byte input[];
		byte[] plain_left, plain_right;
		byte[] ciph_left, ciph_right;
		byte[] digest;
		bi = new BigInteger(strDec, RADIX);

		/*
		 * Convert to a BigInteger, extract the bytes
		 */
		input = bi.toByteArray();
		ByteArrayOutputStream scratch = new ByteArrayOutputStream();
		scratch.write(input, 1, input.length - 1);
		
		input = scratch.toByteArray();
		
		plain_left = new byte[PADSIZE / 2];
		plain_right = new byte[PADSIZE / 2];
		ciph_left = new byte[PADSIZE / 2];
		ciph_right = new byte[PADSIZE / 2];
		
		/* Temp storage for the hash */
		digest = new byte[PADSIZE / 2]; 
		
		/* Our pointer into the workspace */
		int cursor = 0;
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		while (cursor < input.length) {
			
			/* Copy the next slab into the left and right */
			for (int i = 0; i < PADSIZE / 2; i++) {
				ciph_left[i] = input[cursor + i];
				ciph_right[i] = input[cursor + PADSIZE / 2 + i];
			}

			/* Start the hash fresh */
			md.reset();
			md.update(ciph_right);
			
			/* Get out the digest bits */
			digest = md.digest();
			for (int i = 0; i < PADSIZE / 2; i++) {
				plain_left[i] = (byte) (digest[i] ^ ciph_left[i]);
			}

			md.reset();
			md.update(plain_left);
			digest = md.digest();
			for (int i = 0; i < PADSIZE / 2; i++) {
				plain_right[i] = (byte) (digest[i] ^ ciph_right[i]);
			}

			out.write(plain_left, 0, PADSIZE / 2);
			out.write(plain_right, 0, PADSIZE / 2);
			cursor += PADSIZE;
		}
		return (doStripPadding(out.toByteArray()));
	}

	/**
	 * Method name StringToBytes Converts String value to Bytes.
	 * 
	 * @param input
	 *            String value to be converted to bytes. *
	 * @return byte The Method converts string to byte
	 * 
	 */
	private byte[] convertStringToBytes(String input) {
		
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		byte scratch[];
		scratch = input.getBytes();
		int len = input.length();
		buffer.write(scratch, 0, len);
		buffer.write(0);
		int padlen = PADSIZE - ((len + 1) % PADSIZE);
		scratch = new byte[padlen];
		sr.nextBytes(scratch);
		buffer.write(scratch, 0, padlen);
		
		return (buffer.toByteArray());
	}

	/**
	 * method name StripPadding
	 * 
	 * @param input
	 *            array
	 * @return string The Method used for padding
	 * 
	 */
	private String doStripPadding(byte input[]) {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int i = 0;

		while (input[i] != 0 && i < input.length) {
			buffer.write(input[i]);
			i++;
		}

		return (new String(buffer.toByteArray()));
	}
	

}
