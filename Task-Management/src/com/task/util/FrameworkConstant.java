/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class FrameworkConstant {
	/** default locale for properties file */
	private static Locale locale = Locale.US;

	/**
	 * @Method : getMessage
	 * @Description : get message based on key and locale
	 * 
	 * @param key
	 * @param locale
	 * @return String
	 */
	public static String getMessage(String key, Locale locale) {
		String message = "";
		ResourceBundle bundle = ResourceBundle.getBundle("message", locale);
		message = bundle.getString(key);
		return message;
	}

	/**
	 * @Method : getMessage
	 * @Description : get formatted message based on key, locale and parameters
	 * 
	 * @param key
	 * @param locale
	 * @param params
	 * @return String
	 */
	public static String getMessage(String key, Locale locale, Object... params) {
		String message = "";
		ResourceBundle bundle = ResourceBundle.getBundle("message", locale);
		message = bundle.getString(key);
		if (params.length == 0) {
			return message;
		} 
			return MessageFormat.format(message, params);
		
	}

	/**
	 * @Method : getLogMessage
	 * @Description : get formatted log message based on key, locale and
	 *              parameters
	 * 
	 * @param key
	 * @param locale
	 * @param params
	 * @return String
	 */
	public static String getLogMessage(String key, Locale locale,
			Object... params) {
		String message = "";
		ResourceBundle bundle = ResourceBundle.getBundle("logmessage", locale);
		message = bundle.getString(key);
		if (params.length == 0) {
			return message;
		}
			return MessageFormat.format(message, params);
		
	}
	
	/**
	 * @Method : getLogMessage
	 * @Description : get log message based on key,
	 * 
	 * @param key
	 * @return String
	 */
	public static String getLogMessage(String key) {
		String message = "";
		ResourceBundle bundle = ResourceBundle.getBundle("logmessage", locale);
		message = bundle.getString(key);
			return message;
	}

	/**
	 * @Method : getLogErrorMessage
	 * @Description : get formatted log error message based on key, locale and
	 *              parameters
	 * 
	 * @param key
	 * @param locale
	 * @param errorMsg
	 * @param params
	 * @return String
	 */
	public static String getLogErrorMessage(String key, Locale locale,
			String errorMsg, Object... params) {
		String message = "";
		ResourceBundle bundle = ResourceBundle.getBundle("logmessage", locale);
		message = bundle.getString(key);
		if (params.length == 0) {
			return message + errorMsg;
		}
			return MessageFormat.format(message, params) + errorMsg;
		
	}

	/**
	 * @Method : getInfo
	 * @Description : get Info message based on key and locale
	 * 
	 * @param key
	 * @param locale
	 * @return String
	 */
	public static String getInfo(String key, Locale locale) {
		String message = "";
		ResourceBundle bundle = ResourceBundle.getBundle("infomessage", locale);
		message = bundle.getString(key);
		return message;
	}

	/**
	 * 
	 * @Method : getInfo
	 * @Description : get formatted info message based on key, locale and
	 *              parameters
	 * 
	 * @param key
	 * @param locale
	 * @param params
	 * @return String
	 */
	public static String getInfo(String key, Locale locale, Object... params) {
		String message = "";
		ResourceBundle bundle = ResourceBundle.getBundle("infomessage", locale);
		message = bundle.getString(key);
		if (params.length == 0) {
			return message;
		} 
			return MessageFormat.format(message, params);
		
	}

	/**
	 * @Method : getError
	 * @Description : get error message based on key, locale
	 * 
	 * @param key
	 * @param locale
	 * @return String
	 */
	public static String getError(String key, Locale locale) {
		String message = "";
		ResourceBundle bundle = ResourceBundle
				.getBundle("errormessage", locale);
		message = bundle.getString(key);
		return message;
	}

	/**
	 * @Method : getError
	 * @Description : get formatted error message based on key, locale and
	 *              parameters
	 * 
	 * @param key
	 * @param locale
	 * @param params
	 * @return String
	 */
	public static String getError(String key, Locale locale, Object... params) {
		String message = "";
		ResourceBundle bundle = ResourceBundle
				.getBundle("errormessage", locale);
		message = bundle.getString(key);
		if (params.length == 0) {
			return message;
		} 
			return MessageFormat.format(message, params);
		
	}

	/**
	 * @Method : getError
	 * @Description : get error message based on key, locale
	 * 
	 * @param key
	 * @return String
	 */
	public static String getError(String key) {
		String message = "";
		ResourceBundle bundle = ResourceBundle
				.getBundle("errormessage", locale);
		message = bundle.getString(key);
		return message;
	}

	/**
	 * @Method : getBeanId
	 * @Description : get BeanId based on key
	 * 
	 * @param key
	 * @return String
	 */
	public static String getBeanId(String key) {
		String message = "";
		ResourceBundle bundle = ResourceBundle.getBundle("beanid", locale);
		message = bundle.getString(key);
		return message;
	}

	/**
	 * @Method : getDefaultParameter
	 * @Description : get default values for parameter from properties file
	 * @return Map<String, String>
	 */
	public static Map<String, String> getDefaultParameter() {
		ResourceBundle bundle = ResourceBundle.getBundle("parameterdefault");
		Map<String, String> map = convertResourceBundleToMap(bundle);
		return map;
	}

	/**
	 * @Method : convertResourceBundleToMap
	 * @Description : convert resource file to map
	 * @param bundle
	 * @return Map<String, String>
	 */
	private static Map<String, String> convertResourceBundleToMap(
			ResourceBundle bundle) {
		Map<String, String> map = new HashMap<String, String>();
		Enumeration<String> keys = bundle.getKeys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			map.put(key, bundle.getString(key));
		}
		return map;
	}

}
