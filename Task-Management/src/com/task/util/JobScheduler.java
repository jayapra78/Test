/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import org.quartz.CronExpression;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

public class JobScheduler {
	@SuppressWarnings("rawtypes")
	Class classHandler = null;

	public void task(String className, String time, Scheduler scheduler)
			throws SchedulerException {
		try {
			classHandler = Class.forName(className);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		JobDetail jobDetail = new JobDetail("JobName", "JobGroup", classHandler);
		CronTrigger cronTrigger = new CronTrigger("TriggerName", "TriggerGroup");

		try {

			CronExpression cexp = new CronExpression(time);
			cronTrigger.setCronExpression(cexp);
		} catch (Exception e) {
			e.printStackTrace();
		}

		scheduler.scheduleJob(jobDetail, cronTrigger);

		// start the scheduler
		scheduler.start();
	}

}
