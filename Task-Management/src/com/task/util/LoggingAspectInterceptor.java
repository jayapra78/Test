/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.util.StopWatch;

import com.task.common.RequestVo;

/**
 * The LoggingAspectInterceptor class used to create log using spring AOP
 *  
 * @author  Neethimohan A 
 */
@Aspect
public class LoggingAspectInterceptor {

	private static Logger log;

	/**
	 * The method to get logger
	 * 
	 * @param classname Class
	 * 
	 * @return Logger
	 */ 
	@SuppressWarnings("rawtypes")
	public static Logger getLog(Class classname) {
		Logger log1 = Logger.getLogger(classname);
		if (FrameworkConstant.getLogMessage("LOG_LEVEL").equals("INFO")) {
			log1.setLevel(Level.INFO);
		}

		if (FrameworkConstant.getLogMessage("LOG_LEVEL").equals("DEBUG")) {
			log1.setLevel(Level.DEBUG);
		}
		
		if (FrameworkConstant.getLogMessage("LOG_LEVEL").equals("WARN")) {
			log1.setLevel(Level.WARN);
		}
		
		if (FrameworkConstant.getLogMessage("LOG_LEVEL").equals("ERROR")) {
			log1.setLevel(Level.ERROR);
		}
		
		if (FrameworkConstant.getLogMessage("LOG_LEVEL").equals("FATAL")) {
			log1.setLevel(Level.FATAL);
		}
		return log1;
	}

	/**
	 * The method to set the Log
	 * 
	 * @param log Logger
	 */
	public static void setLog(Logger log) {
		LoggingAspectInterceptor.log = log;
	}
	
	/**
	 * The method to log before the method start
	 * 
	 * @param joinPoint JoinPoint
	 */
	@Before("execution(* com.shirai.*.*.*.*(..))")
	public void logBefore(JoinPoint joinPoint) {
		log = getLog(joinPoint.getTarget().getClass());
		MDC.put("userName", "" );
		if(null != joinPoint.getArgs() && joinPoint.getArgs().length>0){
    		for(int i= 0; i<joinPoint.getArgs().length; i++){
    			if(joinPoint.getArgs()[i] instanceof RequestVo){
    			if(null != ((RequestVo)joinPoint.getArgs()[i]).getUserVo() ){
    				if(((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName()==null){
    					MDC.put("userName", "" );
    				}else{
    					MDC.put("userName", ((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName() );
    				}
    			
    			//log.debug( "User Name : "+((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName() );
    			}}
    		}}
		log.debug("Class Start : "
				+ joinPoint.getTarget().getClass().getSimpleName());
		log.debug("Method Start : " + joinPoint.getSignature().getName());
	}
	
	/**
	 * The method to log Around the method
	 * 
	 * @param joinPoint ProceedingJoinPoint
	 * 
	 * @return Object
	 * 
	 * @throws Throwable
	 */
	@Around("execution(*  com.shirai.*.*.*.*(..))")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
		MDC.put("userName", "" );
		String name = joinPoint.getTarget().getClass().getName() + "."
				+ joinPoint.getSignature().getName();
		StopWatch stopWatch = new StopWatch();
		if (log.isDebugEnabled()) {
			stopWatch.start(name);
		}
		Object obj = joinPoint.proceed();
		if(null != joinPoint.getArgs() && joinPoint.getArgs().length>0){
    		for(int i= 0; i<joinPoint.getArgs().length; i++){
    			if(joinPoint.getArgs()[i] instanceof RequestVo){
    			if(null != ((RequestVo)joinPoint.getArgs()[i]).getUserVo() ){
    				if(((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName()==null){
    					MDC.put("userName", "" );
    				}else{
    					MDC.put("userName", ((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName() );
    				}
    			
    			//log.debug( "User Name : "+((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName() );
    			}}
    		}}
		
		if (log.isDebugEnabled()) {
			stopWatch.stop();
			StringBuilder msg = new StringBuilder().append(" Elapsed time : ")
					.append(stopWatch.getTotalTimeMillis());
			log.debug(msg);

		}
		return obj;
	}
	
	/**
	 * The method to log After the method
	 * 
	 * @param joinPoint JoinPoint
	 */
	@After("execution(* com.shirai.*.*.*.*(..))")
	public void logAfter(JoinPoint joinPoint) {
		MDC.put("userName", "" );
		if(null != joinPoint.getArgs() && joinPoint.getArgs().length>0){
    		for(int i= 0; i<joinPoint.getArgs().length; i++){
    			if(joinPoint.getArgs()[i] instanceof RequestVo){
    			if(null != ((RequestVo)joinPoint.getArgs()[i]).getUserVo() ){
    				if(((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName()==null){
    					MDC.put("userName", "" );
    				}else{
    					MDC.put("userName", ((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName() );
    				}
    			
    			//log.debug( "User Name : "+((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName() );
    			}}
    		}}
		log.debug("Method End : " + joinPoint.getSignature().getName());
	}
	
	/**
	 * The method to log After returning from the method
	 * 
	 * @param joinPoint JoinPoint
	 * 
	 * @param result Object
	 */
	@AfterReturning(pointcut = "execution(* com.shirai.*.*.*.*(..))", returning = "result")
	public void logAfterReturning(JoinPoint joinPoint, Object result) {
		// log.debug("Method returned value is : " + result);
		MDC.put("userName", "" );
		if(null != joinPoint.getArgs() && joinPoint.getArgs().length>0){
    		for(int i= 0; i<joinPoint.getArgs().length; i++){
    			if(joinPoint.getArgs()[i] instanceof RequestVo){
    			if(null != ((RequestVo)joinPoint.getArgs()[i]).getUserVo() ){
    				if(((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName()==null){
    					MDC.put("userName", "" );
    				}else{
    					MDC.put("userName", ((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName() );
    				}
    			
    			//log.debug( "User Name : "+((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName() );
    			}}
    		}}
		log.debug("Class End : "
				+ joinPoint.getTarget().getClass().getSimpleName());

	}
	
	/**
	 * The method log After throwing the Error
	 * 
	 * @param joinPoint JoinPoint
	 * 
	 * @param error Throwable
	 */
	@AfterThrowing(pointcut = "execution(* com.shirai.*.*.*.*(..))", throwing = "error")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
		MDC.put("userName", "" );
    	if(null != joinPoint.getArgs() && joinPoint.getArgs().length>0){
    		for(int i= 0; i<joinPoint.getArgs().length; i++){
    			if(joinPoint.getArgs()[i] instanceof RequestVo){
    			if(null != ((RequestVo)joinPoint.getArgs()[i]).getUserVo() ){
    				if(((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName()==null){
    					MDC.put("userName", "" );
    				}else{
    					MDC.put("userName", ((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName() );
    				}
    			
    			//log.debug( "User Name : "+((RequestVo)joinPoint.getArgs()[i]).getUserVo().getUserName() );
    			}}
    		}}
    	
		log.error("Exception in the method : "
				+ joinPoint.getSignature().getName());
		log.debug("---------------------------------------------------------------");
		if(error instanceof CustomApplicationException){
			log.error("Exception : " + ((CustomApplicationException)error).getLogMessage());
		}else{
			log.error("Exception : " + getUtilStackTrace(error));
		}
		log.debug("---------------------------------------------------------------");
	}
	
	/**
	 * @Method : getUtilStackTrace
	 * 
	 * @Description : To Get util Stack Trace 
	 * 
	 * @param t Throwable
	 * 
	 * @return String
	 */
	private String getUtilStackTrace(Throwable t) {
    	StringWriter sw = new StringWriter();
    	PrintWriter pw = new PrintWriter(sw);
    	t.printStackTrace(pw);
    	pw.flush();
    	return sw.toString();
   }	 

}