/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

// Read Mail configuration property file
public class MailConfigPropertyReader {
    private static Properties mailConfigProperties = null;
    private static String mailConfigPropertiesPath = null;
    
    // set MailConfigPropertyFilePath
    public static void setMailConfigPropertyFilePath(String mailConfigPropertiesPathIn) {
    	mailConfigPropertiesPath = mailConfigPropertiesPathIn;
    }
    //load property file
	private static void loadMailConfigPropertyFile() throws FileNotFoundException, IOException {
		mailConfigProperties = new Properties();
		if(null != mailConfigPropertiesPath) {
			mailConfigProperties.load(new FileInputStream(mailConfigPropertiesPath));
		} else {
			InputStream in = MailConfigPropertyReader.class.getClassLoader().getResourceAsStream("Mail_Config.properties");
			if (in == null) {
				throw new FileNotFoundException(
						"Mail_Config.properties not found");
			}
			mailConfigProperties.load(in);
		}
	}
	
	// Read optional field property
	public static String getOptionalPropertyValue(String propertyKey) throws FileNotFoundException, IOException {
		if(null == mailConfigProperties) {
			loadMailConfigPropertyFile();
		}
		String propertyValue = null;
		propertyValue = mailConfigProperties.getProperty(propertyKey);
		if(null == propertyValue || propertyValue.trim().isEmpty()) {
			return null;
		}
		return propertyValue.trim();
	}

	// Read Mandatory field property
	public static String getMandatoryPropertyValue(String propertyKey) throws FileNotFoundException, IOException {
		if(null == mailConfigProperties) {
			loadMailConfigPropertyFile();
		}
		String propertyValue = null;
		propertyValue = mailConfigProperties.getProperty(propertyKey);
		if(null == propertyValue || propertyValue.trim().isEmpty()) {
			throw new NullPointerException(propertyKey + " should not be Empty in config file:"+ mailConfigPropertiesPath);
		}
		return propertyValue.trim();
	}
}
