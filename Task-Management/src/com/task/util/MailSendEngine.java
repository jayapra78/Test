/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.sun.mail.util.MailSSLSocketFactory;
import com.task.common.MailVo;

public class MailSendEngine {
	/** log instance for logging */
	private static Logger log = LoggingAspectInterceptor
			.getLog(MailSendEngine.class);
	private StringBuilder logMessage = new StringBuilder("");
	
	
	/**
	 * 
	 * @Method : 
	 * @Description : Task Mail
	 * @param mailVo
	 * @return
	 * @throws Exception
	 */
	public boolean sendmail(MailVo mailVo) throws Exception{
		Properties objProps = new Properties();
		
		ResourceBundle bundle1 = ResourceBundle.getBundle("Mail_Config");
		
		final String userName=bundle1.getString("mail.user.name");
		final String password=bundle1.getString("mail.user.password");
		String fromAddress = bundle1.getString("mail.user.address");
		
		objProps.put("mail.smtp.host", bundle1.getString("mail.smtp.host"));
		objProps.put("mail.smtp.port", bundle1.getString("mail.smtp.port"));
		objProps.put("mail.smtp.auth", bundle1.getString("mail.smtp.auth"));
		objProps.put("mail.smtp.starttls.enable", bundle1.getString("mail.smtp.starttls.enable"));
		
		// Added for SSL Verification other than Google mail
		MailSSLSocketFactory sf = new MailSSLSocketFactory();
	    sf.setTrustAllHosts(true);
	    objProps.put("mail.smtp.ssl.socketFactory", sf);
		
		Session session = Session.getInstance(objProps,
				  new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(userName,password );
					}
				  });
		
		MimeMessage msg = new MimeMessage(session);
		
		try {
			InternetAddress address = new InternetAddress();
			try {
				address.setAddress(fromAddress);
				address.setPersonal(mailVo.getPersonal());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			msg.setFrom(address);
			
			InternetAddress[] addressTo = new InternetAddress[mailVo.getMailTo().length];        
			
			for (int i = 0; i < mailVo.getMailTo().length; i++) {                        
				addressTo[i] = new InternetAddress(mailVo.getMailTo()[i]);   
				logMessage.append("To : " + addressTo[i] + "\n");
			}                
			msg.setRecipients(Message.RecipientType.TO, addressTo);       
			
			InternetAddress[] addressCc = null;
			if(mailVo.getMailCc() !=null && mailVo.getMailCc().length > 0){
				addressCc = new InternetAddress[mailVo.getMailCc().length];                
				for (int i = 0; i < mailVo.getMailCc().length; i++) {                        
					addressCc[i] = new InternetAddress(mailVo.getMailCc()[i]);   
					logMessage.append("Cc : " + addressCc[i] + "\n");
				} 
			}
			if(addressCc != null){
				msg.setRecipients(Message.RecipientType.CC, addressCc);
			}
			
			msg.setSubject(mailVo.getSubject());
			
			// Attachment Start
			Multipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setContent(mailVo.getMessage(),"text/html; charset=UTF-8");
	        //logMessage.append("Message : " +  mailVo.getMessage() + "\n");
	        multipart.addBodyPart(messageBodyPart);
			if(mailVo.getAttachment() != null && mailVo.getAttachment().trim().length() > 0){
				addAtachments(new String[]{mailVo.getAttachment()}, multipart);
			}
			// Attachment End
			
			msg.setContent(multipart);
			msg.saveChanges();
			Transport.send(msg);
			log.info(logMessage.toString());
		} catch (AddressException addressExcep) {
			log.error(addressExcep+" :: "+logMessage);
            throw addressExcep;
		} catch (Exception exception) {
			log.error(exception+" :: "+logMessage);
			throw exception;
		}
		return false;
	}
	/**
	 * @Method : 
	 * @Description : 
	 * @param attachments
	 * @param multipart
	 * @throws MessagingException
	 * @throws AddressException
	 */
	protected void addAtachments(String[] attachments, Multipart multipart)
	
	            throws MessagingException, AddressException {
	
	        for (int i = 0; i <= attachments.length - 1; i++) {
	
	            String filename = attachments[i];
	
	            MimeBodyPart attachmentBodyPart = new MimeBodyPart();
	
	            //use a JAF FileDataSource as it does MIME type detection
	
	            DataSource source = new FileDataSource(filename);
	
	            attachmentBodyPart.setDataHandler(new DataHandler(source));
	
	            attachmentBodyPart.setFileName(filename.substring(filename.lastIndexOf(File.separator)+1, filename.length()));

	            //add the attachment
	
	            multipart.addBodyPart(attachmentBodyPart);
	
	        }
	    }
}


