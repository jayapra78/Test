/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import com.task.common.MailVo;

/**
 * @author neethimohana
 *
 */
public class MailSendResponse  implements MailbackInterface {
	Logger log = Logger.getLogger(MailSendResponse.class);
	Object result;

	public  MailSendResponse() {
		
	}
	
	/**
     * @Method : returnResult
     * 
     * @Description : The method to call the send Mail method
     *  
     * @param valueObject
     */
    public void returnResult(Object result) {
    	log.debug("Method Start : "+ "returnResult");
    	if(null != result && result instanceof MailVo){
    		MailVo mailVo = (MailVo) result;
    		if(null != mailVo ){
    			sendMail(mailVo);
    		}
    	}
      this.result = result;
      
      log.debug("Method End : "+ "returnResult");
    }
    
    /**
     * @Method : andAction
     * 
     * @Description : The method to Start the call mail send method Asynchronously
     * 
     * @param valueObject
     */
    @SuppressWarnings({ "unchecked", "unused", "rawtypes" })
   	public void andAction(Object valueObject) {
    	log.debug("Method Start : "+ "andAction");
    	log.debug("Mail send Asynchronous thread started");
           ExecutorService es = Executors.newFixedThreadPool(10);
           CommonAsynMailThread worker = new CommonAsynMailThread();
           worker.setMailbackInterface(this);
           worker.setValueObject(valueObject);
           final Future future = es.submit(worker);
           es.shutdown();
           log.debug("Method End : "+ "andAction");
       }
    
    /**
	 * @Method : sendMail
	 * 
	 * @Description : The method to send mail 
	 * 
	 * @return boolean
     * @throws Exception 
	 */
	private boolean sendMail( MailVo mailVo){
		log.debug("Method Start : "+ "sendMail");
		MailSendEngine mailSendEngine = new MailSendEngine();
		try {
			mailSendEngine.sendmail(mailVo);
		} catch (CustomApplicationException cae) {
			log.error("Mail not send to "+mailVo.getMailTo()[0].toString()+ "Error occured : " +cae);
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("Mail send successfully to "+mailVo.getMailTo()[0].toString());
		log.debug("Method END : "+ "sendMail");
		return true;
		
	}
}
