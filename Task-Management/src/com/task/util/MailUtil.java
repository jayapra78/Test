/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;
import javax.mail.search.AndTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.SearchTerm;
import javax.mail.search.SubjectTerm;

import org.apache.log4j.Logger;

import com.sun.mail.pop3.POP3SSLStore;

public class MailUtil {
	/** log instance for logging */
	private static Logger log = LoggingAspectInterceptor.getLog(MailUtil.class);

	private Session session = null;
	private Store store = null;
	private Folder folder;
	private String SSL_FACTORY_DEFAULT = "javax.net.ssl.SSLSocketFactory";
	private String filePath;
	private Message[] messages;

	public MailUtil() {
	}
	
	// To open given folder
	public void openFolder() throws Exception {
		String folderName = MailConfigPropertyReader
				.getOptionalPropertyValue("mail.folder.to.read");
		
		// Open the Folder
		folder = store.getDefaultFolder();
		if(folderName != null && folderName.trim().length() > 0){
			folder = folder.getFolder(folderName);
		}

		if (folder == null) {
			throw new Exception("Invalid folder");
		}

		// try to open read/write and if that fails try read-only
		try {

			folder.open(Folder.READ_WRITE);

		} catch (MessagingException ex) {

			folder.open(Folder.READ_ONLY);

		}
	}
	
	// Delete messages 
//	public void deleteMessages(Message[] messages) throws MessagingException{
//	     folder.setFlags(messages, new Flags(Flags.Flag.DELETED), true);
//	}
	
	public void deleteMessages(int msgIndex) throws MessagingException{
		messages[msgIndex].setFlag(Flags.Flag.DELETED, true);
//	     folder.setFlags(messages, new Flags(Flags.Flag.DELETED), true);
	}
	
	public void seenMessages(int msgIndex) throws MessagingException{
		messages[msgIndex].setFlag(Flags.Flag.SEEN, true);
//	     folder.setFlags(messages, new Flags(Flags.Flag.DELETED), true);
	}
	
	// To close folder
	public void closeFolder() throws Exception {
		folder.close(true);
	}
	
	// Get all message count
	public int getMessageCount() throws Exception {
		return folder.getMessageCount();
	}
	// Get new message count
	public int getNewMessageCount() throws Exception {
		SearchTerm term = null;
		String subject = MailConfigPropertyReader
				.getOptionalPropertyValue("mail.subject");
		if (subject != null && subject.trim().length() > 0) {
			term = new SubjectTerm(subject);
			FlagTerm ft = new FlagTerm(new Flags(
					Flags.Flag.DELETED), false);
			term = new AndTerm(term, ft);
			messages = folder.search(term);
			if (messages != null) {
				return messages.length;
			}
		}
		 return 0;
	}
	
	// Get new messages
		public Message[] getNewMessages() throws Exception {
			SearchTerm term = null;
			String subject = MailConfigPropertyReader
					.getOptionalPropertyValue("mail.subject");
			if (subject != null && subject.trim().length() > 0) {
				term = new SubjectTerm(subject);
				FlagTerm deleteTerm = new FlagTerm(new Flags(
						Flags.Flag.DELETED), false);
				FlagTerm seenTerm = new FlagTerm(new Flags(
						Flags.Flag.SEEN), false);
				term = new AndTerm(term, deleteTerm);
				term = new AndTerm(term, seenTerm);
				messages = folder.search(term);
				return messages;
			}
			 return null;
		}
		
	// To disconnect store
	public void disconnect() throws Exception {
		store.close();
	}

	// To Read message using message number
	public void printMessage(int messageNo) throws Exception {
		log.info("Getting message number: " + messageNo);

		Message m = null;

		try {
			m = folder.getMessage(messageNo);
			dumpPart(m);
		} catch (IndexOutOfBoundsException iex) {
			log.info("Message number out of range");
		}
	}
	// To Read all messages Envelopes from given folder
	public void printAllMessageEnvelopes() throws Exception {
		Message[] messages = folder.getMessages();

		// Use a suitable FetchProfile
		FetchProfile fp = new FetchProfile();
		fp.add(FetchProfile.Item.ENVELOPE);
		folder.fetch(messages, fp);

		for (int i = 0; i < messages.length; i++) {
			log.info("--------------------------");
			log.info("MESSAGE #" + (i + 1) + ":");
			dumpEnvelope(messages[i]);

		}

	}
	// To Read New messages from given folder
	public void printNewMessages(int startMsgIndex, int endMsgIndex)
			throws Exception {

		Message[] messages = folder.getMessages(startMsgIndex, endMsgIndex);

		for (int i = 0; i < messages.length; i++) {
			Message message = messages[i];
			if (!message.isSet(Flags.Flag.SEEN)
					&& !message.isSet(Flags.Flag.ANSWERED)) {
				log.info("--------------------------");
				log.info("MESSAGE #" + (i + 1) + ":");
				dumpPart(messages[i]);
			}
		}

	}
	// To Read all messages from given folder
	public void printAllMessages() throws Exception {

		// Attributes & Flags for all messages ..
		Message[] messages = folder.getMessages();
		// Use a suitable FetchProfile
		FetchProfile fp = new FetchProfile();
		fp.add(FetchProfile.Item.ENVELOPE);
		folder.fetch(messages, fp);

		for (int i = 0; i < messages.length; i++) {

			log.info("--------------------------");
			log.info("MESSAGE #" + (i + 1) + ":");
			dumpPart(messages[i]);
		}

	}

	// Read message contend	
	public static void dumpPart(Part p) throws Exception {
		if (p instanceof Message)
			dumpEnvelope((Message) p);
		Object content = p.getContent();
		log.info(content);
		String ct = p.getContentType();
		try {
			pr("CONTENT-TYPE: " + (new ContentType(ct)).toString());
		} catch (ParseException pex) {
			pr("BAD CONTENT-TYPE: " + ct);
		}

		/*
		 * Using isMimeType to determine the content type avoids fetching the
		 * actual content data until we need it.
		 */
		if (p.isMimeType("text/plain")) {
			pr("This is plain text");
			pr("---------------------------");
			log.info((String) p.getContent());
		} else if (content instanceof javax.mail.Multipart) {
			pr("This is Multipart");
			// just a separator
			pr("---------------------------");
			handleMultipart((Multipart) content);
		} else {
			// just a separator
			pr("---------------------------");

		}
	}

	// Handle Multi part data
	public static void handleMultipart(Multipart multipart)
			throws MessagingException, IOException {
		for (int i = 0, n = multipart.getCount(); i < n; i++) {
			handlePart(multipart.getBodyPart(i));
		}
	}

	// handle Message part
	public static void handlePart(Part part) throws MessagingException,
			IOException {
		String disposition = part.getDisposition();
		String contentType = part.getContentType();
		if (disposition == null) { // When just body
			log.info("Null: " + contentType);
			// Check if plain
			if ((contentType.length() >= 10)
					&& (contentType.toLowerCase().substring(0, 10)
							.equals("text/plain"))) {
				part.writeTo(System.out);
			} else { // Don't think this will happen
				log.info("Other body: " + contentType);
				part.writeTo(System.out);
			}
		} else if (disposition.equalsIgnoreCase(Part.ATTACHMENT)) {
			log.info("Attachment: " + part.getFileName() + " : "
					+ contentType);
			saveFile(part.getFileName(), part.getInputStream());
		} else if (disposition.equalsIgnoreCase(Part.INLINE)) {
			log.info("Inline: " + part.getFileName() + " : "
					+ contentType);
			saveFile(part.getFileName(), part.getInputStream());
		} else { // Should never happen
			log.info("Other: " + disposition);
		}
	}
	
	// handle Message part
		public static void handlePartAttachment(Part part) throws MessagingException,
				IOException {
			String disposition = part.getDisposition();
			String contentType = part.getContentType();
			if (disposition == null) { // When just body
				log.info("Null: " + contentType);
				// Check if plain
				if ((contentType.length() >= 10)
						&& (contentType.toLowerCase().substring(0, 10)
								.equals("text/plain"))) {
					part.writeTo(System.out);
				} else { // Don't think this will happen
					log.info("Other body: " + contentType);
					part.writeTo(System.out);
				}
			} else if (disposition.equalsIgnoreCase(Part.ATTACHMENT)) {
				log.info("Attachment: " + part.getFileName() + " : "
						+ contentType);
				saveFile(part.getFileName(), part.getInputStream());
			} else if (disposition.equalsIgnoreCase(Part.INLINE)) {
				log.info("Inline: " + part.getFileName() + " : "
						+ contentType);
				saveFile(part.getFileName(), part.getInputStream());
			} else { // Should never happen
				log.info("Other: " + disposition);
			}
		}

	// Save attachment to downloads folder
	public static void saveFile(String filename, InputStream input)
			throws IOException {
		if (filename == null) {
			filename = File.createTempFile("downloads/", ".out").getName();
		} else {
			filename = "downloads/" + filename;
		}
		// Do not overwrite existing file
		File file = new File(filename);
		for (int i = 0; file.exists(); i++) {
			if (filename.lastIndexOf(".") != -1) {
				filename = filename.substring(0, filename.lastIndexOf("."))
						+ "_"
						+ i
						+ filename.substring(filename.lastIndexOf("."),
								filename.length());
			} else {
				filename = filename + i;
			}
			file = new File(filename);
		}
		FileOutputStream fos = new FileOutputStream(file);
		BufferedOutputStream bos = new BufferedOutputStream(fos);

		BufferedInputStream bis = new BufferedInputStream(input);
		int aByte;
		while ((aByte = bis.read()) != -1) {
			bos.write(aByte);
		}
		bos.flush();
		bos.close();
		bis.close();
	}
	
	// Save attachment to downloads folder
		public static void saveFile(String filePath, String filename, InputStream input)
				throws IOException {
			if (filename == null) {
				filename = File.createTempFile(filePath, ".out").getName();
			} else {
				filename = filePath + filename;
			}
			// Do not overwrite existing file
			File file = new File(filename);
			FileOutputStream fos = new FileOutputStream(file);
			BufferedOutputStream bos = new BufferedOutputStream(fos);

			BufferedInputStream bis = new BufferedInputStream(input);
			int aByte;
			while ((aByte = bis.read()) != -1) {
				bos.write(aByte);
			}
			bos.flush();
			bos.close();
			bis.close();
		}

	// Read Mail Envelope
	public static void dumpEnvelope(Message m) throws Exception {
		pr(" ");
		Address[] a;
		// FROM
		if ((a = m.getFrom()) != null) {
			for (int j = 0; j < a.length; j++)
				pr("FROM: " + a[j].toString());
		}

		// TO
		if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
			for (int j = 0; j < a.length; j++) {
				pr("TO: " + a[j].toString());
			}
		}

		// SUBJECT
		pr("SUBJECT: " + m.getSubject());

		// DATE
		Date d = m.getSentDate();
		pr("SendDate: " + (d != null ? d.toString() : "UNKNOWN"));

	}

	static String indentStr = "                                               ";
	static int level = 0;

	/**
	 * Print a, possibly indented, string.
	 */
	public static void pr(String s) {

		System.out.print(indentStr.substring(0, level * 2));
		log.info(s);
	}
	/**
	 * Getter method for filePath
	 * 
	 * @return the filePath (String)
	 */
	public String getFilePath() {
		return filePath;
	}
	/**
	 * Setter method for filePath
	 *
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	
	
}
