/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.task.common.CommonVo;
import com.task.common.SessionVo;
import com.task.common.UserVo;
import com.task.common.constant.AttributeConstant;

/**
 * @author balajir
 * 
 */
public class SessionHandler {

	public static HttpSession getSession(HttpServletRequest request) {
		if (request != null) {
			return request.getSession();
		}
		return null;

	}

	public static Locale getSessionLocale(HttpServletRequest request) {
		if (request != null
				&& request.getSession() != null
				&& request.getSession().getAttribute(
						AttributeConstant.DEFAULTLOCALESESSION) != null) {
			return (Locale) request.getSession().getAttribute(
					AttributeConstant.DEFAULTLOCALESESSION);
		}
		return null;
	}

	public static String getSecureAccessId(HttpServletRequest request) {
		if (request != null
				&& request.getSession() != null
				&& request.getSession().getAttribute(
						AttributeConstant.SECURE_ACCESS_ID) != null) {
			return (String) request.getSession().getAttribute(
					AttributeConstant.SECURE_ACCESS_ID);
		}
		return null;
	}

	public static SessionVo getSessionVo(HttpServletRequest request) {
		SessionVo sessionVo = null;
		if (getSecureAccessId(request) != null) {
			String secureId = getSecureAccessId(request);
			if (request.getSession().getAttribute(secureId) != null) {
				sessionVo = (SessionVo) request.getSession().getAttribute(
						secureId);
			}
		}
		return sessionVo;
	}

	public static String getUserName(HttpServletRequest request) {
		String userName = "";
		SessionVo sessionVo = getSessionVo(request);
		if (sessionVo != null) {
			userName = sessionVo.getUserVo().getUserName();
		}
		return userName;
	}

	public static String getAssociateCompany(HttpServletRequest request) {
		String associateCompany = "";
		SessionVo sessionVo = getSessionVo(request);
		if (sessionVo != null) {
			associateCompany = sessionVo.getUserVo().getAssocaiteCompany();
		}
		return associateCompany;
	}

	public static Long getCompanyId(HttpServletRequest request) {
		Long companyId = null;
		SessionVo sessionVo = getSessionVo(request);
		if (sessionVo != null) {
			companyId = sessionVo.getUserVo().getCompanyId();
		}
		return companyId;
	}

	public static Integer getUserType(HttpServletRequest request) {
		Integer userType = null;
		SessionVo sessionVo = getSessionVo(request);
		if (sessionVo != null) {
			userType = sessionVo.getUserVo().getUserType();
		}
		return userType;
	}

	public static UserVo getUserDetails(HttpServletRequest request) {
		UserVo userVo = null;
		SessionVo sessionVo = getSessionVo(request);
		if (sessionVo != null) {
			userVo = sessionVo.getUserVo();
		}
		return userVo;
	}
	public static void setSearchMap(HttpServletRequest request, Map<String, CommonVo> searchMap) {
		SessionVo sessionVo = getSessionVo(request);
		if (sessionVo != null) {
			sessionVo.setSearchMap(searchMap);
		}
	}

}
