/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.task.service.custom.TaskService;

public class TaskMailScheduler implements StatefulJob {
	/** log instance for logging */
	private static Logger log = LoggingAspectInterceptor
			.getLog(TaskMailScheduler.class);
	/**
	 * @Override to call job mail service
	 */
	public void execute(JobExecutionContext arg0)
			throws JobExecutionException {
		try{
		TaskService taskService = (TaskService) ContextLoader
				.getBean(FrameworkConstant.getBeanId("TASK_SERVICE"));
		taskService.getTaskListMail();
		taskService = null;
		}catch(Exception e){
			log.error(e.getMessage());
		}

	}

}
