/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.util;

import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;


/**
 * This Class used for Transaction control
 * 
 * @author Raj Narayan Saraf [SRM]
 */
public class TransactionService {
	private HibernateTransactionManager txManager;
	private TransactionStatus status;

	public TransactionService(int tansactionDefinition) {
		txManager = (HibernateTransactionManager) ContextLoader
				.getBean("txManager");
		TransactionDefinition def = new DefaultTransactionDefinition(tansactionDefinition);
		status = txManager.getTransaction(def);

	}

	public void commitTransaction() {
		if(txManager != null){
		txManager.commit(status);
		
		}
		
	}

	public void rollbackTransaction() {
		if(txManager != null){
			if(!status.isCompleted()){
//			if(txManager.isValidateExistingTransaction()){
					txManager.rollback(status);
//			}}
			}
		}
		
	}

	public PlatformTransactionManager getTxManager() {
		return txManager;
	}

	public void setTxManager(HibernateTransactionManager txManager) {
		this.txManager = txManager;
	}

	public TransactionStatus getStatus() {
		return status;
	}

	public void setStatus(TransactionStatus status) {
		this.status = status;
	}

}
