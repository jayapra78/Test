/**
 * Creater / neethimohanaSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import com.task.common.CommonVo;

/**
 * @author neethimohana
 *
 */
public class ActivityActionVo  extends CommonVo{

    private int id;
    private Integer majorId;
    private Integer minorId;

   public ActivityActionVo() {
   }

	
   public ActivityActionVo(int id) {
       this.id = id;
   }
   public ActivityActionVo(int id, Integer majorId, Integer minorId) {
      this.id = id;
      this.majorId = majorId;
      this.minorId = minorId;
   }
  
   public int getId() {
       return this.id;
   }
   
   public void setId(int id) {
       this.id = id;
   }
   
   public Integer getMajorId() {
       return this.majorId;
   }
   
   public void setMajorId(Integer majorId) {
       this.majorId = majorId;
   }
   
   public Integer getMinorId() {
       return this.minorId;
   }
   
   public void setMinorId(Integer minorId) {
       this.minorId = minorId;
   }
}
