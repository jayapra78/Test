/**
 * Creater / balamurugankSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import com.task.common.CommonVo;


/**
 * @author balamurugank
 *
 */
public class ApprovedLeaveResultVo extends CommonVo{
    
    /**
     * long serialVersionUID : 
     */
    private static final long serialVersionUID = 1L;
    private String empName;
    private String empCode;
    private String designation;
    private String deptName;
    private String leaveType;
    private String fromDate;
    private String toDate;
    private String noOfDays;
    private String rejoinDate;
    private String remarks;
    private String reason;
    private String address;
    private String contactNo;
    private String email;
    private String leaveStatus;
    private String pmCode;
    
    /**
     * Getter method for empName
     * 
     * @return the empName (String)
     */
    public String getEmpName() {
        return empName;
    }
    /**
     * Setter method for empName
     *
     * @param empName the empName to set
     */
    public void setEmpName(String empName) {
        this.empName = empName;
    }
    /**
     * Getter method for empCode
     * 
     * @return the empCode (String)
     */
    public String getEmpCode() {
        return empCode;
    }
    /**
     * Setter method for empCode
     *
     * @param empCode the empCode to set
     */
    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }
    /**
     * Getter method for designation
     * 
     * @return the designation (String)
     */
    public String getDesignation() {
        return designation;
    }
    /**
     * Setter method for designation
     *
     * @param designation the designation to set
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }
    /**
     * Getter method for deptName
     * 
     * @return the deptName (String)
     */
    public String getDeptName() {
        return deptName;
    }
    /**
     * Setter method for deptName
     *
     * @param deptName the deptName to set
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
    /**
     * Getter method for leaveType
     * 
     * @return the leaveType (String)
     */
    public String getLeaveType() {
        return leaveType;
    }
    /**
     * Setter method for leaveType
     *
     * @param leaveType the leaveType to set
     */
    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }
    /**
     * Getter method for fromDate
     * 
     * @return the fromDate (String)
     */
    public String getFromDate() {
        return fromDate;
    }
    /**
     * Setter method for fromDate
     *
     * @param fromDate the fromDate to set
     */
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }
    /**
     * Getter method for toDate
     * 
     * @return the toDate (String)
     */
    public String getToDate() {
        return toDate;
    }
    /**
     * Setter method for toDate
     *
     * @param toDate the toDate to set
     */
    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
    /**
     * Getter method for noOfDays
     * 
     * @return the noOfDays (String)
     */
    public String getNoOfDays() {
        return noOfDays;
    }
    /**
     * Setter method for noOfDays
     *
     * @param noOfDays the noOfDays to set
     */
    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }
    /**
     * Getter method for rejoinDate
     * 
     * @return the rejoinDate (String)
     */
    public String getRejoinDate() {
        return rejoinDate;
    }
    /**
     * Setter method for rejoinDate
     *
     * @param rejoinDate the rejoinDate to set
     */
    public void setRejoinDate(String rejoinDate) {
        this.rejoinDate = rejoinDate;
    }
    /**
     * Getter method for remarks
     * 
     * @return the remarks (String)
     */
    public String getRemarks() {
        return remarks;
    }
    /**
     * Setter method for remarks
     *
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    /**
     * Getter method for reason
     * 
     * @return the reason (String)
     */
    public String getReason() {
        return reason;
    }
    /**
     * Setter method for reason
     *
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }
    /**
     * Getter method for address
     * 
     * @return the address (String)
     */
    public String getAddress() {
        return address;
    }
    /**
     * Setter method for address
     *
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }
    /**
     * Getter method for contactNo
     * 
     * @return the contactNo (String)
     */
    public String getContactNo() {
        return contactNo;
    }
    /**
     * Setter method for contactNo
     *
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }
    /**
     * Getter method for email
     * 
     * @return the email (String)
     */
    public String getEmail() {
        return email;
    }
    /**
     * Setter method for email
     *
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * Getter method for leaveStatus
     * 
     * @return the leaveStatus (String)
     */
    public String getLeaveStatus() {
        return leaveStatus;
    }
    /**
     * Setter method for leaveStatus
     *
     * @param leaveStatus the leaveStatus to set
     */
    public void setLeaveStatus(String leaveStatus) {
        this.leaveStatus = leaveStatus;
    }
	public String getPmCode() {
		return pmCode;
	}
	public void setPmCode(String pmCode) {
		this.pmCode = pmCode;
	}

}
