/**
 * Creater / balamurugankSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import java.util.Date;

import com.task.common.CommonVo;

/**
 * @author balamurugank
 *
 */
public class AvailableResourceVo extends CommonVo{

    /**
     * long serialVersionUID : 
     */
    private static final long serialVersionUID = 1L;
    private Integer resourceAllocationId;
    private Integer employeeId;
    private String employeeCode;
    private String employeeName;
    private Integer role;
    private Integer mode;
    private Integer projectId;
    private String startDateStr;
    private String endDateStr;
    private Date startDate;
    private Date endDate;
    private boolean resourceReleased;
    private Date createdOn;
    private Integer createdBy;
    
    /**
     * Getter method for resourceAllocationId
     * 
     * @return the resourceAllocationId (Integer)
     */
    public Integer getResourceAllocationId() {
        return resourceAllocationId;
    }
    /**
     * Setter method for resourceAllocationId
     *
     * @param resourceAllocationId the resourceAllocationId to set
     */
    public void setResourceAllocationId(Integer resourceAllocationId) {
        this.resourceAllocationId = resourceAllocationId;
    }
    /**
     * Getter method for employeeId
     * 
     * @return the employeeId (Integer)
     */
    public Integer getEmployeeId() {
        return employeeId;
    }
    /**
     * Setter method for employeeId
     *
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }
    /**
     * Getter method for employeeCode
     * 
     * @return the employeeCode (String)
     */
    public String getEmployeeCode() {
        return employeeCode;
    }
    /**
     * Setter method for employeeCode
     *
     * @param employeeCode the employeeCode to set
     */
    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
    /**
     * Getter method for employeeName
     * 
     * @return the employeeName (String)
     */
    public String getEmployeeName() {
        return employeeName;
    }
    /**
     * Setter method for employeeName
     *
     * @param employeeName the employeeName to set
     */
    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }
    /**
     * Getter method for role
     * 
     * @return the role (Integer)
     */
    public Integer getRole() {
        return role;
    }
    /**
     * Setter method for role
     *
     * @param role the role to set
     */
    public void setRole(Integer role) {
        this.role = role;
    }
    /**
     * Getter method for mode
     * 
     * @return the mode (Integer)
     */
    public Integer getMode() {
        return mode;
    }
    /**
     * Setter method for mode
     *
     * @param mode the mode to set
     */
    public void setMode(Integer mode) {
        this.mode = mode;
    }
    /**
     * Getter method for projectId
     * 
     * @return the projectId (Integer)
     */
    public Integer getProjectId() {
        return projectId;
    }
    /**
     * Setter method for projectId
     *
     * @param projectId the projectId to set
     */
    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
    /**
     * Getter method for startDate
     * 
     * @return the startDate (Date)
     */
    public Date getStartDate() {
        return startDate;
    }
    /**
     * Setter method for startDate
     *
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    /**
     * Getter method for endDate
     * 
     * @return the endDate (Date)
     */
    public Date getEndDate() {
        return endDate;
    }
    /**
     * Setter method for endDate
     *
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    /**
     * Getter method for resourceReleased
     * 
     * @return the resourceReleased (boolean)
     */
    public boolean isResourceReleased() {
        return resourceReleased;
    }
    /**
     * Setter method for resourceReleased
     *
     * @param resourceReleased the resourceReleased to set
     */
    public void setResourceReleased(boolean resourceReleased) {
        this.resourceReleased = resourceReleased;
    }
	public String getStartDateStr() {
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	public String getEndDateStr() {
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
}
