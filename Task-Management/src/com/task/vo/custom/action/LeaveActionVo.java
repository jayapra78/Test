/**
 * Creater / durgeshkumarjhaSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import java.math.BigDecimal;
import java.util.Date;

import com.task.common.CommonVo;
import com.task.util.CommonUtils;
import com.task.vo.custom.entity.LeaveStatusEntityVo;
import com.task.vo.custom.entity.LeaveTypeEntityVo;

/**
 * @author durgeshkumarjha
 *
 */
public class LeaveActionVo extends CommonVo{
	
	/**
     * long serialVersionUID : 
     */
    private static final long serialVersionUID = 1L;
	
	private Integer empLeaveDetailId;
    private LeaveStatusEntityVo leaveStatus;
    private LeaveStatusActionVo leaveStatusActionVo;
    private LeaveTypeActionVo leaveTypeActionVo;
    private LeaveTypeEntityVo leaveType;
    private Integer employeeId;
    private String employeeName;
    private Date fromDate;
    private Date toDate;
    private String fromDateStr;
    private String toDateStr;
    private BigDecimal noOfDays;
    private Date rejoiningDate;
    private String rejoiningDateStr;
    private String reasonForLeave;
    private String contactNumber;
    private Integer alternateNumber;
    private String contactEmailId;
    private Integer sanctionedBy;
    private Date sanctionedDate;
    private Integer approvedBy;
    private Date approvedDate;
    private String supervisorComments;
    private String empAddress;
    private String empRemarks;
    private Boolean compoff;
    private BigDecimal comoffDays;
    private Date createdOn;
    private String createdBy;
    private String reportingManager;
    
	/**
	 * Getter method for empLeaveDetailId
	 * 
	 * @return the empLeaveDetailId (Integer)
	 */
	public Integer getEmpLeaveDetailId() {
		return empLeaveDetailId;
	}
	/**
	 * Setter method for empLeaveDetailId
	 *
	 * @param empLeaveDetailId the empLeaveDetailId to set
	 */
	public void setEmpLeaveDetailId(Integer empLeaveDetailId) {
		this.empLeaveDetailId = empLeaveDetailId;
	}
	
	/**
	 * Getter method for employeeId
	 * 
	 * @return the employeeId (Integer)
	 */
	public Integer getEmployeeId() {
		return employeeId;
	}
	/**
	 * Setter method for employeeId
	 *
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	/**
	 * Getter method for fromDate
	 * 
	 * @return the fromDate (Date)
	 */
	public Date getFromDate() {
		if(getFromDateStr()!= null && !getFromDateStr().equalsIgnoreCase(""))
			fromDate = CommonUtils.convertStringToDate(getFromDateStr(), "dd/MM/yyyy");
		return fromDate;
		
	}
	/**
	 * Setter method for fromDate
	 *
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * Getter method for toDate
	 * 
	 * @return the toDate (Date)
	 */
	public Date getToDate() {
		if(getToDateStr()!= null && !getToDateStr().equalsIgnoreCase(""))
			toDate = CommonUtils.convertStringToDate(getToDateStr(), "dd/MM/yyyy");
		return toDate;
	}
	/**
	 * Setter method for toDate
	 *
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	/**
	 * Getter method for noOfDays
	 * 
	 * @return the noOfDays (BigDecimal)
	 */
	public BigDecimal getNoOfDays() {
		return noOfDays;
	}
	/**
	 * Setter method for noOfDays
	 *
	 * @param noOfDays the noOfDays to set
	 */
	public void setNoOfDays(BigDecimal noOfDays) {
		this.noOfDays = noOfDays;
	}
	/**
	 * Getter method for rejoiningDate
	 * 
	 * @return the rejoiningDate (Date)
	 */
	public Date getRejoiningDate() {
		if(getRejoiningDateStr()!= null && !getRejoiningDateStr().equalsIgnoreCase(""))
			rejoiningDate = CommonUtils.convertStringToDate(getRejoiningDateStr(), "dd/MM/yyyy");
		return rejoiningDate;
	}
	/**
	 * Setter method for rejoiningDate
	 *
	 * @param rejoiningDate the rejoiningDate to set
	 */
	public void setRejoiningDate(Date rejoiningDate) {
		this.rejoiningDate = rejoiningDate;
	}
	/**
	 * Getter method for reasonForLeave
	 * 
	 * @return the reasonForLeave (String)
	 */
	public String getReasonForLeave() {
		return reasonForLeave;
	}
	/**
	 * Setter method for reasonForLeave
	 *
	 * @param reasonForLeave the reasonForLeave to set
	 */
	public void setReasonForLeave(String reasonForLeave) {
		this.reasonForLeave = reasonForLeave;
	}
	/**
	 * Getter method for contactNumber
	 * 
	 * @return the contactNumber (Integer)
	 */
	public String getContactNumber() {
		return contactNumber;
	}
	/**
	 * Setter method for contactNumber
	 *
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	/**
	 * Getter method for alternateNumber
	 * 
	 * @return the alternateNumber (Integer)
	 */
	public Integer getAlternateNumber() {
		return alternateNumber;
	}
	/**
	 * Setter method for alternateNumber
	 *
	 * @param alternateNumber the alternateNumber to set
	 */
	public void setAlternateNumber(Integer alternateNumber) {
		this.alternateNumber = alternateNumber;
	}
	/**
	 * Getter method for contactEmailId
	 * 
	 * @return the contactEmailId (String)
	 */
	public String getContactEmailId() {
		return contactEmailId;
	}
	/**
	 * Setter method for contactEmailId
	 *
	 * @param contactEmailId the contactEmailId to set
	 */
	public void setContactEmailId(String contactEmailId) {
		this.contactEmailId = contactEmailId;
	}
	/**
	 * Getter method for sanctionedBy
	 * 
	 * @return the sanctionedBy (Integer)
	 */
	public Integer getSanctionedBy() {
		return sanctionedBy;
	}
	/**
	 * Setter method for sanctionedBy
	 *
	 * @param sanctionedBy the sanctionedBy to set
	 */
	public void setSanctionedBy(Integer sanctionedBy) {
		this.sanctionedBy = sanctionedBy;
	}
	/**
	 * Getter method for sanctionedDate
	 * 
	 * @return the sanctionedDate (Date)
	 */
	public Date getSanctionedDate() {
		return sanctionedDate;
	}
	/**
	 * Setter method for sanctionedDate
	 *
	 * @param sanctionedDate the sanctionedDate to set
	 */
	public void setSanctionedDate(Date sanctionedDate) {
		this.sanctionedDate = sanctionedDate;
	}
	/**
	 * Getter method for approvedBy
	 * 
	 * @return the approvedBy (Integer)
	 */
	public Integer getApprovedBy() {
		return approvedBy;
	}
	/**
	 * Setter method for approvedBy
	 *
	 * @param approvedBy the approvedBy to set
	 */
	public void setApprovedBy(Integer approvedBy) {
		this.approvedBy = approvedBy;
	}
	/**
	 * Getter method for approvedDate
	 * 
	 * @return the approvedDate (Date)
	 */
	public Date getApprovedDate() {
		return approvedDate;
	}
	/**
	 * Setter method for approvedDate
	 *
	 * @param approvedDate the approvedDate to set
	 */
	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}
	/**
	 * Getter method for supervisorComments
	 * 
	 * @return the supervisorComments (String)
	 */
	public String getSupervisorComments() {
		return supervisorComments;
	}
	/**
	 * Setter method for supervisorComments
	 *
	 * @param supervisorComments the supervisorComments to set
	 */
	public void setSupervisorComments(String supervisorComments) {
		this.supervisorComments = supervisorComments;
	}
	/**
	 * Getter method for empAddress
	 * 
	 * @return the empAddress (String)
	 */
	public String getEmpAddress() {
		return empAddress;
	}
	/**
	 * Setter method for empAddress
	 *
	 * @param empAddress the empAddress to set
	 */
	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}
	/**
	 * Getter method for empRemarks
	 * 
	 * @return the empRemarks (String)
	 */
	public String getEmpRemarks() {
		return empRemarks;
	}
	/**
	 * Setter method for empRemarks
	 *
	 * @param empRemarks the empRemarks to set
	 */
	public void setEmpRemarks(String empRemarks) {
		this.empRemarks = empRemarks;
	}
	/**
	 * Getter method for compoff
	 * 
	 * @return the compoff (Boolean)
	 */
	public Boolean getCompoff() {
		return compoff;
	}
	/**
	 * Setter method for compoff
	 *
	 * @param compoff the compoff to set
	 */
	public void setCompoff(Boolean compoff) {
		this.compoff = compoff;
	}
	/**
	 * Getter method for comoffDays
	 * 
	 * @return the comoffDays (BigDecimal)
	 */
	public BigDecimal getComoffDays() {
		return comoffDays;
	}
	/**
	 * Setter method for comoffDays
	 *
	 * @param comoffDays the comoffDays to set
	 */
	public void setComoffDays(BigDecimal comoffDays) {
		this.comoffDays = comoffDays;
	}
	/**
	 * Getter method for createdOn
	 * 
	 * @return the createdOn (Date)
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * Setter method for createdOn
	 *
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * Getter method for createdBy
	 * 
	 * @return the createdBy (Integer)
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * Setter method for createdBy
	 *
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * Getter method for leaveStatus
	 * 
	 * @return the leaveStatus (LeaveStatusActionVo)
	 */
	public LeaveStatusEntityVo getLeaveStatus() {
		return leaveStatus;
	}
	/**
	 * Setter method for leaveStatus
	 *
	 * @param leaveStatus the leaveStatus to set
	 */
	public void setLeaveStatus(LeaveStatusEntityVo leaveStatus) {
		this.leaveStatus = leaveStatus;
	}
	/**
	 * Getter method for fromDateStr
	 * 
	 * @return the fromDateStr (String)
	 */
	public String getFromDateStr() {
		return fromDateStr;
	}
	/**
	 * Setter method for fromDateStr
	 *
	 * @param fromDateStr the fromDateStr to set
	 */
	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}
	/**
	 * Getter method for toDateStr
	 * 
	 * @return the toDateStr (String)
	 */
	public String getToDateStr() {
		return toDateStr;
	}
	/**
	 * Setter method for toDateStr
	 *
	 * @param toDateStr the toDateStr to set
	 */
	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}
	/**
	 * Getter method for rejoiningDateStr
	 * 
	 * @return the rejoiningDateStr (String)
	 */
	public String getRejoiningDateStr() {
		return rejoiningDateStr;
	}
	/**
	 * Setter method for rejoiningDateStr
	 *
	 * @param rejoiningDateStr the rejoiningDateStr to set
	 */
	public void setRejoiningDateStr(String rejoiningDateStr) {
		this.rejoiningDateStr = rejoiningDateStr;
	}
	/**
	 * Getter method for reportingManager
	 * 
	 * @return the reportingManager (String)
	 */
	public String getReportingManager() {
		return reportingManager;
	}
	/**
	 * Setter method for reportingManager
	 *
	 * @param reportingManager the reportingManager to set
	 */
	public void setReportingManager(String reportingManager) {
		this.reportingManager = reportingManager;
	}
	/**
	 * Getter method for leaveStatusActionVo
	 * 
	 * @return the leaveStatusActionVo (LeaveStatusActionVo)
	 */
	public LeaveStatusActionVo getLeaveStatusActionVo() {
		return leaveStatusActionVo;
	}
	/**
	 * Setter method for leaveStatusActionVo
	 *
	 * @param leaveStatusActionVo the leaveStatusActionVo to set
	 */
	public void setLeaveStatusActionVo(LeaveStatusActionVo leaveStatusActionVo) {
		this.leaveStatusActionVo = leaveStatusActionVo;
	}
	/**
	 * Getter method for employeeName
	 * 
	 * @return the employeeName (String)
	 */
	public String getEmployeeName() {
		return employeeName;
	}
	/**
	 * Setter method for employeeName
	 *
	 * @param employeeName the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	
	/**
	 * Getter method for leaveType
	 * 
	 * @return the leaveType (LeaveTypeActionVo)
	 */
	public LeaveTypeEntityVo getLeaveType() {
		return leaveType;
	}
	/**
	 * Setter method for leaveType
	 *
	 * @param leaveType the leaveType to set
	 */
	public void setLeaveType(LeaveTypeEntityVo leaveType) {
		this.leaveType = leaveType;
	}
	/**
	 * Getter method for leaveTypeActionVo
	 * 
	 * @return the leaveTypeActionVo (LeaveTypeActionVo)
	 */
	public LeaveTypeActionVo getLeaveTypeActionVo() {
		return leaveTypeActionVo;
	}
	/**
	 * Setter method for leaveTypeActionVo
	 *
	 * @param leaveTypeActionVo the leaveTypeActionVo to set
	 */
	public void setLeaveTypeActionVo(LeaveTypeActionVo leaveTypeActionVo) {
		this.leaveTypeActionVo = leaveTypeActionVo;
	}
    

}
