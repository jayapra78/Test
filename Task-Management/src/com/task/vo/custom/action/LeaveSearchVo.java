/**
 * Creater / durgeshkumarjhaSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import java.util.Date;

import com.task.common.CommonVo;
import com.task.util.CommonUtils;

/**
 * @author durgeshkumarjha
 *
 */
public class LeaveSearchVo extends CommonVo{
	
	/**
     * long serialVersionUID : 
     */
    private static final long serialVersionUID = 1L;
    
	    private Date fromDate;
	    private Date toDate;
	    private String fromDateStr;
	    private String toDateStr;
	    
	    
		/**
		 * Getter method for fromDate
		 * 
		 * @return the fromDate (Date)
		 */
	    public Date getFromDate() {
			if(getFromDateStr()!= null && !getFromDateStr().equalsIgnoreCase(""))
				fromDate = CommonUtils.convertStringToDate(getFromDateStr(), "dd/MM/yyyy");
			return fromDate;
			
		}
		/**
		 * Setter method for fromDate
		 *
		 * @param fromDate the fromDate to set
		 */
		public void setFromDate(Date fromDate) {
			this.fromDate = fromDate;
		}
		/**
		 * Getter method for toDate
		 * 
		 * @return the toDate (Date)
		 */
		public Date getToDate() {
			if(getToDateStr()!= null && !getToDateStr().equalsIgnoreCase(""))
				toDate = CommonUtils.convertStringToDate(getFromDateStr(), "dd/MM/yyyy");
			return toDate;
		}
		/**
		 * Setter method for toDate
		 *
		 * @param toDate the toDate to set
		 */
		public void setToDate(Date toDate) {
			this.toDate = toDate;
		}
		/**
		 * Getter method for fromDateStr
		 * 
		 * @return the fromDateStr (String)
		 */
		public String getFromDateStr() {
			return fromDateStr;
		}
		/**
		 * Setter method for fromDateStr
		 *
		 * @param fromDateStr the fromDateStr to set
		 */
		public void setFromDateStr(String fromDateStr) {
			this.fromDateStr = fromDateStr;
		}
		/**
		 * Getter method for toDateStr
		 * 
		 * @return the toDateStr (String)
		 */
		public String getToDateStr() {
			return toDateStr;
		}
		/**
		 * Setter method for toDateStr
		 *
		 * @param toDateStr the toDateStr to set
		 */
		public void setToDateStr(String toDateStr) {
			this.toDateStr = toDateStr;
		}
	     
}
