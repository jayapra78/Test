/**
 * Creater / durgeshkumarjhaSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import java.util.ArrayList;
import java.util.List;

import com.task.common.CommonVo;

/**
 * @author durgeshkumarjha
 *
 */
public class LeaveStatusActionVo extends CommonVo {
	
	/**
     * long serialVersionUID : 
     */
    private static final long serialVersionUID = 1L;

    private Integer leaveStatusId;
     private String leaveStatus;
     private List<LeaveActionVo> empLeaveDetails = new ArrayList<LeaveActionVo>(0);
	/**
	 * Getter method for leaveStatusId
	 * 
	 * @return the leaveStatusId (Integer)
	 */
	public Integer getLeaveStatusId() {
		return leaveStatusId;
	}
	/**
	 * Setter method for leaveStatusId
	 *
	 * @param leaveStatusId the leaveStatusId to set
	 */
	public void setLeaveStatusId(Integer leaveStatusId) {
		this.leaveStatusId = leaveStatusId;
	}
	/**
	 * Getter method for leaveStatus
	 * 
	 * @return the leaveStatus (String)
	 */
	public String getLeaveStatus() {
		return leaveStatus;
	}
	/**
	 * Setter method for leaveStatus
	 *
	 * @param leaveStatus the leaveStatus to set
	 */
	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}
	/**
	 * Getter method for empLeaveDetails
	 * 
	 * @return the empLeaveDetails (List<LeaveActionVo>)
	 */
	public List<LeaveActionVo> getEmpLeaveDetails() {
		return empLeaveDetails;
	}
	/**
	 * Setter method for empLeaveDetails
	 *
	 * @param empLeaveDetails the empLeaveDetails to set
	 */
	public void setEmpLeaveDetails(List<LeaveActionVo> empLeaveDetails) {
		this.empLeaveDetails = empLeaveDetails;
	}
     
     

}
