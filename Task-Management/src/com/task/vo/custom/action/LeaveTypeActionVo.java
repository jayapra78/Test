/**
 * Creater / durgeshkumarjhaSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import java.util.ArrayList;
import java.util.List;

import com.task.common.CommonVo;

/**
 * @author durgeshkumarjha
 *
 */
public class LeaveTypeActionVo extends CommonVo{
	
	/**
     * long serialVersionUID : 
     */
    private static final long serialVersionUID = 1L;
    
	private Integer leaveTypeId;
     private String leaveType;
     private List<LeaveActionVo> empLeaveDetails = new ArrayList<LeaveActionVo>(0);
	/**
	 * Getter method for leaveTypeId
	 * 
	 * @return the leaveTypeId (Integer)
	 */
	public Integer getLeaveTypeId() {
		return leaveTypeId;
	}
	/**
	 * Setter method for leaveTypeId
	 *
	 * @param leaveTypeId the leaveTypeId to set
	 */
	public void setLeaveTypeId(Integer leaveTypeId) {
		this.leaveTypeId = leaveTypeId;
	}
	/**
	 * Getter method for leaveType
	 * 
	 * @return the leaveType (String)
	 */
	public String getLeaveType() {
		return leaveType;
	}
	/**
	 * Setter method for leaveType
	 *
	 * @param leaveType the leaveType to set
	 */
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	/**
	 * Getter method for empLeaveDetails
	 * 
	 * @return the empLeaveDetails (List<LeaveActionVo>)
	 */
	public List<LeaveActionVo> getEmpLeaveDetails() {
		return empLeaveDetails;
	}
	/**
	 * Setter method for empLeaveDetails
	 *
	 * @param empLeaveDetails the empLeaveDetails to set
	 */
	public void setEmpLeaveDetails(List<LeaveActionVo> empLeaveDetails) {
		this.empLeaveDetails = empLeaveDetails;
	}
     
     

}
