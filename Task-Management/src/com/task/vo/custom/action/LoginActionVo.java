/**
 * Copyright 2012 by Shirai, All rights reserved.
 *
 * 2012/06/20 <Base Line Version 1.0>
 */
package com.task.vo.custom.action;

import com.task.common.CommonVo;

/**
 * ValueObject to store the LoginActionVo.
 *
 * @author Nagababu [SRM]
 */

public class LoginActionVo extends CommonVo {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long loginId;
	private String loginUserName;
	private String loginPassword;
	private Integer userType;
	private Character deleteFlag;

	/**
	 * Getter method for loginId
	 *
	 * @return the loginId
	 */
	public Long getLoginId() {
		return loginId;
	}

	/**
	 * Setter method for loginId
	 *
	 * @param loginId
	 *            the loginId to set
	 */
	public void setLoginId(Long loginId) {
		this.loginId = loginId;
	}

	/**
	 * Getter method for loginUserName
	 *
	 * @return the loginUserName
	 */
	public String getLoginUserName() {
		return loginUserName;
	}

	/**
	 * Setter method for loginUserName
	 *
	 * @param loginUserName
	 *            the loginUserName to set
	 */
	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}

	/**
	 * Getter method for loginPassword
	 *
	 * @return the loginPassword
	 */
	public String getLoginPassword() {
		return loginPassword;
	}

	/**
	 * Setter method for loginPassword
	 *
	 * @param loginPassword
	 *            the loginPassword to set
	 */
	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	/**
	 * Getter method for userType
	 *
	 * @return the userType
	 */
	public Integer getUserType() {
		return userType;
	}

	/**
	 * Setter method for userType
	 *
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}


	/**
	 * @return the deleteFlag
	 */
	public Character getDeleteFlag() {
		return deleteFlag;
	}

	/**
	 * @param deleteFlag the deleteFlag to set
	 */
	public void setDeleteFlag(Character deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

}
