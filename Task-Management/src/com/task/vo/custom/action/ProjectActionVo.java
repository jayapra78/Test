package com.task.vo.custom.action;

import com.task.common.CommonVo;

/**
 * ProjectType generated by hbm2java
 */
public class ProjectActionVo extends CommonVo {


    /**
	 * long serialVersionUID : 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
    private String code;
    private String clientName;
    private String name;
    private Character status;

   public ProjectActionVo() {
   }

	
   public ProjectActionVo(String code) {
       this.code = code;
   }
   public ProjectActionVo(String code, String clientName, String name, Character status) {
      this.code = code;
      this.clientName = clientName;
      this.name = name;
      this.status = status;
   }
  
   public Integer getId() {
       return this.id;
   }
   
   public void setId(Integer id) {
       this.id = id;
   }
   public String getCode() {
       return this.code;
   }
   
   public void setCode(String code) {
       this.code = code;
   }
   
   public String getClientName() {
       return this.clientName;
   }
   
   public void setClientName(String clientName) {
       this.clientName = clientName;
   }
   
   public String getName() {
       return this.name;
   }
   
   public void setName(String name) {
       this.name = name;
   }
   
   public Character getStatus() {
       return this.status;
   }
   
   public void setStatus(Character status) {
       this.status = status;
   }

    
    
}


