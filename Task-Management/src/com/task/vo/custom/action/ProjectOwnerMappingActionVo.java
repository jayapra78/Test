/**
 * Creater / DurgeshKumarJha[SRM] copyright information to SRM Technologies 2013
 * 
 */
package com.task.vo.custom.action;

import java.util.Date;

import com.opensymphony.xwork2.validator.annotations.Validations;
import com.task.common.CommonVo;

/**
 * ValueObject to store the ProjectOwnerMappingActionVo.
 * 
 * @author DurgeshKumarJha[SRM]
 * @since 22/02/2013
 * 
 */
@Validations
public class ProjectOwnerMappingActionVo extends CommonVo{
	/**
     * long serialVersionUID : 
     */
    private static final long serialVersionUID = 1L;
    
    private Integer projectOwnerMappingId;
    private ProjectStatusActionVo projectStatusActionVo;
    private Integer projectTypeId;
    private String projectId;
    private String projectCode;
    private String projectName;
    private String pmId;
    private String pmName;
    private String pmEmailId;
    private String plId;
    private String plName;
    private String plEmailId;
    private String deptId;
    private String createdBy;
    private Date createdOn;
    private String projectType;
    private Date startDate;
    private Date endDate;
    private String associatedProjectId;
    private String associatedProject;
    private String associatedFromStr;
    private Date associatedFrom;
    private String customerName;
    private String customerEmailId;
    private Character statusMail;
    private Character consolidatedMail;
    
    /**
     * Getter method for projectOwnerMappingId
     * 
     * @return the projectOwnerMappingId (Integer)
     */
    public Integer getProjectOwnerMappingId() {
        return projectOwnerMappingId;
    }
    /**
     * Setter method for projectOwnerMappingId
     *
     * @param projectOwnerMappingId the projectOwnerMappingId to set
     */
    public void setProjectOwnerMappingId(Integer projectOwnerMappingId) {
        this.projectOwnerMappingId = projectOwnerMappingId;
    }
    /**
     * Getter method for projectTypeId
     * 
     * @return the projectTypeId (Integer)
     */
    public Integer getProjectTypeId() {
        return projectTypeId;
    }
    /**
     * Setter method for projectTypeId
     *
     * @param projectTypeId the projectTypeId to set
     */
    public void setProjectTypeId(Integer projectTypeId) {
        this.projectTypeId = projectTypeId;
    }
    /**
     * Getter method for projectId
     * 
     * @return the projectId (String)
     */
    public String getProjectId() {
        return projectId;
    }
    /**
     * Setter method for projectId
     *
     * @param projectId the projectId to set
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
    /**
     * Getter method for projectCode
     * 
     * @return the projectCode (String)
     */
    public String getProjectCode() {
        return projectCode;
    }
    /**
     * Setter method for projectCode
     *
     * @param projectCode the projectCode to set
     */
    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }
    /**
     * Getter method for projectName
     * 
     * @return the projectName (String)
     */
    public String getProjectName() {
        return projectName;
    }
    /**
     * Setter method for projectName
     *
     * @param projectName the projectName to set
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    /**
     * Getter method for pmId
     * 
     * @return the pmId (String)
     */
    public String getPmId() {
        return pmId;
    }
    /**
     * Setter method for pmId
     *
     * @param pmId the pmId to set
     */
    public void setPmId(String pmId) {
        this.pmId = pmId;
    }
    /**
     * Getter method for pmName
     * 
     * @return the pmName (String)
     */
    public String getPmName() {
        return pmName;
    }
    /**
     * Setter method for pmName
     *
     * @param pmName the pmName to set
     */
    public void setPmName(String pmName) {
        this.pmName = pmName;
    }
    /**
     * Getter method for pmEmailId
     * 
     * @return the pmEmailId (String)
     */
    public String getPmEmailId() {
        return pmEmailId;
    }
    /**
     * Setter method for pmEmailId
     *
     * @param pmEmailId the pmEmailId to set
     */
    public void setPmEmailId(String pmEmailId) {
        this.pmEmailId = pmEmailId;
    }
    /**
     * Getter method for plId
     * 
     * @return the plId (String)
     */
    public String getPlId() {
        return plId;
    }
    /**
     * Setter method for plId
     *
     * @param plId the plId to set
     */
    public void setPlId(String plId) {
        this.plId = plId;
    }
    /**
     * Getter method for plName
     * 
     * @return the plName (String)
     */
    public String getPlName() {
        return plName;
    }
    /**
     * Setter method for plName
     *
     * @param plName the plName to set
     */
    public void setPlName(String plName) {
        this.plName = plName;
    }
    /**
     * Getter method for plEmailId
     * 
     * @return the plEmailId (String)
     */
    public String getPlEmailId() {
        return plEmailId;
    }
    /**
     * Setter method for plEmailId
     *
     * @param plEmailId the plEmailId to set
     */
    public void setPlEmailId(String plEmailId) {
        this.plEmailId = plEmailId;
    }
    /**
     * Getter method for deptId
     * 
     * @return the deptId (String)
     */
    public String getDeptId() {
        return deptId;
    }
    /**
     * Setter method for deptId
     *
     * @param deptId the deptId to set
     */
    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }
    /**
     * Getter method for createdBy
     * 
     * @return the createdBy (String)
     */
    public String getCreatedBy() {
        return createdBy;
    }
    /**
     * Setter method for createdBy
     *
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    /**
     * Getter method for createdOn
     * 
     * @return the createdOn (Date)
     */
    public Date getCreatedOn() {
        return createdOn;
    }
    /**
     * Setter method for createdOn
     *
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    /**
     * Getter method for projectType
     * 
     * @return the projectType (String)
     */
    public String getProjectType() {
        return projectType;
    }
    /**
     * Setter method for projectType
     *
     * @param projectType the projectType to set
     */
    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }
    /**
     * Getter method for projectStatusActionVo
     * 
     * @return the projectStatusActionVo (ProjectStatusActionVo)
     */
    public ProjectStatusActionVo getProjectStatusActionVo() {
        return projectStatusActionVo;
    }
    /**
     * Setter method for projectStatusActionVo
     *
     * @param projectStatusActionVo the projectStatusActionVo to set
     */
    public void setProjectStatusActionVo(ProjectStatusActionVo projectStatusActionVo) {
        this.projectStatusActionVo = projectStatusActionVo;
    }
    /**
     * Getter method for startDate
     * 
     * @return the startDate (Date)
     */
    public Date getStartDate() {
        return startDate;
    }
    /**
     * Setter method for startDate
     *
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    /**
     * Getter method for endDate
     * 
     * @return the endDate (Date)
     */
    public Date getEndDate() {
        return endDate;
    }
    /**
     * Setter method for endDate
     *
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
	public String getAssociatedProjectId() {
		return associatedProjectId;
	}
	public void setAssociatedProjectId(String associatedProjectId) {
		this.associatedProjectId = associatedProjectId;
	}
	public String getAssociatedFromStr() {
		return associatedFromStr;
	}
	public void setAssociatedFromStr(String associatedFromStr) {
		this.associatedFromStr = associatedFromStr;
	}
	public Date getAssociatedFrom() {
		return associatedFrom;
	}
	public void setAssociatedFrom(Date associatedFrom) {
		this.associatedFrom = associatedFrom;
	}
	public String getAssociatedProject() {
		return associatedProject;
	}
	public void setAssociatedProject(String associatedProject) {
		this.associatedProject = associatedProject;
	}
	/**
	 * Getter method for customerName
	 * 
	 * @return the customerName (String)
	 */
	public String getCustomerName() {
		return customerName;
	}
	/**
	 * Setter method for customerName
	 *
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	/**
	 * Getter method for customerEmailId
	 * 
	 * @return the customerEmailId (String)
	 */
	public String getCustomerEmailId() {
		return customerEmailId;
	}
	/**
	 * Setter method for customerEmailId
	 *
	 * @param customerEmailId the customerEmailId to set
	 */
	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}
	/**
	 * Getter method for statusMail
	 * 
	 * @return the statusMail (Character)
	 */
	public Character getStatusMail() {
		return statusMail;
	}
	/**
	 * Setter method for statusMail
	 *
	 * @param statusMail the statusMail to set
	 */
	public void setStatusMail(Character statusMail) {
		this.statusMail = statusMail;
	}
	/**
	 * Getter method for consolidatedMail
	 * 
	 * @return the consolidatedMail (Character)
	 */
	public Character getConsolidatedMail() {
		return consolidatedMail;
	}
	/**
	 * Setter method for consolidatedMail
	 *
	 * @param consolidatedMail the consolidatedMail to set
	 */
	public void setConsolidatedMail(Character consolidatedMail) {
		this.consolidatedMail = consolidatedMail;
	}
}
