/**
 * Creater / DurgeshKumarJha[SRM] copyright information to SRM Technologies 2013
 * 
 */
package com.task.vo.custom.action;

import com.task.common.CommonVo;

/**
 * ValueObject to store the ProjectOwnerSearchVo for search criteria.
 * 
 * @author DurgeshKumarJha[SRM]
 * @since 22/02/2013
 * 
 */
public class ProjectOwnerSearchVo extends CommonVo {
	
	/**
     * long serialVersionUID : 
     */
    private static final long serialVersionUID = 1L;
    
	    private Integer projectTypeId;
	    private String projectId;
	    private String projectCode;
	    private String pmId;
	    private String plId;
	    
	    private String pmName;
	    private String plName;
	    
		/**
		 * Getter method for projectTypeId
		 * 
		 * @return the projectTypeId (Integer)
		 */
		public Integer getProjectTypeId() {
			return projectTypeId;
		}
		/**
		 * Setter method for projectTypeId
		 *
		 * @param projectTypeId the projectTypeId to set
		 */
		public void setProjectTypeId(Integer projectTypeId) {
			this.projectTypeId = projectTypeId;
		}
		/**
		 * Getter method for projectId
		 * 
		 * @return the projectId (Integer)
		 */
		public String getProjectId() {
			return projectId;
		}
		/**
		 * Setter method for projectId
		 *
		 * @param projectId the projectId to set
		 */
		public void setProjectId(String projectId) {
			this.projectId = projectId;
		}
		/**
		 * Getter method for pmId
		 * 
		 * @return the pmId (String)
		 */
		public String getPmId() {
			return pmId;
		}
		/**
		 * Setter method for pmId
		 *
		 * @param pmId the pmId to set
		 */
		public void setPmId(String pmId) {
			this.pmId = pmId;
		}
		/**
		 * Getter method for plId
		 * 
		 * @return the plId (String)
		 */
		public String getPlId() {
			return plId;
		}
		/**
		 * Setter method for plId
		 *
		 * @param plId the plId to set
		 */
		public void setPlId(String plId) {
			this.plId = plId;
		}
		/**
		 * Getter method for pmName
		 * 
		 * @return the pmName (String)
		 */
		public String getPmName() {
			return pmName;
		}
		/**
		 * Setter method for pmName
		 *
		 * @param pmName the pmName to set
		 */
		public void setPmName(String pmName) {
			this.pmName = pmName;
		}
		/**
		 * Getter method for plName
		 * 
		 * @return the plName (String)
		 */
		public String getPlName() {
			return plName;
		}
		/**
		 * Setter method for plName
		 *
		 * @param plName the plName to set
		 */
		public void setPlName(String plName) {
			this.plName = plName;
		}
		/**
		 * Getter method for projectCode
		 * 
		 * @return the projectCode (String)
		 */
		public String getProjectCode() {
			return projectCode;
		}
		/**
		 * Setter method for projectCode
		 *
		 * @param projectCode the projectCode to set
		 */
		public void setProjectCode(String projectCode) {
			this.projectCode = projectCode;
		}
		
			    
}
