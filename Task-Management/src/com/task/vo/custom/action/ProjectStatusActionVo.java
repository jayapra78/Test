package com.task.vo.custom.action;
// Generated Mar 11, 2013 10:28:00 AM by Hibernate Tools 3.2.1.GA


import com.task.common.CommonVo;
public class ProjectStatusActionVo extends CommonVo {


     /**
     * long serialVersionUID : 
     */
    private static final long serialVersionUID = 1L;
    private Integer statusId;
    private String statusName;
    
     
    /**
     * Getter method for statusId
     * 
     * @return the statusId (Integer)
     */
    public Integer getStatusId() {
        return statusId;
    }
    /**
     * Setter method for statusId
     *
     * @param statusId the statusId to set
     */
    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }
    /**
     * Getter method for statusName
     * 
     * @return the statusName (String)
     */
    public String getStatusName() {
        return statusName;
    }
    /**
     * Setter method for statusName
     *
     * @param statusName the statusName to set
     */
    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}


