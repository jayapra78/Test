/**
 * Creater / neethimohanaSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import com.task.common.CommonVo;

/**
 * @author neethimohana
 *
 */
public class ProjectTypeActionVo extends CommonVo{

	/**
	 * long serialVersionUID : 
	 */
	private static final long serialVersionUID = 1L;
	private int projectTypeId;
	private String projectType;
	/**
	 * Getter method for projectTypeId
	 * 
	 * @return the projectTypeId (int)
	 */
	public int getProjectTypeId() {
		return projectTypeId;
	}
	/**
	 * Setter method for projectTypeId
	 *
	 * @param projectTypeId the projectTypeId to set
	 */
	public void setProjectTypeId(int projectTypeId) {
		this.projectTypeId = projectTypeId;
	}
	/**
	 * Getter method for projectType
	 * 
	 * @return the projectType (String)
	 */
	public String getProjectType() {
		return projectType;
	}
	/**
	 * Setter method for projectType
	 *
	 * @param projectType the projectType to set
	 */
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	
}
