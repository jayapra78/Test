/**
 * Creater / neethimohanaSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import com.task.common.CommonVo;

/**
 * @author neethimohana
 *
 */
public class RecursiveTaskActionVo extends CommonVo{
	   	 /**
	 * long serialVersionUID : 
	 */
	private static final long serialVersionUID = 1L;
	
		 private String taskStartDateStr;
	     private String taskEndDateStr;
	     private String planStartTime;
	     private String planEndTime;
	     private String planDesc;
	     private String taskDay;
	     private String weekDays;
		
		/**
		 * Getter method for planStartTime
		 * 
		 * @return the planStartTime (String)
		 */
	     
		public String getPlanStartTime() {
			return planStartTime;
		}
		/**
		 * Setter method for planStartTime
		 *
		 * @param planStartTime the planStartTime to set
		 */
		public void setPlanStartTime(String planStartTime) {
			this.planStartTime = planStartTime;
		}
		/**
		 * Getter method for planEndTime
		 * 
		 * @return the planEndTime (String)
		 */
		public String getPlanEndTime() {
			return planEndTime;
		}
		/**
		 * Setter method for planEndTime
		 *
		 * @param planEndTime the planEndTime to set
		 */
		public void setPlanEndTime(String planEndTime) {
			this.planEndTime = planEndTime;
		}
		/**
		 * Getter method for taskStartDateStr
		 * 
		 * @return the taskStartDateStr (String)
		 */
		public String getTaskStartDateStr() {
			return taskStartDateStr;
		}
		/**
		 * Setter method for taskStartDateStr
		 *
		 * @param taskStartDateStr the taskStartDateStr to set
		 */
		public void setTaskStartDateStr(String taskStartDateStr) {
			this.taskStartDateStr = taskStartDateStr;
		}
		/**
		 * Getter method for taskEndDateStr
		 * 
		 * @return the taskEndDateStr (String)
		 */
		public String getTaskEndDateStr() {
			return taskEndDateStr;
		}
		/**
		 * Setter method for taskEndDateStr
		 *
		 * @param taskEndDateStr the taskEndDateStr to set
		 */
		public void setTaskEndDateStr(String taskEndDateStr) {
			this.taskEndDateStr = taskEndDateStr;
		}
		/**
		 * Getter method for planDesc
		 * 
		 * @return the planDesc (String)
		 */
		public String getPlanDesc() {
			return planDesc;
		}
		/**
		 * Setter method for planDesc
		 *
		 * @param planDesc the planDesc to set
		 */
		public void setPlanDesc(String planDesc) {
			this.planDesc = planDesc;
		}
		/**
		 * Getter method for taskDay
		 * 
		 * @return the taskDay (String)
		 */
		public String getTaskDay() {
			return taskDay;
		}
		/**
		 * Setter method for taskDay
		 *
		 * @param taskDay the taskDay to set
		 */
		public void setTaskDay(String taskDay) {
			this.taskDay = taskDay;
		}
		/**
		 * Getter method for weekDays
		 * 
		 * @return the weekDays (String)
		 */
		public String getWeekDays() {
			return weekDays;
		}
		/**
		 * Setter method for weekDays
		 *
		 * @param weekDays the weekDays to set
		 */
		public void setWeekDays(String weekDays) {
			this.weekDays = weekDays;
		}

}
