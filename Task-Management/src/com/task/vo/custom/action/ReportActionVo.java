/**
 * Creater / balamurugankSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import java.util.Date;

import com.task.common.CommonVo;

/**
 * @author balamurugank
 *
 */
public class ReportActionVo extends CommonVo{

    /**
     * long serialVersionUID : 
     */
    private static final long serialVersionUID = 1L;
    private String REPORTEMP_ID;	// PM ID
    private String MANAGER;		// PM Name
    private String PLAN_TASK_DESC;	// Plan Task Description
    private String PLAN_START_TIME;	// Plan Start Time
    private String PLAN_END_TIME;	// Plan End Time
    private String ACTUAL_TASK_DESC;	// Actual Task Description
    private String ACTUAL_START_TIME;	// Actual Start Time
    private String ACTUAL_END_TIME;	// Actual End Time
    private String ACTIVITY;		// Activity
    private String WORK_ITEM;		// Work Item
    private Integer TASK_ID;		// Task Id
    private Date TASK_DATE;		// Task Date
    private String Code;		// Emp Code
    private String Name;		// Emp Name
    private String PROJECT_ID;		// Project Id
    private Integer PROJECT_TYPE_ID;	// Project Type Id
    private String PROJECT_TYPE;	// Project Type
    private String PROJECT_CODE;	// Project Code
    private String PROJECT_NAME;	// Project Name
    private boolean overall;;
    
    private String taskDate;
    
    private String taskStartDate;
    private String taskEndDate;
    
    
    
    /**
     * Getter method for REPORTEMP_ID
     * 
     * @return the rEPORTEMP_ID (String)
     */
    public String getREPORTEMP_ID() {
        return REPORTEMP_ID;
    }
    /**
     * Setter method for REPORTEMP_ID
     *
     * @param rEPORTEMP_ID the rEPORTEMP_ID to set
     */
    public void setREPORTEMP_ID(String rEPORTEMP_ID) {
        REPORTEMP_ID = rEPORTEMP_ID;
    }
    /**
     * Getter method for MANAGER
     * 
     * @return the mANAGER (String)
     */
    public String getMANAGER() {
        return MANAGER;
    }
    /**
     * Setter method for MANAGER
     *
     * @param mANAGER the mANAGER to set
     */
    public void setMANAGER(String mANAGER) {
        MANAGER = mANAGER;
    }
    /**
     * Getter method for PLAN_TASK_DESC
     * 
     * @return the pLAN_TASK_DESC (String)
     */
    public String getPLAN_TASK_DESC() {
        return PLAN_TASK_DESC;
    }
    /**
     * Setter method for PLAN_TASK_DESC
     *
     * @param pLAN_TASK_DESC the pLAN_TASK_DESC to set
     */
    public void setPLAN_TASK_DESC(String pLAN_TASK_DESC) {
        PLAN_TASK_DESC = pLAN_TASK_DESC;
    }
    /**
     * Getter method for PLAN_START_TIME
     * 
     * @return the pLAN_START_TIME (String)
     */
    public String getPLAN_START_TIME() {
        return PLAN_START_TIME;
    }
    /**
     * Setter method for PLAN_START_TIME
     *
     * @param pLAN_START_TIME the pLAN_START_TIME to set
     */
    public void setPLAN_START_TIME(String pLAN_START_TIME) {
        PLAN_START_TIME = pLAN_START_TIME;
    }
    /**
     * Getter method for PLAN_END_TIME
     * 
     * @return the pLAN_END_TIME (String)
     */
    public String getPLAN_END_TIME() {
        return PLAN_END_TIME;
    }
    /**
     * Setter method for PLAN_END_TIME
     *
     * @param pLAN_END_TIME the pLAN_END_TIME to set
     */
    public void setPLAN_END_TIME(String pLAN_END_TIME) {
        PLAN_END_TIME = pLAN_END_TIME;
    }
    /**
     * Getter method for ACTUAL_TASK_DESC
     * 
     * @return the aCTUAL_TASK_DESC (String)
     */
    public String getACTUAL_TASK_DESC() {
        return ACTUAL_TASK_DESC;
    }
    /**
     * Setter method for ACTUAL_TASK_DESC
     *
     * @param aCTUAL_TASK_DESC the aCTUAL_TASK_DESC to set
     */
    public void setACTUAL_TASK_DESC(String aCTUAL_TASK_DESC) {
        ACTUAL_TASK_DESC = aCTUAL_TASK_DESC;
    }
    /**
     * Getter method for ACTUAL_START_TIME
     * 
     * @return the aCTUAL_START_TIME (String)
     */
    public String getACTUAL_START_TIME() {
        return ACTUAL_START_TIME;
    }
    /**
     * Setter method for ACTUAL_START_TIME
     *
     * @param aCTUAL_START_TIME the aCTUAL_START_TIME to set
     */
    public void setACTUAL_START_TIME(String aCTUAL_START_TIME) {
        ACTUAL_START_TIME = aCTUAL_START_TIME;
    }
    /**
     * Getter method for ACTUAL_END_TIME
     * 
     * @return the aCTUAL_END_TIME (String)
     */
    public String getACTUAL_END_TIME() {
        return ACTUAL_END_TIME;
    }
    /**
     * Setter method for ACTUAL_END_TIME
     *
     * @param aCTUAL_END_TIME the aCTUAL_END_TIME to set
     */
    public void setACTUAL_END_TIME(String aCTUAL_END_TIME) {
        ACTUAL_END_TIME = aCTUAL_END_TIME;
    }
    /**
     * Getter method for ACTIVITY
     * 
     * @return the aCTIVITY (String)
     */
    public String getACTIVITY() {
        return ACTIVITY;
    }
    /**
     * Setter method for ACTIVITY
     *
     * @param aCTIVITY the aCTIVITY to set
     */
    public void setACTIVITY(String aCTIVITY) {
        ACTIVITY = aCTIVITY;
    }
    /**
     * Getter method for WORK_ITEM
     * 
     * @return the wORK_ITEM (String)
     */
    public String getWORK_ITEM() {
        return WORK_ITEM;
    }
    /**
     * Setter method for WORK_ITEM
     *
     * @param wORK_ITEM the wORK_ITEM to set
     */
    public void setWORK_ITEM(String wORK_ITEM) {
        WORK_ITEM = wORK_ITEM;
    }
    /**
     * Getter method for TASK_ID
     * 
     * @return the tASK_ID (Integer)
     */
    public Integer getTASK_ID() {
        return TASK_ID;
    }
    /**
     * Setter method for TASK_ID
     *
     * @param tASK_ID the tASK_ID to set
     */
    public void setTASK_ID(Integer tASK_ID) {
        TASK_ID = tASK_ID;
    }
    /**
     * Getter method for TASK_DATE
     * 
     * @return the tASK_DATE (Date)
     */
    public Date getTASK_DATE() {
        return TASK_DATE;
    }
    /**
     * Setter method for TASK_DATE
     *
     * @param tASK_DATE the tASK_DATE to set
     */
    public void setTASK_DATE(Date tASK_DATE) {
        TASK_DATE = tASK_DATE;
    }
    /**
     * Getter method for Code
     * 
     * @return the code (String)
     */
    public String getCode() {
        return Code;
    }
    /**
     * Setter method for Code
     *
     * @param code the code to set
     */
    public void setCode(String code) {
        Code = code;
    }
    /**
     * Getter method for Name
     * 
     * @return the name (String)
     */
    public String getName() {
        return Name;
    }
    /**
     * Setter method for Name
     *
     * @param name the name to set
     */
    public void setName(String name) {
        Name = name;
    }
    /**
     * Getter method for PROJECT_ID
     * 
     * @return the pROJECT_ID (String)
     */
    public String getPROJECT_ID() {
        return PROJECT_ID;
    }
    /**
     * Setter method for PROJECT_ID
     *
     * @param pROJECT_ID the pROJECT_ID to set
     */
    public void setPROJECT_ID(String pROJECT_ID) {
        PROJECT_ID = pROJECT_ID;
    }
    /**
     * Getter method for PROJECT_TYPE_ID
     * 
     * @return the pROJECT_TYPE_ID (Integer)
     */
    public Integer getPROJECT_TYPE_ID() {
        return PROJECT_TYPE_ID;
    }
    /**
     * Setter method for PROJECT_TYPE_ID
     *
     * @param pROJECT_TYPE_ID the pROJECT_TYPE_ID to set
     */
    public void setPROJECT_TYPE_ID(Integer pROJECT_TYPE_ID) {
        PROJECT_TYPE_ID = pROJECT_TYPE_ID;
    }
    /**
     * Getter method for PROJECT_TYPE
     * 
     * @return the pROJECT_TYPE (String)
     */
    public String getPROJECT_TYPE() {
        return PROJECT_TYPE;
    }
    /**
     * Setter method for PROJECT_TYPE
     *
     * @param pROJECT_TYPE the pROJECT_TYPE to set
     */
    public void setPROJECT_TYPE(String pROJECT_TYPE) {
        PROJECT_TYPE = pROJECT_TYPE;
    }
    /**
     * Getter method for PROJECT_CODE
     * 
     * @return the pROJECT_CODE (String)
     */
    public String getPROJECT_CODE() {
        return PROJECT_CODE;
    }
    /**
     * Setter method for PROJECT_CODE
     *
     * @param pROJECT_CODE the pROJECT_CODE to set
     */
    public void setPROJECT_CODE(String pROJECT_CODE) {
        PROJECT_CODE = pROJECT_CODE;
    }
    /**
     * Getter method for PROJECT_NAME
     * 
     * @return the pROJECT_NAME (String)
     */
    public String getPROJECT_NAME() {
        return PROJECT_NAME;
    }
    /**
     * Setter method for PROJECT_NAME
     *
     * @param pROJECT_NAME the pROJECT_NAME to set
     */
    public void setPROJECT_NAME(String pROJECT_NAME) {
        PROJECT_NAME = pROJECT_NAME;
    }
    /**
     * Getter method for taskStartDate
     * 
     * @return the taskStartDate (Date)
     */
    public String getTaskStartDate() {
        return taskStartDate;
    }
    /**
     * Setter method for taskStartDate
     *
     * @param taskStartDate the taskStartDate to set
     */
    public void setTaskStartDate(String taskStartDate) {
        this.taskStartDate = taskStartDate;
    }
    /**
     * Getter method for taskEndDate
     * 
     * @return the taskEndDate (Date)
     */
    public String getTaskEndDate() {
        return taskEndDate;
    }
    /**
     * Setter method for taskEndDate
     *
     * @param taskEndDate the taskEndDate to set
     */
    public void setTaskEndDate(String taskEndDate) {
        this.taskEndDate = taskEndDate;
    }
    /**
     * Getter method for taskDate
     * 
     * @return the taskDate (String)
     */
    public String getTaskDate() {
        return taskDate;
    }
    /**
     * Setter method for taskDate
     *
     * @param taskDate the taskDate to set
     */
    public void setTaskDate(String taskDate) {
        this.taskDate = taskDate;
    }
	public boolean isOverall() {
		return overall;
	}
	public void setOverall(boolean overall) {
		this.overall = overall;
	}
}
