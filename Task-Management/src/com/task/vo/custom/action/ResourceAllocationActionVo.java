/**
 * Creater / balamurugankSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import java.util.Date;

import com.task.common.CommonVo;
import com.task.vo.custom.entity.ModeEntityVo;
import com.task.vo.custom.entity.RoleEntityVo;

/**
 * @author balamurugank
 *
 */
public class ResourceAllocationActionVo extends CommonVo{
    
    /**
     * long serialVersionUID : 
     */
    private static final long serialVersionUID = 1L;
    private Integer resourceAllocationId;
    private RoleEntityVo role;
    private ModeEntityVo mode;
    private Integer projectId;
    private Integer employeeId;
    private Date startDate;
    private Date endDate;
    private Integer createdBy;
    private Date createdOn;
    private boolean resourceReleased;
    private Integer projectTypeId;
    
    /**
     * Getter method for resourceAllocationId
     * 
     * @return the resourceAllocationId (Integer)
     */
    public Integer getResourceAllocationId() {
        return resourceAllocationId;
    }
    /**
     * Setter method for resourceAllocationId
     *
     * @param resourceAllocationId the resourceAllocationId to set
     */
    public void setResourceAllocationId(Integer resourceAllocationId) {
        this.resourceAllocationId = resourceAllocationId;
    }
    /**
     * Getter method for role
     * 
     * @return the role (RoleEntityVo)
     */
    public RoleEntityVo getRole() {
        return role;
    }
    /**
     * Setter method for role
     *
     * @param role the role to set
     */
    public void setRole(RoleEntityVo role) {
        this.role = role;
    }
    /**
     * Getter method for mode
     * 
     * @return the mode (ModeEntityVo)
     */
    public ModeEntityVo getMode() {
        return mode;
    }
    /**
     * Setter method for mode
     *
     * @param mode the mode to set
     */
    public void setMode(ModeEntityVo mode) {
        this.mode = mode;
    }
    /**
     * Getter method for projectId
     * 
     * @return the projectId (Integer)
     */
    public Integer getProjectId() {
        return projectId;
    }
    /**
     * Setter method for projectId
     *
     * @param projectId the projectId to set
     */
    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
    /**
     * Getter method for employeeId
     * 
     * @return the employeeId (Integer)
     */
    public Integer getEmployeeId() {
        return employeeId;
    }
    /**
     * Setter method for employeeId
     *
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }
    /**
     * Getter method for startDate
     * 
     * @return the startDate (Date)
     */
    public Date getStartDate() {
        return startDate;
    }
    /**
     * Setter method for startDate
     *
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    /**
     * Getter method for endDate
     * 
     * @return the endDate (Date)
     */
    public Date getEndDate() {
        return endDate;
    }
    /**
     * Setter method for endDate
     *
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    /**
     * Getter method for createdBy
     * 
     * @return the createdBy (Integer)
     */
    public Integer getCreatedBy() {
        return createdBy;
    }
    /**
     * Setter method for createdBy
     *
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }
    /**
     * Getter method for createdOn
     * 
     * @return the createdOn (Date)
     */
    public Date getCreatedOn() {
        return createdOn;
    }
    /**
     * Setter method for createdOn
     *
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    /**
     * Getter method for resourceReleased
     * 
     * @return the resourceReleased (boolean)
     */
    public boolean isResourceReleased() {
        return resourceReleased;
    }
    /**
     * Setter method for resourceReleased
     *
     * @param resourceReleased the resourceReleased to set
     */
    public void setResourceReleased(boolean resourceReleased) {
        this.resourceReleased = resourceReleased;
    }
    /**
     * @return the projectTypeId
     */
    public Integer getProjectTypeId() {
        return projectTypeId;
    }
    /**
     * @param projectTypeId the projectTypeId to set
     */
    public void setProjectTypeId(Integer projectTypeId) {
        this.projectTypeId = projectTypeId;
    }


}
