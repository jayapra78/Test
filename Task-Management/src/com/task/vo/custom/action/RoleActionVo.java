/**
 * Creater / balamurugankSRM copyright information to Shirai 2013
 * 
 */
package com.task.vo.custom.action;

import com.task.common.CommonVo;

/**
 * @author balamurugank
 *
 */
public class RoleActionVo extends CommonVo{
    private Integer roleId;
    private String roleName;
}
