package com.task.vo.custom.action;

import java.math.BigDecimal;
import java.util.Date;

import com.task.common.CommonVo;




public class TaskActionVo extends CommonVo implements Comparable<TaskActionVo> {
     /**
	 * long serialVersionUID : 
	 */
	private static final long serialVersionUID = 1L;
	 private Integer taskId;
     private ProjectTypeActionVo projectTypeActionVo;
     private String reportempId;
     private String employeeId;
     private String employeeCode;
     private String projectId;
     private Date taskDate;
     private String taskDateStr;
     private String planTaskDesc;
     private String planStartTime;
     private String planEndTime;
     private String actualTaskDesc;
     private String actualStartTime;
     private String actualEndTime;
     private String activity;
     private String workItem;
     private Date createdOn;
     private String projectCode;
     private String activityId;
     private Date taskStartDate;
     private Date taskEndDate;
     private String empName;
     private String pmName;
     private String pmEmailId;
     private String sbuEmailId;
     private String sbuName;
     private String plEmailId;
     private String projectName;
     private String module;
     private Integer unitId;
     private BigDecimal size;
     private Integer timesheetId;
     private String associatedProjectId;
     private String bulkStartDateStr;
     private String bulkEndDateStr;
     private String customerEmailId;
     
	 public int compareTo(TaskActionVo vo){
		 int c =(this.getEmpName()).compareTo(vo.getEmpName());
		 if(c == 0 && this.getPlanStartTime()!=null){
			 c = (this.getPlanStartTime()).compareTo(vo.getPlanStartTime());
		 }
		 if(c == 0 && this.getActualStartTime()!=null){
			 c = (this.getActualStartTime()).compareTo(vo.getActualStartTime());
		 }
	    return c;
	 }
     
	/**
	 * Getter method for empName
	 * 
	 * @return the empName (String)
	 */
	public String getEmpName() {
		return empName;
	}
	/**
	 * Setter method for empName
	 *
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	/**
	 * Getter method for taskId
	 * 
	 * @return the taskId (Integer)
	 */
	public Integer getTaskId() {
		return taskId;
	}
	/**
	 * Setter method for taskId
	 *
	 * @param taskId the taskId to set
	 */
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	/**
	 * Getter method for projectTypeActionVo
	 * 
	 * @return the projectTypeActionVo (ProjectTypeActionVo)
	 */
	public ProjectTypeActionVo getProjectTypeActionVo() {
		return projectTypeActionVo;
	}
	/**
	 * Setter method for projectTypeActionVo
	 *
	 * @param projectTypeActionVo the projectTypeActionVo to set
	 */
	public void setProjectTypeActionVo(ProjectTypeActionVo projectTypeActionVo) {
		this.projectTypeActionVo = projectTypeActionVo;
	}
	/**
	 * Getter method for reportempId
	 * 
	 * @return the reportempId (String)
	 */
	public String getReportempId() {
		return reportempId;
	}
	/**
	 * Setter method for reportempId
	 *
	 * @param reportempId the reportempId to set
	 */
	public void setReportempId(String reportempId) {
		this.reportempId = reportempId;
	}
	/**
	 * Getter method for employeeId
	 * 
	 * @return the employeeId (String)
	 */
	public String getEmployeeId() {
		return employeeId;
	}
	/**
	 * Setter method for employeeId
	 *
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	/**
	 * Getter method for projectId
	 * 
	 * @return the projectId (String)
	 */
	public String getProjectId() {
		return projectId;
	}
	/**
	 * Setter method for projectId
	 *
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	/**
	 * Getter method for taskDate
	 * 
	 * @return the taskDate (Date)
	 */
	public Date getTaskDate() {
		return taskDate;
	}
	/**
	 * Setter method for taskDate
	 *
	 * @param taskDate the taskDate to set
	 */
	public void setTaskDate(Date taskDate) {
		this.taskDate = taskDate;
		//this.taskDate = CommonUtils.convertStringToDate(taskDateStr, "dd/MM/yyyy");
	}
	/**
	 * Getter method for planTaskDesc
	 * 
	 * @return the planTaskDesc (String)
	 */
	public String getPlanTaskDesc() {
		return planTaskDesc;
	}
	/**
	 * Setter method for planTaskDesc
	 *
	 * @param planTaskDesc the planTaskDesc to set
	 */
	public void setPlanTaskDesc(String planTaskDesc) {
		this.planTaskDesc = planTaskDesc;
	}
	/**
	 * Getter method for planStartTime
	 * 
	 * @return the planStartTime (Date)
	 */
	public String getPlanStartTime() {
		return planStartTime;
	}
	/**
	 * Setter method for planStartTime
	 *
	 * @param planStartTime the planStartTime to set
	 */
	public void setPlanStartTime(String planStartTime) {
		this.planStartTime = planStartTime;
	}
	/**
	 * Getter method for planEndTime
	 * 
	 * @return the planEndTime (Date)
	 */
	public String getPlanEndTime() {
		return planEndTime;
	}
	/**
	 * Setter method for planEndTime
	 *
	 * @param planEndTime the planEndTime to set
	 */
	public void setPlanEndTime(String planEndTime) {
		this.planEndTime = planEndTime;
	}
	/**
	 * Getter method for actualTaskDesc
	 * 
	 * @return the actualTaskDesc (String)
	 */
	public String getActualTaskDesc() {
		return actualTaskDesc;
	}
	/**
	 * Setter method for actualTaskDesc
	 *
	 * @param actualTaskDesc the actualTaskDesc to set
	 */
	public void setActualTaskDesc(String actualTaskDesc) {
		this.actualTaskDesc = actualTaskDesc;
	}
	/**
	 * Getter method for actualStartTime
	 * 
	 * @return the actualStartTime (String)
	 */
	public String getActualStartTime() {
		return actualStartTime;
	}
	/**
	 * Setter method for actualStartTime
	 *
	 * @param actualStartTime the actualStartTime to set
	 */
	public void setActualStartTime(String actualStartTime) {
		this.actualStartTime = actualStartTime;
	}
	/**
	 * Getter method for actualEndTime
	 * 
	 * @return the actualEndTime (String)
	 */
	public String getActualEndTime() {
		return actualEndTime;
	}
	/**
	 * Setter method for actualEndTime
	 *
	 * @param actualEndTime the actualEndTime to set
	 */
	public void setActualEndTime(String actualEndTime) {
		this.actualEndTime = actualEndTime;
	}
	/**
	 * Getter method for activity
	 * 
	 * @return the activity (String)
	 */
	public String getActivity() {
		return activity;
	}
	/**
	 * Setter method for activity
	 *
	 * @param activity the activity to set
	 */
	public void setActivity(String activity) {
		this.activity = activity;
	}
	/**
	 * Getter method for workItem
	 * 
	 * @return the workItem (String)
	 */
	public String getWorkItem() {
		return workItem;
	}
	/**
	 * Setter method for workItem
	 *
	 * @param workItem the workItem to set
	 */
	public void setWorkItem(String workItem) {
		this.workItem = workItem;
	}
	/**
	 * Getter method for createdOn
	 * 
	 * @return the createdOn (Date)
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * Setter method for createdOn
	 *
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getTaskDateStr() {
		return taskDateStr;
	}
	public void setTaskDateStr(String taskDateStr) {
		this.taskDateStr = taskDateStr;
	}
	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public Date getTaskStartDate() {
		return taskStartDate;
	}
	public void setTaskStartDate(Date taskStartDate) {
		this.taskStartDate = taskStartDate;
	}
	public Date getTaskEndDate() {
		return taskEndDate;
	}
	public void setTaskEndDate(Date taskEndDate) {
		this.taskEndDate = taskEndDate;
	}
	public String getPmName() {
		return pmName;
	}
	public void setPmName(String pmName) {
		this.pmName = pmName;
	}
	public String getPmEmailId() {
		return pmEmailId;
	}
	public void setPmEmailId(String pmEmailId) {
		this.pmEmailId = pmEmailId;
	}
	public String getSbuEmailId() {
		return sbuEmailId;
	}
	public void setSbuEmailId(String sbuEmailId) {
		this.sbuEmailId = sbuEmailId;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getSbuName() {
		return sbuName;
	}

	public void setSbuName(String sbuName) {
		this.sbuName = sbuName;
	}

	public String getPlEmailId() {
		return plEmailId;
	}

	public void setPlEmailId(String plEmailId) {
		this.plEmailId = plEmailId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public BigDecimal getSize() {
		return size;
	}

	public void setSize(BigDecimal size) {
		if(size==null || new BigDecimal("0").compareTo(size)==0) size=new BigDecimal("0.0");
		this.size = size;
	}

	public Integer getTimesheetId() {
		return timesheetId;
	}

	public void setTimesheetId(Integer timesheetId) {
		this.timesheetId = timesheetId;
	}

	public String getAssociatedProjectId() {
		return associatedProjectId;
	}

	public void setAssociatedProjectId(String associatedProjectId) {
		this.associatedProjectId = associatedProjectId;
	}

	public String getBulkStartDateStr() {
		return bulkStartDateStr;
	}

	public void setBulkStartDateStr(String bulkStartDateStr) {
		this.bulkStartDateStr = bulkStartDateStr;
	}

	public String getBulkEndDateStr() {
		return bulkEndDateStr;
	}

	public void setBulkEndDateStr(String bulkEndDateStr) {
		this.bulkEndDateStr = bulkEndDateStr;
	}

	/**
	 * Getter method for customerEmailId
	 * 
	 * @return the customerEmailId (String)
	 */
	public String getCustomerEmailId() {
		return customerEmailId;
	}

	/**
	 * Setter method for customerEmailId
	 *
	 * @param customerEmailId the customerEmailId to set
	 */
	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}

	/**
	 * Getter method for module
	 * 
	 * @return the module (String)
	 */
	public String getModule() {
		return module;
	}

	/**
	 * Setter method for module
	 *
	 * @param module the module to set
	 */
	public void setModule(String module) {
		this.module = module;
	}

}


