/** 
 * Copyright 2012 by Shirai, All rights reserved. 
 * 
 * 2012/06/20 <Base Line Version 1.0> 
 */
package com.task.vo.custom.action;

import com.task.common.CommonVo;

/**
 * ValueObject to store the MobileActionVo.
 * 
 * @author Rajnarayan Saraf [SRM]
 */
public class UserLoginVo extends CommonVo {
	
	private static final long serialVersionUID = 1L;
	private String loginName;
	private String password;
	private String language;
	/**
	 * The Method to get password
	 *
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	/** 
	 * The Method to set password
	 *
	 * @param password  password 
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * The Method to get loginName
	 *
	 * @return loginName
	 */
	public String getLoginName() {
		return loginName;
	}
	/** 
	 * The Method to set loginName
	 *
	 * @param loginName  loginName 
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	/**
	 * Getter method for language
	 * 
	 * @return the language (String)
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * Setter method for language
	 *
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
}
