package com.task.vo.custom.entity;
// Generated Feb 9, 2013 3:25:43 PM by Hibernate Tools 3.2.1.GA


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.task.common.CommonVo;

/**
 * Minoractivity generated by hbm2java
 */
@Entity
@Table(name="minoractivity")
public class MinoractivityEntityVo extends CommonVo {


     private int id;
     private String code;
     private String description;

    public MinoractivityEntityVo() {
    }

	
    public MinoractivityEntityVo(int id) {
        this.id = id;
    }
    public MinoractivityEntityVo(int id, String code, String description) {
       this.id = id;
       this.code = code;
       this.description = description;
    }
   
     @Id 
    
    @Column(name="Id", unique=true, nullable=false)
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    @Column(name="Code", length=65535)
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    @Column(name="Description", length=65535)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }




}


