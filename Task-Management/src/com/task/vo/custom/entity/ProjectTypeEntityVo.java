package com.task.vo.custom.entity;
// Generated Feb 9, 2013 3:25:43 PM by Hibernate Tools 3.2.1.GA


import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.task.common.CommonVo;

/**
 * ProjectType generated by hbm2java
 */
@Entity
@Table(name="project_type")
public class ProjectTypeEntityVo  extends CommonVo {


     private Integer projectTypeId;
     private String projectType;
     private Set<TaskEntityVo> tasks = new HashSet<TaskEntityVo>(0);

    public ProjectTypeEntityVo() {
    }

	
    public ProjectTypeEntityVo(String projectType) {
        this.projectType = projectType;
    }
    public ProjectTypeEntityVo(String projectType, Set<TaskEntityVo> tasks) {
       this.projectType = projectType;
       this.tasks = tasks;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="PROJECT_TYPE_ID", unique=true, nullable=false)
    public Integer getProjectTypeId() {
        return this.projectTypeId;
    }
    
    public void setProjectTypeId(Integer projectTypeId) {
        this.projectTypeId = projectTypeId;
    }
    
    @Column(name="PROJECT_TYPE", nullable=false, length=50)
    public String getProjectType() {
        return this.projectType;
    }
    
    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="projectType")
    public Set<TaskEntityVo> getTasks() {
        return this.tasks;
    }
    
    public void setTasks(Set<TaskEntityVo> tasks) {
        this.tasks = tasks;
    }




}


