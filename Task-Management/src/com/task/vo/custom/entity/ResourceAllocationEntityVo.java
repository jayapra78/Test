package com.task.vo.custom.entity;

// default package
// Generated 26-Mar-2013 09:40:57 by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.task.common.CommonVo;


/**
 * ResourceAllocation generated by hbm2java
 */
@Entity
@Table(name = "resource_allocation")
public class ResourceAllocationEntityVo extends CommonVo implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Integer resourceAllocationId;
    private RoleEntityVo role;
    private ModeEntityVo mode;
    private Integer projectId;
    private Integer employeeId;
    private Date startDate;
    private Date endDate;
    private Integer createdBy;
    private Date createdOn;
    private boolean resourceReleased;
    private Integer projectTypeId;

   public ResourceAllocationEntityVo() {
   }

	
   public ResourceAllocationEntityVo(boolean resourceReleased) {
       this.resourceReleased = resourceReleased;
   }
   public ResourceAllocationEntityVo(RoleEntityVo role, ModeEntityVo mode, Integer projectId, Integer employeeId, Date startDate, Date endDate, Integer createdBy, Date createdOn, boolean resourceReleased, Integer projectTypeId) {
      this.role = role;
      this.mode = mode;
      this.projectId = projectId;
      this.employeeId = employeeId;
      this.startDate = startDate;
      this.endDate = endDate;
      this.createdBy = createdBy;
      this.createdOn = createdOn;
      this.resourceReleased = resourceReleased;
      this.projectTypeId = projectTypeId;
   }
  
    @Id @GeneratedValue(strategy=IDENTITY)
   
   @Column(name="RESOURCE_ALLOCATION_ID", unique=true, nullable=false)
   public Integer getResourceAllocationId() {
       return this.resourceAllocationId;
   }
   
   public void setResourceAllocationId(Integer resourceAllocationId) {
       this.resourceAllocationId = resourceAllocationId;
   }
@ManyToOne(fetch=FetchType.LAZY)
   @JoinColumn(name="ROLE_ID")
   public RoleEntityVo getRole() {
       return this.role;
   }
   
   public void setRole(RoleEntityVo role) {
       this.role = role;
   }
@ManyToOne(fetch=FetchType.LAZY)
   @JoinColumn(name="MODE_ID")
   public ModeEntityVo getMode() {
       return this.mode;
   }
   
   public void setMode(ModeEntityVo mode) {
       this.mode = mode;
   }
   
   @Column(name="PROJECT_ID")
   public Integer getProjectId() {
       return this.projectId;
   }
   
   public void setProjectId(Integer projectId) {
       this.projectId = projectId;
   }
   
   @Column(name="EMPLOYEE_ID")
   public Integer getEmployeeId() {
       return this.employeeId;
   }
   
   public void setEmployeeId(Integer employeeId) {
       this.employeeId = employeeId;
   }
   @Temporal(TemporalType.DATE)
   @Column(name="START_DATE", length=10)
   public Date getStartDate() {
       return this.startDate;
   }
   
   public void setStartDate(Date startDate) {
       this.startDate = startDate;
   }
   @Temporal(TemporalType.DATE)
   @Column(name="END_DATE", length=10)
   public Date getEndDate() {
       return this.endDate;
   }
   
   public void setEndDate(Date endDate) {
       this.endDate = endDate;
   }
   
   @Column(name="CREATED_BY")
   public Integer getCreatedBy() {
       return this.createdBy;
   }
   
   public void setCreatedBy(Integer createdBy) {
       this.createdBy = createdBy;
   }
   @Temporal(TemporalType.TIMESTAMP)
   @Column(name="CREATED_ON", length=19)
   public Date getCreatedOn() {
       return this.createdOn;
   }
   
   public void setCreatedOn(Date createdOn) {
       this.createdOn = createdOn;
   }
   
   @Column(name="RESOURCE_RELEASED", nullable=false)
   public boolean isResourceReleased() {
       return this.resourceReleased;
   }
   
   public void setResourceReleased(boolean resourceReleased) {
       this.resourceReleased = resourceReleased;
   }
   
   @Column(name="PROJECT_TYPE_ID")
   public Integer getProjectTypeId() {
       return this.projectTypeId;
   }
   
   public void setProjectTypeId(Integer projectTypeId) {
       this.projectTypeId = projectTypeId;
   }

}
