package com.task.vo.custom.entity;

// default package
// Generated 26-Mar-2013 09:40:57 by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.task.common.CommonVo;


/**
 * Role generated by hbm2java
 */
@Entity
@Table(name = "role")
public class RoleEntityVo extends CommonVo implements Serializable {

    /**
     * long serialVersionUID :
     */
    private static final long serialVersionUID = 1L;
    private Integer roleId;
    private String roleName;
    private Set<ResourceAllocationEntityVo> resourceAllocations = new HashSet<ResourceAllocationEntityVo>(
	    0);

    public RoleEntityVo() {
    }

    public RoleEntityVo(String roleName) {
	this.roleName = roleName;
    }

    public RoleEntityVo(String roleName,
	    Set<ResourceAllocationEntityVo> resourceAllocations) {
	this.roleName = roleName;
	this.resourceAllocations = resourceAllocations;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ROLE_ID", unique = true, nullable = false)
    public Integer getRoleId() {
	return this.roleId;
    }

    public void setRoleId(Integer roleId) {
	this.roleId = roleId;
    }

    @Column(name = "ROLE_NAME", nullable = false, length = 50)
    public String getRoleName() {
	return this.roleName;
    }

    public void setRoleName(String roleName) {
	this.roleName = roleName;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "role")
    public Set<ResourceAllocationEntityVo> getResourceAllocations() {
	return this.resourceAllocations;
    }

    public void setResourceAllocations(
	    Set<ResourceAllocationEntityVo> resourceAllocations) {
	this.resourceAllocations = resourceAllocations;
    }

}
