package com.task.vo.custom.entity;
// Generated Mar 1, 2013 7:55:55 PM by Hibernate Tools 3.2.1.GA


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* Unit generated by hbm2java
*/
@Entity
@Table(name="Unit")
public class UnitEntityVo  implements java.io.Serializable {


     private int unitId;
     private String name;

    public UnitEntityVo() {
    }

                
    public UnitEntityVo(int unitId) {
        this.unitId = unitId;
    }
    public UnitEntityVo(int unitId, String name) {
       this.unitId = unitId;
       this.name = name;
    }
   
     @Id 
    
    @Column(name="UnitId", unique=true, nullable=false)
    public int getUnitId() {
        return this.unitId;
    }
    
    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }
    
    @Column(name="Name", length=28)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

}
